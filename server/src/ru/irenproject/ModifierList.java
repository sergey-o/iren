/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ModifierList extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public static final class RemoveEvent implements Event {
    private final Modifier fModifier;

    private RemoveEvent(Modifier modifier) {
      fModifier = modifier;
    }

    public Modifier modifier() {
      return fModifier;
    }
  }

  private final ArrayList<Modifier> fModifiers = new ArrayList<>();

  public ModifierList(Realm realm) {
    super(realm);
  }

  public List<Modifier> modifiers() {
    return Collections.unmodifiableList(fModifiers);
  }

  public void add(Modifier modifier) {
    fModifiers.add(modifier);
    post(ChangeEvent.INSTANCE);
  }

  public boolean isEmpty() {
    return fModifiers.isEmpty();
  }

  public void delete(int index) {
    checkIndex(index);

    post(new RemoveEvent(fModifiers.remove(index)));
    post(ChangeEvent.INSTANCE);
  }

  private void checkIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fModifiers.size());
  }

  public void move(int index, boolean forward) {
    checkIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkIndex(newIndex);

    Collections.swap(fModifiers, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }
}
