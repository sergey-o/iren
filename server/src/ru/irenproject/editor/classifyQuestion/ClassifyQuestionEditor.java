/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.classifyQuestion;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.TestInputException;
import ru.irenproject.classifyQuestion.ClassifyQuestion;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.QuestionEditor;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.AddCategory;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.AddItem;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.DeleteCategory;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.DeleteItem;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.MoveCategory;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.MoveItem;
import ru.irenproject.editor.classifyQuestion.Proto.XClassifyQuestionEditor.SetOptions;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.pad.Pad;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

public final class ClassifyQuestionEditor extends QuestionEditor<ClassifyQuestion> {
  public interface Factory extends QuestionEditor.Factory {
    @Override ClassifyQuestionEditor create(Question question);
  }

  private final PadEditor fFormulationEditor;
  private final LinkedHashMap<Pad, PadEditor> fElementEditors = new LinkedHashMap<>();

  @Inject private PadEditor.Factory fPadEditorFactory;

  @Inject private ClassifyQuestionEditor(@Assisted Question question, PadEditor.Factory padEditorFactory) {
    super((ClassifyQuestion) question);
    fFormulationEditor = padEditorFactory.create(question().formulation());
  }

  @Override public Message render(RenderContext context) {
    XClassifyQuestionEditor.Builder b = XClassifyQuestionEditor.newBuilder()
        .setFormulationEditor(context.link(fFormulationEditor))
        .setCanAddCategory(question().categoryCount() < ClassifyQuestion.MAX_CATEGORIES)
        .setCanAddItem(question().totalItemCount() < ClassifyQuestion.MAX_ITEMS)
        .setMaxItems(ClassifyQuestion.MAX_ITEMS)
        .setHasDefaultOptions(question().hasDefaultOptions());

    for (int categoryCount = question().categoryCount(), i = 0; i < categoryCount; ++i) {
      b.addElementBuilder()
          .setCategoryIndex(i)
          .setEditor(context.link(getElementEditor(question().getCategoryTitle(i))))
          .setCanMoveUp(i > 0)
          .setCanMoveDown(i < categoryCount - 1);

      List<Pad> items = question().getCategoryItems(i);
      for (int itemCount = items.size(), j = 0; j < itemCount; ++j) {
        b.addElementBuilder()
            .setCategoryIndex(i)
            .setItemIndex(j)
            .setEditor(context.link(getElementEditor(items.get(j))))
            .setCanMoveUp((j > 0) || (i > 0))
            .setCanMoveDown((j < itemCount - 1) || (i < categoryCount - 1));
      }
    }

    Integer itemLimit = question().itemLimit();
    if (itemLimit != null) {
      b.setItemLimit(itemLimit);

      //noinspection ConstantConditions
      b.setMinItemsPerCategory(question().minItemsPerCategory());
    }

    return b.build();
  }

  private PadEditor getElementEditor(Pad element) {
    return fElementEditors.computeIfAbsent(element, fPadEditorFactory::create);
  }

  @Override protected void handleEvent(Event e, Resident source) {
    super.handleEvent(e, source);
    if (e instanceof Question.ChangeEvent) {
      fElementEditors.keySet().retainAll(elements());
    }
  }

  private Set<Pad> elements() {
    HashSet<Pad> res = new HashSet<>();
    for (int categoryCount = question().categoryCount(), i = 0; i < categoryCount; ++i) {
      res.add(question().getCategoryTitle(i));
      res.addAll(question().getCategoryItems(i));
    }
    return res;
  }

  @Action(badInput = TestInputException.class)
  public void doAddCategory(AddCategory in) {
    question().addCategory(new Pad(realm()));
  }

  @Action(badInput = TestInputException.class)
  public void doDeleteCategory(DeleteCategory in) {
    question().deleteCategory(in.getIndex());
  }

  @Action(badInput = TestInputException.class)
  public void doMoveCategory(MoveCategory in) {
    question().moveCategory(in.getIndex(), in.getForward());
  }

  @Action(badInput = TestInputException.class)
  public void doAddItem(AddItem in) {
    question().addItem(in.getCategoryIndex(), new Pad(realm()));
  }

  @Action(badInput = TestInputException.class)
  public void doDeleteItem(DeleteItem in) {
    question().deleteItem(in.getCategoryIndex(), in.getItemIndex());
  }

  @Action(badInput = TestInputException.class)
  public void doMoveItem(MoveItem in) {
    question().moveItem(in.getCategoryIndex(), in.getItemIndex(), in.getForward());
  }

  @Action(badInput = TestInputException.class)
  public void doSetOptions(SetOptions in) {
    if (in.hasItemLimit()) {
      Check.input(in.hasMinItemsPerCategory());
      question().setLimits(in.getItemLimit(), in.getMinItemsPerCategory());
    } else {
      question().clearLimits();
    }
  }
}
