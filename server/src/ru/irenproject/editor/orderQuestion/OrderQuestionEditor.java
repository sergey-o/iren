/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.orderQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.QuestionEditor;
import ru.irenproject.editor.orderQuestion.Proto.Page;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor.AddElement;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor.DeleteElement;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor.MoveElement;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor.SelectPage;
import ru.irenproject.editor.orderQuestion.Proto.XOrderQuestionEditor.SetOptions;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.orderQuestion.OrderQuestion;
import ru.irenproject.pad.Pad;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public final class OrderQuestionEditor extends QuestionEditor<OrderQuestion> {
  public interface Factory extends QuestionEditor.Factory {
    @Override OrderQuestionEditor create(Question question);
  }

  private final PadEditor fFormulationEditor;
  private final LinkedHashMap<Pad, PadEditor> fElementEditors = new LinkedHashMap<>();
  private Page fPage = Page.ITEMS;

  @Inject private PadEditor.Factory fPadEditorFactory;

  @Inject private OrderQuestionEditor(@Assisted Question question, PadEditor.Factory padEditorFactory) {
    super((OrderQuestion) question);
    fFormulationEditor = padEditorFactory.create(question().formulation());
  }

  @Override public Message render(RenderContext context) {
    XOrderQuestionEditor.Builder b = XOrderQuestionEditor.newBuilder()
        .setFormulationEditor(context.link(fFormulationEditor))
        .setPage(fPage)
        .addAllElementEditor(context.linkAll(getElementEditors(
            (fPage == Page.ITEMS) ? question().items() : question().distractors())))
        .setItemCount(question().items().size())
        .setDistractorCount(question().distractors().size())
        .setMaxElements(OrderQuestion.MAX_ELEMENTS)
        .setHasDefaultOptions(question().hasDefaultOptions());

    Integer itemLimit = question().itemLimit();
    if (itemLimit != null) {
      b.setItemLimit(itemLimit);
    }

    Integer distractorLimit = question().distractorLimit();
    if (distractorLimit != null) {
      b.setDistractorLimit(distractorLimit);
    }

    return b.build();
  }

  private List<PadEditor> getElementEditors(Collection<Pad> elements) {
    return elements.stream()
        .map(element -> fElementEditors.computeIfAbsent(element, fPadEditorFactory::create))
        .collect(Collectors.toList());
  }

  @Override protected void handleEvent(Event e, Resident source) {
    super.handleEvent(e, source);
    if (e instanceof Question.ChangeEvent) {
      fElementEditors.keySet().retainAll(ImmutableSet.copyOf(Iterables.concat(question().items(),
          question().distractors())));
    }
  }

  @Action public void doSelectPage(SelectPage in) {
    fPage = in.getPage();
    markForRender();
  }

  @Action(badInput = TestInputException.class)
  public void doAddElement(AddElement in) {
    switch (fPage) {
      case ITEMS: {
        question().addItem(new Pad(realm()));
        break;
      }
      case DISTRACTORS: {
        question().addDistractor(new Pad(realm()));
        break;
      }
    }
  }

  @Action(badInput = TestInputException.class)
  public void doDeleteElement(DeleteElement in) {
    switch (fPage) {
      case ITEMS: {
        question().deleteItem(in.getIndex());
        break;
      }
      case DISTRACTORS: {
        question().deleteDistractor(in.getIndex());
        break;
      }
    }
  }

  @Action(badInput = TestInputException.class)
  public void doMoveElement(MoveElement in) {
    switch (fPage) {
      case ITEMS: {
        question().moveItem(in.getIndex(), in.getForward());
        break;
      }
      case DISTRACTORS: {
        question().moveDistractor(in.getIndex(), in.getForward());
        break;
      }
    }
  }

  @Action(badInput = TestInputException.class)
  public void doSetOptions(SetOptions in) {
    question().setItemLimit(in.hasItemLimit() ? in.getItemLimit() : null);
    question().setDistractorLimit(in.hasDistractorLimit() ? in.getDistractorLimit() : null);
  }
}
