/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.QuestionList;
import ru.irenproject.Section;
import ru.irenproject.TestInputException;
import ru.irenproject.TestUtils;
import ru.irenproject.editor.Proto.XQuestionListEditor;
import ru.irenproject.editor.Proto.XQuestionListEditor.Add;
import ru.irenproject.editor.Proto.XQuestionListEditor.Copy;
import ru.irenproject.editor.Proto.XQuestionListEditor.Cut;
import ru.irenproject.editor.Proto.XQuestionListEditor.Delete;
import ru.irenproject.editor.Proto.XQuestionListEditor.Move;
import ru.irenproject.editor.Proto.XQuestionListEditor.Paste;
import ru.irenproject.editor.Proto.XQuestionListEditor.Select;
import ru.irenproject.editor.Proto.XQuestionListEditor.SetEnabled;
import ru.irenproject.editor.Proto.XQuestionListEditor.SetWeight;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxOutput;

import com.google.common.collect.Lists;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.apache.commons.lang3.mutable.MutableObject;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class QuestionListEditor extends Widget {
  public interface Factory {
    QuestionListEditor create(QuestionList questionList, Section rootSection, String clipboardOrigin);
  }

  public enum SelectEvent implements Event { INSTANCE }

  private final QuestionList fQuestionList;
  private final Section fRootSection;
  private final String fClipboardOrigin;
  private final QuestionListSelection fSelection;
  private final LinkedHashMap<QuestionItem, QuestionItemEditor> fEditors = new LinkedHashMap<>();

  @Inject private Map<String, Question.Factory> fQuestionFactoriesByType;
  @Inject private QuestionItemEditor.Factory fQuestionItemEditorFactory;
  @Inject private ItxOutput fItxOutput;
  @Inject private ItxInput fItxInput;

  @Inject private QuestionListEditor(
      @Assisted QuestionList questionList,
      @Assisted Section rootSection,
      @Assisted String clipboardOrigin) {
    super(questionList.realm());
    fQuestionList = questionList;
    fQuestionList.addListener(this);

    fRootSection = rootSection;
    fClipboardOrigin = clipboardOrigin;

    fSelection = new QuestionListSelection(fQuestionList);
    if (!fQuestionList.isEmpty()) {
      fSelection.add(fQuestionList.items().get(0));
    }
  }

  public @Nullable QuestionItem activeItem() {
    return (fSelection.items().size() == 1) ? fSelection.items().get(0) : null;
  }

  @Action public void doSelect(Select in) {
    List<QuestionItem> items = fQuestionList.items();
    ArrayList<QuestionItem> selectItems = new ArrayList<>();
    int last = -1;

    for (int index : in.getIndexList()) {
      Check.input(index > last);
      last = index;
      Check.input(index < items.size());
      selectItems.add(items.get(index));
    }

    setSelectedItems(selectItems);
  }

  @Override public Message render(RenderContext context) {
    return XQuestionListEditor.newBuilder()
        .addAllItemEditor(context.linkAll(fQuestionList.items().stream()
            .map(item -> fEditors.computeIfAbsent(item, fQuestionItemEditorFactory::create))
            .collect(Collectors.toList())))
        .setSelection(context.link(fSelection))
        .setMaxWeight(QuestionItem.MAX_WEIGHT)
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof QuestionList.ChangeEvent) {
      markForRender();
      fSelection.forceRender();
    } else if (e instanceof QuestionList.RemoveItemEvent) {
      QuestionItem removedItem = ((QuestionList.RemoveItemEvent) e).item();
      fEditors.remove(removedItem);
      if (fSelection.remove(removedItem)) {
        post(SelectEvent.INSTANCE);
      }
    }
  }

  private void setSelectedItems(Collection<QuestionItem> items) {
    fSelection.clear();
    fSelection.addAll(items);
    post(SelectEvent.INSTANCE);
  }

  private void setSelectedItem(QuestionItem item) {
    setSelectedItems(Collections.singleton(item));
  }

  @Action public void doCopy(Copy in) {
    copy();
  }

  private void copy() {
    try {
      if (!fSelection.items().isEmpty()) {
        try (OutputStream clipboard = Files.newOutputStream(EditorUtils.clipboardFile())) {
          fItxOutput.writeFragment(out -> {
            List<QuestionItem> items = fQuestionList.items();
            for (int i : fSelection.ascendingIndices()) {
              out.writeQuestionItem(items.get(i));
            }
          }, clipboard, fClipboardOrigin);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Action public void doCut(Cut in) {
    copy();
    fQuestionList.removeItems(fSelection.items());
  }

  @Action public void doPaste(Paste in) {
    if (Files.exists(EditorUtils.clipboardFile())) {
      MutableObject<String> origin = new MutableObject<>();
      List<QuestionItem> items = fItxInput.read(EditorUtils.clipboardFile(), realm(), reader -> {
        origin.setValue(reader.root().getAttribute(Itx.ATTR_ORIGIN));
        return Itx.getChildren(reader.root(), Itx.ELEM_QUESTION).stream()
            .map(reader::readQuestionItem)
            .collect(Collectors.toList());
      });

      boolean sameOrigin = origin.getValue().equals(fClipboardOrigin);
      HashSet<String> existingTags = sameOrigin ? fRootSection.listTreeQuestions().stream()
          .map(QuestionItem::tag)
          .collect(Collectors.toCollection(HashSet::new)) : null;

      for (QuestionItem item : items) {
        if ((TestUtils.toNumericTagIfPossible(item.tag()) != null) && !(sameOrigin && existingTags.add(item.tag()))) {
          item.setTag("");
        }
        fQuestionList.addItem(item);
      }

      setSelectedItems(items);
    }
  }

  @Action public void doDelete(Delete in) {
    int selectedIndex = (fSelection.items().size() == 1) ?
        fQuestionList.items().indexOf(fSelection.items().get(0)) : -1;

    fQuestionList.removeItems(fSelection.items());

    selectedIndex = Integer.min(selectedIndex, fQuestionList.items().size() - 1);
    if (selectedIndex >= 0) {
      setSelectedItem(fQuestionList.items().get(selectedIndex));
    }
  }

  @Action(badInput = TestInputException.class)
  public void doAdd(Add in) {
    Question question = Check.inputNotNull(fQuestionFactoriesByType.get(in.getType())).create(realm());
    question.setDefaults(in.getInitialText());

    QuestionItem item = new QuestionItem(question);
    fQuestionList.addItem(item);
    setSelectedItem(item);
  }

  @Action(badInput = TestInputException.class)
  public void doSetWeight(SetWeight in) {
    for (QuestionItem item : fSelection.items()) {
      item.setWeight(in.getWeight());
    }
  }

  @Action public void doSetEnabled(SetEnabled in) {
    for (QuestionItem item : fSelection.items()) {
      item.setEnabled(in.getEnabled());
    }
  }

  @Action(badInput = TestInputException.class)
  public void doMove(Move in) {
    List<Integer> indices = fSelection.ascendingIndices();
    if (in.getForward()) {
      indices = Lists.reverse(indices);
    }

    for (int index : indices) {
      fQuestionList.moveItem(index, in.getForward());
    }
  }
}
