/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Check;
import ru.irenproject.Section;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Proto.SectionNode;
import ru.irenproject.editor.Proto.XSectionTreeEditor;
import ru.irenproject.editor.Proto.XSectionTreeEditor.Add;
import ru.irenproject.editor.Proto.XSectionTreeEditor.Delete;
import ru.irenproject.editor.Proto.XSectionTreeEditor.Move;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import com.google.common.collect.Iterables;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.stream.Collectors;

public final class SectionTreeEditor extends Widget {
  public interface Factory {
    SectionTreeEditor create(Section root);
  }

  public enum SelectEvent implements Event { INSTANCE }

  private final Section fRoot;
  private final SectionTreeSelection fSelection;
  private final LinkedHashMap<Section, SectionEditor> fEditors = new LinkedHashMap<>();

  @SuppressWarnings("FieldMayBeFinal")
  @Inject private SectionEditor.Factory fSectionEditorFactory;

  @Inject private SectionTreeEditor(@Assisted Section root, SectionEditor.Factory sectionEditorFactory) {
    super(root.realm());
    fRoot = root;
    fSectionEditorFactory = sectionEditorFactory; // will be injected later but is needed now

    fSelection = new SectionTreeSelection(getEditor(fRoot));
    fRoot.addListener(this);
  }

  private SectionEditor getEditor(Section section) {
    return fEditors.computeIfAbsent(section, s -> fSectionEditorFactory.create(s, this));
  }

  @Override public Message render(RenderContext context) {
    return XSectionTreeEditor.newBuilder()
        .setRoot(renderSection(fRoot, context))
        .setSelection(context.link(fSelection))
        .setMaxSectionNameLength(Section.MAX_NAME_LENGTH)
        .build();
  }

  private SectionNode renderSection(Section section, RenderContext context) {
    return SectionNode.newBuilder()
        .setEditor(context.link(getEditor(section)))
        .addAllChild(section.sections().stream()
            .map(s -> renderSection(s, context))
            .collect(Collectors.toList()))
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Section.ChangeTreeEvent) {
      markForRender();
      fSelection.forceRender();
    } else if (e instanceof Section.RemoveSectionEvent) {
      Section.RemoveSectionEvent event = (Section.RemoveSectionEvent) e;

      if (event.section() == selectedSection()) {
        Section sectionToSelect;
        if (event.parent().root() == fRoot) {
          int index = Integer.min(event.index(), event.parent().sections().size() - 1);
          sectionToSelect = (index == -1) ? event.parent() : event.parent().sections().get(index);
        } else {
          sectionToSelect = fRoot;
        }
        select(getEditor(sectionToSelect));
      }

      fEditors.remove(event.section());
    }
  }

  public void select(SectionEditor editor) {
    fSelection.setSelectedEditor(editor);
    post(SelectEvent.INSTANCE);
  }

  @Action(badInput = TestInputException.class)
  public void doAdd(Add in) {
    Section section = new Section(realm());
    section.setName(in.getName());
    selectedSection().addSection(section);
  }

  public Section selectedSection() {
    return fSelection.selectedEditor().section();
  }

  @Action public void doDelete(Delete in) {
    Check.inputNotNull(selectedSection().parent()).removeSection(selectedSection());
  }

  @Action public void doMove(Move in) {
    Check.input(in.getForward() ? canMoveDown() : canMoveUp());

    Section parent = Check.notNull(selectedSection().parent());
    int index = parent.sections().indexOf(selectedSection());

    if (in.getForward()) {
      if (index < parent.sections().size() - 1) {
        Section nextSibling = parent.sections().get(index + 1);
        selectedSection().moveTo(nextSibling, 0);
      } else {
        Section grandparent = Check.notNull(parent.parent());
        selectedSection().moveTo(grandparent, grandparent.sections().indexOf(parent) + 1);
      }
    } else {
      if (index > 0) {
        Section previousSibling = parent.sections().get(index - 1);
        selectedSection().moveTo(previousSibling, previousSibling.sections().size());
      } else {
        Section grandparent = Check.notNull(parent.parent());
        selectedSection().moveTo(grandparent, grandparent.sections().indexOf(parent));
      }
    }
  }

  public boolean canMoveUp() {
    return (selectedSection() != fRoot) && (selectedSection() != fRoot.sections().get(0));
  }

  public boolean canMoveDown() {
    return (selectedSection() != fRoot) && (selectedSection() != Iterables.getLast(fRoot.sections()));
  }
}
