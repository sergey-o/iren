/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.inputQuestion;

import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor.SetTextPatternOptions;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.TextPattern;

import com.google.inject.assistedinject.Assisted;

import javax.inject.Inject;

public final class TextPatternEditor extends PatternEditor<TextPattern> {
  public interface Factory extends PatternEditor.Factory {
    @Override TextPatternEditor create(Pattern pattern);
  }

  @Inject private TextPatternEditor(@Assisted Pattern pattern) {
    super((TextPattern) pattern);
  }

  @Override protected void renderSupplement(XPatternEditor.Builder b) {
    b.getTextSupplementBuilder()
        .setCaseSensitive(pattern().caseSensitive())
        .setSpaceDelimited(pattern().spaceDelimited())
        .setWildcard(pattern().wildcard())
        .setMaxPrecisionLength(TextPattern.MAX_PRECISION_LENGTH);
    if (pattern().precision() != null) {
      b.getTextSupplementBuilder().setPrecision(pattern().precisionAsString());
    }
  }

  @Action(badInput = TestInputException.class)
  public void doSetTextPatternOptions(SetTextPatternOptions in) {
    pattern().setPrecisionAsString(in.hasPrecision() ? in.getPrecision() : null);
    pattern().setCaseSensitive(in.getCaseSensitive());
    pattern().setSpaceDelimited(in.getSpaceDelimited());
    pattern().setWildcard(in.getWildcard());
  }
}
