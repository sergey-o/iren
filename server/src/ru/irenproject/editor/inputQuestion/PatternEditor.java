/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.inputQuestion;

import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor.SetQuality;
import ru.irenproject.editor.inputQuestion.Proto.XPatternEditor.SetValue;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.inputQuestion.Pattern;

import com.google.protobuf.Message;

public abstract class PatternEditor<T extends Pattern> extends Widget {
  public interface Factory {
    @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
    PatternEditor<?> create(Pattern pattern);
  }

  private final T fPattern;

  protected PatternEditor(T pattern) {
    super(pattern.realm());
    fPattern = pattern;
    fPattern.addListener(this);
  }

  public final T pattern() {
    return fPattern;
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Pattern.ChangeEvent) {
      markForRender();
    }
  }

  @Override public final Message render(RenderContext context) {
    XPatternEditor.Builder b = XPatternEditor.newBuilder()
        .setValue(pattern().value())
        .setMaxLength(Pattern.MAX_LENGTH)
        .setQualityPercent(pattern().qualityPercent())
        .setHasDefaultOptions(pattern().hasDefaultOptions());
    renderSupplement(b);
    return b.build();
  }

  protected abstract void renderSupplement(XPatternEditor.Builder b);

  @Action(badInput = TestInputException.class)
  public final void doSetValue(SetValue in) {
    pattern().setValue(in.getValue());
  }

  @Action(badInput = TestInputException.class)
  public final void doSetQuality(SetQuality in) {
    pattern().setQualityPercent(in.getQualityPercent());
  }
}
