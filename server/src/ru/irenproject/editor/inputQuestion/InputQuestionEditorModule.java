/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.inputQuestion;

import ru.irenproject.editor.QuestionEditorModule;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.inputQuestion.TextPattern;

import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.MapBinder;

public final class InputQuestionEditorModule extends QuestionEditorModule {
  public InputQuestionEditorModule() {
    super(InputQuestion.class, InputQuestionEditor.Factory.class);
  }

  @Override protected void configure() {
    super.configure();

    install(new FactoryModuleBuilder().build(TextPatternEditor.Factory.class));
    install(new FactoryModuleBuilder().build(RegexpPatternEditor.Factory.class));

    MapBinder<Class<? extends Pattern>, PatternEditor.Factory> b = MapBinder.newMapBinder(binder(),
        new TypeLiteral<>() {}, new TypeLiteral<>() {});
    b.addBinding(TextPattern.class).to(TextPatternEditor.Factory.class);
    b.addBinding(RegexpPattern.class).to(RegexpPatternEditor.Factory.class);
  }
}
