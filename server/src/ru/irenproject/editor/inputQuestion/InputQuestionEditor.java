/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.inputQuestion;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.QuestionEditor;
import ru.irenproject.editor.inputQuestion.Proto.XInputQuestionEditor;
import ru.irenproject.editor.inputQuestion.Proto.XInputQuestionEditor.AddRegexpPattern;
import ru.irenproject.editor.inputQuestion.Proto.XInputQuestionEditor.AddTextPattern;
import ru.irenproject.editor.inputQuestion.Proto.XInputQuestionEditor.DeletePattern;
import ru.irenproject.editor.inputQuestion.Proto.XInputQuestionEditor.MovePattern;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.inputQuestion.TextPattern;

import com.google.common.collect.Iterables;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public final class InputQuestionEditor extends QuestionEditor<InputQuestion> {
  public interface Factory extends QuestionEditor.Factory {
    @Override InputQuestionEditor create(Question question);
  }

  private final PadEditor fFormulationEditor;
  private final LinkedHashMap<Pattern, PatternEditor<?>> fPatternEditors = new LinkedHashMap<>();

  @Inject private Map<Class<? extends Pattern>, PatternEditor.Factory> fPatternEditorFactories;

  @Inject private InputQuestionEditor(@Assisted Question question, PadEditor.Factory padEditorFactory) {
    super((InputQuestion) question);
    fFormulationEditor = padEditorFactory.create(question().formulation());
  }

  @Override public Message render(RenderContext context) {
    return XInputQuestionEditor.newBuilder()
        .setFormulationEditor(context.link(fFormulationEditor))
        .addAllPatternEditor(context.linkAll(question().patterns().stream()
            .map(pattern -> fPatternEditors.computeIfAbsent(pattern, this::createPatternEditor))
            .collect(Collectors.toList())))
        .setMaxPatterns(InputQuestion.MAX_PATTERNS)
        .build();
  }

  private PatternEditor<?> createPatternEditor(Pattern pattern) {
    return Check.notNull(fPatternEditorFactories.get(pattern.getClass())).create(pattern);
  }

  @Action(badInput = TestInputException.class)
  public void doAddTextPattern(AddTextPattern in) {
    addPattern(new TextPattern(realm()));
  }

  private void addPattern(Pattern p) {
    Pattern last = Iterables.getLast(question().patterns(), null);
    if (last != null) {
      p.setQualityPercent(last.qualityPercent());
      p.setOptionsFrom(last);
    }

    question().addPattern(p);
  }

  @Action(badInput = TestInputException.class)
  public void doAddRegexpPattern(AddRegexpPattern in) {
    addPattern(new RegexpPattern(realm()));
  }

  @Override protected void handleEvent(Event e, Resident source) {
    super.handleEvent(e, source);
    if (e instanceof Question.ChangeEvent) {
      fPatternEditors.keySet().retainAll(question().patterns());
    }
  }

  @Action(badInput = TestInputException.class)
  public void doDeletePattern(DeletePattern in) {
    question().deletePattern(in.getIndex());
  }

  @Action(badInput = TestInputException.class)
  public void doMovePattern(MovePattern in) {
    question().movePattern(in.getIndex(), in.getForward());
  }
}
