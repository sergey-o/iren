/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.ModifierList;
import ru.irenproject.QuestionItem;
import ru.irenproject.editor.Proto.XQuestionItemEditor;
import ru.irenproject.editor.Proto.XQuestionItemEditor.ShowModifiers;
import ru.irenproject.editor.modifier.ModifierScreen;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxOutput;
import ru.irenproject.pad.Pad;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class QuestionItemEditor extends Widget {
  public interface Factory {
    QuestionItemEditor create(QuestionItem item);
  }

  private static final int SUMMARY_SOFT_LIMIT = 65;
  private static final int SUMMARY_HARD_LIMIT = SUMMARY_SOFT_LIMIT + 15;

  private final QuestionItem fItem;
  private @Nullable ModifierScreen fModifierScreen;

  @Inject private ItxOutput fItxOutput;
  @Inject private ItxInput fItxInput;
  @Inject private ModifierScreen.Factory fModifierScreenFactory;

  @Inject private QuestionItemEditor(@Assisted QuestionItem item) {
    super(item.realm());
    fItem = item;
    fItem.addListener(this);
    fItem.modifierList().addListener(this);
    fItem.question().formulation().addListener(this);
  }

  @Override public Message render(RenderContext context) {
    return XQuestionItemEditor.newBuilder()
        .setText(fItem.question().formulation().getSummary(SUMMARY_SOFT_LIMIT, SUMMARY_HARD_LIMIT))
        .setWeight(fItem.weight())
        .setType(fItem.question().type())
        .setEnabled(fItem.enabled())
        .setHasModifiers(!fItem.modifierList().isEmpty())
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if ((e instanceof QuestionItem.ChangeEvent) || (e instanceof Pad.ChangeEvent)
        || (e instanceof ModifierList.ChangeEvent)) {
      markForRender();
    }
  }

  @Action public void doShowModifiers(ShowModifiers in) {
    if (fModifierScreen == null) {
      fModifierScreen = fModifierScreenFactory.create(fItem.modifierList(), fItem.question().type(), null);
    }
    showScreen(fModifierScreen);
  }
}
