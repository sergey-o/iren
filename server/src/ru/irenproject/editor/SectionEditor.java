/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.ModifierList;
import ru.irenproject.QuestionList;
import ru.irenproject.Section;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Proto.XSectionEditor;
import ru.irenproject.editor.Proto.XSectionEditor.Select;
import ru.irenproject.editor.Proto.XSectionEditor.SetName;
import ru.irenproject.editor.Proto.XSectionEditor.ShowModifiers;
import ru.irenproject.editor.modifier.ModifierScreen;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class SectionEditor extends Widget {
  public interface Factory {
    SectionEditor create(Section section, SectionTreeEditor sectionTreeEditor);
  }

  private final Section fSection;
  private final SectionTreeEditor fSectionTreeEditor;
  private @Nullable ModifierScreen fModifierScreen;

  @Inject private ModifierScreen.Factory fModifierScreenFactory;

  @Inject private SectionEditor(@Assisted Section section, @Assisted SectionTreeEditor sectionTreeEditor) {
    super(section.realm());
    fSection = section;
    fSectionTreeEditor = sectionTreeEditor;

    fSection.addListener(this);
    fSection.questionList().addListener(this);
    fSection.modifierList().addListener(this);
  }

  public Section section() {
    return fSection;
  }

  public SectionTreeEditor sectionTreeEditor() {
    return fSectionTreeEditor;
  }

  @Override public Message render(RenderContext context) {
    return XSectionEditor.newBuilder()
        .setName(fSection.name())
        .setQuestionCount(fSection.questionList().items().size())
        .setHasModifiers(!fSection.modifierList().isEmpty())
        .build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if ((e instanceof Section.ChangeEvent) || (e instanceof QuestionList.ChangeEvent)
        || (e instanceof ModifierList.ChangeEvent)) {
      markForRender();
    }
  }

  @Action public void doSelect(Select in) {
    fSectionTreeEditor.select(this);
  }

  @Action(badInput = TestInputException.class)
  public void doSetName(SetName in) {
    fSection.setName(in.getName());
  }

  @Action public void doShowModifiers(ShowModifiers in) {
    if (fModifierScreen == null) {
      fModifierScreen = fModifierScreenFactory.create(fSection.modifierList(), null, fSection);
    }
    showScreen(fModifierScreen);
  }
}
