/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.editor.Proto.FileManagerChannel;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Proto.Request;
import ru.irenproject.infra.RequestProcessor;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import io.netty.util.concurrent.EventExecutor;
import io.netty.util.concurrent.ScheduledFuture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.security.MessageDigest;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Predicate;

public final class EditorMessageHandler extends ChannelInboundHandlerAdapter {
  public interface Factory {
    EditorMessageHandler create(byte[] editorKey);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(EditorMessageHandler.class);

  private static final int AUTO_FREEZE_DELAY_SECONDS = 20;

  private static final AtomicBoolean fConnected = new AtomicBoolean();

  private final FileManager.Factory fFileManagerFactory;

  private final byte[] fEditorKey;
  private final LinkedHashMap<String, RequestProcessor> fProcessorsByChannel = new LinkedHashMap<>();
  private boolean fActive;
  private boolean fAuthenticated;
  private @Nullable ScheduledFuture<?> fAutoFreezeTask;

  @Inject private EditorMessageHandler(@Assisted byte[] editorKey, FileManager.Factory fileManagerFactory) {
    fEditorKey = editorKey.clone();
    fFileManagerFactory = fileManagerFactory;
  }

  @Override public void channelActive(ChannelHandlerContext ctx) throws Exception {
    Check.input(fConnected.compareAndSet(false, true));
    fActive = true;
    super.channelActive(ctx);
  }

  @Override public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    if (fActive) {
      if (fAutoFreezeTask != null) {
        fAutoFreezeTask.cancel(false);
        fAutoFreezeTask = null;
      }
      freeze(p -> true);

      fProcessorsByChannel.values().forEach(RequestProcessor::close);

      fConnected.set(false);
    }

    super.channelInactive(ctx);
  }

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if (!fActive) {
      ReferenceCountUtil.release(msg);
      throw new BadInputException();
    }

    if (fAuthenticated) {
      if (msg instanceof byte[]) {
        Request request;
        try {
          request = Request.parseFrom((byte[]) msg);
        } catch (InvalidProtocolBufferException e) {
          throw new BadInputException(e);
        }

        scheduleAutoFreeze(ctx.executor());
        Reply reply = Check.inputNotNull(fProcessorsByChannel.get(request.getChannel())).process(request);

        ctx.writeAndFlush(reply).addListener(Utils.CLOSE_ON_ERROR);
      } else {
        super.channelRead(ctx, msg);
      }
    } else {
      if (msg instanceof byte[]) {
        Check.that(MessageDigest.isEqual((byte[]) msg, fEditorKey), () -> new BadInputException("Wrong editor key."));
        fAuthenticated = true;
        fProcessorsByChannel.put(FileManagerChannel.fileManager.name(),
            fFileManagerFactory.create(fProcessorsByChannel));
      } else {
        ReferenceCountUtil.release(msg);
        throw new BadInputException();
      }
    }
  }

  private void freeze(Predicate<EditorProcessor> filter) {
    for (RequestProcessor p : fProcessorsByChannel.values()) {
      if (p instanceof EditorProcessor) {
        EditorProcessor processor = (EditorProcessor) p;
        if (filter.test(processor)) {
          try {
            processor.freeze();
          } catch (RuntimeException e) {
            fLogger.error("", e);
          }
        }
      }
    }
  }

  private void scheduleAutoFreeze(EventExecutor executor) {
    if (fAutoFreezeTask == null) {
      fAutoFreezeTask = executor.schedule(() -> {
        fAutoFreezeTask = null;
        freeze(EditorProcessor::mayAutoFreeze);
      }, AUTO_FREEZE_DELAY_SECONDS, TimeUnit.SECONDS);
    }
  }
}
