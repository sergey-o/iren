/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.ProgramInfo;
import ru.irenproject.Utils;
import ru.irenproject.archiveEditor.ArchiveEditorDriver;
import ru.irenproject.editor.Proto.OpenFile;
import ru.irenproject.editor.Proto.XFileManager;
import ru.irenproject.editor.Proto.XFileManager.Close;
import ru.irenproject.editor.Proto.XFileManager.ExportToHtml;
import ru.irenproject.editor.Proto.XFileManager.Nop;
import ru.irenproject.editor.Proto.XFileManager.Open;
import ru.irenproject.editor.Proto.XFileManager.Save;
import ru.irenproject.infra.ActionPerformer;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Proto.Request;
import ru.irenproject.infra.Proto.SceneDelta;
import ru.irenproject.infra.Proto.Shape;
import ru.irenproject.infra.RequestProcessor;

import com.google.common.base.Ascii;
import com.google.common.io.BaseEncoding;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Supplier;

public final class FileManager implements RequestProcessor {
  public interface Factory {
    FileManager create(Map<String, RequestProcessor> processorsByChannel);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(FileManager.class);

  private static final String AVAILABLE_FILE = "available";
  private static final String FROZEN_EDITOR_FILE = "editor.frozen";
  private static final String NAME_FILE = "name";
  private static final String TYPE_FILE = "type";
  private static final String NUMBER_FILE = "number";

  private static final String TEST_DOCUMENT_EDITOR_TYPE = "test";
  private static final String ARCHIVE_EDITOR_TYPE = "archive";

  private static final String BLOB_STORE_DIRECTORY = "blobStore";

  private final Map<String, RequestProcessor> fProcessorsByChannel;
  private final EditorProcessor.Factory fEditorProcessorFactory;
  private final KryoFreezer.Factory fKryoFreezerFactory;
  private final ActionPerformer fActionPerformer;
  private final TestDocumentEditorDriver.Factory fTestDocumentEditorDriverFactory;
  private final ArchiveEditorDriver.Factory fArchiveEditorDriverFactory;

  private final LinkedHashMap<String, OpenFile> fOpenFilesByChannel = new LinkedHashMap<>();
  private long fLastFileNumber;

  private long fLastReplyId;

  @Inject private FileManager(
      @Assisted Map<String, RequestProcessor> processorsByChannel,
      EditorProcessor.Factory editorProcessorFactory,
      KryoFreezer.Factory kryoFreezerFactory,
      ActionPerformer actionPerformer,
      TestDocumentEditorDriver.Factory testDocumentEditorDriverFactory,
      ArchiveEditorDriver.Factory archiveEditorDriverFactory) {
    fProcessorsByChannel = processorsByChannel;
    fEditorProcessorFactory = editorProcessorFactory;
    fKryoFreezerFactory = kryoFreezerFactory;
    fActionPerformer = actionPerformer;
    fTestDocumentEditorDriverFactory = testDocumentEditorDriverFactory;
    fArchiveEditorDriverFactory = archiveEditorDriverFactory;

    openEditors();
  }

  @Override public void close() {}

  private void openEditors() {
    TreeMap<Long, Path> editorDirsByNumber = new TreeMap<>();

    for (Path dir : Utils.listDirectory(openFilesPath())) {
      if (Files.isDirectory(dir)) {
        try {
          if (Files.exists(dir.resolve(AVAILABLE_FILE))) {
            long number = readEditorNumber(dir);
            fLastFileNumber = Long.max(fLastFileNumber, number);
            editorDirsByNumber.put(number, dir);
          } else {
            deleteEditorDirectory(dir);
          }
        } catch (RuntimeException e) {
          fLogger.warn("Cannot access editor in '{}'.", dir, e);
        }
      }
    }

    for (Path dir : editorDirsByNumber.values()) {
      try {
        Supplier<EditorDriver> driverSupplier;
        switch (getType(dir)) {
          case TEST_DOCUMENT_EDITOR_TYPE: {
            driverSupplier = () -> fTestDocumentEditorDriverFactory.create(dir.resolve(BLOB_STORE_DIRECTORY));
            break;
          }
          case ARCHIVE_EDITOR_TYPE: {
            driverSupplier = () -> fArchiveEditorDriverFactory.create(Paths.get(getFileName(dir)));
            break;
          }
          default: {
            throw new RuntimeException();
          }
        }

        register(dir, fEditorProcessorFactory.create(driverSupplier, null, createKryoFreezer(dir)));
      } catch (RuntimeException e) {
        fLogger.warn("Cannot load editor from '{}'.", dir, e);
      }
    }
  }

  private KryoFreezer createKryoFreezer(Path editorDir) {
    return fKryoFreezerFactory.create(editorDir.resolve(FROZEN_EDITOR_FILE));
  }

  private static Path openFilesPath() {
    return EditorUtils.dataDirectory().resolve("openFiles-" + ProgramInfo.VERSION);
  }

  private static long readEditorNumber(Path editorDir) {
    try {
      return Long.parseLong(Files.readString(editorDir.resolve(NUMBER_FILE)));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void register(Path editorDir, EditorProcessor processor) {
    String channelName = "editor-" + editorDir.getFileName();
    String editorName = getFileName(editorDir);

    fProcessorsByChannel.put(channelName, processor);
    fOpenFilesByChannel.put(channelName, OpenFile.newBuilder()
        .setChannel(channelName)
        .setName(editorName)
        .build());
  }

  private static void deleteEditorDirectory(Path editorDirectory) {
    Utils.deleteShallowDirectory(editorDirectory.resolve(BLOB_STORE_DIRECTORY));
    Utils.deleteShallowDirectory(editorDirectory);
  }

  @Override public Reply process(Request request) {
    ++fLastReplyId;

    boolean badInput;
    Message output;

    try {
      output = fActionPerformer.perform(this, request.getActionName(), request.getInput());
      badInput = false;
    } catch (BadInputException e) {
      output = null;
      badInput = true;
      fLogger.warn("", e);
    }

    XFileManager shape = XFileManager.newBuilder()
        .addAllFile(fOpenFilesByChannel.values())
        .build();

    Reply.Builder reply = Reply.newBuilder()
        .setChannel(request.getChannel())
        .setId(fLastReplyId)
        .setOk(!badInput)
        .setSceneDelta(SceneDelta.newBuilder()
            .addChanged(Shape.newBuilder()
                .setName("")
                .setType(shape.getDescriptorForType().getName())
                .setData(shape.toByteString())));
    if (output != null) {
      reply.setOutput(output.toByteString());
    }
    return reply.build();
  }

  @SuppressWarnings("EmptyMethod")
  @Action public void doNop(Nop in) {}

  @Action public Open.Reply doOpen(Open in) {
    Open.Reply.Result result;

    try {
      Path file;
      try {
        file = in.getCreateNew() ? null : Paths.get(in.getFileName());
      } catch (InvalidPathException e) {
        throw new BadInputException(e);
      }

      if (file != null) {
        Check.input(Files.isReadable(file));
      }

      Locale locale = Locale.forLanguageTag(in.getLanguage());

      byte[] id = new byte[24];
      new SecureRandom().nextBytes(id);
      String channel = BaseEncoding.base16().lowerCase().encode(id);
      Path editorDir = openFilesPath().resolve(channel);

      boolean archive = Ascii.equalsIgnoreCase(com.google.common.io.Files.getFileExtension(in.getFileName()), "itar");
      Check.input(!(archive && in.getCreateNew()));

      EditorProcessor processor = null;
      boolean ok = false;
      Files.createDirectories(editorDir);
      try {
        processor = fEditorProcessorFactory.create(
            archive ?
                () -> fArchiveEditorDriverFactory.create(file) :
                () -> fTestDocumentEditorDriverFactory.create(editorDir.resolve(BLOB_STORE_DIRECTORY)),
            archive ?
                (driver, realm) -> ((ArchiveEditorDriver) driver).open(realm, locale) :
                (driver, realm) -> ((TestDocumentEditorDriver) driver).open(realm, file, locale, channel),
            createKryoFreezer(editorDir));

        setType(editorDir, archive ? ARCHIVE_EDITOR_TYPE : TEST_DOCUMENT_EDITOR_TYPE);
        setFileName(editorDir, in.getFileName());
        ++fLastFileNumber;
        Files.writeString(editorDir.resolve(NUMBER_FILE), Long.toString(fLastFileNumber));
        Files.createFile(editorDir.resolve(AVAILABLE_FILE));

        ok = true;
      } finally {
        if (!ok) {
          if (processor != null) {
            processor.close();
          }
          deleteEditorDirectory(editorDir);
        }
      }

      register(editorDir, processor);
      result = Open.Reply.Result.OK;
    } catch (EditorDriver.UnknownFileVersionException e) {
      fLogger.warn("", e);
      result = Open.Reply.Result.UNKNOWN_FILE_VERSION;
    } catch (BadInputException e) {
      fLogger.warn("", e);
      result = Open.Reply.Result.INCORRECT_FILE;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return Open.Reply.newBuilder()
        .setResult(result)
        .build();
  }

  private static void setFileName(Path editorDir, String fileName) {
    try {
      Files.write(editorDir.resolve(NAME_FILE), fileName.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String getFileName(Path editorDir) {
    try {
      return Files.readString(editorDir.resolve(NAME_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void setType(Path editorDir, String type) {
    try {
      Files.write(editorDir.resolve(TYPE_FILE), type.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String getType(Path editorDir) {
    try {
      return Files.readString(editorDir.resolve(TYPE_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Action public void doClose(Close in) {
    if (fOpenFilesByChannel.remove(in.getChannel()) != null) {
      EditorProcessor processor = (EditorProcessor) fProcessorsByChannel.remove(in.getChannel());
      processor.close();
      deleteEditorDirectory(getEditorDirectory(processor));
    }
  }

  private static Path getEditorDirectory(EditorProcessor processor) {
    return Check.notNull((KryoFreezer) processor.freezer()).file().getParent();
  }

  @Action public void doSave(Save in) {
    OpenFile f = Check.inputNotNull(fOpenFilesByChannel.get(in.getChannel()));
    EditorProcessor processor = (EditorProcessor) fProcessorsByChannel.get(in.getChannel());

    Path path;
    try {
      path = Paths.get(in.getFileName());
    } catch (InvalidPathException e) {
      throw new BadInputException(e);
    }

    processor.driver().save(processor, path);

    setFileName(getEditorDirectory(processor), in.getFileName());
    fOpenFilesByChannel.put(in.getChannel(), f.toBuilder()
        .setName(in.getFileName())
        .build());
  }

  @Action public ExportToHtml.Reply doExportToHtml(ExportToHtml in) {
    EditorProcessor processor = (EditorProcessor) Check.inputNotNull(fProcessorsByChannel.get(in.getChannel()));

    Path outputFile;
    try {
      outputFile = Paths.get(in.getOutputFileName());
    } catch (InvalidPathException e) {
      throw new BadInputException(e);
    }

    return ExportToHtml.Reply.newBuilder()
        .setResult(processor.driver().exportToHtml(processor, in.getOptions(), outputFile))
        .build();
  }
}
