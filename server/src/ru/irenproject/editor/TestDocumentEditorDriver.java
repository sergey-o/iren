/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.common.export.Proto.ExportOptions;
import ru.irenproject.common.export.Proto.ExportResult;
import ru.irenproject.export.html.HtmlExporter;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.Realm;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxInputException;
import ru.irenproject.itx.ItxOutput;

import com.ctc.wstx.exc.WstxIOException;
import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Locale;

public final class TestDocumentEditorDriver extends EditorDriver {
  public interface Factory {
    TestDocumentEditorDriver create(Path blobStoreDirectory);
  }

  private final FileBlobStore fFileBlobStore;
  private final ItxInput fItxInput;
  private final ItxOutput fItxOutput;
  private final TestDocumentEditor.Factory fTestDocumentEditorFactory;
  private final HtmlExporter fHtmlExporter;

  @Inject private TestDocumentEditorDriver(
      @Assisted Path blobStoreDirectory,
      ItxInput itxInput,
      ItxOutput itxOutput,
      TestDocumentEditor.Factory testDocumentEditorFactory,
      HtmlExporter htmlExporter) {
    fFileBlobStore = new FileBlobStore(blobStoreDirectory);
    fItxInput = itxInput;
    fItxOutput = itxOutput;
    fTestDocumentEditorFactory = testDocumentEditorFactory;
    fHtmlExporter = htmlExporter;
  }

  @Override public BlobStore blobStore() {
    return fFileBlobStore;
  }

  public TestDocumentEditor open(Realm realm, @Nullable Path testFile, Locale locale, String channel) {
    Test test;
    if (testFile == null) {
      test = new Test(realm);
    } else {
      try {
        test = fItxInput.readTest(testFile, realm);
      } catch (ItxInputException e) {
        throw (e.reason() == ItxInputException.Reason.UNKNOWN_VERSION) ?
            new UnknownFileVersionException(e) : new BadInputException(e);
      }
    }

    return fTestDocumentEditorFactory.create(test, locale, channel);
  }

  @Override public void save(EditorProcessor editorProcessor, Path path) {
    try {
      try {
        TestDocumentEditor editor = (TestDocumentEditor) editorProcessor.documentEditor();
        Test test = editor.test();

        editorProcessor.runAndDispatchEvents(test::generateMissingTags);
        editorProcessor.runAndDispatchEvents(test::reconcileSectionProfiles);

        Path tempPath = Utils.changeFileName(path, Utils::generateTempFileName);
        boolean deleteTempFile = true;
        try {
          try (OutputStream outputStream = Files.newOutputStream(tempPath, StandardOpenOption.CREATE_NEW)) {
            fItxOutput.writeTest(test, outputStream, false, null);
          }

          Files.move(tempPath, path, StandardCopyOption.ATOMIC_MOVE);
          deleteTempFile = false;
        } finally {
          if (deleteTempFile) {
            Files.deleteIfExists(tempPath);
          }
        }
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    } catch (RuntimeException e) {
      throw ((e.getCause() instanceof IOException) || (e.getCause() instanceof WstxIOException)) ?
          new BadInputException(e) : e;
    }
  }

  @Override public boolean autoFreezable() {
    return true;
  }

  @Override public ExportResult exportToHtml(EditorProcessor editorProcessor, ExportOptions options, Path outputFile) {
    TestDocumentEditor editor = (TestDocumentEditor) editorProcessor.documentEditor();
    Test test = editor.test();

    editorProcessor.runAndDispatchEvents(test::reconcileSectionProfiles);

    return fHtmlExporter.export(test, editor.testScreen().activeProfile(), options, outputFile);
  }
}
