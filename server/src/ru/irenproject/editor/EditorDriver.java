/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.common.export.Proto.ExportOptions;
import ru.irenproject.common.export.Proto.ExportResult;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.infra.Event;

import java.nio.file.Path;

public abstract class EditorDriver implements AutoCloseable {
  public static final class UnknownFileVersionException extends RuntimeException {
    public UnknownFileVersionException(Throwable cause) {
      super(cause);
    }
  }

  public static EditorDriver createBasic(BlobStore blobStore) {
    return new BasicEditorDriver(blobStore);
  }

  public abstract BlobStore blobStore();

  public abstract void save(EditorProcessor editorProcessor, Path path);

  public void handleGlobalEvent(Event event) {}

  @Override public void close() {}

  public abstract boolean autoFreezable();

  public abstract ExportResult exportToHtml(EditorProcessor editorProcessor, ExportOptions options, Path outputFile);

  private static final class BasicEditorDriver extends EditorDriver {
    final BlobStore fBlobStore;

    BasicEditorDriver(BlobStore blobStore) {
      fBlobStore = blobStore;
    }

    @Override public BlobStore blobStore() {
      return fBlobStore;
    }

    @Override public void save(EditorProcessor editorProcessor, Path path) {}

    @Override public boolean autoFreezable() {
      return false;
    }

    @Override public ExportResult exportToHtml(EditorProcessor editorProcessor, ExportOptions options,
        Path outputFile) {
      throw new BadInputException();
    }
  }
}
