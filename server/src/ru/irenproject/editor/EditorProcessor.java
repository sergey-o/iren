/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.ChangeContentEvent;
import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.infra.ActionPerformer;
import ru.irenproject.infra.AsyncEventQueue;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobWidget;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Proto.Request;
import ru.irenproject.infra.Proto.SceneDelta;
import ru.irenproject.infra.Proto.Shape;
import ru.irenproject.infra.Proto.XBlob;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.RequestProcessor;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import com.google.common.base.Equivalence;
import com.google.common.collect.MapDifference;
import com.google.common.collect.MapDifference.ValueDifference;
import com.google.common.collect.Maps;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.function.Supplier;

public final class EditorProcessor implements RequestProcessor {
  public interface Factory {
    EditorProcessor create(Supplier<EditorDriver> driverSupplier,
        @Nullable DocumentEditorProvider documentEditorProvider, @Nullable Freezer freezer);
  }

  public interface DocumentEditorProvider {
    DocumentEditor get(EditorDriver editorDriver, Realm realm);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(EditorProcessor.class);

  private static final String ROOT_WIDGET_NAME = "";

  private final EditorDriver fDriver;
  private final ActionPerformer fActionPerformer;
  private final @Nullable Freezer fFreezer;

  private final AsyncEventQueue fEventQueue = new AsyncEventQueue();
  private final Realm fRealm;
  private final DocumentEditor fDocumentEditor;

  private long fLastReplyId;
  private boolean fMayAutoFreeze;

  private HashMap<String, Widget> fWidgets = new HashMap<>();
  private HashMap<String, Message> fShapes = new HashMap<>();
  private HashMap<Widget, LinkedHashSet<Widget>> fDependencies = new HashMap<>();
  private final HashMap<BlobId, BlobWidget> fBlobWidgets = new HashMap<>();

  private final HashSet<Widget> fMarkedForRender = new HashSet<>();

  @Inject private EditorProcessor(
      @Assisted Supplier<EditorDriver> driverSupplier,
      @Assisted @Nullable DocumentEditorProvider documentEditorProvider,
      @Assisted @Nullable Freezer freezer,
      ActionPerformer actionPerformer) {
    fActionPerformer = actionPerformer;
    fFreezer = freezer;

    boolean ok = false;
    try {
      fDriver = driverSupplier.get();
      fRealm = new Realm(fEventQueue, new GlobalEventHandler(), fDriver.blobStore());

      if (documentEditorProvider == null) {
        fDocumentEditor = Check.notNull(fFreezer).unfreeze(fRealm);
      } else {
        fDocumentEditor = documentEditorProvider.get(fDriver, fRealm);
        fEventQueue.dispatch();
        fMarkedForRender.clear();
        freeze();
      }

      ok = true;
    } finally {
      if (!ok) {
        close();
      }
    }
  }

  @Override public void close() {
    if (fDriver != null) {
      fDriver.close();
    }
  }

  private final class GlobalEventHandler implements Realm.GlobalEventSink {
    @Override public void postGlobal(Resident source, Event event) {
      if (event instanceof Widget.MarkForRenderEvent) {
        fMarkedForRender.add((Widget) source);
      } else if (event instanceof Widget.ShowScreenEvent) {
        fDocumentEditor.pushScreen(((Widget.ShowScreenEvent) event).screen());
      } else if (event instanceof Widget.DismissScreenEvent) {
        fDocumentEditor.popScreen();
      } else if (event instanceof ChangeContentEvent) {
        if (fDriver.autoFreezable()) {
          fMayAutoFreeze = true;
        }
        if (fDocumentEditor != null) {
          fDocumentEditor.setModified(true);
        }
      } else {
        fDriver.handleGlobalEvent(event);
      }
    }
  }

  public DocumentEditor documentEditor() {
    return fDocumentEditor;
  }

  public EditorDriver driver() {
    return fDriver;
  }

  public @Nullable Freezer freezer() {
    return fFreezer;
  }

  @Override public Reply process(Request request) {
    ++fLastReplyId;

    SceneDelta sceneDelta;
    boolean badInput;
    Message output;

    try {
      Widget target = request.getTargetName().equals(ROOT_WIDGET_NAME) ?
          fDocumentEditor : Check.inputNotNull(fWidgets.get(request.getTargetName()));
      output = fActionPerformer.perform(target, request.getActionName(), request.getInput());
      badInput = false;
    } catch (BadInputException e) {
      output = null;
      badInput = true;
      fLogger.warn("", e);
    } finally {
      fEventQueue.dispatch();
      sceneDelta = render();
    }

    Check.that(fEventQueue.isEmpty());

    Reply.Builder reply = Reply.newBuilder()
        .setChannel(request.getChannel())
        .setId(fLastReplyId)
        .setOk(!badInput)
        .setSceneDelta(sceneDelta);
    if (output != null) {
      reply.setOutput(output.toByteString());
    }
    return reply.build();
  }

  private SceneDelta render() {
    HashMap<String, Widget> newWidgets = new HashMap<>();
    HashMap<String, Message> newShapes = new HashMap<>();
    HashMap<Widget, LinkedHashSet<Widget>> newDependencies = new HashMap<>();

    Queue<Widget> pending = new ArrayDeque<>();
    pending.add(fDocumentEditor);

    while (!pending.isEmpty()) {
      Widget widget = pending.remove();
      boolean alreadyRendered = newDependencies.containsKey(widget);
      if (!alreadyRendered) {
        String name = getWidgetName(widget);
        Message shape;
        LinkedHashSet<Widget> references;

        Message oldShape = fMarkedForRender.contains(widget) ? null : fShapes.get(name);
        if (oldShape == null) {
          DefaultRenderContext renderContext = new DefaultRenderContext(this::getWidgetName, fBlobWidgets, fRealm);
          shape = widget.render(renderContext);
          references = renderContext.fReferences;
        } else {
          shape = oldShape;
          references = Check.notNull(fDependencies.get(widget));
        }

        newWidgets.put(name, widget);
        newShapes.put(name, shape);
        newDependencies.put(widget, references);

        pending.addAll(references);
      }
    }

    SceneDelta res = computeSceneDelta(fShapes, newShapes);

    fWidgets = newWidgets;
    fShapes = newShapes;
    fDependencies = newDependencies;
    fMarkedForRender.clear();
    fBlobWidgets.values().retainAll(fDependencies.keySet());

    return res;
  }

  private static final class DefaultRenderContext implements RenderContext {
    final Function<Widget, String> fNameProvider;
    final Map<BlobId, BlobWidget> fBlobWidgets;
    final Realm fRealm;
    final LinkedHashSet<Widget> fReferences = new LinkedHashSet<>();

    DefaultRenderContext(Function<Widget, String> nameProvider, Map<BlobId, BlobWidget> blobWidgets, Realm realm) {
      fNameProvider = nameProvider;
      fBlobWidgets = blobWidgets;
      fRealm = realm;
    }

    @Override public String link(Widget widget) {
      fReferences.add(widget);
      return fNameProvider.apply(widget);
    }

    @Override public String linkBlob(BlobId blobId) {
      return link(fBlobWidgets.computeIfAbsent(blobId, _blobId -> new BlobWidget(fRealm, _blobId)));
    }
  }

  private String getWidgetName(Widget widget) {
    return (widget == fDocumentEditor) ? ROOT_WIDGET_NAME : Long.toString(widget.id());
  }

  private SceneDelta computeSceneDelta(Map<String, Message> old, Map<String, Message> current) {
    MapDifference<String, Message> diff = Maps.difference(old, current, Equivalence.identity());

    TreeMap<String, Message> changed = new TreeMap<>();
    changed.putAll(diff.entriesOnlyOnRight());
    for (Map.Entry<String, ValueDifference<Message>> e : diff.entriesDiffering().entrySet()) {
      changed.put(e.getKey(), e.getValue().rightValue());
    }

    TreeSet<String> deleted = new TreeSet<>();
    deleted.addAll(diff.entriesOnlyOnLeft().keySet());

    SceneDelta.Builder b = SceneDelta.newBuilder();

    for (Map.Entry<String, Message> e : changed.entrySet()) {
      Message m = e.getValue();

      if (m instanceof XBlob) {
        m = replaceBlobIdWithContent((XBlob) m);
      }

      b.addChanged(Shape.newBuilder()
          .setName(e.getKey())
          .setType(m.getDescriptorForType().getName())
          .setData(m.toByteString()));
    }

    return b
        .addAllDeleted(deleted)
        .build();
  }

  private XBlob replaceBlobIdWithContent(XBlob m) {
    return XBlob.newBuilder()
        .setBlob(Utils.readByteSourceToByteString(fRealm.blobStore().get(new BlobId(m.getBlob()))))
        .build();
  }

  public void freeze() {
    if (fFreezer != null) {
      fFreezer.freeze(fDocumentEditor);
    }
    fMayAutoFreeze = false;
  }

  public boolean mayAutoFreeze() {
    return fMayAutoFreeze;
  }

  public void runAndDispatchEvents(Runnable r) {
    try {
      r.run();
    } finally {
      fEventQueue.dispatch();
    }
  }
}
