/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Test;
import ru.irenproject.editor.Proto.XTestDocumentEditor;
import ru.irenproject.editor.Proto.XTestDocumentEditor.MarkAsUnmodified;
import ru.irenproject.infra.RenderContext;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.Locale;

public final class TestDocumentEditor extends DocumentEditor {
  public interface Factory {
    TestDocumentEditor create(Test test, Locale locale, String channel);
  }

  private final Test fTest;
  private final TestScreen fTestScreen;

  @Inject private TestDocumentEditor(
      @Assisted Test test,
      @Assisted Locale locale,
      @Assisted String channel,
      TestScreen.Factory testScreenFactory) {
    super(test.realm());
    fTest = test;
    fTestScreen = testScreenFactory.create(fTest, locale, channel);
    pushScreen(fTestScreen);
  }

  public Test test() {
    return fTest;
  }

  @Override public Message render(RenderContext context) {
    return XTestDocumentEditor.newBuilder()
        .setScreen(context.link(topScreen()))
        .setModified(modified())
        .build();
  }

  @Action public void doMarkAsUnmodified(MarkAsUnmodified in) {
    setModified(false);
  }

  public TestScreen testScreen() {
    return fTestScreen;
  }
}
