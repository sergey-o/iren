/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.QuestionList;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.editor.Proto.XTestScreen;
import ru.irenproject.editor.Proto.XTestScreen.SetViewMode;
import ru.irenproject.editor.Proto.XTestScreen.ShowProfiles;
import ru.irenproject.editor.profile.ProfileScreen;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.Profile;

import com.google.common.base.MoreObjects;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.LinkedHashMap;
import java.util.Locale;

public final class TestScreen extends Widget {
  public interface Factory {
    TestScreen create(Test test, Locale locale, String clipboardOrigin);
  }

  private final Test fTest;
  private final String fClipboardOrigin;
  private final LinkedHashMap<QuestionList, QuestionListEditor> fQuestionListEditors = new LinkedHashMap<>();
  private final LinkedHashMap<Question, QuestionEditor<?>> fQuestionEditors = new LinkedHashMap<>();
  private final SectionTreeEditor fSectionTreeEditor;
  private final ProfileScreen fProfileScreen;
  private final Profile fDefaultProfile;
  private boolean fViewMode;
  private final QuestionViewer fQuestionViewer;

  @Inject private QuestionEditorProvider fQuestionEditorProvider;
  @Inject private QuestionListEditor.Factory fQuestionListEditorFactory;

  @Inject private TestScreen(
      @Assisted Test test,
      @Assisted Locale locale,
      @Assisted String clipboardOrigin,
      SectionTreeEditor.Factory sectionTreeEditorFactory,
      ProfileScreen.Factory profileScreenFactory,
      QuestionViewer.Factory questionViewerFactory) {
    super(test.realm());
    fTest = test;
    fTest.root().addListener(this);
    fClipboardOrigin = clipboardOrigin;
    fSectionTreeEditor = sectionTreeEditorFactory.create(fTest.root());
    fSectionTreeEditor.addListener(this);
    fProfileScreen = profileScreenFactory.create(fTest.profileList(), fTest.root(), ProfileScreen.Mode.FULL_EDITOR);
    fDefaultProfile = Profile.createDefault(realm());
    fQuestionViewer = questionViewerFactory.create(realm(), locale);
  }

  @Override public Message render(RenderContext context) {
    QuestionListEditor questionListEditor = fQuestionListEditors.computeIfAbsent(
        fSectionTreeEditor.selectedSection().questionList(),
        this::createQuestionListEditor);

    XTestScreen.Builder b = XTestScreen.newBuilder()
        .setQuestionListEditor(context.link(questionListEditor))
        .setSectionTreeEditor(context.link(fSectionTreeEditor))
        .setViewMode(fViewMode);

    QuestionItem activeItem = questionListEditor.activeItem();
    if (activeItem != null) {
      b.setQuestionEditor(context.link(fQuestionEditors.computeIfAbsent(activeItem.question(),
          fQuestionEditorProvider::create)));
      if (fViewMode) {
        fQuestionViewer.setSource(activeItem, fSectionTreeEditor.selectedSection(), activeProfile());
        b.setQuestionViewer(context.link(fQuestionViewer));
      }
    }

    return b.build();
  }

  public Profile activeProfile() {
    return MoreObjects.firstNonNull(fProfileScreen.selectedProfile(), fDefaultProfile);
  }

  private QuestionListEditor createQuestionListEditor(QuestionList questionList) {
    questionList.addListener(this);

    QuestionListEditor res = fQuestionListEditorFactory.create(questionList, fTest.root(), fClipboardOrigin);
    res.addListener(this);
    return res;
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if ((e instanceof SectionTreeEditor.SelectEvent) || (e instanceof QuestionListEditor.SelectEvent)) {
      markForRender();
    } else if (e instanceof Section.RemoveSectionEvent) {
      QuestionList questionList = ((Section.RemoveSectionEvent) e).section().questionList();
      for (QuestionItem item : questionList.items()) {
        fQuestionEditors.remove(item.question());
      }
      fQuestionListEditors.remove(questionList);
    } else if (e instanceof QuestionList.RemoveItemEvent) {
      fQuestionEditors.remove(((QuestionList.RemoveItemEvent) e).item().question());
    }
  }

  @Action public void doShowProfiles(ShowProfiles in) {
    showScreen(fProfileScreen);
  }

  @Action public void doSetViewMode(SetViewMode in) {
    fViewMode = in.getViewMode();
    markForRender();
  }
}
