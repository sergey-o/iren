/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.editor.Proto.XQuestionViewerDashboard;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;

import com.google.protobuf.Message;

import javax.annotation.Nullable;
import java.math.BigDecimal;

public final class QuestionViewerDashboard extends Widget {
  private @Nullable BigDecimal fResult;

  public QuestionViewerDashboard(Realm realm) {
    super(realm);
  }

  @Override public Message render(RenderContext context) {
    XQuestionViewerDashboard.Builder b = XQuestionViewerDashboard.newBuilder();
    if (fResult != null) {
      b.setResultPercent(fResult.scaleByPowerOfTen(2).intValue());
    }
    return b.build();
  }

  public void setResult(@Nullable BigDecimal value) {
    fResult = value;
    markForRender();
  }
}
