/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.modifier;

import ru.irenproject.Modifier;

import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton public final class ModifierEditorProvider {
  private final ImmutableMap<Class<? extends Modifier>, ModifierEditor.Factory> fMap;

  @Inject private ModifierEditorProvider(Map<Class<? extends Modifier>, ModifierEditor.Factory> map) {
    fMap = ImmutableMap.copyOf(map);
  }

  public @Nullable ModifierEditor<?> create(Modifier modifier, @Nullable String questionType) {
    ModifierEditor.Factory factory = fMap.get(modifier.getClass());
    return (factory == null) ? null : factory.create(modifier, questionType);
  }
}
