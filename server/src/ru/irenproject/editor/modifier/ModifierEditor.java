/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.modifier;

import ru.irenproject.Modifier;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;

import javax.annotation.Nullable;

public abstract class ModifierEditor<T extends Modifier> extends Widget {
  public interface Factory {
    @SuppressWarnings({"EmptyMethod", "UnusedParameters"})
    ModifierEditor<?> create(Modifier modifier, @Nullable String questionType);
  }

  private final T fModifier;
  private final @Nullable String fQuestionType;

  protected ModifierEditor(T modifier, @Nullable String questionType) {
    super(modifier.realm());
    fModifier = modifier;
    fQuestionType = questionType;
    fModifier.addListener(this);
  }

  public final T modifier() {
    return fModifier;
  }

  public final @Nullable String questionType() {
    return fQuestionType;
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Modifier.ChangeEvent) {
      markForRender();
    }
  }
}
