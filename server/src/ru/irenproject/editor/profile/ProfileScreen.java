/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.profile;

import ru.irenproject.Check;
import ru.irenproject.Section;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.profile.Proto.XProfileScreen;
import ru.irenproject.editor.profile.Proto.XProfileScreen.Close;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.LinkedHashMap;

public final class ProfileScreen extends Widget {
  public interface Factory {
    ProfileScreen create(ProfileList profileList, Section rootSection, Mode mode);
  }

  public enum Mode { FULL_EDITOR, PROFILE_SELECTOR }

  private final ProfileList fProfileList;
  private final Section fRootSection;
  private final Mode fMode;
  private final ProfileListEditor fProfileListEditor;
  private final LinkedHashMap<Profile, ProfileEditor> fProfileEditors = new LinkedHashMap<>();

  @Inject private ProfileEditor.Factory fProfileEditorFactory;

  @Inject private ProfileScreen(
      @Assisted ProfileList profileList,
      @Assisted Section rootSection,
      @Assisted Mode mode) {
    super(profileList.realm());
    fProfileList = profileList;
    fProfileList.addListener(this);
    fRootSection = rootSection;
    fMode = mode;

    fProfileListEditor = new ProfileListEditor(fProfileList, fMode == Mode.FULL_EDITOR);
    fProfileListEditor.addListener(this);
  }

  @Override public Message render(RenderContext context) {
    XProfileScreen.Builder b = XProfileScreen.newBuilder()
        .setProfileListEditor(context.link(fProfileListEditor))
        .setAllowClose(fMode == Mode.FULL_EDITOR);

    Profile selected = fProfileListEditor.selected();
    if (selected != null) {
      b.setProfileEditor(context.link(fProfileEditors.computeIfAbsent(selected,
          profile -> fProfileEditorFactory.create(profile, fRootSection, fMode == Mode.FULL_EDITOR))));
    }

    return b.build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof ProfileListEditor.SelectEvent) {
      markForRender();
    } else if (e instanceof ProfileList.RemoveEvent) {
      fProfileEditors.remove(((ProfileList.RemoveEvent) e).profile());
    }
  }

  @Action public void doClose(Close in) {
    Check.input(fMode == Mode.FULL_EDITOR);
    dismissScreen();
  }

  public @Nullable Profile selectedProfile() {
    return fProfileListEditor.selected();
  }
}
