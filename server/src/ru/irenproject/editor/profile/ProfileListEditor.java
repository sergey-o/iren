/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.profile;

import ru.irenproject.Check;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.profile.Proto.XProfileListEditor;
import ru.irenproject.editor.profile.Proto.XProfileListEditor.Add;
import ru.irenproject.editor.profile.Proto.XProfileListEditor.Delete;
import ru.irenproject.editor.profile.Proto.XProfileListEditor.Move;
import ru.irenproject.editor.profile.Proto.XProfileListEditor.Select;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;

import com.google.protobuf.Message;

import javax.annotation.Nullable;
import java.util.stream.Collectors;

public final class ProfileListEditor extends Widget {
  public enum SelectEvent implements Event { INSTANCE }

  private final ProfileList fProfileList;
  private final boolean fAllowEdit;
  private @Nullable Profile fSelected;

  public ProfileListEditor(ProfileList profileList, boolean allowEdit) {
    super(profileList.realm());
    fProfileList = profileList;
    fAllowEdit = allowEdit;
    fProfileList.addListener(this);

    for (Profile p : fProfileList.profiles()) {
      p.addListener(this);
    }

    if (!fProfileList.isEmpty()) {
      fSelected = fProfileList.profiles().get(0);
    }
  }

  @Override public Message render(RenderContext context) {
    XProfileListEditor.Builder b = XProfileListEditor.newBuilder()
        .addAllTitle(fProfileList.profiles().stream().map(Profile::title).collect(Collectors.toList()))
        .setAllowEdit(fAllowEdit);
    if (fSelected != null) {
      b.setSelectedIndex(fProfileList.profiles().indexOf(fSelected));
    }
    return b.build();
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof ProfileList.AddEvent) {
      ((ProfileList.AddEvent) e).profile().addListener(this);
    } else if (e instanceof ProfileList.RemoveEvent) {
      if (fSelected == ((ProfileList.RemoveEvent) e).profile()) {
        fSelected = null;
        post(SelectEvent.INSTANCE);
      }
    } else if ((e instanceof ProfileList.ChangeEvent) || (e instanceof Profile.ChangeTitleEvent)) {
      markForRender();
    }
  }

  @Action public void doSelect(Select in) {
    Check.input(in.getIndex() >= 0);
    Check.input(in.getIndex() < fProfileList.profiles().size());

    fSelected = fProfileList.profiles().get(in.getIndex());
    markForRender();
    post(SelectEvent.INSTANCE);
  }

  public @Nullable Profile selected() {
    return fSelected;
  }

  @Action public void doAdd(Add in) {
    Check.input(fAllowEdit);

    Profile p = Profile.createDefault(realm());
    fProfileList.add(p);
    fSelected = p;
    post(SelectEvent.INSTANCE);
  }

  @Action public void doDelete(Delete in) {
    Check.input(fAllowEdit);
    Check.inputNotNull(fSelected);

    int selectedIndex = fProfileList.profiles().indexOf(fSelected);
    fProfileList.delete(selectedIndex);

    selectedIndex = Integer.min(selectedIndex, fProfileList.profiles().size() - 1);
    if (selectedIndex >= 0) {
      fSelected = fProfileList.profiles().get(selectedIndex);
      post(SelectEvent.INSTANCE);
    }
  }

  @Action(badInput = TestInputException.class)
  public void doMove(Move in) {
    Check.input(fAllowEdit);
    Check.inputNotNull(fSelected);
    fProfileList.move(fProfileList.profiles().indexOf(fSelected), in.getForward());
  }
}
