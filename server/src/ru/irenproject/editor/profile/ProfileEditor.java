/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.profile;

import ru.irenproject.Check;
import ru.irenproject.Section;
import ru.irenproject.TestInputException;
import ru.irenproject.TestUtils;
import ru.irenproject.common.profile.Proto.Mark;
import ru.irenproject.common.profile.Proto.MarkScaleData;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.ModifierListEditor;
import ru.irenproject.editor.profile.Proto.SectionNode;
import ru.irenproject.editor.profile.Proto.XProfileEditor;
import ru.irenproject.editor.profile.Proto.XProfileEditor.AddMark;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetOptions;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetScore;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetSectionQuestionCount;
import ru.irenproject.editor.profile.Proto.XProfileEditor.SetTitle;
import ru.irenproject.editor.profile.Proto.XProfileEditor.UpdateMark;
import ru.irenproject.editor.profile.Proto.XSectionTree;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.infra.Widget;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.QuestionCount;
import ru.irenproject.profile.SectionProfile;

import com.google.common.collect.Maps;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public final class ProfileEditor extends Widget {
  public interface Factory {
    ProfileEditor create(Profile profile, Section rootSection, boolean allowEditTitle);
  }

  private final Profile fProfile;
  private final Section fRootSection;
  private final boolean fAllowEditTitle;
  private final ModifierListEditor fModifierListEditor;
  private @Nullable SectionTree fSectionTree;

  @Inject private ProfileEditor(
      @Assisted Profile profile,
      @Assisted Section rootSection,
      @Assisted boolean allowEditTitle,
      ModifierListEditor.Factory modifierListEditorFactory) {
    super(profile.realm());
    fProfile = profile;
    fProfile.addListener(this);
    fRootSection = rootSection;
    fAllowEditTitle = allowEditTitle;
    fModifierListEditor = modifierListEditorFactory.create(fProfile.modifierList(), null, null);
  }

  @Override public Message render(RenderContext context) {
    XProfileEditor.Builder b = XProfileEditor.newBuilder()
        .setTitle(fProfile.title())
        .setAllowEditTitle(fAllowEditTitle)
        .setOptions(fProfile.options())
        .setMarkScale(fProfile.markScale().data())
        .setMaxMarks(MarkScale.MAX_MARKS)
        .setModifierListEditor(context.link(fModifierListEditor));
    if (fProfile.availableScore() != null) {
      b.setScore(fProfile.availableScore());
    }

    SectionProfile sectionProfile = fProfile.sectionProfile();
    if (sectionProfile != null) {
      SectionNode root = renderSectionTree(fRootSection);
      if ((fSectionTree == null) || !fSectionTree.fShape.getRoot().equals(root)) {
        fSectionTree = new SectionTree(realm(), root);
      }
      b.setSectionTree(context.link(fSectionTree));
      renderQuestionCounts(fRootSection, sectionProfile.profilesBySectionId(), b::addSectionQuestionCount);
    }

    return b.build();
  }

  private static final class SectionTree extends Widget {
    final XSectionTree fShape;

    SectionTree(Realm realm, SectionNode root) {
      super(realm);
      fShape = XSectionTree.newBuilder()
          .setRoot(root)
          .build();
    }

    @Override public Message render(RenderContext context) {
      return fShape;
    }
  }

  private static SectionNode renderSectionTree(Section section) {
    return SectionNode.newBuilder()
        .setSectionId(section.id())
        .setName(section.name())
        .addAllSection(section.sections().stream()
            .map(ProfileEditor::renderSectionTree)
            .collect(Collectors.toList()))
        .build();
  }

  private static void renderQuestionCounts(Section section, Map<Long, SectionProfile> profilesBySectionId,
      Consumer<String> out) {
    out.accept(Optional.ofNullable(profilesBySectionId.get(section.id()))
        .map(SectionProfile::questionCount)
        .map(QuestionCount::text)
        .orElse(""));
    for (Section child : section.sections()) {
      renderQuestionCounts(child, profilesBySectionId, out);
    }
  }

  @Override protected void handleEvent(Event e, Resident source) {
    if (e instanceof Profile.ChangeEvent) {
      markForRender();
    }
  }

  @Action public void doSetTitle(SetTitle in) {
    Check.input(fAllowEditTitle);
    fProfile.setTitle(in.getTitle());
  }

  @Action(badInput = TestInputException.class)
  public void doSetOptions(SetOptions in) {
    fProfile.setOptions(in.getOptions());
    if (in.getUseSectionProfile()) {
      if (fProfile.sectionProfile() == null) {
        fProfile.setSectionProfile(SectionProfile.createForTree(fRootSection, Collections.emptyMap()));
      }
    } else {
      fProfile.setSectionProfile(null);
    }
  }

  @Action(badInput = TestInputException.class)
  public void doSetScore(SetScore in) {
    fProfile.setAvailableScore(in.hasScore() ? in.getScore() : null);
  }

  @Action(badInput = TestInputException.class)
  public void doAddMark(AddMark in) {
    fProfile.setMarkScale(addMark(fProfile.markScale(), in.getMark()));
  }

  private static MarkScale addMark(MarkScale markScale, Mark mark) {
    Integer index = markScale.getMarkIndexForResult(TestUtils.parseNormalizedDecimal(mark.getLowerBound()));
    return new MarkScale(markScale.data().toBuilder()
        .addMark((index == null) ? 0 : index + 1, mark)
        .buildPartial());
  }

  @Action(badInput = TestInputException.class)
  public void doUpdateMark(UpdateMark in) {
    int index = in.getIndex();
    Check.input(index >= 0);
    Check.input(index < fProfile.markScale().markCount());

    MarkScaleData.Builder data = fProfile.markScale().data().toBuilder();
    MarkScale updated;

    if (in.hasMark()) {
      updated = in.getMark().getLowerBound().equals(data.getMark(index).getLowerBound()) ?
          new MarkScale(data.setMark(index, in.getMark()).buildPartial()) :
          addMark(new MarkScale(data.removeMark(index).buildPartial()), in.getMark());
    } else {
      data.removeMark(index);
      if (data.getMarkCount() > 0) {
        data.getMarkBuilder(0).setLowerBound(MarkScale.ZERO_LOWER_BOUND);
      }
      updated = new MarkScale(data.buildPartial());
    }

    fProfile.setMarkScale(updated);
  }

  @Action(badInput = TestInputException.class)
  public void doSetSectionQuestionCount(SetSectionQuestionCount in) {
    //noinspection ConstantConditions
    HashMap<Long, QuestionCount> questionCountsBySectionId = new HashMap<>(Maps.transformValues(
        Check.inputNotNull(fProfile.sectionProfile()).profilesBySectionId(), SectionProfile::questionCount));
    questionCountsBySectionId.put(in.getSectionId(),
        in.getSectionQuestionCount().isEmpty() ? null : QuestionCount.fromText(in.getSectionQuestionCount()));
    fProfile.setSectionProfile(SectionProfile.createForTree(fRootSection, questionCountsBySectionId));
  }
}
