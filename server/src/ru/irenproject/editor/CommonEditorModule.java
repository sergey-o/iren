/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.editor.modifier.ModifierListEditor;
import ru.irenproject.editor.profile.ProfileEditor;
import ru.irenproject.editor.profile.ProfileScreen;
import ru.irenproject.editor.scriptModifier.ScriptModifierEditorModule;
import ru.irenproject.editor.setEvaluationModelModifier.SetEvaluationModelModifierEditorModule;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public final class CommonEditorModule extends AbstractModule {
  @Override protected void configure() {
    install(new FactoryModuleBuilder().build(EditorProcessor.Factory.class));
    install(new FactoryModuleBuilder().build(ProfileScreen.Factory.class));
    install(new FactoryModuleBuilder().build(ProfileEditor.Factory.class));
    install(new FactoryModuleBuilder().build(ModifierListEditor.Factory.class));

    install(new SetEvaluationModelModifierEditorModule());
    install(new ScriptModifierEditorModule());
  }
}
