/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.editor.Proto.XQuestionViewer;
import ru.irenproject.editor.Proto.XQuestionViewer.Issue;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionIssueException;
import ru.irenproject.live.QuestionIssuer;
import ru.irenproject.profile.Profile;

import com.google.common.base.Strings;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.Locale;

public final class QuestionViewer extends Widget {
  public interface Factory {
    QuestionViewer create(Realm realm, Locale locale);
  }

  private final Locale fLocale;
  private final QuestionViewerDashboard fDashboard;

  private QuestionItem fQuestionItem;
  private Section fSection;
  private Profile fProfile;

  @Inject private QuestionIssuer fQuestionIssuer;
  @Inject private QuestionViewerContent.Factory fQuestionViewerContentFactory;

  @Inject private QuestionViewer(@Assisted Realm realm, @Assisted Locale locale) {
    super(realm);
    fLocale = locale;
    fDashboard = new QuestionViewerDashboard(realm());
  }

  @Override public Message render(RenderContext context) {
    XQuestionViewer.Builder b = XQuestionViewer.newBuilder()
        .setDashboard(context.link(fDashboard));

    _LiveQuestion liveQuestion = null;
    try {
      liveQuestion = fQuestionIssuer.issue(fQuestionItem, fSection, fProfile, fLocale);
    } catch (QuestionIssueException e) {
      b.setErrorMessage(Strings.nullToEmpty(e.getMessage()));
      fDashboard.setResult(null);
    }

    if (liveQuestion != null) {
      QuestionViewerContent content = fQuestionViewerContentFactory.create(liveQuestion, fQuestionItem.question(),
          fDashboard);
      content.evaluateEmptyResponse();
      b.setContent(context.link(content));
    }

    return b.build();
  }

  public void setSource(QuestionItem questionItem, Section section, Profile profile) {
    if ((fQuestionItem != questionItem) || (fSection != section) || (fProfile != profile)) {
      fQuestionItem = questionItem;
      fSection = section;
      fProfile = profile;
      markForRender();
    }
  }

  @Action public void doIssue(Issue in) {
    markForRender();
  }
}
