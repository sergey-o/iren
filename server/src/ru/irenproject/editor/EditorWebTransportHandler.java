/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.ClosingChannelException;
import ru.irenproject.Utils;
import ru.irenproject.infra.Proto.Reply;

import com.google.common.base.Ascii;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker13;
import io.netty.util.ReferenceCountUtil;

public final class EditorWebTransportHandler extends ChannelDuplexHandler {
  public static final int MAX_FRAME_SIZE = 100_000_000;

  private boolean fWebSocketUpgradeReceived;
  private boolean fCloseFrameSent;

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    boolean release = true;
    try {
      if (msg instanceof FullHttpRequest) {
        handleHttpRequest((FullHttpRequest) msg, ctx);
      } else if (msg instanceof BinaryWebSocketFrame) {
        handleBinaryFrame((BinaryWebSocketFrame) msg, ctx);
      } else if (msg instanceof TextWebSocketFrame) {
        throw new BadInputException();
      } else if (msg instanceof CloseWebSocketFrame) {
        handleCloseFrame(ctx);
      } else if (msg instanceof PingWebSocketFrame) {
        handlePingFrame((PingWebSocketFrame) msg, ctx);
      } else if (msg instanceof PongWebSocketFrame) {
        // do nothing
      } else {
        release = false;
        super.channelRead(ctx, msg);
      }
    } finally {
      if (release) {
        ReferenceCountUtil.release(msg);
      }
    }
  }

  private void handleHttpRequest(FullHttpRequest request, ChannelHandlerContext ctx) {
    Check.input(!fWebSocketUpgradeReceived);

    Check.input(request.decoderResult().isSuccess());
    Check.input(request.method().equals(HttpMethod.GET));
    Check.input(request.protocolVersion().equals(HttpVersion.HTTP_1_1));

    String upgrade = Check.inputNotNull(request.headers().get(HttpHeaderNames.UPGRADE));
    Check.input(Ascii.equalsIgnoreCase(upgrade, "websocket"));

    fWebSocketUpgradeReceived = true;
    new WebSocketServerHandshaker13(null, "", false, MAX_FRAME_SIZE)
        .handshake(ctx.channel(), request)
        .addListener(Utils.CLOSE_ON_ERROR);
  }

  private void handleBinaryFrame(BinaryWebSocketFrame frame, ChannelHandlerContext ctx) {
    byte[] message = new byte[frame.content().readableBytes()];
    frame.content().readBytes(message);
    ctx.fireChannelRead(message);
  }

  private void handleCloseFrame(ChannelHandlerContext ctx) {
    ctx.writeAndFlush(new CloseWebSocketFrame()).addListener(ChannelFutureListener.CLOSE);
    fCloseFrameSent = true;
  }

  private void handlePingFrame(PingWebSocketFrame frame, ChannelHandlerContext ctx) {
    PongWebSocketFrame reply = new PongWebSocketFrame(frame.content().retain());
    ctx.writeAndFlush(reply).addListener(Utils.CLOSE_ON_ERROR);
  }

  @Override public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
    if (fCloseFrameSent) {
      ReferenceCountUtil.release(msg);
      promise.setFailure(new ClosingChannelException());
    } else if (msg instanceof Reply) {
      ctx.write(new BinaryWebSocketFrame(Utils.messageToDirectByteBuf((Reply) msg, ctx.alloc())), promise);
    } else {
      super.write(ctx, msg, promise);
    }
  }
}
