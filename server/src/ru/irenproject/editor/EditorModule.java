/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.TestModule;
import ru.irenproject.archiveEditor.ArchiveEditor;
import ru.irenproject.archiveEditor.ArchiveEditorDriver;
import ru.irenproject.archiveEditor.ArchiveSessionScreen;
import ru.irenproject.archiveEditor.ArchiveWorkScreen;
import ru.irenproject.archiveEditor.ArchiveWorkSelectorScreen;
import ru.irenproject.editor.classifyQuestion.ClassifyQuestionEditorModule;
import ru.irenproject.editor.inputQuestion.InputQuestionEditorModule;
import ru.irenproject.editor.matchQuestion.MatchQuestionEditorModule;
import ru.irenproject.editor.modifier.ModifierScreen;
import ru.irenproject.editor.orderQuestion.OrderQuestionEditorModule;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.editor.selectQuestion.SelectQuestionEditorModule;
import ru.irenproject.formula.FormulaRenderer;
import ru.irenproject.live.LiveModule;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;

public final class EditorModule extends AbstractModule {
  @Override protected void configure() {
    install(new TestModule());
    install(new LiveModule());
    install(new CommonEditorModule());

    install(new FactoryModuleBuilder().build(KryoFreezer.Factory.class));
    install(new FactoryModuleBuilder().build(FileManager.Factory.class));
    install(new FactoryModuleBuilder().build(EditorMessageHandler.Factory.class));
    install(new FactoryModuleBuilder().build(TestDocumentEditor.Factory.class));
    install(new FactoryModuleBuilder().build(TestDocumentEditorDriver.Factory.class));
    install(new FactoryModuleBuilder().build(TestScreen.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionListEditor.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionItemEditor.Factory.class));
    install(new FactoryModuleBuilder().build(SectionTreeEditor.Factory.class));
    install(new FactoryModuleBuilder().build(SectionEditor.Factory.class));
    install(new FactoryModuleBuilder().build(ModifierScreen.Factory.class));
    install(new FactoryModuleBuilder().build(PadEditor.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionViewer.Factory.class));
    install(new FactoryModuleBuilder().build(QuestionViewerContent.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveEditor.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveEditorDriver.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveWorkSelectorScreen.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveWorkScreen.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveSessionScreen.Factory.class));

    install(new SelectQuestionEditorModule());
    install(new InputQuestionEditorModule());
    install(new MatchQuestionEditorModule());
    install(new OrderQuestionEditorModule());
    install(new ClassifyQuestionEditorModule());

    bind(Integer.class).annotatedWith(FormulaRenderer.MaxIdleContexts.class).toInstance(1);
  }
}
