/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.setEvaluationModelModifier;

import ru.irenproject.Modifier;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.ModifierEditor;
import ru.irenproject.editor.setEvaluationModelModifier.Proto.XSetEvaluationModelModifierEditor;
import ru.irenproject.editor.setEvaluationModelModifier.Proto.XSetEvaluationModelModifierEditor.SetModelType;
import ru.irenproject.editor.setEvaluationModelModifier.Proto.XSetEvaluationModelModifierEditor.SetQuestionTypes;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifier;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class SetEvaluationModelModifierEditor extends ModifierEditor<SetEvaluationModelModifier> {
  public interface Factory extends ModifierEditor.Factory {
    @Override SetEvaluationModelModifierEditor create(Modifier modifier, @Nullable String questionType);
  }

  @Inject private SetEvaluationModelModifierEditor(
      @Assisted Modifier modifier,
      @Assisted @Nullable String questionType) {
    super((SetEvaluationModelModifier) modifier, questionType);
  }

  @Override public Message render(RenderContext context) {
    return XSetEvaluationModelModifierEditor.newBuilder()
        .setModelType(modifier().modelType())
        .addAllQuestionType(modifier().questionTypes())
        .setSingleQuestionModifier(questionType() != null)
        .build();
  }

  @Action public void doSetModelType(SetModelType in) {
    modifier().setModelType(in.getModelType());
  }

  @Action public void doSetQuestionTypes(SetQuestionTypes in) {
    modifier().setQuestionTypes(in.getQuestionTypeList());
  }
}
