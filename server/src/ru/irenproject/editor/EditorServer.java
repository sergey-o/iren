/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.ProgramInfo;
import ru.irenproject.Utils;
import ru.irenproject.formula.FormulaRendererPreloaderModule;
import ru.irenproject.work.ExceptionHandler;
import ru.irenproject.work.MessageLogger;
import ru.irenproject.work.NetworkExceptionHandler;

import com.google.common.io.BaseEncoding;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import io.netty.util.concurrent.DefaultThreadFactory;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.inject.Inject;
import java.net.InetAddress;
import java.util.Locale;

public final class EditorServer {
  private static final int DEFAULT_PORT = 9982;

  private static final Logger fLogger = LoggerFactory.getLogger(EditorServer.class);

  public static void main(String[] args) {
    Locale.setDefault(Locale.ROOT); // guard against libraries inadvertently using default locale
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> fLogger.error("Exception in thread {}:", t.getName(), e));

    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();

    fLogger.info("-".repeat(80));
    fLogger.info("irenEditorServer " + ProgramInfo.VERSION);
    fLogger.info("-".repeat(80));

    Injector injector = Guice.createInjector(
        new EditorModule(),
        new FormulaRendererPreloaderModule());
    injector.getInstance(EditorServer.class).run(ArrayUtils.contains(args, "--companion"), DEFAULT_PORT,
        Utils::readFromSystemIn);
  }

  private final EditorMessageHandler.Factory fEditorMessageHandlerFactory;

  @Inject private EditorServer(EditorMessageHandler.Factory editorMessageHandlerFactory) {
    fEditorMessageHandlerFactory = editorMessageHandlerFactory;
  }

  private void run(boolean companion, int port, Runnable shutdownWaiter) {
    byte[] editorKey = Utils.readOrGenerateKey(companion ? null : EditorUtils.dataDirectory().resolve("editorKey"));

    NioEventLoopGroup childGroup = new NioEventLoopGroup(1, new DefaultThreadFactory("editor"));
    try {
      NioEventLoopGroup parentGroup = new NioEventLoopGroup(1);
      try {
        ServerBootstrap bootstrap = new ServerBootstrap();

        bootstrap
            .group(parentGroup, childGroup)
            .channel(NioServerSocketChannel.class)
            .childOption(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT)
            .childHandler(new ChannelInitializer<SocketChannel>() {
              @Override protected void initChannel(SocketChannel ch) {
                ch.pipeline().addLast(
                    NetworkExceptionHandler.get(),
                    new HttpRequestDecoder(),
                    new HttpResponseEncoder(),
                    new HttpObjectAggregator(1000),
                    new WebSocketFrameAggregator(EditorWebTransportHandler.MAX_FRAME_SIZE),
                    new EditorWebTransportHandler());
                MessageLogger.addIfLoggingEnabled(ch.pipeline());
                ch.pipeline().addLast(
                    fEditorMessageHandlerFactory.create(editorKey),
                    ExceptionHandler.get());
              }
            });
        InetAddress loopbackAddress = InetAddress.getLoopbackAddress();
        bootstrap.bind(loopbackAddress, port).syncUninterruptibly();

        fLogger.info("Listening on {}:{}.", loopbackAddress.getHostAddress(), port);

        if (companion) {
          System.out.format((Locale) null, "{\"port\": %d, \"editorKey\": \"%s\"}\n", port,
              BaseEncoding.base16().lowerCase().encode(editorKey));
          System.out.close();
        }

        shutdownWaiter.run();
      } finally {
        Utils.stopExecutorGroup(parentGroup);
      }
    } finally {
      Utils.stopExecutorGroup(childGroup);
    }

    fLogger.info("Stopped.");
    Runtime.getRuntime().halt(0); // Netty's lingering GlobalEventExecutor delays normal exit for a second
  }
}
