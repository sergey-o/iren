/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.scriptModifier;

import ru.irenproject.Modifier;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.modifier.ModifierEditor;
import ru.irenproject.editor.scriptModifier.Proto.XScriptModifierEditor;
import ru.irenproject.editor.scriptModifier.Proto.XScriptModifierEditor.SetScript;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.scriptModifier.ScriptModifier;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class ScriptModifierEditor extends ModifierEditor<ScriptModifier> {
  public interface Factory extends ModifierEditor.Factory {
    @Override ScriptModifierEditor create(Modifier modifier, @Nullable String questionType);
  }

  private int fCaretPosition;

  @Inject private ScriptModifierEditor(
      @Assisted Modifier modifier,
      @Assisted @Nullable String questionType) {
    super((ScriptModifier) modifier, questionType);
  }

  @Override public Message render(RenderContext context) {
    return XScriptModifierEditor.newBuilder()
        .addAllScript(modifier().script())
        .setCaretPosition(fCaretPosition)
        .build();
  }

  @Action public void doSetScript(SetScript in) {
    modifier().setScript(in.getScriptList());
    fCaretPosition = in.getCaretPosition();
  }
}
