/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor;

import ru.irenproject.Question;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.editor.Proto.XQuestionViewerContent;
import ru.irenproject.editor.Proto.XQuestionViewerContent.Evaluate;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.LiveQuestionLoader;
import ru.irenproject.live.Proto._LiveQuestion;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;

public final class QuestionViewerContent extends Widget {
  public interface Factory {
    QuestionViewerContent create(_LiveQuestion liveQuestion, Question sourceQuestion,
        QuestionViewerDashboard dashboard);
  }

  private final _LiveQuestion fLiveQuestion;
  private final Question fSourceQuestion;
  private final QuestionViewerDashboard fDashboard;

  @Inject private LiveQuestionLoader fLiveQuestionLoader;

  /** {@code sourceQuestion} must not change while the constructed object is in use. */
  @Inject private QuestionViewerContent(@Assisted _LiveQuestion liveQuestion, @Assisted Question sourceQuestion,
      @Assisted QuestionViewerDashboard dashboard) {
    super(sourceQuestion.realm());
    fLiveQuestion = liveQuestion;
    fSourceQuestion = sourceQuestion;
    fDashboard = dashboard;
  }

  @Override public Message render(RenderContext context) {
    LiveQuestion liveQuestion = liveQuestion();
    return XQuestionViewerContent.newBuilder()
        .setDialog(liveQuestion.render())
        .setEmptyResponse(liveQuestion.getEmptyResponse())
        .setCorrectResponse(liveQuestion.getCorrectResponse())
        .build();
  }

  private LiveQuestion liveQuestion() {
    return fLiveQuestionLoader.load(fLiveQuestion, id -> fSourceQuestion);
  }

  @Action public void doEvaluate(Evaluate in) {
    evaluate(in.getResponse());
  }

  private void evaluate(DialogResponse response) {
    fDashboard.setResult(liveQuestion().evaluate(response));
  }

  public void evaluateEmptyResponse() {
    evaluate(liveQuestion().getEmptyResponse());
  }
}
