/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.editor.matchQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestInputException;
import ru.irenproject.editor.Action;
import ru.irenproject.editor.QuestionEditor;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor.AddDistractor;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor.AddPair;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor.DeleteRow;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor.MoveRow;
import ru.irenproject.editor.matchQuestion.Proto.XMatchQuestionEditor.SetOptions;
import ru.irenproject.editor.pad.PadEditor;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Resident;
import ru.irenproject.matchQuestion.MatchQuestion;
import ru.irenproject.pad.Pad;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;

import javax.inject.Inject;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

public final class MatchQuestionEditor extends QuestionEditor<MatchQuestion> {
  public interface Factory extends QuestionEditor.Factory {
    @Override MatchQuestionEditor create(Question question);
  }

  private final PadEditor fFormulationEditor;
  private final LinkedHashMap<Pad, PadEditor> fItemEditors = new LinkedHashMap<>();

  @Inject private PadEditor.Factory fPadEditorFactory;

  @Inject private MatchQuestionEditor(@Assisted Question question, PadEditor.Factory padEditorFactory) {
    super((MatchQuestion) question);
    fFormulationEditor = padEditorFactory.create(question().formulation());
  }

  @Override public Message render(RenderContext context) {
    XMatchQuestionEditor.Builder b = XMatchQuestionEditor.newBuilder()
        .setFormulationEditor(context.link(fFormulationEditor))
        .addAllLeftEditor(context.linkAll(getItemEditors(question().left())))
        .addAllRightEditor(context.linkAll(getItemEditors(question().right())))
        .setMaxRows(MatchQuestion.MAX_ROWS)
        .setHasDefaultOptions(question().hasDefaultOptions());

    Integer pairLimit = question().pairLimit();
    if (pairLimit != null) {
      b.setPairLimit(pairLimit);
    }

    Integer distractorLimit = question().distractorLimit();
    if (distractorLimit != null) {
      b.setDistractorLimit(distractorLimit);
    }

    return b.build();
  }

  private List<PadEditor> getItemEditors(Collection<Pad> items) {
    return items.stream()
        .map(item -> fItemEditors.computeIfAbsent(item, fPadEditorFactory::create))
        .collect(Collectors.toList());
  }

  @Override protected void handleEvent(Event e, Resident source) {
    super.handleEvent(e, source);
    if (e instanceof Question.ChangeEvent) {
      fItemEditors.keySet().retainAll(ImmutableSet.copyOf(Iterables.concat(question().left(), question().right())));
    }
  }

  @Action(badInput = TestInputException.class)
  public void doAddPair(AddPair in) {
    question().addPair(new Pad(realm()), new Pad(realm()));
  }

  @Action(badInput = TestInputException.class)
  public void doAddDistractor(AddDistractor in) {
    question().addDistractor(new Pad(realm()));
  }

  @Action(badInput = TestInputException.class)
  public void doDeleteRow(DeleteRow in) {
    question().deleteRow(in.getIndex());
  }

  @Action(badInput = TestInputException.class)
  public void doMoveRow(MoveRow in) {
    question().moveRow(in.getIndex(), in.getForward());
  }

  @Action(badInput = TestInputException.class)
  public void doSetOptions(SetOptions in) {
    question().setPairLimit(in.hasPairLimit() ? in.getPairLimit() : null);
    question().setDistractorLimit(in.hasDistractorLimit() ? in.getDistractorLimit() : null);
  }
}
