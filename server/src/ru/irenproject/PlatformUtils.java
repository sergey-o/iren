/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.content.Proto.FormulaBlock;
import ru.irenproject.common.dialog.Proto.DialogResponse;

import com.google.common.io.ByteSource;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import java.util.Map;

public final class PlatformUtils {
  public static Any toAny(Message m) {
    return Any.pack(m, "");
  }

  public static <T extends Message> T unpackAny(Any any, T prototype) throws InvalidProtocolBufferException {
    @SuppressWarnings("unchecked")
    Class<? extends T> messageClass = (Class<? extends T>) prototype.getClass();
    return any.unpack(messageClass);
  }

  public static Map<String, Any> getDialogResponseAreaMap(DialogResponse response) {
    return response.getAreaMap();
  }

  public static DialogResponse.Builder putDialogResponseArea(DialogResponse.Builder builder, String areaId,
      Any areaResponse) {
    return builder.putArea(areaId, areaResponse);
  }

  public static FormulaBlock.Builder putAllFormulaBlockStyle(FormulaBlock.Builder builder, Map<String, String> style) {
    return builder.putAllStyle(style);
  }

  public static void removeFlowBlock(Flow.Builder flow, int blockIndex) {
    flow.removeBlock(blockIndex);
  }

  public static int getByteStringSize(ByteString byteString) {
    return byteString.size();
  }

  public static boolean isInitializedOrTrueIfUnsupported(Message m) {
    return m.isInitialized();
  }

  public static ByteString readByteSourceToByteString(ByteSource byteSource) {
    return Utils.readByteSourceToByteString(byteSource);
  }

  private PlatformUtils() {}
}
