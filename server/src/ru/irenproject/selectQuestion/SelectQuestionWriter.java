/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.selectQuestion;

import ru.irenproject.Question;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.QuestionWriter;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class SelectQuestionWriter implements QuestionWriter {
  @Inject private SelectQuestionWriter() {}

  @Override public void write(Question question, ItxWriter out) {
    SelectQuestion q = (SelectQuestion) question;
    out.writePad(q.formulation());

    if (!q.choices().isEmpty()) {
      out.begin(Itx.ELEM_CHOICES);
      for (Choice c : q.choices()) {
        out.begin(Itx.ELEM_CHOICE);
        out.addBoolean(Itx.ATTR_CORRECT, c.correct());
        out.addBoolean(Itx.ATTR_FIXED, c.fixed());
        out.addBoolean(Itx.ATTR_NEGATIVE, c.negative());
        out.writePad(c.pad());
        out.end();
      }
      out.end();
    }
  }
}
