/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;
import ru.irenproject.profile.SectionProfile;

import com.google.common.collect.Maps;
import com.google.common.collect.Streams;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;

public final class Test extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  private final Section fRoot;
  private final ProfileList fProfileList;
  private BigInteger fLastGeneratedTag = BigInteger.ZERO;

  public Test(Realm realm) {
    super(realm);
    fRoot = new Section(realm);
    fProfileList = new ProfileList(realm);
  }

  public Section root() {
    return fRoot;
  }

  public ProfileList profileList() {
    return fProfileList;
  }

  public BigInteger lastGeneratedTag() {
    return fLastGeneratedTag;
  }

  public void setLastGeneratedTag(BigInteger value) {
    if (!fLastGeneratedTag.equals(value)) {
      TestCheck.input(value.signum() >= 0);
      fLastGeneratedTag = value;
      post(ChangeEvent.INSTANCE);
    }
  }

  public void generateMissingTags() {
    BigInteger lastTag = fLastGeneratedTag.max(fRoot.maxNumericTag());

    for (QuestionItem item : fRoot.listTreeQuestions()) {
      if (item.tag().isEmpty()) {
        lastTag = lastTag.add(BigInteger.ONE);
        item.setTag(lastTag.toString());
      }
    }

    setLastGeneratedTag(lastTag);
  }

  public void reconcileSectionProfiles() {
    for (Profile profile : fProfileList.profiles()) {
      SectionProfile sectionProfile = profile.sectionProfile();
      if (sectionProfile != null) {
        //noinspection ConstantConditions
        SectionProfile reconciled = SectionProfile.createForTree(
            fRoot,
            Maps.transformValues(sectionProfile.profilesBySectionId(), SectionProfile::questionCount));
        if (!reconciled.sameAs(sectionProfile)) {
          profile.setSectionProfile(reconciled);
        }
      }
    }
  }

  public List<Modifier> listModifiers() {
    return
        Streams.concat(
            fRoot.listTreeQuestions().stream().map(QuestionItem::modifierList),
            fRoot.listTreeSections().stream().map(Section::modifierList),
            fProfileList.profiles().stream().map(Profile::modifierList))
        .flatMap(modifierList -> modifierList.modifiers().stream())
        .collect(Collectors.toList());
  }
}
