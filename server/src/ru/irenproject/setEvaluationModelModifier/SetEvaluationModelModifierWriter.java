/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setEvaluationModelModifier;

import ru.irenproject.Modifier;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.ModifierWriter;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class SetEvaluationModelModifierWriter implements ModifierWriter {
  @Inject private SetEvaluationModelModifierWriter() {}

  @Override public void write(Modifier modifier, ItxWriter out) {
    SetEvaluationModelModifier m = (SetEvaluationModelModifier) modifier;
    out.add(Itx.ATTR_NEW_EVALUATION_MODEL, typeToXml(m.modelType()));

    if (m.hasQuestionTypes()) {
      out.begin(Itx.ELEM_APPLY_TO);
      for (String questionType : m.questionTypes()) {
        out.begin(Itx.ELEM_QUESTION_TYPE);
        out.add(Itx.ATTR_VALUE, questionType);
        out.end();
      }
      out.end();
    }
  }

  private static String typeToXml(EvaluationModelType type) {
    switch (type) {
      case DICHOTOMIC: return Itx.VAL_EVALUATION_MODEL_TYPE_DICHOTOMIC;
      case LAX: return Itx.VAL_EVALUATION_MODEL_TYPE_LAX;
      default: throw new RuntimeException();
    }
  }
}
