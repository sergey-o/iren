/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setEvaluationModelModifier;

import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInputException;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.ModifierReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class SetEvaluationModelModifierReader implements ModifierReader {
  @Inject SetEvaluationModelModifierReader() {}

  @Override public SetEvaluationModelModifier read(ItxReader in, Element e) {
    SetEvaluationModelModifier res = new SetEvaluationModelModifier(in.realm());
    res.setModelType(xmlToType(Itx.find(e, Itx.ATTR_NEW_EVALUATION_MODEL)));

    Element xmlApplyTo = Itx.getChildIfExists(e, Itx.ELEM_APPLY_TO);
    if (xmlApplyTo != null) {
      for (Element xmlQuestionType : Itx.getChildren(xmlApplyTo, Itx.ELEM_QUESTION_TYPE)) {
        res.addQuestionType(Itx.find(xmlQuestionType, Itx.ATTR_VALUE));
      }
    }

    return res;
  }

  private static EvaluationModelType xmlToType(String s) {
    switch (s) {
      case Itx.VAL_EVALUATION_MODEL_TYPE_DICHOTOMIC: return EvaluationModelType.DICHOTOMIC;
      case Itx.VAL_EVALUATION_MODEL_TYPE_LAX: return EvaluationModelType.LAX;
      default: throw new ItxInputException();
    }
  }
}
