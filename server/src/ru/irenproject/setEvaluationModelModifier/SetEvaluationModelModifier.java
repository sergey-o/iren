/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setEvaluationModelModifier;

import ru.irenproject.Modifier;
import ru.irenproject.common.setEvaluationModelModifier.Proto.SetEvaluationModelModifierType;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.infra.Realm;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public final class SetEvaluationModelModifier extends Modifier {
  private EvaluationModelType fModelType = EvaluationModelType.LAX;
  private final LinkedHashSet<String> fQuestionTypes = new LinkedHashSet<>();

  public SetEvaluationModelModifier(Realm realm) {
    super(realm);
  }

  @Override public String type() {
    return SetEvaluationModelModifierType.setEvaluationModel.name();
  }

  public EvaluationModelType modelType() {
    return fModelType;
  }

  public void setModelType(EvaluationModelType value) {
    fModelType = value;
    post(ChangeEvent.INSTANCE);
  }

  public Set<String> questionTypes() {
    return Collections.unmodifiableSet(fQuestionTypes);
  }

  public void addQuestionType(String questionType) {
    fQuestionTypes.add(questionType);
    post(ChangeEvent.INSTANCE);
  }

  public void setQuestionTypes(Collection<String> value) {
    fQuestionTypes.clear();
    fQuestionTypes.addAll(value);
    post(ChangeEvent.INSTANCE);
  }

  public boolean hasQuestionTypes() {
    return !fQuestionTypes.isEmpty();
  }
}
