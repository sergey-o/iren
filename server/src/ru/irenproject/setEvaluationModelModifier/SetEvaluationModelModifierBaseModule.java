/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setEvaluationModelModifier;

import ru.irenproject.Modifier;
import ru.irenproject.common.setEvaluationModelModifier.Proto.SetEvaluationModelModifierType;
import ru.irenproject.itx.ModifierReader;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;

@Module public final class SetEvaluationModelModifierBaseModule {
  /** {@link SetEvaluationModelModifierType#setEvaluationModel} */
  @StringKey("setEvaluationModel")
  @Provides @IntoMap static Modifier.Factory provideFactory() {
    return SetEvaluationModelModifier::new;
  }

  /** {@link SetEvaluationModelModifierType#setEvaluationModel} */
  @StringKey("setEvaluationModel")
  @Provides @IntoMap static ModifierReader provideReader(SetEvaluationModelModifierReader reader) {
    return reader;
  }
}
