/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

public abstract class Modifier extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public interface Factory {
    Modifier create(Realm realm);
  }

  protected Modifier(Realm realm) {
    super(realm);
  }

  public abstract String type();

  public void setDefaults() {}
}
