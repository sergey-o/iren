/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.export.html;

import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.Translate;
import ru.irenproject.Utils;
import ru.irenproject.common.export.Proto.ExportOptions;
import ru.irenproject.common.export.Proto.ExportResult;
import ru.irenproject.export.ExportHelper;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.infra.Realm;
import ru.irenproject.itx.ExternallyStoredObjectWriter;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxCopier;
import ru.irenproject.itx.ItxImage;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.ObjectWriter;
import ru.irenproject.pad.FormulaBlock;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.QuestionCount;
import ru.irenproject.script.ScriptException;

import com.ctc.wstx.exc.WstxIOException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.html.HtmlEscapers;
import com.google.common.io.BaseEncoding;
import com.google.common.io.MoreFiles;
import com.google.common.io.Resources;
import com.google.gson.Gson;
import com.google.protobuf.Empty;
import org.apache.commons.io.output.CloseShieldOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedWriter;
import java.io.FilterWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.zip.DeflaterOutputStream;

@Singleton public final class HtmlExporter {
  private static final Logger fLogger = LoggerFactory.getLogger(HtmlExporter.class);

  private static final Path SUPPLY_DIR = Utils.externalResourcePath().resolve("htmlExport");

  private static final Gson GSON = new Gson();

  private static final ImmutableSet<String> POCKET_SERVER_DICTIONARY_KEYS = ImmutableSet.of("negativeChoice");

  private final ItxCopier fItxCopier;
  private final ItxWriter.Factory fItxWriterFactory;
  private final ExportHelper fExportHelper;

  @Inject HtmlExporter(
      ItxCopier itxCopier,
      ItxWriter.Factory itxWriterFactory,
      ExportHelper exportHelper) {
    fItxCopier = itxCopier;
    fItxWriterFactory = itxWriterFactory;
    fExportHelper = exportHelper;
  }

  public ExportResult export(Test test, Profile reconciledProfile, ExportOptions options, Path outputFile) {
    Realm exportRealm = new Realm(Utils::logUnexpectedEvent, null, test.realm().blobStore());

    Test exportedTest = fItxCopier.copy(
        w -> w.writeTest(test),
        r -> r.readTest(r.root()),
        exportRealm);

    Profile exportedProfile = fItxCopier.copy(
        w -> w.writeProfile(reconciledProfile),
        r -> r.readAndLinkProfile(Itx.getChild(r.root(), Itx.ELEM_PROFILE), exportedTest),
        exportRealm);

    ExportResult res;
    try {
      prepare(exportedTest, exportedProfile, options);
      write(exportedTest, options, outputFile);
      res = ExportResult.newBuilder()
          .setOk(Empty.getDefaultInstance())
          .build();
    } catch (ExportException e) {
      res = e.fFailure;
    } catch (RuntimeException e) {
      if ((e.getCause() instanceof IOException) || (e.getCause() instanceof WstxIOException)) {
        fLogger.warn("", e);
        res = ExportResult.newBuilder()
            .setIoError(Empty.getDefaultInstance())
            .build();
      } else {
        throw e;
      }
    }

    return res;
  }

  private static final class ExportException extends RuntimeException {
    final ExportResult fFailure;

    ExportException(ExportResult failure) {
      fFailure = failure;
    }
  }

  private void prepare(Test test, Profile profile, ExportOptions options) {
    //TODO
    if (profile.options().hasLabelFilter()) {
      throw new ExportException(ExportResult.newBuilder()
          .setCustomErrorMessage("Labels are not supported yet.")
          .build());
    }

    test.profileList().clear();
    test.profileList().add(profile);

    deleteQuestionsExcludedBySectionProfile(test, profile);
    fExportHelper.deleteDisabledQuestions(test.root());

    fExportHelper.deleteEmptySections(test.root());
    test.reconcileSectionProfiles();

    for (QuestionItem questionItem : test.root().listTreeQuestions()) {
      questionItem.clearLabels();
      questionItem.setTag("");
    }

    profile.setTitle("");
    profile.setOptions(profile.options().toBuilder()
        .clearLabelFilter()
        .buildPartial());

    test.setLastGeneratedTag(BigInteger.ZERO);

    try {
      fExportHelper.computeScriptResults(test, Locale.forLanguageTag(options.getLanguage()),
          scriptNumber -> fLogger.info("Running script {}...", scriptNumber));
    } catch (ScriptException e) {
      throw new ExportException(ExportResult.newBuilder()
          .setCustomErrorMessage("[Script problem] " + e.getMessage())
          .build());
    }
  }

  private void deleteQuestionsExcludedBySectionProfile(Test test, Profile profile) {
    if (profile.sectionProfile() != null) {
      ImmutableMap<Long, QuestionCount> map = profile.sectionProfile().effectiveQuestionCountsBySectionId();
      for (Section section : test.root().listTreeSections()) {
        if (map.get(section.id()).value() == 0) {
          section.questionList().clear();
        }
      }
    }
  }

  private void write(Test test, ExportOptions options, Path outputFile) {
    try {
      Path tempFile = Utils.changeFileName(outputFile, Utils::generateTempFileName);

      boolean deleteTempFile = true;
      try {
        try (BufferedWriter writer = Files.newBufferedWriter(tempFile, StandardOpenOption.CREATE_NEW)) {
          writeResource("header", writer);

          writer.write("<title>");
          writer.write(HtmlEscapers.htmlEscaper().escape(options.getTitle()));
          writer.write("</title>\n");

          pack("irenStyle", writer, out -> Files.copy(SUPPLY_DIR.resolve("style.css"), out));

          BlobIdMemorizingObjectWriter objectWriter = new BlobIdMemorizingObjectWriter();
          ItxWriterFilter itxWriterFilter = new ItxWriterFilter();

          pack("irenTest", writer, out -> Utils.writeXml(out, w -> {
            try {
              w.writeStartElement(Itx.ELEM_IREN_TEST);
              w.writeAttribute(Itx.ATTR_VERSION, Integer.toString(Itx.VERSION));

              ItxWriter itxWriter = fItxWriterFactory.create(w, objectWriter);
              itxWriter.setFilter(itxWriterFilter);
              itxWriter.writeTest(test);

              w.writeEndElement();
            } catch (XMLStreamException e) {
              throw new RuntimeException(e);
            }
          }));

          for (BlobId blobId : objectWriter.fBlobIds) {
            writer.write(String.format("<script id=\"irenBlob-%s\" type=\"text/plain\">", blobId.toHexString()));
            try (OutputStream encoder = BaseEncoding.base64().encodingStream(preventClose(writer))) {
              test.realm().blobStore().get(blobId).copyTo(encoder);
            }
            writer.write("</script>\n");
          }

          pack("irenClientServer", writer, out -> {
            out.write(String.format("window.irenLanguage = %s;\n", GSON.toJson(options.getLanguage()))
                .getBytes(StandardCharsets.UTF_8));

            ImmutableMap<String, String> serverDictionary = buildPocketServerDictionary(
                Locale.forLanguageTag(options.getLanguage()));
            out.write(String.format("window.irenPocketServerDictionary = %s;\n", GSON.toJson(serverDictionary))
                .getBytes(StandardCharsets.UTF_8));

            if (itxWriterFilter.fFormulaFound) {
              Files.copy(SUPPLY_DIR.resolve("formulaRenderer.js"), out);
            }
            Files.copy(SUPPLY_DIR.resolve("pocketServer.js"), out);
            Files.copy(SUPPLY_DIR.resolve("client.js"), out);
          });

          writer.write("<script>\n");
          MoreFiles.asCharSource(SUPPLY_DIR.resolve("pako_inflate.min.js"), StandardCharsets.UTF_8).copyTo(writer);
          writer.write("</script>\n");

          writeResource("unpacker", writer);
          writeResource("footer", writer);
        }

        Files.move(tempFile, outputFile, StandardCopyOption.ATOMIC_MOVE);
        deleteTempFile = false;
      } finally {
        if (deleteTempFile) {
          Files.deleteIfExists(tempFile);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void writeResource(String name, Writer out) throws IOException {
    Resources.asCharSource(Resources.getResource(HtmlExporter.class, name), StandardCharsets.UTF_8).copyTo(out);
  }

  private void pack(String scriptId, Writer out, Generator generator) throws IOException {
    out.write(String.format("<script id=\"%s\" type=\"text/plain\">", scriptId));
    try (
        OutputStream encoder = BaseEncoding.base64().encodingStream(preventClose(out));
        DeflaterOutputStream compressor = new DeflaterOutputStream(new CloseShieldOutputStream(encoder))) {
      generator.generate(compressor);
    }
    out.write("</script>\n");
  }

  private interface Generator {
    void generate(OutputStream out) throws IOException;
  }

  private Writer preventClose(Writer writer) {
    return new FilterWriter(writer) {
      @SuppressWarnings("MethodDoesntCallSuperMethod")
      @Override public void close() {}
    };
  }

  private static final class BlobIdMemorizingObjectWriter implements ObjectWriter {
    final LinkedHashSet<BlobId> fBlobIds = new LinkedHashSet<>();
    final ExternallyStoredObjectWriter fActualObjectWriter = new ExternallyStoredObjectWriter();

    @Override public ItxImage writeImage(ImageBlock block, BlobStore blobStore) {
      fBlobIds.add(block.data());
      return fActualObjectWriter.writeImage(block, blobStore);
    }
  }

  private static final class ItxWriterFilter implements ItxWriter.Filter {
    boolean fFormulaFound;

    @Override public void filterFormulaBlock(FormulaBlock block) {
      fFormulaFound = true;
    }
  }

  private ImmutableMap<String, String> buildPocketServerDictionary(Locale locale) {
    return POCKET_SERVER_DICTIONARY_KEYS.stream()
        .collect(ImmutableMap.toImmutableMap(key -> key, key -> Translate.message(key, locale)));
  }
}
