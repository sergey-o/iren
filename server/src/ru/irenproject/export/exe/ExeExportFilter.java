/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.export.exe;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.QuestionItem;
import ru.irenproject.Test;
import ru.irenproject.TestModule;
import ru.irenproject.Utils;
import ru.irenproject.export.ExportHelper;
import ru.irenproject.infra.MemoryBlobStore;
import ru.irenproject.infra.Realm;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxOutput;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.pad.FormulaBlock;
import ru.irenproject.profile.Profile;
import ru.irenproject.script.ScriptException;

import com.google.common.io.BaseEncoding;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.Locale.Category;

public final class ExeExportFilter {
  public static void main(String[] args) {
    Locale locale = Locale.getDefault(Category.DISPLAY);
    Locale.setDefault(Locale.ROOT); // guard against libraries inadvertently using default locale

    Check.input(args.length == 2);

    Injector injector = Guice.createInjector(new TestModule());
    try {
      injector.getInstance(ExeExportFilter.class).run(unescapePath(args[0]), unescapePath(args[1]), locale);
    } catch (ScriptException e) {
      System.err.println("[Script problem] " + e.getMessage());
      System.exit(1);
    } catch (BadInputException e) {
      System.err.println(e);
      System.exit(1);
    }
  }

  private static Path unescapePath(String s) {
    return Paths.get(new String(BaseEncoding.base16().lowerCase().decode(s), StandardCharsets.UTF_8));
  }

  private final ItxInput fItxInput;
  private final ItxOutput fItxOutput;
  private final ExportHelper fExportHelper;

  @Inject private ExeExportFilter(
      ItxInput itxInput,
      ItxOutput itxOutput,
      ExportHelper exportHelper) {
    fItxInput = itxInput;
    fItxOutput = itxOutput;
    fExportHelper = exportHelper;
  }

  private void run(Path in, Path out, Locale locale) {
    try {
      Test test = fItxInput.readTest(in, new Realm(Utils::logUnexpectedEvent, null, new MemoryBlobStore()));

      fExportHelper.deleteDisabledQuestions(test.root());
      fExportHelper.deleteEmptySections(test.root());

      rejectRegexpPatterns(test);
      test.root().listTreeQuestions().forEach(QuestionItem::clearLabels);

      if (test.profileList().isEmpty()) {
        test.profileList().add(Profile.createDefault(test.realm()));
      } else {
        for (int i = test.profileList().profiles().size() - 1; i >= 1; --i) {
          test.profileList().delete(i);
        }
      }

      Profile profile = test.profileList().profiles().get(0);

      //TODO
      if (profile.options().hasLabelFilter()) {
        throw new BadInputException("Labels are not supported yet.");
      }

      if (profile.sectionProfile() != null) {
        throw new BadInputException("Section profiles are not supported in .exe tests."
            + " Please export to .html instead.");
      }

      profile.setTitle("");
      profile.setOptions(profile.options().toBuilder()
          .clearLabelFilter()
          .buildPartial());

      fExportHelper.computeScriptResults(test, locale,
          scriptNumber -> System.err.format("Running script %d...%n", scriptNumber));

      try (OutputStream outputStream = Files.newOutputStream(out)) {
        fItxOutput.writeTest(test, outputStream, true, new OutputFilter());
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static void rejectRegexpPatterns(Test test) {
    for (QuestionItem item : test.root().listTreeQuestions()) {
      if ((item.question() instanceof InputQuestion)
          && ((InputQuestion) item.question()).patterns().stream().anyMatch(p -> p instanceof RegexpPattern)) {
        throw new BadInputException("Regular expression patterns are not supported in .exe tests."
            + " Please export to .html instead.");
      }
    }
  }

  private static final class OutputFilter implements ItxWriter.Filter {
    @Override public void filterFormulaBlock(FormulaBlock block) {
      throw new BadInputException("Formulas are not supported in .exe tests."
          + " Please export to .html instead.");
    }
  }
}
