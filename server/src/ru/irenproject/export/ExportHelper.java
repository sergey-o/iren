/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.export;

import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.script.ScriptResult;
import ru.irenproject.script.isolated.IsolatedScriptRunner;
import ru.irenproject.scriptModifier.ScriptModifier;

import com.google.common.collect.Iterables;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Singleton public final class ExportHelper {
  private static final int SCRIPT_RUN_COUNT = 50;

  private static final Pattern ESCAPED_CHARACTERS = Pattern.compile("[\u0000-\u001f\ufffe\uffff\\\\]");

  private final IsolatedScriptRunner fScriptRunner;

  @Inject ExportHelper(IsolatedScriptRunner scriptRunner) {
    fScriptRunner = scriptRunner;
  }

  public void deleteDisabledQuestions(Section section) {
    section.questionList().removeItems(section.questionList().items().stream()
        .filter(item -> !item.enabled())
        .collect(Collectors.toList()));
    section.sections().forEach(this::deleteDisabledQuestions);
  }

  public void deleteEmptySections(Section section) {
    ArrayList<Section> empty = new ArrayList<>();

    for (Section s : section.sections()) {
      deleteEmptySections(s);
      if (s.questionList().isEmpty() && s.sections().isEmpty()) {
        empty.add(s);
      }
    }

    empty.forEach(section::removeSection);
  }

  public void computeScriptResults(Test test, Locale locale, Consumer<Long> progressCallback) {
    long scriptNumber = 0;
    for (ScriptModifier m : Iterables.filter(test.listModifiers(), ScriptModifier.class)) {
      ++scriptNumber;
      progressCallback.accept(scriptNumber);

      LinkedHashSet<List<String>> formattedResults = new LinkedHashSet<>();
      for (int i = 0; i < SCRIPT_RUN_COUNT; ++i) {
        formattedResults.add(formatScriptResult(fScriptRunner.run(m.script(), locale, null)));
      }

      ArrayList<String> exportedScript = new ArrayList<>();
      exportedScript.add("//@@EXPORTED_RESULTS@@");
      for (List<String> formattedResult : formattedResults) {
        exportedScript.addAll(formattedResult);
        exportedScript.add("//");
      }
      exportedScript.add("//@@END_EXPORTED_RESULTS@@");

      exportedScript.add("");
      exportedScript.add("begin end.");

      m.setScript(exportedScript);
    }
  }

  private List<String> formatScriptResult(ScriptResult result) {
    return result.variables().entrySet().stream()
        .map(e -> String.format("//%s=%s", e.getKey(), escapeVariableValue(e.getValue())))
        .collect(Collectors.toList());
  }

  private String escapeVariableValue(String s) {
    Matcher matcher = ESCAPED_CHARACTERS.matcher(s);
    StringBuffer result = new StringBuffer();
    while (matcher.find()) {
      matcher.appendReplacement(result, String.format("\\\\u%04x", (int) matcher.group().charAt(0)));
    }
    matcher.appendTail(result);

    return result.toString();
  }
}
