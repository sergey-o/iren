/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Block;
import ru.irenproject.common.content.Proto.BlockType;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.content.Proto.FlowArea;
import ru.irenproject.common.dialog.Proto.DialogArea;
import ru.irenproject.pad.FormulaBlock;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.LineFeedBlock;
import ru.irenproject.pad.Pad;
import ru.irenproject.pad.TextBlock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public final class QuestionUtils {
  private static final Block LINE_FEED_BLOCK = Block.newBuilder()
      .setType(BlockType.LINE_FEED)
      .build();

  public static BigDecimal evaluateMappingLaxly(List<Integer> answer, List<Integer> correct) {
    int size = correct.size();
    Check.that(answer.size() == size);

    int good = 0;
    for (int i = 0; i < size; ++i) {
      int a = answer.get(i);
      if (a != -1) {
        int c = correct.get(i);
        if (a == c) {
          ++good;
        } else {
          --good;
        }
      }
    }
    good = Integer.max(good, 0);

    return (good == size) ? BigDecimal.ONE :
        BigDecimal.valueOf(good).divide(BigDecimal.valueOf(size),
            LiveQuestion.SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
  }

  public static Flow toFlow(Pad pad) {
    Flow.Builder b = Flow.newBuilder();

    for (ru.irenproject.pad.Block block : pad.blocks()) {
      if (block instanceof TextBlock) {
        b.addBlock(Block.newBuilder()
            .setType(BlockType.TEXT)
            .setTextBlock(ru.irenproject.common.content.Proto.TextBlock.newBuilder()
                .setText(((TextBlock) block).text())));
      } else if (block instanceof ImageBlock) {
        ImageBlock imageBlock = (ImageBlock) block;
        b.addBlock(Block.newBuilder()
            .setType(BlockType.IMAGE)
            .setImageBlock(ru.irenproject.common.content.Proto.ImageBlock.newBuilder()
                .setMimeType(imageBlock.mimeType())
                .setData(PlatformUtils.readByteSourceToByteString(pad.realm().blobStore().get(imageBlock.data())))));
      } else if (block instanceof LineFeedBlock) {
        b.addBlock(LINE_FEED_BLOCK);
      } else if (block instanceof FormulaBlock) {
        b.addBlock(Block.newBuilder()
            .setType(BlockType.FORMULA)
            .setFormulaBlock(ru.irenproject.common.content.Proto.FormulaBlock.newBuilder()
                .setSource(((FormulaBlock) block).source())));
      } else {
        throw new RuntimeException();
      }
    }

    return b.build();
  }

  public static DialogArea buildFlowArea(Flow flow) {
    return DialogArea.newBuilder()
        .setArea(PlatformUtils.toAny(FlowArea.newBuilder()
            .setFlow(flow)
            .build()))
        .build();
  }

  private QuestionUtils() {}
}
