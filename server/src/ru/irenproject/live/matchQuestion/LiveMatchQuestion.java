/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.matchQuestion;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.dialog.Proto.Dialog;
import ru.irenproject.common.dialog.Proto.DialogArea;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.matchArea.Proto.MatchArea;
import ru.irenproject.common.matchArea.Proto.MatchResponse;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.live.ContentPostprocessor;
import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.matchQuestion.Proto.Match;
import ru.irenproject.matchQuestion.MatchQuestion;
import ru.irenproject.pad.Pad;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@AutoFactory(implementing = LiveQuestion.Factory.class)
public final class LiveMatchQuestion extends LiveQuestion {
  private final Match $;

  LiveMatchQuestion(
      _LiveQuestion message,
      SourceQuestionResolver resolver,
      @Provided ContentTransformer.Factory contentTransformerFactory,
      @Provided ContentPostprocessor contentPostprocessor) {
    super(message, resolver, contentTransformerFactory, contentPostprocessor);
    $ = unpack(Match.getDefaultInstance());
  }

  @Override public Dialog render() {
    Flow formulation = QuestionUtils.toFlow(source().formulation());
    return Dialog.newBuilder()
        .addDialogArea(QuestionUtils.buildFlowArea(publish(formulation)))
        .addDialogArea(DialogArea.newBuilder()
            .setId(DEFAULT_AREA_ID)
            .setArea(PlatformUtils.toAny(buildMatchArea())))
        .build();
  }

  private MatchArea buildMatchArea() {
    MatchArea.Builder b = MatchArea.newBuilder();

    List<Pad> left = source().left();
    for (int i : $.getLeftPlacementList()) {
      b.addLeft(publish(QuestionUtils.toFlow(left.get(i))));
    }

    List<Pad> right = source().right();
    for (int i : $.getRightPlacementList()) {
      b.addRight(publish(QuestionUtils.toFlow(right.get(i))));
    }

    return b.build();
  }

  @Override public MatchQuestion source() {
    return (MatchQuestion) super.source();
  }

  @Override public DialogResponse getEmptyResponse() {
    return buildResponse(Collections.nCopies($.getLeftPlacementCount(), -1));
  }

  private static DialogResponse buildResponse(Iterable<Integer> mapping) {
    return buildSimpleDialogResponse(MatchResponse.newBuilder()
        .addAllMapping(mapping)
        .build());
  }

  @Override public DialogResponse getCorrectResponse() {
    ArrayList<Integer> correctMapping = new ArrayList<>();
    int[] inverse = inverseRightPlacement();

    for (int sourceLeftIndex : $.getLeftPlacementList()) {
      int rightPlacementIndex = inverse[sourceLeftIndex];
      Check.that(rightPlacementIndex != -1);
      correctMapping.add(rightPlacementIndex);
    }

    return buildResponse(correctMapping);
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    MatchResponse r = getSingleResponse(response, MatchResponse.getDefaultInstance());
    check(r);

    List<Integer> answer = mappingToSourceRightIndices(r.getMappingList());
    BigDecimal res;

    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = $.getLeftPlacementList().equals(answer) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = QuestionUtils.evaluateMappingLaxly(answer, $.getLeftPlacementList());
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private void check(MatchResponse response) {
    checkResponse(response.getMappingCount() == $.getLeftPlacementCount());
    int rightCount = $.getRightPlacementCount();
    boolean[] itemChosen = new boolean[rightCount];

    for (int itemIndex : response.getMappingList()) {
      checkResponse((itemIndex >= -1) && (itemIndex < rightCount));
      if (itemIndex != -1) {
        checkResponse(!itemChosen[itemIndex]);
        itemChosen[itemIndex] = true;
      }
    }
  }

  private List<Integer> mappingToSourceRightIndices(Iterable<Integer> mapping) {
    ArrayList<Integer> res = new ArrayList<>();
    for (int itemIndex : mapping) {
      res.add((itemIndex == -1) ? -1 : $.getRightPlacement(itemIndex));
    }
    return res;
  }

  private int[] inverseRightPlacement() {
    int[] res = new int[source().right().size()];
    Arrays.fill(res, -1);

    for (int i = 0; i < $.getRightPlacementCount(); ++i) {
      res[$.getRightPlacement(i)] = i;
    }

    return res;
  }
}
