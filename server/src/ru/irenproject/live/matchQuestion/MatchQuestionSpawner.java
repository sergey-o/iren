/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.matchQuestion;

import ru.irenproject.Question;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.Live;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.matchQuestion.Proto.Match;
import ru.irenproject.matchQuestion.MatchQuestion;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@Singleton public final class MatchQuestionSpawner implements QuestionSpawner {
  private final ThreadLocal<Random> fRandom;

  @Inject MatchQuestionSpawner(@Live ThreadLocal<Random> random) {
    fRandom = random;
  }

  @Override public void prepare(Question source, IssueOrderList orderList) {}

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    MatchQuestion q = (MatchQuestion) source;
    int sourceLeftCount = q.left().size();
    int sourceRightCount = q.right().size();

    ArrayList<Integer> left = new ArrayList<>();
    for (int i = 0; i < sourceLeftCount; ++i) {
      left.add(i);
    }

    ArrayList<Integer> right = new ArrayList<>();
    for (int distractorIndex = sourceLeftCount; distractorIndex < sourceRightCount; ++distractorIndex) {
      right.add(distractorIndex);
    }

    Integer pairLimit = q.pairLimit();
    if (pairLimit != null) {
      for (int pairsUsed = Integer.min(pairLimit, sourceLeftCount),
          excessPairs = sourceLeftCount - pairsUsed,
          i = 0; i < excessPairs; ++i) {
        int n = fRandom.get().nextInt(left.size());
        right.add(left.get(n)); // use excluded item as a distractor
        left.remove(n);
      }
    }

    // `right` now consists of all available distractors

    Integer distractorLimit = q.distractorLimit();
    if (distractorLimit != null) {
      for (int distractorsUsed = Integer.min(distractorLimit, right.size()),
          excessDistractors = right.size() - distractorsUsed,
          i = 0; i < excessDistractors; ++i) {
        right.remove(fRandom.get().nextInt(right.size()));
      }
    }

    right.addAll(left);

    Collections.shuffle(right, fRandom.get());

    Match m = Match.newBuilder()
        .addAllLeftPlacement(left)
        .addAllRightPlacement(right)
        .build();

    return LiveQuestion.initialize(m, source, orderList);
  }
}
