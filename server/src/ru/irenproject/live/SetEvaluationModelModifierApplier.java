/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Modifier;
import ru.irenproject.Question;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifier;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class SetEvaluationModelModifierApplier implements ModifierApplier {
  @Inject SetEvaluationModelModifierApplier() {}

  @Override public void apply(Modifier modifier, Question question, IssueOrderList orderList) {
    SetEvaluationModelModifier m = (SetEvaluationModelModifier) modifier;

    if (!m.hasQuestionTypes() || m.questionTypes().contains(question.type())) {
      SetEvaluationModelIssueOrder order = orderList.getInstanceIfExists(SetEvaluationModelIssueOrder.class);
      if (order == null) {
        orderList.add(new SetEvaluationModelIssueOrder(m.modelType()));
      } else {
        order.setModelType(m.modelType());
      }
    }
  }
}
