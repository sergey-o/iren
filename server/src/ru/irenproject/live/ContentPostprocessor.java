/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Block;
import ru.irenproject.common.content.Proto.BlockType;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.content.Proto.FormulaBlock;
import ru.irenproject.formula.FormulaImage;
import ru.irenproject.formula.FormulaRendererLike;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;

@Singleton public final class ContentPostprocessor {
  private final FormulaRendererLike fFormulaRenderer;

  @Inject ContentPostprocessor(FormulaRendererLike formulaRenderer) {
    fFormulaRenderer = formulaRenderer;
  }

  public Flow postprocessFlow(Flow flow) {
    ArrayList<Block> blocks = null;

    for (int blockCount = flow.getBlockCount(), i = 0; i < blockCount; ++i) {
      Block block = flow.getBlock(i);
      if (block.getType() == BlockType.FORMULA) {
        if (blocks == null) {
          blocks = new ArrayList<>(flow.getBlockList());
        }
        blocks.set(i, block.toBuilder()
            .setFormulaBlock(renderFormula(block.getFormulaBlock()))
            .buildPartial());
      }
    }

    return (blocks == null) ? flow : flow.toBuilder()
        .clearBlock()
        .addAllBlock(blocks)
        .buildPartial();
  }

  private FormulaBlock renderFormula(FormulaBlock formula) {
    FormulaImage formulaImage = fFormulaRenderer.render(formula.getSource());
    return PlatformUtils.putAllFormulaBlockStyle(formula.toBuilder(), formulaImage.style())
        .setSvg(formulaImage.svg())
        .clearSource()
        .buildPartial();
  }
}
