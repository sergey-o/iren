/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.Question;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.dialog.Proto.Dialog;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.live.Proto._ContentTransformer;
import ru.irenproject.live.Proto._LiveQuestion;

import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import javax.annotation.Nullable;
import java.math.BigDecimal;

public abstract class LiveQuestion {
  public interface Factory {
    LiveQuestion create(_LiveQuestion message, SourceQuestionResolver resolver);
  }

  public interface SourceQuestionResolver {
    Question getQuestion(String source);
  }

  public static final int SCORE_FRACTION_DIGITS = 4;
  protected static final String DEFAULT_AREA_ID = "_";

  public static _LiveQuestion initialize(Message data, Question source, IssueOrderList issueOrders) {
    _LiveQuestion.Builder b = _LiveQuestion.newBuilder()
        .setData(PlatformUtils.toAny(data))
        .setSource(Long.toString(source.id()))
        .setWeight(issueOrders.getInstance(SetWeightIssueOrder.class).weight());

    _ContentTransformer contentTransformer = ContentTransformer.buildIfNotEmpty(issueOrders);
    if (contentTransformer != null) {
      b.setContentTransformer(contentTransformer);
    }

    SetEvaluationModelIssueOrder evaluationModel = issueOrders.getInstanceIfExists(SetEvaluationModelIssueOrder.class);
    if (evaluationModel != null) {
      b.setEvaluationModel(evaluationModel.modelType());
    }

    return b.build();
  }

  public static void checkResponse(boolean condition) {
    if (!condition) {
      throw new BadResponseException();
    }
  }

  public static <T extends Message> T getSingleResponse(DialogResponse response, T prototype) {
    checkResponse(response.getAreaCount() == 1);
    Any areaResponse = Check.notNull(PlatformUtils.getDialogResponseAreaMap(response).get(DEFAULT_AREA_ID),
        BadResponseException::new);
    try {
      return PlatformUtils.unpackAny(areaResponse, prototype);
    } catch (InvalidProtocolBufferException e) {
      throw new BadResponseException(e);
    }
  }

  public static DialogResponse buildSimpleDialogResponse(Message responseForDefaultArea) {
    return
        PlatformUtils.putDialogResponseArea(
            DialogResponse.newBuilder(),
            DEFAULT_AREA_ID,
            PlatformUtils.toAny(responseForDefaultArea)
        ).build();
  }

  private final _LiveQuestion $;
  private final ContentTransformer.Factory fContentTransformerFactory;
  private final ContentPostprocessor fContentPostprocessor;
  private final Question fSource;
  private @Nullable ContentTransformer fContentTransformer;

  protected LiveQuestion(
      _LiveQuestion message,
      SourceQuestionResolver resolver,
      ContentTransformer.Factory contentTransformerFactory,
      ContentPostprocessor contentPostprocessor) {
    $ = message;
    fContentTransformerFactory = contentTransformerFactory;
    fContentPostprocessor = contentPostprocessor;
    fSource = resolver.getQuestion($.getSource());
  }

  public Question source() {
    return fSource;
  }

  public abstract Dialog render();
  public abstract DialogResponse getEmptyResponse();
  public abstract DialogResponse getCorrectResponse();

  public abstract BigDecimal evaluate(DialogResponse response);

  protected final <T extends Message> T unpack(T prototype) {
    try {
      return PlatformUtils.unpackAny($.getData(), prototype);
    } catch (InvalidProtocolBufferException e) {
      throw new BadLiveQuestionException(e);
    }
  }

  protected final Flow publish(Flow flow) {
    return fContentPostprocessor.postprocessFlow(contentTransformer().transformFlow(flow));
  }

  protected final ContentTransformer contentTransformer() {
    if (fContentTransformer == null) {
      fContentTransformer = $.hasContentTransformer() ?
          fContentTransformerFactory.create($.getContentTransformer()) :
          ContentTransformer.IDENTITY;
    }
    return fContentTransformer;
  }

  protected final EvaluationModelType getEvaluationModelOrDefault(EvaluationModelType defaultValue) {
    return $.hasEvaluationModel() ? $.getEvaluationModel() : defaultValue;
  }
}
