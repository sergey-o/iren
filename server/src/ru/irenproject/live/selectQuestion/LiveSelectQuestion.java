/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.selectQuestion;

import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Block;
import ru.irenproject.common.content.Proto.BlockType;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.content.Proto.TextBlock;
import ru.irenproject.common.dialog.Proto.Dialog;
import ru.irenproject.common.dialog.Proto.DialogArea;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.selectArea.Proto.SelectArea;
import ru.irenproject.common.selectArea.Proto.SelectResponse;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.live.ContentPostprocessor;
import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.selectQuestion.Proto.Select;
import ru.irenproject.selectQuestion.Choice;
import ru.irenproject.selectQuestion.SelectQuestion;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AutoFactory(implementing = LiveQuestion.Factory.class)
public final class LiveSelectQuestion extends LiveQuestion {
  private final Select $;

  LiveSelectQuestion(
      _LiveQuestion message,
      SourceQuestionResolver resolver,
      @Provided ContentTransformer.Factory contentTransformerFactory,
      @Provided ContentPostprocessor contentPostprocessor) {
    super(message, resolver, contentTransformerFactory, contentPostprocessor);
    $ = unpack(Select.getDefaultInstance());
  }

  @Override public SelectQuestion source() {
    return (SelectQuestion) super.source();
  }

  private int dialogChoiceCount() {
    return source().choices().size() + (hasSyntheticNegativeChoice() ? 1 : 0);
  }

  private boolean hasSyntheticNegativeChoice() {
    return $.hasCustomNegativeChoice() && !source().hasNegativeChoice();
  }

  @Override public Dialog render() {
    Flow formulation = QuestionUtils.toFlow(source().formulation());
    return Dialog.newBuilder()
        .addDialogArea(QuestionUtils.buildFlowArea(publish(formulation)))
        .addDialogArea(DialogArea.newBuilder()
            .setId(DEFAULT_AREA_ID)
            .setArea(PlatformUtils.toAny(buildSelectArea())))
        .build();
  }

  private SelectArea buildSelectArea() {
    SelectArea.Builder b = SelectArea.newBuilder()
        .setMultipleSelection($.getMultipleSelection());

    for (Choice choice : shuffledChoiceList()) {
      Flow flow = (choice.negative() && $.hasCustomNegativeChoice()) ?
          customNegativeChoice() : QuestionUtils.toFlow(choice.pad());
      b.addChoice(publish(flow));
    }

    if (hasSyntheticNegativeChoice()) {
      b.addChoice(publish(customNegativeChoice()));
    }

    return b.build();
  }

  private Flow customNegativeChoice() {
    return Flow.newBuilder()
        .addBlock(Block.newBuilder()
            .setType(BlockType.TEXT)
            .setTextBlock(TextBlock.newBuilder()
                .setText($.getCustomNegativeChoice().getText())))
        .build();
  }

  private List<Choice> shuffledChoiceList() {
    List<Choice> choiceList = source().choices();
    return $.getChoiceIndexList().stream()
        .map(choiceList::get)
        .collect(Collectors.toList());
  }

  @Override public DialogResponse getEmptyResponse() {
    return buildSimpleDialogResponse(SelectResponse.newBuilder()
        .addAllSelected(Collections.nCopies(dialogChoiceCount(), false))
        .build());
  }

  @Override public DialogResponse getCorrectResponse() {
    return buildSimpleDialogResponse(SelectResponse.newBuilder()
        .addAllSelected(correctAnswer())
        .build());
  }

  private List<Boolean> correctAnswer() {
    ArrayList<Boolean> res = new ArrayList<>();

    for (Choice choice : shuffledChoiceList()) {
      res.add(choice.correct());
    }

    if (hasSyntheticNegativeChoice()) {
      res.add(false);
    }

    return res;
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    SelectResponse r = getSingleResponse(response, SelectResponse.getDefaultInstance());
    checkResponse(r.getSelectedCount() == dialogChoiceCount());

    BigDecimal res;
    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = correctAnswer().equals(r.getSelectedList()) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = evaluateLaxly(r);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private BigDecimal evaluateLaxly(SelectResponse r) {
    List<Boolean> correctAnswer = correctAnswer();
    int correctTotal = 0;
    int correctSelected = 0;
    boolean incorrectSelected = false;

    for (int i = 0; i < correctAnswer.size(); ++i) {
      boolean selected = r.getSelected(i);
      if (correctAnswer.get(i)) {
        ++correctTotal;
        if (selected) {
          ++correctSelected;
        }
      } else {
        if (selected) {
          incorrectSelected = true;
          break;
        }
      }
    }

    BigDecimal res;
    if (incorrectSelected) {
      res = BigDecimal.ZERO;
    } else if (correctSelected == correctTotal) {
      res = BigDecimal.ONE;
    } else {
      res = BigDecimal.valueOf(correctSelected).divide(BigDecimal.valueOf(correctTotal),
          SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
    }

    return res;
  }
}
