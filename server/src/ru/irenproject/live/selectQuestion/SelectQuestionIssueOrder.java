/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.selectQuestion;

import ru.irenproject.live.IssueOrder;

import javax.annotation.Nullable;

public final class SelectQuestionIssueOrder implements IssueOrder {
  private boolean fShuffleChoices;
  private boolean fSuppressSingleChoiceHint;
  private boolean fAddNegativeChoice;
  private @Nullable String fNegativeChoiceContent;

  public boolean shuffleChoices() {
    return fShuffleChoices;
  }

  public void setShuffleChoices(boolean shuffleChoices) {
    fShuffleChoices = shuffleChoices;
  }

  public boolean suppressSingleChoiceHint() {
    return fSuppressSingleChoiceHint;
  }

  public void setSuppressSingleChoiceHint(boolean suppressSingleChoiceHint) {
    fSuppressSingleChoiceHint = suppressSingleChoiceHint;
  }

  public @Nullable String negativeChoiceContent() {
    return fNegativeChoiceContent;
  }

  public void setNegativeChoiceContent(@Nullable String negativeChoiceContent) {
    fNegativeChoiceContent = negativeChoiceContent;
  }

  public boolean addNegativeChoice() {
    return fAddNegativeChoice;
  }

  public void setAddNegativeChoice(boolean addNegativeChoice) {
    fAddNegativeChoice = addNegativeChoice;
  }
}
