/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.selectQuestion;

import ru.irenproject.Question;
import ru.irenproject.Translate;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.Live;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.SetLocaleIssueOrder;
import ru.irenproject.live.selectQuestion.Proto.Select;
import ru.irenproject.selectQuestion.Choice;
import ru.irenproject.selectQuestion.SelectQuestion;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Singleton public final class SelectQuestionSpawner implements QuestionSpawner {
  private final ThreadLocal<Random> fRandom;

  @Inject SelectQuestionSpawner(@Live ThreadLocal<Random> random) {
    fRandom = random;
  }

  @Override public void prepare(Question source, IssueOrderList orderList) {
    orderList.add(new SelectQuestionIssueOrder());
  }

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    SelectQuestion q = (SelectQuestion) source;
    SelectQuestionIssueOrder order = orderList.getInstance(SelectQuestionIssueOrder.class);

    ArrayList<Integer> choiceIndices = new ArrayList<>();
    if (order.shuffleChoices()) {
      shuffleChoices(q, choiceIndices);
    } else {
      for (int choiceCount = q.choices().size(), i = 0; i < choiceCount; ++i) {
        choiceIndices.add(i);
      }
    }

    Select.Builder b = Select.newBuilder()
        .addAllChoiceIndex(choiceIndices)
        .setMultipleSelection(order.suppressSingleChoiceHint() || !hasSingleCorrectChoice(q));

    boolean hasNegativeChoice = q.hasNegativeChoice();
    boolean synthesizeNegativeChoice = !hasNegativeChoice && order.addNegativeChoice();
    boolean replaceExistingNegativeChoiceContent = hasNegativeChoice && (order.negativeChoiceContent() != null);

    if (synthesizeNegativeChoice || replaceExistingNegativeChoiceContent) {
      b.setCustomNegativeChoice(Select.CustomNegativeChoice.newBuilder()
          .setText(Optional.ofNullable(order.negativeChoiceContent()).orElseGet(
              () -> Translate.message("negativeChoice", orderList.getInstance(SetLocaleIssueOrder.class).locale()))));
    }

    return LiveQuestion.initialize(b.build(), source, orderList);
  }

  private void shuffleChoices(SelectQuestion question, ArrayList<Integer> choiceIndices) {
    List<Choice> choices = question.choices();

    ArrayList<Integer> movableChoiceIndices = new ArrayList<>();
    for (int i = 0; i < choices.size(); ++i) {
      if (!choices.get(i).fixed()) {
        movableChoiceIndices.add(i);
      }
    }

    Collections.shuffle(movableChoiceIndices, fRandom.get());

    int nextMovable = 0;
    for (int i = 0; i < choices.size(); ++i) {
      if (choices.get(i).fixed()) {
        choiceIndices.add(i);
      } else {
        choiceIndices.add(movableChoiceIndices.get(nextMovable));
        ++nextMovable;
      }
    }
  }

  private boolean hasSingleCorrectChoice(SelectQuestion question) {
    boolean res = false;

    for (Choice choice : question.choices()) {
      if (choice.correct()) {
        if (res) {
          res = false;
          break;
        } else {
          res = true;
        }
      }
    }

    return res;
  }
}
