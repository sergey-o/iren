/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.Modifier;
import ru.irenproject.ModifierList;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.QuestionCount;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;

@Singleton public final class QuestionIssuer {
  private final ImmutableMap<Class<? extends Question>, QuestionSpawner> fQuestionSpawners;
  private final ImmutableMap<Class<? extends Modifier>, ModifierApplier> fModifierAppliers;
  private final ThreadLocal<Random> fRandom;

  @Inject QuestionIssuer(
      Map<Class<? extends Question>, QuestionSpawner> questionSpawners,
      Map<Class<? extends Modifier>, ModifierApplier> modifierAppliers,
      @Live ThreadLocal<Random> random) {
    fQuestionSpawners = ImmutableMap.copyOf(questionSpawners);
    fModifierAppliers = ImmutableMap.copyOf(modifierAppliers);
    fRandom = random;
  }

  public _LiveQuestion issue(QuestionItem questionItem, Section section, Profile profile, Locale locale) {
    Question question = questionItem.question();
    QuestionSpawner spawner = Check.notNull(fQuestionSpawners.get(question.getClass()),
        () -> new RuntimeException("No question spawner for " + question.getClass() + "."));

    IssueOrderList issueOrderList = new IssueOrderList();
    issueOrderList.add(new SetLocaleIssueOrder(locale));
    issueOrderList.add(new SetWeightIssueOrder(questionItem.weight()));

    spawner.prepare(question, issueOrderList);
    applyModifiers(questionItem.modifierList(), question, issueOrderList);

    for (Section s = section; s != null; s = s.parent()) {
      applyModifiers(s.modifierList(), question, issueOrderList);
    }

    applyModifiers(profile.modifierList(), question, issueOrderList);

    return spawner.spawn(question, issueOrderList);
  }

  private void applyModifiers(ModifierList modifierList, Question question, IssueOrderList issueOrderList) {
    for (Modifier m : modifierList.modifiers()) {
      ModifierApplier modifierApplier = Check.notNull(fModifierAppliers.get(m.getClass()),
          () -> new RuntimeException("Don't know how to apply modifier " + m.getClass().getName() + "."));
      modifierApplier.apply(m, question, issueOrderList);
    }
  }

  public List<_LiveQuestion> issueFromTest(Test test, Profile profile, Locale locale) {
    //TODO
    if (profile.options().hasLabelFilter()) {
      throw new RuntimeException("Labels are not supported yet.");
    }

    Function<Section, QuestionCount> questionCountsBySection;
    if (profile.sectionProfile() == null) {
      QuestionCount fixedQuestionCount = profile.options().hasQuestionsPerSection() ?
          new QuestionCount(profile.options().getQuestionsPerSection(), false) : QuestionCount.ONE_HUNDRED_PERCENT;
      questionCountsBySection = s -> fixedQuestionCount;
    } else {
      ImmutableMap<Long, QuestionCount> map = profile.sectionProfile().effectiveQuestionCountsBySectionId();
      questionCountsBySection = s -> Check.notNull(map.get(s.id()));
    }

    ArrayList<_LiveQuestion> res = new ArrayList<>();
    issueFromTree(test.root(), profile, questionCountsBySection, locale, res);

    if (profile.options().getShuffleQuestions()) {
      Collections.shuffle(res, fRandom.get());
    }

    return res;
  }

  private void issueFromTree(Section section, Profile profile, Function<Section, QuestionCount> questionCountsBySection,
      Locale locale, List<_LiveQuestion> out) {
    ArrayList<QuestionItem> candidates = section.questionList().items().stream()
        .filter(QuestionItem::enabled)
        .collect(Collectors.toCollection(ArrayList::new));
    int candidateCount = candidates.size();

    int questionsToChoose;
    QuestionCount requestedQuestionCount = questionCountsBySection.apply(section);
    if (requestedQuestionCount.equals(QuestionCount.ONE_HUNDRED_PERCENT)) {
      questionsToChoose = candidateCount;
    } else {
      questionsToChoose = requestedQuestionCount.percent() ?
          computePercentageRoundingUp(candidateCount, requestedQuestionCount.value()) :
          Integer.min(requestedQuestionCount.value(), candidateCount);
      for (int i = candidateCount; i > candidateCount - questionsToChoose; --i) {
        Collections.swap(candidates, i - 1, fRandom.get().nextInt(i));
      }
    }

    for (QuestionItem questionItem : candidates.subList(candidateCount - questionsToChoose, candidateCount)) {
      out.add(issue(questionItem, section, profile, locale));
    }

    for (Section child : section.sections()) {
      issueFromTree(child, profile, questionCountsBySection, locale, out);
    }
  }

  private static int computePercentageRoundingUp(int value, int percent) {
    // assert: value >= 0
    // assert: 0 <= percent <= 100
    return (int) (((long) value * percent + 99) / 100);
  }
}
