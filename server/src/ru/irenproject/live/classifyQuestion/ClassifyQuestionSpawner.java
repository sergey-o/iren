/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.classifyQuestion;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.classifyQuestion.ClassifyQuestion;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.Live;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.classifyQuestion.Proto.Classify;
import ru.irenproject.live.classifyQuestion.Proto.Classify.OfferedItem;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Singleton public final class ClassifyQuestionSpawner implements QuestionSpawner {
  private final ThreadLocal<Random> fRandom;

  @Inject ClassifyQuestionSpawner(@Live ThreadLocal<Random> random) {
    fRandom = random;
  }

  @Override public void prepare(Question source, IssueOrderList orderList) {}

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    ClassifyQuestion q = (ClassifyQuestion) source;
    ArrayList<OfferedItem> offered = new ArrayList<>();

    Integer itemLimit = q.itemLimit();
    if (itemLimit == null) {
      for (int c = 0; c < q.categoryCount(); ++c) {
        for (int itemCount = q.getCategoryItems(c).size(), i = 0; i < itemCount; ++i) {
          offered.add(OfferedItem.newBuilder()
              .setCategoryIndex(c)
              .setItemIndex(i)
              .build());
        }
      }
    } else {
      ArrayList<AvailableCategory> availableCategories = new ArrayList<>();
      for (int c = 0; c < q.categoryCount(); ++c) {
        int itemCount = q.getCategoryItems(c).size();
        if (itemCount > 0) {
          availableCategories.add(new AvailableCategory(c, itemCount));
        }
      }

      // Choose the specified minimum number of items from each category.
      for (int i = availableCategories.size() - 1; i >= 0; --i) {
        chooseItems(availableCategories, i, Check.notNull(q.minItemsPerCategory()), offered);
      }

      // Choose the remaining items from random categories.
      while ((offered.size() < itemLimit) && !availableCategories.isEmpty()) {
        chooseItems(availableCategories, fRandom.get().nextInt(availableCategories.size()), 1, offered);
      }
    }

    Collections.shuffle(offered, fRandom.get());

    Classify m = Classify.newBuilder()
        .addAllOfferedPlacement(offered)
        .build();

    return LiveQuestion.initialize(m, source, orderList);
  }

  private void chooseItems(List<AvailableCategory> availableCategories, int availableCategoryIndex,
      int itemCount, Collection<OfferedItem> out) {
    AvailableCategory ac = availableCategories.get(availableCategoryIndex);

    for (int m = Integer.min(itemCount, ac.fItems.size()), i = 0; i < m; ++i) {
      int n = fRandom.get().nextInt(ac.fItems.size());
      out.add(OfferedItem.newBuilder()
          .setCategoryIndex(ac.fCategoryIndex)
          .setItemIndex(ac.fItems.get(n))
          .build());
      ac.fItems.remove(n);
    }

    if (ac.fItems.isEmpty()) {
      availableCategories.remove(availableCategoryIndex);
    }
  }

  private static final class AvailableCategory {
    final int fCategoryIndex;
    final ArrayList<Integer> fItems = new ArrayList<>();

    AvailableCategory(int categoryIndex, int itemCount) {
      fCategoryIndex = categoryIndex;
      for (int i = 0; i < itemCount; ++i) {
        fItems.add(i);
      }
    }
  }
}
