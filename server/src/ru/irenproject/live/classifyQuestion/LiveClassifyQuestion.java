/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.classifyQuestion;

import ru.irenproject.PlatformUtils;
import ru.irenproject.classifyQuestion.ClassifyQuestion;
import ru.irenproject.common.classifyArea.Proto.ClassifyArea;
import ru.irenproject.common.classifyArea.Proto.ClassifyResponse;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.dialog.Proto.Dialog;
import ru.irenproject.common.dialog.Proto.DialogArea;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.live.ContentPostprocessor;
import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;
import ru.irenproject.live.classifyQuestion.Proto.Classify;
import ru.irenproject.live.classifyQuestion.Proto.Classify.OfferedItem;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@AutoFactory(implementing = LiveQuestion.Factory.class)
public final class LiveClassifyQuestion extends LiveQuestion {
  private final Classify $;

  LiveClassifyQuestion(
      _LiveQuestion message,
      SourceQuestionResolver resolver,
      @Provided ContentTransformer.Factory contentTransformerFactory,
      @Provided ContentPostprocessor contentPostprocessor) {
    super(message, resolver, contentTransformerFactory, contentPostprocessor);
    $ = unpack(Classify.getDefaultInstance());
  }

  @Override public ClassifyQuestion source() {
    return (ClassifyQuestion) super.source();
  }

  @Override public Dialog render() {
    Flow formulation = QuestionUtils.toFlow(source().formulation());
    return Dialog.newBuilder()
        .addDialogArea(QuestionUtils.buildFlowArea(publish(formulation)))
        .addDialogArea(DialogArea.newBuilder()
            .setId(DEFAULT_AREA_ID)
            .setArea(PlatformUtils.toAny(buildClassifyArea())))
        .build();
  }

  private ClassifyArea buildClassifyArea() {
    ClassifyArea.Builder b = ClassifyArea.newBuilder();

    for (int categoryCount = source().categoryCount(), i = 0; i < categoryCount; ++i) {
      b.addCategoryTitle(publish(QuestionUtils.toFlow(source().getCategoryTitle(i))));
    }

    for (OfferedItem offeredItem : $.getOfferedPlacementList()) {
      b.addOffered(publish(QuestionUtils.toFlow(source()
          .getCategoryItems(offeredItem.getCategoryIndex())
          .get(offeredItem.getItemIndex()))));
    }

    return b.build();
  }

  @Override public DialogResponse getEmptyResponse() {
    return buildResponse(Collections.nCopies($.getOfferedPlacementCount(), -1));
  }

  private static DialogResponse buildResponse(Iterable<Integer> mapping) {
    return buildSimpleDialogResponse(ClassifyResponse.newBuilder()
        .addAllMapping(mapping)
        .build());
  }

  @Override public DialogResponse getCorrectResponse() {
    return buildResponse(getCorrectMapping());
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    ClassifyResponse r = getSingleResponse(response, ClassifyResponse.getDefaultInstance());
    check(r);

    List<Integer> answer = r.getMappingList();
    BigDecimal res;

    switch (getEvaluationModelOrDefault(EvaluationModelType.DICHOTOMIC)) {
      case DICHOTOMIC: {
        res = getCorrectMapping().equals(answer) ? BigDecimal.ONE : BigDecimal.ZERO;
        break;
      }
      case LAX: {
        res = QuestionUtils.evaluateMappingLaxly(answer, getCorrectMapping());
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }

    return res;
  }

  private void check(ClassifyResponse response) {
    checkResponse(response.getMappingCount() == $.getOfferedPlacementCount());
    int categoryCount = source().categoryCount();

    for (int categoryIndex : response.getMappingList()) {
      checkResponse((categoryIndex >= -1) && (categoryIndex < categoryCount));
    }
  }

  private List<Integer> getCorrectMapping() {
    return $.getOfferedPlacementList().stream()
        .map(OfferedItem::getCategoryIndex)
        .collect(Collectors.toList());
  }
}
