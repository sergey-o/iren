/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.classifyQuestion;

import ru.irenproject.common.classifyArea.Proto.ClassifyResponse;
import ru.irenproject.live.classifyQuestion.Proto.Classify;

import com.google.protobuf.Descriptors.Descriptor;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

@Module(includes = LiveClassifyQuestionBaseModule.class)
public final class LiveClassifyQuestionModule {
  @Provides @IntoSet static Descriptor provideDescriptor() {
    return Classify.getDescriptor();
  }

  @Provides @IntoSet static Descriptor provideResponseDescriptor() {
    return ClassifyResponse.getDescriptor();
  }
}
