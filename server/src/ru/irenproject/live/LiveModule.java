/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Modifier;
import ru.irenproject.addNegativeChoiceModifier.AddNegativeChoiceModifier;
import ru.irenproject.formula.CachingFormulaRenderer;
import ru.irenproject.formula.FormulaRendererLike;
import ru.irenproject.live.classifyQuestion.LiveClassifyQuestionModule;
import ru.irenproject.live.inputQuestion.LiveInputQuestionModule;
import ru.irenproject.live.matchQuestion.LiveMatchQuestionModule;
import ru.irenproject.live.orderQuestion.LiveOrderQuestionModule;
import ru.irenproject.live.selectQuestion.LiveSelectQuestionModule;
import ru.irenproject.script.isolated.IsolatedScriptRunner;
import ru.irenproject.scriptModifier.ScriptModifier;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifier;
import ru.irenproject.setNegativeChoiceContentModifier.SetNegativeChoiceContentModifier;
import ru.irenproject.setWeightModifier.SetWeightModifier;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifier;
import ru.irenproject.suppressSingleChoiceHintModifier.SuppressSingleChoiceHintModifier;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.daggeradapter.DaggerAdapter;
import com.google.inject.multibindings.MapBinder;

import java.security.SecureRandom;
import java.util.Random;

public final class LiveModule extends AbstractModule {
  @Override protected void configure() {
    registerLiveQuestions();
    registerModifierAppliers();

    bind(new TypeLiteral<ThreadLocal<Random>>() {}).annotatedWith(Live.class)
        .toInstance(ThreadLocal.withInitial(SecureRandom::new));

    bind(ContentTransformer.Factory.class).to(DefaultContentTransformerFactory.class);
    bind(IsolatedScriptRunner.class).asEagerSingleton();

    bind(FormulaRendererLike.class).to(CachingFormulaRenderer.class);
    bind(CachingFormulaRenderer.class).asEagerSingleton();
  }

  private void registerLiveQuestions() {
    install(DaggerAdapter.from(
        LiveSelectQuestionModule.class,
        LiveInputQuestionModule.class,
        LiveMatchQuestionModule.class,
        LiveOrderQuestionModule.class,
        LiveClassifyQuestionModule.class));
  }

  private void registerModifierAppliers() {
    MapBinder<Class<? extends Modifier>, ModifierApplier> b = MapBinder.newMapBinder(binder(),
        new TypeLiteral<>() {}, new TypeLiteral<>() {});
    b.addBinding(AddNegativeChoiceModifier.class).to(AddNegativeChoiceModifierApplier.class);
    b.addBinding(ScriptModifier.class).to(ScriptModifierApplier.class);
    b.addBinding(SetEvaluationModelModifier.class).to(SetEvaluationModelModifierApplier.class);
    b.addBinding(SetNegativeChoiceContentModifier.class).to(SetNegativeChoiceContentModifierApplier.class);
    b.addBinding(SetWeightModifier.class).to(SetWeightModifierApplier.class);
    b.addBinding(ShuffleChoicesModifier.class).to(ShuffleChoicesModifierApplier.class);
    b.addBinding(SuppressSingleChoiceHintModifier.class).to(SuppressSingleChoiceHintModifierApplier.class);
  }
}
