/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.dialog.Proto.Dialog;
import ru.irenproject.common.dialog.Proto.DialogArea;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.inputArea.Proto.InputArea;
import ru.irenproject.common.inputArea.Proto.InputResponse;
import ru.irenproject.common.test.Proto.EvaluationModelType;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.live.ContentPostprocessor;
import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionUtils;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;

import java.math.BigDecimal;
import java.util.Map;

@AutoFactory(implementing = LiveQuestion.Factory.class)
public final class LiveInputQuestion extends LiveQuestion {
  public static final int MAX_RESPONSE_SIZE = Pattern.MAX_LENGTH;

  private static final DialogResponse EMPTY_RESPONSE = buildResponse("");

  private static DialogResponse buildResponse(String input) {
    return buildSimpleDialogResponse(InputResponse.newBuilder()
        .setInput(input)
        .build());
  }

  private final Map<Class<? extends Pattern>, PatternMatcher> fPatternMatchers;

  LiveInputQuestion(
      _LiveQuestion message,
      SourceQuestionResolver resolver,
      @Provided ContentTransformer.Factory contentTransformerFactory,
      @Provided ContentPostprocessor contentPostprocessor,
      @Provided Map<Class<? extends Pattern>, PatternMatcher> patternMatchers) {
    super(message, resolver, contentTransformerFactory, contentPostprocessor);
    fPatternMatchers = patternMatchers;
  }

  @Override public Dialog render() {
    Flow formulation = QuestionUtils.toFlow(source().formulation());
    return Dialog.newBuilder()
        .addDialogArea(QuestionUtils.buildFlowArea(publish(formulation)))
        .addDialogArea(DialogArea.newBuilder()
            .setId(DEFAULT_AREA_ID)
            .setArea(PlatformUtils.toAny(InputArea.newBuilder()
                .setMaxSize(MAX_RESPONSE_SIZE)
                .build())))
        .build();
  }

  @Override public InputQuestion source() {
    return (InputQuestion) super.source();
  }

  @Override public DialogResponse getEmptyResponse() {
    return EMPTY_RESPONSE;
  }

  @Override public DialogResponse getCorrectResponse() {
    String correct = source().patterns().isEmpty() ?
        "" : contentTransformer().transformText(source().patterns().get(0).value());
    return buildResponse(correct);
  }

  @Override public BigDecimal evaluate(DialogResponse response) {
    String input = getSingleResponse(response, InputResponse.getDefaultInstance()).getInput();
    checkResponse(input.length() <= MAX_RESPONSE_SIZE);

    BigDecimal res = BigDecimal.ZERO;

    for (Pattern p : source().patterns()) {
      PatternMatcher pm = Check.notNull(fPatternMatchers.get(p.getClass()),
          () -> new RuntimeException("No pattern matcher for " + p.getClass() + "."));
      res = pm.match(p, contentTransformer(), input);
      if (res.signum() > 0) {
        break;
      }
    }

    switch (getEvaluationModelOrDefault(EvaluationModelType.LAX)) {
      case DICHOTOMIC: {
        if (res.compareTo(BigDecimal.ONE) < 0) {
          res = BigDecimal.ZERO;
        }
        break;
      }
      case LAX: {
        break; // do nothing
      }
    }

    return res;
  }
}
