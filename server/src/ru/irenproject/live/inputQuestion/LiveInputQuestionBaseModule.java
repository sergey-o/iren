/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.QuestionClassKey;
import ru.irenproject.inputQuestion.InputQuestion;
import ru.irenproject.inputQuestion.PatternClassKey;
import ru.irenproject.inputQuestion.RegexpPattern;
import ru.irenproject.inputQuestion.TextPattern;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.inputQuestion.Proto.Input;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;

@Module public final class LiveInputQuestionBaseModule {
  @QuestionClassKey(InputQuestion.class)
  @Provides @IntoMap static QuestionSpawner provideSpawner(InputQuestionSpawner spawner) {
    return spawner;
  }

  /** {@link Input} */
  @StringKey("/ru.irenproject.Input")
  @Provides @IntoMap static LiveQuestion.Factory provideFactory(LiveInputQuestionFactory factory) {
    return factory;
  }

  @PatternClassKey(TextPattern.class)
  @Provides @IntoMap static PatternMatcher provideTextPatternMatcher(TextPatternMatcher matcher) {
    return matcher;
  }

  @PatternClassKey(RegexpPattern.class)
  @Provides @IntoMap static PatternMatcher provideRegexpPatternMatcher(RegexpPatternMatcher matcher) {
    return matcher;
  }
}
