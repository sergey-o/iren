/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.BaseUtils;
import ru.irenproject.inputQuestion.Pattern;
import ru.irenproject.inputQuestion.TextPattern;
import ru.irenproject.live.ContentTransformer;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOCase;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.ArrayList;

@Singleton public final class TextPatternMatcher implements PatternMatcher {
  private static final Splitter WORD_SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();
  private static final CharMatcher NUMBER_COMPONENT_MATCHER = BaseUtils.javaDigitCharMatcher()
      .or(CharMatcher.anyOf("+-."));
  private static final CharMatcher COMMA_MATCHER = CharMatcher.is(',');

  @Inject TextPatternMatcher() {}

  @Override public BigDecimal match(Pattern pattern, ContentTransformer contentTransformer, String input) {
    TextPattern p = (TextPattern) pattern;
    String correct = contentTransformer.transformText(p.value());

    boolean matches = p.spaceDelimited() ?
        matchWords(WORD_SPLITTER.split(input), WORD_SPLITTER.split(correct), p) :
        matchWord(CharMatcher.whitespace().removeFrom(input), CharMatcher.whitespace().removeFrom(correct), p);

    return matches ? BigDecimal.valueOf(p.qualityPercent(), 2) : BigDecimal.ZERO;
  }

  private static boolean matchWords(Iterable<String> inputWords, Iterable<String> correctWords, TextPattern pattern) {
    ArrayList<String> inputList = Lists.newArrayList(inputWords);
    ArrayList<String> correctList = Lists.newArrayList(correctWords);
    int size = inputList.size();
    boolean res = (size == correctList.size());

    if (res) {
      for (int i = 0; i < size; ++i) {
        if (!matchWord(inputList.get(i), correctList.get(i), pattern)) {
          res = false;
          break;
        }
      }
    }

    return res;
  }

  private static boolean matchWord(String input, String correct, TextPattern pattern) {
    boolean res;
    BigDecimal inputNumber, correctNumber;

    BigDecimal precision = pattern.precision();
    if ((precision != null)
        && ((inputNumber = tryParseNumber(input)) != null)
        && ((correctNumber = tryParseNumber(correct)) != null)) {
      res = inputNumber.subtract(correctNumber).abs().compareTo(precision) <= 0;
    } else if (pattern.wildcard()) {
      res = FilenameUtils.wildcardMatch(input, correct,
          pattern.caseSensitive() ? IOCase.SENSITIVE : IOCase.INSENSITIVE);
    } else {
      res = pattern.caseSensitive() ? input.equals(correct) : input.equalsIgnoreCase(correct);
    }

    return res;
  }

  private static @Nullable BigDecimal tryParseNumber(String text) {
    BigDecimal res;

    String s = COMMA_MATCHER.replaceFrom(text, '.');
    boolean marginalDot = s.startsWith(".") || s.startsWith("+.") || s.startsWith("-.") || s.endsWith(".");
    if (!marginalDot && NUMBER_COMPONENT_MATCHER.matchesAllOf(s)) {
      try {
        res = new BigDecimal(s);
      } catch (NumberFormatException ignored) {
        res = null;
      }
    } else {
      res = null;
    }

    return res;
  }
}
