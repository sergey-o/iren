/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.inputQuestion;

import ru.irenproject.common.inputArea.Proto.InputResponse;
import ru.irenproject.live.inputQuestion.Proto.Input;

import com.google.protobuf.Descriptors.Descriptor;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoSet;

@Module(includes = LiveInputQuestionBaseModule.class)
public final class LiveInputQuestionModule {
  @Provides @IntoSet static Descriptor provideDescriptor() {
    return Input.getDescriptor();
  }

  @Provides @IntoSet static Descriptor provideResponseDescriptor() {
    return InputResponse.getDescriptor();
  }
}
