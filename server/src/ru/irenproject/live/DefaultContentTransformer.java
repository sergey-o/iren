/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Utils;
import ru.irenproject.live.Proto._ContentTransformer;

import com.google.auto.factory.AutoFactory;

import javax.annotation.Nullable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@AutoFactory(implementing = ContentTransformer.Factory.class)
public final class DefaultContentTransformer extends ContentTransformer {
  private final List<Replacement> fReplacements;

  DefaultContentTransformer(_ContentTransformer message) {
    fReplacements = message.getTextReplacementList().stream()
        .map(r -> new Replacement(
            Pattern.compile(r.getPattern(), Pattern.LITERAL | Pattern.CASE_INSENSITIVE),
            Matcher.quoteReplacement(r.getReplacement())))
        .collect(Collectors.toList());
  }

  private static final class Replacement {
    final Pattern pattern;
    final String replacement;

    Replacement(Pattern pattern, String replacement) {
      this.pattern = pattern;
      this.replacement = replacement;
    }
  }

  @Override protected int replacementCount() {
    return fReplacements.size();
  }

  @Override protected @Nullable String replaceAllIfFound(String source, int replacementIndex) {
    Replacement r = fReplacements.get(replacementIndex);
    return Utils.replaceAllIfFound(source, r.pattern, r.replacement);
  }
}
