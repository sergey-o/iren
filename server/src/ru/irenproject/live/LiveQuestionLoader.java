/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.Check;
import ru.irenproject.live.LiveQuestion.SourceQuestionResolver;
import ru.irenproject.live.Proto._LiveQuestion;

import com.google.common.collect.ImmutableMap;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Map;

@Singleton public final class LiveQuestionLoader {
  private final ImmutableMap<String, LiveQuestion.Factory> fFactoriesByTypeUrl;

  @Inject LiveQuestionLoader(Map<String, LiveQuestion.Factory> factoriesByTypeUrl) {
    fFactoriesByTypeUrl = ImmutableMap.copyOf(factoriesByTypeUrl);
  }

  public LiveQuestion load(_LiveQuestion message, SourceQuestionResolver resolver) {
    String typeUrl = message.getData().getTypeUrl();
    LiveQuestion.Factory factory = Check.notNull(fFactoriesByTypeUrl.get(typeUrl),
        () -> new BadLiveQuestionException("Unknown question type: '" + typeUrl + "'."));
    return factory.create(message, resolver);
  }
}
