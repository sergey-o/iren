/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live.orderQuestion;

import ru.irenproject.Question;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.Live;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionSpawner;
import ru.irenproject.live.orderQuestion.Proto.Order;
import ru.irenproject.orderQuestion.OrderQuestion;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

@Singleton public final class OrderQuestionSpawner implements QuestionSpawner {
  private final ThreadLocal<Random> fRandom;

  @Inject OrderQuestionSpawner(@Live ThreadLocal<Random> random) {
    fRandom = random;
  }

  @Override public void prepare(Question source, IssueOrderList orderList) {}

  @Override public _LiveQuestion spawn(Question source, IssueOrderList orderList) {
    OrderQuestion q = (OrderQuestion) source;
    int sourceItemCount = q.items().size();
    int sourceDistractorCount = q.distractors().size();

    ArrayList<Integer> correct = new ArrayList<>();
    for (int i = 0; i < sourceItemCount; ++i) {
      correct.add(i);
    }

    Integer itemLimit = q.itemLimit();
    if (itemLimit != null) {
      for (int itemsUsed = Integer.min(itemLimit, sourceItemCount),
          excessItems = sourceItemCount - itemsUsed,
          i = 0; i < excessItems; ++i) {
        correct.remove(fRandom.get().nextInt(correct.size()));
      }
    }

    ArrayList<Integer> offered = new ArrayList<>();
    for (int i = 0; i < sourceDistractorCount; ++i) {
      offered.add(i + sourceItemCount);
    }

    Integer distractorLimit = q.distractorLimit();
    if (distractorLimit != null) {
      for (int distractorsUsed = Integer.min(distractorLimit, sourceDistractorCount),
          excessDistractors = sourceDistractorCount - distractorsUsed,
          i = 0; i < excessDistractors; ++i) {
        offered.remove(fRandom.get().nextInt(offered.size()));
      }
    }

    offered.addAll(correct);

    Collections.shuffle(offered, fRandom.get());

    Order m = Order.newBuilder()
        .addAllCorrectPlacement(correct)
        .addAllOfferedPlacement(offered)
        .build();

    return LiveQuestion.initialize(m, source, orderList);
  }
}
