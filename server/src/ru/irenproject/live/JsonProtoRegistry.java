package ru.irenproject.live;

import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.util.JsonFormat.TypeRegistry;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Set;

@Singleton public final class JsonProtoRegistry {
  private final TypeRegistry fRegistry;

  @Inject private JsonProtoRegistry(Set<Descriptor> descriptors) {
    fRegistry = TypeRegistry.newBuilder().add(descriptors).build();
  }

  public TypeRegistry get() {
    return fRegistry;
  }
}
