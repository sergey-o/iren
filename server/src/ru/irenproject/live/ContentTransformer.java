/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.live;

import ru.irenproject.PlatformUtils;
import ru.irenproject.common.content.Proto.Block;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.live.Proto._ContentTransformer;
import ru.irenproject.live.Proto._TextReplacement;

import com.google.common.base.MoreObjects;

import javax.annotation.Nullable;
import java.util.Map;

public abstract class ContentTransformer {
  public interface Factory {
    ContentTransformer create(_ContentTransformer message);
  }

  public static final ContentTransformer IDENTITY = new IdentityContentTransformer();

  public static @Nullable _ContentTransformer buildIfNotEmpty(IssueOrderList issueOrders) {
    _ContentTransformer.Builder b = _ContentTransformer.newBuilder();

    for (IssueOrder order : issueOrders.items()) {
      if (order instanceof ReplaceTextIssueOrder) {
        ReplaceTextIssueOrder rt = (ReplaceTextIssueOrder) order;

        for (Map.Entry<String, String> e : rt.map().entrySet()) {
          b.addTextReplacement(_TextReplacement.newBuilder()
              .setPattern(e.getKey())
              .setReplacement(e.getValue()));
        }
      }
    }

    return (b.getTextReplacementCount() > 0) ? b.build() : null;
  }

  protected ContentTransformer() {}

  protected abstract int replacementCount();

  protected abstract @Nullable String replaceAllIfFound(String source, int replacementIndex);

  public final Flow transformFlow(Flow flow) {
    Flow res;

    int replacementCount = replacementCount();
    if (replacementCount == 0) {
      res = flow;
    } else {
      Flow.Builder b = flow.toBuilder();
      for (int i = 0; i < replacementCount; ++i) {
        replaceInFlow(b, i);
      }
      res = b.buildPartial();
    }

    return res;
  }

  private void replaceInFlow(Flow.Builder flow, int replacementIndex) {
    int i = 0;
    while (i < flow.getBlockCount()) {
      Block block = flow.getBlock(i);
      switch (block.getType()) {
        case TEXT: {
          String transformed = replaceAllIfFound(block.getTextBlock().getText(), replacementIndex);
          if (transformed == null) {
            ++i;
          } else if (transformed.isEmpty()) {
            PlatformUtils.removeFlowBlock(flow, i);
          } else {
            // cannot use `flow.getBlockBuilder(i).getTextBlockBuilder().setText(transformed)` in J2CL
            flow.setBlock(i, block.toBuilder()
                .setTextBlock(block.getTextBlock().toBuilder()
                    .setText(transformed)));
            ++i;
          }
          break;
        }
        case FORMULA: {
          String transformed = replaceAllIfFound(block.getFormulaBlock().getSource(), replacementIndex);
          if (transformed != null) {
            // cannot use `flow.getBlockBuilder(i).getFormulaBlockBuilder().setSource(transformed)` in J2CL
            flow.setBlock(i, block.toBuilder()
                .setFormulaBlock(block.getFormulaBlock().toBuilder()
                    .setSource(transformed)));
          }
          ++i;
          break;
        }
        case IMAGE:
        case LINE_FEED: {
          ++i;
          break;
        }
        default: {
          throw new RuntimeException();
        }
      }
    }
  }

  public final String transformText(String text) {
    String res = text;
    for (int replacementCount = replacementCount(), i = 0; i < replacementCount; ++i) {
      res = MoreObjects.firstNonNull(replaceAllIfFound(res, i), res);
    }
    return res;
  }

  private static final class IdentityContentTransformer extends ContentTransformer {
    @Override protected int replacementCount() {
      return 0;
    }

    @Override protected @Nullable String replaceAllIfFound(String source, int replacementIndex) {
      return null;
    }
  }
}
