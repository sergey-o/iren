/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.Question;
import ru.irenproject.common.inputQuestion.Proto.SpacePreprocessMode;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.itx.QuestionWriter;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Locale;

@Singleton public final class InputQuestionWriter implements QuestionWriter {
  @Inject private InputQuestionWriter() {}

  @Override public void write(Question question, ItxWriter out) {
    InputQuestion q = (InputQuestion) question;
    out.writePad(q.formulation());

    if (!q.patterns().isEmpty()) {
      out.begin(Itx.ELEM_PATTERNS);
      for (Pattern p : q.patterns()) {
        out.begin(Itx.ELEM_PATTERN);
        out.add(Itx.ATTR_TYPE, (p instanceof TextPattern) ?
            Itx.VAL_PATTERN_TYPE_TEXT : Itx.VAL_PATTERN_TYPE_REGEXP);
        out.add(Itx.ATTR_VALUE, p.value());
        out.add(Itx.ATTR_QUALITY, String.format(Locale.ENGLISH, "%.4f",
            BigDecimal.valueOf(p.qualityPercent()).scaleByPowerOfTen(-2)));

        if (p instanceof TextPattern) {
          TextPattern tp = (TextPattern) p;
          out.addBoolean(Itx.ATTR_CASE_SENSITIVE, tp.caseSensitive());
          out.add(Itx.ATTR_SPACES, tp.spaceDelimited() ?
              Itx.VAL_PATTERN_SPACES_NORMALIZE : Itx.VAL_PATTERN_SPACES_IGNORE);
          out.addBoolean(Itx.ATTR_NUMERIC_AWARE, tp.precision() != null);
          if (tp.precision() != null) {
            out.add(Itx.ATTR_PRECISION, tp.precisionAsString());
          }
          out.addBoolean(Itx.ATTR_WILDCARD, tp.wildcard());
        } else if (p instanceof RegexpPattern) {
          RegexpPattern rp = (RegexpPattern) p;
          out.addBoolean(Itx.ATTR_CASE_SENSITIVE, rp.caseSensitive());
          out.add(Itx.ATTR_SPACES, spacePreprocessModeToXml(rp.spacePreprocessMode()));
        } else {
          throw new RuntimeException();
        }

        out.end();
      }
      out.end();
    }
  }

  private static String spacePreprocessModeToXml(SpacePreprocessMode v) {
    switch (v) {
      case KEEP: return Itx.VAL_PATTERN_SPACES_EXACT;
      case NORMALIZE: return Itx.VAL_PATTERN_SPACES_NORMALIZE;
      case REMOVE: return Itx.VAL_PATTERN_SPACES_IGNORE;
      default: throw new RuntimeException();
    }
  }
}
