/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.common.inputQuestion.Proto.SpacePreprocessMode;
import ru.irenproject.infra.Realm;

public final class RegexpPattern extends Pattern {
  private boolean fCaseSensitive;
  private SpacePreprocessMode fSpacePreprocessMode = SpacePreprocessMode.NORMALIZE;

  public RegexpPattern(Realm realm) {
    super(realm);
  }

  @Override public boolean hasDefaultOptions() {
    return !fCaseSensitive && (fSpacePreprocessMode == SpacePreprocessMode.NORMALIZE);
  }

  public boolean caseSensitive() {
    return fCaseSensitive;
  }

  public void setCaseSensitive(boolean caseSensitive) {
    fCaseSensitive = caseSensitive;
    post(ChangeEvent.INSTANCE);
  }

  public SpacePreprocessMode spacePreprocessMode() {
    return fSpacePreprocessMode;
  }

  public void setSpacePreprocessMode(SpacePreprocessMode mode) {
    fSpacePreprocessMode = mode;
    post(ChangeEvent.INSTANCE);
  }

  @Override public void setOptionsFrom(Pattern source) {
    if (source instanceof RegexpPattern) {
      RegexpPattern p = (RegexpPattern) source;

      setCaseSensitive(p.fCaseSensitive);
      setSpacePreprocessMode(p.fSpacePreprocessMode);
    }
  }
}
