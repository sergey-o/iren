/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.inputQuestion;

import ru.irenproject.BaseUtils;
import ru.irenproject.TestCheck;
import ru.irenproject.TestInputException;
import ru.irenproject.infra.Realm;

import com.google.common.base.CharMatcher;

import javax.annotation.Nullable;
import java.math.BigDecimal;

public final class TextPattern extends Pattern {
  public static final int MAX_PRECISION_LENGTH = 10;

  private static final CharMatcher MATCHER = BaseUtils.javaDigitCharMatcher().or(CharMatcher.is('.'));

  private boolean fCaseSensitive;
  private boolean fSpaceDelimited = true;
  private boolean fWildcard = true;
  private @Nullable BigDecimal fPrecision = BigDecimal.ZERO;

  public TextPattern(Realm realm) {
    super(realm);
  }

  @Override public boolean hasDefaultOptions() {
    return !fCaseSensitive && fSpaceDelimited && fWildcard && (fPrecision != null) && (fPrecision.signum() == 0);
  }

  public boolean caseSensitive() {
    return fCaseSensitive;
  }

  public void setCaseSensitive(boolean caseSensitive) {
    fCaseSensitive = caseSensitive;
    post(ChangeEvent.INSTANCE);
  }

  public boolean spaceDelimited() {
    return fSpaceDelimited;
  }

  public void setSpaceDelimited(boolean spaceDelimited) {
    fSpaceDelimited = spaceDelimited;
    post(ChangeEvent.INSTANCE);
  }

  public boolean wildcard() {
    return fWildcard;
  }

  public void setWildcard(boolean wildcard) {
    fWildcard = wildcard;
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable BigDecimal precision() {
    return fPrecision;
  }

  private void setPrecision(@Nullable BigDecimal precision) {
    fPrecision = precision;
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable String precisionAsString() {
    return (fPrecision == null) ? null : fPrecision.toPlainString();
  }

  public void setPrecisionAsString(@Nullable String precision) {
    BigDecimal value;

    if (precision == null) {
      value = null;
    } else {
      TestCheck.input(precision.length() <= MAX_PRECISION_LENGTH);
      TestCheck.input(MATCHER.matchesAllOf(precision));

      BigDecimal p;
      try {
        p = new BigDecimal(precision);
      } catch (NumberFormatException e) {
        throw new TestInputException(e);
      }

      value = p.stripTrailingZeros();
    }

    setPrecision(value);
  }

  @Override public void setOptionsFrom(Pattern source) {
    if (source instanceof TextPattern) {
      TextPattern p = (TextPattern) source;

      setCaseSensitive(p.fCaseSensitive);
      setSpaceDelimited(p.fSpaceDelimited);
      setWildcard(p.fWildcard);
      setPrecision(p.fPrecision);
    }
  }
}
