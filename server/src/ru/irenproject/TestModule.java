/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.addNegativeChoiceModifier.AddNegativeChoiceModifierModule;
import ru.irenproject.classifyQuestion.ClassifyQuestionModule;
import ru.irenproject.inputQuestion.InputQuestionModule;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.matchQuestion.MatchQuestionModule;
import ru.irenproject.orderQuestion.OrderQuestionModule;
import ru.irenproject.scriptModifier.ScriptModifierModule;
import ru.irenproject.selectQuestion.SelectQuestionModule;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifierModule;
import ru.irenproject.setNegativeChoiceContentModifier.SetNegativeChoiceContentModifierModule;
import ru.irenproject.setWeightModifier.SetWeightModifierModule;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifierModule;
import ru.irenproject.suppressSingleChoiceHintModifier.SuppressSingleChoiceHintModifierModule;

import com.google.inject.AbstractModule;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.daggeradapter.DaggerAdapter;

public final class TestModule extends AbstractModule {
  @Override protected void configure() {
    install(new FactoryModuleBuilder().build(ItxWriter.Factory.class));

    install(DaggerAdapter.from(
        SelectQuestionModule.class,
        InputQuestionModule.class,
        MatchQuestionModule.class,
        OrderQuestionModule.class,
        ClassifyQuestionModule.class,
        AddNegativeChoiceModifierModule.class,
        ScriptModifierModule.class,
        SetEvaluationModelModifierModule.class,
        SetNegativeChoiceContentModifierModule.class,
        SetWeightModifierModule.class,
        ShuffleChoicesModifierModule.class,
        SuppressSingleChoiceHintModifierModule.class));
  }
}
