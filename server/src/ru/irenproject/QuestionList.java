/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

public final class QuestionList extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public static final class RemoveItemEvent implements Event {
    private final QuestionItem fItem;

    private RemoveItemEvent(QuestionItem item) {
      fItem = item;
    }

    public QuestionItem item() {
      return fItem;
    }
  }

  private final ArrayList<QuestionItem> fItems = new ArrayList<>();

  public QuestionList(Realm realm) {
    super(realm);
  }

  public List<QuestionItem> items() {
    return Collections.unmodifiableList(fItems);
  }

  public LinkedHashMap<QuestionItem, Integer> itemsWithIndices() {
    LinkedHashMap<QuestionItem, Integer> res = new LinkedHashMap<>();
    for (int i = 0; i < fItems.size(); ++i) {
      res.put(fItems.get(i), i);
    }
    return res;
  }

  public void addItem(QuestionItem item) {
    fItems.add(item);
    post(ChangeEvent.INSTANCE);
  }

  public boolean isEmpty() {
    return fItems.isEmpty();
  }

  public void removeItems(Collection<QuestionItem> items) {
    fItems.removeAll(items);

    for (QuestionItem item : items) {
      post(new RemoveItemEvent(item));
    }

    post(ChangeEvent.INSTANCE);
  }

  public void clear() {
    for (QuestionItem item : fItems) {
      post(new RemoveItemEvent(item));
    }
    fItems.clear();
    post(ChangeEvent.INSTANCE);
  }

  public void moveItem(int index, boolean forward) {
    checkItemIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkItemIndex(newIndex);

    Collections.swap(fItems, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  private void checkItemIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fItems.size());
  }
}
