/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.pad.ImageBlock;

public final class ExternallyStoredObjectReader implements ObjectReader {
  @Override public ImageBlock readImage(ItxImage image, BlobStore blobStore) {
    BlobId dataBlobId = Check.inputNotNull(BlobId.tryFromHex(image.src()));

    Check.input(Itx.IMAGE_EXTENSIONS_BY_MIME_TYPE.containsKey(image.type()));
    Check.input(image.clipboardFormat() == null);

    return new ImageBlock(image.type(), dataBlobId, null);
  }
}
