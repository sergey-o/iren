/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.ModifierList;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.common.profile.Proto.AvailableQuestionScore;
import ru.irenproject.common.profile.Proto.AvailableScore;
import ru.irenproject.common.profile.Proto.AvailableSectionScore;
import ru.irenproject.common.profile.Proto.Mark;
import ru.irenproject.common.profile.Proto.MarkScaleData;
import ru.irenproject.common.profile.Proto.ProfileOptions;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.FormulaBlock;
import ru.irenproject.pad.Pad;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.profile.Profile;
import ru.irenproject.profile.ProfileList;
import ru.irenproject.profile.QuestionCount;
import ru.irenproject.profile.SectionProfile;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.google.common.primitives.Longs;
import org.w3c.dom.Element;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@AutoFactory public final class ItxReader {
  private final Map<String, QuestionReader> fQuestionReaders;
  private final Map<String, ModifierReader> fModifierReaders;
  private final ObjectReader fObjectReader;
  private final Element fRoot;
  private final Realm fRealm;

  ItxReader(
      ObjectReader objectReader,
      Element root,
      Realm realm,
      @Provided Map<String, QuestionReader> questionReaders,
      @Provided Map<String, ModifierReader> modifierReaders) {
    fQuestionReaders = questionReaders;
    fModifierReaders = modifierReaders;
    fObjectReader = objectReader;
    fRoot = root;
    fRealm = realm;
  }

  public Element root() {
    return fRoot;
  }

  public Realm realm() {
    return fRealm;
  }

  public Test readTest(Element e) {
    Test res = new Test(fRealm);
    readSection(Itx.getChild(e, Itx.ELEM_SECTION), res.root());

    Element xmlProfiles = Itx.getChildIfExists(e, Itx.ELEM_PROFILES);
    if (xmlProfiles != null) {
      readUnlinkedProfiles(xmlProfiles, res.profileList());
    }

    Element xmlTestInfo = Itx.getChildIfExists(e, Itx.ELEM_TEST_INFO);
    if (xmlTestInfo != null) {
      readTestInfo(xmlTestInfo, res);
    }

    for (Profile profile : res.profileList().profiles()) {
      linkProfile(profile, res);
    }

    return res;
  }

  private void linkProfile(Profile profile, Test referenceTest) {
    if (profile.sectionProfile() != null) {
      profile.setSectionProfile(linkSectionProfile(profile.sectionProfile(), referenceTest.root()));
    }
  }

  private void readSection(Element e, Section out) {
    out.setName(Itx.find(e, Itx.ATTR_TITLE));

    Element xmlQuestions = Itx.getChildIfExists(e, Itx.ELEM_QUESTIONS);
    if (xmlQuestions != null) {
      readQuestionList(xmlQuestions, out);
    }

    Element xmlSections = Itx.getChildIfExists(e, Itx.ELEM_SECTIONS);
    if (xmlSections != null) {
      readSections(xmlSections, out);
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, out.modifierList());
    }
  }

  private void readQuestionList(Element e, Section out) {
    for (Element xmlQuestion : Itx.getChildren(e, Itx.ELEM_QUESTION)) {
      out.questionList().addItem(readQuestionItem(xmlQuestion));
    }
  }

  private void readSections(Element e, Section out) {
    for (Element xmlSection : Itx.getChildren(e, Itx.ELEM_SECTION)) {
      Section child = new Section(fRealm);
      readSection(xmlSection, child);
      out.addSection(child);
    }
  }

  public QuestionItem readQuestionItem(Element e) {
    QuestionItem res = new QuestionItem(readQuestion(e));

    if (e.hasAttribute(Itx.ATTR_TAG)) {
      res.setTag(e.getAttribute(Itx.ATTR_TAG));
    } else if (e.hasAttribute(Itx.ATTR_ID)) { // .it2
      Long id = Longs.tryParse(e.getAttribute(Itx.ATTR_ID));
      Check.that((id != null) && (id >= 1), ItxInputException::new);
      res.setTag(id.toString());
    }

    res.setWeight(Itx.findInteger(e, Itx.ATTR_WEIGHT));
    res.setEnabled(Itx.findBoolean(e, Itx.ATTR_ENABLED));

    Element xmlLabels = Itx.getChildIfExists(e, Itx.ELEM_LABELS);
    if (xmlLabels != null) {
      for (Element xmlLabel : Itx.getChildren(xmlLabels, Itx.ELEM_LABEL)) {
        res.addLabel(Itx.find(xmlLabel, Itx.ATTR_VALUE));
      }
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, res.modifierList());
    }

    return res;
  }

  public Question readQuestion(Element e) {
    QuestionReader reader = Check.notNull(fQuestionReaders.get(Itx.find(e, Itx.ATTR_TYPE)), ItxInputException::new);
    return reader.read(this, e);
  }

  public void readPad(Element e, Pad out) {
    for (Element child : Itx.getChildren(e, null)) {
      switch (child.getTagName()) {
        case Itx.ELEM_TEXT: {
          readText(child, out);
          break;
        }
        case Itx.ELEM_IMG: {
          readImage(child, out);
          break;
        }
        case Itx.ELEM_BR: {
          readLineFeed(out);
          break;
        }
        case Itx.ELEM_FORMULA: {
          readFormula(child, out);
          break;
        }
        default: {
          throw new ItxInputException();
        }
      }
    }
  }

  public Pad readNewPad(Element e) {
    Pad res = new Pad(fRealm);
    readPad(e, res);
    return res;
  }

  private void readText(Element e, Pad out) {
    out.appendText(Itx.find(e, Itx.ATTR_VALUE));
  }

  private void readImage(Element e, Pad out) {
    String src = Itx.find(e, Itx.ATTR_SRC);
    String type = e.hasAttribute(Itx.ATTR_TYPE) ? Itx.find(e, Itx.ATTR_TYPE) : null;
    String clipboardFormat = e.hasAttribute(Itx.ATTR_CLIPBOARD_FORMAT) ? Itx.find(e, Itx.ATTR_CLIPBOARD_FORMAT) : null;
    String clipboardData = (clipboardFormat == null) ? null : Itx.find(e, Itx.ATTR_CLIPBOARD_DATA);

    ItxImage itxImage = new ItxImage(src, type, clipboardFormat, clipboardData);
    out.appendImage(fObjectReader.readImage(itxImage, fRealm.blobStore()));
  }

  private void readLineFeed(Pad out) {
    out.appendLineFeed();
  }

  private void readFormula(Element e, Pad out) {
    out.appendFormula(new FormulaBlock(Itx.find(e, Itx.ATTR_SOURCE)));
  }

  public void readModifiers(Element e, ModifierList out) {
    for (Element xmlModifier : Itx.getChildren(e, Itx.ELEM_MODIFIER)) {
      ModifierReader reader = Check.notNull(fModifierReaders.get(Itx.find(xmlModifier, Itx.ATTR_TYPE)),
          ItxInputException::new);
      out.add(reader.read(this, xmlModifier));
    }
  }

  private void readUnlinkedProfiles(Element e, ProfileList out) {
    for (Element xmlProfile : Itx.getChildren(e, Itx.ELEM_PROFILE)) {
      out.add(readUnlinkedProfile(xmlProfile));
    }
  }

  public Profile readUnlinkedProfile(Element e) {
    Profile res = new Profile(fRealm);
    res.setTitle(Itx.find(e, Itx.ATTR_TITLE));
    Element xmlSectionProfile = Itx.getChildIfExists(e, Itx.ELEM_SECTION_PROFILE);

    res.setOptions(readProfileOptions(e, xmlSectionProfile == null));

    if (xmlSectionProfile != null) {
      res.setSectionProfile(readSectionProfile(xmlSectionProfile));
    }

    Element xmlTestResults = Itx.getChildIfExists(e, Itx.ELEM_TEST_RESULTS);
    if (xmlTestResults != null) {
      res.setAvailableScore(readAvailableScore(xmlTestResults));
    }

    Element xmlMarkScale = Itx.getChildIfExists(e, Itx.ELEM_MARK_SCALE);
    if (xmlMarkScale != null) {
      res.setMarkScale(readMarkScale(xmlMarkScale));
    }

    Element xmlModifiers = Itx.getChildIfExists(e, Itx.ELEM_MODIFIERS);
    if (xmlModifiers != null) {
      readModifiers(xmlModifiers, res.modifierList());
    }

    return res;
  }

  public Profile readAndLinkProfile(Element e, Test referenceTest) {
    Profile res = readUnlinkedProfile(e);
    linkProfile(res, referenceTest);
    return res;
  }

  private ProfileOptions readProfileOptions(Element e, boolean expectQuestionsPerSection) {
    ProfileOptions.Builder options = ProfileOptions.newBuilder();

    Element xmlQuestionSelection = Itx.getChild(e, Itx.ELEM_QUESTION_SELECTION);

    if (expectQuestionsPerSection) {
      String questionsPerSection = Itx.find(xmlQuestionSelection, Itx.ATTR_QUESTIONS_PER_SECTION);
      if (!questionsPerSection.equals(Itx.VAL_ALL)) {
        options.setQuestionsPerSection(Itx.toInteger(questionsPerSection));
      }
    } else {
      Check.that(!xmlQuestionSelection.hasAttribute(Itx.ATTR_QUESTIONS_PER_SECTION), ItxInputException::new);
    }

    if (xmlQuestionSelection.hasAttribute(Itx.ATTR_LABEL_FILTER)) {
      options.setLabelFilter(Itx.find(xmlQuestionSelection, Itx.ATTR_LABEL_FILTER));
    }

    options.setShuffleQuestions(Itx.findBoolean(xmlQuestionSelection, Itx.ATTR_SHUFFLE_QUESTIONS));

    Element xmlSessionOptions = Itx.getChild(e, Itx.ELEM_SESSION_OPTIONS);
    String durationMinutes = Itx.find(xmlSessionOptions, Itx.ATTR_DURATION_MINUTES);
    if (!durationMinutes.equals(Itx.VAL_UNLIMITED)) {
      options.setDurationMinutes(Itx.toInteger(durationMinutes));
    }

    Element xmlInstantFeedback = Itx.getChildIfExists(e, Itx.ELEM_INSTANT_FEEDBACK);

    return options
        .setEditableAnswers(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_EDITABLE_ANSWERS))
        .setBrowsableQuestions(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_BROWSABLE_QUESTIONS))
        .setWeightCues(Itx.findBoolean(xmlSessionOptions, Itx.ATTR_WEIGHT_CUES))
        .setInstantAnswerCorrectness(
            (xmlInstantFeedback != null) && Itx.findBoolean(xmlInstantFeedback, Itx.ATTR_ANSWER_CORRECTNESS))
        .setInstantTotalPercentCorrect(
            (xmlInstantFeedback != null) && Itx.findBoolean(xmlInstantFeedback, Itx.ATTR_TOTAL_PERCENT_CORRECT))
        .build();
  }

  private AvailableScore readAvailableScore(Element e) {
    AvailableScore.Builder b = AvailableScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setMark(Itx.findBoolean(e, Itx.ATTR_MARK));

    Element xmlQuestionResults = Itx.getChildIfExists(e, Itx.ELEM_QUESTION_RESULTS);
    if (xmlQuestionResults != null) {
      b.setForQuestions(readAvailableQuestionScore(xmlQuestionResults));
    }

    Element xmlSectionResults = Itx.getChildIfExists(e, Itx.ELEM_SECTION_RESULTS);
    if (xmlSectionResults != null) {
      b.setForSections(readAvailableSectionScore(xmlSectionResults));
    }

    return b.build();
  }

  private AvailableQuestionScore readAvailableQuestionScore(Element e) {
    return AvailableQuestionScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setCorrectAnswer(Itx.findBoolean(e, Itx.ATTR_CORRECT_ANSWER))
        .build();
  }

  private AvailableSectionScore readAvailableSectionScore(Element e) {
    return AvailableSectionScore.newBuilder()
        .setPercentCorrect(Itx.findBoolean(e, Itx.ATTR_PERCENT_CORRECT))
        .setPoints(Itx.findBoolean(e, Itx.ATTR_POINTS))
        .setQuestionCount(Itx.findBoolean(e, Itx.ATTR_QUESTION_COUNT))
        .setQuestionList(Itx.findBoolean(e, Itx.ATTR_QUESTION_LIST))
        .build();
  }

  private MarkScale readMarkScale(Element e) {
    MarkScaleData.Builder b = MarkScaleData.newBuilder();
    for (Element xmlMark : Itx.getChildren(Itx.getChild(e, Itx.ELEM_MARKS), Itx.ELEM_MARK)) {
      b.addMark(Mark.newBuilder()
          .setTitle(Itx.find(xmlMark, Itx.ATTR_TITLE))
          .setLowerBound(Itx.find(xmlMark, Itx.ATTR_LOWER_BOUND)));
    }
    return new MarkScale(b.build());
  }

  private void readTestInfo(Element e, Test out) {
    if (e.hasAttribute(Itx.ATTR_LAST_GENERATED_TAG)) {
      out.setLastGeneratedTag(Itx.findBigInteger(e, Itx.ATTR_LAST_GENERATED_TAG));
    } else if (e.hasAttribute(Itx.ATTR_NEXT_QUESTION_ID)) { // .it2
      Long nextQuestionId = Longs.tryParse(e.getAttribute(Itx.ATTR_NEXT_QUESTION_ID));
      Check.that((nextQuestionId != null) && (nextQuestionId >= 1), ItxInputException::new);
      out.setLastGeneratedTag(BigInteger.valueOf(nextQuestionId - 1));
    }
  }

  private SectionProfile readSectionProfile(Element e) {
    QuestionCount questionCount = e.hasAttribute(Itx.ATTR_QUESTIONS) ?
        QuestionCount.fromText(e.getAttribute(Itx.ATTR_QUESTIONS)) : null;

    ArrayList<SectionProfile> sectionProfiles = new ArrayList<>();
    Element xmlSectionProfiles = Itx.getChildIfExists(e, Itx.ELEM_SECTION_PROFILES);
    if (xmlSectionProfiles != null) {
      for (Element xmlSectionProfile : Itx.getChildren(xmlSectionProfiles, Itx.ELEM_SECTION_PROFILE)) {
        sectionProfiles.add(readSectionProfile(xmlSectionProfile));
      }
    }

    return new SectionProfile(Itx.find(e, Itx.ATTR_TITLE), null, questionCount, sectionProfiles);
  }

  private SectionProfile linkSectionProfile(SectionProfile sectionProfile, Section section) {
    Check.that(sectionProfile.sectionName().equals(section.name()), ItxInputException::new);

    List<SectionProfile> sectionProfiles = sectionProfile.sectionProfiles();
    List<Section> sections = section.sections();
    Check.that(sectionProfiles.size() == sections.size(), ItxInputException::new);

    ArrayList<SectionProfile> linkedSectionProfiles = new ArrayList<>();
    for (int sectionCount = sections.size(), i = 0; i < sectionCount; ++i) {
      linkedSectionProfiles.add(linkSectionProfile(sectionProfiles.get(i), sections.get(i)));
    }

    return new SectionProfile(section.name(), section.id(), sectionProfile.questionCount(), linkedSectionProfiles);
  }
}
