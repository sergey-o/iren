/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Utils;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.ImageSource;

import com.google.protobuf.ByteString;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.stream.XMLStreamException;
import java.util.function.Consumer;
import java.util.function.Function;

@Singleton public final class ItxCopier {
  private final ItxWriter.Factory fItxWriterFactory;
  private final ItxInput fItxInput;

  @Inject ItxCopier(
      ItxWriter.Factory itxWriterFactory,
      ItxInput itxInput) {
    fItxWriterFactory = itxWriterFactory;
    fItxInput = itxInput;
  }

  /**
   * The source and target realm must share the same {@link BlobStore}.
   * {@link ImageSource}s are not copied.
   */
  public <R> R copy(
      Consumer<ItxWriter> writeHandler,
      Function<ItxReader, R> readHandler,
      Realm targetRealm) {
    ByteString.Output buffer = ByteString.newOutput();

    Utils.writeXml(buffer, w -> {
      try {
        w.writeStartElement(Itx.ELEM_TEST_FRAGMENT);
        writeHandler.accept(fItxWriterFactory.create(w, new ExternallyStoredObjectWriter()));
        w.writeEndElement();
      } catch (XMLStreamException e) {
        throw new RuntimeException(e);
      }
    });

    return fItxInput.readFromStream(
        buffer.toByteString().newInput(),
        new ExternallyStoredObjectReader(),
        targetRealm,
        readHandler);
  }
}
