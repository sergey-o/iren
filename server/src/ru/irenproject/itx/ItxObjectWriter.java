/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.ImageSource;

import com.google.common.hash.Hashing;
import com.google.common.io.ByteSource;

import java.io.IOException;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ItxObjectWriter implements ObjectWriter {
  private static final long ZIP_ENTRY_TIME = new GregorianCalendar(2000, 0, 1).getTimeInMillis();

  public static ZipEntry createZipEntryWithFixedTime(String name) {
    ZipEntry res = new ZipEntry(name);
    res.setTime(ZIP_ENTRY_TIME);
    return res;
  }

  private final ZipOutputStream fZip;
  private final boolean fSkipImageSource;

  private final HashMap<ImageBlock, ItxImage> fImages = new HashMap<>();
  private long fImageNumber;

  public ItxObjectWriter(ZipOutputStream zip, boolean skipImageSource) {
    fZip = zip;
    fSkipImageSource = skipImageSource;
  }

  @Override public ItxImage writeImage(ImageBlock block, BlobStore blobStore) {
    ImageBlock b = fSkipImageSource ? new ImageBlock(block.mimeType(), block.data(), null) : block;
    return fImages.computeIfAbsent(b, ignored -> writeImageInternal(b, blobStore));
  }

  private ItxImage writeImageInternal(ImageBlock block, BlobStore blobStore) {
    try {
      ++fImageNumber;

      String dataLocation = String.format((Locale) null, "%s/%d.%s", Itx.IMAGE_DIRECTORY_NAME,
          fImageNumber, Check.notNull(Itx.IMAGE_EXTENSIONS_BY_MIME_TYPE.get(block.mimeType())));
      ByteSource data = blobStore.get(block.data());

      ZipEntry entry = createZipEntryWithFixedTime(dataLocation);
      entry.setMethod(ZipEntry.STORED);
      entry.setSize(data.size());
      entry.setCrc(data.hash(Hashing.crc32()).padToLong());

      fZip.putNextEntry(entry);
      data.copyTo(fZip);
      fZip.closeEntry();

      ImageSource source = block.source();
      String sourceFormat, sourceLocation;
      if (source == null) {
        sourceFormat = null;
        sourceLocation = null;
      } else {
        sourceFormat = source.format();
        sourceLocation = String.format((Locale) null, "%s/%d.%s", Itx.IMAGE_DIRECTORY_NAME,
            fImageNumber, Itx.SOURCE_DATA_FILE_EXTENSION);
        fZip.putNextEntry(createZipEntryWithFixedTime(sourceLocation));
        blobStore.get(source.data()).copyTo(fZip);
        fZip.closeEntry();
      }

      return new ItxImage(dataLocation, null, sourceFormat, sourceLocation);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
