/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.TestInputException;
import ru.irenproject.infra.Realm;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.util.function.Function;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

@Singleton public final class ItxInput {
  private final ItxReaderFactory fItxReaderFactory;

  @Inject private ItxInput(ItxReaderFactory itxReaderFactory) {
    fItxReaderFactory = itxReaderFactory;
  }

  public <R> R read(Path in, Realm realm, Function<ItxReader, R> readHandler) {
    try (ZipFile zip = new ZipFile(in.toFile())) {
      ZipEntry entry = Check.notNull(zip.getEntry(Itx.TEST_FILE_NAME), ItxInputException::new);

      try (InputStream xmlIn = zip.getInputStream(entry)) {
        return readFromStream(xmlIn, new ItxObjectReader(zip), realm, readHandler);
      }
    } catch (ZipException e) {
      throw new ItxInputException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public <R> R readFromStream(InputStream in, ObjectReader objectReader, Realm realm,
      Function<ItxReader, R> readHandler) {
    Document document = readDocument(in);

    ItxReader itxReader = fItxReaderFactory.create(objectReader, document.getDocumentElement(), realm);
    try {
      return readHandler.apply(itxReader);
    } catch (TestInputException e) {
      throw new ItxInputException(e);
    }
  }

  private static Document readDocument(InputStream in) {
    try {
      DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
      dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
      return dbf.newDocumentBuilder().parse(in);
    } catch (SAXException | UnsupportedEncodingException e) {
      throw new ItxInputException(e);
    } catch (ParserConfigurationException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  public Test readTest(Path in, Realm realm) {
    return read(in, realm, reader -> {
      Itx.checkName(reader.root(), Itx.ELEM_IREN_TEST);

      if (reader.root().hasAttribute(Itx.ATTR_VERSION)) {
        int version = Itx.findInteger(reader.root(), Itx.ATTR_VERSION);
        Check.that(version >= 2, ItxInputException::new);
        Check.that(version <= Itx.VERSION, () -> new ItxInputException(ItxInputException.Reason.UNKNOWN_VERSION));
      }

      return reader.readTest(reader.root());
    });
  }
}
