/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;

import com.google.common.collect.ImmutableBiMap;
import com.google.common.net.MediaType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.annotation.Nullable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public final class Itx {
  public static final int VERSION = 6;

  public static final String ELEM_APPLY_TO = "applyTo";
  public static final String ELEM_BASE_ITEM = "baseItem";
  public static final String ELEM_BR = "br";
  public static final String ELEM_CATEGORIES = "categories";
  public static final String ELEM_CATEGORY = "category";
  public static final String ELEM_CATEGORY_TITLE = "categoryTitle";
  public static final String ELEM_CATEGORY_ITEM = "categoryItem";
  public static final String ELEM_CATEGORY_ITEMS = "categoryItems";
  public static final String ELEM_CHOICE = "choice";
  public static final String ELEM_CHOICES = "choices";
  public static final String ELEM_CLASSIFY_OPTIONS = "classifyOptions";
  public static final String ELEM_CONTENT = "content";
  public static final String ELEM_DISTRACTOR = "distractor";
  public static final String ELEM_DISTRACTORS = "distractors";
  public static final String ELEM_FORMULA = "formula";
  public static final String ELEM_IMG = "img";
  public static final String ELEM_INSTANT_FEEDBACK = "instantFeedback";
  public static final String ELEM_IREN_TEST = "irenTest";
  public static final String ELEM_LABEL = "label";
  public static final String ELEM_LABELS = "labels";
  public static final String ELEM_MARK = "mark";
  public static final String ELEM_MARKS = "marks";
  public static final String ELEM_MARK_SCALE = "markScale";
  public static final String ELEM_MATCH_OPTIONS = "matchOptions";
  public static final String ELEM_MATCHING_ITEM = "matchingItem";
  public static final String ELEM_MODIFIER = "modifier";
  public static final String ELEM_MODIFIERS = "modifiers";
  public static final String ELEM_ORDER_OPTIONS = "orderOptions";
  public static final String ELEM_PAIR = "pair";
  public static final String ELEM_PAIRS = "pairs";
  public static final String ELEM_PATTERN = "pattern";
  public static final String ELEM_PATTERNS = "patterns";
  public static final String ELEM_PROFILE = "profile";
  public static final String ELEM_PROFILES = "profiles";
  public static final String ELEM_QUESTION = "question";
  public static final String ELEM_QUESTION_RESULTS = "questionResults";
  public static final String ELEM_QUESTION_SELECTION = "questionSelection";
  public static final String ELEM_QUESTIONS = "questions";
  public static final String ELEM_QUESTION_TYPE = "questionType";
  public static final String ELEM_SCRIPT = "script";
  public static final String ELEM_SECTION = "section";
  public static final String ELEM_SECTIONS = "sections";
  public static final String ELEM_SECTION_PROFILE = "sectionProfile";
  public static final String ELEM_SECTION_PROFILES = "sectionProfiles";
  public static final String ELEM_SECTION_RESULTS = "sectionResults";
  public static final String ELEM_SEQUENCE = "sequence";
  public static final String ELEM_SEQUENCE_ITEM = "sequenceItem";
  public static final String ELEM_SESSION_OPTIONS = "sessionOptions";
  public static final String ELEM_TEST_INFO = "testInfo";
  public static final String ELEM_TEST_RESULTS = "testResults";
  public static final String ELEM_TEXT = "text";

  public static final String ATTR_ANSWER_CORRECTNESS = "answerCorrectness";
  public static final String ATTR_BASE_ITEMS_USED = "baseItemsUsed";
  public static final String ATTR_BROWSABLE_QUESTIONS = "browsableQuestions";
  public static final String ATTR_CASE_SENSITIVE = "caseSensitive";
  public static final String ATTR_CLIPBOARD_DATA = "clipboardData";
  public static final String ATTR_CLIPBOARD_FORMAT = "clipboardFormat";
  public static final String ATTR_CORRECT = "correct";
  public static final String ATTR_CORRECT_ANSWER = "correctAnswer";
  public static final String ATTR_DISTRACTORS_USED = "distractorsUsed";
  public static final String ATTR_DURATION_MINUTES = "durationMinutes";
  public static final String ATTR_EDITABLE_ANSWERS = "editableAnswers";
  public static final String ATTR_ENABLED = "enabled";
  public static final String ATTR_FIXED = "fixed";
  public static final String ATTR_ID = "id"; // .it2
  public static final String ATTR_ITEMS_USED = "itemsUsed";
  public static final String ATTR_LABEL_FILTER = "labelFilter";
  public static final String ATTR_LAST_GENERATED_TAG = "lastGeneratedTag";
  public static final String ATTR_LINE = "line";
  public static final String ATTR_LOWER_BOUND = "lowerBound";
  public static final String ATTR_MARK = "mark";
  public static final String ATTR_MIN_ITEMS_PER_CATEGORY_USED = "minItemsPerCategoryUsed";
  public static final String ATTR_NEGATIVE = "negative";
  public static final String ATTR_NEW_EVALUATION_MODEL = "newEvaluationModel";
  public static final String ATTR_NEW_WEIGHT = "newWeight";
  public static final String ATTR_NEXT_QUESTION_ID = "nextQuestionId"; // .it2
  public static final String ATTR_NUMERIC_AWARE = "numericAware";
  public static final String ATTR_PERCENT_CORRECT = "percentCorrect";
  public static final String ATTR_POINTS = "points";
  public static final String ATTR_PRECISION = "precision";
  public static final String ATTR_QUALITY = "quality";
  public static final String ATTR_QUESTIONS = "questions";
  public static final String ATTR_QUESTIONS_PER_SECTION = "questionsPerSection";
  public static final String ATTR_QUESTION_COUNT = "questionCount";
  public static final String ATTR_QUESTION_LIST = "questionList";
  public static final String ATTR_SEQUENCE_ITEMS_USED = "sequenceItemsUsed";
  public static final String ATTR_SHUFFLE_QUESTIONS = "shuffleQuestions";
  public static final String ATTR_SOURCE = "source";
  public static final String ATTR_SPACES = "spaces";
  public static final String ATTR_SRC = "src";
  public static final String ATTR_TAG = "tag";
  public static final String ATTR_TITLE = "title";
  public static final String ATTR_TOTAL_PERCENT_CORRECT = "totalPercentCorrect";
  public static final String ATTR_TYPE = "type";
  public static final String ATTR_VALUE = "value";
  public static final String ATTR_VERSION = "version";
  public static final String ATTR_WEIGHT = "weight";
  public static final String ATTR_WEIGHT_CUES = "weightCues";
  public static final String ATTR_WILDCARD = "wildcard";

  public static final String VAL_TRUE = "true";
  public static final String VAL_FALSE = "false";
  public static final String VAL_ALL = "all";
  public static final String VAL_UNLIMITED = "unlimited";

  public static final String VAL_EVALUATION_MODEL_TYPE_DICHOTOMIC = "dichotomic";
  public static final String VAL_EVALUATION_MODEL_TYPE_LAX = "lax";

  public static final String VAL_PATTERN_TYPE_TEXT = "text";
  public static final String VAL_PATTERN_TYPE_REGEXP = "regexp";

  public static final String VAL_PATTERN_SPACES_NORMALIZE = "normalize";
  public static final String VAL_PATTERN_SPACES_IGNORE = "ignore";
  public static final String VAL_PATTERN_SPACES_EXACT = "exact";

  public static final String TEST_FILE_NAME = "test.xml";
  public static final String IMAGE_DIRECTORY_NAME = "images";
  public static final String SOURCE_DATA_FILE_EXTENSION = "dat";
  public static final String IMAGE_PNG_EXTENSION = "png";
  public static final String IMAGE_JPG_EXTENSION = "jpg";

  public static final String ELEM_TEST_FRAGMENT = "testFragment";
  public static final String ATTR_ORIGIN = "origin";

  public static final ImmutableBiMap<String, String> IMAGE_EXTENSIONS_BY_MIME_TYPE = ImmutableBiMap.of(
      MediaType.PNG.toString(), IMAGE_PNG_EXTENSION,
      MediaType.JPEG.toString(), IMAGE_JPG_EXTENSION);

  public static void checkName(Element e, String expectedName) {
    Check.that(e.getTagName().equalsIgnoreCase(expectedName), ItxInputException::new);
  }

  public static String find(Element e, String attributeName) {
    Check.that(e.hasAttribute(attributeName), ItxInputException::new);
    return e.getAttribute(attributeName);
  }

  public static boolean findBoolean(Element e, String attributeName) {
    return toBoolean(find(e, attributeName));
  }

  private static boolean toBoolean(String s) {
    switch (s) {
      case VAL_TRUE: return true;
      case VAL_FALSE: return false;
      default: throw new ItxInputException();
    }
  }

  public static int findInteger(Element e, String attributeName) {
    return toInteger(find(e, attributeName));
  }

  public static int toInteger(String s) {
    try {
      return Integer.parseInt(s);
    } catch (NumberFormatException e) {
      throw new ItxInputException(e);
    }
  }

  public static BigInteger findBigInteger(Element e, String attributeName) {
    return toBigInteger(find(e, attributeName));
  }

  public static BigInteger toBigInteger(String s) {
    try {
      return new BigInteger(s);
    } catch (NumberFormatException e) {
      throw new ItxInputException(e);
    }
  }

  private static @Nullable Element toElementIfNameMatches(Node node, @Nullable String name) {
    if (node instanceof Element) {
      Element element = (Element) node;
      if ((name == null) || element.getTagName().equalsIgnoreCase(name)) {
        return element;
      }
    }
    return null;
  }

  public static @Nullable Element getChildIfExists(Element parent, String name) {
    for (Node n = parent.getFirstChild(); n != null; n = n.getNextSibling()) {
      Element res = toElementIfNameMatches(n, name);
      if (res != null) {
        return res;
      }
    }
    return null;
  }

  public static Element getChild(Element parent, String name) {
    return Check.notNull(getChildIfExists(parent, name), ItxInputException::new);
  }

  public static List<Element> getChildren(Element parent, @Nullable String name) {
    List<Element> res = new ArrayList<>();

    for (Node n = parent.getFirstChild(); n != null; n = n.getNextSibling()) {
      Element e = toElementIfNameMatches(n, name);
      if (e != null) {
        res.add(e);
      }
    }

    return res;
  }

  private Itx() {}
}
