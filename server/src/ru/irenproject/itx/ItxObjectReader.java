/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.pad.ImageBlock;
import ru.irenproject.pad.ImageSource;

import com.google.protobuf.ByteString;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public final class ItxObjectReader implements ObjectReader {
  private final ZipFile fZip;
  private final HashMap<ItxImage, ImageBlock> fImages = new HashMap<>();

  public ItxObjectReader(ZipFile zip) {
    fZip = zip;
  }

  @Override public ImageBlock readImage(ItxImage image, BlobStore blobStore) {
    return fImages.computeIfAbsent(image, ignored -> readImageInternal(image, blobStore));
  }

  private ImageBlock readImageInternal(ItxImage image, BlobStore blobStore) {
    String mimeType = Check.notNull(Itx.IMAGE_EXTENSIONS_BY_MIME_TYPE.inverse().get(
        StringUtils.substringAfterLast(image.src(), ".")), ItxInputException::new);

    ByteString dataBlob = read(image.src());
    BlobId dataBlobId = blobStore.put(Utils.byteStringAsByteSource(dataBlob));

    ImageSource source;
    if (image.clipboardFormat() == null) {
      source = null;
    } else {
      ByteString clipboardDataBlob = read(image.clipboardData());
      BlobId clipboardDataBlobId = blobStore.put(Utils.byteStringAsByteSource(clipboardDataBlob));
      source = new ImageSource(image.clipboardFormat(), clipboardDataBlobId);
    }

    return new ImageBlock(mimeType, dataBlobId, source);
  }

  private ByteString read(String location) {
    ZipEntry entry = Check.notNull(fZip.getEntry(location), ItxInputException::new);

    try (InputStream in = fZip.getInputStream(entry)) {
      return ByteString.readFrom(in);
    } catch (ZipException e) {
      throw new ItxInputException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
