/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.itx;

import ru.irenproject.Test;
import ru.irenproject.Utils;

import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.function.Consumer;
import java.util.zip.ZipOutputStream;

@Singleton public final class ItxOutput {
  private final ItxWriter.Factory fItxWriterFactory;

  @Inject private ItxOutput(ItxWriter.Factory itxWriterFactory) {
    fItxWriterFactory = itxWriterFactory;
  }

  public void writeFragment(Consumer<ItxWriter> writeHandler, OutputStream out, @Nullable String origin) {
    write(writeHandler, Itx.ELEM_TEST_FRAGMENT, out, false, origin);
  }

  public void writeTest(Test test, OutputStream out, boolean skipImageSource, @Nullable ItxWriter.Filter filter) {
    write(writer -> {
      writer.setFilter(filter);
      writer.add(Itx.ATTR_VERSION, Integer.toString(Itx.VERSION));
      writer.writeTest(test);
    }, Itx.ELEM_IREN_TEST, out, skipImageSource, null);
  }

  private void write(Consumer<ItxWriter> writeHandler, String rootElementName, OutputStream out,
      boolean skipImageSource, @Nullable String origin) {
    try (ZipOutputStream zipOut = new ZipOutputStream(out)) {
      ByteArrayOutputStream xmlOut = new ByteArrayOutputStream();

      Utils.writeXml(xmlOut, w -> {
        try {
          w.writeStartElement(rootElementName);
          if (origin != null) {
            w.writeAttribute(Itx.ATTR_ORIGIN, origin);
          }

          writeHandler.accept(fItxWriterFactory.create(w, new ItxObjectWriter(zipOut, skipImageSource)));

          w.writeEndElement();
        } catch (XMLStreamException e) {
          throw new RuntimeException(e);
        }
      });

      zipOut.putNextEntry(ItxObjectWriter.createZipEntryWithFixedTime(Itx.TEST_FILE_NAME));
      xmlOut.writeTo(zipOut);
      zipOut.closeEntry();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
