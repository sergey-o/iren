/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Initializable;
import ru.irenproject.TestModule;
import ru.irenproject.authentication.AuthenticationService;
import ru.irenproject.authentication.UserManagerChannel;
import ru.irenproject.editor.CommonEditorModule;
import ru.irenproject.formula.FormulaRenderer;
import ru.irenproject.live.LiveModule;

import akka.actor.typed.ActorRef;
import akka.actor.typed.DispatcherSelector;
import akka.actor.typed.javadsl.Behaviors;
import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.multibindings.Multibinder;

import javax.inject.Inject;
import javax.inject.Provider;

public final class WorkModule extends AbstractModule {
  private final WorkEnvironment fWorkEnvironment;

  public WorkModule(WorkEnvironment workEnvironment) {
    fWorkEnvironment = workEnvironment;
  }

  @Override protected void configure() {
    install(new TestModule());
    install(new LiveModule());
    install(new CommonEditorModule());

    bind(WorkEnvironment.class).toInstance(fWorkEnvironment);

    install(new FactoryModuleBuilder().build(WorkListSender.Factory.class));
    install(new FactoryModuleBuilder().build(WebTransportHandler.Factory.class));
    install(new FactoryModuleBuilder().build(MessageHandler.Factory.class));
    install(new FactoryModuleBuilder().build(ConnectionCatalog.Factory.class));
    install(new FactoryModuleBuilder().build(ConnectionHandler.Factory.class));
    install(new FactoryModuleBuilder().build(WorkCatalog.Factory.class));
    install(new FactoryModuleBuilder().build(WorkHub.Factory.class));
    install(new FactoryModuleBuilder().build(WorkSourcePool.Factory.class));
    install(new FactoryModuleBuilder().build(WorkArchiver.Factory.class));
    install(new FactoryModuleBuilder().build(AuthenticationService.Factory.class));

    install(new FactoryModuleBuilder().build(LoginChannel.Factory.class));
    install(new FactoryModuleBuilder().build(SupervisorLoginHandler.Factory.class));
    install(new FactoryModuleBuilder().build(LoginHandler.Factory.class));
    install(new FactoryModuleBuilder().build(UserManagerChannel.Factory.class)); //TODO separate module?
    install(new FactoryModuleBuilder().build(WorkCreatorChannel.Factory.class));
    install(new FactoryModuleBuilder().build(WorkControllerChannel.Factory.class));
    install(new FactoryModuleBuilder().build(ArchiveManagerChannel.Factory.class));
    install(new FactoryModuleBuilder().build(WatcherChannel.Factory.class));
    install(new FactoryModuleBuilder().build(ShutDownWorkMediator.Factory.class));
    install(new FactoryModuleBuilder().build(StudentWorkSelectorChannel.Factory.class));
    install(new FactoryModuleBuilder().build(SessionProvider.Factory.class));
    install(new FactoryModuleBuilder().build(StudentChannel.Factory.class));

    bind(LoginRateLimiter.class).asEagerSingleton();

    Multibinder<Class<?>> initializables = Multibinder.newSetBinder(binder(), new TypeLiteral<>() {},
        Initializable.class);

    bind(new TypeLiteral<ActorRef<DataDirectoryUnlocker.Message>>() {}).toProvider(DataDirectoryUnlockerProvider.class)
        .asEagerSingleton();

    bind(new TypeLiteral<ActorRef<AuthenticationService.Message>>() {}).toProvider(AuthenticationServiceProvider.class)
        .asEagerSingleton();
    initializables.addBinding().toInstance(AuthenticationService.class);

    bind(new TypeLiteral<ActorRef<WorkArchiver.Message>>() {}).toProvider(WorkArchiverProvider.class)
        .asEagerSingleton();
    initializables.addBinding().toInstance(WorkArchiver.class);

    bind(new TypeLiteral<ActorRef<WorkCatalog.Message>>() {}).toProvider(WorkCatalogProvider.class)
        .asEagerSingleton();
    bind(new TypeLiteral<ActorRef<ConnectionCatalog.Message>>() {}).toProvider(ConnectionCatalogProvider.class)
        .asEagerSingleton();

    bind(Integer.class).annotatedWith(FormulaRenderer.MaxIdleContexts.class)
        .toInstance(Runtime.getRuntime().availableProcessors());
  }

  private static final class DataDirectoryUnlockerProvider
      implements Provider<ActorRef<DataDirectoryUnlocker.Message>> {
    @Inject WorkEnvironment fWorkEnvironment;
    @Inject Provider<DataDirectoryUnlocker> fBehaviorProvider;

    @Override public ActorRef<DataDirectoryUnlocker.Message> get() {
      return fWorkEnvironment.createComponent(
          Behaviors.setup(ignored -> fBehaviorProvider.get()),
          DataDirectoryUnlocker.class.getSimpleName(),
          9999,
          DataDirectoryUnlocker.Stop.INSTANCE);
    }
  }

  private static final class AuthenticationServiceProvider
      implements Provider<ActorRef<AuthenticationService.Message>> {
    @Inject WorkEnvironment fWorkEnvironment;
    @Inject AuthenticationService.Factory fAuthenticationServiceFactory;

    @Override public ActorRef<AuthenticationService.Message> get() {
      return fWorkEnvironment.createComponent(
          Behaviors.setup(fAuthenticationServiceFactory::create),
          AuthenticationService.class.getSimpleName(),
          5200,
          AuthenticationService.Stop.INSTANCE);
    }
  }

  private static final class WorkArchiverProvider implements Provider<ActorRef<WorkArchiver.Message>> {
    @Inject WorkEnvironment fWorkEnvironment;
    @Inject WorkArchiver.Factory fWorkArchiverFactory;

    @Override public ActorRef<WorkArchiver.Message> get() {
      return fWorkEnvironment.createComponentWithProps(
          Behaviors.setup(fWorkArchiverFactory::create),
          WorkArchiver.class.getSimpleName(),
          DispatcherSelector.fromConfig("work-archiver-dispatcher"),
          5100,
          WorkArchiver.Stop.INSTANCE);
    }
  }

  private static final class WorkCatalogProvider implements Provider<ActorRef<WorkCatalog.Message>> {
    @Inject WorkEnvironment fWorkEnvironment;
    @Inject WorkCatalog.Factory fWorkCatalogFactory;

    @Override public ActorRef<WorkCatalog.Message> get() {
      return fWorkEnvironment.createComponent(
          Behaviors.setup(fWorkCatalogFactory::create),
          WorkCatalog.class.getSimpleName(),
          5000,
          WorkCatalog.Stop.INSTANCE);
    }
  }

  private static final class ConnectionCatalogProvider implements Provider<ActorRef<ConnectionCatalog.Message>> {
    @Inject WorkEnvironment fWorkEnvironment;
    @Inject ConnectionCatalog.Factory fConnectionCatalogFactory;

    @Override public ActorRef<ConnectionCatalog.Message> get() {
      return fWorkEnvironment.createComponent(
          Behaviors.setup(fConnectionCatalogFactory::create),
          ConnectionCatalog.class.getSimpleName(),
          4900,
          ConnectionCatalog.Stop.INSTANCE);
    }
  }
}
