/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.ProgramInfo;
import ru.irenproject.RequestContext;
import ru.irenproject.Utils;
import ru.irenproject.common.protocol.Proto.BootstrapNotice;
import ru.irenproject.common.protocol.Proto.ClientMessage;
import ru.irenproject.common.protocol.Proto.ProtocolConstants;
import ru.irenproject.common.protocol.Proto.ServerHello;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.protocol.Proto.ServerVersion;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.ChildFailed;
import akka.actor.typed.PostStop;
import akka.actor.typed.Terminated;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;

public final class ConnectionHandler extends AbstractBehavior<ConnectionHandler.Message> {
  public interface Message {}

  public enum Activate implements Message { INSTANCE }

  public static final class Data implements Message {
    public final ClientMessage clientMessage;

    public Data(ClientMessage clientMessage) {
      this.clientMessage = clientMessage;
    }
  }

  public static final class SpawnChannel<T, R> implements Message {
    public final Behavior<T> behavior;
    public final Predicate<ClientMessage> validator;
    public final BiFunction<ClientMessage, RequestContext, T> messageBuilder;
    public final Function<SpawnChannelResult<T>, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public SpawnChannel(
        Behavior<T> behavior,
        Predicate<ClientMessage> validator,
        BiFunction<ClientMessage, RequestContext, T> messageBuilder,
        Function<SpawnChannelResult<T>, R> replyBuilder,
        ActorRef<R> replyTo) {
      this.behavior = behavior;
      this.validator = validator;
      this.messageBuilder = messageBuilder;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public static final class SpawnChannelResult<T> {
    final ActorRef<T> channel;
    final long channelId;

    public SpawnChannelResult(ActorRef<T> channel, long channelId) {
      this.channel = channel;
      this.channelId = channelId;
    }
  }

  public interface Factory {
    ConnectionHandler create(
        ActorContext<Message> context,
        Connection connection,
        KeyVerifier keyVerifier,
        @Nullable String serverAddress);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        KeyVerifier keyVerifier,
        @Nullable String serverAddress) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, keyVerifier, serverAddress));
    }
  }

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final KeyVerifier fKeyVerifier;
  private final @Nullable String fServerAddress;
  private final LoginChannel.Factory fLoginChannelFactory;
  private final StudentWorkSelectorChannel.Factory fStudentWorkSelectorChannelFactory;

  private final HashMap<Long, RequestForwarder<?>> fRequestForwardersByChannelId = new HashMap<>();
  private long fLastUsedChannelId;
  private long fLastRequestId = ProtocolConstants.LAST_RESERVED_CLIENT_CHANNEL_ID_VALUE;

  @Inject private ConnectionHandler(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted KeyVerifier keyVerifier,
      @Assisted @Nullable String serverAddress,
      LoginChannel.Factory loginChannelFactory,
      StudentWorkSelectorChannel.Factory studentWorkSelectorChannelFactory) {
    fContext = context;
    fConnection = connection;
    fKeyVerifier = keyVerifier;
    fServerAddress = serverAddress;
    fLoginChannelFactory = loginChannelFactory;
    fStudentWorkSelectorChannelFactory = studentWorkSelectorChannelFactory;

    fConnection.closeFuture().addListener(ignored -> context.getSelf().tell(Stop.INSTANCE));
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(Terminated.class, this::onTerminated)
        .onSignal(PostStop.class, ignored -> onPostStop())
        .onMessageEquals(Activate.INSTANCE, this::onActivate)
        .onMessage(Data.class, this::onData)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(SpawnChannel.class, m -> onSpawnChannel((SpawnChannel<?, ?>) m))
        .build();
  }

  private Behavior<Message> onTerminated(Terminated signal) {
    Behavior<Message> res;
    if (signal instanceof ChildFailed) {
      res = Behaviors.stopped();
    } else {
      Utils.removeFirstWhere(fRequestForwardersByChannelId.values(), rf -> rf.fTarget.equals(signal.getRef()));
      res = this;
    }

    return res;
  }

  private Behavior<Message> onPostStop() {
    fConnection.close();
    return this;
  }

  private Behavior<Message> onActivate() {
    fConnection.enqueue(ServerMessage.newBuilder()
        .setChannelId(ProtocolConstants.BOOTSTRAP_CHANNEL_ID_VALUE)
        .setServerVersion(ServerVersion.newBuilder()
            .setEpoch(1)
            .setVersion(ProgramInfo.VERSION))
        .build());

    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(ProtocolConstants.BOOTSTRAP_CHANNEL_ID_VALUE)
        .setBootstrapNotice(BootstrapNotice.newBuilder()
            .setServerHello(ServerHello.newBuilder()
                .setLoginChannelId(openLoginChannel())
                .setStudentWorkSelectorChannelId(openStudentWorkSelectorChannel())))
        .build());

    return this;
  }

  private long openLoginChannel() {
    return
        spawnChannel(
            LoginChannel.Factory.makeBehavior(
                fLoginChannelFactory,
                fConnection,
                fContext.getSelf(),
                fKeyVerifier,
                fServerAddress),
            ClientMessage::hasLoginRequest,
            (r, rc) -> new LoginChannel.RequestMessage(r.getLoginRequest(), rc)
        ).channelId;
  }

  private long openStudentWorkSelectorChannel() {
    return
        spawnChannel(
            StudentWorkSelectorChannel.Factory.makeBehavior(
                fStudentWorkSelectorChannelFactory,
                fConnection,
                fContext.getSelf()),
            ClientMessage::hasStudentWorkSelectorRequest,
            (r, rc) -> new StudentWorkSelectorChannel.RequestMessage(r.getStudentWorkSelectorRequest(), rc)
        ).channelId;
  }

  private <T> SpawnChannelResult<T> spawnChannel(
      Behavior<T> behavior,
      Predicate<ClientMessage> validator,
      BiFunction<ClientMessage, RequestContext, T> messageBuilder) {
    long channelId = generateChannelId();

    ActorRef<T> channel = fContext.spawn(behavior, Long.toString(channelId));
    fContext.watch(channel);

    fRequestForwardersByChannelId.put(channelId, new RequestForwarder<>(validator, messageBuilder, channel));

    return new SpawnChannelResult<>(channel, channelId);
  }

  private long generateChannelId() {
    ++fLastUsedChannelId;
    return fLastUsedChannelId;
  }

  private static final class RequestForwarder<T> {
    final Predicate<ClientMessage> fValidator;
    final BiFunction<ClientMessage, RequestContext, T> fTargetMessageBuilder;
    final ActorRef<T> fTarget;

    RequestForwarder(
        Predicate<ClientMessage> validator,
        BiFunction<ClientMessage, RequestContext, T> targetMessageBuilder,
        ActorRef<T> target) {
      fValidator = validator;
      fTargetMessageBuilder = targetMessageBuilder;
      fTarget = target;
    }

    void forward(ClientMessage request, RequestContext requestContext) {
      Check.input(fValidator.test(request));
      fTarget.tell(fTargetMessageBuilder.apply(request, requestContext));
    }
  }

  private Behavior<Message> onData(Data m) {
    handle(m.clientMessage);
    return this;
  }

  private enum Stop implements Message { INSTANCE }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }

  private void handle(ClientMessage m) {
    ++fLastRequestId;

    RequestForwarder<?> requestForwarder = fRequestForwardersByChannelId.get(m.getChannelId());
    if (requestForwarder != null) {
      requestForwarder.forward(m, new RequestContext(fLastRequestId));
    }
  }

  private <T, R> Behavior<Message> onSpawnChannel(SpawnChannel<T, R> m) {
    SpawnChannelResult<T> result = spawnChannel(m.behavior, m.validator, m.messageBuilder);

    m.replyTo.tell(m.replyBuilder.apply(result));

    return this;
  }
}
