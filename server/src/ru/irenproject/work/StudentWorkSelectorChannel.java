/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.Utils;
import ru.irenproject.common.protocol.Proto.ClientMessage;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.studentWorkSelector.Proto.OpenWork;
import ru.irenproject.common.studentWorkSelector.Proto.ReopenWork;
import ru.irenproject.common.studentWorkSelector.Proto.StudentWorkSelectorNotice;
import ru.irenproject.common.studentWorkSelector.Proto.StudentWorkSelectorReply;
import ru.irenproject.common.studentWorkSelector.Proto.StudentWorkSelectorRequest;
import ru.irenproject.common.studentWorkSelector.Proto.WorkList;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.collect.ImmutableList;
import com.google.common.io.BaseEncoding;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class StudentWorkSelectorChannel extends AbstractBehavior<StudentWorkSelectorChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final StudentWorkSelectorRequest request;
    public final RequestContext requestContext;

    public RequestMessage(StudentWorkSelectorRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    StudentWorkSelectorChannel create(
        ActorContext<Message> context,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, connectionHandler));
    }
  }

  private static final StudentWorkSelectorReply WORK_NOT_FOUND = StudentWorkSelectorReply.newBuilder()
      .setOpenWorkReply(OpenWork.Reply.newBuilder()
          .setWorkNotFound(Empty.getDefaultInstance()))
      .build();

  private static final StudentWorkSelectorReply REOPEN_FAILED = StudentWorkSelectorReply.newBuilder()
      .setReopenWorkReply(ReopenWork.Reply.newBuilder()
          .setFailure(Empty.getDefaultInstance()))
      .build();

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final ActorRef<ConnectionHandler.Message> fConnectionHandler;
  private final WorkListSender.Factory fWorkListSenderFactory;
  private final SessionProvider.Factory fSessionProviderFactory;
  private final StudentChannel.Factory fStudentChannelFactory;

  private final ActorRef<SessionProvider.Result> fSessionProviderAdapter;

  private @Nullable ActorRef<WorkListSender.Message> fWorkListSender;
  private /* @Nullable */OpenWorkContext fOpenWorkContext;

  @Inject private StudentWorkSelectorChannel(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted ActorRef<ConnectionHandler.Message> connectionHandler,
      WorkListSender.Factory workListSenderFactory,
      SessionProvider.Factory sessionProviderFactory,
      StudentChannel.Factory studentChannelFactory) {
    fContext = context;
    fConnection = connection;
    fConnectionHandler = connectionHandler;
    fWorkListSenderFactory = workListSenderFactory;
    fSessionProviderFactory = sessionProviderFactory;
    fStudentChannelFactory = studentChannelFactory;

    fSessionProviderAdapter = fContext.messageAdapter(SessionProvider.Result.class, SessionProviderResult::new);
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessage(SessionProviderResult.class, m -> onSessionProviderResult(m.original))
        .onMessageEquals(SessionProviderTerminated.INSTANCE, this::onSessionProviderTerminated)
        .onMessage(StudentChannelOpened.class, this::onStudentChannelOpened)
        .onMessage(StudentChannelTerminated.class, this::onStudentChannelTerminated)
        .build();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    StudentWorkSelectorRequest r = m.request;
    switch (r.getRequestCase()) {
      case WATCH_WORK_LIST: return onWatchWorkList(m.requestContext);
      case UNWATCH_WORK_LIST: return onUnwatchWorkList();
      case OPEN_WORK: return onOpenWork(r.getOpenWork(), m.requestContext);
      case REOPEN_WORK: return onReopenWork(r.getReopenWork(), m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onWatchWorkList(RequestContext requestContext) {
    Check.input(fWorkListSender == null);

    Function<ImmutableList<Work>, ServerMessage> noticeBuilder = createNoticeBuilder(requestContext.id());

    fWorkListSender = fContext.spawn(
        WorkListSender.Factory.makeBehavior(fWorkListSenderFactory, fConnection, noticeBuilder),
        Utils.makeUniqueChildActorName(WorkListSender.class.getSimpleName(), fContext));

    return this;
  }

  private static Function<ImmutableList<Work>, ServerMessage> createNoticeBuilder(long noticeChannelId) {
    return workList -> ServerMessage.newBuilder()
        .setChannelId(noticeChannelId)
        .setStudentWorkSelectorNotice(StudentWorkSelectorNotice.newBuilder()
            .setWorkList(WorkList.newBuilder()
                .addAllItem(workList.stream()
                    .map(work -> WorkList.Item.newBuilder()
                        .setId(Ids.toHexString(work.id()))
                        .setTitle(work.title())
                        .build())
                    .collect(Collectors.toList()))))
        .build();
  }

  private Behavior<Message> onUnwatchWorkList() {
    if (fWorkListSender != null) {
      fContext.stop(fWorkListSender);
      fWorkListSender = null;
    }

    return this;
  }

  private Behavior<Message> onOpenWork(OpenWork m, RequestContext requestContext) {
    ByteString workId = Check.inputNotNull(Ids.tryFromHexString(m.getWorkId()));

    Check.input(fOpenWorkContext == null);
    fOpenWorkContext = new OpenWorkContext(requestContext, m.getStudentNoticeChannelId(), false);

    ActorRef<SessionProvider.Message> sessionProvider = fContext.spawn(
        SessionProvider.Factory.makeBehavior(
            fSessionProviderFactory,
            workId,
            m.hasUserData() ? m.getUserData() : null,
            null,
            fConnection,
            fSessionProviderAdapter),
        SessionProvider.class.getSimpleName());
    fContext.watchWith(sessionProvider, SessionProviderTerminated.INSTANCE);

    return this;
  }

  private Behavior<Message> onReopenWork(ReopenWork m, RequestContext requestContext) {
    ByteString workId = Ids.tryFromHexString(m.getWorkId());
    ByteString sessionKey = hexStringToSessionKeyIfValid(m.getSessionKey());

    if ((workId == null) || (sessionKey == null)) {
      send(REOPEN_FAILED, requestContext);
    } else {
      Check.input(fOpenWorkContext == null);
      fOpenWorkContext = new OpenWorkContext(requestContext, m.getStudentNoticeChannelId(), true);

      ActorRef<SessionProvider.Message> sessionProvider = fContext.spawn(
          SessionProvider.Factory.makeBehavior(
              fSessionProviderFactory,
              workId,
              null,
              sessionKey,
              fConnection,
              fSessionProviderAdapter),
          SessionProvider.class.getSimpleName());
      fContext.watchWith(sessionProvider, SessionProviderTerminated.INSTANCE);
    }

    return this;
  }

  private static final class OpenWorkContext {
    final RequestContext requestContext;
    final long studentNoticeChannelId;
    final boolean reopening;
    /* @Nullable */SessionProvider.Result result;

    OpenWorkContext(RequestContext requestContext, long studentNoticeChannelId, boolean reopening) {
      this.requestContext = requestContext;
      this.studentNoticeChannelId = studentNoticeChannelId;
      this.reopening = reopening;
    }
  }

  private static final class SessionProviderResult implements Message {
    final SessionProvider.Result original;

    SessionProviderResult(SessionProvider.Result original) {
      this.original = original;
    }
  }

  private Behavior<Message> onSessionProviderResult(SessionProvider.Result m) {
    fOpenWorkContext.result = m;
    return this;
  }

  private enum SessionProviderTerminated implements Message { INSTANCE }

  private Behavior<Message> onSessionProviderTerminated() {
    if (fOpenWorkContext.result instanceof SessionProvider.Ok) {
      SessionProvider.Ok ok = (SessionProvider.Ok) fOpenWorkContext.result;

      fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
          StudentChannel.Factory.makeBehavior(
              fStudentChannelFactory,
              fConnection,
              fOpenWorkContext.studentNoticeChannelId,
              ok.sessionId,
              ok.workHub),
          ClientMessage::hasStudentRequest,
          (r, rc) -> new StudentChannel.RequestMessage(r.getStudentRequest(), rc),
          result -> new StudentChannelOpened(result, ok.sessionKey),
          fContext.getSelf()));
    } else {
      StudentWorkSelectorReply failureReply;
      if (fOpenWorkContext.reopening) {
        failureReply = REOPEN_FAILED;
      } else if (fOpenWorkContext.result == null) {
        failureReply = WORK_NOT_FOUND;
      } else {
        failureReply = StudentWorkSelectorReply.newBuilder()
            .setOpenWorkReply(((SessionProvider.Failure) fOpenWorkContext.result).failureReply)
            .build();
      }

      send(failureReply, fOpenWorkContext.requestContext);
      fOpenWorkContext = null;
    }

    return this;
  }

  private static final class StudentChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<StudentChannel.Message> result;
    final ByteString sessionKey;

    StudentChannelOpened(ConnectionHandler.SpawnChannelResult<StudentChannel.Message> result, ByteString sessionKey) {
      this.result = result;
      this.sessionKey = sessionKey;
    }
  }

  private Behavior<Message> onStudentChannelOpened(StudentChannelOpened m) {
    send(fOpenWorkContext.reopening ?
        StudentWorkSelectorReply.newBuilder()
            .setReopenWorkReply(ReopenWork.Reply.newBuilder()
                .setOk(ReopenWork.Reply.Ok.newBuilder()
                    .setStudentChannelId(m.result.channelId)))
            .build() :
        StudentWorkSelectorReply.newBuilder()
            .setOpenWorkReply(OpenWork.Reply.newBuilder()
                .setOk(OpenWork.Reply.Ok.newBuilder()
                    .setStudentChannelId(m.result.channelId)
                    .setSessionKey(sessionKeyToHexString(m.sessionKey))))
            .build(), fOpenWorkContext.requestContext);

    fContext.watchWith(
        m.result.channel,
        new StudentChannelTerminated(fOpenWorkContext.requestContext.id()));

    m.result.channel.tell(StudentChannel.Activate.INSTANCE);

    fOpenWorkContext = null;
    return this;
  }

  private static final class StudentChannelTerminated implements Message {
    final long noticeChannelId;

    StudentChannelTerminated(long noticeChannelId) {
      this.noticeChannelId = noticeChannelId;
    }
  }

  private Behavior<Message> onStudentChannelTerminated(StudentChannelTerminated m) {
    sendNotice(StudentWorkSelectorNotice.newBuilder()
        .setStudentChannelClosed(Empty.getDefaultInstance())
        .build(), m.noticeChannelId);

    return this;
  }

  private void send(StudentWorkSelectorReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setStudentWorkSelectorReply(reply)
        .build());
  }

  private void sendNotice(StudentWorkSelectorNotice notice, long noticeChannelId) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(noticeChannelId)
        .setStudentWorkSelectorNotice(notice)
        .build());
  }

  private static @Nullable ByteString hexStringToSessionKeyIfValid(String s) {
    byte[] ba;
    try {
      ba = BaseEncoding.base16().lowerCase().decode(s);
    } catch (IllegalArgumentException ignored) {
      ba = null;
    }

    return ((ba != null) && (ba.length == WorkUtils.SESSION_KEY_SIZE)) ? ByteString.copyFrom(ba) : null;
  }

  private static String sessionKeyToHexString(ByteString sessionKey) {
    return BaseEncoding.base16().lowerCase().encode(sessionKey.toByteArray());
  }
}
