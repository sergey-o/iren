/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;

import com.google.protobuf.Message;
import com.google.protobuf.TextFormat;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ChannelHandler.Sharable public final class MessageLogger extends ChannelDuplexHandler {
  private static final Logger fLogger = LoggerFactory.getLogger(MessageLogger.class);
  private static final MessageLogger fInstance = new MessageLogger();

  public static void addIfLoggingEnabled(ChannelPipeline pipeline) {
    if (fLogger.isTraceEnabled()) {
      pipeline.addLast(fInstance);
    }
  }

  private MessageLogger() {}

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if (msg instanceof Message) {
      fLogger.trace("Received from {}:{}{}", Utils.channelToString(ctx.channel()), System.lineSeparator(),
          TextFormat.printer().escapingNonAscii(false).printToString((Message) msg));
    }
    super.channelRead(ctx, msg);
  }

  @Override public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
    if (msg instanceof Message) {
      fLogger.trace("Sent to {}:{}{}", Utils.channelToString(ctx.channel()), System.lineSeparator(),
          TextFormat.printer().escapingNonAscii(false).printToString((Message) msg));
    }
    super.write(ctx, msg, promise);
  }
}
