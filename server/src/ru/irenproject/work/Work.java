/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;

import com.google.common.base.CharMatcher;
import com.google.protobuf.ByteString;
import com.google.protobuf.Message;

import javax.annotation.concurrent.Immutable;
import java.time.Instant;
import java.util.Locale;

@Immutable public final class Work {
  public static final CharMatcher BAD_TITLE_CHAR_MATCHER = CharMatcher.javaIsoControl().or(CharMatcher.anyOf("/\\"));

  private final ByteString fId;
  private final String fTitle;
  private final String fLanguage;
  private final Instant fStartedAt;
  private final Message fAuthenticationMode;

  private final Locale fLocale;

  public Work(ByteString id, String title, String language, Instant startedAt, Message authenticationMode) {
    Ids.checkValid(id);
    Check.that(BAD_TITLE_CHAR_MATCHER.matchesNoneOf(title));

    fId = id;
    fTitle = title;
    fLanguage = language;
    fStartedAt = startedAt;
    fAuthenticationMode = authenticationMode;

    fLocale = Locale.forLanguageTag(fLanguage);
  }

  public ByteString id() {
    return fId;
  }

  public String title() {
    return fTitle;
  }

  public String language() {
    return fLanguage;
  }

  public Instant startedAt() {
    return fStartedAt;
  }

  public Message authenticationMode() {
    return fAuthenticationMode;
  }

  public Locale locale() {
    return fLocale;
  }
}
