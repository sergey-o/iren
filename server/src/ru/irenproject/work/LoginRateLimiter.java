/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;

import com.google.common.base.Ascii;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Supplier;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton public final class LoginRateLimiter {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("failedLoginLimit")
    String failedLoginLimit = "5 30s";
  }

  private final int fFailureLimit;
  private final /* @Nullable */LoadingCache<String, AtomicInteger> fFailureCountersByAddress;

  @Inject private LoginRateLimiter(Config config) {
    if (Ascii.equalsIgnoreCase(config.failedLoginLimit, "off")) {
      fFailureLimit = 0;
      fFailureCountersByAddress = null;
    } else {
      List<String> parts = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings()
          .splitToList(config.failedLoginLimit);
      Check.that(parts.size() == 2, ConfigException::new);

      fFailureLimit = Check.notNull(Ints.tryParse(parts.get(0)), ConfigException::new);
      Check.that((fFailureLimit >= 1) && (fFailureLimit <= 1_000_000), ConfigException::new);

      Check.that(parts.get(1).endsWith("s"), ConfigException::new);
      int interval = Check.notNull(Ints.tryParse(StringUtils.substring(parts.get(1), 0, -1)), ConfigException::new);
      Check.that((interval >= 1) && (interval <= 1_000_000), ConfigException::new);

      fFailureCountersByAddress = CacheBuilder.newBuilder()
          .expireAfterWrite(interval, TimeUnit.SECONDS)
          .build(CacheLoader.from((Supplier<AtomicInteger>) AtomicInteger::new));
    }
  }

  private static final class ConfigException extends RuntimeException {
    ConfigException() {
      super("Incorrect 'failedLoginLimit' value.");
    }
  }

  private boolean limitEnabled() {
    return fFailureLimit > 0;
  }

  public boolean allowed(String address) {
    AtomicInteger counter = limitEnabled() ? fFailureCountersByAddress.getIfPresent(address) : null;
    return (counter == null) || (counter.get() < fFailureLimit);
  }

  public boolean registerAttempt(String address, boolean successful) {
    boolean res;

    if (!limitEnabled() || (successful && (fFailureCountersByAddress.getIfPresent(address) == null))) {
      res = true;
    } else {
      AtomicInteger counter = fFailureCountersByAddress.getUnchecked(address);
      res = (successful ? counter.get() : counter.getAndUpdate(n -> Integer.min(n + 1, fFailureLimit))) < fFailureLimit;
    }

    return res;
  }
}
