/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.DefaultDataDirectory;
import ru.irenproject.Oops;

import org.apache.commons.io.IOUtils;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Singleton public final class DataDirectory {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("dataDirectory")
    @Nullable String dataDirectory;
  }

  private final Path fPath;
  private final LockData fLockData;

  @Inject private DataDirectory(Config config) {
    fPath = (config.dataDirectory == null) ? DefaultDataDirectory.get() : Paths.get(config.dataDirectory);
    fLockData = lock(fPath);
  }

  private static LockData lock(Path dir) {
    try {
      FileChannel channel = FileChannel.open(dir.resolve("lockedByServer"),
          StandardOpenOption.CREATE, StandardOpenOption.WRITE);
      FileLock lock = null;
      try {
        lock = channel.tryLock();
        if (lock == null) {
          throw Oops.format("Another instance of the server is already using the directory '%s'.", dir);
        }
      } finally {
        if (lock == null) {
          IOUtils.closeQuietly(channel);
        }
      }
      return new LockData(channel, lock);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class LockData {
    final FileChannel channel;

    @SuppressWarnings("unused")
    final FileLock lock;

    LockData(FileChannel channel, FileLock lock) {
      this.channel = channel;
      this.lock = lock;
    }
  }

  public Path get() {
    return fPath;
  }

  public void unlock() {
    try {
      fLockData.channel.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
