/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Singleton public final class ArchiveLocation {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("archiveFile")
    @Nullable String archiveFile;
  }

  private final Config fConfig;
  private final DataDirectory fDataDirectory;

  @Inject private ArchiveLocation(
      Config config,
      DataDirectory dataDirectory) {
    fConfig = config;
    fDataDirectory = dataDirectory;
  }

  public @Nullable Path getIfDefined() {
    Path res;
    try {
      res = Paths.get((fConfig.archiveFile == null) ?
          Files.readString(fDataDirectory.get().resolve("archiveLocation")) :
          fConfig.archiveFile);
    } catch (IOException | RuntimeException ignored) {
      res = null;
    }
    return res;
  }
}
