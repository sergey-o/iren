/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.KryoFactory;
import ru.irenproject.infra.Realm;

import com.google.common.io.MoreFiles;
import com.google.inject.assistedinject.Assisted;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;

import javax.inject.Inject;

public final class WorkSourcePool implements AutoCloseable {
  public interface Factory {
    WorkSourcePool create(Work work);
  }

  private final GenericObjectPool<BorrowedWorkSource> fPool;

  @Inject private WorkSourcePool(
      @Assisted Work work,
      WorkDirectory workDirectory,
      KryoFactory kryoFactory) {
    WorkSourceFactory factory = new WorkSourceFactory(work, workDirectory, kryoFactory);
    fPool = new GenericObjectPool<>(factory, Utils.unboundedPoolConfig());
    factory.fPool = fPool; // this is safe as no other threads can access the pool or the factory yet
  }

  public ClosableWorkSource acquire() {
    try {
      return Utils.callWithRetriesUninterruptibly(fPool::borrowObject);
    } catch (Exception anyButInterruptedException) {
      throw new RuntimeException(anyButInterruptedException);
    }
  }

  @Override public void close() {
    fPool.close();
  }

  private static final class WorkSourceFactory extends BasePooledObjectFactory<BorrowedWorkSource> {
    final Work fWork;
    final WorkDirectory fWorkDirectory;
    final KryoFactory fKryoFactory;

    GenericObjectPool<BorrowedWorkSource> fPool;

    WorkSourceFactory(Work work, WorkDirectory workDirectory, KryoFactory kryoFactory) {
      fWork = work;
      fWorkDirectory = workDirectory;
      fKryoFactory = kryoFactory;
    }

    @Override public BorrowedWorkSource create() {
      Realm realm = new Realm(Utils::logUnexpectedEvent, null,
          new FileBlobStore(fWorkDirectory.getBlobStoreDirectory(fWork.id())));

      FrozenWorkSource frozenWorkSource = Utils.unfreeze(
          MoreFiles.asByteSource(fWorkDirectory.getFrozenWorkSourceFile(fWork.id())),
          fKryoFactory.createForReading(realm),
          FrozenWorkSource.class);

      return new BorrowedWorkSource(frozenWorkSource, Check.notNull(fPool));
    }

    @Override public PooledObject<BorrowedWorkSource> wrap(BorrowedWorkSource obj) {
      return new DefaultPooledObject<>(obj);
    }
  }

  private static final class BorrowedWorkSource extends ClosableWorkSource {
    final GenericObjectPool<BorrowedWorkSource> fPool;

    BorrowedWorkSource(FrozenWorkSource frozenWorkSource, GenericObjectPool<BorrowedWorkSource> pool) {
      super(frozenWorkSource);
      fPool = pool;
    }

    @Override public void close() {
      fPool.returnObject(this);
    }
  }
}
