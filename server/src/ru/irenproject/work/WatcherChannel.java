/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.SortDirection;
import ru.irenproject.Utils;
import ru.irenproject.common.authenticationMode.accountLogin.Proto.AccountLogin;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.watcher.Proto.ChangeTimeLimit;
import ru.irenproject.common.watcher.Proto.Detach;
import ru.irenproject.common.watcher.Proto.DiscoverSessionActions;
import ru.irenproject.common.watcher.Proto.ExportToTsv;
import ru.irenproject.common.watcher.Proto.ModifySessionSelection;
import ru.irenproject.common.watcher.Proto.QuestionDelta;
import ru.irenproject.common.watcher.Proto.Resume;
import ru.irenproject.common.watcher.Proto.SessionCount;
import ru.irenproject.common.watcher.Proto.SessionDelta;
import ru.irenproject.common.watcher.Proto.SessionOverview;
import ru.irenproject.common.watcher.Proto.SessionOverviewDelta;
import ru.irenproject.common.watcher.Proto.SessionOverviewOrder;
import ru.irenproject.common.watcher.Proto.SessionOverviewSortField;
import ru.irenproject.common.watcher.Proto.SetBanned;
import ru.irenproject.common.watcher.Proto.SetSessionOverviewOrder;
import ru.irenproject.common.watcher.Proto.UpdateWatchedSessionOverviewSet;
import ru.irenproject.common.watcher.Proto.WatchQuestion;
import ru.irenproject.common.watcher.Proto.WatchSelectedSession;
import ru.irenproject.common.watcher.Proto.WatcherHello;
import ru.irenproject.common.watcher.Proto.WatcherNotice;
import ru.irenproject.common.watcher.Proto.WatcherReply;
import ru.irenproject.common.watcher.Proto.WatcherRequest;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.LiveQuestionLoader;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;
import com.google.common.math.IntMath;
import com.google.common.primitives.UnsignedBytes;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.util.Timestamps;
import org.apache.commons.collections4.list.TreeList;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.math.BigInteger;
import java.text.CollationKey;
import java.text.Collator;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.RandomAccess;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

public final class WatcherChannel extends AbstractBehavior<WatcherChannel.Message> {
  public interface Message {}

  public enum Activate implements Message { INSTANCE }

  public static final class RequestMessage implements Message {
    public final WatcherRequest request;
    public final RequestContext requestContext;

    public RequestMessage(WatcherRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    WatcherChannel create(
        ActorContext<Message> context,
        ByteString workId,
        Connection connection,
        long noticeChannelId,
        Locale locale);

    static Behavior<Message> makeBehavior(
        Factory factory,
        ByteString workId,
        Connection connection,
        long noticeChannelId,
        Locale locale) {
      return Behaviors.setup(ctx -> factory.create(ctx, workId, connection, noticeChannelId, locale));
    }
  }

  private static final SessionOverviewOrder INITIAL_ORDER = SessionOverviewOrder.newBuilder()
      .setSortField(SessionOverviewSortField.RESULT)
      .setSortAscending(false)
      .build();

  private static final Duration MIN_TIME_LIMIT = Duration.ofMinutes(1);
  private static final Duration MAX_TIME_LIMIT = Duration.ofHours(23);

  private final ActorContext<Message> fContext;
  private final ByteString fWorkId;
  private final Connection fConnection;
  private final long fNoticeChannelId;
  private final Collator fCollator;
  private final ActorRef<WorkCatalog.Message> fWorkCatalog;
  private final LiveQuestionLoader fLiveQuestionLoader;

  private ActorRef<WorkHub.Message> fWorkHub;
  private LiveWork fLiveWork;

  private final HashMap<Long, SessionOverview> fSessionOverviewsBySessionId = new HashMap<>();

  private final RandomAccessAnnouncingTreeList<SessionOverview> fSortedSessionOverviews =
      new RandomAccessAnnouncingTreeList<>();
  private Comparator<SessionOverview> fComparator;

  private final IdentityHashMap<SessionOverview, CollationKey> fUserDisplayNameCollationKeyCache =
      new IdentityHashMap<>();
  private final IdentityHashMap<SessionOverview, CollationKey> fGroupNameCollationKeyCache = new IdentityHashMap<>();
  private final IdentityHashMap<SessionOverview, byte[]> fAddressCollationKeyCache = new IdentityHashMap<>();

  private final LinkedHashSet<Long> fSelectedSessionIds = new LinkedHashSet<>();

  private final LinkedHashMap<Integer, SessionOverview> fWatchedSessionOverviewsByIndex = new LinkedHashMap<>();
  private final HashSet<Integer> fWatchedSelectedSessionIndices = new HashSet<>();

  private /* @Nullable */ActorRef<SessionDelta> fSessionDeltaSender;
  private /* @Nullable */ActorRef<QuestionDelta> fQuestionDeltaSender;

  @Inject private WatcherChannel(
      @Assisted ActorContext<Message> context,
      @Assisted ByteString workId,
      @Assisted Connection connection,
      @Assisted long noticeChannelId,
      @Assisted Locale locale,
      ActorRef<WorkCatalog.Message> workCatalog,
      LiveQuestionLoader liveQuestionLoader) {
    fContext = context;
    fWorkId = workId;
    fConnection = connection;
    fNoticeChannelId = noticeChannelId;
    fCollator = Collator.getInstance(locale);
    fWorkCatalog = workCatalog;
    fLiveQuestionLoader = liveQuestionLoader;

    fComparator = createComparator(INITIAL_ORDER);
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessageEquals(Activate.INSTANCE, this::onActivate)
        .onMessage(WorkHubObtained.class, this::onWorkHubObtained)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(Registered.class, this::onRegistered)
        .onMessage(SessionListDelta.class, m -> onSessionListDelta(m.original))
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessage(SendReply.class, this::onSendReply)
        .build();
  }

  private static final class RandomAccessAnnouncingTreeList<E> extends TreeList<E> implements RandomAccess {}

  private Behavior<Message> onActivate() {
    fWorkCatalog.tell(new WorkCatalog.Get<>(
        fWorkId,
        WorkHubObtained::new,
        fContext.getSelf()));

    return this;
  }

  private static final class WorkHubObtained implements Message {
    final @Nullable ActorRef<WorkHub.Message> workHub;

    WorkHubObtained(@Nullable ActorRef<WorkHub.Message> workHub) {
      this.workHub = workHub;
    }
  }

  private Behavior<Message> onWorkHubObtained(WorkHubObtained m) {
    Behavior<Message> res;
    if (m.workHub == null) {
      res = Behaviors.stopped();
    } else {
      fWorkHub = m.workHub;
      fContext.watchWith(fWorkHub, Stop.INSTANCE);

      fWorkHub.tell(new WorkHub.RegisterParticipant<>(
          null,
          fContext.getSelf(),
          Stop.INSTANCE,
          Registered::new));
      res = this;
    }

    return res;
  }

  private enum Stop implements Message { INSTANCE }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }

  private static final class Registered implements Message {
    final LiveWork liveWork;

    Registered(LiveWork liveWork) {
      this.liveWork = liveWork;
    }
  }

  private Behavior<Message> onRegistered(Registered m) {
    fLiveWork = m.liveWork;

    enqueueNotice(WatcherNotice.newBuilder()
        .setWatcherHello(WatcherHello.newBuilder()
            .setWorkTitle(fLiveWork.work().title())
            .setServerTimeMilliseconds(System.currentTimeMillis())
            .setOrder(INITIAL_ORDER))
        .build());

    fWorkHub.tell(new WorkHub.TrackSessionList(
        fContext.messageAdapter(WorkHub.SessionListDelta.class, SessionListDelta::new)));

    return this;
  }

  private void enqueue(WatcherReply reply, RequestContext requestContext) {
    fConnection.enqueue(toServerMessage(reply, requestContext));
  }

  private void send(WatcherReply reply, RequestContext requestContext) {
    fConnection.send(toServerMessage(reply, requestContext));
  }

  private static ServerMessage toServerMessage(WatcherReply reply, RequestContext requestContext) {
    return ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setWatcherReply(reply)
        .build();
  }

  private void enqueueNotice(WatcherNotice notice) {
    fConnection.enqueue(toNoticeServerMessage(notice));
  }

  private void sendNotice(WatcherNotice notice) {
    fConnection.send(toNoticeServerMessage(notice));
  }

  private ServerMessage toNoticeServerMessage(WatcherNotice notice) {
    return ServerMessage.newBuilder()
        .setChannelId(fNoticeChannelId)
        .setWatcherNotice(notice)
        .build();
  }

  private static final class SessionListDelta implements Message {
    final WorkHub.SessionListDelta original;

    SessionListDelta(WorkHub.SessionListDelta original) {
      this.original = original;
    }
  }

  private Behavior<Message> onSessionListDelta(WorkHub.SessionListDelta m) {
    int oldSessionCount = fSortedSessionOverviews.size();
    boolean changed;

    try (ClosableWorkSource workSource = fLiveWork.acquireSource()) {
      MarkScale markScale = workSource.profile().markScale();

      if (fSortedSessionOverviews.isEmpty()) { // optimization for faster initial fillup
        //noinspection ConstantConditions
        fSessionOverviewsBySessionId.putAll(Maps.transformEntries(m.sessionsById,
            (sessionId, session) -> WorkUtils.buildSessionOverview(sessionId, session, markScale)));
        sort();
        changed = true;
      } else {
        changed = false;
        for (Map.Entry<Long, FullSession> e : m.sessionsById.entrySet()) {
          Long sessionId = e.getKey();
          SessionOverview current = WorkUtils.buildSessionOverview(sessionId, e.getValue(), markScale);

          boolean addCurrent;
          SessionOverview old = fSessionOverviewsBySessionId.get(sessionId);
          if (old == null) {
            addCurrent = true;
          } else if (current.equals(old)) {
            addCurrent = false;
          } else {
            fSortedSessionOverviews.remove(findExistingIndex(old));
            dropCachedData(old);
            addCurrent = true;
          }

          if (addCurrent) {
            fSessionOverviewsBySessionId.put(sessionId, current);
            int currentPosition = findPosition(current);
            Check.that(currentPosition < 0);
            fSortedSessionOverviews.add(~currentPosition, current);
            changed = true;
          }
        }
      }
    }

    if (changed) {
      if (fSortedSessionOverviews.size() != oldSessionCount) {
        enqueueNotice(WatcherNotice.newBuilder()
            .setSessionCount(SessionCount.newBuilder()
                .setValue(fSortedSessionOverviews.size()))
            .build());
      }

      enqueueSessionOverviewDeltaIfPresent();

      fConnection.flush();
    }

    return this;
  }

  private void sort() {
    Check.that(fSortedSessionOverviews.isEmpty());
    fSortedSessionOverviews.addAll(fSessionOverviewsBySessionId.values().stream()
        .sorted(fComparator)
        .collect(Collectors.toList()));
  }

  private int findExistingIndex(SessionOverview sessionOverview) {
    int res = findPosition(sessionOverview);
    Check.that(res >= 0);
    return res;
  }

  private int findPosition(SessionOverview sessionOverview) {
    return Collections.binarySearch(fSortedSessionOverviews, sessionOverview, fComparator);
  }

  private void enqueueSessionOverviewDeltaIfPresent() {
    SessionOverviewDelta.Builder delta = SessionOverviewDelta.newBuilder();

    for (Map.Entry<Integer, SessionOverview> e : fWatchedSessionOverviewsByIndex.entrySet()) {
      int index = e.getKey();
      boolean changed = false;

      SessionOverview dataOnClient = e.getValue();
      SessionOverview data = fSortedSessionOverviews.get(index);
      if (data != dataOnClient) {
        e.setValue(data);
        changed = true;
      }

      boolean selectedOnClient = fWatchedSelectedSessionIndices.contains(index);
      boolean selected = fSelectedSessionIds.contains(data.getId());
      if (selected != selectedOnClient) {
        Utils.setPresence(fWatchedSelectedSessionIndices, index, selected);
        changed = true;
      }

      if (changed) {
        delta.addItem(SessionOverviewDelta.Item.newBuilder()
            .setIndex(index)
            .setSessionOverview(data)
            .setSelected(selected));
      }
    }

    if (delta.getItemCount() > 0) {
      enqueueNotice(WatcherNotice.newBuilder()
          .setSessionOverviewDelta(delta)
          .build());
    }
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    Check.inputNotNull(fLiveWork);

    WatcherRequest r = m.request;
    switch (r.getRequestCase()) {
      case CLOSE: return onClose();
      case UPDATE_WATCHED_SESSION_OVERVIEW_SET: return onUpdateWatchedSessionOverviewSet(
          r.getUpdateWatchedSessionOverviewSet());
      case SET_SESSION_OVERVIEW_ORDER: return onSetSessionOverviewOrder(
          r.getSetSessionOverviewOrder(), m.requestContext);
      case WATCH_SELECTED_SESSION: return onWatchSelectedSession(m.requestContext);
      case UNWATCH_SESSION: return onUnwatchSession();
      case WATCH_QUESTION: return onWatchQuestion(r.getWatchQuestion(), m.requestContext);
      case UNWATCH_QUESTION: return onUnwatchQuestion();
      case EXPORT_TO_TSV: return onExportToTsv(r.getExportToTsv(), m.requestContext);
      case MODIFY_SESSION_SELECTION: return onModifySessionSelection(r.getModifySessionSelection(), m.requestContext);
      case SET_BANNED: return onSetBanned(r.getSetBanned(), m.requestContext);
      case RESUME: return onResume(m.requestContext);
      case CHANGE_TIME_LIMIT: return onChangeTimeLimit(r.getChangeTimeLimit(), m.requestContext);
      case DETACH: return onDetach(m.requestContext);
      case DISCOVER_SESSION_ACTIONS: return onDiscoverSessionActions(m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onClose() {
    return Behaviors.stopped();
  }

  private Behavior<Message> onUpdateWatchedSessionOverviewSet(UpdateWatchedSessionOverviewSet m) {
    LinkedHashMap<Integer, SessionOverview> forSend = new LinkedHashMap<>();

    for (UpdateWatchedSessionOverviewSet.Item item : m.getItemList()) {
      int index = item.getIndex();
      Check.input(index >= 0);
      Check.input(index < fSortedSessionOverviews.size());

      if (item.getWatch()) {
        SessionOverview sessionOverview = fSortedSessionOverviews.get(index);
        Check.input(fWatchedSessionOverviewsByIndex.put(index, sessionOverview) == null);
        if (fSelectedSessionIds.contains(sessionOverview.getId())) {
          fWatchedSelectedSessionIndices.add(index);
        }
        forSend.put(index, sessionOverview);
      } else {
        Check.inputNotNull(fWatchedSessionOverviewsByIndex.remove(index));
        fWatchedSelectedSessionIndices.remove(index);
        forSend.remove(index);
      }
    }

    if (!forSend.isEmpty()) {
      sendNotice(WatcherNotice.newBuilder()
          .setSessionOverviewDelta(SessionOverviewDelta.newBuilder()
              .addAllItem(forSend.entrySet().stream()
                  .map(e -> SessionOverviewDelta.Item.newBuilder()
                      .setIndex(e.getKey())
                      .setSessionOverview(e.getValue())
                      .setSelected(fSelectedSessionIds.contains(e.getValue().getId()))
                      .build())
                  .collect(Collectors.toList())))
          .build());
    }

    return this;
  }

  private Behavior<Message> onSetSessionOverviewOrder(SetSessionOverviewOrder m, RequestContext requestContext) {
    fSortedSessionOverviews.clear();
    fComparator = createComparator(m.getOrder());
    sort();

    enqueueSessionOverviewDeltaIfPresent();

    send(WatcherReply.newBuilder()
        .setSetSessionOverviewOrderReply(SetSessionOverviewOrder.Reply.getDefaultInstance())
        .build(), requestContext);

    return this;
  }

  private Comparator<SessionOverview> createComparator(SessionOverviewOrder order) {
    Comparator<SessionOverview> comparator = createPrimaryComparator(order);
    if (order.getSortField() != SessionOverviewSortField.USER_NAME) {
      comparator = comparator.thenComparing(this::getUserDisplayNameCollationKey);
    }
    return comparator.thenComparingLong(SessionOverview::getId);
  }

  private Comparator<SessionOverview> createPrimaryComparator(SessionOverviewOrder order) {
    SortDirection d = SortDirection.get(order.getSortAscending());

    switch (order.getSortField()) {
      case USER_NAME: return Comparator.comparing(
          this::getUserDisplayNameCollationKey,
          d.apply(Comparator.<CollationKey>naturalOrder()));
      case GROUP_NAME: return Comparator.comparing(
          this::getGroupNameCollationKeyIfPresent,
          Comparator.nullsLast(
              d.apply(Comparator.<CollationKey>naturalOrder())));
      case RESULT: return d.apply(WatcherChannel::compareResults);
      case DEADLINE: return Comparator.comparing(
          WatcherChannel::getDeadlineMillisecondsIfPresent,
          Comparator.nullsLast(
              d.apply(Comparator.<Double>naturalOrder())));
      case OPENED_AT: return Comparator.comparing(
          SessionOverview::getOpenedAt,
          d.apply(Timestamps.comparator()));
      case ADDRESS: return Comparator.comparing(
          this::getAddressCollationKey,
          d.apply(UnsignedBytes.lexicographicalComparator()));
      default: throw new RuntimeException();
    }
  }

  private CollationKey getUserDisplayNameCollationKey(SessionOverview so) {
    return fUserDisplayNameCollationKeyCache.computeIfAbsent(so,
        _so -> fCollator.getCollationKey(_so.getUserDisplayName()));
  }

  private @Nullable CollationKey getGroupNameCollationKeyIfPresent(SessionOverview so) {
    return so.hasGroupName() ?
        fGroupNameCollationKeyCache.computeIfAbsent(so, _so -> fCollator.getCollationKey(_so.getGroupName())) :
        null;
  }

  private static @Nullable Double getDeadlineMillisecondsIfPresent(SessionOverview so) {
    return so.hasDeadlineMilliseconds() ? so.getDeadlineMilliseconds() : null;
  }

  private byte[] getAddressCollationKey(SessionOverview so) {
    return fAddressCollationKeyCache.computeIfAbsent(so, _so -> Utils.getIpAddressCollationKey(_so.getAddress()));
  }

  private void dropCachedData(SessionOverview so) {
    fUserDisplayNameCollationKeyCache.remove(so);
    fGroupNameCollationKeyCache.remove(so);
    fAddressCollationKeyCache.remove(so);
  }

  private static int compareResults(SessionOverview a, SessionOverview b) {
    return (a.getPerfectScore() == b.getPerfectScore()) ?
        Long.compare(a.getScaledScore(), b.getScaledScore()) :
        compareFractions(
            BigInteger.valueOf(a.getScaledScore()), BigInteger.valueOf(a.getPerfectScore()),
            BigInteger.valueOf(b.getScaledScore()), BigInteger.valueOf(b.getPerfectScore()));
  }

  private static int compareFractions(BigInteger n1, BigInteger d1, BigInteger n2, BigInteger d2) {
    return n1.multiply(d2).compareTo(n2.multiply(d1));
  }

  private Behavior<Message> onWatchSelectedSession(RequestContext requestContext) {
    Check.input(fSelectedSessionIds.size() == 1);
    long sessionId = fSelectedSessionIds.iterator().next();
    Session session = fLiveWork.store().read(WorkUtils.loadSession(sessionId));

    unwatchSession();

    enqueue(WatcherReply.newBuilder()
        .setWatchSelectedSessionReply(WatchSelectedSession.Reply.newBuilder()
            .setSessionId(sessionId)
            .addAllQuestionWeight(session.getQuestionList().stream()
                .map(SessionQuestion::getWeight)
                .collect(Collectors.toList()))
            .setCurrentQuestionIndex(session.getCurrentQuestionIndex()))
        .build(), requestContext);

    fSessionDeltaSender = fContext.spawn(
        SessionDeltaSender.makeBehavior(fConnection, requestContext.id()),
        Utils.makeUniqueChildActorName(SessionDeltaSender.class.getSimpleName(), fContext));

    fWorkHub.tell(new WorkHub.TrackSession(
        sessionId,
        fSessionDeltaSender));

    return this;
  }

  private static final class SessionDeltaSender {
    static Behavior<SessionDelta> makeBehavior(Connection connection, long noticeChannelId) {
      return Behaviors.receiveMessage(m -> onSessionDelta(m, connection, noticeChannelId));
    }

    static Behavior<SessionDelta> onSessionDelta(SessionDelta m, Connection connection, long noticeChannelId) {
      connection.send(ServerMessage.newBuilder()
          .setChannelId(noticeChannelId)
          .setWatcherNotice(WatcherNotice.newBuilder()
              .setSessionDelta(m))
          .build());
      return Behaviors.same();
    }
  }

  private Behavior<Message> onUnwatchSession() {
    unwatchSession();
    return this;
  }

  private void unwatchSession() {
    if (fSessionDeltaSender != null) {
      fContext.stop(fSessionDeltaSender);
      fSessionDeltaSender = null;
    }
  }

  private Behavior<Message> onWatchQuestion(WatchQuestion m, RequestContext requestContext) {
    Session session = Check.inputNotNull(fLiveWork.store().read(WorkUtils.loadSessionIfExists(m.getSessionId())));
    BaseWorkUtils.checkQuestionIndex(m.getQuestionIndex(), session);

    unwatchQuestion();

    SessionQuestion sq = session.getQuestion(m.getQuestionIndex());

    _LiveQuestion liveQuestionData = fLiveWork.store().read(WorkUtils.loadRawScreen(sq.getScreenId()))
        .liveQuestionData;

    DialogResponse emptyResponse;

    try (ClosableWorkSource workSource = fLiveWork.acquireSource()) {
      LiveQuestion liveQuestion = fLiveQuestionLoader.load(liveQuestionData, workSource::getQuestion);
      emptyResponse = liveQuestion.getEmptyResponse();

      enqueue(WatcherReply.newBuilder()
          .setWatchQuestionReply(WatchQuestion.Reply.newBuilder()
              .setDialog(liveQuestion.render())
              .setCorrectResponse(liveQuestion.getCorrectResponse())
              .setWeight(sq.getWeight()))
          .build(), requestContext);
    }

    fQuestionDeltaSender = fContext.spawn(
        QuestionDeltaSender.makeBehavior(fConnection, requestContext.id()),
        Utils.makeUniqueChildActorName(QuestionDeltaSender.class.getSimpleName(), fContext));

    fWorkHub.tell(new WorkHub.TrackQuestion(
        m.getSessionId(),
        m.getQuestionIndex(),
        emptyResponse,
        fQuestionDeltaSender));

    return this;
  }

  private static final class QuestionDeltaSender {
    static Behavior<QuestionDelta> makeBehavior(Connection connection, long noticeChannelId) {
      return Behaviors.receiveMessage(m -> onQuestionDelta(m, connection, noticeChannelId));
    }

    static Behavior<QuestionDelta> onQuestionDelta(QuestionDelta m, Connection connection, long noticeChannelId) {
      connection.send(ServerMessage.newBuilder()
          .setChannelId(noticeChannelId)
          .setWatcherNotice(WatcherNotice.newBuilder()
              .setQuestionDelta(m))
          .build());
      return Behaviors.same();
    }
  }

  private Behavior<Message> onUnwatchQuestion() {
    unwatchQuestion();
    return this;
  }

  private void unwatchQuestion() {
    if (fQuestionDeltaSender != null) {
      fContext.stop(fQuestionDeltaSender);
      fQuestionDeltaSender = null;
    }
  }

  private Behavior<Message> onExportToTsv(ExportToTsv m, RequestContext requestContext) {
    Check.input(m.getHeaderCount() == 5);

    DateTimeFormatter openedAtFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")
        .withZone(Utils.parseTimeZone(m.getTimeZone(), BadInputException::new));

    StringBuilder tsv = new StringBuilder();
    Joiner tabber = Joiner.on('\t');
    CharMatcher unexportable = CharMatcher.anyOf("\t\n\r");
    int percentScale = IntMath.checkedPow(10, LiveQuestion.SCORE_FRACTION_DIGITS - 2);

    tabber.appendTo(tsv, m.getHeaderList());
    tsv.append('\n');

    for (SessionOverview so : fSortedSessionOverviews) {
      tabber.appendTo(tsv,
          so.getUserDisplayName(),
          so.hasGroupName() ? so.getGroupName() : "",
          Integer.toString(so.getScaledResult() / percentScale),
          unexportable.removeFrom(so.getMark()),
          openedAtFormatter.format(Utils.timestampToInstant(so.getOpenedAt())));
      tsv.append('\n');
    }

    send(WatcherReply.newBuilder()
        .setExportToTsvReply(ExportToTsv.Reply.newBuilder()
            .setTsv(tsv.toString()))
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onModifySessionSelection(ModifySessionSelection m, RequestContext requestContext) {
    SessionOverview target = Check.inputNotNull(fSessionOverviewsBySessionId.get(m.getSessionId()));

    if (m.getToggle()) {
      Utils.toggle(fSelectedSessionIds, m.getSessionId());
    } else if (m.getRange()) {
      if (fSelectedSessionIds.size() == 1) {
        int startIndex = findExistingIndex(fSessionOverviewsBySessionId.get(fSelectedSessionIds.iterator().next()));
        int endIndex = findExistingIndex(target);
        Iterator<SessionOverview> i = Iterators.limit(
            fSortedSessionOverviews.listIterator(Integer.min(startIndex, endIndex)),
            Math.abs(startIndex - endIndex) + 1);
        while (i.hasNext()) {
          fSelectedSessionIds.add(i.next().getId());
        }
      }
    } else {
      fSelectedSessionIds.clear();
      fSelectedSessionIds.add(m.getSessionId());
    }

    enqueueSessionOverviewDeltaIfPresent();

    send(WatcherReply.newBuilder()
        .setModifySessionSelectionReply(ModifySessionSelection.Reply.newBuilder()
            .setSelectedSessionCount(fSelectedSessionIds.size()))
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onSetBanned(SetBanned m, RequestContext requestContext) {
    fWorkHub.tell(new WorkHub.UpdateSessionStatus<>(
        ImmutableSet.copyOf(fSelectedSessionIds),
        setBanned(m.getBanned()),
        new SendReply(WatcherReply.newBuilder()
            .setSetBannedReply(SetBanned.Reply.getDefaultInstance())
            .build(), requestContext),
        fContext.getSelf()));

    return this;
  }

  private static UnaryOperator<SessionStatus> setBanned(boolean banned) {
    return status -> status
        .toBuilder()
        .setBanned(banned)
        .buildPartial();
  }

  private static final class SendReply implements Message {
    final WatcherReply reply;
    final RequestContext requestContext;

    SendReply(WatcherReply reply, RequestContext requestContext) {
      this.reply = reply;
      this.requestContext = requestContext;
    }
  }

  private Behavior<Message> onSendReply(SendReply m) {
    send(m.reply, m.requestContext);
    return this;
  }

  private Behavior<Message> onResume(RequestContext requestContext) {
    fWorkHub.tell(new WorkHub.UpdateSessionStatus<>(
        ImmutableSet.copyOf(fSelectedSessionIds),
        resume(),
        new SendReply(WatcherReply.newBuilder()
            .setResumeReply(Resume.Reply.getDefaultInstance())
            .build(), requestContext),
        fContext.getSelf()));

    return this;
  }

  private static UnaryOperator<SessionStatus> resume() {
    return status -> canResume(status) ?
        status
            .toBuilder()
            .setFinished(false)
            .buildPartial() :
        status;
  }

  private static boolean canResume(SessionStatus status) {
    return status.getFinished() && !BaseWorkUtils.isTimedOut(status);
  }

  private static boolean canResumeOverviewedSession(SessionOverview so) {
    return so.getFinished() && !WorkUtils.isOverviewedSessionTimedOut(so);
  }

  private Behavior<Message> onChangeTimeLimit(ChangeTimeLimit m, RequestContext requestContext) {
    fWorkHub.tell(new WorkHub.UpdateSessionStatus<>(
        ImmutableSet.copyOf(fSelectedSessionIds),
        changeTimeLimit(m, Utils.milliClock().instant()),
        new SendReply(WatcherReply.newBuilder()
            .setChangeTimeLimitReply(ChangeTimeLimit.Reply.getDefaultInstance())
            .build(), requestContext),
        fContext.getSelf()));

    return this;
  }

  private static UnaryOperator<SessionStatus> changeTimeLimit(ChangeTimeLimit m, Instant now) {
    return status -> {
      SessionStatus res;
      if (m.hasValue()) {
        if (m.getValue().getRelative()) {
          if (status.hasDeadline()) {
            Instant oldDeadline = Instant.ofEpochMilli(status.getDeadline());
            Duration delta = Duration.ofMinutes(m.getValue().getLimitMinutes());
            if (delta.isNegative()) {
              Instant minDeadline = now.plus(MIN_TIME_LIMIT);
              res = (oldDeadline.compareTo(minDeadline) > 0) ?
                  status.toBuilder()
                      .setDeadline(Ordering.natural().max(
                          oldDeadline.plus(delta),
                          minDeadline).toEpochMilli())
                      .buildPartial() :
                  status;
            } else if (delta.isZero()) {
              res = status;
            } else {
              Instant base = Ordering.natural().max(oldDeadline, now);
              Instant maxDeadline = now.plus(MAX_TIME_LIMIT);
              res = status.toBuilder()
                  .setDeadline(Ordering.natural().min(
                      base.plus(delta),
                      maxDeadline).toEpochMilli())
                  .setFinished(false)
                  .buildPartial();
            }
          } else {
            res = status;
          }
        } else {
          Duration limit = Ordering.natural().min(
              Duration.ofMinutes(Integer.max(m.getValue().getLimitMinutes(), 1)),
              MAX_TIME_LIMIT);
          res = status.toBuilder()
              .setDeadline(now.plus(limit).toEpochMilli())
              .setFinished(false)
              .buildPartial();
        }
      } else {
        res = status.toBuilder()
            .clearDeadline()
            .setFinished(false)
            .buildPartial();
      }

      return res;
    };
  }

  private Behavior<Message> onDetach(RequestContext requestContext) {
    Check.input(detachingSupported());

    fWorkHub.tell(new WorkHub.Detach<>(
        ImmutableSet.copyOf(fSelectedSessionIds),
        new SendReply(WatcherReply.newBuilder()
            .setDetachReply(Detach.Reply.getDefaultInstance())
            .build(), requestContext),
        fContext.getSelf()));

    return this;
  }

  private boolean detachingSupported() {
    return !(fLiveWork.work().authenticationMode() instanceof AccountLogin);
  }

  private Behavior<Message> onDiscoverSessionActions(RequestContext requestContext) {
    boolean canBan = false;
    boolean canUnban = false;
    boolean canResume = false;

    for (Long sessionId : fSelectedSessionIds) {
      SessionOverview so = fSessionOverviewsBySessionId.get(sessionId);

      canBan = canBan || !so.getBanned();
      canUnban = canUnban || so.getBanned();
      canResume = canResume || canResumeOverviewedSession(so);

      if (canBan && canUnban && canResume) {
        break;
      }
    }

    send(WatcherReply.newBuilder()
        .setDiscoverSessionActionsReply(DiscoverSessionActions.Reply.newBuilder()
            .setCanBan(canBan)
            .setCanUnban(canUnban)
            .setCanResume(canResume)
            .setCanDetach(detachingSupported() && !fSelectedSessionIds.isEmpty()))
        .build(), requestContext);

    return this;
  }
}
