/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Test;
import ru.irenproject.profile.Profile;

import java.util.HashMap;

public class WorkSource {
  private final Test fTest;
  private final Profile fProfile;

  private final HashMap<String, QuestionItem> fQuestionItemsByQuestionId = new HashMap<>();
  private final HashMap<QuestionItem, Section> fSections = new HashMap<>();

  public WorkSource(FrozenWorkSource frozenWorkSource) {
    fTest = frozenWorkSource.test;
    fProfile = frozenWorkSource.profile;

    for (Section section : fTest.root().listTreeSections()) {
      for (QuestionItem item : section.questionList().items()) {
        fQuestionItemsByQuestionId.put(Long.toString(item.question().id()), item);
        fSections.put(item, section);
      }
    }
  }

  public final Test test() {
    return fTest;
  }

  public final Profile profile() {
    return fProfile;
  }

  public final QuestionItem getQuestionItem(String source) {
    return Check.notNull(fQuestionItemsByQuestionId.get(source));
  }

  public final Question getQuestion(String source) {
    return getQuestionItem(source).question();
  }

  public final Section getSection(QuestionItem questionItem) {
    return Check.notNull(fSections.get(questionItem));
  }
}
