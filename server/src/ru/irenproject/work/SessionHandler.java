/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.Section;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.miscTypes.Proto.QuestionDescriptor;
import ru.irenproject.common.profile.Proto.AvailableQuestionScore;
import ru.irenproject.common.profile.Proto.AvailableScore;
import ru.irenproject.common.profile.Proto.AvailableSectionScore;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.student.Proto.ChooseQuestion;
import ru.irenproject.common.student.Proto.Closing;
import ru.irenproject.common.student.Proto.MainScreenTurnOn;
import ru.irenproject.common.student.Proto.QueryQuestionDetails;
import ru.irenproject.common.student.Proto.QuestionScore;
import ru.irenproject.common.student.Proto.SectionOutcome;
import ru.irenproject.common.student.Proto.SectionScore;
import ru.irenproject.common.student.Proto.SetResponse;
import ru.irenproject.common.student.Proto.ShowCurrentResult;
import ru.irenproject.common.student.Proto.ShowDialog;
import ru.irenproject.common.student.Proto.ShowQuestionDetails;
import ru.irenproject.common.student.Proto.ShowQuestionStatus;
import ru.irenproject.common.student.Proto.ShowRemainingTime;
import ru.irenproject.common.student.Proto.ShowScore;
import ru.irenproject.common.student.Proto.ShowScoreScreen;
import ru.irenproject.common.student.Proto.StudentNotice;
import ru.irenproject.common.student.Proto.SubmitOk;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.LiveQuestionLoader;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;

import com.google.auto.factory.AutoFactory;
import com.google.auto.factory.Provided;
import com.google.common.math.IntMath;
import com.google.common.primitives.Ints;
import com.google.protobuf.Any;
import com.google.protobuf.Empty;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@AutoFactory public final class SessionHandler {
  public interface RawScreenLoader {
    RawScreen load(long screenId);
  }

  public interface BatchQuestionSourceLoader {
    List<String> load(Session session);
  }

  public static final class SessionStatusDeltaResult {
    public final boolean proceed;

    public SessionStatusDeltaResult(boolean proceed) {
      this.proceed = proceed;
    }
  }

  public static final class SetResponseResult {
    public final Session session;

    public SetResponseResult(Session session) {
      this.session = session;
    }
  }

  public static final class SubmitResult {
    public final Session session;
    public final UpdateResponse updateResponse;
    public final ContinueSubmit continueSubmit;

    public SubmitResult(Session session, UpdateResponse updateResponse, ContinueSubmit continueSubmit) {
      this.session = session;
      this.updateResponse = updateResponse;
      this.continueSubmit = continueSubmit;
    }
  }

  public static final class ContinueSubmit {
    public final StudentNotice showQuestionStatusNotice;
    public final boolean newQuestionSelected;

    public ContinueSubmit(StudentNotice showQuestionStatusNotice, boolean newQuestionSelected) {
      this.showQuestionStatusNotice = showQuestionStatusNotice;
      this.newQuestionSelected = newQuestionSelected;
    }
  }

  public static final class ChooseQuestionResult {
    public final Session session;

    public ChooseQuestionResult(Session session) {
      this.session = session;
    }
  }

  public static final class FinishWorkResult {
    public final Session session;
    public final @Nullable UpdateResponse updateResponse;

    public FinishWorkResult(Session session, @Nullable UpdateResponse updateResponse) {
      this.session = session;
      this.updateResponse = updateResponse;
    }
  }

  private enum Mode { MAIN, SCORE }

  private final ConnectionLike fConnection;
  private final long fNoticeChannelId;
  private final boolean fCheckForTimeout;
  private final Supplier<ClosableWorkSource> fClosableWorkSourceSupplier;
  private final RawScreenLoader fRawScreenLoader;
  private final BatchQuestionSourceLoader fBatchQuestionSourceLoader;
  private final LiveQuestionLoader fLiveQuestionLoader;

  private Session fSession;
  private SessionStatus fSessionStatus;
  private @Nullable Long fDeadlineOnClient;
  private Mode fMode;

  private boolean fAcceptUserRequests;

  SessionHandler(
      Session session,
      ConnectionLike connection,
      long noticeChannelId,
      boolean checkForTimeout,
      Supplier<ClosableWorkSource> closableWorkSourceSupplier,
      RawScreenLoader rawScreenLoader,
      BatchQuestionSourceLoader batchQuestionSourceLoader,
      @Provided LiveQuestionLoader liveQuestionLoader) {
    fSession = session;
    fConnection = connection;
    fNoticeChannelId = noticeChannelId;
    fCheckForTimeout = checkForTimeout;
    fClosableWorkSourceSupplier = closableWorkSourceSupplier;
    fRawScreenLoader = rawScreenLoader;
    fBatchQuestionSourceLoader = batchQuestionSourceLoader;
    fLiveQuestionLoader = liveQuestionLoader;
  }

  public SessionStatusDeltaResult handleSessionStatusDelta(SessionStatus status) {
    if (fSessionStatus == null) {
      fAcceptUserRequests = true;
    }
    fSessionStatus = status;

    boolean proceed;
    if (fSessionStatus.getBanned()) {
      sendNotice(StudentNotice.newBuilder()
          .setClosing(Closing.newBuilder()
              .setReason(Closing.Reason.BANNED))
          .build());
      proceed = false;
    } else {
      boolean mainModeNeeded = !fSessionStatus.getFinished() && !timedOut();
      if (fMode == null) {
        if (mainModeNeeded) {
          fMode = Mode.MAIN;
          sendMainScreen();
        } else {
          fMode = Mode.SCORE;
          sendFinalScore();
        }
        proceed = true;
      } else if ((fMode == Mode.SCORE) && mainModeNeeded) {
        sendNotice(StudentNotice.newBuilder()
            .setClosing(Closing.newBuilder()
                .setReason(Closing.Reason.RESUMED))
            .build());
        proceed = false;
      } else {
        proceed = true;
      }

      if (fMode == Mode.MAIN) {
        Long deadline = fSessionStatus.hasDeadline() ? fSessionStatus.getDeadline() : null;
        if (!Objects.equals(fDeadlineOnClient, deadline)) {
          fDeadlineOnClient = deadline;
          sendNotice(buildShowRemainingTimeNotice());
        }
      }
    }

    return new SessionStatusDeltaResult(proceed);
  }

  private void sendMainScreen() {
    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      List<QuestionDescriptor> descriptors = BaseWorkUtils.getQuestionDescriptors(fSession,
          workSource.profile().options().getWeightCues(),
          workSource.profile().options().getInstantAnswerCorrectness());

      MainScreenTurnOn.Builder b = MainScreenTurnOn.newBuilder()
          .addAllQuestionDescriptor(descriptors);

      if (workSource.profile().options().hasDurationMinutes()) {
        b.setDurationMilliseconds(workSource.profile().options().getDurationMinutes() * 60_000L);
      }

      enqueueNotice(StudentNotice.newBuilder()
          .setMainScreenTurnOn(b)
          .build());

      enqueueNotice(buildShowDialogNotice(workSource));

      if (workSource.profile().options().getInstantTotalPercentCorrect()) {
        enqueueNotice(buildShowCurrentResultNotice());
      }
    }

    fConnection.flush();
  }

  private StudentNotice buildShowDialogNotice(WorkSource workSource) {
    Screen screen = loadScreen(currentQuestion().getScreenId(), workSource);

    return StudentNotice.newBuilder()
        .setShowDialog(ShowDialog.newBuilder()
            .setDialog(screen.fLiveQuestion.render())
            .setResponse(fSession.hasNewResponse() ? fSession.getNewResponse() : screen.userOrEmptyResponse())
            .setCanSubmit(fSession.hasNewResponse())
            .setQuestionIndex(fSession.getCurrentQuestionIndex())
            .setReadOnly(currentQuestion().hasScaledResult() && !workSource.profile().options().getEditableAnswers()))
        .build();
  }

  private SessionQuestion currentQuestion() {
    return fSession.getQuestion(fSession.getCurrentQuestionIndex());
  }

  private Screen loadScreen(long screenId, WorkSource workSource) {
    RawScreen rawScreen = fRawScreenLoader.load(screenId);
    return new Screen(
        fLiveQuestionLoader.load(rawScreen.liveQuestionData, workSource::getQuestion),
        rawScreen.response);
  }

  private static final class Screen {
    final LiveQuestion fLiveQuestion;
    final @Nullable DialogResponse fResponse;

    Screen(LiveQuestion liveQuestion, @Nullable DialogResponse response) {
      fLiveQuestion = liveQuestion;
      fResponse = response;
    }

    DialogResponse userOrEmptyResponse() {
      return (fResponse == null) ? fLiveQuestion.getEmptyResponse() : fResponse;
    }
  }

  private StudentNotice buildShowRemainingTimeNotice() {
    ShowRemainingTime.Builder b = ShowRemainingTime.newBuilder();

    if (fSessionStatus.hasDeadline()) {
      b.setMillisecondsRemaining(Long.max(fSessionStatus.getDeadline() - System.currentTimeMillis(), 0));
    }

    return StudentNotice.newBuilder()
        .setShowRemainingTime(b)
        .build();
  }

  private StudentNotice buildShowCurrentResultNotice() {
    return StudentNotice.newBuilder()
        .setShowCurrentResult(ShowCurrentResult.newBuilder()
            .setScaledSessionResult(Ints.checkedCast(
                BaseWorkUtils.scale(BaseWorkUtils.computeSessionResult(fSession)))))
        .build();
  }

  private void enqueueNotice(StudentNotice notice) {
    fConnection.enqueue(toNoticeServerMessage(notice));
  }

  private void sendNotice(StudentNotice notice) {
    fConnection.send(toNoticeServerMessage(notice));
  }

  private ServerMessage toNoticeServerMessage(StudentNotice notice) {
    return ServerMessage.newBuilder()
        .setChannelId(fNoticeChannelId)
        .setStudentNotice(notice)
        .build();
  }

  public SetResponseResult handleSetResponse(SetResponse m) {
    Check.input(fAcceptUserRequests);
    Check.input(active());

    for (Any areaResponse : PlatformUtils.getDialogResponseAreaMap(m.getResponse()).values()) {
      Check.input(areaResponse.getTypeUrl().lastIndexOf('/') == 0);
    }

    BigDecimal result;
    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      Check.input(!currentQuestion().hasScaledResult() || workSource.profile().options().getEditableAnswers());

      Screen screen = loadScreen(currentQuestion().getScreenId(), workSource);
      result = screen.fLiveQuestion.evaluate(m.getResponse());
    }

    fSession = fSession.toBuilder()
        .setNewResponse(m.getResponse())
        .setNewScaledResult(Ints.checkedCast(BaseWorkUtils.scale(result)))
        .buildPartial();

    fAcceptUserRequests = false;
    return new SetResponseResult(fSession);
  }

  public void handleContinueSetResponse() {
    sendNotice(StudentNotice.newBuilder()
        .setSetResponseOk(Empty.getDefaultInstance())
        .build());

    fAcceptUserRequests = true;
  }

  private boolean active() {
    return (fMode == Mode.MAIN) && !timedOut();
  }

  private boolean timedOut() {
    return fCheckForTimeout && BaseWorkUtils.isTimedOut(fSessionStatus);
  }

  public SubmitResult handleSubmit() {
    Check.input(fAcceptUserRequests);
    Check.input(active());
    Check.input(fSession.hasNewResponse());

    boolean wasPreviouslyAnswered = currentQuestion().hasScaledResult();

    UpdateResponse updateResponse = submitCurrentResponseIfPresent();

    StudentNotice showQuestionStatusNotice;
    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      showQuestionStatusNotice = StudentNotice.newBuilder()
          .setShowQuestionStatus(ShowQuestionStatus.newBuilder()
              .setQuestionIndex(fSession.getCurrentQuestionIndex())
              .setStatus(BaseWorkUtils.getQuestionStatus(
                  currentQuestion(),
                  workSource.profile().options().getInstantAnswerCorrectness())))
          .build();
    }

    boolean newQuestionSelected = !wasPreviouslyAnswered && selectNextUnansweredQuestion();

    fAcceptUserRequests = false;

    return new SubmitResult(
        fSession,
        updateResponse,
        new ContinueSubmit(showQuestionStatusNotice, newQuestionSelected));
  }

  public void handleContinueSubmit(ContinueSubmit m) {
    enqueueNotice(m.showQuestionStatusNotice);

    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      if (workSource.profile().options().getInstantTotalPercentCorrect()) {
        enqueueNotice(buildShowCurrentResultNotice());
      }

      sendNotice(m.newQuestionSelected ?
          buildShowDialogNotice(workSource) :
          StudentNotice.newBuilder()
              .setSubmitOk(SubmitOk.newBuilder()
                  .setMakeReadOnly(!workSource.profile().options().getEditableAnswers()))
              .build());
    }

    fAcceptUserRequests = true;
  }

  private @Nullable UpdateResponse submitCurrentResponseIfPresent() {
    UpdateResponse res;

    if (fSession.hasNewResponse()) {
      SessionQuestion q = currentQuestion();

      res = new UpdateResponse(
          fSession.getCurrentQuestionIndex(),
          fSession.getNewScaledResult(),
          q.getWeight(),
          q.getScreenId(),
          fSession.getNewResponse());

      BigDecimal score;
      {
        BigDecimal responseResultDelta;
        {
          BigDecimal newResponseResult = BaseWorkUtils.unscale(fSession.getNewScaledResult());
          BigDecimal oldResponseResult = q.hasScaledResult() ?
              BaseWorkUtils.unscale(q.getScaledResult()) :
              BigDecimal.ZERO;
          responseResultDelta = newResponseResult.subtract(oldResponseResult);
        }

        BigDecimal oldScore = BaseWorkUtils.unscale(fSession.getScaledScore());
        BigDecimal scoreDelta = responseResultDelta.multiply(BigDecimal.valueOf(q.getWeight()));
        score = oldScore.add(scoreDelta);
      }

      int attemptedScore = IntMath.checkedAdd(
          fSession.getAttemptedScore(),
          q.hasScaledResult() ? 0 : q.getWeight());

      fSession = fSession.toBuilder()
          .clearNewResponse()
          .clearNewScaledResult()
          .setScaledScore(BaseWorkUtils.scale(score))
          .setAttemptedScore(attemptedScore)
          .setQuestion(fSession.getCurrentQuestionIndex(), q.toBuilder()
              .setScaledResult(fSession.getNewScaledResult()))
          .buildPartial();
    } else {
      res = null;
    }

    return res;
  }

  private boolean selectNextUnansweredQuestion() {
    int currentQuestionIndex = fSession.getCurrentQuestionIndex();
    int questionCount = fSession.getQuestionCount();

    int n = currentQuestionIndex;
    do {
      n = (n + 1) % questionCount;
    } while ((n != currentQuestionIndex) && fSession.getQuestion(n).hasScaledResult());

    boolean res = (n != currentQuestionIndex);
    if (res) {
      fSession = fSession.toBuilder()
          .setCurrentQuestionIndex(n)
          .buildPartial();
    }

    return res;
  }

  public ChooseQuestionResult handleChooseQuestion(ChooseQuestion m) {
    Check.input(fAcceptUserRequests);
    Check.input(active());
    Check.input(!fSession.hasNewResponse());
    BaseWorkUtils.checkQuestionIndex(m.getQuestionIndex(), fSession);

    fSession = fSession.toBuilder()
        .setCurrentQuestionIndex(m.getQuestionIndex())
        .buildPartial();

    fAcceptUserRequests = false;
    return new ChooseQuestionResult(fSession);
  }

  public void handleContinueChooseQuestion() {
    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      sendNotice(buildShowDialogNotice(workSource));
    }

    fAcceptUserRequests = true;
  }

  public FinishWorkResult handleFinishWork() {
    Check.input(fAcceptUserRequests);
    Check.input(fMode == Mode.MAIN);

    UpdateResponse updateResponse = timedOut() ? null : submitCurrentResponseIfPresent();

    fAcceptUserRequests = false;
    return new FinishWorkResult(fSession, updateResponse);
  }

  public void handleContinueFinishWork() {
    fMode = Mode.SCORE;
    sendFinalScore();

    fAcceptUserRequests = true;
  }

  private void sendFinalScore() {
    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      ShowScoreScreen.Builder b = ShowScoreScreen.newBuilder();

      AvailableScore a = workSource.profile().availableScore();
      if (a != null) {
        ShowScore.Builder showScore = ShowScore.newBuilder()
            .setQuestionCount(fSession.getQuestionCount());

        BigDecimal result = BaseWorkUtils.computeSessionResult(fSession);

        if (a.getPercentCorrect()) {
          showScore.setScaledResult(Ints.checkedCast(BaseWorkUtils.scale(result)));
        }

        if (a.getPoints()) {
          showScore
              .setScaledScore(fSession.getScaledScore())
              .setPerfectScore(fSession.getPerfectScore());
        }

        if (a.getMark()) {
          String mark = workSource.profile().markScale().getTitleForResult(result);
          if (!mark.isEmpty()) {
            showScore.setMark(mark);
          }
        }

        if (a.hasForQuestions()) {
          boolean revealWeight = workSource.profile().options().getWeightCues() || a.getForQuestions().getPoints();
          boolean revealCorrectness = a.getForQuestions().getPercentCorrect();
          List<QuestionDescriptor> descriptors = BaseWorkUtils.getQuestionDescriptors(fSession, revealWeight,
              revealCorrectness);

          showScore.setQuestionScore(QuestionScore.newBuilder()
              .setCorrectResponseAvailable(a.getForQuestions().getCorrectAnswer())
              .addAllQuestionDescriptor(descriptors));
        }

        if (a.hasForSections()) {
          showScore.setSectionScore(computeSectionScore(a.getForSections(), workSource));
        }

        b.setShowScore(showScore);
      }

      sendNotice(StudentNotice.newBuilder()
          .setShowScoreScreen(b)
          .build());
    }
  }

  private SectionScore computeSectionScore(AvailableSectionScore available, WorkSource workSource) {
    HashMap<Section, SectionSum> sums = new HashMap<>();

    ArrayList<SectionSum> questionSections = available.getQuestionList() ?
        new ArrayList<>(fSession.getQuestionCount()) : null;

    List<String> questionSources = fBatchQuestionSourceLoader.load(fSession);

    for (int questionCount = fSession.getQuestionCount(), i = 0; i < questionCount; ++i) {
      Section section = workSource.getSection(workSource.getQuestionItem(questionSources.get(i)));
      SectionSum sum = sums.computeIfAbsent(section, ignored -> new SectionSum());

      if (available.getPercentCorrect() || available.getPoints()) {
        sum.fScore = sum.fScore.add(BaseWorkUtils.getScore(fSession.getQuestion(i)));
        sum.fPerfectScore = IntMath.checkedAdd(sum.fPerfectScore, fSession.getQuestion(i).getWeight());
      }

      ++sum.fQuestionCount;

      if (questionSections != null) {
        questionSections.add(sum);
      }
    }

    SectionScore.Builder sectionScore = SectionScore.newBuilder()
        .setResultAvailable(available.getPercentCorrect())
        .setScoreAvailable(available.getPoints())
        .setQuestionCountAvailable(available.getQuestionCount());

    for (Section section : workSource.test().root().listTreeSections()) {
      SectionSum sum = sums.get(section);
      if (sum != null) {
        SectionOutcome.Builder outcome = SectionOutcome.newBuilder()
            .setName(section.name());

        if (available.getPercentCorrect()) {
          BigDecimal result = sum.fScore.divide(BigDecimal.valueOf(sum.fPerfectScore),
              LiveQuestion.SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
          outcome.setScaledResult(Ints.checkedCast(BaseWorkUtils.scale(result)));
        }

        if (available.getPoints()) {
          outcome
              .setScaledScore(BaseWorkUtils.scale(sum.fScore))
              .setPerfectScore(sum.fPerfectScore);
        }

        if (available.getQuestionCount()) {
          outcome.setQuestionCount(sum.fQuestionCount);
        }

        sum.fIndex = sectionScore.getSectionOutcomeCount();

        sectionScore.addSectionOutcome(outcome);
      }
    }

    if (questionSections != null) {
      sectionScore.addAllQuestionSectionIndex(questionSections.stream()
          .map(s -> s.fIndex)
          .collect(Collectors.toList()));
    }

    return sectionScore.build();
  }

  private static final class SectionSum {
    BigDecimal fScore = BigDecimal.ZERO;
    int fPerfectScore;
    int fQuestionCount;
    int fIndex;
  }

  public void handleQueryQuestionDetails(QueryQuestionDetails m) {
    Check.input(fAcceptUserRequests);
    Check.input(fMode == Mode.SCORE);

    try (ClosableWorkSource workSource = fClosableWorkSourceSupplier.get()) {
      AvailableScore a = Check.inputNotNull(workSource.profile().availableScore());
      Check.input(a.hasForQuestions());
      AvailableQuestionScore qs = a.getForQuestions();

      BaseWorkUtils.checkQuestionIndex(m.getQuestionIndex(), fSession);
      SessionQuestion q = fSession.getQuestion(m.getQuestionIndex());

      Screen screen = loadScreen(q.getScreenId(), workSource);

      ShowQuestionDetails.Builder showQuestionDetails = ShowQuestionDetails.newBuilder()
          .setQuestionIndex(m.getQuestionIndex())
          .setDialog(screen.fLiveQuestion.render())
          .setResponse(screen.userOrEmptyResponse());

      if (qs.getCorrectAnswer()) {
        showQuestionDetails.setCorrectResponse(screen.fLiveQuestion.getCorrectResponse());
      }

      if (qs.getPercentCorrect()) {
        showQuestionDetails.setScaledResult(q.hasScaledResult() ? q.getScaledResult() : 0);
      }

      if (qs.getPoints()) {
        showQuestionDetails.setWeight(q.getWeight());
      }

      if (qs.getPercentCorrect() && qs.getPoints()) {
        showQuestionDetails.setScaledScore(BaseWorkUtils.getScaledScore(q));
      }

      sendNotice(StudentNotice.newBuilder()
          .setShowQuestionDetails(showQuestionDetails)
          .build());
    }
  }
}
