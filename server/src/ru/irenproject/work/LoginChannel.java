/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.common.login.Proto.LogIn;
import ru.irenproject.common.login.Proto.LoginReply;
import ru.irenproject.common.login.Proto.LoginRequest;
import ru.irenproject.common.login.Proto.Supervise;
import ru.irenproject.common.protocol.Proto.ServerMessage;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class LoginChannel extends AbstractBehavior<LoginChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final LoginRequest request;
    public final RequestContext requestContext;

    public RequestMessage(LoginRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    LoginChannel create(
        ActorContext<Message> context,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler,
        KeyVerifier keyVerifier,
        @Nullable String serverAddress);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler,
        KeyVerifier keyVerifier,
        @Nullable String serverAddress) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, connectionHandler, keyVerifier, serverAddress));
    }
  }

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final ActorRef<ConnectionHandler.Message> fConnectionHandler;
  private final KeyVerifier fKeyVerifier;
  private final @Nullable String fServerAddress;
  private final SupervisorLoginHandler.Factory fSupervisorLoginHandlerFactory;
  private final LoginHandler.Factory fLoginHandlerFactory;

  private @Nullable SupervisorLoginHandler fSupervisorLoginHandler;
  private @Nullable LoginHandler fLoginHandler;

  @Inject private LoginChannel(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted ActorRef<ConnectionHandler.Message> connectionHandler,
      @Assisted KeyVerifier keyVerifier,
      @Assisted @Nullable String serverAddress,
      SupervisorLoginHandler.Factory supervisorLoginHandlerFactory,
      LoginHandler.Factory loginHandlerFactory) {
    fContext = context;
    fConnection = connection;
    fConnectionHandler = connectionHandler;
    fKeyVerifier = keyVerifier;
    fServerAddress = serverAddress;
    fSupervisorLoginHandlerFactory = supervisorLoginHandlerFactory;
    fLoginHandlerFactory = loginHandlerFactory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessage(SupervisorLoginHandler.Message.class, this::onSupervisorLoginHandlerMessage)
        .onMessage(LoginHandler.Message.class, this::onLoginHandlerMessage)
        .build();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    LoginRequest r = m.request;
    switch (r.getRequestCase()) {
      case SUPERVISE: return onSupervise(r.getSupervise(), m.requestContext);
      case LOG_IN: return onLogIn(r.getLogIn(), m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onSupervise(Supervise m, RequestContext requestContext) {
    Check.input((fSupervisorLoginHandler == null) && (fLoginHandler == null));

    fSupervisorLoginHandler = fSupervisorLoginHandlerFactory.create(
        fContext,
        m,
        requestContext,
        fConnection,
        fConnectionHandler,
        fKeyVerifier,
        fServerAddress);

    fContext.getSelf().tell(SupervisorLoginHandler.Activate.INSTANCE);

    return this;
  }

  private Behavior<Message> onLogIn(LogIn m, RequestContext requestContext) {
    Check.input((fSupervisorLoginHandler == null) && (fLoginHandler == null));

    fLoginHandler = fLoginHandlerFactory.create(
        fContext,
        m,
        requestContext,
        fConnection);

    fContext.getSelf().tell(LoginHandler.Activate.INSTANCE);

    return this;
  }

  private Behavior<Message> onSupervisorLoginHandlerMessage(SupervisorLoginHandler.Message m) {
    Check.notNull(fSupervisorLoginHandler).handle(m);

    Behavior<Message> res;
    Supervise.Reply reply = fSupervisorLoginHandler.reply();
    if (reply == null) {
      res = this;
    } else {
      send(LoginReply.newBuilder()
          .setSuperviseReply(reply)
          .build(), fSupervisorLoginHandler.requestContext());
      fSupervisorLoginHandler = null;
      res = (reply.getResultCase() == Supervise.Reply.ResultCase.OK) ? Behaviors.stopped() : this;
    }

    return res;
  }

  private Behavior<Message> onLoginHandlerMessage(LoginHandler.Message m) {
    Check.notNull(fLoginHandler).handle(m);

    Behavior<Message> res;
    LogIn.Reply reply = fLoginHandler.reply();
    if (reply == null) {
      res = this;
    } else {
      send(LoginReply.newBuilder()
          .setLogInReply(reply)
          .build(), fLoginHandler.requestContext());
      fLoginHandler = null;
      res = (reply.getResultCase() == LogIn.Reply.ResultCase.OK) ? Behaviors.stopped() : this;
    }

    return res;
  }

  private void send(LoginReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setLoginReply(reply)
        .build());
  }
}
