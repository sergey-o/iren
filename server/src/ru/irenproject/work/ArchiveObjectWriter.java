/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.itx.ItxImage;
import ru.irenproject.itx.ObjectWriter;
import ru.irenproject.pad.ImageBlock;

import com.google.common.hash.HashingOutputStream;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipMethod;
import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.time.Instant;
import java.util.Set;
import java.util.function.Consumer;

public final class ArchiveObjectWriter implements ObjectWriter {
  static {
    Check.that(ArchiveUtils.OBJECT_ID_HASH_FUNCTION == BlobId.HASH_FUNCTION);
  }

  private final ZipArchiveOutputStream fZip;
  private final Set<String> fEntryNames;
  private final Instant fArchivedAt;

  public ArchiveObjectWriter(ZipArchiveOutputStream zip, Set<String> entryNames, Instant archivedAt) {
    fZip = zip;
    fEntryNames = entryNames;
    fArchivedAt = archivedAt;
  }

  @Override public ItxImage writeImage(ImageBlock block, BlobStore blobStore) {
    String objectId = block.data().toHexString();
    writeObject(objectId, ZipMethod.STORED, () -> blobStore.get(block.data()).copyTo(fZip));
    return new ItxImage(objectId, block.mimeType(), null, null);
  }

  private void writeObject(String objectId, ZipMethod zipMethod, EntryWriter entryWriter) {
    try {
      String entryName = ArchiveUtils.getObjectEntryName(objectId);
      if (fEntryNames.add(entryName)) {
        ZipArchiveEntry entry = ArchiveUtils.createZipEntry(entryName, fArchivedAt);
        entry.setMethod(zipMethod.getCode());

        fZip.putArchiveEntry(entry);
        entryWriter.write();
        fZip.closeArchiveEntry();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private interface EntryWriter {
    void write() throws IOException;
  }

  /** @return object id */
  public String writeXml(Consumer<XMLStreamWriter> generator) {
    ByteArrayOutputStream xml = new ByteArrayOutputStream();
    HashingOutputStream h = new HashingOutputStream(ArchiveUtils.OBJECT_ID_HASH_FUNCTION, xml);
    Utils.writeXml(h, generator);
    String res = h.hash().toString();

    writeObject(res, ZipMethod.DEFLATED, () -> xml.writeTo(fZip));
    return res;
  }
}
