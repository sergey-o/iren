/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;
import ru.irenproject.authentication.AuthenticatedUser;
import ru.irenproject.authentication.AuthenticationService;
import ru.irenproject.authentication.TextFileUserRegistry;
import ru.irenproject.authentication.User;
import ru.irenproject.common.authenticationMode.accountLogin.Proto.AccountLogin;
import ru.irenproject.common.authenticationMode.selfRegistration.Proto.SelfRegistration;
import ru.irenproject.common.authenticationMode.textFile.Proto.TextFile;
import ru.irenproject.common.studentWorkSelector.Proto.OpenWork;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionIssuer;
import ru.irenproject.profile.Profile;
import ru.irenproject.work.Proto.SessionConnection;
import ru.irenproject.work.db.DbReader;
import ru.irenproject.work.db.DbTask;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableList;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;
import com.ibm.icu.text.Normalizer2;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.security.SecureRandom;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.regex.Pattern;

public final class SessionProvider extends AbstractBehavior<SessionProvider.Message> {
  public interface Message {}

  public interface Factory {
    SessionProvider create(
        ActorContext<Message> context,
        @Assisted("workId") ByteString workId,
        @Nullable OpenWork.UserData userData,
        @Assisted("sessionKey") @Nullable ByteString sessionKey,
        Connection connection,
        ActorRef<Result> replyTo);

    static Behavior<Message> makeBehavior(
        Factory factory,
        ByteString workId,
        @Nullable OpenWork.UserData userData, // ignored if sessionKey is present
        @Nullable ByteString sessionKey,
        Connection connection,
        ActorRef<Result> replyTo) {
      return Behaviors.setup(ctx -> factory.create(ctx, workId, userData, sessionKey, connection, replyTo));
    }
  }

  public interface Result {}

  public static final class Ok implements Result {
    public final ActorRef<WorkHub.Message> workHub;
    public final long sessionId;
    public final ByteString sessionKey;

    public Ok(ActorRef<WorkHub.Message> workHub, long sessionId, ByteString sessionKey) {
      this.workHub = workHub;
      this.sessionId = sessionId;
      this.sessionKey = sessionKey;
    }
  }

  public static final class Failure implements Result {
    public final OpenWork.Reply failureReply;

    public Failure(OpenWork.Reply failureReply) {
      this.failureReply = failureReply;
    }
  }

  private static final int MAX_SELF_REGISTRATION_USER_NAME_LENGTH = 100;
  private static final Pattern SELF_REGISTRATION_USER_NAME_PATTERN =
      Pattern.compile("[\\p{L}\\p{Mn}\\p{Mc}\\p{Nd} '\u2019-]*");

  private static final OpenWork.Reply LOGIN_REQUIRED = OpenWork.Reply.newBuilder()
      .setLoginRequired(OpenWork.Reply.LoginRequired.newBuilder()
          .setMaxUserNameLength(AuthenticationService.MAX_USER_NAME_LENGTH)
          .setMaxPasswordLength(AuthenticationService.MAX_PASSWORD_LENGTH))
      .build();

  private static final OpenWork.Reply INCORRECT_USER_NAME = OpenWork.Reply.newBuilder()
      .setIncorrectUserName(Empty.getDefaultInstance())
      .build();

  private static final OpenWork.Reply INCORRECT_USER_DATA = OpenWork.Reply.newBuilder()
      .setIncorrectUserData(Empty.getDefaultInstance())
      .build();

  private static final OpenWork.Reply USER_SESSION_ALREADY_EXISTS = OpenWork.Reply.newBuilder()
      .setUserSessionAlreadyExists(Empty.getDefaultInstance())
      .build();

  private static final ThreadLocal<SecureRandom> fSecureRandom = ThreadLocal.withInitial(SecureRandom::new);

  private final ActorContext<Message> fContext;
  private final ByteString fWorkId;
  private final @Nullable OpenWork.UserData fUserData;
  private final @Nullable ByteString fSessionKey;
  private final Connection fConnection;
  private final ActorRef<Result> fReplyTo;
  private final ActorRef<WorkCatalog.Message> fWorkCatalog;
  private final TextFileUserRegistry fTextFileUserRegistry;
  private final QuestionIssuer fQuestionIssuer;

  private ActorRef<WorkHub.Message> fWorkHub;
  private LiveWork fLiveWork;

  @Inject private SessionProvider(
      @Assisted ActorContext<Message> context,
      @Assisted("workId") ByteString workId,
      @Assisted @Nullable OpenWork.UserData userData,
      @Assisted("sessionKey") @Nullable ByteString sessionKey,
      @Assisted Connection connection,
      @Assisted ActorRef<Result> replyTo,
      ActorRef<WorkCatalog.Message> workCatalog,
      TextFileUserRegistry textFileUserRegistry,
      QuestionIssuer questionIssuer) {
    fContext = context;
    fWorkId = workId;
    fUserData = userData;
    fSessionKey = sessionKey;
    fConnection = connection;
    fReplyTo = replyTo;
    fWorkCatalog = workCatalog;
    fTextFileUserRegistry = textFileUserRegistry;
    fQuestionIssuer = questionIssuer;

    fContext.getSelf().tell(Activate.INSTANCE);
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessageEquals(Activate.INSTANCE, this::onActivate)
        .onMessage(WorkHubObtained.class, this::onWorkHubObtained)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(Registered.class, this::onRegistered)
        .onMessage(ContinueAfterCreateSessionIfAbsent.class, this::onContinueAfterCreateSessionIfAbsent)
        .onMessage(ContinueHandleExistingSession.class, this::onContinueHandleExistingSession)
        .build();
  }

  private enum Activate implements Message { INSTANCE }

  private Behavior<Message> onActivate() {
    fWorkCatalog.tell(new WorkCatalog.Get<>(
        fWorkId,
        WorkHubObtained::new,
        fContext.getSelf()));

    return this;
  }

  private static final class WorkHubObtained implements Message {
    final @Nullable ActorRef<WorkHub.Message> workHub;

    WorkHubObtained(@Nullable ActorRef<WorkHub.Message> workHub) {
      this.workHub = workHub;
    }
  }

  private Behavior<Message> onWorkHubObtained(WorkHubObtained m) {
    Behavior<Message> res;
    if (m.workHub == null) {
      res = Behaviors.stopped();
    } else {
      fWorkHub = m.workHub;
      fContext.watchWith(fWorkHub, Stop.INSTANCE);

      fWorkHub.tell(new WorkHub.RegisterParticipant<>(
          null,
          fContext.getSelf(),
          Stop.INSTANCE,
          Registered::new));
      res = this;
    }

    return res;
  }

  private enum Stop implements Message { INSTANCE }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }

  private static final class Registered implements Message {
    final LiveWork liveWork;

    Registered(LiveWork liveWork) {
      this.liveWork = liveWork;
    }
  }

  private Behavior<Message> onRegistered(Registered m) {
    fLiveWork = m.liveWork;
    return (fSessionKey == null) ? obtainSession() : findSessionByKey();
  }

  private Behavior<Message> obtainSession() {
    Behavior<Message> res;

    SessionUser user = null;
    OpenWork.Reply failureReply = null;
    try {
      user = prepareSessionUser();
    } catch (SessionUserException e) {
      failureReply = e.fFailureReply;
    }

    if (user == null) {
      res = finish(new Failure(failureReply));
    } else {
      Long existingSessionId = fLiveWork.store().read(
          WorkUtils.getSessionIdByCanonicalUserNameIfExists(user.canonicalName));

      if (existingSessionId == null) {
        try (ClosableWorkSource workSource = fLiveWork.acquireSource()) {
          List<_LiveQuestion> liveQuestions = fQuestionIssuer.issueFromTest(
              workSource.test(),
              workSource.profile(),
              fLiveWork.work().locale());

          if (liveQuestions.isEmpty()) {
            res = Behaviors.stopped();
          } else {
            ByteString sessionKey = generateSessionKey();
            boolean canStealAttachedSession = user.canStealAttachedSession;

            FullSession session = prepareSession(user, liveQuestions, workSource.profile());

            fWorkHub.tell(new WorkHub.CreateSessionIfAbsent<>(
                user.canonicalName,
                session.data,
                ImmutableList.copyOf(liveQuestions),
                session.status,
                sessionKey,
                result -> new ContinueAfterCreateSessionIfAbsent(result, sessionKey, canStealAttachedSession),
                fContext.getSelf()));
            res = this;
          }
        }
      } else {
        res = handleExistingSession(existingSessionId, user.canStealAttachedSession);
      }
    }

    return res;
  }

  private FullSession prepareSession(SessionUser user, List<_LiveQuestion> liveQuestions, Profile profile) {
    Instant now = Utils.milliClock().instant();

    return BaseWorkUtils.buildSessionPrecursor(
        user.name,
        user.displayName,
        user.groupName,
        SessionConnection.newBuilder()
            .setOpenedAt(Utils.instantToTimestamp(now))
            .setAddress(fConnection.clientAddress())
            .build(),
        profile.options().hasDurationMinutes() ?
            now.plus(Duration.ofMinutes(profile.options().getDurationMinutes())).toEpochMilli() :
            null,
        liveQuestions);
  }

  private static final class ContinueAfterCreateSessionIfAbsent implements Message {
    final WorkHub.CreateSessionIfAbsentResult result;
    final ByteString keyForNewSession;
    final boolean canStealAttachedSession;

    ContinueAfterCreateSessionIfAbsent(WorkHub.CreateSessionIfAbsentResult result, ByteString keyForNewSession,
        boolean canStealAttachedSession) {
      this.result = result;
      this.keyForNewSession = keyForNewSession;
      this.canStealAttachedSession = canStealAttachedSession;
    }
  }

  private Behavior<Message> onContinueAfterCreateSessionIfAbsent(ContinueAfterCreateSessionIfAbsent m) {
    Behavior<Message> res;

    //noinspection IfStatementWithIdenticalBranches
    if (m.result.newSession == null) {
      long existingSessionId = m.result.newOrExistingSessionId;
      res = handleExistingSession(existingSessionId, m.canStealAttachedSession);
    } else {
      long newSessionId = m.result.newOrExistingSessionId;
      res = finish(new Ok(fWorkHub, newSessionId, m.keyForNewSession));
    }

    return res;
  }

  private Behavior<Message> handleExistingSession(long existingSessionId, boolean canStealAttachedSession) {
    ByteString newSessionKey = generateSessionKey();

    fWorkHub.tell(new WorkHub.SetSessionKey<>(
        existingSessionId,
        newSessionKey,
        canStealAttachedSession,
        keyHasBeenSet -> new ContinueHandleExistingSession(keyHasBeenSet, existingSessionId, newSessionKey),
        fContext.getSelf()));

    return this;
  }

  private static final class ContinueHandleExistingSession implements Message {
    final boolean keyHasBeenSet;
    final long existingSessionId;
    final ByteString newSessionKey;

    ContinueHandleExistingSession(boolean keyHasBeenSet, long existingSessionId, ByteString newSessionKey) {
      this.keyHasBeenSet = keyHasBeenSet;
      this.existingSessionId = existingSessionId;
      this.newSessionKey = newSessionKey;
    }
  }

  private Behavior<Message> onContinueHandleExistingSession(ContinueHandleExistingSession m) {
    return finish(m.keyHasBeenSet ?
        new Ok(fWorkHub, m.existingSessionId, m.newSessionKey) :
        new Failure(USER_SESSION_ALREADY_EXISTS));
  }

  private Behavior<Message> finish(Result result) {
    fReplyTo.tell(result);
    return Behaviors.stopped();
  }

  private static final class SessionUser {
    final String name;
    final String canonicalName;
    final @Nullable String displayName;
    final @Nullable String groupName;
    final boolean canStealAttachedSession;

    SessionUser(String name, String canonicalName, @Nullable String displayName, @Nullable String groupName,
        boolean canStealAttachedSession) {
      this.name = name;
      this.canonicalName = canonicalName;
      this.displayName = displayName;
      this.groupName = groupName;
      this.canStealAttachedSession = canStealAttachedSession;
    }
  }

  private static final class SessionUserException extends RuntimeException {
    final OpenWork.Reply fFailureReply;

    SessionUserException(OpenWork.Reply failureReply) {
      fFailureReply = failureReply;
    }
  }

  private SessionUser prepareSessionUser() {
    com.google.protobuf.Message authenticationMode = fLiveWork.work().authenticationMode();

    if (authenticationMode instanceof SelfRegistration) {
      return prepareSelfRegistrationSessionUser();
    } else if (authenticationMode instanceof TextFile) {
      return prepareTextFileSessionUser();
    } else if (authenticationMode instanceof AccountLogin) {
      return prepareAccountLoginSessionUser();
    } else {
      throw new RuntimeException();
    }
  }

  private SessionUser prepareSelfRegistrationSessionUser() {
    if (fUserData == null) {
      throw new SessionUserException(OpenWork.Reply.newBuilder()
          .setUserDataRequired(OpenWork.Reply.UserDataRequired.newBuilder()
              .setMaxUserNameLength(MAX_SELF_REGISTRATION_USER_NAME_LENGTH))
          .build());
    }

    String userName = toSelfRegistrationUserNameIfValid(fUserData.getUserName());
    if (userName == null) {
      throw new SessionUserException(INCORRECT_USER_NAME);
    }

    if (fUserData.hasGroupName()) {
      throw new SessionUserException(INCORRECT_USER_DATA);
    }

    return new SessionUser(userName, canonicalizeSelfRegistrationUserName(userName), null, null, false);
  }

  private static @Nullable String toSelfRegistrationUserNameIfValid(String suppliedUserName) {
    String userName = CharMatcher.whitespace().trimAndCollapseFrom(suppliedUserName, ' ');
    return validSelfRegistrationUserName(userName) ? userName : null;
  }

  private static boolean validSelfRegistrationUserName(String s) {
    return !s.isEmpty() && (s.length() <= MAX_SELF_REGISTRATION_USER_NAME_LENGTH)
        && SELF_REGISTRATION_USER_NAME_PATTERN.matcher(s).matches();
  }

  private static String canonicalizeSelfRegistrationUserName(String userName) {
    return Normalizer2.getNFKCCasefoldInstance().normalize(userName);
  }

  private SessionUser prepareTextFileSessionUser() {
    if (fUserData == null) {
      throw new SessionUserException(OpenWork.Reply.newBuilder()
          .setUserDataRequired(OpenWork.Reply.UserDataRequired.newBuilder()
              .setMaxUserNameLength(TextFileUserRegistry.MAX_NAME_LENGTH)
              .setMaxGroupNameLength(TextFileUserRegistry.MAX_NAME_LENGTH))
          .build());
    }

    if (!fUserData.hasGroupName()) {
      throw new SessionUserException(INCORRECT_USER_DATA);
    }

    User user = fTextFileUserRegistry.getUserIfExists(fUserData.getUserName(), fUserData.getGroupName());
    if (user == null) {
      throw new SessionUserException(INCORRECT_USER_DATA);
    }

    return new SessionUser(user.name(), user.name(), user.displayName(), user.groupName(), false);
  }

  private SessionUser prepareAccountLoginSessionUser() {
    AuthenticatedUser user = fConnection.user();
    if ((user == null) || (fUserData != null)) {
      throw new SessionUserException(LOGIN_REQUIRED);
    }

    return new SessionUser(user.name(), user.name(), user.displayName(), null, true);
  }

  private Behavior<Message> findSessionByKey() {
    Long sessionId = fLiveWork.store().read(getSessionIdByKeyIfExists(fSessionKey));

    return (sessionId == null) ?
        Behaviors.stopped() :
        finish(new Ok(fWorkHub, sessionId, fSessionKey));
  }

  private static DbTask<DbReader, /* @Nullable */Long> getSessionIdByKeyIfExists(ByteString sessionKey) {
    return dbr -> {
      try {
        SQLiteStatement s = dbr.prepare("SELECT ID FROM SESSIONS WHERE KEY = ?1");
        try {
          s.bind(1, sessionKey.toByteArray());
          return s.step() ? s.columnLong(0) : null;
        } finally {
          s.dispose();
        }
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private static ByteString generateSessionKey() {
    byte[] key = new byte[WorkUtils.SESSION_KEY_SIZE];
    fSecureRandom.get().nextBytes(key);
    return ByteString.copyFrom(key);
  }
}
