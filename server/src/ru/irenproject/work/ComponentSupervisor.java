/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.Props;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.util.Map;
import java.util.TreeMap;

public final class ComponentSupervisor extends AbstractBehavior<ComponentSupervisor.Message> {
  public interface Message {}

  public static final class CreateComponent<T> implements Message {
    public final Behavior<T> behavior;
    public final String name;
    public final Props props;
    public final int stopPhase;
    public final T stopMessage;
    public final ActorRef<ActorRef<T>> replyTo;

    public CreateComponent(Behavior<T> behavior, String name, Props props, int stopPhase, T stopMessage,
        ActorRef<ActorRef<T>> replyTo) {
      this.behavior = behavior;
      this.name = name;
      this.props = props;
      this.stopPhase = stopPhase;
      this.stopMessage = stopMessage;
      this.replyTo = replyTo;
    }
  }

  public enum Stop implements Message { INSTANCE }

  private final ActorContext<Message> fContext;

  private final TreeMap<Integer, Component<?>> fComponentsByStopPhase = new TreeMap<>();
  private boolean fStopping;

  public ComponentSupervisor(ActorContext<Message> context) {
    fContext = context;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(CreateComponent.class, m -> onCreateComponent((CreateComponent<?>) m))
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessageEquals(CurrentComponentTerminated.INSTANCE, this::onCurrentComponentTerminated)
        .build();
  }

  private <T> Behavior<Message> onCreateComponent(CreateComponent<T> m) {
    Check.that(!fStopping);
    Check.that(!fComponentsByStopPhase.containsKey(m.stopPhase));

    ActorRef<T> actorRef = fContext.spawn(m.behavior, m.name, m.props);
    fContext.watch(actorRef);
    fComponentsByStopPhase.put(m.stopPhase, new Component<>(actorRef, m.stopMessage));

    m.replyTo.tell(actorRef);

    return this;
  }

  private static final class Component<T> {
    final ActorRef<T> fActorRef;
    final T fStopMessage;

    Component(ActorRef<T> actorRef, T stopMessage) {
      fActorRef = actorRef;
      fStopMessage = stopMessage;
    }

    void stop() {
      fActorRef.tell(fStopMessage);
    }
  }

  private Behavior<Message> onStop() {
    Behavior<Message> res;
    if (fStopping) {
      res = this;
    } else {
      fStopping = true;
      res = stopNextComponentIfPresent();
    }

    return res;
  }

  private Behavior<Message> stopNextComponentIfPresent() {
    Behavior<Message> res;
    Map.Entry<Integer, Component<?>> entry = fComponentsByStopPhase.pollFirstEntry();
    if (entry == null) {
      res = Behaviors.stopped();
    } else {
      Component<?> component = entry.getValue();
      fContext.unwatch(component.fActorRef);
      fContext.watchWith(component.fActorRef, CurrentComponentTerminated.INSTANCE);
      component.stop();
      res = this;
    }

    return res;
  }

  private enum CurrentComponentTerminated implements Message { INSTANCE }

  private Behavior<Message> onCurrentComponentTerminated() {
    return stopNextComponentIfPresent();
  }
}
