/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.authentication.AuthenticatedUser;
import ru.irenproject.authentication.AuthenticationService;
import ru.irenproject.common.login.Proto.LogIn;

import akka.actor.typed.ActorRef;
import akka.actor.typed.javadsl.ActorContext;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Empty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;

final class LoginHandler {
  public interface Message extends LoginChannel.Message {}

  public enum Activate implements Message { INSTANCE }

  interface Factory {
    LoginHandler create(
        ActorContext<LoginChannel.Message> context,
        LogIn request,
        RequestContext requestContext,
        Connection connection);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(LoginHandler.class);

  private static final LogIn.Reply OK = LogIn.Reply.newBuilder()
      .setOk(Empty.getDefaultInstance())
      .build();

  private final ActorContext<LoginChannel.Message> fContext;
  private final LogIn fRequest;
  private final RequestContext fRequestContext;
  private final Connection fConnection;
  private final ActorRef<AuthenticationService.Message> fAuthenticationService;

  private @Nullable LogIn.Reply fReply;

  @Inject private LoginHandler(
      @Assisted ActorContext<LoginChannel.Message> context,
      @Assisted LogIn request,
      @Assisted RequestContext requestContext,
      @Assisted Connection connection,
      ActorRef<AuthenticationService.Message> authenticationService) {
    fContext = context;
    fRequest = request;
    fRequestContext = requestContext;
    fConnection = connection;
    fAuthenticationService = authenticationService;
  }

  public void handle(Message m) {
    if (m instanceof Activate) {
      onActivate();
    } else if (m instanceof AuthenticateResult) {
      onAuthenticateResult(((AuthenticateResult) m).original);
    } else {
      throw new RuntimeException(m.toString());
    }
  }

  public @Nullable LogIn.Reply reply() {
    return fReply;
  }

  public RequestContext requestContext() {
    return fRequestContext;
  }

  private void onActivate() {
    fAuthenticationService.tell(new AuthenticationService.Authenticate<>(
        fRequest.getUserName(),
        fRequest.getPassword(),
        AuthenticateResult::new,
        fContext.getSelf()));
  }

  private static final class AuthenticateResult implements Message {
    final AuthenticationService.AuthenticateResult original;

    AuthenticateResult(AuthenticationService.AuthenticateResult original) {
      this.original = original;
    }
  }

  private void onAuthenticateResult(AuthenticationService.AuthenticateResult m) {
    if (m instanceof AuthenticationService.AuthenticateResult.Ok) {
      AuthenticatedUser authenticatedUser = ((AuthenticationService.AuthenticateResult.Ok) m).authenticatedUser;

      Check.that(fConnection.setUserIfAbsent(authenticatedUser));

      fLogger.info("Login: \"{}\" {}.", authenticatedUser.name(), fConnection);
      fReply = OK;
    } else {
      AuthenticationService.AuthenticateResult.Failure f = ((AuthenticationService.AuthenticateResult.Failure) m);

      fLogger.warn("Login failed: {}{} {}.",
          (f.userName == null) ? "" : String.format("\"%s\" - ", f.userName),
          f.failure,
          fConnection);

      fReply = LogIn.Reply.newBuilder()
          .setFailure(f.failure)
          .build();
    }
  }
}
