/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.Utils;
import ru.irenproject.common.protocol.Proto.ClientMessage;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.workController.Proto.DeleteWork;
import ru.irenproject.common.workController.Proto.SendWorkToArchive;
import ru.irenproject.common.workController.Proto.ShutDownWorkResult;
import ru.irenproject.common.workController.Proto.WatchWork;
import ru.irenproject.common.workController.Proto.WorkControllerNotice;
import ru.irenproject.common.workController.Proto.WorkControllerReply;
import ru.irenproject.common.workController.Proto.WorkControllerRequest;
import ru.irenproject.common.workController.Proto.WorkList;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.collect.ImmutableList;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.Empty;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.HashSet;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class WorkControllerChannel extends AbstractBehavior<WorkControllerChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final WorkControllerRequest request;
    public final RequestContext requestContext;

    public RequestMessage(WorkControllerRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    WorkControllerChannel create(
        ActorContext<Message> context,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, connectionHandler));
    }
  }

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final ActorRef<ConnectionHandler.Message> fConnectionHandler;
  private final WorkListSender.Factory fWorkListSenderFactory;
  private final WatcherChannel.Factory fWatcherChannelFactory;
  private final ShutDownWorkMediator.Factory fShutDownWorkMediatorFactory;

  private @Nullable ActorRef<WorkListSender.Message> fWorkListSender;
  private final HashSet<ByteString> fWatchedWorkIds = new HashSet<>();

  @Inject private WorkControllerChannel(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted ActorRef<ConnectionHandler.Message> connectionHandler,
      WorkListSender.Factory workListSenderFactory,
      WatcherChannel.Factory watcherChannelFactory,
      ShutDownWorkMediator.Factory shutDownWorkMediatorFactory) {
    fContext = context;
    fConnection = connection;
    fConnectionHandler = connectionHandler;
    fWorkListSenderFactory = workListSenderFactory;
    fWatcherChannelFactory = watcherChannelFactory;
    fShutDownWorkMediatorFactory = shutDownWorkMediatorFactory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessage(WatcherChannelOpened.class, this::onWatcherChannelOpened)
        .onMessage(WatcherChannelTerminated.class, this::onWatcherChannelTerminated)
        .onMessage(ShutDownWorkComplete.class, this::onShutDownWorkComplete)
        .build();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    WorkControllerRequest r = m.request;
    switch (r.getRequestCase()) {
      case WATCH_WORK_LIST: return onWatchWorkList(m.requestContext);
      case UNWATCH_WORK_LIST: return onUnwatchWorkList();
      case WATCH_WORK: return onWatchWork(r.getWatchWork(), m.requestContext);
      case DELETE_WORK: return onDeleteWork(r.getDeleteWork(), m.requestContext);
      case SEND_WORK_TO_ARCHIVE: return onSendWorkToArchive(r.getSendWorkToArchive(), m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onWatchWorkList(RequestContext requestContext) {
    Check.input(fWorkListSender == null);

    Function<ImmutableList<Work>, ServerMessage> noticeBuilder = createNoticeBuilder(requestContext.id());

    fWorkListSender = fContext.spawn(
        WorkListSender.Factory.makeBehavior(fWorkListSenderFactory, fConnection, noticeBuilder),
        Utils.makeUniqueChildActorName(WorkListSender.class.getSimpleName(), fContext));

    return this;
  }

  private static Function<ImmutableList<Work>, ServerMessage> createNoticeBuilder(long noticeChannelId) {
    return workList -> ServerMessage.newBuilder()
        .setChannelId(noticeChannelId)
        .setWorkControllerNotice(WorkControllerNotice.newBuilder()
            .setWorkList(WorkList.newBuilder()
                .addAllItem(workList.stream()
                    .map(work -> WorkList.Item.newBuilder()
                        .setId(Ids.toHexString(work.id()))
                        .setTitle(work.title())
                        .setStartedAt(Utils.instantToTimestamp(work.startedAt()))
                        .build())
                    .collect(Collectors.toList()))))
        .build();
  }

  private Behavior<Message> onUnwatchWorkList() {
    if (fWorkListSender != null) {
      fContext.stop(fWorkListSender);
      fWorkListSender = null;
    }

    return this;
  }

  private Behavior<Message> onWatchWork(WatchWork m, RequestContext requestContext) {
    ByteString workId = Check.inputNotNull(Ids.tryFromHexString(m.getWorkId()));
    Check.input(fWatchedWorkIds.add(workId));

    fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
        WatcherChannel.Factory.makeBehavior(
            fWatcherChannelFactory,
            workId,
            fConnection,
            m.getWatcherNoticeChannelId(),
            Locale.forLanguageTag(m.getLanguage())),
        ClientMessage::hasWatcherRequest,
        (r, rc) -> new WatcherChannel.RequestMessage(r.getWatcherRequest(), rc),
        result -> new WatcherChannelOpened(result, requestContext, workId),
        fContext.getSelf()));

    return this;
  }

  private static final class WatcherChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<WatcherChannel.Message> result;
    final RequestContext requestContext;
    final ByteString workId;

    WatcherChannelOpened(ConnectionHandler.SpawnChannelResult<WatcherChannel.Message> result,
        RequestContext requestContext, ByteString workId) {
      this.result = result;
      this.requestContext = requestContext;
      this.workId = workId;
    }
  }

  private Behavior<Message> onWatcherChannelOpened(WatcherChannelOpened m) {
    send(WorkControllerReply.newBuilder()
        .setWatchWorkReply(WatchWork.Reply.newBuilder()
            .setWatcherChannelId(m.result.channelId))
        .build(), m.requestContext);

    fContext.watchWith(
        m.result.channel,
        new WatcherChannelTerminated(m.requestContext.id(), m.workId));

    m.result.channel.tell(WatcherChannel.Activate.INSTANCE);

    return this;
  }

  private static final class WatcherChannelTerminated implements Message {
    final long noticeChannelId;
    final ByteString workId;

    WatcherChannelTerminated(long noticeChannelId, ByteString workId) {
      this.noticeChannelId = noticeChannelId;
      this.workId = workId;
    }
  }

  private Behavior<Message> onWatcherChannelTerminated(WatcherChannelTerminated m) {
    fWatchedWorkIds.remove(m.workId);

    sendNotice(WorkControllerNotice.newBuilder()
        .setWatcherChannelClosed(Empty.getDefaultInstance())
        .build(), m.noticeChannelId);

    return this;
  }

  private void send(WorkControllerReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setWorkControllerReply(reply)
        .build());
  }

  private void sendNotice(WorkControllerNotice notice, long noticeChannelId) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(noticeChannelId)
        .setWorkControllerNotice(notice)
        .build());
  }

  private Behavior<Message> onDeleteWork(DeleteWork m, RequestContext requestContext) {
    ByteString workId = Check.inputNotNull(Ids.tryFromHexString(m.getWorkId()));

    shutDownWork(
        workId,
        WorkHub.ShutDownWorkAction.DELETE,
        result -> WorkControllerReply.newBuilder()
            .setDeleteWorkReply(DeleteWork.Reply.newBuilder()
                .setResult(result))
            .build(),
        requestContext);

    return this;
  }

  private Behavior<Message> onSendWorkToArchive(SendWorkToArchive m, RequestContext requestContext) {
    ByteString workId = Check.inputNotNull(Ids.tryFromHexString(m.getWorkId()));

    shutDownWork(
        workId,
        WorkHub.ShutDownWorkAction.SEND_TO_ARCHIVE,
        result -> WorkControllerReply.newBuilder()
            .setSendWorkToArchiveReply(SendWorkToArchive.Reply.newBuilder()
                .setResult(result))
            .build(),
        requestContext);

    return this;
  }

  private void shutDownWork(
      ByteString workId,
      WorkHub.ShutDownWorkAction action,
      Function<ShutDownWorkResult, WorkControllerReply> replyBuilder,
      RequestContext requestContext) {
    fContext.spawn(
        ShutDownWorkMediator.Factory.makeBehavior(
            fShutDownWorkMediatorFactory,
            workId,
            new WorkHub.ShutDownWork<>(
                action,
                result -> new ShutDownWorkComplete(result, replyBuilder, requestContext),
                fContext.getSelf())),
        Utils.makeUniqueChildActorName(ShutDownWorkMediator.class.getSimpleName(), fContext));
  }

  private static final class ShutDownWorkComplete implements Message {
    final ShutDownWorkResult result;
    final Function<ShutDownWorkResult, WorkControllerReply> replyBuilder;
    final RequestContext requestContext;

    ShutDownWorkComplete(ShutDownWorkResult result, Function<ShutDownWorkResult, WorkControllerReply> replyBuilder,
        RequestContext requestContext) {
      this.result = result;
      this.replyBuilder = replyBuilder;
      this.requestContext = requestContext;
    }
  }

  private Behavior<Message> onShutDownWorkComplete(ShutDownWorkComplete m) {
    send(m.replyBuilder.apply(m.result), m.requestContext);
    return this;
  }
}
