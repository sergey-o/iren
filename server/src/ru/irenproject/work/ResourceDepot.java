/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;

import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.protobuf.ByteString;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Immutable public final class ResourceDepot {
  @Immutable public static final class Resource {
    private final ByteString fGzippedData;
    private final String fEtag;

    private Resource(ByteString gzippedData, String etag) {
      fGzippedData = gzippedData;
      fEtag = etag;
    }

    public ByteString gzippedData() {
      return fGzippedData;
    }

    public String etag() {
      return fEtag;
    }
  }

  private final ImmutableMap<String, Resource> fResources;

  public ResourceDepot(Path zipPath) {
    try {
      ImmutableMap<String, Resource> resources;
      try (ZipFile zip = new ZipFile(zipPath.toFile())) {
        resources = load(zip);
      } catch (NoSuchFileException ignored) {
        resources = ImmutableMap.of();
      }
      fResources = resources;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ImmutableMap<String, Resource> load(ZipFile zip) {
    try {
      ImmutableMap.Builder<String, Resource> builder = ImmutableMap.builder();

      Enumeration<? extends ZipEntry> entries = zip.entries();
      while (entries.hasMoreElements()) {
        ZipEntry entry = entries.nextElement();

        ByteString data;
        try (InputStream in = zip.getInputStream(entry)) {
          data = ByteString.readFrom(in);
        }

        String etag = '"' + Utils.byteStringAsByteSource(data).hash(Hashing.sha256()).toString() + '"';

        ByteString.Output gzipped = ByteString.newOutput();
        try (GZIPOutputStream compressor = new GZIPOutputStream(gzipped)) {
          data.writeTo(compressor);
        }

        builder.put(entry.getName(), new Resource(gzipped.toByteString(), etag));
      }
      return builder.build();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public @Nullable Resource getIfExists(String name) {
    return fResources.get(name);
  }
}
