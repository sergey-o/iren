/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.InitializationTracker;
import ru.irenproject.QuestionItem;
import ru.irenproject.Section;
import ru.irenproject.Utils;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.archive.BadArchiveVersionException;
import ru.irenproject.archive.Proto.RecordedQuestion;
import ru.irenproject.archive.Proto.RecordedSection;
import ru.irenproject.archive.Proto.RecordedSession;
import ru.irenproject.archive.Proto.RecordedWork;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.workController.Proto.ShutDownWorkResult;
import ru.irenproject.itx.ItxWriter;
import ru.irenproject.live.JsonProtoRegistry;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;
import ru.irenproject.work.db.DbReader;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.math.IntMath;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.output.CloseShieldOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public final class WorkArchiver extends AbstractBehavior<WorkArchiver.Message> {
  public interface Message {}

  public static final class PutIntoArchive<R> implements Message {
    public final LiveWork liveWork;
    public final Function<ShutDownWorkResult, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public PutIntoArchive(LiveWork liveWork, Function<ShutDownWorkResult, R> replyBuilder, ActorRef<R> replyTo) {
      this.liveWork = liveWork;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public enum Stop implements Message { INSTANCE }

  public interface Factory {
    WorkArchiver create(ActorContext<Message> context);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(WorkArchiver.class);

  private static final DateTimeFormatter ENTRY_NAME_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern(
      ArchiveUtils.ENTRY_NAME_TIMESTAMP_PATTERN).withZone(ZoneId.systemDefault());

  private final ArchiveLocation fArchiveLocation;
  private final JsonProtoRegistry fJsonProtoRegistry;
  private final ItxWriter.Factory fItxWriterFactory;

  @Inject private WorkArchiver(
      @Assisted ActorContext<Message> context, // currently unused
      InitializationTracker initializationTracker,
      ArchiveLocation archiveLocation,
      JsonProtoRegistry jsonProtoRegistry,
      ItxWriter.Factory itxWriterFactory) {
    fArchiveLocation = archiveLocation;
    fJsonProtoRegistry = jsonProtoRegistry;
    fItxWriterFactory = itxWriterFactory;

    initializationTracker.recordCompletion(getClass());
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(PutIntoArchive.class, m -> onPutIntoArchive((PutIntoArchive<?>) m))
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .build();
  }

  private <R> Behavior<Message> onPutIntoArchive(PutIntoArchive<R> m) {
    ShutDownWorkResult result;

    Path archiveFile = fArchiveLocation.getIfDefined();
    if (archiveFile == null) {
      result = ShutDownWorkResult.SEND_TO_ARCHIVE_FAILED;
      fLogger.warn("Archive location is not defined.");
    } else {
      try {
        archive(m.liveWork, archiveFile);
        result = ShutDownWorkResult.OK;
      } catch (BadArchiveVersionException e) {
        result = e.tooNew() ?
            ShutDownWorkResult.UNKNOWN_ARCHIVE_FILE_VERSION :
            ShutDownWorkResult.INCORRECT_ARCHIVE_FILE;
      } catch (RuntimeException e) {
        result = ShutDownWorkResult.SEND_TO_ARCHIVE_FAILED;
        fLogger.warn("Cannot send to archive:", e);
      }
    }

    m.replyTo.tell(m.replyBuilder.apply(result));

    return this;
  }

  private void archive(LiveWork liveWork, Path archiveFile) {
    try {
      Instant archivedAt = Utils.milliClock().instant();

      Utils.tryCreateAncestorDirectories(archiveFile);
      Path tempArchiveFile = Utils.changeFileName(archiveFile, Utils::generateTempFileName);

      boolean deleteTempArchive = true;
      try {
        try (
            SeekableByteChannel tempArchiveChannel = Files.newByteChannel(tempArchiveFile,
                StandardOpenOption.CREATE_NEW, StandardOpenOption.WRITE, StandardOpenOption.READ);
            ZipArchiveOutputStream tempArchive = new ZipArchiveOutputStream(tempArchiveChannel)) {
          LinkedHashSet<String> entryNames = new LinkedHashSet<>();

          ArchiveUtils.writeVersion(tempArchive, archivedAt);
          entryNames.add(ArchiveUtils.VERSION_ENTRY_NAME);

          copyEntries(archiveFile, tempArchive, entryNames);

          RecordedWork recordedWork;
          try (ClosableWorkSource workSource = liveWork.acquireSource()) {
            recordedWork = liveWork.store().readDirectly(dbr -> recordWork(
                liveWork.work(),
                workSource,
                dbr,
                archivedAt,
                new ArchiveObjectWriter(tempArchive, entryNames, archivedAt)));
          }

          tempArchive.putArchiveEntry(ArchiveUtils.createZipEntry(
              composeWorkEntryName(liveWork.work(), archivedAt, entryNames),
              archivedAt));
          try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
              new CloseShieldOutputStream(tempArchive), StandardCharsets.UTF_8))) {
            JsonFormat.printer().usingTypeRegistry(fJsonProtoRegistry.get()).appendTo(recordedWork, writer);
          }
          tempArchive.closeArchiveEntry();
        }

        Utils.renameFilePreferablyAtomically(tempArchiveFile, archiveFile);
        deleteTempArchive = false;
      } finally {
        if (deleteTempArchive) {
          Files.deleteIfExists(tempArchiveFile);
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private RecordedWork recordWork(
      Work work,
      WorkSource workSource,
      DbReader dbr,
      Instant archivedAt,
      ArchiveObjectWriter objectWriter) {
    try {
      HashMap<String, String> questionObjectIds = new HashMap<>();
      HashSet<String> tags = new HashSet<>();
      MarkScale markScale = workSource.profile().markScale();

      RecordedWork.Builder recordedWork = RecordedWork.newBuilder()
          .setTestName(work.title())
          .setStartedAt(Utils.instantToTimestamp(work.startedAt()))
          .setArchivedAt(Utils.instantToTimestamp(archivedAt));

      SQLiteStatement sessionQuery = dbr.prepare("SELECT DATA, STATUS FROM SESSIONS ORDER BY ID");
      try {
        SQLiteStatement questionQuery = dbr.prepare("SELECT LIVE_QUESTION, RESPONSE FROM SCREENS WHERE ID = ?1");
        try {
          while (sessionQuery.step()) {
            Session session = Session.parseFrom(sessionQuery.columnBlob(0));
            SessionStatus status = SessionStatus.parseFrom(sessionQuery.columnBlob(1));

            RecordedSession.Builder recordedSession = RecordedSession.newBuilder()
                .setUserName(session.getUserName())
                .setScore(BaseWorkUtils.unscale(session.getScaledScore()).toPlainString())
                .setPerfectScore(session.getPerfectScore())
                .setAttemptedScore(session.getAttemptedScore())
                .setBanned(status.getBanned())
                .addConnection(session.getFirstConnection());
            if (session.hasUserDisplayName()) {
              recordedSession.setUserDisplayName(session.getUserDisplayName());
            }
            if (session.hasGroupName()) {
              recordedSession.setGroupName(session.getGroupName());
            }

            Integer markIndex = markScale.getMarkIndexForResult(BaseWorkUtils.computeSessionResult(session));
            if (markIndex != null) {
              recordedSession.setMark(markScale.getTitle(markIndex));
            }

            for (SessionQuestion sq : session.getQuestionList()) {
              Check.that(questionQuery
                  .reset()
                  .bind(1, sq.getScreenId())
                  .step());

              _LiveQuestion lq = _LiveQuestion.parseFrom(questionQuery.columnBlob(0));
              QuestionItem questionItem = workSource.getQuestionItem(lq.getSource());

              String questionObjectId = questionObjectIds.computeIfAbsent(lq.getSource(),
                  ignored -> objectWriter.writeXml(
                      w -> fItxWriterFactory.create(w, objectWriter).writeQuestion(questionItem.question())));

              RecordedQuestion.Builder recordedQuestion = RecordedQuestion.newBuilder()
                  .setQuestion(lq.toBuilder()
                      .setSource(questionObjectId));
              if (sq.hasScaledResult()) {
                recordedQuestion
                    .setResponse(DialogResponse.parseFrom(Check.notNull(questionQuery.columnBlob(1))))
                    .setResult(BaseWorkUtils.unscale(sq.getScaledResult()).toPlainString());
              }

              if (!questionItem.tag().isEmpty()) {
                recordedQuestion.setTag(questionItem.tag());
                tags.add(questionItem.tag());
              }

              recordedSession.addQuestion(recordedQuestion);
            }

            recordedWork.addSession(recordedSession);
          }
        } finally {
          questionQuery.dispose();
        }
      } finally {
        sessionQuery.dispose();
      }

      return recordedWork
          .addAllSection(recordSections(workSource.test().root(), tags))
          .setProfile(
              objectWriter.writeXml(
                  w -> fItxWriterFactory.create(w, objectWriter).writeProfile(workSource.profile())))
          .build();
    } catch (SQLiteException | InvalidProtocolBufferException e) {
      throw new RuntimeException(e);
    }
  }

  private static void copyEntries(Path sourceFile, ZipArchiveOutputStream target, Set<String> copiedEntryNames) {
    try {
      if (Files.exists(sourceFile)) {
        try (ZipFile source = new ZipFile(sourceFile.toFile())) {
          ArchiveUtils.checkVersion(source);

          Enumeration<ZipArchiveEntry> entries = source.getEntries();
          while (entries.hasMoreElements()) {
            ZipArchiveEntry entry = entries.nextElement();
            if (!entry.getName().equals(ArchiveUtils.VERSION_ENTRY_NAME)) {
              copiedEntryNames.add(entry.getName());
              target.addRawArchiveEntry(entry, source.getRawInputStream(entry));
            }
          }
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static String composeWorkEntryName(Work work, Instant archivedAt, Set<String> existingEntryNames) {
    String timestamp = ENTRY_NAME_TIMESTAMP_FORMATTER.format(archivedAt);
    int n = 0;
    String res;

    do {
      n = IntMath.checkedAdd(n, 1);
      res = String.format("[%s%s] %s", timestamp, (n == 1) ? "" : " n=" + n, work.title());
    } while (existingEntryNames.contains(res));

    return res;
  }

  private static List<RecordedSection> recordSections(Section parent, Set<String> tagsToInclude) {
    ArrayList<RecordedSection> res = new ArrayList<>();

    for (Section s : parent.sections()) {
      RecordedSection.Builder rs = RecordedSection.newBuilder()
          .addAllQuestion(s.questionList().items().stream()
              .map(QuestionItem::tag)
              .filter(tagsToInclude::contains)
              .collect(Collectors.toList()))
          .addAllSection(recordSections(s, tagsToInclude));

      if ((rs.getQuestionCount() > 0) || (rs.getSectionCount() > 0)) {
        res.add(rs
            .setName(s.name())
            .build());
      }
    }

    return res;
  }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }
}
