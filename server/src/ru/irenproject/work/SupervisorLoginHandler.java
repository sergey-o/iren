/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.RequestContext;
import ru.irenproject.authentication.UserManagerChannel;
import ru.irenproject.common.login.Proto.Supervise;
import ru.irenproject.common.protocol.Proto.ClientMessage;

import akka.actor.typed.ActorRef;
import akka.actor.typed.javadsl.ActorContext;
import com.google.inject.assistedinject.Assisted;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;

final class SupervisorLoginHandler {
  public interface Message extends LoginChannel.Message {}

  public enum Activate implements Message { INSTANCE }

  interface Factory {
    SupervisorLoginHandler create(
        ActorContext<LoginChannel.Message> context,
        Supervise request,
        RequestContext requestContext,
        Connection connection,
        ActorRef<ConnectionHandler.Message> connectionHandler,
        KeyVerifier keyVerifier,
        @Nullable String serverAddress);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(SupervisorLoginHandler.class);

  private final ActorContext<LoginChannel.Message> fContext;
  private final Supervise fRequest;
  private final RequestContext fRequestContext;
  private final Connection fConnection;
  private final ActorRef<ConnectionHandler.Message> fConnectionHandler;
  private final KeyVerifier fKeyVerifier;
  private final @Nullable String fServerAddress;
  private final LoginRateLimiter fLoginRateLimiter;
  private final UserManagerChannel.Factory fUserManagerChannelFactory;
  private final WorkCreatorChannel.Factory fWorkCreatorChannelFactory;
  private final WorkControllerChannel.Factory fWorkControllerChannelFactory;
  private final ArchiveManagerChannel.Factory fArchiveManagerChannelFactory;

  private /* @Nullable */Supervise.Reply.Ok.Builder fOk;
  private @Nullable Supervise.Reply fReply;

  @Inject private SupervisorLoginHandler(
      @Assisted ActorContext<LoginChannel.Message> context,
      @Assisted Supervise request,
      @Assisted RequestContext requestContext,
      @Assisted Connection connection,
      @Assisted ActorRef<ConnectionHandler.Message> connectionHandler,
      @Assisted KeyVerifier keyVerifier,
      @Assisted @Nullable String serverAddress,
      LoginRateLimiter loginRateLimiter,
      UserManagerChannel.Factory userManagerChannelFactory,
      WorkCreatorChannel.Factory workCreatorChannelFactory,
      WorkControllerChannel.Factory workControllerChannelFactory,
      ArchiveManagerChannel.Factory archiveManagerChannelFactory) {
    fContext = context;
    fRequest = request;
    fRequestContext = requestContext;
    fConnection = connection;
    fConnectionHandler = connectionHandler;
    fKeyVerifier = keyVerifier;
    fServerAddress = serverAddress;
    fLoginRateLimiter = loginRateLimiter;
    fUserManagerChannelFactory = userManagerChannelFactory;
    fWorkCreatorChannelFactory = workCreatorChannelFactory;
    fWorkControllerChannelFactory = workControllerChannelFactory;
    fArchiveManagerChannelFactory = archiveManagerChannelFactory;
  }

  public void handle(Message m) {
    if (m instanceof Activate) {
      onActivate();
    } else if (m instanceof UserManagerChannelOpened) {
      onUserManagerChannelOpened((UserManagerChannelOpened) m);
    } else if (m instanceof WorkCreatorChannelOpened) {
      onWorkCreatorChannelOpened((WorkCreatorChannelOpened) m);
    } else if (m instanceof WorkControllerChannelOpened) {
      onWorkControllerChannelOpened((WorkControllerChannelOpened) m);
    } else if (m instanceof ArchiveManagerChannelOpened) {
      onArchiveManagerChannelOpened((ArchiveManagerChannelOpened) m);
    } else {
      throw new RuntimeException(m.toString());
    }
  }

  public @Nullable Supervise.Reply reply() {
    return fReply;
  }

  public RequestContext requestContext() {
    return fRequestContext;
  }

  private void onActivate() {
    if (fLoginRateLimiter.allowed(fConnection.clientAddress())) {
      boolean correctKey = fKeyVerifier.verify(fRequest.getSupervisorKey().toByteArray());
      if (fLoginRateLimiter.registerAttempt(fConnection.clientAddress(), correctKey)) {
        if (correctKey) {
          fLogger.info("Supervisor login {}.", fConnection);

          fOk = Supervise.Reply.Ok.newBuilder();
          if (fServerAddress != null) {
            fOk.setServerAddress(fServerAddress);
          }

          fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
              UserManagerChannel.Factory.makeBehavior(fUserManagerChannelFactory, fConnection),
              ClientMessage::hasUserManagerRequest,
              (r, rc) -> new UserManagerChannel.RequestMessage(r.getUserManagerRequest(), rc),
              UserManagerChannelOpened::new,
              fContext.getSelf()));
        } else {
          fLogger.warn("Incorrect supervisor password {}.", fConnection);
          fReply = makeFailure(Supervise.Reply.Failure.Reason.INCORRECT_KEY);
        }
      } else {
        signalTooManyFailures();
      }
    } else {
      signalTooManyFailures();
    }
  }

  private void signalTooManyFailures() {
    fLogger.warn("Too many failed login attempts {}.", fConnection);
    fReply = makeFailure(Supervise.Reply.Failure.Reason.TOO_MANY_FAILURES);
  }

  private static Supervise.Reply makeFailure(Supervise.Reply.Failure.Reason reason) {
    return Supervise.Reply.newBuilder()
        .setFailure(Supervise.Reply.Failure.newBuilder()
            .setReason(reason))
        .build();
  }

  private static final class UserManagerChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<UserManagerChannel.Message> result;

    UserManagerChannelOpened(ConnectionHandler.SpawnChannelResult<UserManagerChannel.Message> result) {
      this.result = result;
    }
  }

  private void onUserManagerChannelOpened(UserManagerChannelOpened m) {
    fOk.setUserManagerChannelId(m.result.channelId);

    fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
        WorkCreatorChannel.Factory.makeBehavior(fWorkCreatorChannelFactory, fConnection),
        ClientMessage::hasWorkCreatorRequest,
        (r, rc) -> new WorkCreatorChannel.RequestMessage(r.getWorkCreatorRequest(), rc),
        WorkCreatorChannelOpened::new,
        fContext.getSelf()));
  }

  private static final class WorkCreatorChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<WorkCreatorChannel.Message> result;

    WorkCreatorChannelOpened(ConnectionHandler.SpawnChannelResult<WorkCreatorChannel.Message> result) {
      this.result = result;
    }
  }

  private void onWorkCreatorChannelOpened(WorkCreatorChannelOpened m) {
    fOk.setWorkCreatorChannelId(m.result.channelId);

    fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
        WorkControllerChannel.Factory.makeBehavior(
            fWorkControllerChannelFactory, fConnection, fConnectionHandler),
        ClientMessage::hasWorkControllerRequest,
        (r, rc) -> new WorkControllerChannel.RequestMessage(r.getWorkControllerRequest(), rc),
        WorkControllerChannelOpened::new,
        fContext.getSelf()));
  }

  private static final class WorkControllerChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<WorkControllerChannel.Message> result;

    WorkControllerChannelOpened(ConnectionHandler.SpawnChannelResult<WorkControllerChannel.Message> result) {
      this.result = result;
    }
  }

  private void onWorkControllerChannelOpened(WorkControllerChannelOpened m) {
    fOk.setWorkControllerChannelId(m.result.channelId);

    fConnectionHandler.tell(new ConnectionHandler.SpawnChannel<>(
        ArchiveManagerChannel.Factory.makeBehavior(fArchiveManagerChannelFactory, fConnection),
        ClientMessage::hasArchiveManagerRequest,
        (r, rc) -> new ArchiveManagerChannel.RequestMessage(r.getArchiveManagerRequest(), rc),
        ArchiveManagerChannelOpened::new,
        fContext.getSelf()));
  }

  private static final class ArchiveManagerChannelOpened implements Message {
    final ConnectionHandler.SpawnChannelResult<ArchiveManagerChannel.Message> result;

    ArchiveManagerChannelOpened(ConnectionHandler.SpawnChannelResult<ArchiveManagerChannel.Message> result) {
      this.result = result;
    }
  }

  private void onArchiveManagerChannelOpened(ArchiveManagerChannelOpened m) {
    fOk.setArchiveManagerChannelId(m.result.channelId);

    fReply = Supervise.Reply.newBuilder()
        .setOk(fOk)
        .build();
  }
}
