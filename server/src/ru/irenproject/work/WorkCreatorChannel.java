/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.BaseUtils;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.Test;
import ru.irenproject.Utils;
import ru.irenproject.authentication.DefaultAuthenticationMode;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.workCreator.Proto.BeginCreateWork;
import ru.irenproject.common.workCreator.Proto.CompleteCreateWork;
import ru.irenproject.common.workCreator.Proto.PerformProfileSelectorAction;
import ru.irenproject.common.workCreator.Proto.WorkCreatorReply;
import ru.irenproject.common.workCreator.Proto.WorkCreatorRequest;
import ru.irenproject.common.workCreator.Proto.XWorkProfileSelector;
import ru.irenproject.editor.DocumentEditor;
import ru.irenproject.editor.EditorDriver;
import ru.irenproject.editor.EditorProcessor;
import ru.irenproject.editor.profile.ProfileScreen;
import ru.irenproject.infra.FileBlobStore;
import ru.irenproject.infra.KryoFactory;
import ru.irenproject.infra.Proto.Reply;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.itx.ItxInputException;
import ru.irenproject.itx.ItxOutput;
import ru.irenproject.profile.Profile;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.google.common.io.MoreFiles;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.SecureRandom;

public final class WorkCreatorChannel extends AbstractBehavior<WorkCreatorChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final WorkCreatorRequest request;
    public final RequestContext requestContext;

    public RequestMessage(WorkCreatorRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    WorkCreatorChannel create(
        ActorContext<Message> context,
        Connection connection);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection));
    }
  }

  private final Connection fConnection;
  private final WorkDirectory fWorkDirectory;
  private final ItxInput fItxInput;
  private final ItxOutput fItxOutput;
  private final KryoFactory fKryoFactory;
  private final EditorProcessor.Factory fEditorProcessorFactory;
  private final ProfileScreen.Factory fProfileScreenFactory;
  private final DefaultAuthenticationMode fDefaultAuthenticationMode;
  private final ActorRef<WorkCatalog.Message> fWorkCatalog;

  private @Nullable Job fJob;

  @Inject private WorkCreatorChannel(
      @Assisted ActorContext<Message> context, // currently unused
      @Assisted Connection connection,
      WorkDirectory workDirectory,
      ItxInput itxInput,
      ItxOutput itxOutput,
      KryoFactory kryoFactory,
      EditorProcessor.Factory editorProcessorFactory,
      ProfileScreen.Factory profileScreenFactory,
      DefaultAuthenticationMode defaultAuthenticationMode,
      ActorRef<WorkCatalog.Message> workCatalog) {
    fConnection = connection;
    fWorkDirectory = workDirectory;
    fItxInput = itxInput;
    fItxOutput = itxOutput;
    fKryoFactory = kryoFactory;
    fEditorProcessorFactory = editorProcessorFactory;
    fProfileScreenFactory = profileScreenFactory;
    fDefaultAuthenticationMode = defaultAuthenticationMode;
    fWorkCatalog = workCatalog;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(PostStop.class, ignored -> onPostStop())
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .build();
  }

  private Behavior<Message> onPostStop() {
    cleanUpJob(false);
    return this;
  }

  private void cleanUpJob(boolean keepWorkDirectory) {
    if (fJob != null) {
      if (fJob.editorProcessor != null) {
        fJob.editorProcessor.close();
      }
      if (!keepWorkDirectory) {
        fWorkDirectory.tryDelete(fJob.workId);
      }
      fJob = null;
    }
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    WorkCreatorRequest r = m.request;
    switch (r.getRequestCase()) {
      case BEGIN_CREATE_WORK: return onBeginCreateWork(r.getBeginCreateWork(), m.requestContext);
      case PERFORM_PROFILE_SELECTOR_ACTION: return onPerformProfileSelectorAction(
          r.getPerformProfileSelectorAction(), m.requestContext);
      case COMPLETE_CREATE_WORK: return onCompleteCreateWork(r.getCompleteCreateWork(), m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onBeginCreateWork(BeginCreateWork m, RequestContext requestContext) {
    try {
      Check.input(fJob == null);
      fJob = new Job(
          Work.BAD_TITLE_CHAR_MATCHER.replaceFrom(BaseUtils.shortenString(m.getTitle(), 200, 215), '_'),
          m.getLanguage(),
          generateWorkIdAndCreateDirectory());

      Path tempSourceFile = fWorkDirectory.getTempSourceFile(fJob.workId);
      try (OutputStream out = Files.newOutputStream(tempSourceFile)) {
        m.getTest().writeTo(out);
      }

      BeginCreateWork.Reply.Result result;
      try {
        fJob.editorProcessor = fEditorProcessorFactory.create(
            () -> EditorDriver.createBasic(new FileBlobStore(fWorkDirectory.getBlobStoreDirectory(fJob.workId))),
            (ignored, realm) -> open(tempSourceFile, realm, m.getDefaultProfileTitle()),
            null);
        result = BeginCreateWork.Reply.Result.OK;
      } catch (ItxInputException e) {
        result = (e.reason() == ItxInputException.Reason.UNKNOWN_VERSION) ?
            BeginCreateWork.Reply.Result.UNKNOWN_TEST_FILE_VERSION :
            BeginCreateWork.Reply.Result.INCORRECT_TEST_FILE;
        cleanUpJob(false);
      }

      if (fJob != null) {
        Utils.tryDeleteFile(tempSourceFile);
      }

      send(WorkCreatorReply.newBuilder()
          .setBeginCreateWorkReply(BeginCreateWork.Reply.newBuilder()
              .setResult(result))
          .build(), requestContext);

      return this;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class Job {
    final String title;
    final String language;
    final ByteString workId;
    EditorProcessor editorProcessor;

    Job(String title, String language, ByteString workId) {
      this.title = title;
      this.language = language;
      this.workId = workId;
    }
  }

  private ByteString generateWorkIdAndCreateDirectory() {
    byte[] ba = new byte[Ids.SIZE];
    new SecureRandom().nextBytes(ba);
    ByteString res = Ids.fromBytes(ba);

    fWorkDirectory.create(res);

    return res;
  }

  private WorkProfileSelector open(Path sourceFile, Realm realm, String defaultProfileTitle) {
    Test test = fItxInput.readTest(sourceFile, realm);

    Utils.freeze(
        test,
        fKryoFactory.createForWriting(),
        MoreFiles.asByteSink(fWorkDirectory.getTempTestFile(Check.notNull(fJob).workId)));

    Profile defaultProfile = Profile.createDefault(realm);
    defaultProfile.setTitle(defaultProfileTitle);
    test.profileList().add(defaultProfile);

    return new WorkProfileSelector(test, fProfileScreenFactory);
  }

  private void send(WorkCreatorReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setWorkCreatorReply(reply)
        .build());
  }

  private static final class WorkProfileSelector extends DocumentEditor {
    WorkProfileSelector(Test test, ProfileScreen.Factory profileScreenFactory) {
      super(test.realm());
      pushScreen(profileScreenFactory.create(test.profileList(), test.root(), ProfileScreen.Mode.PROFILE_SELECTOR));
    }

    @Override public com.google.protobuf.Message render(RenderContext context) {
      return XWorkProfileSelector.newBuilder()
          .setScreen(context.link(topScreen()))
          .build();
    }

    ProfileScreen profileScreen() {
      return (ProfileScreen) topScreen();
    }
  }

  private Behavior<Message> onPerformProfileSelectorAction(PerformProfileSelectorAction m,
      RequestContext requestContext) {
    Reply reply = Check.inputNotNull(fJob).editorProcessor.process(m.getRequest());

    send(WorkCreatorReply.newBuilder()
        .setPerformProfileSelectorActionReply(PerformProfileSelectorAction.Reply.newBuilder()
            .setReply(reply))
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onCompleteCreateWork(CompleteCreateWork m, RequestContext requestContext) {
    Check.inputNotNull(fJob);

    if (m.getCommit()) {
      Profile selectedProfile = Check.notNull(
          ((WorkProfileSelector) fJob.editorProcessor.documentEditor()).profileScreen().selectedProfile());

      Realm editorRealm = selectedProfile.realm();
      Realm workRealm = new Realm(Utils::logUnexpectedEvent, null, editorRealm.blobStore());

      Path tempTestFile = fWorkDirectory.getTempTestFile(fJob.workId);
      Test test = Utils.unfreeze(
          MoreFiles.asByteSource(tempTestFile),
          fKryoFactory.createForReading(workRealm),
          Test.class);
      Utils.tryDeleteFile(tempTestFile);

      Profile profile = copyProfile(selectedProfile, test, fWorkDirectory.getTempProfileFile(fJob.workId));

      Utils.freeze(
          new FrozenWorkSource(test, profile),
          fKryoFactory.createForWriting(),
          MoreFiles.asByteSink(fWorkDirectory.getFrozenWorkSourceFile(fJob.workId)));

      createDatabase(fJob.workId);

      Work work = new Work(
          fJob.workId,
          fJob.title,
          fJob.language,
          Utils.milliClock().instant(),
          fDefaultAuthenticationMode.get());

      fWorkDirectory.writeAndMarkAvailable(work);
      fWorkCatalog.tell(new WorkCatalog.Open(work));
    }

    cleanUpJob(m.getCommit());

    send(WorkCreatorReply.newBuilder()
        .setCompleteCreateWorkReply(CompleteCreateWork.Reply.getDefaultInstance())
        .build(), requestContext);

    return this;
  }

  private Profile copyProfile(Profile profile, Test referenceTestInTargetRealm, Path tempFile) {
    try {
      try (OutputStream temp = Files.newOutputStream(tempFile)) {
        fItxOutput.writeFragment(out -> out.writeProfile(profile), temp, null);
      }

      Profile res = fItxInput.read(tempFile, referenceTestInTargetRealm.realm(),
          reader -> reader.readAndLinkProfile(
              Itx.getChild(reader.root(), Itx.ELEM_PROFILE),
              referenceTestInTargetRealm));

      Utils.tryDeleteFile(tempFile);

      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void createDatabase(ByteString workId) {
    try {
      SQLiteConnection connection = new SQLiteConnection(fWorkDirectory.getDatabaseFile(workId).toFile());
      connection.open();
      try {
        connection.exec("PRAGMA journal_mode = WAL");

        connection.exec("CREATE TABLE SESSIONS ("
            + "  ID INTEGER NOT NULL PRIMARY KEY"
            + ", DATA BLOB NOT NULL"
            + ", STATUS BLOB NOT NULL"
            + ", CANONICAL_USER_NAME TEXT NOT NULL UNIQUE"
            + ", KEY BLOB UNIQUE"
            + ")");
        connection.exec("CREATE TABLE SCREENS ("
            + "  ID INTEGER NOT NULL PRIMARY KEY"
            + ", LIVE_QUESTION BLOB NOT NULL"
            + ", RESPONSE BLOB"
            + ")");
      } finally {
        connection.dispose();
      }
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }
}
