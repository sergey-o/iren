/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.Utils;
import ru.irenproject.common.archiveManager.Proto.ArchiveManagerNotice;
import ru.irenproject.common.archiveManager.Proto.ArchiveManagerReply;
import ru.irenproject.common.archiveManager.Proto.ArchiveManagerRequest;
import ru.irenproject.common.archiveManager.Proto.ArchivePart;
import ru.irenproject.common.archiveManager.Proto.EndOfArchive;
import ru.irenproject.common.archiveManager.Proto.GetArchive;
import ru.irenproject.common.protocol.Proto.ServerMessage;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelFuture;
import io.netty.handler.stream.ChunkedNioFile;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.function.Function;

public final class ArchiveManagerChannel extends AbstractBehavior<ArchiveManagerChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final ArchiveManagerRequest request;
    public final RequestContext requestContext;

    public RequestMessage(ArchiveManagerRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    ArchiveManagerChannel create(
        ActorContext<Message> context,
        Connection connection);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection));
    }
  }

  private static final Logger fLogger = LoggerFactory.getLogger(ArchiveManagerChannel.class);

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final ArchiveLocation fArchiveLocation;

  private /* @Nullable */GetArchiveContext fGetArchiveContext;

  @Inject private ArchiveManagerChannel(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      ArchiveLocation archiveLocation) {
    fContext = context;
    fConnection = connection;
    fArchiveLocation = archiveLocation;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessage(SendComplete.class, this::onSendComplete)
        .build();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    ArchiveManagerRequest r = m.request;
    switch (r.getRequestCase()) {
      case GET_ARCHIVE: return onGetArchive(m.requestContext);
      case CANCEL_GET_ARCHIVE: return onCancelGetArchive();
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onGetArchive(RequestContext requestContext) {
    Check.input(fGetArchiveContext == null);

    Path archiveFile = fArchiveLocation.getIfDefined();
    OpenedArchive openedArchive = (archiveFile == null) ? null : tryOpen(archiveFile);

    send(ArchiveManagerReply.newBuilder()
        .setGetArchiveReply(GetArchive.Reply.newBuilder()
            .setArchiveAvailable(openedArchive != null))
        .build(), requestContext);

    if (openedArchive != null) {
      fGetArchiveContext = new GetArchiveContext(requestContext, openedArchive.fileChannel);
      ChannelFuture f = fConnection.sendChunked(
          Utils.transformChunkedInput(openedArchive.chunkedNioFile, createNoticeBuilder(requestContext.id())));
      f.addListener(ignored -> fContext.getSelf().tell(new SendComplete(f)));
    }

    return this;
  }

  private static final class GetArchiveContext {
    final RequestContext requestContext;
    final FileChannel archiveFileChannel;
    boolean canceled;

    GetArchiveContext(RequestContext requestContext, FileChannel archiveFileChannel) {
      this.requestContext = requestContext;
      this.archiveFileChannel = archiveFileChannel;
    }
  }

  private static @Nullable OpenedArchive tryOpen(Path file) {
    OpenedArchive res;
    FileChannel channel = null;
    try {
      channel = FileChannel.open(file);
      res = new OpenedArchive(channel, new ChunkedNioFile(channel));
      channel = null;
    } catch (IOException | RuntimeException e) {
      if (!(e instanceof NoSuchFileException)) {
        fLogger.warn("", e);
      }
      res = null;
    } finally {
      IOUtils.closeQuietly(channel);
    }
    return res;
  }

  private static final class OpenedArchive {
    final FileChannel fileChannel;
    final ChunkedNioFile chunkedNioFile;

    OpenedArchive(FileChannel fileChannel, ChunkedNioFile chunkedNioFile) {
      this.fileChannel = fileChannel;
      this.chunkedNioFile = chunkedNioFile;
    }
  }

  private static Function<ByteBuf, ServerMessage> createNoticeBuilder(long noticeChannelId) {
    return b -> {
      try {
        return toNoticeServerMessage(ArchiveManagerNotice.newBuilder()
            .setArchivePart(ArchivePart.newBuilder()
                .setData(ByteString.readFrom(new ByteBufInputStream(b), b.readableBytes())))
            .build(), noticeChannelId);
      } catch (IOException e) {
        throw new RuntimeException(e);
      } finally {
        b.release();
      }
    };
  }

  private static ServerMessage toNoticeServerMessage(ArchiveManagerNotice notice, long noticeChannelId) {
    return ServerMessage.newBuilder()
        .setChannelId(noticeChannelId)
        .setArchiveManagerNotice(notice)
        .build();
  }

  private void send(ArchiveManagerReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setArchiveManagerReply(reply)
        .build());
  }

  private Behavior<Message> onCancelGetArchive() {
    if (fGetArchiveContext != null) {
      fGetArchiveContext.canceled = true;
      try {
        fGetArchiveContext.archiveFileChannel.close();
      } catch (IOException | RuntimeException e) {
        fLogger.warn("", e);
      }
    }

    return this;
  }

  private static final class SendComplete implements Message {
    final ChannelFuture result;

    SendComplete(ChannelFuture result) {
      this.result = result;
    }
  }

  private Behavior<Message> onSendComplete(SendComplete m) {
    fConnection.send(toNoticeServerMessage(ArchiveManagerNotice.newBuilder()
        .setEndOfArchive(EndOfArchive.newBuilder()
            .setOk(m.result.isSuccess() && !fGetArchiveContext.canceled))
        .build(), fGetArchiveContext.requestContext.id()));
    fGetArchiveContext = null;

    return this;
  }
}
