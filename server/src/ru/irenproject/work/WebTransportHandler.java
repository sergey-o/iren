/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.ClosingChannelException;
import ru.irenproject.Utils;
import ru.irenproject.common.protocol.Proto.ClientMessage;
import ru.irenproject.common.protocol.Proto.ServerMessage;

import com.google.common.base.Ascii;
import com.google.common.collect.ImmutableMap;
import com.google.common.net.MediaType;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;
import io.netty.buffer.ByteBufInputStream;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpResponse;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaderNames;
import io.netty.handler.codec.http.HttpHeaderValues;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.LastHttpContent;
import io.netty.handler.codec.http.websocketx.BinaryWebSocketFrame;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PingWebSocketFrame;
import io.netty.handler.codec.http.websocketx.PongWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker13;
import io.netty.handler.stream.ChunkedStream;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.util.ReferenceCountUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.IOException;

public final class WebTransportHandler extends ChannelDuplexHandler {
  public interface Factory {
    WebTransportHandler create(boolean useRealIpHeader, ResourceDepot resourceDepot);
  }

  private enum State { NEW, HTTP, WEB_SOCKET }

  private static final Logger fLogger = LoggerFactory.getLogger(WebTransportHandler.class);

  public static final int MAX_FRAME_SIZE = 32_000_000;

  private static final ImmutableMap<String, MediaType> MEDIA_TYPES = ImmutableMap.of(
      "html", MediaType.HTML_UTF_8,
      "css", MediaType.CSS_UTF_8,
      "js", MediaType.JAVASCRIPT_UTF_8);

  private static final String CACHE_CONTROL = "max-age=0, must-revalidate";

  private final boolean fUseRealIpHeader;
  private final ResourceDepot fResourceDepot;

  private State fState = State.NEW;
  private boolean fCloseFrameSent;

  // can be converted to non-@Inject
  @Inject private WebTransportHandler(
      @Assisted boolean useRealIpHeader,
      @Assisted ResourceDepot resourceDepot) {
    fUseRealIpHeader = useRealIpHeader;
    fResourceDepot = resourceDepot;
  }

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    boolean release = true;
    try {
      if (msg instanceof FullHttpRequest) {
        handleHttpRequest((FullHttpRequest) msg, ctx);
      } else if (msg instanceof BinaryWebSocketFrame) {
        handleBinaryFrame((BinaryWebSocketFrame) msg, ctx);
      } else if (msg instanceof TextWebSocketFrame) {
        throw new BadInputException();
      } else if (msg instanceof CloseWebSocketFrame) {
        handleCloseFrame(ctx);
      } else if (msg instanceof PingWebSocketFrame) {
        handlePingFrame((PingWebSocketFrame) msg, ctx);
      } else if (msg instanceof PongWebSocketFrame) {
        // do nothing
      } else {
        release = false;
        super.channelRead(ctx, msg);
      }
    } finally {
      if (release) {
        ReferenceCountUtil.release(msg);
      }
    }
  }

  private void handleHttpRequest(FullHttpRequest request, ChannelHandlerContext ctx) {
    Check.input(fState != State.WEB_SOCKET);

    Check.input(request.decoderResult().isSuccess());
    Check.input(request.method().equals(HttpMethod.GET));
    Check.input(request.protocolVersion().equals(HttpVersion.HTTP_1_1));

    String upgrade = request.headers().get(HttpHeaderNames.UPGRADE);
    if ((upgrade != null) && Ascii.equalsIgnoreCase(upgrade, "websocket")) {
      Check.input(fState == State.NEW);
      fState = State.WEB_SOCKET;

      if (fUseRealIpHeader) {
        String realIp = request.headers().get("X-Real-IP");
        if (realIp != null) {
          Utils.setOriginalClientAddress(ctx.channel(), realIp);
        }
      }

      fLogger.info("Accepted {}.", Utils.channelToString(ctx.channel()));

      new WebSocketServerHandshaker13(null, "", false, MAX_FRAME_SIZE)
          .handshake(ctx.channel(), request)
          .addListener((ChannelFuture f) -> {
            if (f.isSuccess()) {
              ctx.fireUserEventTriggered(MessageHandler.ReadyEvent.INSTANCE);
            }
          })
          .addListener(Utils.CLOSE_ON_ERROR);
    } else {
      if (fState == State.NEW) {
        fState = State.HTTP;
        ctx.pipeline().addBefore(ctx.name(), "chunkedWriteHandler", new ChunkedWriteHandler());
      }

      String resourceName = request.uri().equals("/") ? "index.html" : StringUtils.removeStart(request.uri(), "/");
      ResourceDepot.Resource resource = fResourceDepot.getIfExists(resourceName);

      if (resource == null) {
        sendNotFoundResponse(ctx);
      } else if (resource.etag().equals(request.headers().get(HttpHeaderNames.IF_NONE_MATCH))) {
        sendNotModifiedResponse(resource.etag(), ctx);
      } else {
        DefaultHttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().set(HttpHeaderNames.CONTENT_ENCODING, HttpHeaderValues.GZIP);
        response.headers().set(HttpHeaderNames.CONTENT_LENGTH, resource.gzippedData().size());

        MediaType mediaType = MEDIA_TYPES.get(StringUtils.substringAfterLast(resourceName, "."));
        if (mediaType != null) {
          response.headers().set(HttpHeaderNames.CONTENT_TYPE, mediaType);
        }

        response.headers().set(HttpHeaderNames.ETAG, resource.etag());
        response.headers().set(HttpHeaderNames.CACHE_CONTROL, CACHE_CONTROL);

        ctx.write(response).addListener(Utils.CLOSE_ON_ERROR);
        ctx.write(new ChunkedStream(resource.gzippedData().newInput())).addListener(Utils.CLOSE_ON_ERROR);
        ctx.writeAndFlush(LastHttpContent.EMPTY_LAST_CONTENT).addListener(Utils.CLOSE_ON_ERROR);
      }
    }
  }

  @Override public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    if (fState == State.WEB_SOCKET) {
      fLogger.info("Closed {}.", Utils.channelToString(ctx.channel()));
    }
    super.channelInactive(ctx);
  }

  private static void sendNotFoundResponse(ChannelHandlerContext ctx) {
    DefaultFullHttpResponse r = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_FOUND);
    r.headers().set(HttpHeaderNames.CONTENT_LENGTH, 0);

    ctx.writeAndFlush(r).addListener(Utils.CLOSE_ON_ERROR);
  }

  private static void sendNotModifiedResponse(String etag, ChannelHandlerContext ctx) {
    DefaultFullHttpResponse r = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.NOT_MODIFIED);
    r.headers().set(HttpHeaderNames.ETAG, etag);
    r.headers().set(HttpHeaderNames.CACHE_CONTROL, CACHE_CONTROL);

    ctx.writeAndFlush(r).addListener(Utils.CLOSE_ON_ERROR);
  }

  private void handleBinaryFrame(BinaryWebSocketFrame frame, ChannelHandlerContext ctx) {
    try {
      ClientMessage m;
      try {
        m = ClientMessage.parseFrom(new ByteBufInputStream(frame.content()));
      } catch (InvalidProtocolBufferException e) {
        throw new BadInputException(e);
      }

      ctx.fireChannelRead(m);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void handleCloseFrame(ChannelHandlerContext ctx) {
    ctx.writeAndFlush(new CloseWebSocketFrame()).addListener(ChannelFutureListener.CLOSE);
    fCloseFrameSent = true;
  }

  private void handlePingFrame(PingWebSocketFrame frame, ChannelHandlerContext ctx) {
    PongWebSocketFrame reply = new PongWebSocketFrame(frame.content().retain());
    ctx.writeAndFlush(reply).addListener(Utils.CLOSE_ON_ERROR);
  }

  @Override public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
    if (fCloseFrameSent) {
      ReferenceCountUtil.release(msg);
      promise.setFailure(new ClosingChannelException());
    } else if (msg instanceof ServerMessage) {
      ctx.write(new BinaryWebSocketFrame(Utils.messageToDirectByteBuf((ServerMessage) msg, ctx.alloc())), promise);
    } else {
      super.write(ctx, msg, promise);
    }
  }
}
