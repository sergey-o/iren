/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.common.miscTypes.Proto.QuestionDescriptor;
import ru.irenproject.common.miscTypes.Proto.QuestionStatus;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionConnection;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;

import com.google.common.math.IntMath;
import com.google.common.primitives.Ints;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

public final class BaseWorkUtils {
  private static final int SCALED_ONE = Ints.checkedCast(scale(BigDecimal.ONE));

  public static long scale(BigDecimal value) {
    return value.scaleByPowerOfTen(LiveQuestion.SCORE_FRACTION_DIGITS).longValueExact();
  }

  public static BigDecimal unscale(long value) {
    return BigDecimal.valueOf(value, LiveQuestion.SCORE_FRACTION_DIGITS);
  }

  public static BigDecimal computeSessionResult(Session session) {
    return unscale(session.getScaledScore()).divide(BigDecimal.valueOf(session.getPerfectScore()),
        LiveQuestion.SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
  }

  public static List<QuestionDescriptor> getQuestionDescriptors(
      Session session,
      boolean revealWeight,
      boolean revealCorrectness) {
    return session.getQuestionList().stream()
        .map(q -> QuestionDescriptor.newBuilder()
            .setWeight(revealWeight ? q.getWeight() : 1)
            .setStatus(getQuestionStatus(q, revealCorrectness))
            .build())
        .collect(Collectors.toList());
  }

  public static QuestionStatus getQuestionStatus(SessionQuestion q, boolean revealCorrectness) {
    QuestionStatus res;
    if (q.hasScaledResult()) {
      res = revealCorrectness ? scaledResultToQuestionStatus(q.getScaledResult()) : QuestionStatus.ANSWERED;
    } else {
      res = QuestionStatus.UNANSWERED;
    }

    return res;
  }

  public static QuestionStatus scaledResultToQuestionStatus(int scaledResult) {
    QuestionStatus res;
    if (scaledResult == 0) {
      res = QuestionStatus.INCORRECT;
    } else if (scaledResult == SCALED_ONE) {
      res = QuestionStatus.CORRECT;
    } else {
      res = QuestionStatus.PARTIALLY_CORRECT;
    }

    return res;
  }

  public static long getScaledScore(SessionQuestion q) {
    return scale(getScore(q));
  }

  public static BigDecimal getScore(SessionQuestion q) {
    return q.hasScaledResult() ?
        unscale(q.getScaledResult()).multiply(BigDecimal.valueOf(q.getWeight())) : BigDecimal.ZERO;
  }

  public static void checkQuestionIndex(int questionIndex, Session session) {
    Check.input(questionIndex >= 0);
    Check.input(questionIndex < session.getQuestionCount());
  }

  public static boolean isTimedOut(SessionStatus status) {
    return status.hasDeadline() && (System.currentTimeMillis() > status.getDeadline());
  }

  /** Does not fill the {@link Session#getQuestionList question} list in the resulting {@code Session}. */
  public static FullSession buildSessionPrecursor(
      String userName,
      @Nullable String userDisplayName,
      @Nullable String groupName,
      SessionConnection sessionConnection,
      @Nullable Long deadline,
      List<_LiveQuestion> liveQuestions) {
    Session.Builder session = Session.newBuilder()
        .setUserName(userName)
        .setCurrentQuestionIndex(0)
        .setScaledScore(0)
        .setPerfectScore(liveQuestions.stream()
            .mapToInt(_LiveQuestion::getWeight)
            .reduce(0, IntMath::checkedAdd))
        .setAttemptedScore(0)
        .setFirstConnection(sessionConnection);

    if (userDisplayName != null) {
      session.setUserDisplayName(userDisplayName);
    }

    if (groupName != null) {
      session.setGroupName(groupName);
    }

    SessionStatus.Builder status = SessionStatus.newBuilder()
        .setBanned(false)
        .setFinished(false);

    if (deadline != null) {
      status.setDeadline(deadline);
    }

    return new FullSession(session.build(), status.build());
  }

  private BaseWorkUtils() {}
}
