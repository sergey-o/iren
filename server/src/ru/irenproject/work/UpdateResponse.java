/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.common.dialog.Proto.DialogResponse;

public final class UpdateResponse {
  public final int questionIndex;
  public final int scaledResult;
  public final int weight;
  public final long screenId;
  public final DialogResponse response;

  public UpdateResponse(int questionIndex, int scaledResult, int weight, long screenId, DialogResponse response) {
    this.questionIndex = questionIndex;
    this.scaledResult = scaledResult;
    this.weight = weight;
    this.screenId = screenId;
    this.response = response;
  }
}
