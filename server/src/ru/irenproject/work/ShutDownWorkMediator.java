/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.common.workController.Proto.ShutDownWorkResult;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class ShutDownWorkMediator extends AbstractBehavior<ShutDownWorkMediator.Message> {
  public interface Message {}

  public interface Factory {
    ShutDownWorkMediator create(
        ActorContext<Message> context,
        ByteString workId,
        WorkHub.ShutDownWork<?> request);

    static Behavior<Message> makeBehavior(
        Factory factory,
        ByteString workId,
        WorkHub.ShutDownWork<?> request) {
      return Behaviors.setup(ctx -> factory.create(ctx, workId, request));
    }
  }

  private final ActorContext<Message> fContext;
  private final WorkHub.ShutDownWork<?> fRequest;

  @Inject private ShutDownWorkMediator(
      @Assisted ActorContext<Message> context,
      @Assisted ByteString workId,
      @Assisted WorkHub.ShutDownWork<?> request,
      ActorRef<WorkCatalog.Message> workCatalog) {
    fContext = context;
    fRequest = request;

    workCatalog.tell(new WorkCatalog.Get<>(
        workId,
        WorkHubObtained::new,
        fContext.getSelf()));
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(WorkHubObtained.class, this::onWorkHubObtained)
        .onMessageEquals(WorkHubTerminated.INSTANCE, this::onWorkHubTerminated)
        .onMessage(ShutDownWorkComplete.class, this::onShutDownWorkComplete)
        .build();
  }

  private static final class WorkHubObtained implements Message {
    final @Nullable ActorRef<WorkHub.Message> workHub;

    WorkHubObtained(@Nullable ActorRef<WorkHub.Message> workHub) {
      this.workHub = workHub;
    }
  }

  private Behavior<Message> onWorkHubObtained(WorkHubObtained m) {
    Behavior<Message> res;

    if (m.workHub == null) {
      replyWith(ShutDownWorkResult.WORK_NOT_FOUND, fRequest);
      res = Behaviors.stopped();
    } else {
      fContext.watchWith(m.workHub, WorkHubTerminated.INSTANCE);

      m.workHub.tell(new WorkHub.ShutDownWork<>(
          fRequest.action,
          ShutDownWorkComplete::new,
          fContext.getSelf()));

      res = this;
    }

    return res;
  }

  private static <R> void replyWith(ShutDownWorkResult result, WorkHub.ShutDownWork<R> request) {
    request.replyTo.tell(request.replyBuilder.apply(result));
  }

  private enum WorkHubTerminated implements Message { INSTANCE }

  private Behavior<Message> onWorkHubTerminated() {
    replyWith(ShutDownWorkResult.WORK_NOT_FOUND, fRequest);
    return Behaviors.stopped();
  }

  private static final class ShutDownWorkComplete implements Message {
    final ShutDownWorkResult result;

    ShutDownWorkComplete(ShutDownWorkResult result) {
      this.result = result;
    }
  }

  private Behavior<Message> onShutDownWorkComplete(ShutDownWorkComplete m) {
    replyWith(m.result, fRequest);
    return Behaviors.stopped();
  }
}
