/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.watcher.Proto.SessionOverview;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.profile.MarkScale;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionStatus;
import ru.irenproject.work.db.DbReader;
import ru.irenproject.work.db.DbTask;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.primitives.Ints;
import com.google.protobuf.InvalidProtocolBufferException;

import java.math.BigDecimal;
import java.math.RoundingMode;

public final class WorkUtils {
  public static final int SESSION_KEY_SIZE = 32;

  private static BigDecimal computeAttemptedSessionResult(Session session) {
    return BigDecimal.valueOf(session.getAttemptedScore()).divide(BigDecimal.valueOf(session.getPerfectScore()),
        LiveQuestion.SCORE_FRACTION_DIGITS, RoundingMode.DOWN);
  }

  public static SessionOverview buildSessionOverview(long sessionId, FullSession fullSession, MarkScale markScale) {
    Session session = fullSession.data;
    SessionStatus status = fullSession.status;

    BigDecimal sessionResult = BaseWorkUtils.computeSessionResult(session);
    SessionOverview.Builder b = SessionOverview.newBuilder()
        .setId(sessionId)
        .setUserDisplayName(session.hasUserDisplayName() ? session.getUserDisplayName() : session.getUserName())
        .setScaledScore(session.getScaledScore())
        .setScaledResult(Ints.checkedCast(BaseWorkUtils.scale(sessionResult)))
        .setPerfectScore(session.getPerfectScore())
        .setScaledAttemptedResult(Ints.checkedCast(BaseWorkUtils.scale(computeAttemptedSessionResult(session))))
        .setFinished(status.getFinished())
        .setMark(markScale.getTitleForResult(sessionResult))
        .setOpenedAt(session.getFirstConnection().getOpenedAt())
        .setAddress(session.getFirstConnection().getAddress())
        .setBanned(status.getBanned());
    if (session.hasGroupName()) {
      b.setGroupName(session.getGroupName());
    }
    if (status.hasDeadline()) {
      b.setDeadlineMilliseconds(status.getDeadline());
    }
    return b.build();
  }

  public static DbTask<DbReader, /* @Nullable */Long> getSessionIdByCanonicalUserNameIfExists(
      String canonicalUserName) {
    return dbr -> {
      try {
        SQLiteStatement s = dbr.prepare("SELECT ID FROM SESSIONS WHERE CANONICAL_USER_NAME = ?1");
        try {
          s.bind(1, canonicalUserName);
          return s.step() ? s.columnLong(0) : null;
        } finally {
          s.dispose();
        }
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }

  public static DbTask<DbReader, /* @Nullable */Session> loadSessionIfExists(long sessionId) {
    return dbr -> {
      try {
        SQLiteStatement s = dbr.prepare("SELECT DATA FROM SESSIONS WHERE ID = ?1");
        try {
          s.bind(1, sessionId);
          return s.step() ? Session.parseFrom(s.columnBlob(0)) : null;
        } finally {
          s.dispose();
        }
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  public static DbTask<DbReader, Session> loadSession(long sessionId) {
    return dbr -> Check.notNull(loadSessionIfExists(sessionId).run(dbr));
  }

  public static DbTask<DbReader, RawScreen> loadRawScreen(long screenId) {
    return dbr -> {
      try {
        SQLiteStatement s = dbr.prepare("SELECT LIVE_QUESTION, RESPONSE FROM SCREENS WHERE ID = ?1");
        try {
          s.bind(1, screenId);
          Check.that(s.step());

          byte[] responseBlob = s.columnBlob(1);

          return new RawScreen(
              _LiveQuestion.parseFrom(s.columnBlob(0)),
              (responseBlob == null) ? null : DialogResponse.parseFrom(responseBlob));
        } finally {
          s.dispose();
        }
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  public static boolean isOverviewedSessionTimedOut(SessionOverview so) {
    return so.hasDeadlineMilliseconds() && (System.currentTimeMillis() > so.getDeadlineMilliseconds());
  }

  private WorkUtils() {}
}
