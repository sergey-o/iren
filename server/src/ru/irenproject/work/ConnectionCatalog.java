/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;

import javax.annotation.Nullable;
import javax.inject.Inject;

public final class ConnectionCatalog extends AbstractBehavior<ConnectionCatalog.Message> {
  public interface Message {}

  public static final class CreateConnectionHandler implements Message {
    public final Connection connection;
    public final KeyVerifier keyVerifier;
    public final @Nullable String serverAddress;
    public final ActorRef<ActorRef<ConnectionHandler.Message>> replyTo;

    public CreateConnectionHandler(Connection connection, KeyVerifier keyVerifier,
        @Nullable String serverAddress, ActorRef<ActorRef<ConnectionHandler.Message>> replyTo) {
      this.connection = connection;
      this.keyVerifier = keyVerifier;
      this.serverAddress = serverAddress;
      this.replyTo = replyTo;
    }
  }

  public enum Stop implements Message { INSTANCE }

  public interface Factory {
    ConnectionCatalog create(ActorContext<Message> context);
  }

  private final ActorContext<Message> fContext;
  private final ConnectionHandler.Factory fConnectionHandlerFactory;

  @Inject private ConnectionCatalog(
      @Assisted ActorContext<Message> context,
      ConnectionHandler.Factory connectionHandlerFactory) {
    fContext = context;
    fConnectionHandlerFactory = connectionHandlerFactory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(CreateConnectionHandler.class, this::onCreateConnectionHandler)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .build();
  }

  private Behavior<Message> onCreateConnectionHandler(CreateConnectionHandler m) {
    m.replyTo.tell(fContext.spawn(
        ConnectionHandler.Factory.makeBehavior(fConnectionHandlerFactory, m.connection, m.keyVerifier, m.serverAddress),
        Utils.makeUniqueChildActorName(String.format("%08x", m.connection.channelId()), fContext)));
    return this;
  }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }
}
