/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Oops;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.google.common.base.Ascii;
import com.google.common.collect.ImmutableList;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton public final class SqliteTuner {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("sqlite.sync")
    String sync = "full";
  }

  private static final ImmutableList<String> SYNC_VALUES = ImmutableList.of("FULL", "NORMAL");

  private final String fSync;

  @Inject private SqliteTuner(Config config) {
    fSync = Ascii.toUpperCase(config.sync);
    Check.that(SYNC_VALUES.contains(fSync), () -> Oops.format("Incorrect 'sqlite.sync' value: '%s'.", config.sync));
  }

  public void tune(SQLiteConnection connection) {
    try {
      connection.exec("PRAGMA synchronous = " + fSync);
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }
}
