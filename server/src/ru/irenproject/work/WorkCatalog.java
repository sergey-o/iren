/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.ChildFailed;
import akka.actor.typed.DeathPactException;
import akka.actor.typed.Terminated;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableList;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;

import javax.inject.Inject;
import java.util.Collections;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.function.Function;

public final class WorkCatalog extends AbstractBehavior<WorkCatalog.Message> {
  public interface Message {}

  public static final class Open implements Message {
    public final Work work;

    public Open(Work work) {
      this.work = work;
    }
  }

  public enum Stop implements Message { INSTANCE }

  public static final class Get<R> implements Message {
    public final ByteString workId;
    public final Function</* @Nullable */ActorRef<WorkHub.Message>, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public Get(ByteString workId, Function</* @Nullable */ActorRef<WorkHub.Message>, R> replyBuilder,
        ActorRef<R> replyTo) {
      this.workId = workId;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public static final class TrackWorkList implements Message {
    public final ActorRef<WorkList> tracker;

    public TrackWorkList(ActorRef<WorkList> tracker) {
      this.tracker = tracker;
    }
  }

  public static final class WorkList {
    public final ImmutableList<Work> workListSortedById;

    public WorkList(ImmutableList<Work> workListSortedById) {
      this.workListSortedById = workListSortedById;
    }
  }

  public interface Factory {
    WorkCatalog create(ActorContext<Message> context);
  }

  private final ActorContext<Message> fContext;
  private final WorkHub.Factory fWorkHubFactory;

  private boolean fStopping;

  private final TreeMap<ByteString, Work> fWorksById = new TreeMap<>(Ids::compare);
  private final HashBiMap<Work, ActorRef<WorkHub.Message>> fWorkHubs = HashBiMap.create();
  private final HashSet<ActorRef<WorkList>> fWorkListTrackers = new HashSet<>();

  @Inject private WorkCatalog(
      @Assisted ActorContext<Message> context,
      WorkHub.Factory workHubFactory) {
    fContext = context;
    fWorkHubFactory = workHubFactory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(Terminated.class, this::onTerminated)
        .onMessage(Open.class, this::onOpen)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(Get.class, m -> onGet((Get<?>) m))
        .onMessage(TrackWorkList.class, this::onTrackWorkList)
        .onMessage(WorkListTrackerTerminated.class, this::onWorkListTrackerTerminated)
        .build();
  }

  private Behavior<Message> onTerminated(Terminated signal) {
    if (signal instanceof ChildFailed) {
      throw new DeathPactException(signal.ref());
    } else {
      Work work = fWorkHubs.inverse().get(signal.ref());
      fWorkHubs.remove(work);
      fWorksById.remove(work.id());

      notifyWorkListTrackers(fWorkListTrackers);

      return (fStopping && fWorkHubs.isEmpty()) ? Behaviors.stopped() : this;
    }
  }

  private Behavior<Message> onOpen(Open m) {
    if (!fStopping) {
      ActorRef<WorkHub.Message> workHub = fContext.spawn(
          WorkHub.Factory.makeBehavior(fWorkHubFactory, m.work),
          Utils.makeUniqueChildActorName(Ids.toHexString(m.work.id()), fContext));
      fContext.watch(workHub);

      Check.that(fWorkHubs.put(m.work, workHub) == null);
      Check.that(fWorksById.put(m.work.id(), m.work) == null);

      notifyWorkListTrackers(fWorkListTrackers);
    }

    return this;
  }

  private void notifyWorkListTrackers(Iterable<ActorRef<WorkList>> trackers) {
    Utils.broadcast(new WorkList(ImmutableList.copyOf(fWorksById.values())), trackers);
  }

  private Behavior<Message> onStop() {
    Behavior<Message> res;
    if (fStopping) {
      res = this;
    } else {
      fStopping = true;

      if (fWorkHubs.isEmpty()) {
        res = Behaviors.stopped();
      } else {
        for (ActorRef<WorkHub.Message> workHub : fWorkHubs.values()) {
          workHub.tell(WorkHub.Stop.INSTANCE);
        }
        res = this;
      }
    }

    return res;
  }

  private <R> Behavior<Message> onGet(Get<R> m) {
    Work work = fWorksById.get(m.workId);
    ActorRef<WorkHub.Message> workHub = (work == null) ? null : fWorkHubs.get(work);

    m.replyTo.tell(m.replyBuilder.apply(workHub));

    return this;
  }

  private Behavior<Message> onTrackWorkList(TrackWorkList m) {
    fContext.watchWith(m.tracker, new WorkListTrackerTerminated(m.tracker));
    fWorkListTrackers.add(m.tracker);

    notifyWorkListTrackers(Collections.singleton(m.tracker));

    return this;
  }

  private static final class WorkListTrackerTerminated implements Message {
    final ActorRef<WorkList> tracker;

    WorkListTrackerTerminated(ActorRef<WorkList> tracker) {
      this.tracker = tracker;
    }
  }

  private Behavior<Message> onWorkListTrackerTerminated(WorkListTrackerTerminated m) {
    fWorkListTrackers.remove(m.tracker);
    return this;
  }
}
