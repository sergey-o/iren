/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.ProgramInfo;
import ru.irenproject.Utils;
import ru.irenproject.authentication.AuthenticationModeRegistry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Singleton public final class WorkDirectory {
  private static final Logger fLogger = LoggerFactory.getLogger(WorkDirectory.class);

  private static final String TEMP_SOURCE_FILE = "source.tmp";
  private static final String TEMP_TEST_FILE = "test.tmp";
  private static final String TEMP_PROFILE_FILE = "profile.tmp";
  private static final String WORK_FILE = "work";
  private static final String AVAILABLE_FILE = "available";
  private static final String DATABASE_FILE = "db";
  private static final String FROZEN_WORK_SOURCE_FILE = "workSource.frozen";
  private static final String BLOB_STORE_DIRECTORY = "blobStore";
  private static final String AUTHENTICATION_MODE_FILE = "authenticationMode";

  private final AuthenticationModeRegistry fAuthenticationModeRegistry;

  private final Path fWorkDir;

  private final Gson fGson = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create();

  @Inject private WorkDirectory(
      AuthenticationModeRegistry authenticationModeRegistry,
      DataDirectory dataDirectory) {
    try {
      fAuthenticationModeRegistry = authenticationModeRegistry;

      fWorkDir = dataDirectory.get().resolve("works-" + ProgramInfo.VERSION);
      Files.createDirectories(fWorkDir);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public List<Work> listWorksDeletingUnavailable() {
    try {
      ArrayList<Work> res = new ArrayList<>();

      for (Path dir : Utils.listDirectory(fWorkDir)) {
        if (Files.isDirectory(dir)) {
          ByteString workId = Ids.fromHexString(dir.getFileName().toString());

          if (Files.exists(dir.resolve(AVAILABLE_FILE))) {
            WorkFile f = Check.notNull(fGson.fromJson(Files.readString(dir.resolve(WORK_FILE)), WorkFile.class));
            Check.notNull(f.title);
            Check.notNull(f.language);
            Check.notNull(f.startedAt);

            Message authenticationMode;
            try (BufferedReader reader = Files.newBufferedReader(dir.resolve(AUTHENTICATION_MODE_FILE))) {
              authenticationMode = fAuthenticationModeRegistry.load(reader);
            }

            res.add(new Work(workId, f.title, f.language, Instant.parse(f.startedAt), authenticationMode));
          } else {
            tryDelete(workId);
          }
        }
      }

      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class WorkFile {
    String title;
    String language;
    String startedAt;
  }

  public void create(ByteString workId) {
    try {
      Files.createDirectories(get(workId));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Path get(ByteString workId) {
    return fWorkDir.resolve(Ids.toHexString(workId));
  }

  public Path getTempSourceFile(ByteString workId) {
    return get(workId).resolve(TEMP_SOURCE_FILE);
  }

  public Path getTempTestFile(ByteString workId) {
    return get(workId).resolve(TEMP_TEST_FILE);
  }

  public Path getTempProfileFile(ByteString workId) {
    return get(workId).resolve(TEMP_PROFILE_FILE);
  }

  public Path getBlobStoreDirectory(ByteString workId) {
    return get(workId).resolve(BLOB_STORE_DIRECTORY);
  }

  public Path getDatabaseFile(ByteString workId) {
    return get(workId).resolve(DATABASE_FILE);
  }

  public Path getFrozenWorkSourceFile(ByteString workId) {
    return get(workId).resolve(FROZEN_WORK_SOURCE_FILE);
  }

  public void delete(ByteString workId) {
    try {
      if (fLogger.isTraceEnabled()) {
        fLogger.trace("Deleting directory for work {}.", Ids.toHexString(workId));
      }

      Files.deleteIfExists(get(workId).resolve(AVAILABLE_FILE));

      Utils.deleteShallowDirectory(getBlobStoreDirectory(workId));
      Utils.deleteShallowDirectory(get(workId));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void tryDelete(ByteString workId) {
    try {
      delete(workId);
    } catch (RuntimeException e) {
      fLogger.warn("", e);
    }
  }

  public void writeAndMarkAvailable(Work work) {
    try {
      Path dir = get(work.id());

      WorkFile f = new WorkFile();
      f.title = work.title();
      f.language = work.language();
      f.startedAt = work.startedAt().toString();
      Files.write(dir.resolve(WORK_FILE), fGson.toJson(f).getBytes(StandardCharsets.UTF_8));

      try (BufferedWriter writer = Files.newBufferedWriter(dir.resolve(AUTHENTICATION_MODE_FILE))) {
        fAuthenticationModeRegistry.save(work.authenticationMode(), writer);
      }

      Files.createFile(dir.resolve(AVAILABLE_FILE));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
