/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.watcher.Proto.QuestionDelta;
import ru.irenproject.common.watcher.Proto.SessionDelta;
import ru.irenproject.common.workController.Proto.ShutDownWorkResult;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;
import ru.irenproject.work.db.DbReader;
import ru.irenproject.work.db.DbTask;
import ru.irenproject.work.db.DbWriter;
import ru.irenproject.work.db.WorkStore;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.SetMultimap;
import com.google.common.math.LongMath;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.function.UnaryOperator;

public final class WorkHub extends AbstractBehavior<WorkHub.Message> {
  public interface Message {}

  public static final class RegisterParticipant<T> implements Message {
    public final @Nullable Long sessionId;
    public final ActorRef<T> participant;
    public final T stopMessage;
    public final Function<LiveWork, T> ackBuilder;

    public RegisterParticipant(@Nullable Long sessionId, ActorRef<T> participant, T stopMessage,
        Function<LiveWork, T> ackBuilder) {
      this.sessionId = sessionId;
      this.participant = participant;
      this.stopMessage = stopMessage;
      this.ackBuilder = ackBuilder;
    }
  }

  public enum Stop implements Message { INSTANCE }

  public static final class CreateSessionIfAbsent<R> implements Message {
    public final String canonicalUserName;
    public final Session sessionWithoutQuestions;
    public final ImmutableList<_LiveQuestion> liveQuestions;
    public final SessionStatus status;
    public final ByteString sessionKey;
    public final Function<CreateSessionIfAbsentResult, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public CreateSessionIfAbsent(String canonicalUserName, Session sessionWithoutQuestions,
        ImmutableList<_LiveQuestion> liveQuestions, SessionStatus status, ByteString sessionKey,
        Function<CreateSessionIfAbsentResult, R> replyBuilder, ActorRef<R> replyTo) {
      this.canonicalUserName = canonicalUserName;
      this.sessionWithoutQuestions = sessionWithoutQuestions;
      this.liveQuestions = liveQuestions;
      this.status = status;
      this.sessionKey = sessionKey;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public static final class CreateSessionIfAbsentResult {
    public final long newOrExistingSessionId;
    public final @Nullable FullSession newSession;

    public CreateSessionIfAbsentResult(long newOrExistingSessionId, @Nullable FullSession newSession) {
      this.newOrExistingSessionId = newOrExistingSessionId;
      this.newSession = newSession;
    }
  }

  public static final class UpdateSession<R> implements Message {
    public final long sessionId;
    public final Session session;
    public final @Nullable UpdateResponse updateResponse;
    public final boolean finish;
    public final R reply;
    public final ActorRef<R> replyTo;

    public UpdateSession(long sessionId, Session session, @Nullable UpdateResponse updateResponse, boolean finish,
        R reply, ActorRef<R> replyTo) {
      this.sessionId = sessionId;
      this.session = session;
      this.updateResponse = updateResponse;
      this.finish = finish;
      this.reply = reply;
      this.replyTo = replyTo;
    }
  }

  public static final class SetSessionKey<R> implements Message {
    public final long sessionId;
    public final ByteString key;
    public final boolean overwriteExisting;
    public final Function<Boolean, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public SetSessionKey(long sessionId, ByteString key, boolean overwriteExisting, Function<Boolean, R> replyBuilder,
        ActorRef<R> replyTo) {
      this.sessionId = sessionId;
      this.key = key;
      this.overwriteExisting = overwriteExisting;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public static final class TrackSessionStatus implements Message {
    public final long sessionId;
    public final ActorRef<SessionStatusDelta> tracker;

    public TrackSessionStatus(long sessionId, ActorRef<SessionStatusDelta> tracker) {
      this.sessionId = sessionId;
      this.tracker = tracker;
    }
  }

  public static final class SessionStatusDelta {
    public final SessionStatus status;

    public SessionStatusDelta(SessionStatus status) {
      this.status = status;
    }
  }

  public static final class TrackSessionList implements Message {
    public final ActorRef<SessionListDelta> tracker;

    public TrackSessionList(ActorRef<SessionListDelta> tracker) {
      this.tracker = tracker;
    }
  }

  public static final class SessionListDelta {
    public final ImmutableMap<Long, FullSession> sessionsById;

    public SessionListDelta(ImmutableMap<Long, FullSession> sessionsById) {
      this.sessionsById = sessionsById;
    }
  }

  public static final class TrackSession implements Message {
    public final long sessionId;
    public final ActorRef<SessionDelta> tracker;

    public TrackSession(long sessionId, ActorRef<SessionDelta> tracker) {
      this.sessionId = sessionId;
      this.tracker = tracker;
    }
  }

  public static final class TrackQuestion implements Message {
    public final long sessionId;
    public final int questionIndex;
    public final DialogResponse emptyResponse;
    public final ActorRef<QuestionDelta> tracker;

    public TrackQuestion(long sessionId, int questionIndex, DialogResponse emptyResponse,
        ActorRef<QuestionDelta> tracker) {
      this.sessionId = sessionId;
      this.questionIndex = questionIndex;
      this.emptyResponse = emptyResponse;
      this.tracker = tracker;
    }
  }

  public static final class ShutDownWork<R> implements Message {
    public final ShutDownWorkAction action;
    public final Function<ShutDownWorkResult, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public ShutDownWork(ShutDownWorkAction action, Function<ShutDownWorkResult, R> replyBuilder, ActorRef<R> replyTo) {
      this.action = action;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public enum ShutDownWorkAction { DELETE, SEND_TO_ARCHIVE }

  public static final class UpdateSessionStatus<R> implements Message {
    public final ImmutableSet<Long> sessionIds;
    public final UnaryOperator<SessionStatus> updater;
    public final R reply;
    public final ActorRef<R> replyTo;

    public UpdateSessionStatus(ImmutableSet<Long> sessionIds, UnaryOperator<SessionStatus> updater, R reply,
        ActorRef<R> replyTo) {
      this.sessionIds = sessionIds;
      this.updater = updater;
      this.reply = reply;
      this.replyTo = replyTo;
    }
  }

  public static final class Detach<R> implements Message {
    public final ImmutableSet<Long> sessionIds;
    public final R reply;
    public final ActorRef<R> replyTo;

    public Detach(ImmutableSet<Long> sessionIds, R reply, ActorRef<R> replyTo) {
      this.sessionIds = sessionIds;
      this.reply = reply;
      this.replyTo = replyTo;
    }
  }

  public interface Factory {
    WorkHub create(
        ActorContext<Message> context,
        Work work);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Work work) {
      return Behaviors.setup(ctx -> factory.create(ctx, work));
    }
  }

  private final ActorContext<Message> fContext;
  private final WorkDirectory fWorkDirectory;
  private final ActorRef<WorkArchiver.Message> fWorkArchiver;

  private final Store fStore;
  private final WorkSourcePool fWorkSourcePool;
  private final LiveWork fLiveWork;

  private boolean fClosing;
  private boolean fForceStop;
  private @Nullable ShutDownWork<?> fScheduledShutDownWork;
  private boolean fShutDownWorkActionInvoked;
  private @Nullable ShutDownWorkResult fShutDownWorkResult;
  private boolean fDeleteWorkDirectory;

  private final HashSet<Participant<?>> fGlobalParticipants = new HashSet<>();
  private final HashMap<Long, Participant<?>> fParticipantsBySessionId = new HashMap<>();
  private final HashMap<Long, RegisterParticipant<?>> fRegisterRequestsBySessionId = new HashMap<>();

  private final HashSet<ActorRef<SessionListDelta>> fSessionListTrackers = new HashSet<>();

  private final SetMultimap<Long, ActorRef<SessionDelta>> fSessionTrackersBySessionId = MultimapBuilder
      .hashKeys().hashSetValues().build();

  private final SetMultimap<Long, ActorRef<SessionStatusDelta>> fSessionStatusTrackersBySessionId = MultimapBuilder
      .hashKeys().hashSetValues().build();

  private final SetMultimap<Long, ActorRef<QuestionDelta>> fQuestionTrackersByScreenId = MultimapBuilder
      .hashKeys().hashSetValues().build();

  @Inject private WorkHub(
      @Assisted ActorContext<Message> context,
      @Assisted Work work,
      WorkDirectory workDirectory,
      WorkStore workStore,
      WorkSourcePool.Factory workSourcePoolFactory,
      ActorRef<WorkArchiver.Message> workArchiver) {
    fContext = context;
    fWorkDirectory = workDirectory;
    fWorkArchiver = workArchiver;

    fStore = new Store(workStore, work);
    fWorkSourcePool = workSourcePoolFactory.create(work);

    fLiveWork = new LiveWork(work, fStore, fWorkSourcePool::acquire);
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(PostStop.class, ignored -> onPostStop())
        .onMessage(RegisterParticipant.class, m -> onRegisterParticipant((RegisterParticipant<?>) m))
        .onMessage(ParticipantTerminated.class, this::onParticipantTerminated)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(ShutDownWork.class, this::onShutDownWork)
        .onMessage(ShutDownWorkActionComplete.class, this::onShutDownWorkActionComplete)
        .onMessage(CreateSessionIfAbsent.class, m -> onCreateSessionIfAbsent((CreateSessionIfAbsent<?>) m))
        .onMessage(UpdateSession.class, m -> onUpdateSession((UpdateSession<?>) m))
        .onMessage(SetSessionKey.class, m -> onSetSessionKey((SetSessionKey<?>) m))
        .onMessage(TrackSessionStatus.class, this::onTrackSessionStatus)
        .onMessage(SessionStatusTrackerTerminated.class, this::onSessionStatusTrackerTerminated)
        .onMessage(TrackSessionList.class, this::onTrackSessionList)
        .onMessage(SessionListTrackerTerminated.class, this::onSessionListTrackerTerminated)
        .onMessage(TrackSession.class, this::onTrackSession)
        .onMessage(SessionTrackerTerminated.class, this::onSessionTrackerTerminated)
        .onMessage(TrackQuestion.class, this::onTrackQuestion)
        .onMessage(QuestionTrackerTerminated.class, this::onQuestionTrackerTerminated)
        .onMessage(UpdateSessionStatus.class, m -> onUpdateSessionStatus((UpdateSessionStatus<?>) m))
        .onMessage(Detach.class, m -> onDetach((Detach<?>) m))
        .build();
  }

  private Behavior<Message> onPostStop() {
    fStore.close();
    fWorkSourcePool.close();

    if (fDeleteWorkDirectory) {
      fWorkDirectory.tryDelete(fLiveWork.work().id());
    }

    return this;
  }

  private static final class Store implements LiveWork.Store {
    final WorkStore fWorkStore;
    final Work fWork;

    Store(WorkStore workStore, Work work) {
      fWorkStore = workStore;
      fWork = work;

      fWorkStore.open(fWork);
    }

    void close() {
      fWorkStore.close(fWork);
    }

    @Override public <R> R read(DbTask<DbReader, R> task) {
      return fWorkStore.read(fWork, task);
    }

    <R> R write(DbTask<DbWriter, R> task) {
      return fWorkStore.write(fWork, task);
    }

    @Override public <R> R readDirectly(DbTask<DbReader, R> task) {
      return fWorkStore.readDirectly(fWork, task);
    }
  }

  private <T> Behavior<Message> onRegisterParticipant(RegisterParticipant<T> m) {
    if (fClosing) {
      failRegisterRequest(m);
    } else {
      if (m.sessionId == null) {
        registerNow(m);
      } else {
        Participant<?> existing = fParticipantsBySessionId.get(m.sessionId);
        if (existing == null) {
          registerNow(m);
        } else {
          existing.stop();
          RegisterParticipant<?> oldRegisterRequest = fRegisterRequestsBySessionId.put(m.sessionId, m);
          if (oldRegisterRequest != null) {
            failRegisterRequest(oldRegisterRequest);
          }
        }
      }
    }

    return this;
  }

  private static <T> void failRegisterRequest(RegisterParticipant<T> m) {
    m.participant.tell(m.stopMessage);
  }

  private <T> void registerNow(RegisterParticipant<T> m) {
    Participant<T> participant = new Participant<>(m.sessionId, m.participant, m.stopMessage);

    fContext.watchWith(m.participant, new ParticipantTerminated(participant));

    if (m.sessionId == null) {
      fGlobalParticipants.add(participant);
    } else {
      fParticipantsBySessionId.put(m.sessionId, participant);
    }

    m.participant.tell(m.ackBuilder.apply(fLiveWork));
  }

  private static final class Participant<T> {
    final @Nullable Long fSessionId;
    final ActorRef<T> fActorRef;
    final T fStopMessage;

    Participant(@Nullable Long sessionId, ActorRef<T> actorRef, T stopMessage) {
      fSessionId = sessionId;
      fActorRef = actorRef;
      fStopMessage = stopMessage;
    }

    void stop() {
      fActorRef.tell(fStopMessage);
    }
  }

  private static final class ParticipantTerminated implements Message {
    final Participant<?> participant;

    ParticipantTerminated(Participant<?> participant) {
      this.participant = participant;
    }
  }

  private Behavior<Message> onParticipantTerminated(ParticipantTerminated m) {
    if (m.participant.fSessionId == null) {
      fGlobalParticipants.remove(m.participant);
    } else {
      fParticipantsBySessionId.remove(m.participant.fSessionId);

      RegisterParticipant<?> registerRequest = fRegisterRequestsBySessionId.remove(m.participant.fSessionId);
      if (registerRequest != null) {
        registerNow(registerRequest);
      }
    }

    return checkForStop();
  }

  private Behavior<Message> onStop() {
    fForceStop = true;
    initiateCloseIfNotAlready();
    return checkForStop();
  }

  private void initiateCloseIfNotAlready() {
    if (!fClosing) {
      fClosing = true;

      fRegisterRequestsBySessionId.values().forEach(WorkHub::failRegisterRequest);
      fRegisterRequestsBySessionId.clear();

      fGlobalParticipants.forEach(Participant::stop);
      fParticipantsBySessionId.values().forEach(Participant::stop);
    }
  }

  private Behavior<Message> checkForStop() {
    Behavior<Message> res;

    if (fClosing && fGlobalParticipants.isEmpty() && fParticipantsBySessionId.isEmpty()) {
      if (fScheduledShutDownWork == null) {
        res = Behaviors.stopped();
      } else if (!fShutDownWorkActionInvoked) {
        fShutDownWorkActionInvoked = true;
        performShutDownWorkAction(fScheduledShutDownWork.action);
        res = this;
      } else if (fShutDownWorkResult == null) {
        res = this;
      } else {
        replyToShutDownWork(fShutDownWorkResult, fScheduledShutDownWork);
        if ((fShutDownWorkResult == ShutDownWorkResult.OK) || fForceStop) {
          fDeleteWorkDirectory = (fShutDownWorkResult == ShutDownWorkResult.OK);
          res = Behaviors.stopped();
        } else {
          cancelClose();
          res = this;
        }
      }
    } else {
      res = this;
    }

    return res;
  }

  private void cancelClose() {
    Check.that(!fForceStop);
    Check.that(!fDeleteWorkDirectory);
    fClosing = false;
    fScheduledShutDownWork = null;
    fShutDownWorkActionInvoked = false;
    fShutDownWorkResult = null;
  }

  private Behavior<Message> onShutDownWork(ShutDownWork<?> m) {
    Behavior<Message> res;
    if (fClosing) {
      replyToShutDownWork(ShutDownWorkResult.WORK_NOT_FOUND, m);
      res = this;
    } else {
      fScheduledShutDownWork = m;
      initiateCloseIfNotAlready();
      res = checkForStop();
    }

    return res;
  }

  private static <R> void replyToShutDownWork(ShutDownWorkResult result, ShutDownWork<R> request) {
    request.replyTo.tell(request.replyBuilder.apply(result));
  }

  private void performShutDownWorkAction(ShutDownWorkAction action) {
    switch (action) {
      case DELETE: {
        fContext.getSelf().tell(new ShutDownWorkActionComplete(ShutDownWorkResult.OK));
        break;
      }
      case SEND_TO_ARCHIVE: {
        fWorkArchiver.tell(new WorkArchiver.PutIntoArchive<>(
            fLiveWork,
            ShutDownWorkActionComplete::new,
            fContext.getSelf()));
        break;
      }
    }
  }

  private static final class ShutDownWorkActionComplete implements Message {
    final ShutDownWorkResult result;

    ShutDownWorkActionComplete(ShutDownWorkResult result) {
      this.result = result;
    }
  }

  private Behavior<Message> onShutDownWorkActionComplete(ShutDownWorkActionComplete m) {
    fShutDownWorkResult = m.result;
    return checkForStop();
  }

  private <R> Behavior<Message> onCreateSessionIfAbsent(CreateSessionIfAbsent<R> m) {
    CreateSessionIfAbsentResult result = fStore.write(createSessionIfAbsent(
        m.canonicalUserName, m.sessionWithoutQuestions, m.liveQuestions, m.status, m.sessionKey));

    if (result.newSession != null) {
      Utils.broadcast(
          new SessionListDelta(ImmutableMap.of(result.newOrExistingSessionId, result.newSession)),
          fSessionListTrackers);
    }

    m.replyTo.tell(m.replyBuilder.apply(result));

    return this;
  }

  private static DbTask<DbWriter, CreateSessionIfAbsentResult> createSessionIfAbsent(
      String canonicalUserName,
      Session sessionWithoutQuestions,
      ImmutableList<_LiveQuestion> liveQuestions,
      SessionStatus status,
      ByteString sessionKey) {
    return dbw -> {
      try {
        CreateSessionIfAbsentResult res;
        Long existingSessionId = WorkUtils.getSessionIdByCanonicalUserNameIfExists(canonicalUserName).run(dbw);
        if (existingSessionId == null) {
          Session.Builder sessionBuilder = sessionWithoutQuestions.toBuilder();
          SQLiteStatement s;

          s = dbw.prepare("INSERT INTO SCREENS (LIVE_QUESTION) VALUES (?1)");
          try {
            for (_LiveQuestion lq : liveQuestions) {
              s.reset();
              s.bind(1, lq.toByteArray());
              s.step();
              sessionBuilder.addQuestionBuilder()
                  .setScreenId(dbw.getLastInsertId())
                  .setWeight(lq.getWeight());
            }
          } finally {
            s.dispose();
          }

          Session createdSession = sessionBuilder.build();

          s = dbw.prepare("INSERT INTO SESSIONS (DATA, STATUS, CANONICAL_USER_NAME, KEY) VALUES (?1, ?2, ?3, ?4)");
          try {
            s.bind(1, createdSession.toByteArray());
            s.bind(2, status.toByteArray());
            s.bind(3, canonicalUserName);
            s.bind(4, sessionKey.toByteArray());
            s.step();
            res = new CreateSessionIfAbsentResult(dbw.getLastInsertId(), new FullSession(createdSession, status));
          } finally {
            s.dispose();
          }
        } else {
          res = new CreateSessionIfAbsentResult(existingSessionId, null);
        }

        return res;
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private <R> Behavior<Message> onUpdateSession(UpdateSession<R> m) {
    SessionStatus status = fStore.read(loadSessionStatus(m.sessionId));

    if (m.finish) {
      status = status.toBuilder()
          .setFinished(true)
          .buildPartial();
    }

    fStore.write(updateSession(m.sessionId, m.session, m.finish ? status : null, m.updateResponse));

    Utils.broadcast(
        new SessionListDelta(ImmutableMap.of(m.sessionId, new FullSession(m.session, status))),
        fSessionListTrackers);

    if (m.finish) {
      Utils.broadcast(
          new SessionStatusDelta(status),
          fSessionStatusTrackersBySessionId.get(m.sessionId));
    }

    if (m.updateResponse != null) {
      Utils.broadcast(SessionDelta.newBuilder()
          .putQuestionStatus(
              m.updateResponse.questionIndex,
              BaseWorkUtils.scaledResultToQuestionStatus(m.updateResponse.scaledResult))
          .build(), fSessionTrackersBySessionId.get(m.sessionId));

      Utils.broadcast(
          toQuestionDelta(m.updateResponse.response, m.updateResponse.scaledResult, m.updateResponse.weight),
          fQuestionTrackersByScreenId.get(m.updateResponse.screenId));
    }

    m.replyTo.tell(m.reply);

    return this;
  }

  private static DbTask<DbReader, SessionStatus> loadSessionStatus(long sessionId) {
    return dbr -> {
      try {
        SQLiteStatement s = dbr.prepare("SELECT STATUS FROM SESSIONS WHERE ID = ?1");
        try {
          s.bind(1, sessionId);
          Check.that(s.step());
          return SessionStatus.parseFrom(s.columnBlob(0));
        } finally {
          s.dispose();
        }
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private static DbTask<DbWriter, Void> updateSession(
      long sessionId,
      Session session,
      @Nullable SessionStatus sessionStatus,
      @Nullable UpdateResponse updateResponse) {
    return dbw -> {
      try {
        SQLiteStatement s;

        s = dbw.prepare(String.format("UPDATE SESSIONS SET DATA = ?1%s WHERE ID = ?2",
            (sessionStatus == null) ? "" : ", STATUS = ?3"));
        try {
          s.bind(1, session.toByteArray());
          s.bind(2, sessionId);
          if (sessionStatus != null) {
            s.bind(3, sessionStatus.toByteArray());
          }
          s.step();
          Check.that(dbw.getChanges() == 1);
        } finally {
          s.dispose();
        }

        if (updateResponse != null) {
          s = dbw.prepare("UPDATE SCREENS SET RESPONSE = ?1 WHERE ID = ?2");
          try {
            s.bind(1, updateResponse.response.toByteArray());
            s.bind(2, updateResponse.screenId);
            s.step();
            Check.that(dbw.getChanges() == 1);
          } finally {
            s.dispose();
          }
        }

        return null;
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private static QuestionDelta toQuestionDelta(DialogResponse response, int scaledResult, int weight) {
    return QuestionDelta.newBuilder()
        .setResponse(response)
        .setScaledResult(scaledResult)
        .setScaledScore(LongMath.checkedMultiply(scaledResult, weight))
        .build();
  }

  private <R> Behavior<Message> onSetSessionKey(SetSessionKey<R> m) {
    Boolean keyHasBeenSet = fStore.write(setSessionKey(m.sessionId, m.key, m.overwriteExisting));

    m.replyTo.tell(m.replyBuilder.apply(keyHasBeenSet));

    return this;
  }

  private static DbTask<DbWriter, Boolean> setSessionKey(long sessionId, ByteString key, boolean overwriteExisting) {
    return dbw -> {
      try {
        SQLiteStatement s = dbw.prepare(String.format("UPDATE SESSIONS SET KEY = ?1 WHERE ID = ?2%s",
            overwriteExisting ? "" : " AND KEY IS NULL"));
        try {
          s.bind(1, key.toByteArray());
          s.bind(2, sessionId);
          s.step();
          return dbw.getChanges() == 1;
        } finally {
          s.dispose();
        }
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private Behavior<Message> onTrackSessionStatus(TrackSessionStatus m) {
    SessionStatus status = fStore.read(loadSessionStatus(m.sessionId));

    fContext.watchWith(m.tracker, new SessionStatusTrackerTerminated(m.sessionId, m.tracker));
    fSessionStatusTrackersBySessionId.put(m.sessionId, m.tracker);

    m.tracker.tell(new SessionStatusDelta(status));

    return this;
  }

  private static final class SessionStatusTrackerTerminated implements Message {
    final long sessionId;
    final ActorRef<SessionStatusDelta> tracker;

    SessionStatusTrackerTerminated(long sessionId, ActorRef<SessionStatusDelta> tracker) {
      this.sessionId = sessionId;
      this.tracker = tracker;
    }
  }

  private Behavior<Message> onSessionStatusTrackerTerminated(SessionStatusTrackerTerminated m) {
    fSessionStatusTrackersBySessionId.remove(m.sessionId, m.tracker);
    return this;
  }

  private Behavior<Message> onTrackSessionList(TrackSessionList m) {
    fContext.watchWith(m.tracker, new SessionListTrackerTerminated(m.tracker));
    fSessionListTrackers.add(m.tracker);

    m.tracker.tell(new SessionListDelta(fStore.read(allSessionsById())));

    return this;
  }

  private static DbTask<DbReader, ImmutableMap<Long, FullSession>> allSessionsById() {
    return dbr -> {
      try {
        ImmutableMap.Builder<Long, FullSession> b = ImmutableMap.builder();

        SQLiteStatement s = dbr.prepare("SELECT ID, DATA, STATUS FROM SESSIONS");
        try {
          while (s.step()) {
            b.put(s.columnLong(0), new FullSession(
                Session.parseFrom(s.columnBlob(1)),
                SessionStatus.parseFrom(s.columnBlob(2))));
          }
        } finally {
          s.dispose();
        }

        return b.build();
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private static final class SessionListTrackerTerminated implements Message {
    final ActorRef<SessionListDelta> tracker;

    SessionListTrackerTerminated(ActorRef<SessionListDelta> tracker) {
      this.tracker = tracker;
    }
  }

  private Behavior<Message> onSessionListTrackerTerminated(SessionListTrackerTerminated m) {
    fSessionListTrackers.remove(m.tracker);
    return this;
  }

  private Behavior<Message> onTrackSession(TrackSession m) {
    Session session = fStore.read(WorkUtils.loadSession(m.sessionId));
    int questionCount = session.getQuestionCount();

    SessionDelta.Builder b = SessionDelta.newBuilder();
    for (int i = 0; i < questionCount; ++i) {
      b.putQuestionStatus(i, BaseWorkUtils.getQuestionStatus(session.getQuestion(i), true));
    }

    fContext.watchWith(m.tracker, new SessionTrackerTerminated(m.sessionId, m.tracker));
    fSessionTrackersBySessionId.put(m.sessionId, m.tracker);

    m.tracker.tell(b.build());

    return this;
  }

  private static final class SessionTrackerTerminated implements Message {
    final long sessionId;
    final ActorRef<SessionDelta> tracker;

    SessionTrackerTerminated(long sessionId, ActorRef<SessionDelta> tracker) {
      this.sessionId = sessionId;
      this.tracker = tracker;
    }
  }

  private Behavior<Message> onSessionTrackerTerminated(SessionTrackerTerminated m) {
    fSessionTrackersBySessionId.remove(m.sessionId, m.tracker);
    return this;
  }

  private Behavior<Message> onTrackQuestion(TrackQuestion m) {
    Session session = fStore.read(WorkUtils.loadSession(m.sessionId));
    SessionQuestion sq = session.getQuestion(m.questionIndex);

    DialogResponse response = fStore.read(WorkUtils.loadRawScreen(sq.getScreenId())).response;

    fContext.watchWith(m.tracker, new QuestionTrackerTerminated(sq.getScreenId(), m.tracker));
    fQuestionTrackersByScreenId.put(sq.getScreenId(), m.tracker);

    m.tracker.tell(toQuestionDelta(
        (response == null) ? m.emptyResponse : response,
        sq.hasScaledResult() ? sq.getScaledResult() : 0,
        sq.getWeight()));

    return this;
  }

  private static final class QuestionTrackerTerminated implements Message {
    final long screenId;
    final ActorRef<QuestionDelta> tracker;

    QuestionTrackerTerminated(long screenId, ActorRef<QuestionDelta> tracker) {
      this.screenId = screenId;
      this.tracker = tracker;
    }
  }

  private Behavior<Message> onQuestionTrackerTerminated(QuestionTrackerTerminated m) {
    fQuestionTrackersByScreenId.remove(m.screenId, m.tracker);
    return this;
  }

  private <R> Behavior<Message> onUpdateSessionStatus(UpdateSessionStatus<R> m) {
    ImmutableMap<Long, FullSession> sessionsById = fStore.write(updateSessionStatus(m.sessionIds, m.updater));

    Utils.broadcast(
        new SessionListDelta(sessionsById),
        fSessionListTrackers);

    for (Map.Entry<Long, FullSession> e : sessionsById.entrySet()) {
      Utils.broadcast(
          new SessionStatusDelta(e.getValue().status),
          fSessionStatusTrackersBySessionId.get(e.getKey()));
    }

    m.replyTo.tell(m.reply);

    return this;
  }

  private static DbTask<DbWriter, ImmutableMap<Long, FullSession>> updateSessionStatus(
      ImmutableSet<Long> sessionIds, UnaryOperator<SessionStatus> updater) {
    return dbw -> {
      try {
        ImmutableMap.Builder<Long, FullSession> b = ImmutableMap.builder();

        SQLiteStatement readQuery = dbw.prepare("SELECT DATA, STATUS FROM SESSIONS WHERE ID = ?1");
        try {
          SQLiteStatement updateQuery = dbw.prepare("UPDATE SESSIONS SET STATUS = ?1 WHERE ID = ?2");
          try {
            for (Long sessionId : sessionIds) {
              Check.that(readQuery
                  .reset()
                  .bind(1, sessionId)
                  .step());

              Session session = Session.parseFrom(readQuery.columnBlob(0));
              SessionStatus newStatus = updater.apply(SessionStatus.parseFrom(readQuery.columnBlob(1)));

              updateQuery
                  .reset()
                  .bind(1, newStatus.toByteArray())
                  .bind(2, sessionId)
                  .step();

              b.put(sessionId, new FullSession(session, newStatus));
            }
          } finally {
            updateQuery.dispose();
          }
        } finally {
          readQuery.dispose();
        }

        return b.build();
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private <R> Behavior<Message> onDetach(Detach<R> m) {
    fStore.write(nullifySessionKey(m.sessionIds));

    for (Long sessionId : m.sessionIds) {
      Participant<?> participant = fParticipantsBySessionId.get(sessionId);
      if (participant != null) {
        participant.stop();
      }
    }

    m.replyTo.tell(m.reply);

    return this;
  }

  private static DbTask<DbWriter, Void> nullifySessionKey(ImmutableSet<Long> sessionIds) {
    return dbw -> {
      try {
        SQLiteStatement s = dbw.prepare("UPDATE SESSIONS SET KEY = NULL WHERE ID = ?1");
        try {
          for (Long sessionId : sessionIds) {
            s.reset();
            s.bind(1, sessionId);
            s.step();
            Check.that(dbw.getChanges() == 1);
          }
        } finally {
          s.dispose();
        }

        return null;
      } catch (SQLiteException e) {
        throw new RuntimeException(e);
      }
    };
  }
}
