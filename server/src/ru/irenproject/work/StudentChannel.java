/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.RequestContext;
import ru.irenproject.authentication.AuthenticationUtils;
import ru.irenproject.common.student.Proto.ChooseQuestion;
import ru.irenproject.common.student.Proto.QueryQuestionDetails;
import ru.irenproject.common.student.Proto.SetResponse;
import ru.irenproject.common.student.Proto.StudentRequest;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.db.DbReader;
import ru.irenproject.work.db.DbTask;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.collect.ImmutableList;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

public final class StudentChannel extends AbstractBehavior<StudentChannel.Message> {
  public interface Message {}

  public enum Activate implements Message { INSTANCE }

  public static final class RequestMessage implements Message {
    public final StudentRequest request;

    public RequestMessage(StudentRequest request, @SuppressWarnings("unused") RequestContext requestContext) {
      this.request = request;
    }
  }

  public interface Factory {
    StudentChannel create(
        ActorContext<Message> context,
        Connection connection,
        @Assisted("noticeChannelId") long noticeChannelId,
        @Assisted("sessionId") long sessionId,
        ActorRef<WorkHub.Message> workHub);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        long noticeChannelId,
        long sessionId,
        ActorRef<WorkHub.Message> workHub) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, noticeChannelId, sessionId, workHub));
    }
  }

  private static final Logger fLogger = LoggerFactory.getLogger(StudentChannel.class);

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final long fNoticeChannelId;
  private final long fSessionId;
  private final ActorRef<WorkHub.Message> fWorkHub;
  private final SessionHandlerFactory fSessionHandlerFactory;

  private SessionHandler fHandler;
  private String fUserDescription;

  @Inject private StudentChannel(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted("noticeChannelId") long noticeChannelId,
      @Assisted("sessionId") long sessionId,
      @Assisted ActorRef<WorkHub.Message> workHub,
      SessionHandlerFactory sessionHandlerFactory) {
    fContext = context;
    fConnection = connection;
    fNoticeChannelId = noticeChannelId;
    fSessionId = sessionId;
    fWorkHub = workHub;
    fSessionHandlerFactory = sessionHandlerFactory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(PostStop.class, ignored -> onPostStop())
        .onMessageEquals(Activate.INSTANCE, this::onActivate)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .onMessage(Registered.class, this::onRegistered)
        .onMessage(SessionStatusDelta.class, m -> onSessionStatusDelta(m.original))
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .onMessageEquals(ContinueSetResponse.INSTANCE, this::onContinueSetResponse)
        .onMessage(ContinueSubmit.class, m -> onContinueSubmit(m.original))
        .onMessageEquals(ContinueChooseQuestion.INSTANCE, this::onContinueChooseQuestion)
        .onMessageEquals(ContinueFinishWork.INSTANCE, this::onContinueFinishWork)
        .build();
  }

  private Behavior<Message> onPostStop() {
    if (fUserDescription != null) {
      fLogger.info("Session closed: \"{}\" {}.", fUserDescription, fConnection);
    }

    return this;
  }

  private Behavior<Message> onActivate() {
    fContext.watchWith(fWorkHub, Stop.INSTANCE);

    fWorkHub.tell(new WorkHub.RegisterParticipant<>(
        fSessionId,
        fContext.getSelf(),
        Stop.INSTANCE,
        Registered::new));

    return this;
  }

  private enum Stop implements Message { INSTANCE }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }

  private static final class Registered implements Message {
    final LiveWork liveWork;

    Registered(LiveWork liveWork) {
      this.liveWork = liveWork;
    }
  }

  private Behavior<Message> onRegistered(Registered m) {
    LiveWork liveWork = m.liveWork;
    Session session = liveWork.store().read(WorkUtils.loadSession(fSessionId));

    fHandler = fSessionHandlerFactory.create(
        session,
        fConnection,
        fNoticeChannelId,
        true,
        liveWork::acquireSource,
        screenId -> liveWork.store().read(WorkUtils.loadRawScreen(screenId)),
        _session -> liveWork.store().read(getQuestionSources(_session)));

    fUserDescription = AuthenticationUtils.getUserDescription(
        session.getUserName(),
        session.hasUserDisplayName() ? session.getUserDisplayName() : null,
        session.hasGroupName() ? session.getGroupName() : null);

    fLogger.info("Session opened: \"{}\" {}.", fUserDescription, fConnection);

    fWorkHub.tell(new WorkHub.TrackSessionStatus(
        fSessionId,
        fContext.messageAdapter(WorkHub.SessionStatusDelta.class, SessionStatusDelta::new)));

    return this;
  }

  private static final class SessionStatusDelta implements Message {
    final WorkHub.SessionStatusDelta original;

    SessionStatusDelta(WorkHub.SessionStatusDelta original) {
      this.original = original;
    }
  }

  private Behavior<Message> onSessionStatusDelta(WorkHub.SessionStatusDelta m) {
    return fHandler.handleSessionStatusDelta(m.status).proceed ? this : Behaviors.stopped();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    Check.inputNotNull(fHandler);

    StudentRequest r = m.request;
    switch (r.getRequestCase()) {
      case SET_RESPONSE: return onSetResponse(r.getSetResponse());
      case SUBMIT: return onSubmit();
      case CHOOSE_QUESTION: return onChooseQuestion(r.getChooseQuestion());
      case FINISH_WORK: return onFinishWork();
      case QUERY_QUESTION_DETAILS: return onQueryQuestionDetails(r.getQueryQuestionDetails());
      case CLOSE: return onClose();
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private Behavior<Message> onSetResponse(SetResponse m) {
    SessionHandler.SetResponseResult r = fHandler.handleSetResponse(m);

    fWorkHub.tell(new WorkHub.UpdateSession<>(
        fSessionId,
        r.session,
        null,
        false,
        ContinueSetResponse.INSTANCE,
        fContext.getSelf()));

    return this;
  }

  private enum ContinueSetResponse implements Message { INSTANCE }

  private Behavior<Message> onContinueSetResponse() {
    fHandler.handleContinueSetResponse();
    return this;
  }

  private Behavior<Message> onSubmit() {
    SessionHandler.SubmitResult r = fHandler.handleSubmit();

    fWorkHub.tell(new WorkHub.UpdateSession<>(
        fSessionId,
        r.session,
        r.updateResponse,
        false,
        new ContinueSubmit(r.continueSubmit),
        fContext.getSelf()));

    return this;
  }

  private static final class ContinueSubmit implements Message {
    final SessionHandler.ContinueSubmit original;

    ContinueSubmit(SessionHandler.ContinueSubmit original) {
      this.original = original;
    }
  }

  private Behavior<Message> onContinueSubmit(SessionHandler.ContinueSubmit m) {
    fHandler.handleContinueSubmit(m);
    return this;
  }

  private Behavior<Message> onChooseQuestion(ChooseQuestion m) {
    SessionHandler.ChooseQuestionResult r = fHandler.handleChooseQuestion(m);

    fWorkHub.tell(new WorkHub.UpdateSession<>(
        fSessionId,
        r.session,
        null,
        false,
        ContinueChooseQuestion.INSTANCE,
        fContext.getSelf()));

    return this;
  }

  private enum ContinueChooseQuestion implements Message { INSTANCE }

  private Behavior<Message> onContinueChooseQuestion() {
    fHandler.handleContinueChooseQuestion();
    return this;
  }

  private Behavior<Message> onFinishWork() {
    SessionHandler.FinishWorkResult r = fHandler.handleFinishWork();

    fWorkHub.tell(new WorkHub.UpdateSession<>(
        fSessionId,
        r.session,
        r.updateResponse,
        true,
        ContinueFinishWork.INSTANCE,
        fContext.getSelf()));

    return this;
  }

  private enum ContinueFinishWork implements Message { INSTANCE }

  private Behavior<Message> onContinueFinishWork() {
    fHandler.handleContinueFinishWork();
    return this;
  }

  private static DbTask<DbReader, ImmutableList<String>> getQuestionSources(Session session) {
    return dbr -> {
      try {
        ImmutableList.Builder<String> b = ImmutableList.builderWithExpectedSize(session.getQuestionCount());

        SQLiteStatement query = dbr.prepare("SELECT LIVE_QUESTION FROM SCREENS WHERE ID = ?1");
        try {
          for (SessionQuestion sq : session.getQuestionList()) {
            Check.that(query
                .reset()
                .bind(1, sq.getScreenId())
                .step());
            b.add(_LiveQuestion.parseFrom(query.columnBlob(0)).getSource());
          }
        } finally {
          query.dispose();
        }

        return b.build();
      } catch (SQLiteException | InvalidProtocolBufferException e) {
        throw new RuntimeException(e);
      }
    };
  }

  private Behavior<Message> onQueryQuestionDetails(QueryQuestionDetails m) {
    fHandler.handleQueryQuestionDetails(m);
    return this;
  }

  private Behavior<Message> onClose() {
    return Behaviors.stopped();
  }
}
