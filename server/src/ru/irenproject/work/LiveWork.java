/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.work.db.DbReader;
import ru.irenproject.work.db.DbTask;

import java.util.function.Supplier;

public final class LiveWork {
  public interface Store {
    <R> R read(DbTask<DbReader, R> task);
    <R> R readDirectly(DbTask<DbReader, R> task);
  }

  private final Work fWork;
  private final Store fStore;
  private final Supplier<ClosableWorkSource> fClosableWorkSourceSupplier;

  public LiveWork(Work work, Store store, Supplier<ClosableWorkSource> closableWorkSourceSupplier) {
    fWork = work;
    fStore = store;
    fClosableWorkSourceSupplier = closableWorkSourceSupplier;
  }

  public Work work() {
    return fWork;
  }

  public Store store() {
    return fStore;
  }

  public ClosableWorkSource acquireSource() {
    return fClosableWorkSourceSupplier.get();
  }
}
