/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work.db;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.work.Work;

import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.Futures;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadFactory;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Singleton public final class WorkStore {
  private static final Logger fLogger = LoggerFactory.getLogger(WorkStore.class);

  private final DbTaskRunner fDbTaskRunner;

  private final ThreadFactory fThreadFactory = new BasicThreadFactory.Builder()
      .namingPattern("db-%d")
      .build();

  private final Object fLock = new Object();

  private /* @Nullable */ImmutableList<ExecutorService> fExecutors;
  private int fNextExecutorIndex;

  private final HashSet<Work> fOpenWorks = new HashSet<>();
  private long fOpenWorkCount; // includes works scheduled for closing which have already been removed from `fOpenWorks`

  @Inject private WorkStore(DbTaskRunner dbTaskRunner) {
    fDbTaskRunner = dbTaskRunner;
  }

  public void open(Work work) {
    synchronized (fLock) {
      Check.that(fOpenWorks.add(work));
      ++fOpenWorkCount;

      if (fOpenWorkCount == 1) {
        Check.that(fExecutors == null);
        fExecutors = Stream.generate(() -> Executors.newSingleThreadExecutor(fThreadFactory))
            .limit(Runtime.getRuntime().availableProcessors())
            .collect(ImmutableList.toImmutableList());
        fNextExecutorIndex = 0;
      }
    }
  }

  public void close(Work work) {
    List<Future<?>> executorConnectionsClosed;
    synchronized (fLock) {
      boolean removed = fOpenWorks.remove(work);
      executorConnectionsClosed = removed ?
          fExecutors.stream()
              .map(e -> e.submit(() -> fDbTaskRunner.closeCachedConnection(work)))
              .collect(Collectors.toList()) :
          null;
    }

    if (executorConnectionsClosed != null) {
      try {
        executorConnectionsClosed.forEach(Futures::getUnchecked);
      } catch (RuntimeException e) { // should not happen
        fLogger.warn("", e);
      }

      synchronized (fLock) {
        --fOpenWorkCount;
        if (fOpenWorkCount == 0) {
          // all executors are now idle, will terminate quickly
          fExecutors.forEach(ExecutorService::shutdown);
          fExecutors.forEach(Utils::awaitTerminationUninterruptibly);
          fExecutors = null;
        } else {
          Check.that(fOpenWorkCount > 0);
        }
      }
    }
  }

  public <R> R read(Work work, DbTask<DbReader, R> task) {
    return runTask(work, task, true);
  }

  public <R> R write(Work work, DbTask<DbWriter, R> task) {
    return runTask(work, task, false);
  }

  private <R> R runTask(Work work, DbTask<? super DbTransaction, R> task, boolean readOnly) {
    Future<R> result;
    synchronized (fLock) {
      Check.that(fOpenWorks.contains(work));

      ExecutorService executor = fExecutors.get(fNextExecutorIndex);
      fNextExecutorIndex = (fNextExecutorIndex + 1) % fExecutors.size();
      result = executor.submit(() -> fDbTaskRunner.runTaskInCachedConnection(work, task, readOnly));
    }

    return Futures.getUnchecked(result);
  }

  public <R> R readDirectly(Work work, DbTask<DbReader, R> task) {
    return fDbTaskRunner.runTaskInSeparateConnection(work, task, true);
  }
}
