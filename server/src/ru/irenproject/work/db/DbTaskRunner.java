/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work.db;

import ru.irenproject.Check;
import ru.irenproject.work.SqliteTuner;
import ru.irenproject.work.Work;
import ru.irenproject.work.WorkDirectory;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;

final class DbTaskRunner {
  private static final Logger fLogger = LoggerFactory.getLogger(DbTaskRunner.class);

  private final WorkDirectory fWorkDirectory;
  private final SqliteTuner fSqliteTuner;

  private final Object fConnectDisconnectLock = new Object();

  private final ThreadLocal<LoadingCache<Work, SQLiteConnection>> fConnections =
      ThreadLocal.withInitial(this::createConnectionCache);

  @Inject private DbTaskRunner(WorkDirectory workDirectory, SqliteTuner sqliteTuner) {
    fWorkDirectory = workDirectory;
    fSqliteTuner = sqliteTuner;
  }

  private LoadingCache<Work, SQLiteConnection> createConnectionCache() {
    return CacheBuilder.newBuilder()
        .concurrencyLevel(1)
        .removalListener(this::onConnectionRemoved)
        .build(CacheLoader.from(this::openConnection));
  }

  private LoadingCache<Work, SQLiteConnection> threadLocalConnections() {
    return fConnections.get();
  }

  private SQLiteConnection openConnection(Work work) {
    try {
      SQLiteConnection res = new SQLiteConnection(fWorkDirectory.getDatabaseFile(work.id()).toFile());

      boolean ok = false;
      try {
        // SQLite needs exclusive access to the database in WAL mode during the initial activity
        // in the first connection.
        synchronized (fConnectDisconnectLock) {
          res.open(false);
          SQLiteStatement s = res.prepare("SELECT * FROM SQLITE_MASTER LIMIT 1");
          try {
            s.step();
          } finally {
            s.dispose();
          }
        }

        // In theory, this shouldn't be needed since we use a single-writer-multiple-readers scheme which should work
        // without blocking in WAL mode, according to the SQLite documentation. However, SQLITE_BUSY errors sometimes
        // occurred during stress testing in the absence of this timeout.
        res.setBusyTimeout(1000);

        fSqliteTuner.tune(res);
        ok = true;
      } finally {
        if (!ok) {
          dispose(res);
        }
      }

      return res;
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private void dispose(SQLiteConnection connection) {
    synchronized (fConnectDisconnectLock) {
      connection.dispose();
    }
  }

  private void onConnectionRemoved(RemovalNotification<Work, SQLiteConnection> n) {
    dispose(n.getValue());
  }

  public <R> R runTaskInCachedConnection(Work work, DbTask<? super DbTransaction, R> task, boolean readOnly) {
    return runTaskInConnection(
        task,
        threadLocalConnections().getUnchecked(work),
        readOnly,
        () -> threadLocalConnections().invalidate(work));
  }

  public <R> R runTaskInSeparateConnection(Work work, DbTask<? super DbTransaction, R> task, boolean readOnly) {
    SQLiteConnection connection = openConnection(work);
    try {
      return runTaskInConnection(
          task,
          connection,
          readOnly,
          () -> {});
    } finally {
      dispose(connection);
    }
  }

  private static <R> R runTaskInConnection(
      DbTask<? super DbTransaction, R> task,
      SQLiteConnection connection,
      boolean readOnly,
      Runnable onRollbackFailure) {
    try {
      boolean notInsideTransaction = connection.getAutoCommit();
      Check.that(notInsideTransaction);

      connection.exec("PRAGMA query_only = " + readOnly);
      connection.exec(readOnly ? "BEGIN TRANSACTION" : "BEGIN IMMEDIATE TRANSACTION");

      R res;
      boolean ok = false;
      try {
        res = task.run(new DbTransaction(connection));
        connection.exec("COMMIT");
        ok = true;
      } finally {
        if (!ok && !tryRollback(connection)) {
          onRollbackFailure.run();
        }
      }

      return res;
    } catch (SQLiteException e) {
      throw new RuntimeException(e);
    }
  }

  private static boolean tryRollback(SQLiteConnection connection) {
    boolean res;
    try {
      connection.exec("ROLLBACK");
      res = true;
    } catch (SQLiteException | RuntimeException e) {
      fLogger.error("Error on transaction rollback:", e);
      res = false;
    }

    return res;
  }

  public void closeCachedConnection(Work work) {
    threadLocalConnections().invalidate(work);
  }
}
