/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work.db;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

final class DbTransaction implements DbWriter {
  private final SQLiteConnection fConnection;

  public DbTransaction(SQLiteConnection connection) {
    fConnection = connection;
  }

  @Override public SQLiteStatement prepare(String sql) throws SQLiteException {
    return fConnection.prepare(sql);
  }

  @Override public int getChanges() throws SQLiteException {
    return fConnection.getChanges();
  }

  @Override public long getLastInsertId() throws SQLiteException {
    return fConnection.getLastInsertId();
  }
}
