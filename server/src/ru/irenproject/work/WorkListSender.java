/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.common.protocol.Proto.ServerMessage;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.collect.ImmutableList;
import com.google.inject.assistedinject.Assisted;
import io.netty.channel.ChannelFuture;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Comparator;
import java.util.function.Function;

public final class WorkListSender extends AbstractBehavior<WorkListSender.Message> {
  public interface Message {}

  public interface Factory {
    WorkListSender create(
        ActorContext<Message> context,
        Connection connection,
        Function<ImmutableList<Work>, ServerMessage> noticeBuilder);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection,
        Function<ImmutableList<Work>, ServerMessage> noticeBuilder) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection, noticeBuilder));
    }
  }

  private final ActorContext<Message> fContext;
  private final Connection fConnection;
  private final Function<ImmutableList<Work>, ServerMessage> fNoticeBuilder;

  private @Nullable ImmutableList<Work> fUnsent;
  private boolean fSending;

  @Inject private WorkListSender(
      @Assisted ActorContext<Message> context,
      @Assisted Connection connection,
      @Assisted Function<ImmutableList<Work>, ServerMessage> noticeBuilder,
      ActorRef<WorkCatalog.Message> workCatalog) {
    fContext = context;
    fConnection = connection;
    fNoticeBuilder = noticeBuilder;

    workCatalog.tell(new WorkCatalog.TrackWorkList(
        fContext.messageAdapter(WorkCatalog.WorkList.class, WorkList::new)));
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(WorkList.class, m -> onWorkList(m.original))
        .onMessageEquals(SendComplete.INSTANCE, this::onSendComplete)
        .build();
  }

  private static final class WorkList implements Message {
    final WorkCatalog.WorkList original;

    WorkList(WorkCatalog.WorkList original) {
      this.original = original;
    }
  }

  private Behavior<Message> onWorkList(WorkCatalog.WorkList m) {
    fUnsent = m.workListSortedById;
    sendIfNeeded();

    return this;
  }

  private void sendIfNeeded() {
    if ((fUnsent != null) && !fSending) {
      ImmutableList<Work> sorted = fUnsent.stream()
          .sorted(Comparator
              .comparing(Work::title, String.CASE_INSENSITIVE_ORDER) //TODO use locale
              .thenComparing(Work::startedAt, Comparator.reverseOrder()))
          .collect(ImmutableList.toImmutableList());

      ChannelFuture f = fConnection.sendWithFeedback(fNoticeBuilder.apply(sorted));
      f.addListener(ignored -> fContext.getSelf().tell(SendComplete.INSTANCE));

      fUnsent = null;
      fSending = true;
    }
  }

  private enum SendComplete implements Message { INSTANCE }

  private Behavior<Message> onSendComplete() {
    fSending = false;
    sendIfNeeded();

    return this;
  }
}
