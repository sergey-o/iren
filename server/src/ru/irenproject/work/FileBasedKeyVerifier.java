/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.io.BaseEncoding;
import com.google.common.io.MoreFiles;
import com.google.common.primitives.Ints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.util.List;

public final class FileBasedKeyVerifier implements KeyVerifier {
  private static final Logger fLogger = LoggerFactory.getLogger(FileBasedKeyVerifier.class);

  private static final Splitter SPLITTER = Splitter.on(CharMatcher.whitespace()).omitEmptyStrings();

  private final Path fKeyFile;

  public FileBasedKeyVerifier(Path keyFile) {
    fKeyFile = keyFile;
  }

  @Override public boolean verify(byte[] key) {
    boolean res;

    try {
      String line;
      try {
        line = Check.notNull(MoreFiles.asCharSource(fKeyFile, StandardCharsets.UTF_8).readFirstLine(),
            BadKeyFileException::new);
      } catch (IOException e) {
        throw new BadKeyFileException(e);
      }

      List<String> fields = SPLITTER.splitToList(line);
      Check.that(fields.size() == 4, BadKeyFileException::new);
      Check.that(fields.get(0).equals("pbkdf2-sha256"), BadKeyFileException::new);

      int iterationCount = Check.notNull(Ints.tryParse(fields.get(1)), BadKeyFileException::new);
      Check.that((iterationCount >= 1) && (iterationCount <= 1_000_000), BadKeyFileException::new);

      byte[] salt, hash;
      try {
        salt = BaseEncoding.base16().lowerCase().decode(fields.get(2));
        hash = BaseEncoding.base16().lowerCase().decode(fields.get(3));
      } catch (IllegalArgumentException e) {
        throw new BadKeyFileException(e);
      }

      CharBuffer keyChars;
      try {
        keyChars = StandardCharsets.UTF_8.newDecoder().decode(ByteBuffer.wrap(key));
      } catch (CharacterCodingException ignored) {
        keyChars = null;
      }

      res = (keyChars != null) && MessageDigest.isEqual(
          Utils.pbkdf2Sha256(Utils.readCharBuffer(keyChars), salt, iterationCount), hash);
    } catch (BadKeyFileException e) {
      fLogger.warn("", e);
      res = false;
    }

    return res;
  }

  private static final class BadKeyFileException extends RuntimeException {
    BadKeyFileException() {}

    BadKeyFileException(Throwable cause) {
      super(cause);
    }
  }
}
