/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Provider;

public final class DataDirectoryUnlocker extends AbstractBehavior<DataDirectoryUnlocker.Message> {
  public interface Message {}

  public enum Stop implements Message { INSTANCE }

  private final @Nullable DataDirectory fDataDirectory;

  @Inject private DataDirectoryUnlocker(Provider<DataDirectory> dataDirectoryProvider) {
    DataDirectory dataDirectory;
    try {
      dataDirectory = dataDirectoryProvider.get();
    } catch (RuntimeException ignored) {
      dataDirectory = null;
    }

    fDataDirectory = dataDirectory;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .build();
  }

  private Behavior<Message> onStop() {
    if (fDataDirectory != null) {
      fDataDirectory.unlock();
    }

    return Behaviors.stopped();
  }
}
