/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.Utils;
import ru.irenproject.authentication.AuthenticatedUser;
import ru.irenproject.common.protocol.Proto.ServerMessage;

import com.google.common.base.Strings;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.handler.stream.ChunkedInput;

import javax.annotation.Nullable;
import java.util.concurrent.atomic.AtomicReference;

public final class Connection implements ConnectionLike {
  private final Channel fChannel;
  private final AtomicReference</* @Nullable */AuthenticatedUser> fUser = new AtomicReference<>();

  public Connection(Channel channel) {
    fChannel = channel;
  }

  @Override public String toString() {
    return Utils.channelToString(fChannel);
  }

  public int channelId() {
    return fChannel.hashCode();
  }

  public String clientAddress() {
    return Strings.nullToEmpty(Utils.getClientAddressIfKnown(fChannel));
  }

  @Override public void enqueue(ServerMessage message) {
    write(message, false);
  }

  @Override public void send(ServerMessage message) {
    write(message, true);
  }

  public ChannelFuture sendWithFeedback(ServerMessage message) {
    return write(message, true);
  }

  public ChannelFuture sendChunked(ChunkedInput<?> input) {
    return write(input, true);
  }

  private ChannelFuture write(Object message, boolean flush) {
    ChannelFuture res = flush ? fChannel.writeAndFlush(message) : fChannel.write(message);
    res.addListener(Utils.CLOSE_ON_ERROR);
    return res;
  }

  @Override public void flush() {
    fChannel.flush();
  }

  public void close() {
    fChannel.close();
  }

  public ChannelFuture closeFuture() {
    return fChannel.closeFuture();
  }

  public void setAutoRead(boolean autoRead) {
    fChannel.config().setAutoRead(autoRead);
  }

  public @Nullable AuthenticatedUser user() {
    return fUser.get();
  }

  public boolean setUserIfAbsent(AuthenticatedUser user) {
    return fUser.compareAndSet(null, user);
  }
}
