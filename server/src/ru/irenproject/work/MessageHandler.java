/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import ru.irenproject.common.protocol.Proto.ClientMessage;

import akka.actor.typed.ActorRef;
import akka.actor.typed.javadsl.AskPattern;
import com.google.inject.assistedinject.Assisted;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.time.Duration;
import java.util.concurrent.CompletionStage;

public final class MessageHandler extends ChannelInboundHandlerAdapter {
  public interface Factory {
    MessageHandler create(
        KeyVerifier keyVerifier,
        @Nullable String serverAddress);
  }

  public enum ReadyEvent { INSTANCE }

  private static final Logger fLogger = LoggerFactory.getLogger(MessageHandler.class);

  private final KeyVerifier fKeyVerifier;
  private final @Nullable String fServerAddress;
  private final WorkEnvironment fWorkEnvironment;
  private final ActorRef<ConnectionCatalog.Message> fConnectionCatalog;

  private volatile ActorRef<ConnectionHandler.Message> fConnectionHandler;

  @Inject private MessageHandler(
      @Assisted KeyVerifier keyVerifier,
      @Assisted @Nullable String serverAddress,
      WorkEnvironment workEnvironment,
      ActorRef<ConnectionCatalog.Message> connectionCatalog) {
    fKeyVerifier = keyVerifier;
    fServerAddress = serverAddress;
    fWorkEnvironment = workEnvironment;
    fConnectionCatalog = connectionCatalog;
  }

  @Override public void channelActive(ChannelHandlerContext ctx) throws Exception {
    Connection connection = new Connection(ctx.channel());

    CompletionStage<ActorRef<ConnectionHandler.Message>> createConnectionHandler = AskPattern.ask(
        fConnectionCatalog,
        replyTo -> new ConnectionCatalog.CreateConnectionHandler(connection, fKeyVerifier, fServerAddress, replyTo),
        Duration.ofSeconds(5),
        fWorkEnvironment.scheduler());
    createConnectionHandler
        .thenAcceptAsync(connectionHandler -> {
          fConnectionHandler = connectionHandler;
          connection.setAutoRead(true);
        })
        .whenCompleteAsync((ignored, e) -> {
          if (e != null) {
            fLogger.warn("", e);
            connection.close();
          }
        });
    super.channelActive(ctx);
  }

  @Override public void userEventTriggered(ChannelHandlerContext ctx, Object event) throws Exception {
    if (event instanceof ReadyEvent) {
      fConnectionHandler.tell(ConnectionHandler.Activate.INSTANCE);
    } else {
      super.userEventTriggered(ctx, event);
    }
  }

  @Override public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
    if (msg instanceof ClientMessage) {
      fConnectionHandler.tell(new ConnectionHandler.Data((ClientMessage) msg));
    } else {
      super.channelRead(ctx, msg);
    }
  }
}
