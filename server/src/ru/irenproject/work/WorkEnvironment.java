/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.work;

import akka.actor.Scheduler;
import akka.actor.typed.ActorRef;
import akka.actor.typed.ActorSystem;
import akka.actor.typed.Behavior;
import akka.actor.typed.ChildFailed;
import akka.actor.typed.DispatcherSelector;
import akka.actor.typed.Props;
import akka.actor.typed.Terminated;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.AskPattern;
import akka.actor.typed.javadsl.Behaviors;

import java.time.Duration;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.atomic.AtomicBoolean;

public final class WorkEnvironment implements AutoCloseable {
  private final ActorSystem<Root.Message> fActorSystem;
  private final CompletionStage<Boolean> fWhenTerminated;
  private final ActorRef<ComponentSupervisor.Message> fComponentSupervisor;

  public WorkEnvironment() {
    AtomicBoolean normalCompletion = new AtomicBoolean();
    fActorSystem = ActorSystem.create(Root.makeBehavior(normalCompletion), Root.class.getSimpleName());
    fWhenTerminated = fActorSystem.getWhenTerminated().handle((ignored, ignored2) -> normalCompletion.get());

    boolean ok = false;
    try {
      fComponentSupervisor = createComponentSupervisor(fActorSystem);
      ok = true;
    } finally {
      if (!ok) {
        close();
      }
    }
  }

  @Override public void close() {
    if (fComponentSupervisor == null) {
      fActorSystem.terminate();
    } else {
      fComponentSupervisor.tell(ComponentSupervisor.Stop.INSTANCE);
    }

    fWhenTerminated.toCompletableFuture().join();
  }

  private static ActorRef<ComponentSupervisor.Message> createComponentSupervisor(
      ActorSystem<Root.Message> actorSystem) {
    CompletionStage<ActorRef<ComponentSupervisor.Message>> cs = AskPattern.ask(
        actorSystem,
        Root.CreateComponentSupervisor::new,
        Duration.ofSeconds(5),
        actorSystem.scheduler());
    return cs.toCompletableFuture().join();
  }

  private static final class Root {
    interface Message {}

    static final class CreateComponentSupervisor implements Message {
      final ActorRef<ActorRef<ComponentSupervisor.Message>> replyTo;

      CreateComponentSupervisor(ActorRef<ActorRef<ComponentSupervisor.Message>> replyTo) {
        this.replyTo = replyTo;
      }
    }

    static Behavior<Message> makeBehavior(AtomicBoolean normalCompletion) {
      return Behaviors.receive(Message.class)
          .onSignal(Terminated.class, (ignored, signal) -> {
            normalCompletion.set(!(signal instanceof ChildFailed));
            return Behaviors.stopped();
          })
          .onMessage(CreateComponentSupervisor.class, Root::onCreateComponentSupervisor)
          .build();
    }

    static Behavior<Message> onCreateComponentSupervisor(ActorContext<Message> ctx, CreateComponentSupervisor m) {
      ActorRef<ComponentSupervisor.Message> componentSupervisor = ctx.spawn(
          Behaviors.setup(ComponentSupervisor::new),
          ComponentSupervisor.class.getSimpleName(),
          DispatcherSelector.fromConfig("component-supervisor-dispatcher"));
      ctx.watch(componentSupervisor);

      m.replyTo.tell(componentSupervisor);

      return Behaviors.same();
    }
  }

  public <T> ActorRef<T> createComponent(Behavior<T> behavior, String name, int stopPhase, T stopMessage) {
    return createComponentWithProps(behavior, name, Props.empty(), stopPhase, stopMessage);
  }

  public <T> ActorRef<T> createComponentWithProps(Behavior<T> behavior, String name, Props props, int stopPhase,
      T stopMessage) {
    CompletionStage<ActorRef<T>> cs = AskPattern.ask(
        fComponentSupervisor,
        replyTo -> new ComponentSupervisor.CreateComponent<>(behavior, name, props, stopPhase, stopMessage, replyTo),
        Duration.ofSeconds(5),
        fActorSystem.scheduler());

    // Waiting here is ok: the target (ComponentSupervisor) runs in a dedicated thread
    // and replies quickly.
    return cs.toCompletableFuture().join();
  }

  public Scheduler scheduler() {
    return fActorSystem.scheduler();
  }

  public CompletionStage<Boolean> whenTerminated() {
    return fWhenTerminated;
  }
}
