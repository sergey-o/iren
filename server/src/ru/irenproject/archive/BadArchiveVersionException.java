/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archive;

public final class BadArchiveVersionException extends RuntimeException {
  private final boolean fTooNew;

  public BadArchiveVersionException() {
    fTooNew = false;
  }

  public BadArchiveVersionException(Throwable cause) {
    super(cause);
    fTooNew = false;
  }

  public BadArchiveVersionException(int version) {
    super(String.format("You need to update the program to open a version %d archive.", version));
    fTooNew = true;
  }

  public boolean tooNew() {
    return fTooNew;
  }
}
