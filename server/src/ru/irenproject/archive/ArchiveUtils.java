/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archive;

import ru.irenproject.Check;
import ru.irenproject.infra.BlobId;

import com.google.common.hash.HashFunction;
import com.google.common.io.ByteStreams;
import com.google.common.primitives.Ints;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.zip.ZipEntry;

public final class ArchiveUtils {
  public static final HashFunction OBJECT_ID_HASH_FUNCTION = BlobId.HASH_FUNCTION;

  public static final String VERSION_ENTRY_NAME = "meta/ru.irenproject.archive.version";
  private static final int VERSION = 4;

  public static final String ENTRY_NAME_TIMESTAMP_PATTERN = "uuuu-MM-dd'_'HHmmssZ";

  public static String getObjectEntryName(String objectId) {
    return "objects/" + objectId;
  }

  public static ZipArchiveEntry createZipEntry(String name, Instant time) {
    ZipArchiveEntry res = new ZipArchiveEntry(name);
    res.setTime(time.toEpochMilli());

    //noinspection OctalInteger
    res.setUnixMode(0100660); // unzip on Linux doesn't like UTF-8 entry names without this

    return res;
  }

  public static void checkVersion(ZipFile archive) {
    ZipArchiveEntry entry = Check.notNull(archive.getEntry(VERSION_ENTRY_NAME), BadArchiveVersionException::new);

    byte[] versionBytes = new byte[10];
    int bytesRead;
    try (InputStream in = archive.getInputStream(entry)) {
      bytesRead = ByteStreams.read(in, versionBytes, 0, versionBytes.length);
    } catch (IOException e) {
      throw new BadArchiveVersionException(e);
    }
    Check.that(bytesRead < versionBytes.length, BadArchiveVersionException::new);

    int version = Check.notNull(Ints.tryParse(new String(versionBytes, 0, bytesRead, StandardCharsets.US_ASCII)),
        BadArchiveVersionException::new);
    Check.that(version >= 1, BadArchiveVersionException::new);
    if (version > VERSION) {
      throw new BadArchiveVersionException(version);
    }
  }

  public static void writeVersion(ZipArchiveOutputStream archive, Instant time) {
    try {
      ZipArchiveEntry entry = createZipEntry(VERSION_ENTRY_NAME, time);
      entry.setMethod(ZipEntry.STORED);

      archive.putArchiveEntry(entry);
      archive.write(Integer.toString(VERSION).getBytes(StandardCharsets.US_ASCII));
      archive.closeArchiveEntry();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private ArchiveUtils() {}
}
