/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.formula.FormulaRendererPreloaderModule;
import ru.irenproject.work.DataDirectory;
import ru.irenproject.work.ExceptionHandler;
import ru.irenproject.work.FileBasedKeyVerifier;
import ru.irenproject.work.KeyVerifier;
import ru.irenproject.work.MessageHandler;
import ru.irenproject.work.MessageLogger;
import ru.irenproject.work.NetworkExceptionHandler;
import ru.irenproject.work.ResourceDepot;
import ru.irenproject.work.WebTransportHandler;
import ru.irenproject.work.Work;
import ru.irenproject.work.WorkCatalog;
import ru.irenproject.work.WorkDirectory;
import ru.irenproject.work.WorkEnvironment;
import ru.irenproject.work.WorkModule;

import akka.actor.typed.ActorRef;
import ch.qos.logback.classic.Level;
import com.google.common.io.BaseEncoding;
import com.google.common.net.InetAddresses;
import com.google.common.util.concurrent.Uninterruptibles;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.UnpooledByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.ServerChannel;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerDomainSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.unix.DomainSocketAddress;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.websocketx.WebSocketFrameAggregator;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.util.concurrent.DefaultPromise;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import sun.misc.Signal;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFilePermissions;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.function.Function;

@Singleton public final class Server {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("listen")
    @Nullable String listen;
  }

  private static final Logger fLogger = LoggerFactory.getLogger(Server.class);

  public static final int DEFAULT_PORT = 9981;

  private static final String UNIX_SOCKET_PREFIX = "unix:";

  public static void main(String[] args) {
    Locale.setDefault(Locale.ROOT); // guard against libraries inadvertently using default locale
    Thread.setDefaultUncaughtExceptionHandler((t, e) -> fLogger.error("Exception in thread {}:", t.getName(), e));

    SLF4JBridgeHandler.removeHandlersForRootLogger();
    SLF4JBridgeHandler.install();

    fLogger.info("-".repeat(80));
    fLogger.info("irenServer " + ProgramInfo.VERSION);
    fLogger.info("-".repeat(80));

    boolean service = ArrayUtils.contains(args, "--service");
    CountDownLatch shutdownLatch;
    if (service) {
      shutdownLatch = new CountDownLatch(1);
      Signal.handle(new Signal("TERM"), ignored -> shutdownLatch.countDown());
    } else {
      shutdownLatch = null;
    }

    try (WorkEnvironment environment = new WorkEnvironment()) {
      environment.whenTerminated().thenAccept(normalCompletion -> {
        if (!normalCompletion) {
          fLogger.error("Actor system terminated abnormally.");
          Runtime.getRuntime().halt(1);
        }
      });

      ArrayList<Module> modules = new ArrayList<>();
      modules.add(new WorkModule(environment));

      String configFile = Utils.getOptionValue("--config", args);
      if (configFile != null) {
        modules.add(new OptionModule(Paths.get(configFile)));
      }

      modules.add(new FormulaRendererPreloaderModule());

      Injector injector = Guice.createInjector(modules);
      injector.getInstance(Server.class).run(ArrayUtils.contains(args, "--companion"), () -> {
        if (service) {
          int notifyResult = LibSystemd.sd_notify(0, "READY=1");
          if (notifyResult <= 0) {
            fLogger.warn("sd_notify failed: {}.", notifyResult);
          }
          Uninterruptibles.awaitUninterruptibly(shutdownLatch);
        } else {
          Utils.readFromSystemIn();
        }
        Utils.setLoggerLevel(DefaultPromise.class.getName() + ".rejectedExecution", Level.OFF);
      });
    }

    fLogger.info("Stopped.");
  }

  private final Config fConfig;
  private final DataDirectory fDataDirectory;
  private final WebTransportHandler.Factory fWebTransportHandlerFactory;
  private final MessageHandler.Factory fMessageHandlerFactory;
  private final WorkDirectory fWorkDirectory;
  private final ActorRef<WorkCatalog.Message> fWorkCatalog;

  @Inject private Server(
      InitializationTracker initializationTracker,
      Config config,
      DataDirectory dataDirectory,
      WebTransportHandler.Factory webTransportHandlerFactory,
      MessageHandler.Factory messageHandlerFactory,
      WorkDirectory workDirectory,
      ActorRef<WorkCatalog.Message> workCatalog) {
    fConfig = config;
    fDataDirectory = dataDirectory;
    fWebTransportHandlerFactory = webTransportHandlerFactory;
    fMessageHandlerFactory = messageHandlerFactory;
    fWorkDirectory = workDirectory;
    fWorkCatalog = workCatalog;

    initializationTracker.await();
  }

  public void run(boolean companion, Runnable shutdownWaiter) {
    try {
      String unixSocketPath;
      SocketAddress listeningAddress;
      boolean useRealIpHeader;
      Function<Integer, EventLoopGroup> eventLoopGroupFactory;
      Class<? extends ServerChannel> serverChannelClass;
      String announcedAddress;

      if ((fConfig.listen != null) && fConfig.listen.startsWith(UNIX_SOCKET_PREFIX)) {
        unixSocketPath = fConfig.listen.substring(UNIX_SOCKET_PREFIX.length());
        Files.deleteIfExists(Paths.get(unixSocketPath));
        listeningAddress = LinuxHelper.createDomainSocketAddress(unixSocketPath);
        useRealIpHeader = true;
        eventLoopGroupFactory = LinuxHelper::createEpollEventLoopGroup;
        serverChannelClass = LinuxHelper.epollServerDomainSocketChannelClass();
        announcedAddress = null;
      } else {
        int port = (fConfig.listen == null) ? DEFAULT_PORT : Integer.parseInt(fConfig.listen);
        unixSocketPath = null;
        listeningAddress = new InetSocketAddress(port);
        useRealIpHeader = false;
        eventLoopGroupFactory = NioEventLoopGroup::new;
        serverChannelClass = NioServerSocketChannel.class;
        announcedAddress = obtainServerAddress(port);
      }

      byte[] generatedSupervisorKey;
      KeyVerifier keyVerifier;
      if (companion) {
        generatedSupervisorKey = new byte[32];
        new SecureRandom().nextBytes(generatedSupervisorKey);
        keyVerifier = key -> MessageDigest.isEqual(key, generatedSupervisorKey);
      } else {
        generatedSupervisorKey = null;
        keyVerifier = new FileBasedKeyVerifier(fDataDirectory.get().resolve("supervisorKey"));
      }

      for (Work work : fWorkDirectory.listWorksDeletingUnavailable()) {
        fWorkCatalog.tell(new WorkCatalog.Open(work));
      }

      EventLoopGroup childGroup = eventLoopGroupFactory.apply(Runtime.getRuntime().availableProcessors());
      try {
        EventLoopGroup parentGroup = eventLoopGroupFactory.apply(1);
        try {
          ServerBootstrap bootstrap = new ServerBootstrap();
          ResourceDepot resourceDepot = new ResourceDepot(Utils.externalResourcePath().resolve("irenClient.zip"));

          bootstrap
              .group(parentGroup, childGroup)
              .channel(serverChannelClass)
              .childOption(ChannelOption.ALLOCATOR, UnpooledByteBufAllocator.DEFAULT)
              .childOption(ChannelOption.AUTO_READ, false)
              .childHandler(new ChannelInitializer<>() {
                @Override protected void initChannel(Channel ch) {
                  ch.pipeline().addLast(
                      NetworkExceptionHandler.get(),
                      new HttpRequestDecoder(),
                      new HttpResponseEncoder(),
                      new HttpObjectAggregator(1000),
                      new WebSocketFrameAggregator(WebTransportHandler.MAX_FRAME_SIZE),
                      fWebTransportHandlerFactory.create(useRealIpHeader, resourceDepot));
                  MessageLogger.addIfLoggingEnabled(ch.pipeline());
                  ch.pipeline().addLast(
                      new ChunkedWriteHandler(),
                      fMessageHandlerFactory.create(keyVerifier, announcedAddress),
                      ExceptionHandler.get());
                }
              });
          bootstrap.bind(listeningAddress).syncUninterruptibly();

          fLogger.info("Listening on {}.", (listeningAddress instanceof InetSocketAddress) ?
              String.format("port %d", ((InetSocketAddress) listeningAddress).getPort()) :
              String.format("'%s'", listeningAddress));

          if (unixSocketPath != null) {
            try {
              Files.setPosixFilePermissions(Paths.get(unixSocketPath), PosixFilePermissions.fromString("rw-rw-rw-"));
            } catch (IOException | RuntimeException e) {
              fLogger.warn("", e);
            }
          }

          if (companion) {
            Check.that(listeningAddress instanceof InetSocketAddress, () -> Oops.format(
                "Cannot listen on '%s' in companion mode.", listeningAddress));
            System.out.format((Locale) null, "{\"port\": %d, \"supervisorKey\": \"%s\"}\n",
                ((InetSocketAddress) listeningAddress).getPort(),
                BaseEncoding.base16().lowerCase().encode(generatedSupervisorKey));
            System.out.close();
          }

          shutdownWaiter.run();
        } finally {
          Utils.stopExecutorGroup(parentGroup);
        }
      } finally {
        Utils.stopExecutorGroup(childGroup);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // Due to the way Java class loading works, isolating all references to Linux-specific classes here
  // allows us to exclude the corresponding `jar`s from the Windows build.
  private static final class LinuxHelper {
    static SocketAddress createDomainSocketAddress(String path) {
      return new DomainSocketAddress(path);
    }

    static EventLoopGroup createEpollEventLoopGroup(int nThreads) {
      return new EpollEventLoopGroup(nThreads);
    }

    static Class<? extends ServerChannel> epollServerDomainSocketChannelClass() {
      return EpollServerDomainSocketChannel.class;
    }
  }

  private String obtainServerAddress(int port) {
    return String.format("http://%s%s/", InetAddresses.toUriString(someAddressOfTheHost()),
        (port == 80) ? "" : ":" + port);
  }

  private InetAddress someAddressOfTheHost() {
    try {
      InetAddress res = null;
      Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

      outer:
      while (interfaces.hasMoreElements()) {
        Enumeration<InetAddress> addresses = interfaces.nextElement().getInetAddresses();
        while (addresses.hasMoreElements()) {
          InetAddress address = addresses.nextElement();
          if (!address.isLoopbackAddress()) {
            if (address instanceof Inet4Address) {
              res = address;
              break outer;
            } else if (res == null) {
              res = address;
            }
          }
        }
      }

      if (res == null) {
        res = InetAddress.getLocalHost();
      }

      return res;
    } catch (SocketException | UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }
}
