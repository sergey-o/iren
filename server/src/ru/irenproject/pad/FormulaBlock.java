/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pad;

import ru.irenproject.infra.Freezable;

import javax.annotation.Nullable;

@Freezable public final class FormulaBlock implements Block {
  private final String fSource;

  public FormulaBlock(String source) {
    fSource = source;
  }

  public String source() {
    return fSource;
  }

  @Override public int length() {
    return 1;
  }

  @Override public String toString() {
    return fSource;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    FormulaBlock other = (FormulaBlock) obj;
    return fSource.equals(other.fSource);
  }

  @Override public int hashCode() {
    return fSource.hashCode();
  }
}
