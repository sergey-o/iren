/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import com.google.common.io.BaseEncoding;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Locale;

public final class KeyHasher {
  private static final int SALT_SIZE = 16;
  private static final int ITERATION_COUNT = 20_000;

  public static void main(String[] args) {
    try (
        InputStreamReader r = new InputStreamReader(System.in, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(r)) {
      String key = br.readLine();
      if (key != null) {
        byte[] salt = new byte[SALT_SIZE];
        new SecureRandom().nextBytes(salt);

        byte[] hash = Utils.pbkdf2Sha256(key.toCharArray(), salt, ITERATION_COUNT);
        System.out.format((Locale) null, "pbkdf2-sha256 %d %s %s%n", ITERATION_COUNT,
            BaseEncoding.base16().lowerCase().encode(salt),
            BaseEncoding.base16().lowerCase().encode(hash));
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
