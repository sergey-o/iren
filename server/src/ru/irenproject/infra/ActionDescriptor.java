/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import ru.irenproject.editor.Action;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;

import java.lang.reflect.Method;

final class ActionDescriptor {
  public interface InputParser {
    @SuppressWarnings("RedundantThrows")
    Message parseFrom(ByteString data) throws InvalidProtocolBufferException;
  }

  private final InputParser fInputParser;
  private final Method fMethod;
  private final Action fAnnotation;

  public ActionDescriptor(InputParser inputParser, Method method, Action annotation) {
    fInputParser = inputParser;
    fMethod = method;
    fAnnotation = annotation;
  }

  public InputParser inputParser() {
    return fInputParser;
  }

  public Method method() {
    return fMethod;
  }

  public Action annotation() {
    return fAnnotation;
  }
}
