/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoException;
import com.esotericsoftware.kryo.Serializer;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.common.io.ByteStreams;
import com.google.protobuf.ByteString;

import java.io.IOException;

public final class ByteStringSerializer extends Serializer<ByteString> {
  public ByteStringSerializer() {
    super(false, true);
  }

  @Override public void write(Kryo kryo, Output output, ByteString object) {
    output.writeInt(object.size(), true);
    try {
      object.writeTo(output);
    } catch (IOException e) {
      throw new KryoException(e);
    }
  }

  @Override public ByteString read(Kryo kryo, Input input, Class<ByteString> type) {
    int size = input.readInt(true);
    try {
      return ByteString.readFrom(ByteStreams.limit(input, size));
    } catch (IOException e) {
      throw new KryoException(e);
    }
  }
}
