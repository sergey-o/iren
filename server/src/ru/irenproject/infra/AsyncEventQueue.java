/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;
import java.util.LinkedHashSet;

public final class AsyncEventQueue implements Realm.EventSink {
  private static final Logger fLogger = LoggerFactory.getLogger(AsyncEventQueue.class);

  private final LinkedHashSet<PostedEvent> fPostedEvents = new LinkedHashSet<>();

  @Override public void post(Resident source, Resident target, Event event) {
    fPostedEvents.add(new PostedEvent(source, target, event));
  }

  public void dispatch() {
    Iterator<PostedEvent> i;
    while ((i = fPostedEvents.iterator()).hasNext()) {
      PostedEvent e = i.next();
      i.remove();
      try {
        e.target().handleEvent(e.event(), e.source());
      } catch (RuntimeException ex) {
        fLogger.error("", ex);
      }
    }
  }

  public boolean isEmpty() {
    return fPostedEvents.isEmpty();
  }
}
