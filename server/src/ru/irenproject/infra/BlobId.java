/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.protobuf.ByteString;

import javax.annotation.Nullable;

@Freezable public final class BlobId {
  public static final HashFunction HASH_FUNCTION = Hashing.sha256();
  public static final int SIZE = HASH_FUNCTION.bits() / 8;

  public static @Nullable BlobId tryFromHex(String s) {
    byte[] ba;
    try {
      ba = BaseEncoding.base16().lowerCase().decode(s);
    } catch (IllegalArgumentException ignored) {
      ba = null;
    }
    return ((ba != null) && (ba.length == SIZE)) ? new BlobId(ByteString.copyFrom(ba)) : null;
  }

  private final ByteString fValue;

  public BlobId(ByteString value) {
    Check.that(PlatformUtils.getByteStringSize(value) == SIZE);
    fValue = value;
  }

  public ByteString value() {
    return fValue;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    BlobId other = (BlobId) obj;
    return fValue.equals(other.fValue);
  }

  @Override public int hashCode() {
    return fValue.hashCode();
  }

  public String toHexString() {
    return BaseEncoding.base16().lowerCase().encode(fValue.toByteArray());
  }

  @Override public String toString() {
    return toHexString();
  }
}
