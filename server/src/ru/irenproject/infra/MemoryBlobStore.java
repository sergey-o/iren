/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import com.google.common.io.ByteSource;
import com.google.protobuf.ByteString;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

public final class MemoryBlobStore implements BlobStore {
  private final ConcurrentHashMap<BlobId, ByteString> fMap = new ConcurrentHashMap<>();

  @Override public BlobId put(ByteSource blob) {
    try {
      BlobId res = new BlobId(ByteString.copyFrom(blob.hash(BlobId.HASH_FUNCTION).asBytes()));
      fMap.computeIfAbsent(res, ignored -> Utils.readByteSourceToByteString(blob));
      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override public ByteSource get(BlobId blobId) {
    return Utils.byteStringAsByteSource(Check.notNull(fMap.get(blobId)));
  }
}
