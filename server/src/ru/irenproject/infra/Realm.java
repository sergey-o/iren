/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import javax.annotation.Nullable;

public final class Realm {
  public interface EventSink {
    void post(Resident source, Resident target, Event event);
  }

  public interface GlobalEventSink {
    void postGlobal(Resident source, Event event);
  }

  private final EventSink fEventSink;
  private final @Nullable GlobalEventSink fGlobalEventSink;
  private final BlobStore fBlobStore;

  private long fLastResidentId;

  public Realm(EventSink eventSink, @Nullable GlobalEventSink globalEventSink, BlobStore blobStore) {
    fEventSink = eventSink;
    fGlobalEventSink = globalEventSink;
    fBlobStore = blobStore;
  }

  long generateResidentId() {
    ++fLastResidentId;
    return fLastResidentId;
  }

  void post(Resident source, Resident target, Event event) {
    fEventSink.post(source, target, event);
  }

  void postGlobal(Resident source, Event event) {
    if (fGlobalEventSink != null) {
      fGlobalEventSink.postGlobal(source, event);
    }
  }

  public BlobStore blobStore() {
    return fBlobStore;
  }

  long lastResidentId() {
    return fLastResidentId;
  }

  void setLastResidentId(long value) {
    fLastResidentId = value;
  }
}
