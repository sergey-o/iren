/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.infra;

import javax.annotation.Nullable;
import java.util.Objects;

public final class PostedEvent {
  private final Resident fSource;
  private final Resident fTarget;
  private final Event fEvent;

  public PostedEvent(Resident source, Resident target, Event event) {
    fSource = source;
    fTarget = target;
    fEvent = event;
  }

  public Resident source() {
    return fSource;
  }

  public Resident target() {
    return fTarget;
  }

  public Event event() {
    return fEvent;
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    PostedEvent other = (PostedEvent) obj;

    return Objects.equals(fSource, other.fSource)
        && Objects.equals(fTarget, other.fTarget)
        && Objects.equals(fEvent, other.fEvent);
  }

  @Override public int hashCode() {
    return Objects.hash(fSource, fTarget, fEvent);
  }
}
