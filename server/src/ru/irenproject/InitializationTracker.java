/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import com.google.common.util.concurrent.Uninterruptibles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Singleton public final class InitializationTracker {
  private static final Logger fLogger = LoggerFactory.getLogger(InitializationTracker.class);

  private final Set<Class<?>> fUninitialized = ConcurrentHashMap.newKeySet();
  private final CountDownLatch fLatch;

  @Inject private InitializationTracker(@Initializable Set<Class<?>> initializables) {
    fUninitialized.addAll(initializables);
    fLatch = new CountDownLatch(fUninitialized.size());
  }

  public void await() {
    while (!Uninterruptibles.awaitUninterruptibly(fLatch, 5, TimeUnit.SECONDS)) {
      fLogger.warn("Still waiting for initialization of {}.", fUninitialized.stream()
          .map(Class::getSimpleName)
          .collect(Collectors.toList()));
    }
  }

  public void recordCompletion(Class<?> source) {
    if (fUninitialized.remove(source)) {
      fLatch.countDown();
    } else {
      fLogger.warn("Unexpected completion signal from {}.", source.getSimpleName());
    }
  }
}
