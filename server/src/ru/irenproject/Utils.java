/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Event;
import ru.irenproject.infra.Resident;

import akka.actor.typed.ActorRef;
import akka.actor.typed.javadsl.ActorContext;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.ByteSink;
import com.google.common.io.ByteSource;
import com.google.common.net.InetAddresses;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.UnsignedLongs;
import com.google.protobuf.ByteString;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Message;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.Timestamps;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.stream.ChunkedInput;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.EventExecutorGroup;
import javanet.staxutils.IndentingXMLStreamWriter;
import org.apache.commons.lang3.mutable.MutableObject;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.codehaus.stax2.XMLStreamWriter2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystemException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.time.Clock;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.LongSupplier;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Utils {
  private static final Logger fLogger = LoggerFactory.getLogger(Utils.class);

  public static final int JAVA_EXIT_CODE_ON_OUT_OF_MEMORY_ERROR = 3;

  private static final Clock fMilliClock = Clock.tick(Clock.systemUTC(), Duration.ofMillis(1));

  private static final CharMatcher EXPONENT_INDICATOR_MATCHER = CharMatcher.anyOf("eE");

  private static final Splitter CONFIGURATION_LINE_SPLITTER = Splitter.on('=').limit(2).trimResults();

  public static final ChannelFutureListener CLOSE_ON_ERROR = future -> {
    if (!future.isSuccess()
        && !(future.cause() instanceof ClosingChannelException)
        && !(future.cause() instanceof ClosedChannelException)) {
      fLogger.warn("Closing connection {} due to an error:", channelToString(future.channel()), future.cause());
      future.channel().close();
    }
  };

  private static final byte[] IP4_COLLATION_PREFIX = {1};
  private static final byte[] IP6_COLLATION_PREFIX = {2};
  private static final byte[] BAD_IP_COLLATION_PREFIX = {3};

  public static @Nullable BigDecimal tryParseDecimal(String s) {
    BigDecimal res;
    if (EXPONENT_INDICATOR_MATCHER.matchesAnyOf(s)) {
      res = null;
    } else {
      try {
        res = new BigDecimal(s).stripTrailingZeros();
      } catch (NumberFormatException ignored) {
        res = null;
      }
    }
    return res;
  }

  public static int waitForProcessUninterruptibly(Process process) {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          return process.waitFor();
        } catch (InterruptedException ignored) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public static <T> T callWithRetriesUninterruptibly(Callable<T> callable) throws Exception {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          return callable.call();
        } catch (InterruptedException ignored) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public static void awaitTerminationUninterruptibly(ExecutorService executorService) {
    boolean interrupted = false;
    try {
      while (true) {
        try {
          if (executorService.awaitTermination(5, TimeUnit.SECONDS)) {
            break;
          }
          fLogger.warn("Still waiting for {} to terminate.", executorService, new StackTrace());
        } catch (InterruptedException ignored) {
          interrupted = true;
        }
      }
    } finally {
      if (interrupted) {
        Thread.currentThread().interrupt();
      }
    }
  }

  public static void stopExecutorGroup(EventExecutorGroup group) {
    group.shutdownGracefully(0, 0, TimeUnit.SECONDS);

    while (!group.terminationFuture().awaitUninterruptibly(5, TimeUnit.SECONDS)) {
      fLogger.warn("Still waiting for {} to terminate.", group, new StackTrace());
    }
  }

  public static @Nullable String replaceAllIfFound(String source, Pattern pattern, String replacement) {
    Matcher m = pattern.matcher(source);
    String res;
    if (m.find()) {
      StringBuffer sb = new StringBuffer();

      do {
        m.appendReplacement(sb, replacement);
      } while (m.find());

      m.appendTail(sb);
      res = sb.toString();
    } else {
      res = null;
    }
    return res;
  }

  public static String channelToString(Channel channel) {
    String a = getClientAddressIfKnown(channel);
    return String.format("[%s%s#%08x]", Strings.nullToEmpty(a), (a == null) ? "" : " ", channel.hashCode());
  }

  public static @Nullable String getClientAddressIfKnown(Channel channel) {
    String res = channel.attr(OriginalClientAddressKey.INSTANCE).get();
    if ((res == null) && (channel.remoteAddress() instanceof InetSocketAddress)) {
      InetAddress a = ((InetSocketAddress) channel.remoteAddress()).getAddress();
      if (a != null) {
        res = InetAddresses.toAddrString(a);
      }
    }
    return res;
  }

  public static void setOriginalClientAddress(Channel channel, String address) {
    Check.that(channel.attr(OriginalClientAddressKey.INSTANCE).setIfAbsent(address) == null);
  }

  private static final class OriginalClientAddressKey {
    static final AttributeKey<String> INSTANCE = AttributeKey.newInstance(OriginalClientAddressKey.class.getName());
  }

  public static void deleteShallowDirectory(Path directory) {
    try {
      for (Path path : listDirectory(directory)) {
        Files.deleteIfExists(path);
      }
      Files.deleteIfExists(directory);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static List<Path> listDirectory(Path directory) {
    List<Path> res;

    try (Stream<Path> stream = Files.list(directory)) {
      res = stream.collect(Collectors.toList());
    } catch (NoSuchFileException ignored) {
      res = Collections.emptyList();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    return res;
  }

  public static void tryDeleteFile(Path file) {
    try {
      Files.delete(file);
    } catch (IOException e) {
      fLogger.warn("", e);
    }
  }

  public static Path externalResourcePath() {
    try {
      Path jarFileOrClassDir = Paths.get(Utils.class.getProtectionDomain().getCodeSource().getLocation().toURI());
      return jarFileOrClassDir.resolve(Files.isDirectory(jarFileOrClassDir) ? "../../.." : "..").normalize();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }

  public static ByteSource byteStringAsByteSource(ByteString byteString) {
    return new ByteStringBasedByteSource(byteString);
  }

  private static final class ByteStringBasedByteSource extends ByteSource {
    final ByteString fByteString;

    ByteStringBasedByteSource(ByteString byteString) {
      fByteString = byteString;
    }

    @Override public InputStream openStream() {
      return fByteString.newInput();
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    @Override public long size() {
      return fByteString.size();
    }
  }

  public static ByteString readByteSourceToByteString(ByteSource byteSource) {
    try {
      ByteString res;
      if (byteSource instanceof ByteStringBasedByteSource) {
        res = ((ByteStringBasedByteSource) byteSource).fByteString;
      } else {
        try (InputStream inputStream = byteSource.openStream()) {
          res = ByteString.readFrom(inputStream, (int) Long.min(byteSource.size(), 100 * 1024 * 1024));
        }
      }
      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void setLoggerLevel(String loggerName, @Nullable Level level) {
    ((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(loggerName).setLevel(level);
  }

  @SuppressWarnings("UnusedReturnValue")
  public static int readFromSystemIn() {
    try {
      return System.in.read();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static byte[] readOrGenerateKey(@Nullable Path keyFile) {
    try {
      byte[] res;
      if (keyFile == null) {
        res = new byte[32];
        new SecureRandom().nextBytes(res);
      } else {
        res = Files.readAllBytes(keyFile);
        Check.that(res.length > 0, () -> Oops.format("Key file '%s' is empty.", keyFile));
      }
      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void logUnexpectedEvent(Resident source, Resident target, Event event) {
    fLogger.warn("Unexpected event {} ({} -> {}).", event.getClass().getName(), source, target);
  }

  public static String getTypeUrl(Descriptor d) {
    return '/' + d.getFullName();
  }

  public static Path changeFileName(Path path, UnaryOperator<String> mapper) {
    Path fileName = path.getFileName();
    return path.resolveSibling(mapper.apply((fileName == null) ? "" : fileName.toString()));
  }

  public static void writeXml(OutputStream out, Consumer<XMLStreamWriter> generator) {
    try {
      MutableObject<XMLStreamWriter> w = new MutableObject<>(
          XMLOutputFactory.newInstance().createXMLStreamWriter(out));
      try (ClosableWithXMLStreamException ignored = () -> w.getValue().close()) {
        ((XMLStreamWriter2) w.getValue()).writeRaw("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n");
        w.setValue(new IndentingXMLStreamWriter(w.getValue()));

        generator.accept(w.getValue());

        w.getValue().writeCharacters("\n");
        w.getValue().writeEndDocument();
      }
    } catch (XMLStreamException e) {
      throw new RuntimeException(e);
    }
  }

  private interface ClosableWithXMLStreamException extends AutoCloseable {
    @Override void close() throws XMLStreamException;
  }

  public static Timestamp instantToTimestamp(Instant instant) {
    return Timestamps.checkValid(Timestamp.newBuilder()
        .setSeconds(instant.getEpochSecond())
        .setNanos(instant.getNano())
        .build());
  }

  public static Instant timestampToInstant(Timestamp timestamp) {
    return Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos());
  }

  public static ZoneId parseTimeZone(
      String timeZone,
      Function<? super DateTimeException, ? extends RuntimeException> exceptionConverter) {
    try {
      return ZoneId.of(timeZone);
    } catch (DateTimeException e) {
      throw exceptionConverter.apply(e);
    }
  }

  public static Clock milliClock() {
    return fMilliClock;
  }

  public static void tryCreateAncestorDirectories(Path path) {
    if (path.getParent() != null) {
      try {
        Files.createDirectories(path.getParent());
      } catch (IOException ignored) {
        // among others, ignore symlink-related FileAlreadyExistsException
        // (https://bugs.openjdk.java.net/browse/JDK-8130464)
      }
    }
  }

  public static void renameFilePreferablyAtomically(Path source, Path target) {
    try {
      try {
        Files.move(source, target, StandardCopyOption.ATOMIC_MOVE);
      } catch (FileSystemException ignored) {
        renameFileIfAtomicMoveFailed(source, target);
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  // on Windows can do a rename even if target is currently open (with FILE_SHARE_DELETE)
  private static void renameFileIfAtomicMoveFailed(Path source, Path target) throws IOException {
    Path temp = changeFileName(target, Utils::generateTempFileName);
    Files.move(target, temp, StandardCopyOption.ATOMIC_MOVE);

    try {
      Files.move(source, target, StandardCopyOption.ATOMIC_MOVE);
    } catch (IOException e) {
      try {
        Files.move(temp, target, StandardCopyOption.ATOMIC_MOVE);
      } catch (IOException ignored) {}
      throw e;
    }

    Files.deleteIfExists(temp);
  }

  public static String generateTempFileName(String prefix) {
    return String.format("%s~%016x", prefix, new SecureRandom().nextLong());
  }

  public static byte[] getIpAddressCollationKey(String address) {
    InetAddress a;
    try {
      a = InetAddresses.forString(address);
    } catch (IllegalArgumentException ignored) {
      a = null;
    }

    byte[] res;
    if (a instanceof Inet4Address) {
      res = Bytes.concat(IP4_COLLATION_PREFIX, a.getAddress());
    } else if (a instanceof Inet6Address) {
      res = Bytes.concat(IP6_COLLATION_PREFIX, a.getAddress());
    } else {
      res = Bytes.concat(BAD_IP_COLLATION_PREFIX, address.getBytes(StandardCharsets.UTF_8));
    }
    return res;
  }

  public static @Nullable String getOptionValue(String optionName, String... options) {
    String prefix = optionName + '=';
    return Arrays.stream(options)
        .filter(s -> s.startsWith(prefix))
        .findFirst()
        .map(s -> s.substring(prefix.length()))
        .orElse(null);
  }

  public static ImmutableMap<String, String> readConfigurationFile(Path file) {
    try {
      ImmutableMap.Builder<String, String> b = ImmutableMap.builder();

      for (String line : Files.readAllLines(file, StandardCharsets.UTF_8)) {
        String s = CharMatcher.whitespace().trimLeadingFrom(line);
        if (!s.startsWith("#")) {
          List<String> keyValue = CONFIGURATION_LINE_SPLITTER.splitToList(s);
          if (keyValue.size() == 2) {
            b.put(keyValue.get(0), keyValue.get(1));
          }
        }
      }

      return b.build();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static boolean parseBooleanOption(String value, String errorMessage) {
    switch (value) {
      case "yes": return true;
      case "no": return false;
      default: throw new RuntimeException(errorMessage);
    }
  }

  public static byte[] pbkdf2Sha256(char[] key, byte[] salt, int iterationCount) {
    try {
      PBEKeySpec keySpec = new PBEKeySpec(key, salt, iterationCount, 256);
      return SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256").generateSecret(keySpec).getEncoded();
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      throw new RuntimeException(e);
    }
  }

  public static char[] readCharBuffer(CharBuffer buffer) {
    char[] res = new char[buffer.remaining()];
    buffer.get(res);
    return res;
  }

  public static <S, T> ChunkedInput<T> transformChunkedInput(ChunkedInput<S> source,
      Function<? super S, ? extends T> transformer) {
    return new ChunkedInput<>() {
      @Override public boolean isEndOfInput() throws Exception {
        return source.isEndOfInput();
      }

      @Override public void close() throws Exception {
        source.close();
      }

      @Override @Deprecated public T readChunk(ChannelHandlerContext ctx) throws Exception {
        return readChunk(ctx.alloc());
      }

      @Override public T readChunk(ByteBufAllocator allocator) throws Exception {
        S s = source.readChunk(allocator);
        return (s == null) ? null : transformer.apply(s);
      }

      @Override public long length() {
        return source.length();
      }

      @Override public long progress() {
        return source.progress();
      }
    };
  }

  public static ByteBuf messageToDirectByteBuf(Message message, ByteBufAllocator allocator) {
    int messageSize = message.getSerializedSize();
    ByteBuf res = allocator.directBuffer(messageSize);

    boolean ok = false;
    try {
      CodedOutputStream out = CodedOutputStream.newInstance(res.nioBuffer(0, messageSize));
      message.writeTo(out);
      out.flush();
      out.checkNoSpaceLeft();
      res.writerIndex(messageSize);
      ok = true;
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      if (!ok) {
        res.release();
      }
    }

    return res;
  }

  public static @Nullable Long tryParseUnsignedLong(String s, int radix) {
    Long res;
    try {
      // don't use Long.parseUnsignedLong because of JDK-8030814
      res = UnsignedLongs.parseUnsignedLong(s, radix);
    } catch (NumberFormatException ignored) {
      res = null;
    }
    return res;
  }

  public static <T> void removeFirstWhere(Iterable<T> iterable, Predicate<? super T> predicate) {
    Iterator<T> iterator = iterable.iterator();
    while (iterator.hasNext()) {
      if (predicate.test(iterator.next())) {
        iterator.remove();
        break;
      }
    }
  }

  public static <E> void toggle(Set<E> set, E e) {
    if (!set.add(e)) {
      set.remove(e);
    }
  }

  public static <E> void setPresence(Set<E> set, E e, boolean presence) {
    if (presence) {
      set.add(e);
    } else {
      set.remove(e);
    }
  }

  public static long generateRandomLong(long range, LongSupplier bitGenerator) {
    long res;

    if (range >= 2) {
      long lastCompleteRangeStart = Long.MAX_VALUE - range + 1;
      long n;
      do {
        n = bitGenerator.getAsLong() >>> 1;
        res = n % range;
      } while (n - res > lastCompleteRangeStart);
    } else {
      res = 0;
    }

    return res;
  }

  public static String replaceCharAt(String s, int index, char newChar) {
    char[] chars = s.toCharArray();
    chars[index] = newChar;
    return new String(chars);
  }

  public static boolean bitSetIsSubset(BitSet candidateSubset, BitSet set) {
    return bitSetAnd(candidateSubset, set).equals(candidateSubset);
  }

  public static BitSet bitSetAnd(BitSet a, BitSet b) {
    BitSet res = (BitSet) a.clone();
    res.and(b);
    return res;
  }

  public static BitSet bitSetOr(BitSet a, BitSet b) {
    BitSet res = (BitSet) a.clone();
    res.or(b);
    return res;
  }

  public static BitSet bitSetAndNot(BitSet a, BitSet b) {
    BitSet res = (BitSet) a.clone();
    res.andNot(b);
    return res;
  }

  public static ThreadFactory createRawThreadFactory(long stackSize) {
    ThreadGroup threadGroup = Thread.currentThread().getThreadGroup();
    return r -> {
      Thread res = new Thread(threadGroup, r, "", stackSize);
      res.setDaemon(false);
      res.setPriority(Thread.NORM_PRIORITY);
      return res;
    };
  }

  public static String makeUniqueChildActorName(String baseName, ActorContext<?> ctx) {
    String res = baseName;
    long n = 0;
    while (ctx.getChild(res).isPresent()) {
      ++n;
      res = baseName + '-' + n;
    }
    return res;
  }

  public static <T> void broadcast(T message, Iterable<? extends ActorRef<? super T>> targets) {
    for (ActorRef<? super T> target : targets) {
      target.tell(message);
    }
  }

  public static <T> GenericObjectPoolConfig<T> unboundedPoolConfig() {
    GenericObjectPoolConfig<T> res = new GenericObjectPoolConfig<>();
    res.setJmxEnabled(false);
    res.setMaxTotal(-1);
    res.setMaxIdle(-1);

    return res;
  }

  public static void freeze(Object object, Kryo kryo, ByteSink out) {
    try (
        OutputStream outputStream = out.openStream();
        Output kryoOutput = new Output(outputStream)) {
      kryo.writeObject(kryoOutput, object);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static <T> T unfreeze(ByteSource in, Kryo kryo, Class<T> objectClass) {
    try (
        InputStream inputStream = in.openStream();
        Input kryoInput = new Input(inputStream)) {
      return kryo.readObject(kryoInput, objectClass);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private Utils() {}
}
