/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.formula;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import com.google.common.util.concurrent.Futures;
import com.google.inject.BindingAnnotation;
import com.google.protobuf.ByteString;
import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.NashornScriptEngineFactory;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.script.CompiledScript;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@SuppressWarnings("removal")
@Singleton public final class FormulaRenderer {
  @Target(ElementType.PARAMETER)
  @Retention(RetentionPolicy.RUNTIME)
  @BindingAnnotation public @interface MaxIdleContexts {}

  private static final Logger fLogger = LoggerFactory.getLogger(FormulaRenderer.class);

  private static final long RENDERER_THREAD_STACK_SIZE = 4 * 1024 * 1024;

  private final GenericObjectPool<FormulaRenderContext> fContextPool;

  private final ExecutorService fExecutor = Executors.newCachedThreadPool(new BasicThreadFactory.Builder()
      .wrappedFactory(Utils.createRawThreadFactory(RENDERER_THREAD_STACK_SIZE))
      .namingPattern("formulaRenderer-%d")
      .daemon(true)
      .build());

  @Inject private FormulaRenderer() {
    fContextPool = new GenericObjectPool<>(new ContextFactory(), Utils.unboundedPoolConfig());
  }

  @com.google.inject.Inject(optional = true)
  private void setMaxIdleContexts(@MaxIdleContexts int value) {
    fContextPool.setMaxIdle(value);
  }

  public FormulaImage render(String source) {
    FormulaRenderContext renderContext;
    try {
      renderContext = Utils.callWithRetriesUninterruptibly(fContextPool::borrowObject);
    } catch (Exception anyButInterruptedException) {
      throw new RuntimeException(anyButInterruptedException);
    }

    boolean ok = false;
    try {
      RenderResult result = Futures.getUnchecked(fExecutor.submit(new RenderTask(source, renderContext)));
      ok = result.ok;
      return result.formulaImage;
    } finally {
      if (ok) {
        fContextPool.returnObject(renderContext);
      } else {
        invalidate(renderContext);
      }
    }
  }

  private void invalidate(FormulaRenderContext renderContext) {
    try {
      fContextPool.invalidateObject(renderContext);
    } catch (InterruptedException ignored) { // should not happen
      // no retries, renderContext is presumed to have been invalidated
      Thread.currentThread().interrupt();
    } catch (Exception e) { // should not happen
      fLogger.warn("", e);
    }
  }

  private static final class RenderResult {
    final FormulaImage formulaImage;
    final boolean ok;

    RenderResult(FormulaImage formulaImage, boolean ok) {
      this.formulaImage = formulaImage;
      this.ok = ok;
    }
  }

  private static final class ContextFactory extends BasePooledObjectFactory<FormulaRenderContext> {
    static final ImmutableList<String> SCRIPT_NAMES = scriptNames();

    final NashornScriptEngine fEngine;
    final ImmutableList<CompiledScript> fCompiledScripts;

    ContextFactory() {
      fEngine = (NashornScriptEngine) new NashornScriptEngineFactory().getScriptEngine(
          "--no-java", "--no-deprecation-warning", "--optimistic-types=false");

      fCompiledScripts = SCRIPT_NAMES.stream()
          .map(name -> compile(name, fEngine))
          .collect(ImmutableList.toImmutableList());
    }

    static ImmutableList<String> scriptNames() {
      try {
        ImmutableList.Builder<String> scriptNames = ImmutableList.builder();
        scriptNames.add("formula.js");
        for (String mathjaxFile : Resources.readLines(Resources.getResource(FormulaRenderer.class, "mathjaxFiles"),
            StandardCharsets.UTF_8)) {
          scriptNames.add("mathjax/" + mathjaxFile);
        }
        return scriptNames.build();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }

    static CompiledScript compile(String scriptName, NashornScriptEngine engine) {
      CharSource source = CharSource.concat(
          Resources.asCharSource(Resources.getResource(FormulaRenderer.class, scriptName), StandardCharsets.UTF_8),
          CharSource.wrap("\n//# sourceURL=" + scriptName));
      try (BufferedReader reader = source.openBufferedStream()) {
        return engine.compile(reader);
      } catch (IOException | ScriptException e) {
        throw new RuntimeException(e);
      }
    }

    @Override public FormulaRenderContext create() {
      return new FormulaRenderContext(fEngine, SCRIPT_NAMES, fCompiledScripts);
    }

    @Override public PooledObject<FormulaRenderContext> wrap(FormulaRenderContext obj) {
      return new DefaultPooledObject<>(obj);
    }
  }

  private static final class RenderTask implements Callable<RenderResult> {
    final String fSource;
    final FormulaRenderContext fRenderContext;

    RenderTask(String source, FormulaRenderContext renderContext) {
      fSource = source;
      fRenderContext = renderContext;
    }

    @Override public RenderResult call() {
      ByteString.Output out = ByteString.newOutput();
      LinkedHashMap<String, String> style = new LinkedHashMap<>();

      boolean ok = toSvg(out, style);

      return new RenderResult(new FormulaImage(out.toByteString(), ImmutableMap.copyOf(style)), ok);
    }

    boolean toSvg(OutputStream out, Map<String, String> styleOut) {
      boolean res;

      if (fSource.isEmpty()) {
        emitPlaceholderSvg(true, out, styleOut);
        res = true;
      } else {
        Element svg;
        try {
          svg = toSvgElement(fSource, styleOut);
        } catch (RuntimeException e) {
          fLogger.trace("", e);
          svg = null;
        }

        res = (svg != null);

        if (res) {
          saveSvg(svg, out);
        } else {
          styleOut.clear();
          emitPlaceholderSvg(false, out, styleOut);
        }
      }

      return res;
    }

    Element toSvgElement(String source, Map<String, String> styleOut) {
      ScriptObjectMirror resultArray = (ScriptObjectMirror) fRenderContext.renderFunction().call(null, source);

      Element res = (Element) resultArray.getSlot(0);
      res.removeAttribute("xmlns:xlink");
      res.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");

      styleOut.putAll(Maps.transformValues((ScriptObjectMirror) resultArray.getSlot(1), String.class::cast));

      return res;
    }

    static void saveSvg(Element svg, OutputStream out) {
      DOMImplementationLS domSaveImpl = (DOMImplementationLS) svg.getOwnerDocument().getImplementation();
      LSSerializer serializer = domSaveImpl.createLSSerializer();

      LSOutput output = domSaveImpl.createLSOutput();
      output.setByteStream(out);
      output.setEncoding("utf-8");

      Check.that(serializer.write(svg, output));
    }

    static void emitPlaceholderSvg(boolean transparent, OutputStream out, Map<String, String> styleOut) {
      try {
        out.write(FormulaRendererLike.buildPlaceholderSvg(transparent).getBytes(StandardCharsets.UTF_8));
        styleOut.putAll(FormulaRendererLike.PLACEHOLDER_STYLE);
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
