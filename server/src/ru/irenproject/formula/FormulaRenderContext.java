/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.formula;

import jdk.nashorn.api.scripting.NashornScriptEngine;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import org.w3c.dom.Document;

import javax.script.Bindings;
import javax.script.CompiledScript;
import javax.script.ScriptException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

@SuppressWarnings("removal")
public final class FormulaRenderContext {
  private final Bindings fGlobalObject;
  private final ScriptObjectMirror fRenderFunction;

  public FormulaRenderContext(NashornScriptEngine engine, List<String> scriptNames,
      List<CompiledScript> compiledScripts) {
    try {
      fGlobalObject = engine.createBindings();

      Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
      fGlobalObject.put("nativeDocument", document);

      for (int size = scriptNames.size(), i = 0; i < size; ++i) {
        if (scriptNames.get(i).equals("mathjax/jax/output/SVG/fonts/TeX/fontdata.js")) {
          engine.eval("onload()", fGlobalObject);
        }
        compiledScripts.get(i).eval(fGlobalObject);
      }

      engine.eval("postInitialize()", fGlobalObject);
      fRenderFunction = (ScriptObjectMirror) fGlobalObject.get("render");
    } catch (ParserConfigurationException | ScriptException e) {
      throw new RuntimeException(e);
    }
  }

  public ScriptObjectMirror renderFunction() {
    return fRenderFunction;
  }
}
