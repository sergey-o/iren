/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.formula;

import ru.irenproject.Check;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.primitives.Ints;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton public final class CachingFormulaRenderer implements FormulaRendererLike {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("formulaRenderer.cacheSize")
    String cacheSize = "4m";
  }

  private final Provider<FormulaRenderer> fFormulaRendererProvider;
  private final LoadingCache<String, FormulaImage> fCache;

  @Inject private CachingFormulaRenderer(
      Config config,
      Provider<FormulaRenderer> formulaRendererProvider) {
    fFormulaRendererProvider = formulaRendererProvider;
    fCache = CacheBuilder.newBuilder()
        .softValues()
        .weigher((String key, FormulaImage value) -> value.svg().size())
        .maximumWeight((long) parseCacheSize(config.cacheSize) * 1024 * 1024)
        .recordStats()
        .build(CacheLoader.from(this::renderUncached));
  }

  private static int parseCacheSize(String s) {
    Check.that(s.endsWith("m"), BadCacheSizeException::new);
    int res = Check.notNull(Ints.tryParse(s.substring(0, s.length() - 1)), BadCacheSizeException::new);
    Check.that(res >= 1, BadCacheSizeException::new);
    return res;
  }

  private static final class BadCacheSizeException extends RuntimeException {
    BadCacheSizeException() {
      super("Incorrect 'formulaRenderer.cacheSize' value.");
    }
  }

  @Override public FormulaImage render(String source) {
    return fCache.getUnchecked(source);
  }

  private FormulaImage renderUncached(String source) {
    return fFormulaRendererProvider.get().render(source);
  }

  public CacheStats stats() {
    return fCache.stats();
  }
}
