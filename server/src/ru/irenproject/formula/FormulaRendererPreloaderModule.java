/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.formula;

import com.google.inject.AbstractModule;

import javax.inject.Inject;
import javax.inject.Provider;

public final class FormulaRendererPreloaderModule extends AbstractModule {
  @Override protected void configure() {
    bind(Preloader.class).asEagerSingleton();
  }

  private static final class Preloader {
    @Inject Preloader(Provider<FormulaRenderer> formulaRendererProvider) {
      Thread thread = new Thread(() -> preload(formulaRendererProvider), "formulaRendererPreloader");
      thread.setDaemon(true);
      thread.start();
    }

    static void preload(Provider<FormulaRenderer> formulaRendererProvider) {
      formulaRendererProvider.get().render("x");
    }
  }
}
