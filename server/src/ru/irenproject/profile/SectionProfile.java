/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.profile;

import ru.irenproject.BaseUtils;
import ru.irenproject.Check;
import ru.irenproject.Section;
import ru.irenproject.infra.Freezable;

import com.google.common.base.MoreObjects;
import com.google.common.collect.ImmutableMap;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Freezable public final class SectionProfile {
  public static SectionProfile createForTree(Section section,
      Map<Long, /* @Nullable */QuestionCount> questionCountsBySectionId) {
    return new SectionProfile(section.name(), section.id(), questionCountsBySectionId.get(section.id()),
        section.sections().stream()
            .map(s -> createForTree(s, questionCountsBySectionId))
            .collect(Collectors.toList()));
  }

  private final String fSectionName;
  private final @Nullable Long fSectionId;
  private final @Nullable QuestionCount fQuestionCount;
  private final ArrayList<SectionProfile> fSectionProfiles;

  public SectionProfile(String sectionName, @Nullable Long sectionId, @Nullable QuestionCount questionCount,
      Collection<SectionProfile> sectionProfiles) {
    fSectionName = sectionName;
    fSectionId = sectionId;
    fQuestionCount = questionCount;
    fSectionProfiles = new ArrayList<>(sectionProfiles);
  }

  public String sectionName() {
    return fSectionName;
  }

  public @Nullable QuestionCount questionCount() {
    return fQuestionCount;
  }

  public List<SectionProfile> sectionProfiles() {
    return Collections.unmodifiableList(fSectionProfiles);
  }

  public Map<Long, SectionProfile> profilesBySectionId() {
    HashMap<Long, SectionProfile> res = new HashMap<>();
    getProfilesBySectionId(res);
    return res;
  }

  private void getProfilesBySectionId(Map<Long, SectionProfile> out) {
    if (fSectionId != null) {
      out.put(fSectionId, this);
    }
    for (SectionProfile child : fSectionProfiles) {
      child.getProfilesBySectionId(out);
    }
  }

  public ImmutableMap<Long, QuestionCount> effectiveQuestionCountsBySectionId() {
    ImmutableMap.Builder<Long, QuestionCount> b = ImmutableMap.builder();
    getEffectiveQuestionCountsBySectionId(QuestionCount.ZERO, b);
    return b.build();
  }

  private void getEffectiveQuestionCountsBySectionId(
      QuestionCount inheritedQuestionCount,
      ImmutableMap.Builder<Long, QuestionCount> out) {
    QuestionCount effectiveQuestionCount = MoreObjects.firstNonNull(fQuestionCount, inheritedQuestionCount);
    out.put(Check.notNull(fSectionId), effectiveQuestionCount);

    for (SectionProfile child : fSectionProfiles) {
      child.getEffectiveQuestionCountsBySectionId(effectiveQuestionCount, out);
    }
  }

  public boolean sameAs(SectionProfile other) {
    return Objects.equals(fSectionName, other.fSectionName)
        && Objects.equals(fSectionId, other.fSectionId)
        && Objects.equals(fQuestionCount, other.fQuestionCount)
        && BaseUtils.iterablesEqual(fSectionProfiles, other.fSectionProfiles, SectionProfile::sameAs);
  }
}
