/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.profile;

import ru.irenproject.PlatformUtils;
import ru.irenproject.TestCheck;
import ru.irenproject.TestUtils;
import ru.irenproject.common.profile.Proto.MarkScaleData;
import ru.irenproject.infra.Freezable;

import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;
import java.math.BigDecimal;

@Freezable @Immutable public final class MarkScale {
  public static final int MAX_MARKS = 20;
  public static final String ZERO_LOWER_BOUND = "0.0000";
  public static final MarkScale EMPTY = new MarkScale(MarkScaleData.getDefaultInstance());

  private final MarkScaleData fData;
  private final BigDecimal[] fLowerBounds;

  public MarkScale(MarkScaleData data) {
    fData = data;
    TestCheck.input(PlatformUtils.isInitializedOrTrueIfUnsupported(fData));

    int n = fData.getMarkCount();
    TestCheck.input(n <= MAX_MARKS);

    fLowerBounds = new BigDecimal[n];
    for (int i = 0; i < n; ++i) {
      BigDecimal b = TestUtils.parseNormalizedDecimal(fData.getMark(i).getLowerBound());
      TestCheck.input((i == 0) ? b.signum() == 0 : fLowerBounds[i - 1].compareTo(b) <= 0);
      fLowerBounds[i] = b;
    }
  }

  public MarkScaleData data() {
    return fData;
  }

  public int markCount() {
    return fData.getMarkCount();
  }

  public boolean isEmpty() {
    return fData.getMarkCount() == 0;
  }

  public String getTitle(int index) {
    return fData.getMark(index).getTitle();
  }

  public BigDecimal getLowerBound(int index) {
    return fLowerBounds[index];
  }

  public @Nullable Integer getMarkIndexForResult(BigDecimal result) {
    Integer res = null;
    for (int i = 0; i < fData.getMarkCount(); ++i) {
      if (result.compareTo(fLowerBounds[i]) >= 0) {
        res = i;
      } else {
        break;
      }
    }
    return res;
  }

  public String getTitleForResult(BigDecimal result) {
    Integer index = getMarkIndexForResult(result);
    return (index == null) ? "" : getTitle(index);
  }
}
