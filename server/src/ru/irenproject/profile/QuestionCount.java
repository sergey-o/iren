/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.profile;

import ru.irenproject.BaseUtils;
import ru.irenproject.Check;
import ru.irenproject.TestCheck;
import ru.irenproject.TestInputException;
import ru.irenproject.infra.Freezable;

import javax.annotation.Nullable;
import java.util.Objects;

@Freezable public final class QuestionCount {
  public static final QuestionCount ZERO = new QuestionCount(0, false);
  public static final QuestionCount ONE_HUNDRED_PERCENT = new QuestionCount(100, true);

  public static QuestionCount fromText(String text) {
    boolean percent = text.endsWith("%");
    int value = Check.notNull(BaseUtils.tryParseIntegerAmount(percent ? text.substring(0, text.length() - 1) : text),
        TestInputException::new);
    return new QuestionCount(value, percent);
  }

  private final int fValue;
  private final boolean fPercent;

  public QuestionCount(int value, boolean percent) {
    TestCheck.input(value >= 0);
    TestCheck.input(value <= (percent ? 100 : 999));
    fValue = value;
    fPercent = percent;
  }

  public int value() {
    return fValue;
  }

  public boolean percent() {
    return fPercent;
  }

  public String text() {
    String s = Integer.toString(fValue);
    return fPercent ? s + '%' : s;
  }

  @Override public String toString() {
    return text();
  }

  @Override public boolean equals(@Nullable Object obj) {
    if (this == obj) {
      return true;
    }

    if ((obj == null) || (getClass() != obj.getClass())) {
      return false;
    }

    QuestionCount other = (QuestionCount) obj;
    return (fValue == other.fValue) && (fPercent == other.fPercent);
  }

  @Override public int hashCode() {
    return Objects.hash(fValue, fPercent);
  }
}
