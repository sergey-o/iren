/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.profile;

import ru.irenproject.ChangeContentEvent;
import ru.irenproject.TestCheck;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProfileList extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public static final class AddEvent implements Event {
    private final Profile fProfile;

    private AddEvent(Profile profile) {
      fProfile = profile;
    }

    public Profile profile() {
      return fProfile;
    }
  }

  public static final class RemoveEvent implements Event {
    private final Profile fProfile;

    private RemoveEvent(Profile profile) {
      fProfile = profile;
    }

    public Profile profile() {
      return fProfile;
    }
  }

  private final ArrayList<Profile> fProfiles = new ArrayList<>();

  public ProfileList(Realm realm) {
    super(realm);
  }

  public void add(Profile profile) {
    fProfiles.add(profile);
    post(new AddEvent(profile));
    post(ChangeEvent.INSTANCE);
  }

  public List<Profile> profiles() {
    return Collections.unmodifiableList(fProfiles);
  }

  public boolean isEmpty() {
    return fProfiles.isEmpty();
  }

  public void delete(int index) {
    checkIndex(index);

    post(new RemoveEvent(fProfiles.remove(index)));
    post(ChangeEvent.INSTANCE);
  }

  public void clear() {
    while (!isEmpty()) {
      delete(fProfiles.size() - 1);
    }
  }

  public void move(int index, boolean forward) {
    checkIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkIndex(newIndex);

    Collections.swap(fProfiles, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  private void checkIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fProfiles.size());
  }
}
