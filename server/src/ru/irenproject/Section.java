/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;

import javax.annotation.Nullable;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class Section extends Resident {
  public enum ChangeEvent implements ChangeContentEvent { INSTANCE }

  public enum ChangeTreeEvent implements ChangeContentEvent { INSTANCE }

  public static final class RemoveSectionEvent implements Event {
    private final Section fSection;
    private final Section fParent;
    private final int fIndex;

    private RemoveSectionEvent(Section section, Section parent, int index) {
      fSection = section;
      fParent = parent;
      fIndex = index;
    }

    public Section section() {
      return fSection;
    }

    public Section parent() {
      return fParent;
    }

    public int index() {
      return fIndex;
    }
  }

  public static final int MAX_NAME_LENGTH = 100;

  private @Nullable Section fParent;
  private String fName = "";
  private final QuestionList fQuestionList;
  private final ModifierList fModifierList;
  private final ArrayList<Section> fSections = new ArrayList<>();

  public Section(Realm realm) {
    super(realm);
    fQuestionList = new QuestionList(realm);
    fModifierList = new ModifierList(realm);
  }

  public @Nullable Section parent() {
    return fParent;
  }

  public String name() {
    return fName;
  }

  public void setName(String name) {
    TestCheck.input(validName(name));

    fName = name;
    post(ChangeEvent.INSTANCE);
  }

  private static boolean validName(String name) {
    return name.length() <= MAX_NAME_LENGTH;
  }

  public QuestionList questionList() {
    return fQuestionList;
  }

  public ModifierList modifierList() {
    return fModifierList;
  }

  public List<Section> sections() {
    return Collections.unmodifiableList(fSections);
  }

  public void addSection(Section section) {
    Check.that(section.fParent == null);
    fSections.add(section);
    section.fParent = this;

    root().post(ChangeTreeEvent.INSTANCE);
  }

  public void removeSection(Section section) {
    int index = fSections.indexOf(section);
    if (index != -1) {
      Section root = root();
      removeSectionInternal(index, root);
      root.post(ChangeTreeEvent.INSTANCE);
    }
  }

  private void removeSectionInternal(int index, Section root) {
    Section section = fSections.get(index);

    for (int i = section.fSections.size() - 1; i >= 0; --i) {
      section.removeSectionInternal(i, root);
    }

    fSections.remove(index);
    section.fParent = null;
    root.post(new RemoveSectionEvent(section, this, index));
  }

  public Section root() {
    Section res = this;
    while (res.fParent != null) {
      res = res.fParent;
    }
    return res;
  }

  public void moveTo(Section target, int index) {
    Check.notNull(fParent).fSections.remove(this);
    target.fSections.add(index, this);
    fParent = target;

    root().post(ChangeTreeEvent.INSTANCE);
  }

  public List<Section> listTreeSections() {
    ArrayList<Section> res = new ArrayList<>();
    listTreeSectionsInto(res);
    return res;
  }

  private void listTreeSectionsInto(Collection<Section> out) {
    out.add(this);
    for (Section section : fSections) {
      section.listTreeSectionsInto(out);
    }
  }

  public List<QuestionItem> listTreeQuestions() {
    return listTreeSections().stream()
        .flatMap(s -> s.fQuestionList.items().stream())
        .collect(Collectors.toList());
  }

  public BigInteger maxNumericTag() {
    BigInteger res = BigInteger.ZERO;

    for (QuestionItem item : fQuestionList.items()) {
      BigInteger n = TestUtils.toNumericTagIfPossible(item.tag());
      if (n != null) {
        res = res.max(n);
      }
    }

    for (Section section : fSections) {
      res = res.max(section.maxNumericTag());
    }

    return res;
  }
}
