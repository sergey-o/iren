/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;

import java.nio.charset.StandardCharsets;
import java.util.Collections;

public final class LibSystemd {
  static {
    System.setProperty("jna.nosys", Boolean.toString(true));
    Native.register(NativeLibrary.getInstance("libsystemd.so.0",
        Collections.singletonMap(Library.OPTION_STRING_ENCODING, StandardCharsets.UTF_8.name())));
  }

  public static native int sd_notify(int unset_environment, String state);

  private LibSystemd() {}
}
