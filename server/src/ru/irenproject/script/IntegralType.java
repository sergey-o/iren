/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public final class IntegralType extends Type {
  private static final Long INITIAL_VALUE = 0L;

  private final Assigner fAssignerFromIntegral;
  private final Assigner fAssignerFromReal;

  private final long fMinValue;
  private final long fMaxValue;

  public IntegralType(boolean signed, long mask) {
    if (~mask == 0) {
      Check.that(signed);
      fAssignerFromIntegral = (source, target) -> target.set(source);
    } else {
      ConverterFromIntegral converter = new ConverterFromIntegral(signed, mask);
      fAssignerFromIntegral = (source, target) -> target.set(converter.convert((Long) source));
    }

    fMinValue = signed ? (~mask >>> 1) | Long.MIN_VALUE : 0;
    fMaxValue = signed ? mask >>> 1 : mask;

    ConverterFromReal converterFromReal = new ConverterFromReal(fMinValue, fMaxValue);
    fAssignerFromReal = (source, target) -> target.set(converterFromReal.convert((Double) source));
  }

  public long minValue() {
    return fMinValue;
  }

  public long maxValue() {
    return fMaxValue;
  }

  @Override public Supplier<Long> factory() {
    return () -> INITIAL_VALUE;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    Assigner res;
    if (sourceType == this) {
      res = (source, target) -> target.set(source);
    } else if (sourceType instanceof IntegralType) {
      res = fAssignerFromIntegral;
    } else if (sourceType instanceof RealType) {
      res = fAssignerFromReal;
    } else {
      res = null;
    }
    return res;
  }

  private static final class ConverterFromIntegral {
    final long fMask;
    final long fSignMask;

    ConverterFromIntegral(boolean signed, long mask) {
      fMask = mask;
      fSignMask = signed ? Long.highestOneBit(fMask) : 0;
    }

    long convert(long n) {
      return ((n & fSignMask) == 0) ? n & fMask : n | ~fMask;
    }
  }

  private static final class ConverterFromReal {
    final long fMin;
    final long fMax;

    ConverterFromReal(long min, long max) {
      fMin = min;
      fMax = max;
    }

    long convert(double d) {
      long res;
      try {
        res = ScriptUtils.roundDoubleToLong(d);
        Check.that((res >= fMin) && (res <= fMax), ArithmeticException::new);
      } catch (ArithmeticException ignored) {
        throw new ScriptException(String.format("Value '%s' exceeds target type's range.", d), null);
      }
      return res;
    }
  }

  @Override public boolean assignableBySharing() {
    return true;
  }
}
