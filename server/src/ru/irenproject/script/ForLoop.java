/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

public final class ForLoop {
  private final Assigner fInitialValueAssigner;
  private final Assigner fNextValueAssigner;
  private final int fStep;

  public ForLoop(Assigner initialValueAssigner, Assigner nextValueAssigner, int step) {
    fInitialValueAssigner = initialValueAssigner;
    fNextValueAssigner = nextValueAssigner;
    fStep = step;
  }

  public Assigner initialValueAssigner() {
    return fInitialValueAssigner;
  }

  public Assigner nextValueAssigner() {
    return fNextValueAssigner;
  }

  public int step() {
    return fStep;
  }
}
