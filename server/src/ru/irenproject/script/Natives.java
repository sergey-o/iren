/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import com.google.common.base.Ascii;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import com.google.common.math.DoubleMath;
import com.google.common.math.LongMath;
import com.google.common.primitives.Longs;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Nullable;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.Supplier;

public final class Natives {
  private static final CharMatcher FLOAT_CHAR_MATCHER = CharMatcher.inRange('0', '9').or(CharMatcher.anyOf(".+-Ee"));

  private static final String OUT_OF_RANGE = "Value is out of range.";

  public static void register(Scope scope) {
    scope.add(new NativeSubroutine("Abs", 1, Natives::inspectAbs), null);
    scope.add(new NativeSubroutine("AnsiLowerCase", 1, Natives::inspectAnsiLowerCase), null);
    scope.add(new NativeSubroutine("AnsiUpperCase", 1, Natives::inspectAnsiUpperCase), null);
    scope.add(new NativeSubroutine("Chr", 1, Natives::inspectChr), null);
    scope.add(new NativeSubroutine("Copy", 3, Natives::inspectCopy), null);
    scope.add(new NativeSubroutine("Cos", 1, Natives::inspectCos), null);
    scope.add(new NativeSubroutine("Dec", 1, Natives::inspectDec), null);
    scope.add(new NativeSubroutine("Delete", 3, Natives::inspectDelete), null);
    scope.add(new NativeSubroutine("Error", 1, Natives::inspectError), null);
    scope.add(new NativeSubroutine("Exp", 1, Natives::inspectExp), null);
    scope.add(new NativeSubroutine("FloatToStr", 1, Natives::inspectFloatToStr), null);
    scope.add(new NativeSubroutine("FloatToString", 2, Natives::inspectFloatToString), null);
    scope.add(new NativeSubroutine("GetArrayLength", 1, Natives::inspectGetArrayLength), null);
    scope.add(new NativeSubroutine("High", 1, Natives::inspectHigh), null);
    scope.add(new NativeSubroutine("Inc", 1, Natives::inspectInc), null);
    scope.add(new NativeSubroutine("Insert", 3, Natives::inspectInsert), null);
    scope.add(new NativeSubroutine("Int", 1, Natives::inspectInt), null);
    scope.add(new NativeSubroutine("IntToStr", 1, Natives::inspectIntToStr), null);
    scope.add(new NativeSubroutine("Int64ToStr", 1, Natives::inspectIntToStr), null);
    scope.add(new NativeSubroutine("Length", 1, Natives::inspectLength), null);
    scope.add(new NativeSubroutine("Ln", 1, Natives::inspectLn), null);
    scope.add(new NativeSubroutine("Low", 1, Natives::inspectLow), null);
    scope.add(new NativeSubroutine("LowerCase", 1, Natives::inspectLowerCase), null);
    scope.add(new NativeSubroutine("Ord", 1, Natives::inspectOrd), null);
    scope.add(new NativeSubroutine("PadL", 2, Natives::inspectPadL), null);
    scope.add(new NativeSubroutine("PadR", 2, Natives::inspectPadR), null);
    scope.add(new NativeSubroutine("PadZ", 2, Natives::inspectPadZ), null);
    scope.add(new NativeSubroutine("Pi", 0, Natives::inspectPi), null);
    scope.add(new NativeSubroutine("Pos", 2, Natives::inspectPos), null);
    scope.add(new NativeSubroutine("Power", 2, Natives::inspectPower), null);
    scope.add(new NativeSubroutine("Pred", 1, Natives::inspectPred), null);
    scope.add(new NativeSubroutine("Random", 1, Natives::inspectRandom), null);
    scope.add(new NativeSubroutine("RandomFloat", 0, Natives::inspectRandomFloat), null);
    scope.add(new NativeSubroutine("Replicate", 2, Natives::inspectStringOfChar), null);
    scope.add(new NativeSubroutine("Round", 1, Natives::inspectRound), null);
    scope.add(new NativeSubroutine("SetArrayLength", 2, Natives::inspectSetArrayLength), null);
    scope.add(new NativeSubroutine("SetLength", 2, Natives::inspectSetLength), null);
    scope.add(new NativeSubroutine("Sin", 1, Natives::inspectSin), null);
    scope.add(new NativeSubroutine("Sqrt", 1, Natives::inspectSqrt), null);
    scope.add(new NativeSubroutine("StringOfChar", 2, Natives::inspectStringOfChar), null);
    scope.add(new NativeSubroutine("StrToFloat", 1, Natives::inspectStrToFloat), null);
    scope.add(new NativeSubroutine("StrToInt", 1, Natives::inspectStrToInt), null);
    scope.add(new NativeSubroutine("StrToIntDef", 2, Natives::inspectStrToIntDef), null);
    scope.add(new NativeSubroutine("StrToInt64", 1, Natives::inspectStrToInt), null);
    scope.add(new NativeSubroutine("StrToInt64Def", 2, Natives::inspectStrToIntDef), null);
    scope.add(new NativeSubroutine("Succ", 1, Natives::inspectSucc), null);
    scope.add(new NativeSubroutine("Trim", 1, Natives::inspectTrim), null);
    scope.add(new NativeSubroutine("Trunc", 1, Natives::inspectTrunc), null);
    scope.add(new NativeSubroutine("UpperCase", 1, Natives::inspectUpperCase), null);
  }

  private static void requireNumeric(NativeCallArgument argument) {
    if (!ScriptUtils.isNumericType(argument.type())) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireText(NativeCallArgument argument) {
    if (!ScriptUtils.isTextType(argument.type())) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireString(NativeCallArgument argument) {
    if (!(argument.type() instanceof StringType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireChar(NativeCallArgument argument) {
    if (!(argument.type() instanceof CharType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireArray(NativeCallArgument argument) {
    if (!ScriptUtils.isArrayType(argument.type())) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireDynamicArray(NativeCallArgument argument) {
    if (!(argument.type() instanceof DynamicArrayType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.location());
    }
  }

  private static void requireAssignable(NativeCallArgument argument) {
    if (!argument.assignable()) {
      throw new ScriptException(ScriptUtils.BAD_VAR_ARGUMENT, argument.location());
    }
  }

  private static double getRealValue(Object argument) {
    return ((Number) ScriptUtils.getValue(argument)).doubleValue();
  }

  private static NativeCall inspectSqrt(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.sqrt(getRealValue(a[0])));
  }

  private static NativeCall inspectPi(@SuppressWarnings("unused") NativeCallArgument[] arguments,
      ScriptExaminer examiner) {
    return new NativeCall(examiner.realType(), (a, c) -> Math.PI);
  }

  private static NativeCall inspectSin(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.sin(getRealValue(a[0])));
  }

  private static NativeCall inspectCos(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.cos(getRealValue(a[0])));
  }

  private static NativeCall inspectInc(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return inspectIncOrDec(arguments, examiner, 1);
  }

  private static NativeCall inspectDec(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return inspectIncOrDec(arguments, examiner, -1);
  }

  private static NativeCall inspectIncOrDec(NativeCallArgument[] arguments, ScriptExaminer examiner, int delta) {
    requireAssignable(arguments[0]);
    Type type = arguments[0].type();

    NativeCallSite evaluator;
    if (type instanceof IntegralType) {
      Assigner assigner = Check.notNull(type.tryMakeAssignmentFrom(examiner.integerType()));
      evaluator = (a, c) -> {
        computeIntegralIncOrDec(a, delta, assigner);
        return null;
      };
    } else if (type instanceof EnumeratedType) {
      int bound = (delta > 0) ? ((EnumeratedType) type).valueCount() - 1 : 0;
      evaluator = (a, c) -> {
        computeEnumeratedIncOrDec(a, bound, delta);
        return null;
      };
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return new NativeCall(null, evaluator);
  }

  private static void computeIntegralIncOrDec(Object[] arguments, int delta, Assigner assigner) {
    Reference a = (Reference) arguments[0];

    long newValue;
    try {
      newValue = LongMath.checkedAdd((Long) a.get(), delta);
    } catch (ArithmeticException ignored) {
      throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, null);
    }

    assigner.assign(newValue, a);
  }

  private static void computeEnumeratedIncOrDec(Object[] arguments, int bound, int delta) {
    Reference a = (Reference) arguments[0];
    int value = (Integer) a.get();
    if (value == bound) {
      throw new ScriptException(OUT_OF_RANGE, null);
    }
    a.set(value + delta);
  }

  private static NativeCall inspectRandom(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.integerType(), (a, c) -> Utils.generateRandomLong(
        ScriptUtils.getIntegralValue(a[0]), c.randomNumberGenerator()::nextLong));
  }

  private static NativeCall inspectRandomFloat(@SuppressWarnings("unused") NativeCallArgument[] arguments,
      ScriptExaminer examiner) {
    return new NativeCall(examiner.realType(), (a, c) -> c.randomNumberGenerator().nextDouble());
  }

  private static NativeCall inspectGetArrayLength(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireArray(arguments[0]);
    return new NativeCall(examiner.integerType(), (a, c) -> (long) (((Object[]) ScriptUtils.getValue(a[0])).length));
  }

  private static NativeCall inspectSetArrayLength(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    requireAssignable(arguments[0]);
    requireDynamicArray(arguments[0]);
    requireNumeric(arguments[1]);

    Type elementType = ((DynamicArrayType) arguments[0].type()).elementType();
    Supplier<?> elementFactory = elementType.factory();
    Assigner elementAssigner = elementType.assignableBySharing() ?
        null : Check.notNull(elementType.tryMakeAssignmentFrom(elementType));

    return new NativeCall(null, (a, c) -> {
      computeSetArrayLength(a, elementFactory, elementAssigner);
      return null;
    });
  }

  private static void computeSetArrayLength(Object[] arguments, Supplier<?> elementFactory,
      @Nullable Assigner elementAssigner) {
    long newLengthAsLong = ScriptUtils.getIntegralValue(arguments[1]);
    if (newLengthAsLong < 0) {
      throw new ScriptException(String.format((Locale) null, "Negative array length: %d.", newLengthAsLong), null);
    }
    if (newLengthAsLong > Integer.MAX_VALUE) {
      throw new ScriptException(String.format((Locale) null, "Array length is too big: %d.", newLengthAsLong), null);
    }

    int newLength = (int) newLengthAsLong;
    Object[] newArray = new Object[newLength];

    boolean elementsAssignableBySharing = (elementAssigner == null);
    if (!elementsAssignableBySharing) {
      Arrays.setAll(newArray, ignored -> elementFactory.get());
    }

    Reference target = (Reference) arguments[0];
    Object[] oldArray = (Object[]) target.get();

    int copyCount = Integer.min(oldArray.length, newLength);
    if (elementsAssignableBySharing) {
      System.arraycopy(oldArray, 0, newArray, 0, copyCount);
    } else {
      for (int i = 0; i < copyCount; ++i) {
        elementAssigner.assign(oldArray[i], new Reference(newArray, i));
      }
    }

    if (elementsAssignableBySharing) {
      Arrays.fill(newArray, copyCount, newLength, elementFactory.get());
    }

    target.set(newArray);
  }

  private static NativeCall inspectLength(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return ScriptUtils.isTextType(arguments[0].type()) ?
        new NativeCall(examiner.integerType(), (a, c) -> (long) (((String) ScriptUtils.getValue(a[0])).length())) :
        inspectGetArrayLength(arguments, examiner);
  }

  private static NativeCall inspectSetLength(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    NativeCall res;
    if (arguments[0].type() instanceof StringType) {
      requireAssignable(arguments[0]);
      requireNumeric(arguments[1]);
      res = new NativeCall(null, (a, c) -> {
        computeSetLength(a);
        return null;
      });
    } else {
      res = inspectSetArrayLength(arguments, examiner);
    }

    return res;
  }

  private static void computeSetLength(Object[] arguments) {
    int newLength = ScriptUtils.checkNewStringLength(ScriptUtils.getIntegralValue(arguments[1]));
    Reference target = (Reference) arguments[0];
    String s = (String) target.get();
    target.set((newLength <= s.length()) ? s.substring(0, newLength) : Strings.padEnd(s, newLength, '\0'));
  }

  private static NativeCall inspectInsert(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    requireText(arguments[0]);
    requireAssignable(arguments[1]);
    requireString(arguments[1]);
    requireNumeric(arguments[2]);
    return new NativeCall(null, (a, c) -> {
      computeInsert(a);
      return null;
    });
  }

  private static void computeInsert(Object[] arguments) {
    String fragment = (String) ScriptUtils.getValue(arguments[0]);
    Reference target = (Reference) arguments[1];
    String s = (String) target.get();
    int p = (int) (Longs.constrainToRange(ScriptUtils.getIntegralValue(arguments[2]), 1, s.length() + 1L) - 1);

    target.set(new StringBuilder(ScriptUtils.checkNewStringLength(((long) s.length()) + fragment.length()))
        .append(s, 0, p)
        .append(fragment)
        .append(s, p, s.length())
        .toString());
  }

  private static NativeCall inspectDelete(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    requireAssignable(arguments[0]);
    requireString(arguments[0]);
    requireNumeric(arguments[1]);
    requireNumeric(arguments[2]);
    return new NativeCall(null, (a, c) -> {
      computeDelete(a);
      return null;
    });
  }

  private static void computeDelete(Object[] arguments) {
    Reference target = (Reference) arguments[0];
    String s = (String) target.get();
    long oneBasedIndex = ScriptUtils.getIntegralValue(arguments[1]);
    if ((oneBasedIndex >= 1) && (oneBasedIndex <= s.length())) {
      int index = ((int) oneBasedIndex) - 1;
      int maxDeletable = s.length() - index;
      int count = (int) Longs.constrainToRange(ScriptUtils.getIntegralValue(arguments[2]), 0, maxDeletable);
      if (count > 0) {
        target.set(new StringBuilder(s.length() - count)
            .append(s, 0, index)
            .append(s, index + count, s.length())
            .toString());
      }
    }
  }

  private static NativeCall inspectCopy(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    requireNumeric(arguments[1]);
    requireNumeric(arguments[2]);
    return new NativeCall(examiner.stringType(), (a, c) -> computeCopy(a));
  }

  private static String computeCopy(Object[] arguments) {
    String s = (String) ScriptUtils.getValue(arguments[0]);
    int index = (int) (Longs.constrainToRange(ScriptUtils.getIntegralValue(arguments[1]), 1, s.length() + 1L) - 1);
    int maxCopiable = s.length() - index;
    int count = (int) Longs.constrainToRange(ScriptUtils.getIntegralValue(arguments[2]), 0, maxCopiable);
    return s.substring(index, index + count);
  }

  private static NativeCall inspectPos(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    requireText(arguments[1]);
    return new NativeCall(examiner.integerType(), (a, c) -> computePos(a));
  }

  private static long computePos(Object[] arguments) {
    String pattern = (String) ScriptUtils.getValue(arguments[0]);
    return pattern.isEmpty() ? 0 : ((String) ScriptUtils.getValue(arguments[1])).indexOf(pattern) + 1;
  }

  private static NativeCall inspectStringOfChar(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireChar(arguments[0]);
    requireNumeric(arguments[1]);
    return new NativeCall(examiner.stringType(), (a, c) -> computeStringOfChar(a));
  }

  private static String computeStringOfChar(Object[] arguments) {
    long count = ScriptUtils.getIntegralValue(arguments[1]);
    return (count <= 0) ? "" : StringUtils.repeat(
        ((String) ScriptUtils.getValue(arguments[0])).charAt(0),
        ScriptUtils.checkNewStringLength(count));
  }

  private static NativeCall inspectPadL(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return inspectPad(arguments, examiner, (s, minLength) -> Strings.padStart(s, minLength, ' '));
  }

  private static NativeCall inspectPadR(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return inspectPad(arguments, examiner, (s, minLength) -> Strings.padEnd(s, minLength, ' '));
  }

  private static NativeCall inspectPadZ(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    return inspectPad(arguments, examiner, (s, minLength) -> Strings.padStart(s, minLength, '0'));
  }

  private static NativeCall inspectPad(NativeCallArgument[] arguments, ScriptExaminer examiner,
      BiFunction<String, Integer, String> padder) {
    requireText(arguments[0]);
    requireNumeric(arguments[1]);
    return new NativeCall(examiner.stringType(), (a, c) -> computePad(a, padder));
  }

  private static String computePad(Object[] arguments, BiFunction<String, Integer, String> padder) {
    String s = (String) ScriptUtils.getValue(arguments[0]);
    long minLength = ScriptUtils.getIntegralValue(arguments[1]);
    return (minLength <= s.length()) ? s : padder.apply(s, ScriptUtils.checkNewStringLength(minLength));
  }

  private static NativeCall inspectUpperCase(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.stringType(), (a, c) -> Ascii.toUpperCase((String) ScriptUtils.getValue(a[0])));
  }

  private static NativeCall inspectLowerCase(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.stringType(), (a, c) -> Ascii.toLowerCase((String) ScriptUtils.getValue(a[0])));
  }

  private static NativeCall inspectAnsiUpperCase(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.stringType(),
        (a, c) -> ((String) ScriptUtils.getValue(a[0])).toUpperCase(Locale.ROOT));
  }

  private static NativeCall inspectAnsiLowerCase(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.stringType(),
        (a, c) -> ((String) ScriptUtils.getValue(a[0])).toLowerCase(Locale.ROOT));
  }

  private static NativeCall inspectTrim(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.stringType(), (a, c) -> ((String) ScriptUtils.getValue(a[0])).trim());
  }

  private static NativeCall inspectIntToStr(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.stringType(), (a, c) -> Long.toString(ScriptUtils.getIntegralValue(a[0])));
  }

  private static NativeCall inspectStrToInt(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.integerType(), (a, c) -> computeStrToInt(a));
  }

  private static Long computeStrToInt(Object[] arguments) {
    Long res = tryStrToInt((String) ScriptUtils.getValue(arguments[0]));
    if (res == null) {
      throw new ScriptException("Cannot convert the specified string to an integer value.", null);
    }
    return res;
  }

  private static @Nullable Long tryStrToInt(String s) {
    Long res = null;

    int p = CharMatcher.isNot(' ').indexIn(s);
    if (p != -1) {
      boolean negative = (s.charAt(p) == '-');
      if (negative || (s.charAt(p) == '+')) {
        ++p;
      }

      boolean hex;
      if (s.startsWith("$", p) || s.startsWith("x", p) || s.startsWith("X", p)) {
        hex = true;
        ++p;
      } else if (s.startsWith("0x", p) || s.startsWith("0X", p)) {
        hex = true;
        p += 2;
      } else {
        hex = false;
      }

      if (hex) {
        String tail = s.substring(p);
        if (CharMatcher.ascii().matchesAllOf(tail)) {
          res = Utils.tryParseUnsignedLong(tail, 16);
          if (negative && (res != null)) {
            res = -res;
          }
        }
      } else if (!s.startsWith("-", p)) {
        res = Longs.tryParse(s.substring(negative ? p - 1 : p));
      }
    }

    return res;
  }

  private static NativeCall inspectStrToIntDef(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    requireNumeric(arguments[1]);
    return new NativeCall(examiner.integerType(), (a, c) -> computeStrToIntDef(a));
  }

  private static Long computeStrToIntDef(Object[] arguments) {
    Long n = tryStrToInt((String) ScriptUtils.getValue(arguments[0]));
    return (n == null) ? ScriptUtils.getIntegralValue(arguments[1]) : n;
  }

  private static NativeCall inspectFloatToStr(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.stringType(), (a, c) -> Double.toString(getRealValue(a[0])));
  }

  private static NativeCall inspectStrToFloat(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> computeStrToFloat(a));
  }

  private static Double computeStrToFloat(Object[] arguments) {
    String s = CharMatcher.is(' ').trimLeadingFrom((String) ScriptUtils.getValue(arguments[0]));
    try {
      if (s.equals("Infinity") || s.equals("-Infinity") || s.equals("NaN") || FLOAT_CHAR_MATCHER.matchesAllOf(s)) {
        return Double.valueOf(s);
      } else {
        throw new NumberFormatException();
      }
    } catch (NumberFormatException ignored) {
      throw new ScriptException("Cannot convert the specified string to a floating-point value.", null);
    }
  }

  private static NativeCall inspectFloatToString(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    requireNumeric(arguments[1]);
    return new NativeCall(examiner.stringType(), Natives::computeFloatToString);
  }

  private static String computeFloatToString(Object[] arguments, ScriptComputer computer) {
    double d = getRealValue(arguments[0]);
    if (Double.isFinite(d)) {
      return ScriptUtils.formatReal(d,
          (int) Longs.constrainToRange(ScriptUtils.getIntegralValue(arguments[1]), 0, Integer.MAX_VALUE),
          computer.locale());
    } else {
      throw new ScriptException(String.format("Cannot convert non-numeric value '%s' to a string.", d), null);
    }
  }

  private static NativeCall inspectRound(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.integerType(), (a, c) -> computeRoundOrTrunc(a, RoundingMode.HALF_UP));
  }

  private static NativeCall inspectTrunc(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.integerType(), (a, c) -> computeRoundOrTrunc(a, RoundingMode.DOWN));
  }

  private static Long computeRoundOrTrunc(Object[] arguments, RoundingMode roundingMode) {
    double d = getRealValue(arguments[0]);
    try {
      return DoubleMath.roundToLong(d, roundingMode);
    } catch (ArithmeticException ignored) {
      throw new ScriptException(ScriptUtils.getOutOfIntegerRangeMessage(d), null);
    }
  }

  private static NativeCall inspectInt(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> computeInt(a));
  }

  private static Double computeInt(Object[] arguments) {
    double d = getRealValue(arguments[0]);
    return (d >= 0) ? Math.floor(d) : Math.ceil(d);
  }

  private static NativeCall inspectAbs(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    NativeCall res;
    if (arguments[0].type() instanceof IntegralType) {
      res = new NativeCall(examiner.integerType(), (a, c) -> computeIntegralAbs(a));
    } else if (arguments[0].type() instanceof RealType) {
      res = new NativeCall(examiner.realType(), (a, c) -> Math.abs((Double) ScriptUtils.getValue(a[0])));
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return res;
  }

  private static Long computeIntegralAbs(Object[] arguments) {
    long n = (Long) ScriptUtils.getValue(arguments[0]);
    if (n == Long.MIN_VALUE) {
      throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, null);
    }
    return Math.abs(n);
  }

  private static NativeCall inspectPower(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    requireNumeric(arguments[1]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.pow(getRealValue(a[0]), getRealValue(a[1])));
  }

  private static NativeCall inspectExp(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.exp(getRealValue(a[0])));
  }

  private static NativeCall inspectLn(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.realType(), (a, c) -> Math.log(getRealValue(a[0])));
  }

  private static NativeCall inspectError(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    requireText(arguments[0]);
    return new NativeCall(null, (a, c) -> {
      throw new ScriptException("Script exited via a call to 'Error'.", null); // ignore argument for now
    });
  }

  private static NativeCall inspectChr(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    requireNumeric(arguments[0]);
    return new NativeCall(examiner.charType(), (a, c) -> Character.toString((char) ScriptUtils.getIntegralValue(a[0])));
  }

  private static NativeCall inspectOrd(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    Type type = arguments[0].type();

    NativeCallSite evaluator;
    if (type instanceof IntegralType) {
      evaluator = (a, c) -> ScriptUtils.getValue(a[0]);
    } else if (type instanceof CharType) {
      evaluator = (a, c) -> (long) (((String) ScriptUtils.getValue(a[0])).charAt(0));
    } else if (type instanceof EnumeratedType) {
      evaluator = (a, c) -> ((Integer) ScriptUtils.getValue(a[0])).longValue();
    } else if (type instanceof BooleanType) {
      evaluator = (a, c) -> ((Boolean) ScriptUtils.getValue(a[0])) ? 1L : 0L;
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return new NativeCall(examiner.integerType(), evaluator);
  }

  private static NativeCall inspectHigh(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    Type type = arguments[0].type();

    NativeCall res;
    if (type instanceof IntegralType) {
      long maxValue = ((IntegralType) type).maxValue();
      res = new NativeCall(type, (a, c) -> maxValue);
    } else if (type instanceof EnumeratedType) {
      int maxValue = ((EnumeratedType) type).valueCount() - 1;
      res = new NativeCall(type, (a, c) -> maxValue);
    } else if (type instanceof StaticArrayType) {
      long upperBound = ((StaticArrayType) type).upperBound();
      res = new NativeCall(examiner.integerType(), (a, c) -> upperBound);
    } else if (type instanceof DynamicArrayType) {
      res = new NativeCall(examiner.integerType(),
          (a, c) -> (long) (((Object[]) ScriptUtils.getValue(a[0])).length - 1));
    } else if (type instanceof StringType) {
      res = new NativeCall(examiner.integerType(), (a, c) -> (long) (((String) ScriptUtils.getValue(a[0])).length()));
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return res;
  }

  private static NativeCall inspectLow(NativeCallArgument[] arguments, ScriptExaminer examiner) {
    Type type = arguments[0].type();

    NativeCall res;
    if (type instanceof IntegralType) {
      long minValue = ((IntegralType) type).minValue();
      res = new NativeCall(type, (a, c) -> minValue);
    } else if (type instanceof EnumeratedType) {
      res = new NativeCall(type, (a, c) -> 0);
    } else if (type instanceof StaticArrayType) {
      long lowerBound = ((StaticArrayType) type).lowerBound();
      res = new NativeCall(examiner.integerType(), (a, c) -> lowerBound);
    } else if (type instanceof DynamicArrayType) {
      res = new NativeCall(examiner.integerType(), (a, c) -> 0L);
    } else if (type instanceof StringType) {
      res = new NativeCall(examiner.integerType(), (a, c) -> 1L);
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return res;
  }

  private static NativeCall inspectSucc(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    Type type = arguments[0].type();

    NativeCallSite evaluator;
    if (type instanceof IntegralType) {
      long maxValue = ((IntegralType) type).maxValue();
      evaluator = (a, c) -> computeIntegralSuccOrPred(a, maxValue, 1);
    } else if (type instanceof EnumeratedType) {
      int maxValue = ((EnumeratedType) type).valueCount() - 1;
      evaluator = (a, c) -> computeEnumeratedSuccOrPred(a, maxValue, 1);
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return new NativeCall(type, evaluator);
  }

  private static NativeCall inspectPred(NativeCallArgument[] arguments,
      @SuppressWarnings("unused") ScriptExaminer examiner) {
    Type type = arguments[0].type();

    NativeCallSite evaluator;
    if (type instanceof IntegralType) {
      long minValue = ((IntegralType) type).minValue();
      evaluator = (a, c) -> computeIntegralSuccOrPred(a, minValue, -1);
    } else if (type instanceof EnumeratedType) {
      evaluator = (a, c) -> computeEnumeratedSuccOrPred(a, 0, -1);
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, arguments[0].location());
    }

    return new NativeCall(type, evaluator);
  }

  private static Long computeIntegralSuccOrPred(Object[] arguments, long bound, int delta) {
    long value = (Long) ScriptUtils.getValue(arguments[0]);
    if (value == bound) {
      throw new ScriptException(OUT_OF_RANGE, null);
    }
    return value + delta;
  }

  private static Integer computeEnumeratedSuccOrPred(Object[] arguments, int bound, int delta) {
    int value = (Integer) ScriptUtils.getValue(arguments[0]);
    if (value == bound) {
      throw new ScriptException(OUT_OF_RANGE, null);
    }
    return value + delta;
  }

  private Natives() {}
}
