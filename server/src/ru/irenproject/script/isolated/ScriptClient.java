/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script.isolated;

import ru.irenproject.Utils;
import ru.irenproject.script.ScriptException;
import ru.irenproject.script.ScriptResult;
import ru.irenproject.script.isolated.Proto.ScriptReply;
import ru.irenproject.script.isolated.Proto.ScriptRequest;
import ru.irenproject.script.isolated.Proto.Variable;

import com.google.common.collect.ImmutableMap;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

public final class ScriptClient {
  public static final class DisconnectedException extends RuntimeException {
    private final int fServerExitCode;

    public DisconnectedException(int serverExitCode) {
      fServerExitCode = serverExitCode;
    }

    public int serverExitCode() {
      return fServerExitCode;
    }
  }

  private static final Logger fLogger = LoggerFactory.getLogger(ScriptClient.class);

  private final Process fServerProcess;
  private final MessageWriter fWriter;
  private final Thread fReader;

  private final Object fLock = new Object();
  private final HashMap<Long, CompletableFuture<ScriptResult>> fRequestsInProgress = new HashMap<>();
  private @Nullable Integer fServerExitCode;
  private long fLastRequestId;

  public ScriptClient(Process serverProcess) {
    fServerProcess = serverProcess;
    fWriter = new MessageWriter(fServerProcess.getOutputStream(), true);
    fReader = new Thread(this::runReader, "scriptMessageReader");
    fReader.setDaemon(true);
  }

  public void start() {
    fWriter.start();
    fReader.start();
  }

  public Future<ScriptResult> send(String script, Locale locale, long cpuTimeLimitNanoseconds,
      int resultSizeLimit, @Nullable Long randomNumberGeneratorSeed) {
    CompletableFuture<ScriptResult> res = new CompletableFuture<>();

    synchronized (fLock) {
      if (fServerExitCode == null) {
        ++fLastRequestId;
        fRequestsInProgress.put(fLastRequestId, res);

        ScriptRequest.Builder request = ScriptRequest.newBuilder()
            .setScript(script)
            .setLanguage(locale.toLanguageTag())
            .setCpuTimeLimitNanoseconds(cpuTimeLimitNanoseconds)
            .setResultSizeLimit(resultSizeLimit);
        if (randomNumberGeneratorSeed != null) {
          request.setRandomNumberGeneratorSeed(randomNumberGeneratorSeed);
        }

        fWriter.enqueue(request.build());
      } else {
        res.completeExceptionally(new DisconnectedException(fServerExitCode));
      }
    }

    return res;
  }

  private void runReader() {
    try {
      ScriptReply reply;
      while ((reply = ScriptReply.parseDelimitedFrom(fServerProcess.getInputStream())) != null) {
        long replyId = reply.getId();

        CompletableFuture<ScriptResult> f;
        synchronized (fLock) {
          f = fRequestsInProgress.remove(replyId);
        }

        if (f == null) {
          fLogger.warn("Unexpected reply {}.", replyId);
        } else if (reply.hasErrorMessage()) {
          f.completeExceptionally(new ScriptException(reply.getErrorMessage(), null));
        } else {
          f.complete(new ScriptResult(reply.getVariableList().stream()
              .collect(ImmutableMap.toImmutableMap(Variable::getName, Variable::getValue))));
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    } finally {
      synchronized (fLock) {
        fWriter.stop();

        IOUtils.closeQuietly(fServerProcess.getInputStream());
        IOUtils.closeQuietly(fServerProcess.getOutputStream());
        fServerExitCode = Utils.waitForProcessUninterruptibly(fServerProcess);

        for (CompletableFuture<ScriptResult> f : fRequestsInProgress.values()) {
          f.completeExceptionally(new DisconnectedException(fServerExitCode));
        }
      }
    }
  }
}
