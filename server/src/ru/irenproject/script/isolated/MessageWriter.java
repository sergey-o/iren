/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script.isolated;

import com.google.common.util.concurrent.Uninterruptibles;
import com.google.protobuf.Message;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingQueue;

public final class MessageWriter {
  private static final Object EOF = new Object();

  private final OutputStream fOut;
  private final LinkedBlockingQueue<Object> fQueue = new LinkedBlockingQueue<>();
  private final Thread fThread;

  public MessageWriter(OutputStream out, boolean daemon) {
    fOut = out;
    fThread = new Thread(this::run, "scriptMessageWriter");
    fThread.setDaemon(daemon);
  }

  public void start() {
    fThread.start();
  }

  public void stop() {
    fQueue.add(EOF);
    Uninterruptibles.joinUninterruptibly(fThread);
  }

  public void enqueue(Message message) {
    fQueue.add(message);
  }

  private void run() {
    Object object;
    while ((object = Uninterruptibles.takeUninterruptibly(fQueue)) != EOF) {
      Message message = (Message) object;
      try {
        message.writeDelimitedTo(fOut);
        fOut.flush();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }
}
