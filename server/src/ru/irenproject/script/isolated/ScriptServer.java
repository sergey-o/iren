/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script.isolated;

import ru.irenproject.Utils;
import ru.irenproject.script.BasicScriptRunner;
import ru.irenproject.script.ScriptException;
import ru.irenproject.script.ScriptResult;
import ru.irenproject.script.isolated.Proto.ScriptReply;
import ru.irenproject.script.isolated.Proto.ScriptRequest;

import com.google.common.primitives.Longs;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class ScriptServer {
  private final InputStream fIn;
  private final MessageWriter fMessageWriter;

  public static void main(String[] args) {
    Locale.setDefault(Locale.ROOT); // guard against libraries inadvertently using default locale
    new ScriptServer(System.in, System.out).run();
  }

  public ScriptServer(InputStream in, OutputStream out) {
    fIn = in;
    fMessageWriter = new MessageWriter(out, false);
  }

  public void run() {
    try {
      fMessageWriter.start();
      try {
        ExecutorService executor = Executors.newFixedThreadPool(
            (int) Longs.constrainToRange(Math.round(Runtime.getRuntime().maxMemory() / 30_000_000.0),
                1, Runtime.getRuntime().availableProcessors()),
            new BasicThreadFactory.Builder()
                .namingPattern("scriptRunner-%d")
                .daemon(true)
                .build());
        long requestId = 0;
        ScriptRequest request;
        while ((request = ScriptRequest.parseDelimitedFrom(fIn)) != null) {
          ++requestId;
          long _requestId = requestId;
          ScriptRequest _request = request;
          executor.execute(() -> handle(_requestId, _request));
        }
      } finally {
        fMessageWriter.stop();
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private void handle(long requestId, ScriptRequest request) {
    ScriptReply.Builder reply = ScriptReply.newBuilder()
        .setId(requestId);
    ScriptReply replyMessage = null;
    try {
      ScriptResult result = BasicScriptRunner.run(
          request.getScript(),
          Locale.forLanguageTag(request.getLanguage()),
          request.getCpuTimeLimitNanoseconds(),
          request.hasRandomNumberGeneratorSeed() ?
              new Random(request.getRandomNumberGeneratorSeed()) : new SecureRandom());

      for (Map.Entry<String, String> v : result.variables().entrySet()) {
        reply.addVariableBuilder()
            .setName(v.getKey())
            .setValue(v.getValue());
      }

      ScriptReply m = reply.build();
      if (m.getSerializedSize() <= request.getResultSizeLimit()) {
        replyMessage = m;
      } else {
        reply
            .clearVariable()
            .setErrorMessage("Total result size limit exceeded.");
      }
    } catch (ScriptException e) {
      reply.setErrorMessage(e.getMessage());
    } catch (OutOfMemoryError ignored) {
      Runtime.getRuntime().halt(Utils.JAVA_EXIT_CODE_ON_OUT_OF_MEMORY_ERROR);
    } catch (StackOverflowError e) {
      reply.setErrorMessage("Stack overflow.");
      e.setStackTrace(new StackTraceElement[0]);
      throw e;
    } catch (RuntimeException | Error e) {
      reply.setErrorMessage("Internal error.");
      throw e;
    } finally {
      fMessageWriter.enqueue((replyMessage == null) ? reply.build() : replyMessage);
    }
  }
}
