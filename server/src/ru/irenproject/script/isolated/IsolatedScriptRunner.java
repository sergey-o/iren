/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script.isolated;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.script.ScriptException;
import ru.irenproject.script.ScriptResult;
import ru.irenproject.script.isolated.ScriptClient.DisconnectedException;

import com.google.common.base.Joiner;
import com.google.common.primitives.Ints;
import com.google.common.util.concurrent.Uninterruptibles;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Paths;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Singleton public final class IsolatedScriptRunner {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("script.memoryLimit")
    String memoryLimit = "64m";

    @com.google.inject.Inject(optional = true)
    @Named("script.cpuTimeLimit")
    String cpuTimeLimit = "5000ms";

    @com.google.inject.Inject(optional = true)
    @Named("script.resultSizeLimit")
    String resultSizeLimit = "10000";
  }

  private static final Logger fLogger = LoggerFactory.getLogger(IsolatedScriptRunner.class);

  private static final int RETRY_COUNT = 1;

  private final Object fLock = new Object();
  private @Nullable ScriptClient fClient;

  private final int fMemoryLimitMegabytes;
  private final long fCpuTimeLimitNanoseconds;
  private final int fResultSizeLimit;

  @Inject private IsolatedScriptRunner(Config config) {
    fMemoryLimitMegabytes = parseMemoryLimit(config.memoryLimit);
    fCpuTimeLimitNanoseconds = parseCpuTimeLimit(config.cpuTimeLimit);
    fResultSizeLimit = parseResultSizeLimit(config.resultSizeLimit);
  }

  private static int parseMemoryLimit(String s) {
    Check.that(s.endsWith("m"), BadMemoryLimitException::new);
    int res = Check.notNull(Ints.tryParse(s.substring(0, s.length() - 1)), BadMemoryLimitException::new);
    Check.that(res >= 1, BadMemoryLimitException::new);
    return res;
  }

  private static final class BadMemoryLimitException extends RuntimeException {
    BadMemoryLimitException() {
      super("Incorrect 'script.memoryLimit' value.");
    }
  }

  private static long parseCpuTimeLimit(String s) {
    Check.that(s.endsWith("ms"), BadCpuTimeLimitException::new);
    int ms = Check.notNull(Ints.tryParse(s.substring(0, s.length() - 2)), BadCpuTimeLimitException::new);
    Check.that((ms >= 1) && (ms <= 1_000_000), BadCpuTimeLimitException::new);
    return TimeUnit.MILLISECONDS.toNanos(ms);
  }

  private static final class BadCpuTimeLimitException extends RuntimeException {
    BadCpuTimeLimitException() {
      super("Incorrect 'script.cpuTimeLimit' value.");
    }
  }

  private static int parseResultSizeLimit(String s) {
    int res = Check.notNull(Ints.tryParse(s), BadResultSizeLimitException::new);
    Check.that((res >= 1) && (res <= 100_000), BadResultSizeLimitException::new);
    return res;
  }

  private static final class BadResultSizeLimitException extends RuntimeException {
    BadResultSizeLimitException() {
      super("Incorrect 'script.resultSizeLimit' value.");
    }
  }

  public ScriptResult run(Iterable<String> script, Locale locale, @Nullable Long randomNumberGeneratorSeed) {
    String scriptText = Joiner.on('\n').join(script);

    int attempt = 0;
    ScriptResult res = null;
    do {
      ScriptClient client;
      synchronized (fLock) {
        if (fClient == null) {
          fClient = createClient();
        }
        client = fClient;
      }

      try {
        res = Uninterruptibles.getUninterruptibly(client.send(scriptText, locale, fCpuTimeLimitNanoseconds,
            fResultSizeLimit, randomNumberGeneratorSeed));
      } catch (ExecutionException wrapper) {
        Throwable e = wrapper.getCause();
        if (e instanceof ScriptException) {
          throw (ScriptException) e;
        } else {
          if (e instanceof DisconnectedException) {
            synchronized (fLock) {
              if (fClient == client) {
                fClient = null;
              }
            }
          } else {
            fLogger.warn("", e);
          }

          if (attempt == RETRY_COUNT) {
            throw new ScriptException(clientExceptionToMessage(e), null);
          }
        }
      }

      ++attempt;
    } while (res == null);

    return res;
  }

  private ScriptClient createClient() {
    ProcessBuilder pb = new ProcessBuilder(
        Paths.get(SystemUtils.JAVA_HOME, "bin", "java" + (SystemUtils.IS_OS_WINDOWS ? ".exe" : "")).toString(),
        "-XX:+ExitOnOutOfMemoryError",
        "-XX:+DisplayVMOutputToStderr",
        "-XX:-NeverActAsServerClassMachine",
        String.format((Locale) null, "-Xmx%dm", fMemoryLimitMegabytes),
        "-XX:SoftRefLRUPolicyMSPerMB=100000",
        "-classpath",
        SystemUtils.JAVA_CLASS_PATH,
        ScriptServer.class.getName());
    pb.redirectError(Redirect.INHERIT);

    Process serverProcess;
    try {
      serverProcess = pb.start();
    } catch (IOException e) {
      fLogger.warn("", e);
      throw new ScriptException("Cannot start script runner.", null);
    }

    ScriptClient res = new ScriptClient(serverProcess);
    res.start();
    return res;
  }

  private static String clientExceptionToMessage(Throwable e) {
    String res;
    if (e instanceof DisconnectedException) {
      DisconnectedException de = (DisconnectedException) e;
      if (de.serverExitCode() == Utils.JAVA_EXIT_CODE_ON_OUT_OF_MEMORY_ERROR) {
        res = "Memory limit exceeded.";
      } else {
        res = String.format((Locale) null, "Script runner failed with exit code %d.", de.serverExitCode());
      }
    } else {
      res = "Script runner failed.";
    }

    return res;
  }
}
