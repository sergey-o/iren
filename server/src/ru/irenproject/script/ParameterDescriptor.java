/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import com.google.common.base.Ascii;
import org.antlr.v4.runtime.Token;

public final class ParameterDescriptor {
  private final Token fName;
  private final Type fType;
  private final ParameterMode fMode;

  public ParameterDescriptor(Token name, Type type, ParameterMode mode) {
    fName = name;
    fType = type;
    fMode = mode;
  }

  public Token name() {
    return fName;
  }

  public Type type() {
    return fType;
  }

  public ParameterMode mode() {
    return fMode;
  }

  public boolean sameAs(ParameterDescriptor other) {
    return Ascii.equalsIgnoreCase(fName.getText(), other.fName.getText())
        && (fType == other.fType)
        && (fMode == other.fMode);
  }
}
