/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

grammar Script;

program: programHeader? programBlock '.'
  ;

programHeader: PROGRAM ID ';'
  ;

programBlock: (constantSection | typeSection | variableSection | subroutine | forwardSubroutine)* compoundStatement
  ;

constantSection: CONST constantDeclaration+
  ;

constantDeclaration: ID '=' expression ';'
  ;

typeSection: TYPE typeDeclaration+
  ;

typeDeclaration: ID '=' type ';'
  ;

variableSection: VAR variableDeclaration+
  ;

variableDeclaration: ID (',' ID)* ':' type ';'
  ;

type:
    ID #idType
  | '(' ID (',' ID)* ')' #enumeratedType
  | RECORD fieldGroup* END #recordType
  | ARRAY '[' lowerBound=expression '..' upperBound=expression ']' OF type #staticArrayType
  | ARRAY OF type #dynamicArrayType
  | SET OF type #setType
  ;

fieldGroup: ID (',' ID)* ':' type ';'
  ;

subroutine: subroutineHeader subroutineBlock ';'
  ;

forwardSubroutine: subroutineHeader FORWARD ';'
  ;

subroutineHeader: (FUNCTION ID parameterList ':' type | PROCEDURE ID parameterList) ';'
  ;

parameterList: ('(' (parameterGroup (';' parameterGroup)*)? ')')?
  ;

parameterGroup: VAR? ID (',' ID)* ':' type
  ;

subroutineBlock: (constantSection | typeSection | variableSection)* compoundStatement
  ;

compoundStatement: BEGIN statement (';' statement)* END
  ;

statement:
    left=expression ':=' right=expression #assignmentStatement
  | ID ('(' (expression (',' expression)*)? ')')? #callStatement
  | IF expression THEN thenBranch=statement (ELSE elseBranch=statement)? #ifStatement
  | WHILE expression DO statement #whileStatement
  | REPEAT statement (';' statement)* UNTIL expression #repeatStatement
  | FOR counter=expression ':=' from=expression direction=(TO | DOWNTO) to=expression DO statement #forStatement
  | BREAK #breakStatement
  | CONTINUE #continueStatement
  | EXIT #exitStatement
  | CASE expression OF caseBranch (';' caseBranch)* (';'? ELSE statement)? ';'? END #caseStatement
  | compoundStatement #compoundStatementAsStatement
  | #emptyStatement
  ;

caseBranch: caseTest (',' caseTest)* ':' statement
  ;

caseTest: expression ('..' expression)?
  ;

expression:
    ID #idExpression
  | REAL_LITERAL #realLiteralExpression
  | INTEGER_LITERAL #integerLiteralExpression
  | TRUE #trueExpression
  | FALSE #falseExpression
  | textLiteral #textLiteralExpression
  | '(' expression ')' #parenthesizedExpression
  | '[' (expression (',' expression)*)? ']' #setConstructorExpression
  | ID '(' (expression (',' expression)*)? ')' #callExpression
  | expression '.' ID #fieldAccessExpression
  | container=expression '[' index=expression ']' #elementAccessExpression
  | operator=('+' | '-') expression #signExpression
  | NOT expression #notExpression
  | left=expression operator=('*' | '/' | DIV | MOD | AND | SHL | SHR) right=expression #multiplicativeExpression
  | left=expression operator=('+' | '-' | OR | XOR) right=expression #additiveExpression
  | left=expression operator=('=' | '<>' | '<' | '<=' | '>' | '>=' | IN) right=expression #relationalExpression
  ;

textLiteral: textLiteralPart+
  ;

textLiteralPart:
    QUOTED_TEXT #quotedTextLiteralPart
  | hash='#' INTEGER_LITERAL #encodedTextLiteralPart
  ;

WHITE_SPACE: [ \t\n\r]+ -> channel(HIDDEN)
  ;

COMMENT: (
    '{' .*? '}'
  | '(*' .*? '*)'
    ) -> channel(HIDDEN)
  ;

LINE_COMMENT: '//' ~[\n\r]* -> channel(HIDDEN)
  ;

AND: [Aa] [Nn] [Dd]
  ;

ARRAY: [Aa] [Rr] [Rr] [Aa] [Yy]
  ;

BEGIN: [Bb] [Ee] [Gg] [Ii] [Nn]
  ;

BREAK: [Bb] [Rr] [Ee] [Aa] [Kk]
  ;

CASE: [Cc] [Aa] [Ss] [Ee]
  ;

CONST: [Cc] [Oo] [Nn] [Ss] [Tt]
  ;

CONTINUE: [Cc] [Oo] [Nn] [Tt] [Ii] [Nn] [Uu] [Ee]
  ;

DIV: [Dd] [Ii] [Vv]
  ;

DO: [Dd] [Oo]
  ;

DOWNTO: [Dd] [Oo] [Ww] [Nn] [Tt] [Oo]
  ;

ELSE: [Ee] [Ll] [Ss] [Ee]
  ;

END: [Ee] [Nn] [Dd]
  ;

EXIT: [Ee] [Xx] [Ii] [Tt]
  ;

FALSE: [Ff] [Aa] [Ll] [Ss] [Ee]
  ;

FOR: [Ff] [Oo] [Rr]
  ;

FORWARD: [Ff] [Oo] [Rr] [Ww] [Aa] [Rr] [Dd]
  ;

FUNCTION: [Ff] [Uu] [Nn] [Cc] [Tt] [Ii] [Oo] [Nn]
  ;

IF: [Ii] [Ff]
  ;

IN: [Ii] [Nn]
  ;

MOD: [Mm] [Oo] [Dd]
  ;

NOT: [Nn] [Oo] [Tt]
  ;

OF: [Oo] [Ff]
  ;

OR: [Oo] [Rr]
  ;

PROCEDURE: [Pp] [Rr] [Oo] [Cc] [Ee] [Dd] [Uu] [Rr] [Ee]
  ;

PROGRAM: [Pp] [Rr] [Oo] [Gg] [Rr] [Aa] [Mm]
  ;

RECORD: [Rr] [Ee] [Cc] [Oo] [Rr] [Dd]
  ;

REPEAT: [Rr] [Ee] [Pp] [Ee] [Aa] [Tt]
  ;

SET: [Ss] [Ee] [Tt]
  ;

SHL: [Ss] [Hh] [Ll]
  ;

SHR: [Ss] [Hh] [Rr]
  ;

THEN: [Tt] [Hh] [Ee] [Nn]
  ;

TO: [Tt] [Oo]
  ;

TRUE: [Tt] [Rr] [Uu] [Ee]
  ;

TYPE: [Tt] [Yy] [Pp] [Ee]
  ;

UNTIL: [Uu] [Nn] [Tt] [Ii] [Ll]
  ;

VAR: [Vv] [Aa] [Rr]
  ;

WHILE: [Ww] [Hh] [Ii] [Ll] [Ee]
  ;

XOR: [Xx] [Oo] [Rr]
  ;

UNUSED_RESERVED_WORD:
    [Aa] [Ss]
  | [Cc] [Ll] [Aa] [Ss] [Ss]
  | [Cc] [Oo] [Nn] [Ss] [Tt] [Rr] [Uu] [Cc] [Tt] [Oo] [Rr]
  | [Dd] [Ee] [Ss] [Tt] [Rr] [Uu] [Cc] [Tt] [Oo] [Rr]
  | [Ee] [Xx] [Cc] [Ee] [Pp] [Tt]
  | [Ee] [Xx] [Pp] [Oo] [Rr] [Tt]
  | [Ee] [Xx] [Tt] [Ee] [Rr] [Nn] [Aa] [Ll]
  | [Ff] [Ii] [Nn] [Aa] [Ll] [Ii] [Zz] [Aa] [Tt] [Ii] [Oo] [Nn]
  | [Ff] [Ii] [Nn] [Aa] [Ll] [Ll] [Yy]
  | [Gg] [Oo] [Tt] [Oo]
  | [Ii] [Mm] [Pp] [Ll] [Ee] [Mm] [Ee] [Nn] [Tt] [Aa] [Tt] [Ii] [Oo] [Nn]
  | [Ii] [Nn] [Hh] [Ee] [Rr] [Ii] [Tt] [Ee] [Dd]
  | [Ii] [Nn] [Ii] [Tt] [Ii] [Aa] [Ll] [Ii] [Zz] [Aa] [Tt] [Ii] [Oo] [Nn]
  | [Ii] [Nn] [Tt] [Ee] [Rr] [Ff] [Aa] [Cc] [Ee]
  | [Ii] [Ss]
  | [Ll] [Aa] [Bb] [Ee] [Ll]
  | [Nn] [Ii] [Ll]
  | [Oo] [Uu] [Tt]
  | [Oo] [Vv] [Ee] [Rr] [Rr] [Ii] [Dd] [Ee]
  | [Pp] [Rr] [Ii] [Vv] [Aa] [Tt] [Ee]
  | [Pp] [Rr] [Oo] [Pp] [Ee] [Rr] [Tt] [Yy]
  | [Pp] [Rr] [Oo] [Tt] [Ee] [Cc] [Tt] [Ee] [Dd]
  | [Pp] [Uu] [Bb] [Ll] [Ii] [Cc]
  | [Pp] [Uu] [Bb] [Ll] [Ii] [Ss] [Hh] [Ee] [Dd]
  | [Tt] [Rr] [Yy]
  | [Uu] [Nn] [Ii] [Tt]
  | [Uu] [Ss] [Ee] [Ss]
  | [Vv] [Ii] [Rr] [Tt] [Uu] [Aa] [Ll]
  | [Ww] [Ii] [Tt] [Hh]
  ;

ID: (LETTER | '_') (LETTER | '_' | DIGIT)*
  ;

fragment LETTER: [A-Za-z]
  ;

fragment DIGIT: [0-9]
  ;

REAL_LITERAL: DIGIT+ ('.' {_input.LA(1) != '.'}? DIGIT* EXPONENT? | EXPONENT)
  ;

fragment EXPONENT: [Ee] [+-]? DIGIT+
  ;

INTEGER_LITERAL:
    DIGIT+
  | '$' HEX_DIGIT+
  ;

fragment HEX_DIGIT: [0-9A-Fa-f]
  ;

QUOTED_TEXT: '\'' (~['\n\r] | '\'\'')* '\''
  ;

PLUS: '+'
  ;

MINUS: '-'
  ;

STAR: '*'
  ;

SLASH: '/'
  ;

EQUAL: '='
  ;

NOT_EQUAL: '<>'
  ;

LESS: '<'
  ;

LESS_OR_EQUAL: '<='
  ;

GREATER: '>'
  ;

GREATER_OR_EQUAL: '>='
  ;

UNKNOWN_CHARACTER: .
  ;
