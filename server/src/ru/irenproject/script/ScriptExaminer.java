/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.script.parser.ScriptBaseListener;
import ru.irenproject.script.parser.ScriptParser;
import ru.irenproject.script.parser.ScriptParser.AdditiveExpressionContext;
import ru.irenproject.script.parser.ScriptParser.AssignmentStatementContext;
import ru.irenproject.script.parser.ScriptParser.BreakStatementContext;
import ru.irenproject.script.parser.ScriptParser.CallExpressionContext;
import ru.irenproject.script.parser.ScriptParser.CallStatementContext;
import ru.irenproject.script.parser.ScriptParser.CaseBranchContext;
import ru.irenproject.script.parser.ScriptParser.CaseStatementContext;
import ru.irenproject.script.parser.ScriptParser.CaseTestContext;
import ru.irenproject.script.parser.ScriptParser.ConstantDeclarationContext;
import ru.irenproject.script.parser.ScriptParser.ContinueStatementContext;
import ru.irenproject.script.parser.ScriptParser.DynamicArrayTypeContext;
import ru.irenproject.script.parser.ScriptParser.ElementAccessExpressionContext;
import ru.irenproject.script.parser.ScriptParser.EncodedTextLiteralPartContext;
import ru.irenproject.script.parser.ScriptParser.EnumeratedTypeContext;
import ru.irenproject.script.parser.ScriptParser.ExpressionContext;
import ru.irenproject.script.parser.ScriptParser.FalseExpressionContext;
import ru.irenproject.script.parser.ScriptParser.FieldAccessExpressionContext;
import ru.irenproject.script.parser.ScriptParser.FieldGroupContext;
import ru.irenproject.script.parser.ScriptParser.ForStatementContext;
import ru.irenproject.script.parser.ScriptParser.ForwardSubroutineContext;
import ru.irenproject.script.parser.ScriptParser.IdExpressionContext;
import ru.irenproject.script.parser.ScriptParser.IdTypeContext;
import ru.irenproject.script.parser.ScriptParser.IfStatementContext;
import ru.irenproject.script.parser.ScriptParser.IntegerLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.MultiplicativeExpressionContext;
import ru.irenproject.script.parser.ScriptParser.NotExpressionContext;
import ru.irenproject.script.parser.ScriptParser.ParameterGroupContext;
import ru.irenproject.script.parser.ScriptParser.ParameterListContext;
import ru.irenproject.script.parser.ScriptParser.ParenthesizedExpressionContext;
import ru.irenproject.script.parser.ScriptParser.ProgramContext;
import ru.irenproject.script.parser.ScriptParser.QuotedTextLiteralPartContext;
import ru.irenproject.script.parser.ScriptParser.RealLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.RecordTypeContext;
import ru.irenproject.script.parser.ScriptParser.RelationalExpressionContext;
import ru.irenproject.script.parser.ScriptParser.RepeatStatementContext;
import ru.irenproject.script.parser.ScriptParser.SetConstructorExpressionContext;
import ru.irenproject.script.parser.ScriptParser.SetTypeContext;
import ru.irenproject.script.parser.ScriptParser.SignExpressionContext;
import ru.irenproject.script.parser.ScriptParser.StaticArrayTypeContext;
import ru.irenproject.script.parser.ScriptParser.SubroutineBlockContext;
import ru.irenproject.script.parser.ScriptParser.SubroutineContext;
import ru.irenproject.script.parser.ScriptParser.SubroutineHeaderContext;
import ru.irenproject.script.parser.ScriptParser.TextLiteralContext;
import ru.irenproject.script.parser.ScriptParser.TextLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.TextLiteralPartContext;
import ru.irenproject.script.parser.ScriptParser.TrueExpressionContext;
import ru.irenproject.script.parser.ScriptParser.TypeDeclarationContext;
import ru.irenproject.script.parser.ScriptParser.VariableDeclarationContext;
import ru.irenproject.script.parser.ScriptParser.WhileStatementContext;

import com.google.common.math.LongMath;
import com.google.common.primitives.Longs;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.TerminalNode;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public final class ScriptExaminer extends ScriptBaseListener {
  private static final String NO_PROCEDURE_INSIDE_EXPRESSION = "Procedure cannot be called inside an expression.";
  private static final String INCORRECT_ARGUMENT_COUNT = "Incorrect number of arguments.";

  private static final String MIN_LONG_MAGNITUDE = Long.toString(Long.MIN_VALUE).substring(1);
  private static final Pattern TWO_SINGLE_QUOTES = Pattern.compile("''");

  private final NodeData fNodeData = new NodeData();
  private final Scope fGlobalScope;
  private Scope fCurrentScope;

  private final ParseTreeProperty<Type> fTypes = new ParseTreeProperty<>();
  private final ParseTreeProperty<Object> fValues = new ParseTreeProperty<>();
  private final ParseTreeProperty<Boolean> fAssignableFlags = new ParseTreeProperty<>();
  private final ParseTreeProperty<ParameterList> fParameterLists = new ParseTreeProperty<>();

  private final RealType fRealType = new RealType();
  private final IntegralType fIntegerType = new IntegralType(true, 0xffffffffffffffffL);
  private final BooleanType fBooleanType = new BooleanType();
  private final StringType fStringType = new StringType();
  private final CharType fCharType = new CharType();

  private long fLoopNestingLevel;
  private final HashMap<Type, DynamicArrayType> fDynamicArrayTypesByElementType = new HashMap<>();
  private final HashMap</* @Nullable */Type, SetType> fSetTypesByElementType = new HashMap<>();

  public ScriptExaminer(Scope systemScope) {
    fGlobalScope = new Scope(systemScope, false);
    fCurrentScope = fGlobalScope;
  }

  public NodeData nodeData() {
    return fNodeData;
  }

  public Scope globalScope() {
    return fGlobalScope;
  }

  public RealType realType() {
    return fRealType;
  }

  public IntegralType integerType() {
    return fIntegerType;
  }

  public BooleanType booleanType() {
    return fBooleanType;
  }

  public StringType stringType() {
    return fStringType;
  }

  public CharType charType() {
    return fCharType;
  }

  @Override public void visitTerminal(TerminalNode node) {
    Token token = node.getSymbol();
    switch (token.getType()) {
      case ScriptParser.REAL_LITERAL: {
        Double d = Double.valueOf(token.getText());
        if (d.isInfinite()) {
          throw new ScriptException("Real literal is out of range.", token);
        }
        fValues.put(node, d);
        break;
      }
      case ScriptParser.INTEGER_LITERAL: {
        String text = token.getText();
        Long n = text.startsWith("$") ? Utils.tryParseUnsignedLong(text.substring(1), 16) : Longs.tryParse(text);
        if (n == null) {
          if (text.equals(MIN_LONG_MAGNITUDE)
              && (node.getParent() instanceof IntegerLiteralExpressionContext)
              && (node.getParent().getParent() instanceof SignExpressionContext)
              && (((SignExpressionContext) node.getParent().getParent()).operator.getType() == ScriptParser.MINUS)) {
            n = Long.MIN_VALUE;
          } else {
            throw new ScriptException("Integer literal is out of range.", token);
          }
        }
        fValues.put(node, n);
        break;
      }
    }
  }

  @Override public void exitProgram(ProgramContext ctx) {
    Check.that(fCurrentScope == fGlobalScope);
    Check.that(fLoopNestingLevel == 0);

    for (Symbol s : fGlobalScope.symbols()) {
      if ((s instanceof Subroutine) && (fNodeData.subroutineStatements().get(((Subroutine) s).header()) == null)) {
        throw new ScriptException(String.format("Unsatisfied forward declaration of '%s'.", s.name()),
            ((Subroutine) s).header().start);
      }
    }

    fNodeData.storageAllocators().put(ctx, createStorageAllocator(fGlobalScope));
  }

  private static Supplier<Object[]> createStorageAllocator(Scope scope) {
    List<Supplier<?>> factories = scope.symbols().stream()
        .filter(s -> s instanceof Variable)
        .map(s -> (Variable) s)
        .map(v -> v.containsReference() ? (Supplier<?>) (() -> null) : v.type().factory())
        .collect(Collectors.toList());
    return () -> factories.stream()
        .map(Supplier::get)
        .toArray();
  }

  @Override public void exitVariableDeclaration(VariableDeclarationContext ctx) {
    for (TerminalNode id : ctx.ID()) {
      Token name = id.getSymbol();
      fCurrentScope.add(
          new Variable(
              name.getText(),
              fTypes.get(ctx.type()),
              fCurrentScope.local(),
              fCurrentScope.allocateVariableAddress(),
              false),
          name);
    }
  }

  @Override public void exitIdType(IdTypeContext ctx) {
    Token token = ctx.ID().getSymbol();
    Symbol type = fCurrentScope.get(token);
    if (!(type instanceof TypeSymbol)) {
      throw new ScriptException(String.format("'%s' is not a type.", token.getText()), token);
    }
    fTypes.put(ctx, ((TypeSymbol) type).type());
  }

  @Override public void exitIdExpression(IdExpressionContext ctx) {
    Token token = ctx.ID().getSymbol();
    Symbol symbol = fCurrentScope.get(token);
    if (symbol instanceof TypeSymbol) {
      throw new ScriptException(String.format("Type name '%s' cannot be used here.", token.getText()), token);
    }

    if (symbol instanceof Variable) {
      Variable v = (Variable) symbol;
      fTypes.put(ctx, v.type());
      fAssignableFlags.put(ctx, Boolean.TRUE);
      fNodeData.idExpressionTargets().put(ctx, new VariableLocation(v.local(), v.address(), v.containsReference()));
    } else if (symbol instanceof Constant) {
      Constant c = (Constant) symbol;
      fTypes.put(ctx, c.type());
      fNodeData.idExpressionTargets().put(ctx, new ComputedConstant(c.value()));
    } else if (symbol instanceof Subroutine) {
      setUpCallSite(ctx, (Subroutine) symbol, Collections.emptyList(), fNodeData.idExpressionTargets(), fTypes);
    } else if (symbol instanceof NativeSubroutine) {
      setUpNativeCallSite(ctx, (NativeSubroutine) symbol, Collections.emptyList(), fNodeData.idExpressionTargets(),
          fTypes);
    } else {
      throw new RuntimeException();
    }
  }

  @Override public void exitRealLiteralExpression(RealLiteralExpressionContext ctx) {
    fTypes.put(ctx, fRealType);
    fNodeData.literals().put(ctx, fValues.get(ctx.REAL_LITERAL()));
  }

  @Override public void exitIntegerLiteralExpression(IntegerLiteralExpressionContext ctx) {
    fTypes.put(ctx, fIntegerType);
    fNodeData.literals().put(ctx, fValues.get(ctx.INTEGER_LITERAL()));
  }

  @Override public void exitTextLiteralExpression(TextLiteralExpressionContext ctx) {
    String s = (String) fValues.get(ctx.textLiteral());
    fTypes.put(ctx, (s.length() == 1) ? fCharType : fStringType);
    fNodeData.literals().put(ctx, s);
  }

  @Override public void exitTextLiteral(TextLiteralContext ctx) {
    StringBuilder value = new StringBuilder();
    TextLiteralPartContext previousPart = null;

    for (TextLiteralPartContext part : ctx.textLiteralPart()) {
      if (part instanceof QuotedTextLiteralPartContext) {
        if (previousPart instanceof QuotedTextLiteralPartContext) {
          throw new ScriptException(ScriptUtils.SYNTAX_ERROR, part.start);
        }
        value.append(unquote(((QuotedTextLiteralPartContext) part).QUOTED_TEXT().getText()));
      } else if (part instanceof EncodedTextLiteralPartContext) {
        EncodedTextLiteralPartContext p = (EncodedTextLiteralPartContext) part;
        TerminalNode codeNode = p.INTEGER_LITERAL();
        if (p.hash.getTokenIndex() + 1 != codeNode.getSymbol().getTokenIndex()) {
          throw new ScriptException(ScriptUtils.SYNTAX_ERROR, p.start);
        }
        long code = (Long) fValues.get(codeNode);
        if ((code < 0) || (code > Character.MAX_VALUE)) {
          throw new ScriptException("Character code is out of range.", codeNode.getSymbol());
        }
        value.append((char) code);
      } else {
        throw new RuntimeException();
      }

      previousPart = part;
    }

    fValues.put(ctx, value.toString());
  }

  private static String unquote(String s) {
    return TWO_SINGLE_QUOTES.matcher(s.substring(1, s.length() - 1)).replaceAll("'");
  }

  @Override public void exitAssignmentStatement(AssignmentStatementContext ctx) {
    if (Boolean.TRUE.equals(fAssignableFlags.get(ctx.left))) {
      Assigner assigner = fTypes.get(ctx.left).tryMakeAssignmentFrom(fTypes.get(ctx.right));
      if (assigner == null) {
        throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.right.start);
      }
      fNodeData.assigners().put(ctx, assigner);
    } else if (isAssignableIndexedString(ctx.left)) {
      if (!(fTypes.get(ctx.right) instanceof CharType)) {
        throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.right.start);
      }
    } else {
      throw new ScriptException("Incorrect left-hand side in assignment.", ctx.left.start);
    }
  }

  private boolean isAssignableIndexedString(ExpressionContext e) {
    boolean res;
    if (e instanceof ElementAccessExpressionContext) {
      ExpressionContext container = ((ElementAccessExpressionContext) e).container;
      res = (fTypes.get(container) instanceof StringType) && Boolean.TRUE.equals(fAssignableFlags.get(container));
    } else {
      res = false;
    }
    return res;
  }

  @Override public void exitSignExpression(SignExpressionContext ctx) {
    Type type = fTypes.get(ctx.expression());
    boolean minus = (ctx.operator.getType() == ScriptParser.MINUS);

    UnaryOperator<Object> operator;
    if (type instanceof RealType) {
      fTypes.put(ctx, fRealType);
      operator = minus ? d -> -((Double) d) : UnaryOperator.identity();
    } else if (type instanceof IntegralType) {
      fTypes.put(ctx, fIntegerType);
      if (minus) {
        operator = (ctx.expression() instanceof IntegerLiteralExpressionContext) ?
            n -> -((Long) n) : n -> Math.negateExact((Long) n);
      } else {
        operator = UnaryOperator.identity();
      }
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.start);
    }

    fNodeData.unaryOperators().put(ctx, operator);
  }

  @Override public void exitAdditiveExpression(AdditiveExpressionContext ctx) {
    switch (ctx.operator.getType()) {
      case ScriptParser.PLUS: {
        handleAddition(ctx);
        break;
      }
      case ScriptParser.MINUS: {
        handleSubtraction(ctx);
        break;
      }
      case ScriptParser.OR: {
        handleOr(ctx);
        break;
      }
      case ScriptParser.XOR: {
        handleXor(ctx);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }
  }

  private void handleAddition(AdditiveExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if (ScriptUtils.isTextType(leftType) && ScriptUtils.isTextType(rightType)) {
      fTypes.put(ctx, fStringType);
      fNodeData.binaryOperators().put(ctx, (a, b) -> concatenate((String) a, (String) b));
    } else if ((leftType instanceof SetType) && (rightType instanceof SetType)) {
      fTypes.put(ctx, getCompatibleSetType((SetType) leftType, (SetType) rightType, ctx.operator));
      fNodeData.binaryOperators().put(ctx, (a, b) -> Utils.bitSetOr((BitSet) a, (BitSet) b));
    } else {
      setUpBinaryNumericOperator(
          ctx,
          ctx.operator,
          leftType,
          rightType,
          (a, b) -> LongMath.checkedAdd((Long) a, (Long) b),
          (a, b) -> ((Double) a) + ((Double) b),
          (a, b) -> ((Double) a) + ((Long) b),
          (a, b) -> ((Long) a) + ((Double) b),
          fIntegerType,
          fRealType);
    }
  }

  private static String concatenate(String a, String b) {
    ScriptUtils.checkNewStringLength(((long) a.length()) + b.length());
    return a.concat(b);
  }

  private void handleSubtraction(AdditiveExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if ((leftType instanceof SetType) && (rightType instanceof SetType)) {
      fTypes.put(ctx, getCompatibleSetType((SetType) leftType, (SetType) rightType, ctx.operator));
      fNodeData.binaryOperators().put(ctx, (a, b) -> Utils.bitSetAndNot((BitSet) a, (BitSet) b));
    } else {
      setUpBinaryNumericOperator(
          ctx,
          ctx.operator,
          leftType,
          rightType,
          (a, b) -> LongMath.checkedSubtract((Long) a, (Long) b),
          (a, b) -> ((Double) a) - ((Double) b),
          (a, b) -> ((Double) a) - ((Long) b),
          (a, b) -> ((Long) a) - ((Double) b),
          fIntegerType,
          fRealType);
    }
  }

  private void setUpBinaryNumericOperator(
      ParseTree expressionNode,
      @Nullable Token location,
      Type leftType,
      Type rightType,
      BinaryOperator<Object> doIntegralIntegral,
      BinaryOperator<Object> doRealReal,
      BinaryOperator<Object> doRealIntegral,
      BinaryOperator<Object> doIntegralReal,
      Type resultTypeIfBothIntegral,
      Type resultTypeIfAtLeastOneReal) {
    BinaryOperator<Object> operator = chooseBinaryNumericOperator(
        leftType,
        rightType,
        doIntegralIntegral,
        doRealReal,
        doRealIntegral,
        doIntegralReal,
        location);
    fTypes.put(expressionNode, ((leftType instanceof IntegralType) && (rightType instanceof IntegralType)) ?
        resultTypeIfBothIntegral : resultTypeIfAtLeastOneReal);
    fNodeData.binaryOperators().put(expressionNode, operator);
  }

  private static BinaryOperator<Object> chooseBinaryNumericOperator(
      Type leftType,
      Type rightType,
      BinaryOperator<Object> doIntegralIntegral,
      BinaryOperator<Object> doRealReal,
      BinaryOperator<Object> doRealIntegral,
      BinaryOperator<Object> doIntegralReal,
      @Nullable Token location) {
    BinaryOperator<Object> res = null;
    if (leftType instanceof IntegralType) {
      if (rightType instanceof IntegralType) {
        res = doIntegralIntegral;
      } else if (rightType instanceof RealType) {
        res = doIntegralReal;
      }
    } else if (leftType instanceof RealType) {
      if (rightType instanceof IntegralType) {
        res = doRealIntegral;
      } else if (rightType instanceof RealType) {
        res = doRealReal;
      }
    }

    if (res == null) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, location);
    }

    return res;
  }

  private void handleOr(AdditiveExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if ((leftType instanceof BooleanType) && (rightType instanceof BooleanType)) {
      fTypes.put(ctx, fBooleanType);
    } else {
      setUpBinaryIntegralOperator(ctx, ctx.operator, leftType, rightType, (a, b) -> ((Long) a) | ((Long) b));
    }
  }

  private void handleXor(AdditiveExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if ((leftType instanceof BooleanType) && (rightType instanceof BooleanType)) {
      fTypes.put(ctx, fBooleanType);
      fNodeData.binaryOperators().put(ctx, (a, b) -> ((Boolean) a) ^ ((Boolean) b));
    } else {
      setUpBinaryIntegralOperator(ctx, ctx.operator, leftType, rightType, (a, b) -> ((Long) a) ^ ((Long) b));
    }
  }

  @Override public void exitMultiplicativeExpression(MultiplicativeExpressionContext ctx) {
    switch (ctx.operator.getType()) {
      case ScriptParser.STAR: {
        handleMultiplication(ctx);
        break;
      }
      case ScriptParser.SLASH: {
        handleDivision(ctx);
        break;
      }
      case ScriptParser.DIV: {
        handleDiv(ctx);
        break;
      }
      case ScriptParser.MOD: {
        handleMod(ctx);
        break;
      }
      case ScriptParser.AND: {
        handleAnd(ctx);
        break;
      }
      case ScriptParser.SHL:
      case ScriptParser.SHR: {
        handleShift(ctx);
        break;
      }
      default: {
        throw new RuntimeException();
      }
    }
  }

  private void handleMultiplication(MultiplicativeExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if ((leftType instanceof SetType) && (rightType instanceof SetType)) {
      fTypes.put(ctx, getCompatibleSetType((SetType) leftType, (SetType) rightType, ctx.operator));
      fNodeData.binaryOperators().put(ctx, (a, b) -> Utils.bitSetAnd((BitSet) a, (BitSet) b));
    } else {
      setUpBinaryNumericOperator(
          ctx,
          ctx.operator,
          leftType,
          rightType,
          (a, b) -> LongMath.checkedMultiply((Long) a, (Long) b),
          (a, b) -> ((Double) a) * ((Double) b),
          (a, b) -> ((Double) a) * ((Long) b),
          (a, b) -> ((Long) a) * ((Double) b),
          fIntegerType,
          fRealType);
    }
  }

  private void handleDivision(MultiplicativeExpressionContext ctx) {
    fNodeData.binaryOperators().put(ctx, chooseBinaryNumericOperator(
        fTypes.get(ctx.left),
        fTypes.get(ctx.right),
        (a, b) -> ((Long) a).doubleValue() / ((Long) b),
        (a, b) -> ((Double) a) / ((Double) b),
        (a, b) -> ((Double) a) / ((Long) b),
        (a, b) -> ((Long) a) / ((Double) b),
        ctx.operator));
    fTypes.put(ctx, fRealType);
  }

  private void handleDiv(MultiplicativeExpressionContext ctx) {
    setUpBinaryIntegralOperator(ctx, ctx.operator, fTypes.get(ctx.left), fTypes.get(ctx.right),
        (a, b) -> computeDiv((Long) a, (Long) b));
  }

  private static long computeDiv(long a, long b) {
    if ((a == Long.MIN_VALUE) && (b == -1)) {
      throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, null);
    }
    return a / b;
  }

  private void handleMod(MultiplicativeExpressionContext ctx) {
    setUpBinaryIntegralOperator(ctx, ctx.operator, fTypes.get(ctx.left), fTypes.get(ctx.right),
        (a, b) -> ((Long) a) % ((Long) b));
  }

  private void setUpBinaryIntegralOperator(ParseTree expressionNode, Token operatorToken, Type leftType, Type rightType,
      BinaryOperator<Object> operator) {
    if (!((leftType instanceof IntegralType) && (rightType instanceof IntegralType))) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, operatorToken);
    }
    fTypes.put(expressionNode, fIntegerType);
    fNodeData.binaryOperators().put(expressionNode, operator);
  }

  private void handleAnd(MultiplicativeExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if ((leftType instanceof BooleanType) && (rightType instanceof BooleanType)) {
      fTypes.put(ctx, fBooleanType);
    } else {
      setUpBinaryIntegralOperator(ctx, ctx.operator, leftType, rightType, (a, b) -> ((Long) a) & ((Long) b));
    }
  }

  @Override public void exitParenthesizedExpression(ParenthesizedExpressionContext ctx) {
    fTypes.put(ctx, fTypes.get(ctx.expression()));
    if (Boolean.TRUE.equals(fAssignableFlags.get(ctx.expression()))) {
      fAssignableFlags.put(ctx, Boolean.TRUE);
    }
  }

  @Override public void exitParameterList(ParameterListContext ctx) {
    ParameterList parameterList = new ParameterList();
    for (ParameterGroupContext group : ctx.parameterGroup()) {
      Type type = fTypes.get(group.type());
      ParameterMode mode = (group.VAR() == null) ? ParameterMode.VALUE : ParameterMode.REFERENCE;
      for (TerminalNode id : group.ID()) {
        parameterList.add(new ParameterDescriptor(id.getSymbol(), type, mode));
      }
    }
    fParameterLists.put(ctx, parameterList);
  }

  @Override public void exitForwardSubroutine(ForwardSubroutineContext ctx) {
    SubroutineHeaderContext header = ctx.subroutineHeader();
    Token name = header.ID().getSymbol();
    fCurrentScope.add(
        new Subroutine(
            name.getText(),
            header,
            fParameterLists.get(header.parameterList()),
            (header.type() == null) ? null : fTypes.get(header.type())),
        name);
  }

  @Override public void enterSubroutineBlock(SubroutineBlockContext ctx) {
    SubroutineHeaderContext header = ((SubroutineContext) ctx.parent).subroutineHeader();
    Token name = header.ID().getSymbol();
    ParameterList parameterList = fParameterLists.get(header.parameterList());
    Type resultType = (header.type() == null) ? null : fTypes.get(header.type());

    Subroutine s;
    Symbol symbol = fCurrentScope.getIfExistsInThisScope(name.getText());
    if ((symbol instanceof Subroutine)
        && (fNodeData.subroutineStatements().get(((Subroutine) symbol).header()) == null)) {
      s = (Subroutine) symbol;
      if (!(s.parameterList().sameAs(parameterList) && (s.resultType() == resultType))) {
        throw new ScriptException(String.format("'%s' does not match its forward declaration.", name.getText()), name);
      }
    } else {
      s = new Subroutine(name.getText(), header, parameterList, resultType);
      fCurrentScope.add(s, name);
    }

    fNodeData.subroutineStatements().put(s.header(), ctx.compoundStatement());
    fCurrentScope = new Scope(fCurrentScope, true);

    if (resultType != null) {
      fCurrentScope.add(new Variable("Result", resultType, true, fCurrentScope.allocateVariableAddress(), false), null);
    }

    for (ParameterDescriptor p : parameterList.descriptors()) {
      fCurrentScope.add(
          new Variable(
              p.name().getText(),
              p.type(),
              true,
              fCurrentScope.allocateVariableAddress(),
              p.mode() == ParameterMode.REFERENCE),
          p.name());
    }
  }

  @Override public void exitSubroutineBlock(SubroutineBlockContext ctx) {
    fNodeData.storageAllocators().put(ctx.compoundStatement(), createStorageAllocator(fCurrentScope));
    fCurrentScope = fCurrentScope.outerScope();
  }

  @Override public void exitCallExpression(CallExpressionContext ctx) {
    Symbol symbol = fCurrentScope.get(ctx.ID().getSymbol());

    if (symbol instanceof Subroutine) {
      setUpCallSite(ctx, (Subroutine) symbol, ctx.expression(), fNodeData.callSites(), fTypes);
    } else if (symbol instanceof NativeSubroutine) {
      setUpNativeCallSite(ctx, (NativeSubroutine) symbol, ctx.expression(), fNodeData.callSites(), fTypes);
    } else {
      throw new ScriptException(ScriptUtils.SYNTAX_ERROR, ctx.start);
    }
  }

  @Override public void exitCallStatement(CallStatementContext ctx) {
    Symbol symbol = fCurrentScope.get(ctx.ID().getSymbol());

    if (symbol instanceof Subroutine) {
      setUpCallSite(ctx, (Subroutine) symbol, ctx.expression(), fNodeData.callSites(), null);
    } else if (symbol instanceof NativeSubroutine) {
      setUpNativeCallSite(ctx, (NativeSubroutine) symbol, ctx.expression(), fNodeData.callSites(), null);
    } else {
      throw new ScriptException(ScriptUtils.SYNTAX_ERROR, ctx.start);
    }
  }

  private void setUpCallSite(
      ParserRuleContext callNode,
      Subroutine subroutine,
      List<ExpressionContext> arguments,
      ParseTreeProperty<Object> callSiteProperty,
      @Nullable ParseTreeProperty<Type> resultTypeProperty) {
    int parameterCount = subroutine.parameterList().descriptors().size();
    if (parameterCount != arguments.size()) {
      throw new ScriptException(INCORRECT_ARGUMENT_COUNT, callNode.start);
    }
    ArrayList<Assigner> parameterAssigners = new ArrayList<>(parameterCount);

    Iterator<ExpressionContext> argumentIterator = arguments.iterator();
    for (ParameterDescriptor p : subroutine.parameterList().descriptors()) {
      ExpressionContext argument = argumentIterator.next();
      Type argumentType = fTypes.get(argument);
      Assigner assigner;

      switch (p.mode()) {
        case VALUE: {
          assigner = p.type().tryMakeAssignmentFrom(argumentType);
          if (assigner == null) {
            throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.start);
          }
          break;
        }
        case REFERENCE: {
          if (!Boolean.TRUE.equals(fAssignableFlags.get(argument))) {
            throw new ScriptException(ScriptUtils.BAD_VAR_ARGUMENT, argument.start);
          }
          if (p.type() != argumentType) {
            throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, argument.start);
          }
          assigner = null;
          break;
        }
        default: {
          throw new RuntimeException();
        }
      }

      parameterAssigners.add(assigner);
    }

    callSiteProperty.put(callNode, new CallSite(subroutine.header(), parameterAssigners));

    if (resultTypeProperty != null) {
      if (subroutine.resultType() == null) {
        throw new ScriptException(NO_PROCEDURE_INSIDE_EXPRESSION, callNode.start);
      }
      resultTypeProperty.put(callNode, subroutine.resultType());
    }
  }

  private void setUpNativeCallSite(
      ParserRuleContext callNode,
      NativeSubroutine subroutine,
      List<ExpressionContext> arguments,
      ParseTreeProperty<Object> callSiteProperty,
      @Nullable ParseTreeProperty<Type> resultTypeProperty) {
    if (subroutine.parameterCount() != arguments.size()) {
      throw new ScriptException(INCORRECT_ARGUMENT_COUNT, callNode.start);
    }

    NativeCallArgument[] nativeCallArguments = arguments.stream()
        .map(e -> new NativeCallArgument(fTypes.get(e), Boolean.TRUE.equals(fAssignableFlags.get(e)), e.start))
        .toArray(NativeCallArgument[]::new);
    NativeCall nativeCall = subroutine.inspector().inspect(nativeCallArguments, this);

    callSiteProperty.put(callNode, nativeCall.callSite());

    if (resultTypeProperty != null) {
      if (nativeCall.resultType() == null) {
        throw new ScriptException(NO_PROCEDURE_INSIDE_EXPRESSION, callNode.start);
      }
      resultTypeProperty.put(callNode, nativeCall.resultType());
    }
  }

  @Override public void exitConstantDeclaration(ConstantDeclarationContext ctx) {
    Token name = ctx.ID().getSymbol();
    fCurrentScope.add(
        new Constant(
            name.getText(),
            fTypes.get(ctx.expression()),
            computeConstantExpression(ctx.expression())),
        name);
  }

  private Object computeConstantExpression(ExpressionContext expression) {
    return ScriptUtils.getValue(new ScriptComputer(fNodeData, true, null, null).visit(expression));
  }

  @Override public void exitTrueExpression(TrueExpressionContext ctx) {
    fTypes.put(ctx, fBooleanType);
  }

  @Override public void exitFalseExpression(FalseExpressionContext ctx) {
    fTypes.put(ctx, fBooleanType);
  }

  @Override public void exitIfStatement(IfStatementContext ctx) {
    if (!(fTypes.get(ctx.expression()) instanceof BooleanType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.expression().start);
    }
  }

  @Override public void exitRelationalExpression(RelationalExpressionContext ctx) {
    Type leftType = fTypes.get(ctx.left);
    Type rightType = fTypes.get(ctx.right);

    if (ctx.operator.getType() == ScriptParser.IN) {
      handleIn(ctx, leftType, rightType);
    } else if ((leftType instanceof SetType) && (rightType instanceof SetType)) {
      handleSetComparison(ctx, (SetType) leftType, (SetType) rightType);
    } else {
      fNodeData.binaryOperators().put(ctx, getComparator(leftType, rightType, ctx.operator));
      fTypes.put(ctx, fBooleanType);
    }
  }

  /** The result type of the returned comparator is {@code @Nullable Integer}. */
  private static BinaryOperator<Object> getComparator(Type leftType, Type rightType, @Nullable Token location) {
    BinaryOperator<Object> res;
    if ((leftType instanceof BooleanType) && (rightType instanceof BooleanType)) {
      res = (a, b) -> ((Boolean) a).compareTo((Boolean) b);
    } else if ((leftType instanceof EnumeratedType) && (rightType == leftType)) {
      res = (a, b) -> ((Integer) a).compareTo((Integer) b);
    } else if (ScriptUtils.isTextType(leftType) && ScriptUtils.isTextType(rightType)) {
      res = (a, b) -> ((String) a).compareTo((String) b);
    } else {
      res = chooseBinaryNumericOperator(
          leftType,
          rightType,
          (a, b) -> ((Long) a).compareTo((Long) b),
          (a, b) -> tryCompareDoubles((Double) a, (Double) b),
          (a, b) -> tryCompareDoubles((Double) a, ((Long) b).doubleValue()),
          (a, b) -> tryCompareDoubles(((Long) a).doubleValue(), (Double) b),
          location);
    }

    return res;
  }

  private static @Nullable Integer tryCompareDoubles(double a, double b) {
    Integer res;
    if (a < b) {
      res = -1;
    } else if (a > b) {
      res = 1;
    } else if (Double.isNaN(a) || Double.isNaN(b)) {
      res = null;
    } else {
      res = 0;
    }

    return res;
  }

  private void handleIn(RelationalExpressionContext ctx, Type leftType, Type rightType) {
    if (!(rightType instanceof SetType)) {
      throw new ScriptException("Set type expected.", ctx.right.start);
    }
    SetType setType = (SetType) rightType;

    boolean compatible;
    if (setType.isBlank()) {
      compatible = (leftType instanceof EnumeratedType)
          || (leftType instanceof IntegralType)
          || (leftType instanceof CharType);
    } else {
      compatible = setElementTypesCompatible(leftType, setType.elementType());
    }

    if (!compatible) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.operator);
    }

    fTypes.put(ctx, fBooleanType);
    fNodeData.binaryOperators().put(ctx, (a, b) -> computeIn(a, (BitSet) b));
  }

  private static boolean computeIn(Object element, BitSet set) {
    boolean outOfRange = (element instanceof Long) && !ScriptUtils.validIntegralSetElement((Long) element);
    return !outOfRange && set.get(ScriptUtils.getBitSetIndex(element, null));
  }

  private void handleSetComparison(RelationalExpressionContext ctx, SetType leftType, SetType rightType) {
    getCompatibleSetType(leftType, rightType, ctx.operator);

    BinaryOperator<Object> operator;
    switch (ctx.operator.getType()) {
      case ScriptParser.EQUAL: {
        operator = Object::equals;
        break;
      }
      case ScriptParser.NOT_EQUAL: {
        operator = (a, b) -> !a.equals(b);
        break;
      }
      case ScriptParser.LESS_OR_EQUAL: {
        operator = (a, b) -> Utils.bitSetIsSubset((BitSet) a, (BitSet) b);
        break;
      }
      case ScriptParser.GREATER_OR_EQUAL: {
        operator = (a, b) -> Utils.bitSetIsSubset((BitSet) b, (BitSet) a);
        break;
      }
      default: {
        throw new ScriptException("This operator does not apply to sets.", ctx.operator);
      }
    }

    fTypes.put(ctx, fBooleanType);
    fNodeData.binaryOperators().put(ctx, operator);
  }

  private static SetType getCompatibleSetType(SetType a, SetType b, @Nullable Token location) {
    SetType res = tryGetCompatibleSetType(a, b);
    if (res == null) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, location);
    }
    return res;
  }

  private static @Nullable SetType tryGetCompatibleSetType(SetType a, SetType b) {
    SetType res;
    if ((a == b) || b.isBlank()) {
      res = a;
    } else {
      res = a.isBlank() ? b : null;
    }

    return res;
  }

  @Override public void enterWhileStatement(WhileStatementContext ctx) {
    ++fLoopNestingLevel;
  }

  @Override public void exitWhileStatement(WhileStatementContext ctx) {
    if (!(fTypes.get(ctx.expression()) instanceof BooleanType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.expression().start);
    }
    --fLoopNestingLevel;
  }

  @Override public void exitBreakStatement(BreakStatementContext ctx) {
    if (fLoopNestingLevel == 0) {
      throw new ScriptException("'Break' is not allowed outside a loop.", ctx.start);
    }
  }

  @Override public void exitContinueStatement(ContinueStatementContext ctx) {
    if (fLoopNestingLevel == 0) {
      throw new ScriptException("'Continue' is not allowed outside a loop.", ctx.start);
    }
  }

  @Override public void enterRepeatStatement(RepeatStatementContext ctx) {
    ++fLoopNestingLevel;
  }

  @Override public void exitRepeatStatement(RepeatStatementContext ctx) {
    if (!(fTypes.get(ctx.expression()) instanceof BooleanType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.expression().start);
    }
    --fLoopNestingLevel;
  }

  @Override public void enterForStatement(ForStatementContext ctx) {
    ++fLoopNestingLevel;
  }

  @Override public void exitForStatement(ForStatementContext ctx) {
    if (!Boolean.TRUE.equals(fAssignableFlags.get(ctx.counter))) {
      throw new ScriptException("This expression cannot be used as a loop counter.", ctx.counter.start);
    }

    Type counterType = fTypes.get(ctx.counter);
    if (!(counterType instanceof IntegralType)) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.counter.start);
    }

    Assigner initialValueAssigner = counterType.tryMakeAssignmentFrom(fTypes.get(ctx.from));
    if (initialValueAssigner == null) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.from.start);
    }
    Assigner nextValueAssigner = Check.notNull(counterType.tryMakeAssignmentFrom(fIntegerType));

    if (!ScriptUtils.isNumericType(fTypes.get(ctx.to))) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.to.start);
    }

    fNodeData.forLoops().put(ctx, new ForLoop(initialValueAssigner, nextValueAssigner,
        (ctx.direction.getType() == ScriptParser.TO) ? 1 : -1));
    --fLoopNestingLevel;
  }

  @Override public void exitNotExpression(NotExpressionContext ctx) {
    Type type = fTypes.get(ctx.expression());

    Type resultType;
    UnaryOperator<Object> operator;
    if (type instanceof BooleanType) {
      resultType = fBooleanType;
      operator = a -> !((Boolean) a);
    } else if (type instanceof IntegralType) {
      resultType = fIntegerType;
      operator = a -> ~((Long) a);
    } else {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.start);
    }

    fTypes.put(ctx, resultType);
    fNodeData.unaryOperators().put(ctx, operator);
  }

  private void handleShift(MultiplicativeExpressionContext ctx) {
    setUpBinaryIntegralOperator(ctx, ctx.operator, fTypes.get(ctx.left), fTypes.get(ctx.right),
        (ctx.operator.getType() == ScriptParser.SHL) ?
            (n, distance) -> computeShl((Long) n, (Long) distance) :
            (n, distance) -> computeShr((Long) n, (Long) distance));
  }

  private static long computeShl(long n, long distance) {
    return computeShift(n, distance, true);
  }

  private static long computeShr(long n, long distance) {
    return computeShift(n, distance, false);
  }

  private static long computeShift(long n, long distance, boolean left) {
    if (distance < 0) {
      throw new ScriptException(String.format((Locale) null, "Negative shift distance: %d.", distance), null);
    }

    long res;
    if (distance >= 64) {
      res = 0;
    } else {
      res = left ? n << distance : n >>> distance;
    }

    return res;
  }

  @Override public void exitTypeDeclaration(TypeDeclarationContext ctx) {
    Token name = ctx.ID().getSymbol();
    fCurrentScope.add(
        new TypeSymbol(
            name.getText(),
            fTypes.get(ctx.type())),
        name);
  }

  @Override public void exitRecordType(RecordTypeContext ctx) {
    ArrayList<RecordField> fields = new ArrayList<>();

    for (FieldGroupContext fieldGroup : ctx.fieldGroup()) {
      Type fieldType = fTypes.get(fieldGroup.type());
      for (TerminalNode fieldName : fieldGroup.ID()) {
        fields.add(new RecordField(fieldName.getSymbol(), fieldType));
      }
    }

    fTypes.put(ctx, new RecordType(fields));
  }

  @Override public void exitFieldAccessExpression(FieldAccessExpressionContext ctx) {
    Type type = fTypes.get(ctx.expression());
    if (!(type instanceof RecordType)) {
      throw new ScriptException("Record type expected.", ctx.start);
    }

    RecordType recordType = (RecordType) type;
    int fieldIndex = recordType.getFieldIndex(ctx.ID().getSymbol());

    fTypes.put(ctx, recordType.fields().get(fieldIndex).type());
    fAssignableFlags.put(ctx, Boolean.TRUE);
    fNodeData.fieldIndices().put(ctx, fieldIndex);
  }

  @Override public void exitStaticArrayType(StaticArrayTypeContext ctx) {
    if (!ScriptUtils.isNumericType(fTypes.get(ctx.lowerBound))) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.lowerBound.start);
    }
    long lowerBound = ScriptUtils.getIntegralValueAt(computeConstantExpression(ctx.lowerBound), ctx.lowerBound.start);

    if (!ScriptUtils.isNumericType(fTypes.get(ctx.upperBound))) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.upperBound.start);
    }
    long upperBound = ScriptUtils.getIntegralValueAt(computeConstantExpression(ctx.upperBound), ctx.upperBound.start);

    if (lowerBound > upperBound) {
      throw new ScriptException("Negative array length.", ctx.lowerBound.start);
    }

    long length;
    try {
      length = LongMath.checkedAdd(LongMath.checkedSubtract(upperBound, lowerBound), 1);
      Check.that(length <= Integer.MAX_VALUE, ArithmeticException::new);
    } catch (ArithmeticException ignored) {
      throw new ScriptException("Array length is too big.", ctx.lowerBound.start);
    }

    fTypes.put(ctx, new StaticArrayType(fTypes.get(ctx.type()), (int) length, lowerBound, upperBound));
  }

  @Override public void exitElementAccessExpression(ElementAccessExpressionContext ctx) {
    Type type = fTypes.get(ctx.container);

    Type elementType;
    if (type instanceof StaticArrayType) {
      StaticArrayType arrayType = (StaticArrayType) type;
      elementType = arrayType.elementType();
      fNodeData.arrayLowerBounds().put(ctx, arrayType.lowerBound());
      fAssignableFlags.put(ctx, Boolean.TRUE);
    } else if (type instanceof DynamicArrayType) {
      elementType = ((DynamicArrayType) type).elementType();
      fAssignableFlags.put(ctx, Boolean.TRUE);
    } else if (type instanceof StringType) {
      elementType = fCharType;
    } else {
      throw new ScriptException("Array or String type expected.", ctx.start);
    }

    if (!ScriptUtils.isNumericType(fTypes.get(ctx.index))) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.index.start);
    }

    fTypes.put(ctx, elementType);
  }

  @Override public void exitDynamicArrayType(DynamicArrayTypeContext ctx) {
    Type elementType = fTypes.get(ctx.type());
    fTypes.put(ctx, fDynamicArrayTypesByElementType.computeIfAbsent(elementType, DynamicArrayType::new));
  }

  @Override public void exitEnumeratedType(EnumeratedTypeContext ctx) {
    List<TerminalNode> ids = ctx.ID();
    if (ids.size() > 256) {
      throw new ScriptException("Enumerated types cannot have more than 256 values.", ctx.start);
    }
    EnumeratedType type = new EnumeratedType(ids.size());

    int value = 0;
    for (TerminalNode id : ids) {
      Token name = id.getSymbol();
      fCurrentScope.add(
          new Constant(name.getText(), type, value),
          name);
      ++value;
    }

    fTypes.put(ctx, type);
  }

  @Override public void exitSetType(SetTypeContext ctx) {
    Type specifiedElementType = fTypes.get(ctx.type());

    Type elementType = null;
    if (specifiedElementType instanceof IntegralType) {
      IntegralType t = (IntegralType) specifiedElementType;
      if ((t.minValue() == 0) && (t.maxValue() == ScriptUtils.MAX_INTEGRAL_SET_ELEMENT)) {
        elementType = fIntegerType;
      }
    } else if ((specifiedElementType instanceof EnumeratedType) || (specifiedElementType instanceof CharType)) {
      elementType = specifiedElementType;
    }

    if (elementType == null) {
      throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, ctx.type().start);
    }

    fTypes.put(ctx, getSetType(elementType));
  }

  private SetType getSetType(@Nullable Type elementType) {
    return fSetTypesByElementType.computeIfAbsent(elementType, SetType::new);
  }

  @Override public void exitSetConstructorExpression(SetConstructorExpressionContext ctx) {
    List<ExpressionContext> elements = ctx.expression();

    Type elementType;
    if (elements.isEmpty()) {
      elementType = null;
    } else {
      Type firstElementType = fTypes.get(elements.get(0));
      if (firstElementType instanceof IntegralType) {
        elementType = fIntegerType;
      } else if ((firstElementType instanceof EnumeratedType) || (firstElementType instanceof CharType)) {
        elementType = firstElementType;
      } else {
        throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, elements.get(0).start);
      }

      for (ExpressionContext e : elements.subList(1, elements.size())) {
        if (!setElementTypesCompatible(fTypes.get(e), elementType)) {
          throw new ScriptException(ScriptUtils.INCOMPATIBLE_TYPES, e.start);
        }
      }
    }

    fTypes.put(ctx, getSetType(elementType));
  }

  private static boolean setElementTypesCompatible(Type a, Type b) {
    return ((a instanceof EnumeratedType) && (b == a))
        || ((a instanceof IntegralType) && (b instanceof IntegralType))
        || ((a instanceof CharType) && (b instanceof CharType));
  }

  @Override public void exitCaseStatement(CaseStatementContext ctx) {
    Type type = fTypes.get(ctx.expression());

    for (CaseBranchContext caseBranch : ctx.caseBranch()) {
      for (CaseTestContext caseTest : caseBranch.caseTest()) {
        ArrayList<BinaryOperator<Object>> comparators = new ArrayList<>();
        for (ExpressionContext e : caseTest.expression()) {
          comparators.add(getComparator(type, fTypes.get(e), e.start));
        }

        fNodeData.caseTests().put(caseTest, (comparators.size() == 1) ?
            CaseTest.createExact(comparators.get(0)) :
            CaseTest.createRanged(comparators.get(0), comparators.get(1)));
      }
    }
  }
}
