/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public final class RealType extends Type {
  private static final Double INITIAL_VALUE = 0.0;

  @Override public Supplier<Double> factory() {
    return () -> INITIAL_VALUE;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    Assigner res;
    if (sourceType instanceof RealType) {
      res = (source, target) -> target.set(source);
    } else if (sourceType instanceof IntegralType) {
      res = (source, target) -> target.set(Double.valueOf((Long) source));
    } else {
      res = null;
    }

    return res;
  }

  @Override public boolean assignableBySharing() {
    return true;
  }
}
