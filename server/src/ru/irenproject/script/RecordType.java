/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;

import com.google.common.base.Ascii;
import org.antlr.v4.runtime.Token;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public final class RecordType extends Type {
  private final ArrayList<RecordField> fFields;
  private final HashMap<String, Integer> fFieldIndices = new HashMap<>();

  private final Supplier<Object[]> fFactory;
  private final Assigner fAssigner;

  public RecordType(Collection<RecordField> fields) {
    fFields = new ArrayList<>(fields);

    for (int size = fFields.size(), i = 0; i < size; ++i) {
      Token fieldName = fFields.get(i).name();
      if (fFieldIndices.putIfAbsent(Ascii.toUpperCase(fieldName.getText()), i) != null) {
        throw new ScriptException(String.format("Duplicate field name '%s'.", fieldName.getText()), fieldName);
      }
    }

    List<Supplier<?>> fieldFactories = fFields.stream()
        .map(f -> f.type().factory())
        .collect(Collectors.toList());
    fFactory = () -> fieldFactories.stream()
        .map(Supplier::get)
        .toArray();

    List<Assigner> fieldAssigners = fFields.stream()
        .map(f -> Check.notNull(f.type().tryMakeAssignmentFrom(f.type())))
        .collect(Collectors.toList());
    fAssigner = (source, target) -> assign(source, target, fieldAssigners);
  }

  private static void assign(Object source, Reference target, List<Assigner> fieldAssigners) {
    Object[] sourceFields = (Object[]) source;
    Object[] targetFields = (Object[]) target.get();
    for (int i = 0; i < sourceFields.length; ++i) {
      fieldAssigners.get(i).assign(sourceFields[i], new Reference(targetFields, i));
    }
  }

  public List<RecordField> fields() {
    return Collections.unmodifiableList(fFields);
  }

  @Override public Supplier<Object[]> factory() {
    return fFactory;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    return (sourceType == this) ? fAssigner : null;
  }

  public int getFieldIndex(Token fieldName) {
    Integer index = fFieldIndices.get(Ascii.toUpperCase(fieldName.getText()));
    if (index == null) {
      throw new ScriptException(String.format("Field '%s' does not exist.", fieldName.getText()), fieldName);
    }
    return index;
  }

  @Override public boolean assignableBySharing() {
    return false;
  }
}
