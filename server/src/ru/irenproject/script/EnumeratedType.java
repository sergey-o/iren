/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public final class EnumeratedType extends Type {
  private static final Integer INITIAL_VALUE = 0;

  private final int fValueCount;

  public EnumeratedType(int valueCount) {
    fValueCount = valueCount;
  }

  public int valueCount() {
    return fValueCount;
  }

  @Override public Supplier<Integer> factory() {
    return () -> INITIAL_VALUE;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    return (sourceType == this) ? (source, target) -> target.set(source) : null;
  }

  @Override public boolean assignableBySharing() {
    return true;
  }
}
