/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;

import javax.annotation.Nullable;
import java.util.function.BinaryOperator;

public final class CaseTest {
  public static CaseTest createExact(BinaryOperator<Object> exactComparator) {
    return new CaseTest(exactComparator, null, null);
  }

  public static CaseTest createRanged(
      BinaryOperator<Object> lowerBoundComparator,
      BinaryOperator<Object> upperBoundComparator) {
    return new CaseTest(null, lowerBoundComparator, upperBoundComparator);
  }

  private final @Nullable BinaryOperator<Object> fExactComparator;
  private final @Nullable BinaryOperator<Object> fLowerBoundComparator;
  private final @Nullable BinaryOperator<Object> fUpperBoundComparator;

  private CaseTest(
      @Nullable BinaryOperator<Object> exactComparator,
      @Nullable BinaryOperator<Object> lowerBoundComparator,
      @Nullable BinaryOperator<Object> upperBoundComparator) {
    fExactComparator = exactComparator;
    fLowerBoundComparator = lowerBoundComparator;
    fUpperBoundComparator = upperBoundComparator;
  }

  public boolean exact() {
    return fExactComparator != null;
  }

  public BinaryOperator<Object> exactComparator() {
    return Check.notNull(fExactComparator);
  }

  public BinaryOperator<Object> lowerBoundComparator() {
    return Check.notNull(fLowerBoundComparator);
  }

  public BinaryOperator<Object> upperBoundComparator() {
    return Check.notNull(fUpperBoundComparator);
  }
}
