/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import com.google.common.base.Ascii;
import org.antlr.v4.runtime.Token;

import javax.annotation.Nullable;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;

public final class Scope {
  private final @Nullable Scope fOuterScope;
  private final boolean fLocal;

  private final LinkedHashMap<String, Symbol> fSymbols = new LinkedHashMap<>();
  private int fVariableStorageSize;

  public Scope(@Nullable Scope outerScope, boolean local) {
    fOuterScope = outerScope;
    fLocal = local;
  }

  public @Nullable Scope outerScope() {
    return fOuterScope;
  }

  public boolean local() {
    return fLocal;
  }

  public void add(Symbol symbol, @Nullable Token token) {
    if (fSymbols.putIfAbsent(Ascii.toUpperCase(symbol.name()), symbol) != null) {
      throw new ScriptException(String.format("Duplicate identifier '%s'.", symbol.name()), token);
    }
  }

  public Symbol get(Token name) {
    String canonicalName = Ascii.toUpperCase(name.getText());
    Scope scope = this;
    Symbol s;

    do {
      s = scope.fSymbols.get(canonicalName);
    } while ((s == null) && ((scope = scope.fOuterScope) != null));

    if (s == null) {
      throw new ScriptException(String.format("Unknown identifier '%s'.", name.getText()), name);
    }

    return s;
  }

  public @Nullable Symbol getIfExistsInThisScope(String name) {
    return fSymbols.get(Ascii.toUpperCase(name));
  }

  public Collection<Symbol> symbols() {
    return Collections.unmodifiableCollection(fSymbols.values());
  }

  public int allocateVariableAddress() {
    if (fVariableStorageSize == Integer.MAX_VALUE) {
      throw new ScriptException("Variable storage space exceeded.", null);
    }
    int res = fVariableStorageSize;
    ++fVariableStorageSize;
    return res;
  }
}
