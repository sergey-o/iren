/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.script.parser.ScriptBaseVisitor;
import ru.irenproject.script.parser.ScriptParser;
import ru.irenproject.script.parser.ScriptParser.AdditiveExpressionContext;
import ru.irenproject.script.parser.ScriptParser.AssignmentStatementContext;
import ru.irenproject.script.parser.ScriptParser.BreakStatementContext;
import ru.irenproject.script.parser.ScriptParser.CallExpressionContext;
import ru.irenproject.script.parser.ScriptParser.CallStatementContext;
import ru.irenproject.script.parser.ScriptParser.CaseBranchContext;
import ru.irenproject.script.parser.ScriptParser.CaseStatementContext;
import ru.irenproject.script.parser.ScriptParser.CaseTestContext;
import ru.irenproject.script.parser.ScriptParser.CompoundStatementAsStatementContext;
import ru.irenproject.script.parser.ScriptParser.CompoundStatementContext;
import ru.irenproject.script.parser.ScriptParser.ContinueStatementContext;
import ru.irenproject.script.parser.ScriptParser.ElementAccessExpressionContext;
import ru.irenproject.script.parser.ScriptParser.EmptyStatementContext;
import ru.irenproject.script.parser.ScriptParser.ExitStatementContext;
import ru.irenproject.script.parser.ScriptParser.ExpressionContext;
import ru.irenproject.script.parser.ScriptParser.FalseExpressionContext;
import ru.irenproject.script.parser.ScriptParser.FieldAccessExpressionContext;
import ru.irenproject.script.parser.ScriptParser.ForStatementContext;
import ru.irenproject.script.parser.ScriptParser.IdExpressionContext;
import ru.irenproject.script.parser.ScriptParser.IfStatementContext;
import ru.irenproject.script.parser.ScriptParser.IntegerLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.MultiplicativeExpressionContext;
import ru.irenproject.script.parser.ScriptParser.NotExpressionContext;
import ru.irenproject.script.parser.ScriptParser.ParenthesizedExpressionContext;
import ru.irenproject.script.parser.ScriptParser.ProgramContext;
import ru.irenproject.script.parser.ScriptParser.RealLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.RelationalExpressionContext;
import ru.irenproject.script.parser.ScriptParser.RepeatStatementContext;
import ru.irenproject.script.parser.ScriptParser.SetConstructorExpressionContext;
import ru.irenproject.script.parser.ScriptParser.SignExpressionContext;
import ru.irenproject.script.parser.ScriptParser.StatementContext;
import ru.irenproject.script.parser.ScriptParser.TextLiteralExpressionContext;
import ru.irenproject.script.parser.ScriptParser.TrueExpressionContext;
import ru.irenproject.script.parser.ScriptParser.WhileStatementContext;

import com.google.common.math.LongMath;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTree;

import javax.annotation.Nullable;
import java.lang.management.ManagementFactory;
import java.util.BitSet;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.function.BinaryOperator;
import java.util.function.LongPredicate;

import static ru.irenproject.script.ScriptUtils.getValue;

public final class ScriptComputer extends ScriptBaseVisitor<Object> {
  private final NodeData fNodeData;
  private final boolean fAllowConstantExpressionsOnly;
  private final @Nullable Random fRandomNumberGenerator;
  private final @Nullable Locale fLocale;

  private @Nullable Long fInitialCpuTimeNanoseconds;
  private /* @Nullable */Long fCpuTimeLimitNanoseconds;
  private int fSkippedCpuTimeLimitChecks;

  private @Nullable Object[] fGlobalStorage;
  private /* @Nullable */Object[] fLocalStorage;

  private final BreakException fBreakException = new BreakException();
  private final ContinueException fContinueException = new ContinueException();
  private final ExitException fExitException = new ExitException();

  public ScriptComputer(
      NodeData nodeData,
      boolean allowConstantExpressionsOnly,
      @Nullable Random randomNumberGenerator,
      @Nullable Locale locale) {
    fNodeData = nodeData;
    fAllowConstantExpressionsOnly = allowConstantExpressionsOnly;
    fRandomNumberGenerator = randomNumberGenerator;
    fLocale = locale;
  }

  public Random randomNumberGenerator() {
    return Check.notNull(fRandomNumberGenerator);
  }

  public Locale locale() {
    return Check.notNull(fLocale);
  }

  public @Nullable Object[] globalStorage() {
    return fGlobalStorage;
  }

  public void setCpuTimeLimitNanoseconds(long value) {
    long now;
    try {
      // undefined after ~292 CPU-years, don't use for interstellar missions
      now = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();
      if (now == -1) {
        throw new UnsupportedOperationException();
      }
    } catch (UnsupportedOperationException ignored) {
      throw new ScriptException("Cannot measure CPU time.", null);
    }
    fInitialCpuTimeNanoseconds = now;
    fCpuTimeLimitNanoseconds = value;
  }

  private void checkCpuTimeLimit() {
    if (fInitialCpuTimeNanoseconds != null) {
      ++fSkippedCpuTimeLimitChecks;
      if (fSkippedCpuTimeLimitChecks == 10_000) {
        fSkippedCpuTimeLimitChecks = 0;
        long now = ManagementFactory.getThreadMXBean().getCurrentThreadCpuTime();
        if (now - fInitialCpuTimeNanoseconds >= fCpuTimeLimitNanoseconds) {
          throw new ScriptException("Time limit exceeded.", null);
        }
      }
    }
  }

  private void ensureNonConstantExpressionAllowed(Token location) {
    if (fAllowConstantExpressionsOnly) {
      throw new ScriptException("Not a constant.", location);
    }
  }

  @Override public Object visit(ParseTree tree) {
    checkCpuTimeLimit();
    return super.visit(tree);
  }

  @Override public Object visitProgram(ProgramContext ctx) {
    fGlobalStorage = fNodeData.storageAllocators().get(ctx).get();
    executeBlockStatement(ctx.programBlock().compoundStatement());
    return null;
  }

  @Override public Object visitCompoundStatement(CompoundStatementContext ctx) {
    for (StatementContext statement : ctx.statement()) {
      visit(statement);
    }
    return null;
  }

  @Override public Object visitCompoundStatementAsStatement(CompoundStatementAsStatementContext ctx) {
    visit(ctx.compoundStatement());
    return null;
  }

  @Override public Object visitEmptyStatement(EmptyStatementContext ctx) {
    return null;
  }

  private void executeBlockStatement(CompoundStatementContext statement) {
    try {
      visit(statement);
    } catch (ExitException ignored) {}
  }

  @Override public Object visitIdExpression(IdExpressionContext ctx) {
    Object target = fNodeData.idExpressionTargets().get(ctx);

    Object res;
    if (target instanceof VariableLocation) {
      ensureNonConstantExpressionAllowed(ctx.start);
      VariableLocation v = (VariableLocation) target;
      res = new Reference(v.local() ? fLocalStorage : fGlobalStorage, v.address());
      if (v.containsReference()) {
        res = ((Reference) res).get();
      }
    } else if (target instanceof ComputedConstant) {
      res = ((ComputedConstant) target).value();
    } else if (target instanceof CallSite) {
      ensureNonConstantExpressionAllowed(ctx.start);
      res = callSubroutine((CallSite) target, Collections.emptyList());
    } else if (target instanceof NativeCallSite) {
      ensureNonConstantExpressionAllowed(ctx.start);
      res = callNativeSubroutine((NativeCallSite) target, Collections.emptyList(), ctx.start);
    } else {
      throw new RuntimeException();
    }

    return res;
  }

  @Override public Object visitRealLiteralExpression(RealLiteralExpressionContext ctx) {
    return fNodeData.literals().get(ctx);
  }

  @Override public Object visitIntegerLiteralExpression(IntegerLiteralExpressionContext ctx) {
    return fNodeData.literals().get(ctx);
  }

  @Override public Object visitTextLiteralExpression(TextLiteralExpressionContext ctx) {
    return fNodeData.literals().get(ctx);
  }

  @Override public Object visitAssignmentStatement(AssignmentStatementContext ctx) {
    Assigner assigner = fNodeData.assigners().get(ctx);
    if (assigner == null) { // special case: indexed String assignment
      ElementAccessExpressionContext left = (ElementAccessExpressionContext) ctx.left;
      Reference target = (Reference) visit(left.container);
      long index = ScriptUtils.getIntegralValueAt(visit(left.index), left.index.start);
      char newChar = ((String) getValue(visit(ctx.right))).charAt(0);

      String s = (String) target.get();
      int intIndex = checkStringIndex(s, index, left.index.start);
      target.set(Utils.replaceCharAt(s, intIndex - 1, newChar));
    } else {
      Reference target = (Reference) visit(ctx.left);
      Object source = getValue(visit(ctx.right));
      try {
        assigner.assign(source, target);
      } catch (ScriptException e) {
        throw e.hasToken() ? e : new ScriptException(e.getMessage(), ctx.right.start);
      }
    }

    return null;
  }

  @Override public Object visitSignExpression(SignExpressionContext ctx) {
    Object operand = getValue(visit(ctx.expression()));
    try {
      return fNodeData.unaryOperators().get(ctx).apply(operand);
    } catch (ArithmeticException ignored) {
      throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, ctx.operator);
    }
  }

  @Override public Object visitAdditiveExpression(AdditiveExpressionContext ctx) {
    Object left = getValue(visit(ctx.left));
    BinaryOperator<Object> operator = fNodeData.binaryOperators().get(ctx);

    Object res;
    if (operator == null) {
      res = ((Boolean) left) || ((Boolean) getValue(visit(ctx.right)));
    } else {
      Object right = getValue(visit(ctx.right));
      try {
        res = operator.apply(left, right);
      } catch (ArithmeticException ignored) {
        throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, ctx.operator);
      } catch (ScriptException e) {
        throw e.hasToken() ? e : new ScriptException(e.getMessage(), ctx.operator);
      }
    }

    return res;
  }

  @Override public Object visitMultiplicativeExpression(MultiplicativeExpressionContext ctx) {
    Object left = getValue(visit(ctx.left));
    BinaryOperator<Object> operator = fNodeData.binaryOperators().get(ctx);

    Object res;
    if (operator == null) {
      res = ((Boolean) left) && ((Boolean) getValue(visit(ctx.right)));
    } else {
      Object right = getValue(visit(ctx.right));
      try {
        res = operator.apply(left, right);
      } catch (ArithmeticException ignored) {
        throw new ScriptException((ctx.operator.getType() == ScriptParser.STAR) ?
            ScriptUtils.INTEGER_OVERFLOW : "Integer division by zero.", ctx.operator);
      } catch (ScriptException e) {
        throw e.hasToken() ? e : new ScriptException(e.getMessage(), ctx.operator);
      }
    }

    return res;
  }

  @Override public Object visitParenthesizedExpression(ParenthesizedExpressionContext ctx) {
    return visit(ctx.expression());
  }

  private @Nullable Object callSubroutine(CallSite callSite, List<ExpressionContext> arguments) {
    CompoundStatementContext statement = fNodeData.subroutineStatements().get(callSite.subroutineHeader());
    boolean hasResult = (callSite.subroutineHeader().type() != null);

    Object[] subroutineStorage = fNodeData.storageAllocators().get(statement).get();

    int currentParameterAddress = hasResult ? 1 : 0;
    Iterator<ExpressionContext> argumentIterator = arguments.iterator();
    for (Assigner assigner : callSite.parameterAssigners()) {
      ExpressionContext argument = argumentIterator.next();
      Object computedArgument = visit(argument);

      if (assigner == null) { // pass by reference
        subroutineStorage[currentParameterAddress] = computedArgument;
      } else { // pass by value
        try {
          assigner.assign(getValue(computedArgument), new Reference(subroutineStorage, currentParameterAddress));
        } catch (ScriptException e) {
          throw e.hasToken() ? e : new ScriptException(e.getMessage(), argument.start);
        }
      }

      ++currentParameterAddress;
    }

    Object res;
    Object[] oldLocalStorage = fLocalStorage;
    fLocalStorage = subroutineStorage;
    try {
      executeBlockStatement(statement);
      res = hasResult ? fLocalStorage[0] : null;
    } finally {
      fLocalStorage = oldLocalStorage;
    }

    return res;
  }

  private @Nullable Object callNativeSubroutine(NativeCallSite nativeCallSite, List<ExpressionContext> arguments,
      Token location) {
    Object[] computedArguments = new Object[arguments.size()];
    for (int i = 0; i < computedArguments.length; ++i) {
      computedArguments[i] = visit(arguments.get(i));
    }

    try {
      return nativeCallSite.compute(computedArguments, this);
    } catch (ScriptException e) {
      throw e.hasToken() ? e : new ScriptException(e.getMessage(), location);
    }
  }

  @Override public Object visitCallExpression(CallExpressionContext ctx) {
    ensureNonConstantExpressionAllowed(ctx.start);

    Object callSite = fNodeData.callSites().get(ctx);
    return (callSite instanceof CallSite) ?
        callSubroutine((CallSite) callSite, ctx.expression()) :
        callNativeSubroutine((NativeCallSite) callSite, ctx.expression(), ctx.start);
  }

  @Override public Object visitCallStatement(CallStatementContext ctx) {
    Object callSite = fNodeData.callSites().get(ctx);
    if (callSite instanceof CallSite) {
      callSubroutine((CallSite) callSite, ctx.expression());
    } else {
      callNativeSubroutine((NativeCallSite) callSite, ctx.expression(), ctx.start);
    }

    return null;
  }

  @Override public Object visitTrueExpression(TrueExpressionContext ctx) {
    return Boolean.TRUE;
  }

  @Override public Object visitFalseExpression(FalseExpressionContext ctx) {
    return Boolean.FALSE;
  }

  @Override public Object visitIfStatement(IfStatementContext ctx) {
    if ((Boolean) getValue(visit(ctx.expression()))) {
      visit(ctx.thenBranch);
    } else if (ctx.elseBranch != null) {
      visit(ctx.elseBranch);
    }

    return null;
  }

  @Override public Object visitRelationalExpression(RelationalExpressionContext ctx) {
    Object left = getValue(visit(ctx.left));
    Object right = getValue(visit(ctx.right));

    Boolean res;
    Object operatorResult = fNodeData.binaryOperators().get(ctx).apply(left, right);
    if (operatorResult instanceof Boolean) {
      res = (Boolean) operatorResult;
    } else {
      Integer comparisonResult = (Integer) operatorResult;
      res = (comparisonResult == null) ?
          (ctx.operator.getType() == ScriptParser.NOT_EQUAL) :
          getRelationalResult(ctx.operator, comparisonResult);
    }

    return res;
  }

  private static boolean getRelationalResult(Token operator, int comparisonResult) {
    switch (operator.getType()) {
      case ScriptParser.EQUAL: return comparisonResult == 0;
      case ScriptParser.NOT_EQUAL: return comparisonResult != 0;
      case ScriptParser.LESS: return comparisonResult < 0;
      case ScriptParser.LESS_OR_EQUAL: return comparisonResult <= 0;
      case ScriptParser.GREATER: return comparisonResult > 0;
      case ScriptParser.GREATER_OR_EQUAL: return comparisonResult >= 0;
      default: throw new RuntimeException();
    }
  }

  @Override public Object visitWhileStatement(WhileStatementContext ctx) {
    try {
      while ((Boolean) getValue(visit(ctx.expression()))) {
        try {
          visit(ctx.statement());
        } catch (ContinueException ignored) {}
      }
    } catch (BreakException ignored) {}

    return null;
  }

  @Override public Object visitBreakStatement(BreakStatementContext ctx) {
    throw fBreakException;
  }

  private static class StacklessException extends RuntimeException {
    StacklessException() {
      super(null, null, false, false);
    }
  }

  private static final class BreakException extends StacklessException {}

  @Override public Object visitContinueStatement(ContinueStatementContext ctx) {
    throw fContinueException;
  }

  private static final class ContinueException extends StacklessException {}

  @Override public Object visitRepeatStatement(RepeatStatementContext ctx) {
    try {
      do {
        try {
          for (StatementContext statement : ctx.statement()) {
            visit(statement);
          }
        } catch (ContinueException ignored) {}
      } while (!((Boolean) getValue(visit(ctx.expression()))));
    } catch (BreakException ignored) {}

    return null;
  }

  @Override public Object visitForStatement(ForStatementContext ctx) {
    ForLoop forLoop = fNodeData.forLoops().get(ctx);
    Reference counter = (Reference) visit(ctx.counter);

    Object from = getValue(visit(ctx.from));
    try {
      forLoop.initialValueAssigner().assign(from, counter);
    } catch (ScriptException e) {
      throw e.hasToken() ? e : new ScriptException(e.getMessage(), ctx.from.start);
    }

    long finalValue = ScriptUtils.getIntegralValueAt(visit(ctx.to), ctx.to.start);
    LongPredicate keepSpinning = (forLoop.step() > 0) ? v -> v <= finalValue : v -> v >= finalValue;

    long currentValue = (Long) counter.get();

    try {
      while (keepSpinning.test(currentValue)) {
        try {
          visit(ctx.statement());
        } catch (ContinueException ignored) {}

        try {
          currentValue = LongMath.checkedAdd((Long) counter.get(), forLoop.step());
        } catch (ArithmeticException ignored) {
          throw new ScriptException(ScriptUtils.INTEGER_OVERFLOW, ctx.counter.start);
        }

        forLoop.nextValueAssigner().assign(currentValue, counter);
      }
    } catch (BreakException ignored) {}

    return null;
  }

  @Override public Object visitNotExpression(NotExpressionContext ctx) {
    Object operand = getValue(visit(ctx.expression()));
    return fNodeData.unaryOperators().get(ctx).apply(operand);
  }

  @Override public Object visitExitStatement(ExitStatementContext ctx) {
    throw fExitException;
  }

  private static final class ExitException extends StacklessException {}

  @Override public Object visitFieldAccessExpression(FieldAccessExpressionContext ctx) {
    Object[] fields = (Object[]) getValue(visit(ctx.expression()));
    return new Reference(fields, fNodeData.fieldIndices().get(ctx));
  }

  @Override public Object visitElementAccessExpression(ElementAccessExpressionContext ctx) {
    Object container = getValue(visit(ctx.container));
    long index = ScriptUtils.getIntegralValueAt(visit(ctx.index), ctx.index.start);
    Object res;

    if (container instanceof String) {
      String s = (String) container;
      int intIndex = checkStringIndex(s, index, ctx.index.start);
      res = s.substring(intIndex - 1, intIndex);
    } else {
      Object[] elements = (Object[]) container;
      Long lowerBound = fNodeData.arrayLowerBounds().get(ctx);

      long physicalIndex;
      try {
        physicalIndex = (lowerBound == null) ? index : LongMath.checkedSubtract(index, lowerBound);
        Check.that((physicalIndex >= 0) && (physicalIndex < elements.length), ArithmeticException::new);
      } catch (ArithmeticException ignored) {
        throw new ScriptException(String.format((Locale) null, "Array index is out of range: %d.", index),
            ctx.index.start);
      }
      res = new Reference(elements, (int) physicalIndex);
    }

    return res;
  }

  private static int checkStringIndex(String s, long index, Token location) {
    if ((index <= 0) || (index > s.length())) {
      throw new ScriptException(String.format((Locale) null, "String index is out of range: %d.", index), location);
    }
    return (int) index;
  }

  @Override public Object visitSetConstructorExpression(SetConstructorExpressionContext ctx) {
    BitSet res = new BitSet();
    for (ExpressionContext e : ctx.expression()) {
      res.set(ScriptUtils.getBitSetIndex(getValue(visit(e)), e.start));
    }

    return res;
  }

  @Override public Object visitCaseStatement(CaseStatementContext ctx) {
    Object value = getValue(visit(ctx.expression()));

    StatementContext statement = ctx.statement(); // default to `else` if exists
    outer:
    for (CaseBranchContext caseBranch : ctx.caseBranch()) {
      for (CaseTestContext caseTest : caseBranch.caseTest()) {
        if (evaluateCaseTest(caseTest, value)) {
          statement = caseBranch.statement();
          break outer;
        }
      }
    }

    if (statement != null) {
      visit(statement);
    }

    return null;
  }

  private boolean evaluateCaseTest(CaseTestContext ctx, Object value) {
    CaseTest caseTest = fNodeData.caseTests().get(ctx);

    boolean res;
    if (caseTest.exact()) {
      Integer r = (Integer) caseTest.exactComparator().apply(value, getValue(visit(ctx.expression(0))));
      res = (r != null) && (r == 0);
    } else {
      Integer lr = (Integer) caseTest.lowerBoundComparator().apply(value, getValue(visit(ctx.expression(0))));
      res = (lr != null) && (lr >= 0);
      if (res) {
        Integer ur = (Integer) caseTest.upperBoundComparator().apply(value, getValue(visit(ctx.expression(1))));
        res = (ur != null) && (ur <= 0);
      }
    }

    return res;
  }
}
