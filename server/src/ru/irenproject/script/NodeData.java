/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.script.parser.ScriptParser.CompoundStatementContext;

import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.function.BinaryOperator;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public final class NodeData {
  private final ParseTreeProperty<Supplier<Object[]>> fStorageAllocators = new ParseTreeProperty<>();
  private final ParseTreeProperty<Assigner> fAssigners = new ParseTreeProperty<>();
  private final ParseTreeProperty<Object> fIdExpressionTargets = new ParseTreeProperty<>();
  private final ParseTreeProperty<Object> fLiterals = new ParseTreeProperty<>();
  private final ParseTreeProperty<UnaryOperator<Object>> fUnaryOperators = new ParseTreeProperty<>();
  private final ParseTreeProperty<BinaryOperator<Object>> fBinaryOperators = new ParseTreeProperty<>();
  private final ParseTreeProperty<CompoundStatementContext> fSubroutineStatements = new ParseTreeProperty<>();
  private final ParseTreeProperty<Object> fCallSites = new ParseTreeProperty<>();
  private final ParseTreeProperty<ForLoop> fForLoops = new ParseTreeProperty<>();
  private final ParseTreeProperty<Integer> fFieldIndices = new ParseTreeProperty<>();
  private final ParseTreeProperty<Long> fArrayLowerBounds = new ParseTreeProperty<>();
  private final ParseTreeProperty<CaseTest> fCaseTests = new ParseTreeProperty<>();

  public ParseTreeProperty<Supplier<Object[]>> storageAllocators() {
    return fStorageAllocators;
  }

  public ParseTreeProperty<Assigner> assigners() {
    return fAssigners;
  }

  public ParseTreeProperty<Object> idExpressionTargets() {
    return fIdExpressionTargets;
  }

  public ParseTreeProperty<Object> literals() {
    return fLiterals;
  }

  public ParseTreeProperty<UnaryOperator<Object>> unaryOperators() {
    return fUnaryOperators;
  }

  public ParseTreeProperty<BinaryOperator<Object>> binaryOperators() {
    return fBinaryOperators;
  }

  public ParseTreeProperty<CompoundStatementContext> subroutineStatements() {
    return fSubroutineStatements;
  }

  public ParseTreeProperty<Object> callSites() {
    return fCallSites;
  }

  public ParseTreeProperty<ForLoop> forLoops() {
    return fForLoops;
  }

  public ParseTreeProperty<Integer> fieldIndices() {
    return fFieldIndices;
  }

  public ParseTreeProperty<Long> arrayLowerBounds() {
    return fArrayLowerBounds;
  }

  public ParseTreeProperty<CaseTest> caseTests() {
    return fCaseTests;
  }
}
