/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.script.parser.ScriptLexer;
import ru.irenproject.script.parser.ScriptParser;
import ru.irenproject.script.parser.ScriptParser.ProgramContext;

import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.common.util.concurrent.ExecutionError;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.google.protobuf.ByteString;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import javax.annotation.Nullable;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public final class BasicScriptRunner {
  private static final int CACHE_EXPIRATION_SECONDS = 600;

  private static final ThreadLocal<Cache<ByteString, PreparedScript>> fPreparedScriptsByHash =
      ThreadLocal.withInitial(BasicScriptRunner::createCache);

  public static ScriptResult run(String script, Locale locale, @Nullable Long cpuTimeLimitNanoseconds,
      Random randomNumberGenerator) {
    ByteString hash = ByteString.copyFrom(Hashing.sha256().hashUnencodedChars(script).asBytes());
    PreparedScript preparedScript;
    try {
      preparedScript = fPreparedScriptsByHash.get().get(hash, () -> prepare(script));
    } catch (ExecutionException | UncheckedExecutionException | ExecutionError e) {
      Throwables.throwIfUnchecked(e.getCause());
      throw new RuntimeException(e.getCause());
    }

    ScriptComputer scriptComputer = new ScriptComputer(preparedScript.nodeData(), false, randomNumberGenerator, locale);
    if (cpuTimeLimitNanoseconds != null) {
      scriptComputer.setCpuTimeLimitNanoseconds(cpuTimeLimitNanoseconds);
    }

    scriptComputer.visit(preparedScript.program());

    return buildResult(preparedScript.globalScope(), scriptComputer.globalStorage(), locale);
  }

  private static Cache<ByteString, PreparedScript> createCache() {
    return CacheBuilder.newBuilder()
        .softValues()
        .expireAfterAccess(CACHE_EXPIRATION_SECONDS, TimeUnit.SECONDS)
        .concurrencyLevel(1)
        .build();
  }

  private static PreparedScript prepare(String script) {
    ScriptLexer lexer = new ScriptLexer(createCharStream(script));

    ScriptParser parser = new ScriptParser(new CommonTokenStream(lexer));
    parser.setErrorHandler(new BailErrorStrategy());
    parser.removeErrorListeners();

    ProgramContext program;
    try {
      program = parser.program();
    } catch (ParseCancellationException e) {
      throw new ScriptException(ScriptUtils.SYNTAX_ERROR, (e.getCause() instanceof RecognitionException) ?
          ((RecognitionException) e.getCause()).getOffendingToken() : null);
    }

    Scope systemScope = new Scope(null, false);
    ScriptExaminer examiner = new ScriptExaminer(systemScope);
    setUpSystemScope(systemScope, examiner);

    ParseTreeWalker.DEFAULT.walk(examiner, program);

    return new PreparedScript(program, examiner.nodeData(), examiner.globalScope());
  }

  @SuppressWarnings("deprecation")
  private static CharStream createCharStream(String source) {
    return new org.antlr.v4.runtime.ANTLRInputStream(source);
  }

  private static void setUpSystemScope(Scope scope, ScriptExaminer examiner) {
    scope.add(new TypeSymbol("Real", examiner.realType()), null);
    scope.add(new TypeSymbol("Extended", examiner.realType()), null);
    scope.add(new TypeSymbol("Double", examiner.realType()), null);
    scope.add(new TypeSymbol("Single", examiner.realType()), null);
    scope.add(new TypeSymbol("Integer", examiner.integerType()), null);
    scope.add(new TypeSymbol("Boolean", examiner.booleanType()), null);
    scope.add(new TypeSymbol("String", examiner.stringType()), null);
    scope.add(new TypeSymbol("Char", examiner.charType()), null);

    scope.add(new TypeSymbol("Byte", new IntegralType(false, 0xff)), null);
    scope.add(new TypeSymbol("ShortInt", new IntegralType(true, 0xff)), null);
    scope.add(new TypeSymbol("Word", new IntegralType(false, 0xffff)), null);
    scope.add(new TypeSymbol("SmallInt", new IntegralType(true, 0xffff)), null);
    scope.add(new TypeSymbol("Cardinal", new IntegralType(false, 0xffffffffL)), null);
    scope.add(new TypeSymbol("LongInt", new IntegralType(true, 0xffffffffL)), null);
    scope.add(new TypeSymbol("Int64", new IntegralType(true, 0xffffffffffffffffL)), null);

    Natives.register(scope);
  }

  private static ScriptResult buildResult(Scope globalScope, Object[] globalStorage, Locale locale) {
    ImmutableMap.Builder<String, String> variables = ImmutableMap.builder();
    for (Symbol s : globalScope.symbols()) {
      if (s instanceof Variable) {
        Variable v = (Variable) s;
        Object value = globalStorage[v.address()];

        String text;
        if (v.type() instanceof RealType) {
          Double d = (Double) value;
          if (!Double.isFinite(d)) {
            throw new ScriptException(String.format("Variable '%s' has non-numeric value '%s'.", v.name(), d), null);
          }
          text = ScriptUtils.formatReal(d, 3, locale);
        } else if (v.type() instanceof IntegralType) {
          text = value.toString();
        } else if (ScriptUtils.isTextType(v.type())) {
          text = (String) value;
          ensureValidUtf16(text, v);
        } else {
          text = null;
        }

        if (text != null) {
          variables.put(v.name(), text);
        }
      }
    }

    return new ScriptResult(variables.build());
  }

  private static void ensureValidUtf16(String s, Variable v) {
    int i = 0;
    int length = s.length();
    while (i < length) {
      char c = s.charAt(i);
      if (Character.isSurrogate(c)) {
        if (s.codePointAt(i) == c) {
          throw new ScriptException(String.format((Locale) null,
              "Value of '%s' contains unpaired UTF-16 surrogate %d%s.",
              v.name(), (int) c, (v.type() instanceof StringType) ? " at position " + (i + 1) : ""), null);
        }
        i += 2;
      } else {
        ++i;
      }
    }
  }

  private BasicScriptRunner() {}
}
