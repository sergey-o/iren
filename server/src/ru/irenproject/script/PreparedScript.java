/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.script.parser.ScriptParser.ProgramContext;

public final class PreparedScript {
  private final ProgramContext fProgram;
  private final NodeData fNodeData;
  private final Scope fGlobalScope;

  public PreparedScript(ProgramContext program, NodeData nodeData, Scope globalScope) {
    fProgram = program;
    fNodeData = nodeData;
    fGlobalScope = globalScope;
  }

  public ProgramContext program() {
    return fProgram;
  }

  public NodeData nodeData() {
    return fNodeData;
  }

  public Scope globalScope() {
    return fGlobalScope;
  }
}
