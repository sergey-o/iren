/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import com.google.common.math.DoubleMath;
import org.antlr.v4.runtime.Token;

import javax.annotation.Nullable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public final class ScriptUtils {
  public static final String SYNTAX_ERROR = "Syntax error.";
  public static final String INTEGER_OVERFLOW = "Integer overflow.";
  public static final String INCOMPATIBLE_TYPES = "Incompatible types.";
  public static final String BAD_VAR_ARGUMENT = "This expression cannot be passed as a 'var' parameter.";

  public static final int MAX_INTEGRAL_SET_ELEMENT = 255;

  private static final ThreadLocal<DecimalFormat> fRealFormatter =
      ThreadLocal.withInitial(ScriptUtils::createRealFormatter);

  private static DecimalFormat createRealFormatter() {
    DecimalFormat res = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ROOT));
    res.setRoundingMode(RoundingMode.HALF_UP);
    return res;
  }

  public static String formatReal(double value, int fractionDigits, Locale localeForDecimalMark) {
    DecimalFormat df = fRealFormatter.get();
    df.setMaximumFractionDigits(fractionDigits);

    DecimalFormatSymbols symbols = df.getDecimalFormatSymbols();
    DecimalFormatSymbols localizedSymbols = DecimalFormatSymbols.getInstance(localeForDecimalMark);
    symbols.setDecimalSeparator(localizedSymbols.getDecimalSeparator());
    df.setDecimalFormatSymbols(symbols);

    String s = df.format(value);
    return s.equals("-0") ? "0" : s;
  }

  public static Object getValue(Object maybeReference) {
    return (maybeReference instanceof Reference) ? ((Reference) maybeReference).get() : maybeReference;
  }

  public static long roundDoubleToLong(double d) {
    return DoubleMath.roundToLong(d, RoundingMode.DOWN);
  }

  public static boolean isNumericType(Type type) {
    return (type instanceof IntegralType) || (type instanceof RealType);
  }

  public static boolean isTextType(Type type) {
    return (type instanceof StringType) || (type instanceof CharType);
  }

  public static boolean isArrayType(Type type) {
    return (type instanceof StaticArrayType) || (type instanceof DynamicArrayType);
  }

  public static long getIntegralValue(Object valueOrReference) {
    return getIntegralValueAt(valueOrReference, null);
  }

  public static long getIntegralValueAt(Object valueOrReference, @Nullable Token location) {
    Object v = getValue(valueOrReference);

    long res;
    if (v instanceof Long) {
      res = (Long) v;
    } else {
      Double d = (Double) v;
      try {
        res = roundDoubleToLong(d);
      } catch (ArithmeticException ignored) {
        throw new ScriptException(getOutOfIntegerRangeMessage(d), location);
      }
    }

    return res;
  }

  public static String getOutOfIntegerRangeMessage(double value) {
    return String.format("Value '%s' exceeds Integer range.", value);
  }

  public static int checkNewStringLength(long length) {
    if (length < 0) {
      throw new ScriptException(String.format((Locale) null, "Negative string length: %d.", length), null);
    }
    if (length > Integer.MAX_VALUE) {
      throw new ScriptException(String.format((Locale) null, "String length is too big: %d.", length), null);
    }

    return (int) length;
  }

  public static int getBitSetIndex(Object value, @Nullable Token location) {
    int res;
    if (value instanceof Long) {
      long n = (Long) value;
      if (!validIntegralSetElement(n)) {
        throw new ScriptException(String.format((Locale) null, "Value is out of range: %d.", n), location);
      }
      res = (int) n;
    } else if (value instanceof Integer) { // enumerated type
      res = (Integer) value;
    } else if (value instanceof String) { // Char
      res = ((String) value).charAt(0);
    } else {
      throw new RuntimeException();
    }

    return res;
  }

  public static boolean validIntegralSetElement(long n) {
    return (n >= 0) && (n <= MAX_INTEGRAL_SET_ELEMENT);
  }

  private ScriptUtils() {}
}
