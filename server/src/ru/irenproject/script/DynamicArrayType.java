/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public final class DynamicArrayType extends Type {
  private static final Object[] INITIAL_VALUE = {};

  private final Type fElementType;
  private final Assigner fAssignerFromStaticArray;

  public DynamicArrayType(Type elementType) {
    fElementType = elementType;

    Supplier<?> elementFactory = fElementType.assignableBySharing() ? null : fElementType.factory();
    Assigner elementAssigner = fElementType.assignableBySharing() ?
        null : Check.notNull(fElementType.tryMakeAssignmentFrom(fElementType));
    fAssignerFromStaticArray = (source, target) -> assignFromStaticArray(source, target, elementFactory,
        elementAssigner);
  }

  private void assignFromStaticArray(Object source, Reference target, /* @Nullable */Supplier<?> elementFactory,
      @Nullable Assigner elementAssigner) {
    Object[] sourceArray = (Object[]) source;
    int length = sourceArray.length;
    Object[] targetArray = new Object[length];

    boolean elementsAssignableBySharing = (elementAssigner == null);
    if (elementsAssignableBySharing) {
      System.arraycopy(sourceArray, 0, targetArray, 0, length);
    } else {
      for (int i = 0; i < length; ++i) {
        targetArray[i] = elementFactory.get();
        elementAssigner.assign(sourceArray[i], new Reference(targetArray, i));
      }
    }

    target.set(targetArray);
  }

  public Type elementType() {
    return fElementType;
  }

  @Override public Supplier<Object[]> factory() {
    return () -> INITIAL_VALUE;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    Assigner res;
    if (sourceType == this) {
      res = (source, target) -> target.set(source);
    } else if ((sourceType instanceof StaticArrayType)
        && (((StaticArrayType) sourceType).elementType() == fElementType)) {
      res = fAssignerFromStaticArray;
    } else {
      res = null;
    }

    return res;
  }

  @Override public boolean assignableBySharing() {
    return true;
  }
}
