/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import javax.annotation.Nullable;

public final class NativeCall {
  private final @Nullable Type fResultType;
  private final NativeCallSite fCallSite;

  public NativeCall(@Nullable Type resultType, NativeCallSite callSite) {
    fResultType = resultType;
    fCallSite = callSite;
  }

  public @Nullable Type resultType() {
    return fResultType;
  }

  public NativeCallSite callSite() {
    return fCallSite;
  }
}
