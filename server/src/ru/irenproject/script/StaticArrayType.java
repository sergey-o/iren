/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.script;

import ru.irenproject.Check;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.function.Supplier;

public final class StaticArrayType extends Type {
  private final Type fElementType;
  private final long fLowerBound;
  private final long fUpperBound;

  private final Supplier<Object[]> fFactory;
  private final Assigner fAssigner;

  public StaticArrayType(Type elementType, int length, long lowerBound, long upperBound) {
    fElementType = elementType;
    fLowerBound = lowerBound;
    fUpperBound = upperBound;

    Supplier<?> elementFactory = fElementType.factory();
    boolean elementsAssignableBySharing = fElementType.assignableBySharing();
    fFactory = () -> create(length, elementFactory, elementsAssignableBySharing);

    Assigner elementAssigner = elementsAssignableBySharing ?
        null : Check.notNull(fElementType.tryMakeAssignmentFrom(fElementType));
    fAssigner = (source, target) -> assign(source, target, elementAssigner);
  }

  private static Object[] create(int length, Supplier<?> elementFactory, boolean elementsAssignableBySharing) {
    Object[] res = new Object[length];

    if (elementsAssignableBySharing) {
      Arrays.fill(res, elementFactory.get());
    } else {
      Arrays.setAll(res, ignored -> elementFactory.get());
    }

    return res;
  }

  private static void assign(Object source, Reference target, @Nullable Assigner elementAssigner) {
    Object[] sourceElements = (Object[]) source;
    Object[] targetElements = (Object[]) target.get();
    int length = sourceElements.length;

    if (elementAssigner == null) {
      System.arraycopy(sourceElements, 0, targetElements, 0, length);
    } else {
      for (int i = 0; i < length; ++i) {
        elementAssigner.assign(sourceElements[i], new Reference(targetElements, i));
      }
    }
  }

  public Type elementType() {
    return fElementType;
  }

  public long lowerBound() {
    return fLowerBound;
  }

  public long upperBound() {
    return fUpperBound;
  }

  @Override public Supplier<Object[]> factory() {
    return fFactory;
  }

  @Override public @Nullable Assigner tryMakeAssignmentFrom(Type sourceType) {
    return (sourceType == this) ? fAssigner : null;
  }

  @Override public boolean assignableBySharing() {
    return false;
  }
}
