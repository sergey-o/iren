/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.archive.Proto.RecordedSession;
import ru.irenproject.archive.Proto.RecordedWork;
import ru.irenproject.archiveEditor.Proto.SessionCard;
import ru.irenproject.archiveEditor.Proto.SessionSortField;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionOrder;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkScreen;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkScreen.Close;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkScreen.ExportToTsv;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkScreen.OpenSession;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkScreen.Sort;
import ru.irenproject.editor.Action;
import ru.irenproject.infra.Freezable;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;
import ru.irenproject.work.Proto.SessionConnection;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.primitives.Ints;
import com.google.common.primitives.UnsignedBytes;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.Timestamps;
import org.jetbrains.annotations.Contract;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.CollationKey;
import java.text.Collator;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Locale;

public final class ArchiveWorkScreen extends Widget {
  public interface Factory {
    ArchiveWorkScreen create(Realm realm, RecordedWork work, Locale locale);
  }

  private final RecordedWork fWork;
  private final Locale fLocale;
  private final ArrayList<Session> fSessions;
  private final ArchiveSessionOrder fOrder;

  private transient @Nullable Collator fCollator;

  @Inject private ArchiveSessionScreen.Factory fArchiveSessionScreenFactory;

  @Inject private ArchiveWorkScreen(@Assisted Realm realm, @Assisted RecordedWork work, @Assisted Locale locale) {
    super(realm);
    fWork = work;
    fLocale = locale;

    fSessions = new ArrayList<>(fWork.getSessionCount());
    for (int n = fWork.getSessionCount(), i = 0; i < n; ++i) {
      fSessions.add(new Session(fWork.getSession(i), i));
    }
    fOrder = new ArchiveSessionOrder(realm());
    sort();
  }

  @Freezable private static final class Session {
    final RecordedSession $;
    final int fIndex;
    final @Nullable BigDecimal fScore;

    transient @Nullable CollationKey fUserDisplayNameCollationKey;
    transient @Nullable CollationKey fGroupNameCollationKey;
    transient @Nullable byte[] fAddressCollationKey;

    Session(RecordedSession recordedSession, int index) {
      $ = recordedSession;
      fIndex = index;

      if ($.hasScore()) {
        fScore = Check.inputNotNull(Utils.tryParseDecimal($.getScore()));
        Check.input(fScore.signum() >= 0);

        Check.input($.hasAttemptedScore());
        Check.input(fScore.compareTo(BigDecimal.valueOf($.getAttemptedScore())) <= 0);

        Check.input($.hasPerfectScore());
        Check.input($.getPerfectScore() > 0);
        Check.input($.getAttemptedScore() <= $.getPerfectScore());
      } else {
        fScore = null;
      }
    }

    boolean hasUserDisplayName() {
      return $.hasUserDisplayName() || $.hasUserName();
    }

    String userDisplayName() {
      return $.hasUserDisplayName() ? $.getUserDisplayName() : $.getUserName();
    }

    @Nullable CollationKey getUserDisplayNameCollationKey(Collator collator) {
      if ((fUserDisplayNameCollationKey == null) && hasUserDisplayName()) {
        fUserDisplayNameCollationKey = collator.getCollationKey(userDisplayName());
      }
      return fUserDisplayNameCollationKey;
    }

    @Nullable CollationKey getGroupNameCollationKey(Collator collator) {
      if ((fGroupNameCollationKey == null) && $.hasGroupName()) {
        fGroupNameCollationKey = collator.getCollationKey($.getGroupName());
      }
      return fGroupNameCollationKey;
    }

    @Nullable Result result() {
      return (fScore == null) ? null : new Result(fScore, $.getPerfectScore());
    }

    @Contract(pure = true)
    @Nullable Timestamp openedAt() {
      return ($.getConnectionCount() > 0) ? $.getConnection(0).getOpenedAt() : null;
    }

    @Nullable byte[] addressCollationKey() {
      if ((fAddressCollationKey == null) && ($.getConnectionCount() > 0)) {
        fAddressCollationKey = Utils.getIpAddressCollationKey($.getConnection(0).getAddress());
      }
      return fAddressCollationKey;
    }
  }

  private static final class Result implements Comparable<Result> {
    final BigDecimal fScore;
    final long fPerfectScore;

    Result(BigDecimal score, long perfectScore) {
      fScore = score;
      fPerfectScore = perfectScore;
    }

    @Override public int compareTo(Result other) {
      return (fPerfectScore == other.fPerfectScore) ? fScore.compareTo(other.fScore) :
          fScore.multiply(BigDecimal.valueOf(other.fPerfectScore)).compareTo(
              other.fScore.multiply(BigDecimal.valueOf(fPerfectScore)));
    }
  }

  @Override public Message render(RenderContext context) {
    return XArchiveWorkScreen.newBuilder()
        .setTestName(fWork.getTestName())
        .setArchivedAt(fWork.getArchivedAt())
        .addAllSession(Lists.transform(fSessions, ArchiveWorkScreen::renderSession))
        .setOrder(context.link(fOrder))
        .build();
  }

  private static SessionCard renderSession(Session s) {
    SessionCard.Builder b = SessionCard.newBuilder()
        .setBanned(s.$.getBanned()/* or default */);

    if (s.hasUserDisplayName()) {
      b.setUserDisplayName(s.userDisplayName());
    }
    if (s.$.hasGroupName()) {
      b.setGroupName(s.$.getGroupName());
    }

    if (s.fScore != null) {
      BigDecimal perfectScore = BigDecimal.valueOf(s.$.getPerfectScore());
      BigDecimal percentCorrect = getPercentRatio(s.fScore, perfectScore, 2);
      BigDecimal percentAttempted = getPercentRatio(BigDecimal.valueOf(s.$.getAttemptedScore()), perfectScore, 2);
      b
          .setPercentCorrect(percentCorrect.toPlainString())
          .setPercentWrong(percentAttempted.subtract(percentCorrect).toPlainString());
    }

    if (s.$.hasMark()) {
      b.setMark(s.$.getMark());
    }

    if (s.$.getConnectionCount() > 0) {
      SessionConnection connection = s.$.getConnection(0);
      b
          .setOpenedAt(connection.getOpenedAt())
          .setAddress(connection.getAddress());
    }

    return b.build();
  }

  private static BigDecimal getPercentRatio(BigDecimal a, BigDecimal b, int fractionalPercentDigits) {
    return a.divide(b, 2 + fractionalPercentDigits, RoundingMode.DOWN).movePointRight(2);
  }

  @Action public void doClose(Close in) {
    dismissScreen();
  }

  @Action public void doSort(Sort in) {
    fOrder.fSortField = in.getSortField();
    fOrder.fSortAscending = in.getSortAscending();
    sort();
  }

  @Action public void doOpenSession(OpenSession in) {
    Check.input(in.getIndex() >= 0);
    Check.input(in.getIndex() < fSessions.size());

    RecordedSession session = fWork.getSession(in.getIndex());
    if (session.getQuestionCount() > 0) {
      showScreen(fArchiveSessionScreenFactory.create(realm(), session, fWork));
    }
  }

  private void sort() {
    Comparator<Session> comparator = mainComparator();
    if (fOrder.fSortField != SessionSortField.USER_NAME) {
      comparator = comparator.thenComparing(s -> s.getUserDisplayNameCollationKey(collator()),
          Comparator.nullsLast(Comparator.naturalOrder()));
    }

    fOrder.fIndices = fSessions.stream()
        .sorted(comparator)
        .mapToInt(s -> s.fIndex)
        .toArray();
    fOrder.forceRender();
  }

  private Comparator<Session> mainComparator() {
    switch (fOrder.fSortField) {
      case USER_NAME: return Comparator.comparing(s -> s.getUserDisplayNameCollationKey(collator()),
          wrap(Comparator.naturalOrder()));
      case GROUP_NAME: return Comparator.comparing(s -> s.getGroupNameCollationKey(collator()),
          wrap(Comparator.naturalOrder()));
      case RESULT: return Comparator.comparing(Session::result, wrap(Comparator.naturalOrder()));
      case OPENED_AT: return Comparator.comparing(Session::openedAt, wrap(Timestamps.comparator()));
      case ADDRESS: return Comparator.comparing(Session::addressCollationKey,
          wrap(UnsignedBytes.lexicographicalComparator()));
      default: throw new RuntimeException();
    }
  }

  private <T> Comparator<T> wrap(Comparator<? super T> comparator) {
    return Comparator.nullsLast(fOrder.fSortAscending ? comparator : comparator.reversed());
  }

  private Collator collator() {
    if (fCollator == null) {
      fCollator = Collator.getInstance(fLocale);
    }
    return fCollator;
  }

  private static final class ArchiveSessionOrder extends Widget {
    SessionSortField fSortField = SessionSortField.RESULT;
    boolean fSortAscending;
    int[] fIndices;

    ArchiveSessionOrder(Realm realm) {
      super(realm);
    }

    @Override public Message render(RenderContext context) {
      return XArchiveSessionOrder.newBuilder()
          .setSortField(fSortField)
          .setSortAscending(fSortAscending)
          .addAllIndex(Ints.asList(fIndices))
          .build();
    }

    void forceRender() {
      markForRender();
    }
  }

  @Action public ExportToTsv.Reply doExportToTsv(ExportToTsv in) {
    Check.input(in.getHeaderCount() == 5);

    DateTimeFormatter openedAtFormatter = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss")
        .withZone(Utils.parseTimeZone(in.getTimeZone(), BadInputException::new));

    StringBuilder tsv = new StringBuilder();
    Joiner tabber = Joiner.on('\t');
    CharMatcher unexportable = CharMatcher.anyOf("\t\n\r");

    tabber.appendTo(tsv, in.getHeaderList());
    tsv.append('\n');

    for (int index : fOrder.fIndices) {
      Session s = fSessions.get(index);

      tabber.appendTo(tsv,
          s.hasUserDisplayName() ? unexportable.removeFrom(s.userDisplayName()) : "",
          s.$.hasGroupName() ? unexportable.removeFrom(s.$.getGroupName()) : "",
          (s.fScore != null) ? getPercentRatio(s.fScore, BigDecimal.valueOf(s.$.getPerfectScore()), 0) : "",
          s.$.hasMark() ? unexportable.removeFrom(s.$.getMark()) : "",
          (s.openedAt() == null) ? "" : openedAtFormatter.format(Utils.timestampToInstant(s.openedAt())));
      tsv.append('\n');
    }

    return ExportToTsv.Reply.newBuilder()
        .setTsv(tsv.toString())
        .build();
  }
}
