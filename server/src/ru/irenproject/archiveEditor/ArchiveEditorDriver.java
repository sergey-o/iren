/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.BadInputException;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.archive.BadArchiveVersionException;
import ru.irenproject.common.export.Proto.ExportOptions;
import ru.irenproject.common.export.Proto.ExportResult;
import ru.irenproject.editor.EditorDriver;
import ru.irenproject.editor.EditorProcessor;
import ru.irenproject.infra.BlobStore;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;

import com.google.inject.assistedinject.Assisted;
import org.apache.commons.compress.archivers.zip.ZipFile;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Locale;

public final class ArchiveEditorDriver extends EditorDriver {
  public interface Factory {
    ArchiveEditorDriver create(Path archiveFile);
  }

  private final Path fArchiveFile;
  private final ArchiveEditor.Factory fArchiveEditorFactory;

  private @Nullable ArchiveBlobStore fArchiveBlobStore;
  private @Nullable ZipFile fZip;

  @Inject private ArchiveEditorDriver(
      @Assisted Path archiveFile,
      ArchiveEditor.Factory archiveEditorFactory) {
    fArchiveFile = archiveFile;
    fArchiveEditorFactory = archiveEditorFactory;
  }

  @Override public void close() {
    ZipFile.closeQuietly(fZip);
  }

  @Override public BlobStore blobStore() {
    if (fArchiveBlobStore == null) {
      fArchiveBlobStore = new ArchiveBlobStore(this::zip);
    }
    return fArchiveBlobStore;
  }

  @Override public void save(EditorProcessor editorProcessor, Path path) {
    throw new BadInputException();
  }

  private ZipFile zip() {
    if (fZip == null) {
      try {
        fZip = new ZipFile(fArchiveFile.toFile());
      } catch (IOException e) {
        throw new BadInputException(e);
      }
    }
    return fZip;
  }

  public ArchiveEditor open(Realm realm, Locale locale) {
    try {
      ArchiveUtils.checkVersion(zip());
    } catch (BadArchiveVersionException e) {
      throw e.tooNew() ? new UnknownFileVersionException(e) : new BadInputException(e);
    }

    return fArchiveEditorFactory.create(realm, locale);
  }

  @Override public void handleGlobalEvent(Event event) {
    if (event instanceof ArchiveEditor.GetArchiveEvent) {
      ((ArchiveEditor.GetArchiveEvent) event).setArchive(zip());
    }
  }

  @Override public boolean autoFreezable() {
    return false;
  }

  @Override public ExportResult exportToHtml(EditorProcessor editorProcessor, ExportOptions options, Path outputFile) {
    throw new BadInputException();
  }
}
