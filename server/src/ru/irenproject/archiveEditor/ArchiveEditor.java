/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.Check;
import ru.irenproject.archiveEditor.Proto.XArchiveEditor;
import ru.irenproject.editor.DocumentEditor;
import ru.irenproject.infra.GlobalOnlyEvent;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.apache.commons.compress.archivers.zip.ZipFile;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Locale;
import java.util.function.Consumer;

public final class ArchiveEditor extends DocumentEditor {
  public interface Factory {
    ArchiveEditor create(Realm realm, Locale locale);
  }

  public static final class GetArchiveEvent implements GlobalOnlyEvent {
    public static ZipFile send(Consumer<GetArchiveEvent> consumer) {
      GetArchiveEvent e = new GetArchiveEvent();
      consumer.accept(e);
      return Check.notNull(e.fArchive);
    }

    private @Nullable ZipFile fArchive;

    public void setArchive(ZipFile archive) {
      fArchive = archive;
    }
  }

  @Inject private ArchiveEditor(
      @Assisted Realm realm,
      @Assisted Locale locale,
      ArchiveWorkSelectorScreen.Factory archiveWorkSelectorScreenFactory) {
    super(realm);
    pushScreen(archiveWorkSelectorScreenFactory.create(realm, locale));
  }

  @Override public Message render(RenderContext context) {
    return XArchiveEditor.newBuilder()
        .setScreen(context.link(topScreen()))
        .build();
  }
}
