/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.archive.Proto.RecordedWork;
import ru.irenproject.archiveEditor.Proto.WorkCard;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkSelectorScreen;
import ru.irenproject.archiveEditor.Proto.XArchiveWorkSelectorScreen.OpenWork;
import ru.irenproject.editor.Action;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;
import ru.irenproject.live.JsonProtoRegistry;

import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import com.google.protobuf.Timestamp;
import com.google.protobuf.UninitializedMessageException;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.Timestamps;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class ArchiveWorkSelectorScreen extends Widget {
  public interface Factory {
    ArchiveWorkSelectorScreen create(Realm realm, Locale locale);
  }

  private static final Pattern ENTRY_NAME_PATTERN = Pattern.compile("\\[([^] ]++)[^]]*+] (.*)");

  private static final DateTimeFormatter ENTRY_NAME_TIMESTAMP_PARSER = DateTimeFormatter.ofPattern(
      ArchiveUtils.ENTRY_NAME_TIMESTAMP_PATTERN);

  private final LinkedHashMap<String, WorkCard> fWorksByName = new LinkedHashMap<>();
  private final Locale fLocale;

  @Inject private ArchiveWorkScreen.Factory fArchiveWorkScreenFactory;
  @Inject private JsonProtoRegistry fJsonProtoRegistry;

  @Inject private ArchiveWorkSelectorScreen(@Assisted Realm realm, @Assisted Locale locale) {
    super(realm);
    fLocale = locale;
    listWorks();
  }

  private void listWorks() {
    ArrayList<WorkCard> works = new ArrayList<>();

    Enumeration<ZipArchiveEntry> entries = archive().getEntries();
    while (entries.hasMoreElements()) {
      ZipArchiveEntry entry = entries.nextElement();
      String name = entry.getName();
      if (!name.contains("/")) {
        Matcher m = ENTRY_NAME_PATTERN.matcher(name);
        Check.that(m.matches(), () -> new BadInputException(String.format("Incorrect work entry: '%s'.", name)));

        works.add(WorkCard.newBuilder()
            .setName(name)
            .setTitle(m.group(2))
            .setArchivedAt(parseTimestamp(m.group(1)))
            .build());
      }
    }

    works.sort(Comparator.comparing(WorkCard::getArchivedAt, Timestamps.comparator().reversed()));

    fWorksByName.clear();
    for (WorkCard w : works) {
      fWorksByName.put(w.getName(), w);
    }
  }

  private static Timestamp parseTimestamp(String s) {
    Instant instant;
    try {
      instant = ENTRY_NAME_TIMESTAMP_PARSER.parse(s, Instant::from);
    } catch (DateTimeParseException e) {
      throw new BadInputException(e);
    }

    try {
      return Utils.instantToTimestamp(instant);
    } catch (IllegalArgumentException e) {
      throw new BadInputException(e);
    }
  }

  private ZipFile archive() {
    return ArchiveEditor.GetArchiveEvent.send(this::post);
  }

  @Override public Message render(RenderContext context) {
    return XArchiveWorkSelectorScreen.newBuilder()
        .addAllWork(fWorksByName.values())
        .build();
  }

  @Action public void doOpenWork(OpenWork in) {
    Check.input(fWorksByName.containsKey(in.getName()));
    ZipArchiveEntry entry = Check.inputNotNull(archive().getEntry(in.getName()));

    RecordedWork.Builder b = RecordedWork.newBuilder();
    try (
        InputStream inputStream = archive().getInputStream(entry);
        Reader reader = new InputStreamReader(inputStream, StandardCharsets.UTF_8)) {
      JsonFormat.parser().usingTypeRegistry(fJsonProtoRegistry.get()).merge(reader, b);
    } catch (IOException e) {
      throw new BadInputException(e);
    }

    RecordedWork work;
    try {
      work = b.build();
    } catch (UninitializedMessageException e) {
      throw new BadInputException(e);
    }

    showScreen(fArchiveWorkScreenFactory.create(realm(), work, fLocale));
  }
}
