/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Question;
import ru.irenproject.QuestionItem;
import ru.irenproject.Utils;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.archive.Proto.RecordedQuestion;
import ru.irenproject.archive.Proto.RecordedSession;
import ru.irenproject.archive.Proto.RecordedWork;
import ru.irenproject.archiveEditor.ArchiveEditor.GetArchiveEvent;
import ru.irenproject.archiveEditor.Proto.ArchiveQuestionContent;
import ru.irenproject.archiveEditor.Proto.Page;
import ru.irenproject.archiveEditor.Proto.XArchiveQuestionViewer;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionScreen;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionScreen.Close;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionScreen.SelectPage;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionScreen.SelectQuestion;
import ru.irenproject.archiveEditor.Proto.XArchiveSessionScreenState;
import ru.irenproject.authentication.AuthenticationUtils;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.common.miscTypes.Proto.QuestionStatus;
import ru.irenproject.editor.Action;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.RenderContext;
import ru.irenproject.infra.Widget;
import ru.irenproject.itx.ExternallyStoredObjectReader;
import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxInput;
import ru.irenproject.live.LiveQuestion;
import ru.irenproject.live.LiveQuestionLoader;

import com.google.common.base.MoreObjects;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.Message;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.zip.ZipException;

public final class ArchiveSessionScreen extends Widget {
  public interface Factory {
    ArchiveSessionScreen create(Realm realm, RecordedSession session, RecordedWork work);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(ArchiveSessionScreen.class);

  private final RecordedSession fSession;
  private final RecordedWork fWork;
  private final ArrayList</* @Nullable */BigDecimal> fResults;
  private final ArchiveQuestionViewer fQuestionViewer;
  private final ArchiveSessionScreenState fState;

  private int fQuestionIndex;

  @Inject private LiveQuestionLoader fLiveQuestionLoader;
  @Inject private ItxInput fItxInput;

  @Inject private ArchiveSessionScreen(
      @Assisted Realm realm,
      @Assisted RecordedSession session,
      @Assisted RecordedWork work) {
    super(realm);
    fSession = session;
    fWork = work;

    Check.input(fSession.getQuestionCount() > 0);
    fResults = new ArrayList<>(fSession.getQuestionCount());

    for (RecordedQuestion q : fSession.getQuestionList()) {
      Check.input(QuestionItem.isValidWeight(q.getQuestion().getWeight()));

      BigDecimal result;
      if (q.hasResult()) {
        result = Check.inputNotNull(Utils.tryParseDecimal(q.getResult()));
        Check.input(result.signum() >= 0);
        Check.input(result.compareTo(BigDecimal.ONE) <= 0);
      } else {
        result = null;
      }
      fResults.add(result);
    }

    fQuestionViewer = new ArchiveQuestionViewer(this);
    fState = new ArchiveSessionScreenState(realm());
  }

  @Override public Message render(RenderContext context) {
    XArchiveSessionScreen.Builder b = XArchiveSessionScreen.newBuilder()
        .setTestName(fWork.getTestName())
        .setArchivedAt(fWork.getArchivedAt())
        .setUserDescription(AuthenticationUtils.getUserDescription(
            fSession.hasUserName() ? fSession.getUserName() : null,
            fSession.hasUserDisplayName() ? fSession.getUserDisplayName() : null,
            fSession.hasGroupName() ? fSession.getGroupName() : null))
        .setQuestionViewer(context.link(fQuestionViewer))
        .setState(context.link(fState));

    for (int n = fSession.getQuestionCount(), i = 0; i < n; ++i) {
      b.addQuestionDescriptorBuilder()
          .setWeight(fSession.getQuestion(i).getQuestion().getWeight())
          .setStatus(resultToStatus(fResults.get(i)));
    }

    return b.build();
  }

  private static QuestionStatus resultToStatus(@Nullable BigDecimal result) {
    QuestionStatus res;
    if (result == null) {
      res = QuestionStatus.UNANSWERED;
    } else if (result.signum() == 0) {
      res = QuestionStatus.INCORRECT;
    } else if (result.compareTo(BigDecimal.ONE) == 0) {
      res = QuestionStatus.CORRECT;
    } else {
      res = QuestionStatus.PARTIALLY_CORRECT;
    }
    return res;
  }

  @Action public void doClose(Close in) {
    dismissScreen();
  }

  @Action public void doSelectQuestion(SelectQuestion in) {
    Check.input(in.getIndex() >= 0);
    Check.input(in.getIndex() < fSession.getQuestionCount());

    fQuestionIndex = in.getIndex();
    fQuestionViewer.forceRender();
  }

  private XArchiveQuestionViewer renderQuestionViewer() {
    RecordedQuestion recordedQuestion = fSession.getQuestion(fQuestionIndex);

    int weight = recordedQuestion.getQuestion().getWeight();
    BigDecimal result = MoreObjects.firstNonNull(fResults.get(fQuestionIndex), BigDecimal.ZERO);

    XArchiveQuestionViewer.Builder b = XArchiveQuestionViewer.newBuilder()
        .setQuestionIndex(fQuestionIndex)
        .setWeight(weight)
        .setResultPercent(result.movePointRight(2).toPlainString())
        .setScore(result.multiply(BigDecimal.valueOf(weight)).stripTrailingZeros().toPlainString());

    try {
      LiveQuestion liveQuestion = fLiveQuestionLoader.load(recordedQuestion.getQuestion(), this::getQuestion);
      //TODO check liveQuestion for validity

      DialogResponse response;
      if (recordedQuestion.hasResponse()) {
        response = recordedQuestion.getResponse();
        liveQuestion.evaluate(response); // check response for validity
      } else {
        response = liveQuestion.getEmptyResponse();
      }

      ArchiveQuestionContent content = ArchiveQuestionContent.newBuilder()
          .setDialog(liveQuestion.render())
          .setUserOrEmptyResponse(response)
          .setCorrectResponse(liveQuestion.getCorrectResponse())
          .build();

      b.setContent(content);
    } catch (RuntimeException e) {
      fLogger.warn("", e);
    }

    return b.build();
  }

  @Action public void doSelectPage(SelectPage in) {
    fState.fPage = in.getPage();
    fState.forceRender();
  }

  private Question getQuestion(String source) {
    ZipFile archive = GetArchiveEvent.send(this::post);
    ZipArchiveEntry entry = Check.inputNotNull(archive.getEntry(ArchiveUtils.getObjectEntryName(source)));
    try (InputStream in = archive.getInputStream(entry)) {
      return fItxInput.readFromStream(in, new ExternallyStoredObjectReader(), realm(), reader -> {
        Itx.checkName(reader.root(), Itx.ELEM_QUESTION);
        return reader.readQuestion(reader.root());
      });
    } catch (ZipException e) {
      throw new BadInputException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static final class ArchiveQuestionViewer extends Widget {
    final ArchiveSessionScreen fOwner;

    ArchiveQuestionViewer(ArchiveSessionScreen owner) {
      super(owner.realm());
      fOwner = owner;
    }

    @Override public Message render(RenderContext context) {
      return fOwner.renderQuestionViewer();
    }

    void forceRender() {
      markForRender();
    }
  }

  private static final class ArchiveSessionScreenState extends Widget {
    Page fPage = Page.USER_RESPONSE;

    ArchiveSessionScreenState(Realm realm) {
      super(realm);
    }

    @Override public Message render(RenderContext context) {
      return XArchiveSessionScreenState.newBuilder()
          .setPage(fPage)
          .build();
    }

    void forceRender() {
      markForRender();
    }
  }
}
