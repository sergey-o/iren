/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.archiveEditor;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Utils;
import ru.irenproject.archive.ArchiveUtils;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;

import com.google.common.io.ByteSource;
import com.google.protobuf.ByteString;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;
import java.util.zip.ZipException;

public final class ArchiveBlobStore implements BlobStore {
  private final Supplier<ZipFile> fZipFileSupplier;

  public ArchiveBlobStore(Supplier<ZipFile> zipFileSupplier) {
    fZipFileSupplier = zipFileSupplier;
  }

  @Override public BlobId put(ByteSource blob) {
    throw new RuntimeException();
  }

  @Override public ByteSource get(BlobId blobId) {
    ZipFile zipFile = fZipFileSupplier.get();
    ZipArchiveEntry entry = Check.inputNotNull(zipFile.getEntry(ArchiveUtils.getObjectEntryName(blobId.toHexString())));
    try (InputStream in = zipFile.getInputStream(entry)) {
      return Utils.byteStringAsByteSource(ByteString.readFrom(in));
    } catch (ZipException e) {
      throw new BadInputException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
