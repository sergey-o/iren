/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setNegativeChoiceContentModifier;

import ru.irenproject.Modifier;
import ru.irenproject.common.setNegativeChoiceContentModifier.Proto.SetNegativeChoiceContentModifierType;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.Pad;

public final class SetNegativeChoiceContentModifier extends Modifier {
  private final Pad fContent;

  public SetNegativeChoiceContentModifier(Realm realm) {
    super(realm);
    fContent = new Pad(realm);
  }

  @Override public String type() {
    return SetNegativeChoiceContentModifierType.setNegativeChoiceContent.name();
  }

  public Pad content() {
    return fContent;
  }
}
