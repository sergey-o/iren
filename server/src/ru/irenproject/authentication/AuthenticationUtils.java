/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import javax.annotation.Nullable;

public final class AuthenticationUtils {
  public static String getUserDescription(@Nullable String userName, @Nullable String userDisplayName,
      @Nullable String groupName) {
    StringBuilder result = new StringBuilder();

    if (userDisplayName != null) {
      result.append(userDisplayName);
    } else if (userName != null) {
      result.append(userName);
    }

    if (groupName != null) {
      if (result.length() > 0) {
        result.append(' ');
      }
      result
          .append('(')
          .append(groupName)
          .append(')');
    }

    return result.toString();
  }

  private AuthenticationUtils() {}
}
