/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.Utils;
import ru.irenproject.common.authenticationMode.selfRegistration.Proto.SelfRegistration;
import ru.irenproject.work.DataDirectory;

import com.google.protobuf.Message;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Singleton public final class DefaultAuthenticationMode {
  private final AuthenticationModeRegistry fAuthenticationModeRegistry;
  private final Path fFile;

  private volatile Message fValue;
  private final Object fWriteLock = new Object();

  @Inject private DefaultAuthenticationMode(
      AuthenticationModeRegistry authenticationModeRegistry,
      DataDirectory dataDirectory) {
    fAuthenticationModeRegistry = authenticationModeRegistry;
    fFile = dataDirectory.get().resolve("defaultAuthenticationMode");

    try (BufferedReader reader = Files.newBufferedReader(fFile)) {
      fValue = fAuthenticationModeRegistry.load(reader);
    } catch (NoSuchFileException ignored) {
      fValue = SelfRegistration.getDefaultInstance();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public Message get() {
    return fValue;
  }

  public void set(Message value) {
    try {
      synchronized (fWriteLock) {
        Path temp = Utils.changeFileName(fFile, s -> s + '~');
        try (BufferedWriter writer = Files.newBufferedWriter(temp)) {
          fAuthenticationModeRegistry.save(value, writer);
        }
        Files.move(temp, fFile, StandardCopyOption.ATOMIC_MOVE);
        fValue = value;
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
