/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

public final class User {
  private final String fName;
  private final String fDisplayName;
  private final String fGroupName;

  public User(String name, String displayName, String groupName) {
    fName = name;
    fDisplayName = displayName;
    fGroupName = groupName;
  }

  public String name() {
    return fName;
  }

  public String displayName() {
    return fDisplayName;
  }

  public String groupName() {
    return fGroupName;
  }
}
