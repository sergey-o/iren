/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.Check;
import ru.irenproject.Utils;

import com.google.common.base.Ascii;
import com.google.common.base.Strings;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import com.google.common.io.MoreFiles;
import com.google.common.primitives.Ints;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

final class LdapConfig {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("ldap.url")
    String url = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.startTls")
    String startTls = "yes";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.serviceUser")
    String serviceUser = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.serviceUserPasswordFile")
    String serviceUserPasswordFile = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.userObjectClass")
    String userObjectClass = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.userNameAttribute")
    String userNameAttribute = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.userDisplayNameAttribute")
    String userDisplayNameAttribute = "";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.maxConcurrentRequests")
    String maxConcurrentRequests = "5";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.maxQueuedRequests")
    String maxQueuedRequests = "100";

    @com.google.inject.Inject(optional = true)
    @Named("ldap.serverCertificateFingerprint")
    String serverCertificateFingerprint = "";
  }

  public static final HashFunction FINGERPRINT_HASH_FUNCTION = Hashing.sha256();
  public static final BaseEncoding FINGERPRINT_HEX_CODEC = BaseEncoding.base16().withSeparator(":", 2);

  private static final Pattern ATTRIBUTE_NAME_PATTERN = Pattern.compile("[A-Za-z0-9.-]+");

  public final String url;
  public final boolean startTls;
  public final String serviceUser;
  public final String serviceUserPassword;
  public final String userObjectClass;
  public final String userNameAttribute;
  public final @Nullable String userDisplayNameAttribute;
  public final int maxConcurrentRequests;
  public final int maxQueuedRequests;
  public final @Nullable HashCode serverCertificateFingerprint;

  @Inject private LdapConfig(Config raw) {
    try {
      Check.notNull(new URI(raw.url).getHost(), BadUrl::new);
    } catch (URISyntaxException ignored) {
      throw new BadUrl();
    }
    this.url = raw.url;

    this.startTls = Utils.parseBooleanOption(raw.startTls, "Incorrect 'ldap.startTls' value.");

    if (raw.serviceUser.isEmpty()) {
      throw new RuntimeException("Incorrect 'ldap.serviceUser' value.");
    }
    this.serviceUser = raw.serviceUser;

    Path serviceUserPasswordFile = Paths.get(raw.serviceUserPasswordFile);
    Check.that(serviceUserPasswordFile.isAbsolute(),
        () -> new RuntimeException("Incorrect 'ldap.serviceUserPasswordFile' value."));
    try {
      this.serviceUserPassword = Strings.nullToEmpty(
          MoreFiles.asCharSource(serviceUserPasswordFile, StandardCharsets.UTF_8).readFirstLine());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    this.userObjectClass = raw.userObjectClass;

    Check.that(ATTRIBUTE_NAME_PATTERN.matcher(raw.userNameAttribute).matches(),
        () -> new RuntimeException("Incorrect 'ldap.userNameAttribute' value."));
    this.userNameAttribute = raw.userNameAttribute;

    if (raw.userDisplayNameAttribute.isEmpty()) {
      this.userDisplayNameAttribute = null;
    } else {
      Check.that(ATTRIBUTE_NAME_PATTERN.matcher(raw.userDisplayNameAttribute).matches(),
          () -> new RuntimeException("Incorrect 'ldap.userDisplayNameAttribute' value."));
      this.userDisplayNameAttribute = raw.userDisplayNameAttribute;
    }

    this.maxConcurrentRequests = Check.notNull(Ints.tryParse(raw.maxConcurrentRequests), BadMaxConcurrentRequests::new);
    Check.that((this.maxConcurrentRequests >= 1) && (this.maxConcurrentRequests <= 1000),
        BadMaxConcurrentRequests::new);

    this.maxQueuedRequests = Check.notNull(Ints.tryParse(raw.maxQueuedRequests), BadMaxQueuedRequests::new);
    Check.that((this.maxQueuedRequests >= 1) && (this.maxQueuedRequests <= 10_000), BadMaxQueuedRequests::new);

    if (raw.serverCertificateFingerprint.isEmpty()) {
      this.serverCertificateFingerprint = null;
    } else {
      byte[] decoded;
      try {
        decoded = FINGERPRINT_HEX_CODEC.decode(Ascii.toUpperCase(raw.serverCertificateFingerprint));
      } catch (IllegalArgumentException ignored) {
        throw new BadServerCertificateFingerprint();
      }
      Check.that(decoded.length == FINGERPRINT_HASH_FUNCTION.bits() / 8, BadServerCertificateFingerprint::new);
      this.serverCertificateFingerprint = HashCode.fromBytes(decoded);
    }
  }

  private static final class BadUrl extends RuntimeException {
    BadUrl() {
      super("Incorrect 'ldap.url' value.");
    }
  }

  private static final class BadMaxConcurrentRequests extends RuntimeException {
    BadMaxConcurrentRequests() {
      super("Incorrect 'ldap.maxConcurrentRequests' value.");
    }
  }

  private static final class BadMaxQueuedRequests extends RuntimeException {
    BadMaxQueuedRequests() {
      super("Incorrect 'ldap.maxQueuedRequests' value.");
    }
  }

  private static final class BadServerCertificateFingerprint extends RuntimeException {
    BadServerCertificateFingerprint() {
      super("Incorrect 'ldap.serverCertificateFingerprint' value.");
    }
  }
}
