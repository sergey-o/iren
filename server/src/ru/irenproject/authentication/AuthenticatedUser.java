/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import javax.annotation.Nullable;

public final class AuthenticatedUser {
  private final String fName;
  private final @Nullable String fDisplayName;

  public AuthenticatedUser(String name, @Nullable String displayName) {
    fName = name;
    fDisplayName = displayName;
  }

  public String name() {
    return fName;
  }

  public @Nullable String displayName() {
    return fDisplayName;
  }
}
