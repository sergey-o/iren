/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.Utils;
import ru.irenproject.common.authenticationMode.accountLogin.Proto.AccountLogin;
import ru.irenproject.common.authenticationMode.selfRegistration.Proto.SelfRegistration;
import ru.irenproject.common.authenticationMode.textFile.Proto.TextFile;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.protobuf.Any;
import com.google.protobuf.Descriptors.Descriptor;
import com.google.protobuf.Message;
import com.google.protobuf.util.JsonFormat;
import com.google.protobuf.util.JsonFormat.TypeRegistry;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.Reader;

@Singleton public final class AuthenticationModeRegistry {
  private static final ImmutableList<Message> MODES = ImmutableList.of(
      SelfRegistration.getDefaultInstance(),
      TextFile.getDefaultInstance(),
      AccountLogin.getDefaultInstance());

  private final TypeRegistry fTypeRegistry;
  private final ImmutableMap<String, Class<? extends Message>> fMessageClassesByTypeUrl;

  @Inject private AuthenticationModeRegistry() {
    TypeRegistry.Builder typeRegistry = TypeRegistry.newBuilder();
    ImmutableMap.Builder<String, Class<? extends Message>> messageClassesByTypeUrl = ImmutableMap.builder();

    for (Message mode : MODES) {
      Descriptor descriptor = mode.getDescriptorForType();
      typeRegistry.add(descriptor);
      messageClassesByTypeUrl.put(Utils.getTypeUrl(descriptor), mode.getClass());
    }

    fTypeRegistry = typeRegistry.build();
    fMessageClassesByTypeUrl = messageClassesByTypeUrl.build();
  }

  public ImmutableMap<String, Class<? extends Message>> messageClassesByTypeUrl() {
    return fMessageClassesByTypeUrl;
  }

  public void save(Message authenticationMode, Appendable target) {
    try {
      JsonFormat.printer().usingTypeRegistry(fTypeRegistry).appendTo(PlatformUtils.toAny(authenticationMode), target);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public Message load(Reader source) {
    try {
      Any.Builder b = Any.newBuilder();
      JsonFormat.parser().usingTypeRegistry(fTypeRegistry).merge(source, b);
      Any any = b.build();
      return any.unpack(Check.notNull(fMessageClassesByTypeUrl.get(any.getTypeUrl())));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
