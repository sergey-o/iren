/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.Check;
import ru.irenproject.Oops;
import ru.irenproject.Utils;
import ru.irenproject.common.userManager.Proto.SetUserRegistry;
import ru.irenproject.common.userManager.Proto.TextFileUserRegistryError;
import ru.irenproject.work.DataDirectory;

import com.google.common.base.CharMatcher;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import com.google.inject.ImplementedBy;
import com.google.protobuf.ByteString;
import com.ibm.icu.text.Normalizer2;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

@Singleton public final class TextFileUserRegistry {
  @ImplementedBy(DefaultLocation.class)
  public interface Location {
    Path get();
  }

  @Singleton public static final class DefaultLocation implements Location {
    private final Path fPath;

    @Inject private DefaultLocation(DataDirectory dataDirectory) {
      fPath = dataDirectory.get().resolve("users");
    }

    @Override public Path get() {
      return fPath;
    }
  }

  public static final int MAX_NAME_LENGTH = 100;
  private static final Pattern NAME_PATTERN = Pattern.compile("[\\p{L}\\p{Mn}\\p{Mc}\\p{Nd} '\u2019-]*");

  public static ByteString hash(CharSequence s) {
    return ByteString.copyFrom(Hashing.sha256().hashUnencodedChars(s).asBytes());
  }

  private final Path fFile;

  private volatile ImmutableMap<String, Group> fGroupsByCanonicalName;
  private final Object fWriteLock = new Object();

  @Inject private TextFileUserRegistry(Location location) {
    fFile = location.get();

    ParseResult parseResult;
    try (BufferedReader reader = Files.newBufferedReader(fFile)) {
      parseResult = parse(reader);
    } catch (NoSuchFileException ignored) {
      parseResult = new ParseResult();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

    if (!parseResult.fErrors.isEmpty()) {
      TextFileUserRegistryError e = parseResult.fErrors.get(0);
      throw Oops.format("Error in user registry: %s at line %d of '%s'.", e.getReason(), e.getLine(), fFile);
    }

    fGroupsByCanonicalName = parseResult.fGroupsByCanonicalName;
  }

  private static ParseResult parse(BufferedReader source) {
    try {
      ParseResult res = new ParseResult();

      LinkedHashMap<String, MutableGroup> groupsByCanonicalName = new LinkedHashMap<>();
      MutableGroup currentGroup = null;
      String line;
      int lineNumber = 0;

      while ((line = source.readLine()) != null) {
        ++lineNumber;
        line = CharMatcher.whitespace().trimAndCollapseFrom(line, ' ');
        if (!line.isEmpty()) {
          if (line.endsWith(":")) {
            String groupName = CharMatcher.whitespace().trimTrailingFrom(line.substring(0, line.length() - 1));
            if (validName(groupName)) {
              currentGroup = groupsByCanonicalName.computeIfAbsent(canonicalizeName(groupName),
                  ignored -> new MutableGroup(groupName));
            } else {
              currentGroup = null;
              res.addError(TextFileUserRegistryError.Reason.INCORRECT_GROUP_NAME, lineNumber);
            }
          } else if (currentGroup == null) {
            res.addError(TextFileUserRegistryError.Reason.GROUP_NAME_EXPECTED, lineNumber);
          } else if (!validName(line)) {
            res.addError(TextFileUserRegistryError.Reason.INCORRECT_USER_NAME, lineNumber);
          } else if (currentGroup.fUsersByCanonicalName.putIfAbsent(canonicalizeName(line), line) != null) {
            res.addError(TextFileUserRegistryError.Reason.DUPLICATE_USER_NAME, lineNumber);
          }
        }
      }

      res.fGroupsByCanonicalName = groupsByCanonicalName.entrySet().stream()
          .collect(ImmutableMap.toImmutableMap(Map.Entry::getKey, e -> e.getValue().seal()));

      return res;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  private static boolean validName(String s) {
    return !s.isEmpty() && (s.length() <= MAX_NAME_LENGTH) && NAME_PATTERN.matcher(s).matches();
  }

  private static String canonicalizeName(String s) {
    return Normalizer2.getNFKCCasefoldInstance().normalize(s);
  }

  private static final class ParseResult {
    ImmutableMap<String, Group> fGroupsByCanonicalName = ImmutableMap.of();
    final ArrayList<TextFileUserRegistryError> fErrors = new ArrayList<>();

    void addError(TextFileUserRegistryError.Reason reason, int line) {
      fErrors.add(TextFileUserRegistryError.newBuilder()
          .setReason(reason)
          .setLine(line)
          .build());
    }
  }

  private static final class Group {
    final String fName;
    final ImmutableMap<String, String> fUsersByCanonicalName;

    Group(String name, ImmutableMap<String, String> usersByCanonicalName) {
      fName = name;
      fUsersByCanonicalName = usersByCanonicalName;
    }
  }

  private static final class MutableGroup {
    final String fName;
    final LinkedHashMap<String, String> fUsersByCanonicalName = new LinkedHashMap<>();

    MutableGroup(String name) {
      fName = name;
    }

    Group seal() {
      return new Group(fName, ImmutableMap.copyOf(fUsersByCanonicalName));
    }
  }

  public @Nullable User getUserIfExists(String suppliedUserName, String suppliedGroupName) {
    User res = null;

    String groupName = CharMatcher.whitespace().trimAndCollapseFrom(suppliedGroupName, ' ');
    if (validName(groupName)) {
      String canonicalGroupName = canonicalizeName(groupName);
      Group group = fGroupsByCanonicalName.get(canonicalGroupName);
      if (group != null) {
        String userName = CharMatcher.whitespace().trimAndCollapseFrom(suppliedUserName, ' ');
        if (validName(userName)) {
          String canonicalUserName = canonicalizeName(userName);
          String registeredUserName = group.fUsersByCanonicalName.get(canonicalUserName);
          if (registeredUserName != null) {
            res = new User(canonicalUserName + '|' + canonicalGroupName, registeredUserName, group.fName);
            Check.that(CharMatcher.is('|').countIn(res.name()) == 1);
          }
        }
      }
    }

    return res;
  }

  public void get(Locale collationLocale, Appendable target) {
    format(fGroupsByCanonicalName.values(), collationLocale, target);
  }

  private static void format(Collection<Group> groups, Locale collationLocale, Appendable target) {
    try {
      Collator collator = Collator.getInstance(collationLocale);
      boolean prependGroupSeparator = false;

      for (Group group : ImmutableList.sortedCopyOf(Comparator.comparing(g -> g.fName, collator), groups)) {
        if (prependGroupSeparator) {
          target.append("\n\n\n");
        } else {
          prependGroupSeparator = true;
        }

        target
            .append(group.fName)
            .append(":\n");

        for (String user : ImmutableList.sortedCopyOf(collator, group.fUsersByCanonicalName.values())) {
          target
              .append(user)
              .append('\n');
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public SetUserRegistry.Reply set(String source, Locale collationLocale, ByteString expectedOldHash) {
    try {
      SetUserRegistry.Reply.Builder result = SetUserRegistry.Reply.newBuilder();

      synchronized (fWriteLock) {
        StringBuilder old = new StringBuilder();
        format(fGroupsByCanonicalName.values(), collationLocale, old);
        result.setOldHashMatches(expectedOldHash.equals(hash(old)));

        if (result.getOldHashMatches()) {
          ParseResult parseResult;
          try (BufferedReader reader = new BufferedReader(new StringReader(source))) {
            parseResult = parse(reader);
          }

          if (parseResult.fErrors.isEmpty()) {
            Path temp = Utils.changeFileName(fFile, s -> s + '~');
            try (BufferedWriter writer = Files.newBufferedWriter(temp)) {
              format(parseResult.fGroupsByCanonicalName.values(), collationLocale, writer);
            }
            Files.move(temp, fFile, StandardCopyOption.ATOMIC_MOVE);
            fGroupsByCanonicalName = parseResult.fGroupsByCanonicalName;
          } else {
            result.setError(parseResult.fErrors.get(0));
          }
        }
      }

      return result.build();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
