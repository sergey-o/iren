/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.PlatformUtils;
import ru.irenproject.RequestContext;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.userManager.Proto.GetAuthenticationMode;
import ru.irenproject.common.userManager.Proto.GetUserRegistry;
import ru.irenproject.common.userManager.Proto.SetAuthenticationMode;
import ru.irenproject.common.userManager.Proto.SetUserRegistry;
import ru.irenproject.common.userManager.Proto.UserManagerReply;
import ru.irenproject.common.userManager.Proto.UserManagerRequest;
import ru.irenproject.work.Connection;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.inject.assistedinject.Assisted;
import com.google.protobuf.InvalidProtocolBufferException;

import javax.inject.Inject;
import java.util.Locale;

public final class UserManagerChannel extends AbstractBehavior<UserManagerChannel.Message> {
  public interface Message {}

  public static final class RequestMessage implements Message {
    public final UserManagerRequest request;
    public final RequestContext requestContext;

    public RequestMessage(UserManagerRequest request, RequestContext requestContext) {
      this.request = request;
      this.requestContext = requestContext;
    }
  }

  public interface Factory {
    UserManagerChannel create(
        ActorContext<Message> context,
        Connection connection);

    static Behavior<Message> makeBehavior(
        Factory factory,
        Connection connection) {
      return Behaviors.setup(ctx -> factory.create(ctx, connection));
    }
  }

  private final Connection fConnection;
  private final DefaultAuthenticationMode fDefaultAuthenticationMode;
  private final AuthenticationModeRegistry fAuthenticationModeRegistry;
  private final TextFileUserRegistry fTextFileUserRegistry;

  @Inject private UserManagerChannel(
      @Assisted ActorContext<Message> context, // currently unused
      @Assisted Connection connection,
      DefaultAuthenticationMode defaultAuthenticationMode,
      AuthenticationModeRegistry authenticationModeRegistry,
      TextFileUserRegistry textFileUserRegistry) {
    fConnection = connection;
    fDefaultAuthenticationMode = defaultAuthenticationMode;
    fAuthenticationModeRegistry = authenticationModeRegistry;
    fTextFileUserRegistry = textFileUserRegistry;
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onMessage(RequestMessage.class, this::onRequestMessage)
        .build();
  }

  private Behavior<Message> onRequestMessage(RequestMessage m) {
    UserManagerRequest r = m.request;
    switch (r.getRequestCase()) {
      case GET_AUTHENTICATION_MODE: return onGetAuthenticationMode(m.requestContext);
      case SET_AUTHENTICATION_MODE: return onSetAuthenticationMode(r.getSetAuthenticationMode(), m.requestContext);
      case GET_USER_REGISTRY: return onGetUserRegistry(r.getGetUserRegistry(), m.requestContext);
      case SET_USER_REGISTRY: return onSetUserRegistry(r.getSetUserRegistry(), m.requestContext);
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private void send(UserManagerReply reply, RequestContext requestContext) {
    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(requestContext.id())
        .setUserManagerReply(reply)
        .build());
  }

  private Behavior<Message> onGetAuthenticationMode(RequestContext requestContext) {
    send(UserManagerReply.newBuilder()
        .setGetAuthenticationModeReply(GetAuthenticationMode.Reply.newBuilder()
            .setMode(PlatformUtils.toAny(fDefaultAuthenticationMode.get())))
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onSetAuthenticationMode(SetAuthenticationMode m, RequestContext requestContext) {
    com.google.protobuf.Message authenticationMode;
    try {
      authenticationMode = m.getMode().unpack(Check.inputNotNull(
          fAuthenticationModeRegistry.messageClassesByTypeUrl().get(m.getMode().getTypeUrl())));
    } catch (InvalidProtocolBufferException e) {
      throw new BadInputException(e);
    }

    fDefaultAuthenticationMode.set(authenticationMode);

    send(UserManagerReply.newBuilder()
        .setSetAuthenticationModeReply(SetAuthenticationMode.Reply.getDefaultInstance())
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onGetUserRegistry(GetUserRegistry m, RequestContext requestContext) {
    StringBuilder out = new StringBuilder();
    fTextFileUserRegistry.get(Locale.forLanguageTag(m.getLanguage()), out);
    String text = out.toString();

    send(UserManagerReply.newBuilder()
        .setGetUserRegistryReply(GetUserRegistry.Reply.newBuilder()
            .setText(text)
            .setHash(TextFileUserRegistry.hash(text)))
        .build(), requestContext);

    return this;
  }

  private Behavior<Message> onSetUserRegistry(SetUserRegistry m, RequestContext requestContext) {
    send(UserManagerReply.newBuilder()
        .setSetUserRegistryReply(fTextFileUserRegistry.set(
            m.getText(), Locale.forLanguageTag(m.getLanguage()), m.getExpectedOldHash()))
        .build(), requestContext);

    return this;
  }
}
