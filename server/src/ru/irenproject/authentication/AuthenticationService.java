/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.authentication;

import ru.irenproject.Check;
import ru.irenproject.InitializationTracker;
import ru.irenproject.Utils;
import ru.irenproject.common.login.Proto.AuthenticationFailure;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import com.google.common.base.CharMatcher;
import com.google.common.hash.HashCode;
import com.google.inject.assistedinject.Assisted;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.PartialResultException;
import javax.naming.SizeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import javax.naming.ldap.StartTlsRequest;
import javax.naming.ldap.StartTlsResponse;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.regex.Pattern;

public final class AuthenticationService extends AbstractBehavior<AuthenticationService.Message> {
  private static final class Config {
    @com.google.inject.Inject(optional = true)
    @Named("accountProvider")
    String accountProvider = "";
  }

  public interface Message {}

  public static final class Authenticate<R> implements Message {
    public final String userName;
    public final String password;
    public final Function<AuthenticateResult, R> replyBuilder;
    public final ActorRef<R> replyTo;

    public Authenticate(String userName, String password, Function<AuthenticateResult, R> replyBuilder,
        ActorRef<R> replyTo) {
      this.userName = userName;
      this.password = password;
      this.replyBuilder = replyBuilder;
      this.replyTo = replyTo;
    }
  }

  public interface AuthenticateResult {
    final class Ok implements AuthenticateResult {
      public final AuthenticatedUser authenticatedUser;

      public Ok(AuthenticatedUser authenticatedUser) {
        this.authenticatedUser = authenticatedUser;
      }
    }

    final class Failure implements AuthenticateResult {
      public final AuthenticationFailure failure;
      public final @Nullable String userName;

      public Failure(AuthenticationFailure failure, @Nullable String userName) {
        this.failure = failure;
        this.userName = userName;
      }
    }
  }

  public enum Stop implements Message { INSTANCE }

  public interface Factory {
    AuthenticationService create(ActorContext<Message> context);
  }

  private static final Logger fLogger = LoggerFactory.getLogger(AuthenticationService.class);

  public static final int MAX_USER_NAME_LENGTH = 100;
  private static final Pattern USER_NAME_PATTERN = Pattern.compile("[._@\\p{L}\\p{Mn}\\p{Mc}\\p{Nd} '\u2019-]*");

  public static final int MAX_PASSWORD_LENGTH = 100;

  private final ActorContext<Message> fContext;

  private final @Nullable LdapConfig fLdapConfig;
  private final @Nullable GenericObjectPool<InitialLdapContext> fLdapContextPool;
  private final @Nullable ThreadPoolExecutor fExecutor;

  @Inject private AuthenticationService(
      @Assisted ActorContext<Message> context,
      InitializationTracker initializationTracker,
      Config config,
      Provider<LdapConfig> ldapConfigProvider) {
    fContext = context;

    if (config.accountProvider.isEmpty()) {
      fLdapConfig = null;
      fLdapContextPool = null;
      fExecutor = null;
    } else if (config.accountProvider.equals("ldap")) {
      fLdapConfig = ldapConfigProvider.get();
      fLdapContextPool = new GenericObjectPool<>(new LdapContextFactory(fLdapConfig), Utils.unboundedPoolConfig());
      fExecutor = new ThreadPoolExecutor(
          fLdapConfig.maxConcurrentRequests,
          fLdapConfig.maxConcurrentRequests,
          10,
          TimeUnit.SECONDS,
          new ArrayBlockingQueue<>(fLdapConfig.maxQueuedRequests),
          new BasicThreadFactory.Builder()
              .namingPattern("authenticationService-%d")
              .build());
      fExecutor.allowCoreThreadTimeOut(true);
    } else {
      throw new RuntimeException("Incorrect 'accountProvider' value.");
    }

    initializationTracker.recordCompletion(getClass());
  }

  @Override public Receive<Message> createReceive() {
    return newReceiveBuilder()
        .onSignal(PostStop.class, ignored -> onPostStop())
        .onMessage(Authenticate.class, m -> onAuthenticate((Authenticate<?>) m))
        .onMessage(TaskComplete.class, this::onTaskComplete)
        .onMessageEquals(Stop.INSTANCE, this::onStop)
        .build();
  }

  private Behavior<Message> onPostStop() {
    if (fExecutor != null) {
      fExecutor.shutdownNow();
      Utils.awaitTerminationUninterruptibly(fExecutor);
    }

    if (fLdapContextPool != null) {
      fLdapContextPool.close();
    }

    return this;
  }

  private <R> Behavior<Message> onAuthenticate(Authenticate<R> m) {
    String userName = CharMatcher.whitespace().trimAndCollapseFrom(m.userName, ' ');

    if (!validUserName(userName)) {
      replyWith(new AuthenticateResult.Failure(AuthenticationFailure.INCORRECT_USER_NAME, null), m);
    } else if (!validPasswordString(m.password)) {
      replyWith(new AuthenticateResult.Failure(AuthenticationFailure.UNKNOWN_USER_OR_MISMATCHING_PASSWORD, userName),
          m);
    } else if (fLdapConfig == null) {
      fLogger.warn("User authentication is not configured.");
      replyWith(new AuthenticateResult.Failure(AuthenticationFailure.INCORRECT_USER_NAME, null), m);
    } else {
      CompletableFuture<AuthenticateResult> f;
      try {
        f = CompletableFuture.supplyAsync(new Task(fLdapContextPool, fLdapConfig, userName, m.password), fExecutor);
      } catch (RejectedExecutionException ignored) {
        f = null;
      }

      if (f == null) {
        replyWith(new AuthenticateResult.Failure(AuthenticationFailure.RETRY_LATER, userName), m);
      } else {
        fContext.pipeToSelf(f, (result, error) -> new TaskComplete(result, error, m, userName));
      }
    }

    return this;
  }

  private static final class TaskComplete implements Message {
    final @Nullable AuthenticateResult result;
    final @Nullable Throwable error;
    final Authenticate<?> request;
    final String userName;

    TaskComplete(@Nullable AuthenticateResult result, @Nullable Throwable error, Authenticate<?> request,
        String userName) {
      this.result = result;
      this.error = error;
      this.request = request;
      this.userName = userName;
    }
  }

  private Behavior<Message> onTaskComplete(TaskComplete m) {
    AuthenticateResult result;
    if (m.result == null) {
      result = new AuthenticateResult.Failure(AuthenticationFailure.INTERNAL_ERROR, m.userName);
      fLogger.warn("Error on logging in:", m.error);
    } else {
      result = m.result;
    }

    replyWith(result, m.request);
    return this;
  }

  private Behavior<Message> onStop() {
    return Behaviors.stopped();
  }

  private static boolean validUserName(String s) {
    return !s.isEmpty() && (s.length() <= MAX_USER_NAME_LENGTH) && USER_NAME_PATTERN.matcher(s).matches();
  }

  private static boolean validPasswordString(String s) {
    return !s.isEmpty() && (s.length() <= MAX_PASSWORD_LENGTH);
  }

  private static <R> void replyWith(AuthenticateResult result, Authenticate<R> request) {
    request.replyTo.tell(request.replyBuilder.apply(result));
  }

  private static final class LdapContextFactory extends BasePooledObjectFactory<InitialLdapContext> {
    final LdapConfig fLdapConfig;

    LdapContextFactory(LdapConfig ldapConfig) {
      fLdapConfig = ldapConfig;
    }

    @Override public InitialLdapContext create() throws NamingException, IOException {
      Hashtable<String, Object> env = new Hashtable<>();
      env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
      env.put(Context.PROVIDER_URL, fLdapConfig.url);
      env.put("com.sun.jndi.ldap.connect.timeout", "5000");
      env.put("com.sun.jndi.ldap.read.timeout", "5000");

      InitialLdapContext res = new InitialLdapContext(env, null);

      boolean ok = false;
      try {
        if (fLdapConfig.startTls) {
          StartTlsResponse tls = (StartTlsResponse) res.extendedOperation(new StartTlsRequest());
          if (fLdapConfig.serverCertificateFingerprint != null) {
            // there is no point in host name checking when the server certificate is explicitly specified
            // in the configuration
            tls.setHostnameVerifier((ignored, ignored2) -> true);
          }
          tls.negotiate(sslSocketFactory());
        }

        ok = true;
      } finally {
        if (!ok) {
          closeContext(res);
        }
      }

      return res;
    }

    SSLSocketFactory sslSocketFactory() {
      try {
        SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
        sslContext.init(
            null,
            (fLdapConfig.serverCertificateFingerprint == null) ?
                null :
                new TrustManager[] {new FingerprintVerifyingTrustManager(fLdapConfig.serverCertificateFingerprint)},
            null);
        return sslContext.getSocketFactory();
      } catch (NoSuchAlgorithmException | KeyManagementException e) {
        throw new RuntimeException(e);
      }
    }

    static void closeContext(InitialLdapContext ldapContext) {
      try {
        ldapContext.close();
      } catch (NamingException | RuntimeException e) {
        fLogger.warn("Cannot close LDAP context:", e);
      }
    }

    @Override public PooledObject<InitialLdapContext> wrap(InitialLdapContext obj) {
      return new DefaultPooledObject<>(obj);
    }

    @Override public void destroyObject(PooledObject<InitialLdapContext> p) {
      closeContext(p.getObject());
    }
  }

  private static final class FingerprintVerifyingTrustManager implements X509TrustManager {
    final HashCode fTrustedFingerprint;

    FingerprintVerifyingTrustManager(HashCode trustedFingerprint) {
      fTrustedFingerprint = trustedFingerprint;
    }

    @Override public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
      throw new CertificateException();
    }

    @Override public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
      Check.that(ArrayUtils.isNotEmpty(chain), IllegalArgumentException::new);

      HashCode receivedFingerprint = LdapConfig.FINGERPRINT_HASH_FUNCTION.hashBytes(chain[0].getEncoded());
      if (!receivedFingerprint.equals(fTrustedFingerprint)) {
        throw new CertificateException(String.format(
            "Untrusted LDAP server certificate with fingerprint '%s' has been received.",
            LdapConfig.FINGERPRINT_HEX_CODEC.encode(receivedFingerprint.asBytes())));
      }
    }

    @Override public X509Certificate[] getAcceptedIssuers() {
      return new X509Certificate[0];
    }
  }

  private static final class Task implements Supplier<AuthenticateResult> {
    static final String ROOT_CONTAINER_NAME = "";
    static final String[] NO_ATTRIBUTE_IDS = {};

    final GenericObjectPool<InitialLdapContext> fLdapContextPool;
    final LdapConfig fLdapConfig;
    final String fUserName;
    final String fPassword;

    Task(GenericObjectPool<InitialLdapContext> ldapContextPool, LdapConfig ldapConfig, String userName,
        String password) {
      fLdapContextPool = ldapContextPool;
      fLdapConfig = ldapConfig;
      fUserName = userName;
      fPassword = password;
    }

    @Override public AuthenticateResult get() {
      AuthenticateResult res;

      InitialLdapContext ldapContext = borrow();
      boolean allowRetryWithFreshConnection = allowRetryWithFreshConnection(ldapContext);
      try {
        res = runAndReleaseContext(ldapContext);
      } catch (RuntimeException e) {
        if (allowRetryWithFreshConnection) {
          fLogger.warn("Login processing failed and will be retried with fresh LDAP connection:", e);
          fLdapContextPool.clear();
          res = runAndReleaseContext(borrow());
        } else {
          throw e;
        }
      }

      return res;
    }

    InitialLdapContext borrow() {
      try {
        return Utils.callWithRetriesUninterruptibly(fLdapContextPool::borrowObject);
      } catch (Exception anyButInterruptedException) {
        throw new RuntimeException(anyButInterruptedException);
      }
    }

    static boolean allowRetryWithFreshConnection(InitialLdapContext ldapContext) {
      boolean res;
      try {
        boolean contextWasUsedEarlier = ldapContext.getEnvironment().get(Context.SECURITY_AUTHENTICATION) != null;
        res = contextWasUsedEarlier;
      } catch (NamingException | RuntimeException ignored) {
        res = false;
      }

      return res;
    }

    AuthenticateResult runAndReleaseContext(InitialLdapContext ldapContext) {
      AuthenticateResult res;

      boolean ok = false;
      try {
        res = run(ldapContext);
        ok = true;
      } finally {
        if (ok) {
          fLdapContextPool.returnObject(ldapContext);
        } else {
          invalidate(ldapContext);
        }
      }

      return res;
    }

    void invalidate(InitialLdapContext ldapContext) {
      try {
        fLdapContextPool.invalidateObject(ldapContext);
      } catch (InterruptedException ignored) { // should not happen
        // no retries, ldapContext is presumed to have been invalidated
        Thread.currentThread().interrupt();
      } catch (Exception e) { // should not happen
        fLogger.warn("", e);
      }
    }

    AuthenticateResult run(LdapContext ldapContext) {
      setAuthenticationData(fLdapConfig.serviceUser, fLdapConfig.serviceUserPassword, ldapContext);
      List<Account> found = findAccounts(ldapContext);

      AuthenticateResult res;
      if (found.size() == 1) {
        Account account = found.get(0);
        res = validAccount(account) ?
            authenticate(account, ldapContext) :
            new AuthenticateResult.Failure(AuthenticationFailure.UNKNOWN_USER_OR_MISMATCHING_PASSWORD, fUserName);
      } else {
        if (found.size() >= 2) {
          fLogger.warn("Multiple accounts have been found for user name \"{}\".", fUserName);
        }
        res = new AuthenticateResult.Failure(AuthenticationFailure.UNKNOWN_USER_OR_MISMATCHING_PASSWORD, fUserName);
      }

      return res;
    }

    static void setAuthenticationData(String user, String password, LdapContext ldapContext) {
      try {
        ldapContext.addToEnvironment(Context.SECURITY_AUTHENTICATION, "simple");
        ldapContext.addToEnvironment(Context.SECURITY_PRINCIPAL, user);
        ldapContext.addToEnvironment(Context.SECURITY_CREDENTIALS, password);
      } catch (NamingException e) {
        throw new RuntimeException(e);
      }
    }

    List<Account> findAccounts(LdapContext ldapContext) {
      try {
        ArrayList<Account> res = new ArrayList<>();

        SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        searchControls.setReturningAttributes((fLdapConfig.userDisplayNameAttribute == null) ?
            new String[] {fLdapConfig.userNameAttribute} :
            new String[] {fLdapConfig.userNameAttribute, fLdapConfig.userDisplayNameAttribute});
        searchControls.setCountLimit(2);

        NamingEnumeration<SearchResult> results = ldapContext.search(
            ROOT_CONTAINER_NAME,
            "(&(objectClass={0})({1}={2}))",
            new String[] {fLdapConfig.userObjectClass, fLdapConfig.userNameAttribute, fUserName},
            searchControls);
        try {
          while (results.hasMore()) {
            SearchResult result = results.next();
            res.add(new Account(
                result.getNameInNamespace(),
                tryGetAttribute(result, fLdapConfig.userNameAttribute),
                (fLdapConfig.userDisplayNameAttribute == null) ?
                    null :
                    tryGetAttribute(result, fLdapConfig.userDisplayNameAttribute)));
          }
        } catch (PartialResultException | SizeLimitExceededException ignored) {
          // do nothing
        } finally {
          closeNamingEnumeration(results);
        }

        return res;
      } catch (NamingException e) {
        throw new RuntimeException(e);
      }
    }

    static @Nullable String tryGetAttribute(SearchResult r, String name) throws NamingException {
      String res;

      Attribute a = r.getAttributes().get(name);
      if (a == null) {
        res = null;
      } else {
        Object value = a.get();
        res = (value instanceof String) ? (String) value : null;
      }

      return res;
    }

    static void closeNamingEnumeration(NamingEnumeration<?> enumeration) {
      try {
        enumeration.close();
      } catch (NamingException | RuntimeException e) {
        fLogger.warn("Cannot close NamingEnumeration:", e);
      }
    }

    boolean validAccount(Account account) {
      return
          validUserNameAttribute(fLdapConfig.userNameAttribute, account.userName, fUserName)
          &&
          (
              (fLdapConfig.userDisplayNameAttribute == null)
              ||
              validUserNameAttribute(fLdapConfig.userDisplayNameAttribute, account.userDisplayName, fUserName)
          );
    }

    static boolean validUserNameAttribute(String attributeName, @Nullable String attributeValue,
        String userNameForLogging) {
      boolean res = (attributeValue != null) && validUserName(attributeValue);
      if (!res) {
        fLogger.warn("Attribute \"{}\" of account \"{}\" {}.",
            attributeName,
            userNameForLogging,
            (attributeValue == null) ? "is not set" : String.format("has unacceptable value \"%s\"", attributeValue));
      }
      return res;
    }

    AuthenticateResult authenticate(Account account, LdapContext ldapContext) {
      setAuthenticationData(account.dn, fPassword, ldapContext);

      AuthenticateResult res;
      try {
        ldapContext.getAttributes(ROOT_CONTAINER_NAME, NO_ATTRIBUTE_IDS); // force bind as new user
        res = new AuthenticateResult.Ok(new AuthenticatedUser(account.userName, account.userDisplayName));
      } catch (AuthenticationException e) {
        res = new AuthenticateResult.Failure(AuthenticationFailure.UNKNOWN_USER_OR_MISMATCHING_PASSWORD, fUserName);
        fLogger.warn("Authentication failed for \"{}\": {}", account.userName, e.getMessage());
      } catch (NamingException e) {
        throw new RuntimeException(e);
      }

      return res;
    }
  }

  private static final class Account {
    final String dn;
    final @Nullable String userName;
    final @Nullable String userDisplayName;

    Account(String dn, @Nullable String userName, @Nullable String userDisplayName) {
      this.dn = dn;
      this.userName = userName;
      this.userDisplayName = userDisplayName;
    }
  }
}
