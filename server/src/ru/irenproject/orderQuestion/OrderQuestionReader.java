/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.orderQuestion;

import ru.irenproject.itx.Itx;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.QuestionReader;

import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class OrderQuestionReader implements QuestionReader {
  @Inject OrderQuestionReader() {}

  @Override public OrderQuestion read(ItxReader in, Element e) {
    OrderQuestion res = new OrderQuestion(in.realm());
    in.readPad(Itx.getChild(e, Itx.ELEM_CONTENT), res.formulation());

    Element xmlOptions = Itx.getChild(e, Itx.ELEM_ORDER_OPTIONS);
    String itemLimit = Itx.find(xmlOptions, Itx.ATTR_SEQUENCE_ITEMS_USED);
    if (!itemLimit.equals(Itx.VAL_ALL)) {
      res.setItemLimit(Itx.toInteger(itemLimit));
    }

    String distractorLimit = Itx.find(xmlOptions, Itx.ATTR_DISTRACTORS_USED);
    if (!distractorLimit.equals(Itx.VAL_ALL)) {
      res.setDistractorLimit(Itx.toInteger(distractorLimit));
    }

    Element xmlSequence = Itx.getChildIfExists(e, Itx.ELEM_SEQUENCE);
    if (xmlSequence != null) {
      for (Element xmlSequenceItem : Itx.getChildren(xmlSequence, Itx.ELEM_SEQUENCE_ITEM)) {
        readSequenceItem(in, xmlSequenceItem, res);
      }
    }

    Element xmlDistractors = Itx.getChildIfExists(e, Itx.ELEM_DISTRACTORS);
    if (xmlDistractors != null) {
      for (Element xmlDistractor : Itx.getChildren(xmlDistractors, Itx.ELEM_DISTRACTOR)) {
        readDistractor(in, xmlDistractor, res);
      }
    }

    return res;
  }

  private void readSequenceItem(ItxReader in, Element e, OrderQuestion out) {
    out.addItem(in.readNewPad(Itx.getChild(e, Itx.ELEM_CONTENT)));
  }

  private void readDistractor(ItxReader in, Element e, OrderQuestion out) {
    out.addDistractor(in.readNewPad(Itx.getChild(e, Itx.ELEM_CONTENT)));
  }
}
