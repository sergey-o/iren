/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.orderQuestion;

import ru.irenproject.Question;
import ru.irenproject.TestCheck;
import ru.irenproject.common.orderQuestion.Proto.OrderQuestionType;
import ru.irenproject.infra.Realm;
import ru.irenproject.pad.Pad;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class OrderQuestion extends Question {
  public static final int MAX_ELEMENTS = 50;

  private final Pad fFormulation;
  private final ArrayList<Pad> fItems = new ArrayList<>();
  private final ArrayList<Pad> fDistractors = new ArrayList<>();
  private @Nullable Integer fItemLimit;
  private @Nullable Integer fDistractorLimit;

  public OrderQuestion(Realm realm) {
    super(realm);
    fFormulation = new Pad(realm);
  }

  @Override public String type() {
    return OrderQuestionType.order.name();
  }

  @Override public Pad formulation() {
    return fFormulation;
  }

  public List<Pad> items() {
    return Collections.unmodifiableList(fItems);
  }

  public List<Pad> distractors() {
    return Collections.unmodifiableList(fDistractors);
  }

  public void addItem(Pad item) {
    TestCheck.input(fItems.size() < MAX_ELEMENTS);

    fItems.add(item);
    post(ChangeEvent.INSTANCE);
  }

  public void addDistractor(Pad distractor) {
    TestCheck.input(fDistractors.size() < MAX_ELEMENTS);

    fDistractors.add(distractor);
    post(ChangeEvent.INSTANCE);
  }

  public void deleteItem(int index) {
    checkItemIndex(index);

    fItems.remove(index);
    post(ChangeEvent.INSTANCE);
  }

  private void checkItemIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fItems.size());
  }

  public void deleteDistractor(int index) {
    checkDistractorIndex(index);

    fDistractors.remove(index);
    post(ChangeEvent.INSTANCE);
  }

  private void checkDistractorIndex(int index) {
    TestCheck.input(index >= 0);
    TestCheck.input(index < fDistractors.size());
  }

  public void moveItem(int index, boolean forward) {
    checkItemIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkItemIndex(newIndex);

    Collections.swap(fItems, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  public void moveDistractor(int index, boolean forward) {
    checkDistractorIndex(index);
    int newIndex = index + (forward ? 1 : -1);
    checkDistractorIndex(newIndex);

    Collections.swap(fDistractors, index, newIndex);
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable Integer itemLimit() {
    return fItemLimit;
  }

  public void setItemLimit(@Nullable Integer value) {
    if (value != null) {
      TestCheck.input(value >= 1);
      TestCheck.input(value <= MAX_ELEMENTS);
    }

    fItemLimit = value;
    post(ChangeEvent.INSTANCE);
  }

  public @Nullable Integer distractorLimit() {
    return fDistractorLimit;
  }

  public void setDistractorLimit(@Nullable Integer value) {
    if (value != null) {
      TestCheck.input(value >= 1);
      TestCheck.input(value <= MAX_ELEMENTS);
    }

    fDistractorLimit = value;
    post(ChangeEvent.INSTANCE);
  }

  public boolean hasDefaultOptions() {
    return (fItemLimit == null) && (fDistractorLimit == null);
  }

  @Override public void setDefaults(String initialText) {
    formulation().appendText(initialText);
  }
}
