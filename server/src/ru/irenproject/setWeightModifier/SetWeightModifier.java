/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.setWeightModifier;

import ru.irenproject.Modifier;
import ru.irenproject.QuestionItem;
import ru.irenproject.common.setWeightModifier.Proto.SetWeightModifierType;
import ru.irenproject.infra.Realm;

public final class SetWeightModifier extends Modifier {
  private int fWeight = 1;

  public SetWeightModifier(Realm realm) {
    super(realm);
  }

  @Override public String type() {
    return SetWeightModifierType.setWeight.name();
  }

  public int weight() {
    return fWeight;
  }

  public void setWeight(int value) {
    QuestionItem.checkValidWeight(value);
    fWeight = value;
  }
}
