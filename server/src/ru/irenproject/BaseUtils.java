/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import com.google.common.base.CharMatcher;
import com.google.common.primitives.Ints;

import javax.annotation.Nullable;
import java.util.Iterator;
import java.util.function.BiPredicate;

public final class BaseUtils {
  public static String shortenString(String s, int softLimit, int hardLimit) {
    String res;
    if (s.length() <= softLimit) {
      res = s;
    } else {
      String ellipsis = "...";

      int k = softLimit;
      int e = Integer.min(s.length(), hardLimit);
      while ((k < e) && !Character.isWhitespace(s.charAt(k))) {
        ++k;
      }
      if (k < e) {
        res = s.substring(0, k) + ellipsis;
      } else if (s.length() <= hardLimit) {
        res = s;
      } else {
        int hardBreak = hardLimit;
        if (Character.isLowSurrogate(s.charAt(hardLimit))) {
          --hardBreak;
        }
        res = s.substring(0, hardBreak) + ellipsis;
      }
    }

    return res;
  }

  public static CharMatcher javaDigitCharMatcher() {
    //noinspection deprecation
    return CharMatcher.javaDigit();
  }

  public static CharMatcher javaLetterOrDigitCharMatcher() {
    //noinspection deprecation
    return CharMatcher.javaLetterOrDigit();
  }

  public static @Nullable Integer tryParseIntegerAmount(String s) {
    Integer res;
    if (s.equals("0")) {
      res = 0;
    } else if (s.startsWith("0") || s.startsWith("-")) {
      res = null;
    } else {
      res = Ints.tryParse(s);
    }
    return res;
  }

  public static <T> boolean iterablesEqual(Iterable<T> a, Iterable<T> b,
      BiPredicate<? super T, ? super T> equalityTester) {
    Iterator<T> ia = a.iterator();
    Iterator<T> ib = b.iterator();
    boolean equals = true;
    while (ia.hasNext() && ib.hasNext()) {
      if (!equalityTester.test(ia.next(), ib.next())) {
        equals = false;
        break;
      }
    }
    return equals && !ia.hasNext() && !ib.hasNext();
  }

  private BaseUtils() {}
}
