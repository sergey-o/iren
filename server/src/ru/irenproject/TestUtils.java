/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import com.google.common.base.CharMatcher;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.math.BigInteger;

public final class TestUtils {
  private static final CharMatcher DIGIT_MATCHER = CharMatcher.inRange('0', '9');

  public static @Nullable BigInteger toNumericTagIfPossible(String s) {
    return (!s.isEmpty() && !s.startsWith("0") && DIGIT_MATCHER.matchesAllOf(s)) ? new BigInteger(s) : null;
  }

  public static BigDecimal parseNormalizedDecimal(String s) {
    TestCheck.input(s.length() == 6);
    TestCheck.input(s.charAt(1) == '.');
    TestCheck.input(DIGIT_MATCHER.countIn(s) == s.length() - 1);

    BigDecimal res = new BigDecimal(s);
    TestCheck.input(res.compareTo(BigDecimal.ONE) <= 0);
    return res;
  }

  private TestUtils() {}
}
