/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

var window = this;

var document = {};
Object.bindProperties(document, nativeDocument);

document.appendChild(document.createElement("html"));
document.head = document.documentElement.appendChild(document.createElement("head"));
document.body = document.documentElement.appendChild(document.createElement("body"));

var navigator = {
    platform: "",
    userAgent: ""};

function setTimeout(f, delay) {
  if (delay !== 0) {
    throw "Unexpected setTimeout.";
  }
  f();
}

var renderedFormula;
var unknownCharacterEncountered;

var MathJax = {
    showProcessingMessages: false,
    skipStartupTypeset: true,

    SVG: {useGlobalCache: false},

    AuthorInit: function() {
      delete MathJax.Hub.config.errorSettings.message; // disable formatError call in MathJax.Hub.processError

      MathJax.Hub.config.positionToHash = false;

      MathJax.Hub.processSectionDelay = 0;
      MathJax.Hub.Startup.loadArray = function(files, dir, name, synchronous) {};
      MathJax.Hub.Startup.MenuZoom = function() {};

      MathJax.Hub.elementScripts = function(element) {
        return [element];
      };

      MathJax.Extension.MathEvents = {Event: {}, Touch: {}, Hover: {}};

      MathJax.Ajax.Load = function(file, callback) {
        throw "MathJax.Ajax.Load: " + file.JS;
      };

      MathJax.Ajax.StyleString = function(styles) {
        return "";
      };

      MathJax.HTML.Element = function(type, def, contents) {
        return document.createElement(type);
      };

      MathJax.Message.Init = function(styles) {};
      MathJax.Message.Set = function(text, n, clearDelay) {};
      MathJax.Message.Clear = function(n, delay) {};
      MathJax.Message.Remove = function() {};

      MathJax.Hub.Register.MessageHook("SVG Jax - unknown char", function(message) {
        unknownCharacterEncountered = true;
      });

      MathJax.Hub.Register.StartupHook("SVG Jax Config", function() {
        var SVG = MathJax.OutputJax.SVG;

        SVG.InitializeSVG = function() {};

        var originalSvgElement = SVG.Element;
        SVG.Element = function(type, def) {
          var newType;
          if (type === "svg") {
            renderedFormula = {
                svg: document.createElementNS("http://www.w3.org/2000/svg", type),
                style: {},
                setAttribute: function(name, value) {
                  this.svg.setAttribute(name, value);
                },
                getAttribute: function(name) {
                  return this.svg.getAttribute(name);
                },
                appendChild: function(node) {
                  return this.svg.appendChild(node);
                }};
            newType = renderedFormula;
          } else {
            newType = type;
          }
          return originalSvgElement.call(this, newType, def);
        };

        SVG.addElement = function(parent, type, def) {};

        Object.getPrototypeOf(SVG).preTranslate = function(state) {
          var formula = state.jax[this.id][0];
          var ex = 6;
          formula.MathJax.elementJax.SVG = {
              ex: ex,
              em: ex / SVG.TeX.x_height * 1000};
        };

        SVG.resetGlyphs = function(reset) {
          Object.getPrototypeOf(SVG).resetGlyphs.call(this, true);
        };

        MathJax.ElementJax.mml.mbase.prototype.SVGprocessStyles = function(style) {
          return {};
        };

        // from https://github.com/mathjax/MathJax-node/blob/master/lib/main.js (modified)
        SVG.BBOX.TEXT.Augment({
          Init: function (scale,text,def) {
            if (!def) {def = {}}; def.stroke = "none";
            if (def["font-style"] === "") delete def["font-style"];
            if (def["font-weight"] === "") delete def["font-weight"];
            this.SUPER(arguments).Init.call(this,def);
            SVG.addText(this.element,text);
            var textWidth = text.length * 8.5;
            var bbox = {width: textWidth, height: 18, y: -12};
            scale *= 1000/SVG.em;
            this.element.setAttribute("font-family","monospace");
            this.element.setAttribute("transform","scale("+scale+") matrix(1 0 0 -1 0 0)");
            this.w = this.r = bbox.width*scale; this.l = 0;
            this.h = this.H = -bbox.y*scale;
            this.d = this.D = (bbox.height + bbox.y)*scale;
          }
        });
      });
    }};

function postInitialize() {
  var allowedEnvironments = {
      align: null,
      aligned: null,
      array: null,
      Bmatrix: null,
      bmatrix: null,
      cases: null,
      matrix: null,
      pmatrix: null,
      smallmatrix: null,
      subarray: null,
      Vmatrix: null,
      vmatrix: null};
  var environments = MathJax.InputJax.TeX.Definitions.environment;
  Object.keys(environments).forEach(function(environmentName) {
    if (!allowedEnvironments.hasOwnProperty(environmentName)) {
      delete environments[environmentName];
    }
  });

  var macros = MathJax.InputJax.TeX.Definitions.macros;
  Object.keys(macros).forEach(function(macroName) {
    var macro = macros[macroName];
    if ((macro instanceof Array) && (macro[0] === "Extension")) {
      delete macros[macroName];
    }
  });

  delete macros.above;
  delete macros.abovewithdelims;
  delete macros.array;
  delete macros.bmod; // uses \mmlToken
  delete macros.buildrel;
  delete macros.cases;
  delete macros.cfrac;
  delete macros.color;
  delete macros.cr;
  delete macros.DeclareMathOperator;
  delete macros.displaylines;
  delete macros.eqalign;
  delete macros.eqalignno;
  delete macros.eqref;
  delete macros.genfrac;
  delete macros.hfil;
  delete macros.hfilll;
  delete macros.hskip;
  delete macros.hspace;
  delete macros.idotsint;
  delete macros.kern;
  delete macros.label;
  delete macros.LaTeX; // uses \kern
  delete macros.leftroot;
  delete macros.leqalignno;
  delete macros.lower;
  delete macros.matrix;
  delete macros.mkern;
  delete macros.mmlToken;
  delete macros.mod; // uses \mmlToken
  delete macros.moveleft;
  delete macros.moveright;
  delete macros.mskip;
  delete macros.mspace;
  delete macros.newline;
  delete macros.nonumber;
  delete macros.notag;
  delete macros.pmatrix;
  delete macros.pmb; // uses \kern
  delete macros.pmod; // uses \mmlToken
  delete macros.pod; // uses \kern
  delete macros.raise;
  delete macros.ref;
  delete macros.require;
  delete macros.root;
  delete macros.rule;
  delete macros.Rule;
  delete macros.shoveleft;
  delete macros.shoveright;
  delete macros.skew;
  delete macros.smash;
  delete macros.Space;
  delete macros.tag;
  delete macros.TeX; // uses \kern
  delete macros.uproot;
  delete macros.varinjlim; // uses \mmlToken
  delete macros.varliminf; // uses \mmlToken
  delete macros.varlimsup; // uses \mmlToken
  delete macros.varprojlim; // uses \mmlToken

  macros["\\"] = "Cr";
  macros.arctg = "NamedFn";
  macros.ch = "NamedFn";
  macros.cosec = "NamedFn";
  macros.ctg = "NamedFn";
  macros.cth = "NamedFn";
  macros.sh = "NamedFn";
  macros.tg = "NamedFn";
  macros.th = "NamedFn";

  var mathchar0mi = MathJax.InputJax.TeX.Definitions.mathchar0mi;
  delete mathchar0mi.S; // glyph missing

  MathJax.InputJax.TeX.Definitions.number = /^([0-9]+((,[0-9]+)+|(\.[0-9]*)*)|\.[0-9]+)/;

  var originalParse = MathJax.InputJax.TeX.Parse.prototype.Parse;
  MathJax.InputJax.TeX.Parse.prototype.Parse = function() {
    if (this.string.length > 1000) {
      MathJax.InputJax.TeX.Error("The formula is too long");
    }
    originalParse.call(this);
  };

  // prevent MathJax from doing lookups in Object.prototype
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.delimiter, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.environment, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.macros, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.mathchar0mi, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.mathchar0mo, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.mathchar7, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.remap, null);
  Object.setPrototypeOf(MathJax.InputJax.TeX.Definitions.special, null);
}

var cssMap = {
    verticalAlign: "vertical-align",
    marginTop: "margin-top",
    marginBottom: "margin-bottom",
    marginLeft: "margin-left",
    marginRight: "margin-right"};

function render(tex) {
  var formula = {
      type: "math/tex;mode=display",
      text: tex,
      parentNode: {},
      previousSibling: {
          className: "",
          style: {},
          appendChild: function(node) {},
          removeChild: function(node) {}}};

  // see MathJax.HTML.getScript
  if (tex === "") {
    formula.innerHTML = tex;
  }

  renderedFormula = null;
  unknownCharacterEncountered = false;
  document.head.textContent = document.body.textContent = "";
  var processed = false;

  MathJax.Hub.Process([formula], function() {
    processed = true;
  });

  if (!processed) {
    throw "MathJax.Hub.Process did not complete.";
  }

  if (!renderedFormula) {
    throw "Formula rendering failed.";
  }

  if (unknownCharacterEncountered) {
    throw "Unknown character encountered.";
  }

  var style = {
      width: renderedFormula.svg.getAttribute("width"),
      height: renderedFormula.svg.getAttribute("height")};
  var styleAttribute = [];

  Object.keys(renderedFormula.style).forEach(function(propertyName) {
    if (cssMap.hasOwnProperty(propertyName)) {
      var cssName = cssMap[propertyName];
      var value = renderedFormula.style[propertyName];
      style[cssName] = value;
      styleAttribute.push(cssName + ": " + value);
    }
  });

  renderedFormula.svg.setAttribute("style", "/*" + styleAttribute.join("; ") + ";*/");
  return [renderedFormula.svg, style];
}
