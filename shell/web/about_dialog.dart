/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';

class AboutDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-about-dialog";

  DialogElement w_dialog;
  SpanElement w_version;
  AnchorElement w_link;
  ButtonElement w_licenseButton;
  ButtonElement w_librariesButton;
  ButtonElement w_translatorsButton;

  AboutDialog() : super(TAG);

  Future<void> openModal() {
    w_version.text = programVersion;
    w_link
        ..onClick.listen(_onLinkClick)
        ..onFocus.first.then((_) => w_link.blur());
    w_licenseButton.onClick.listen((_) => CreditsDialog("LICENSE", 30).openModal());
    w_librariesButton.onClick.listen((_) => CreditsDialog("credits/CREDITS", 30).openModal());
    w_translatorsButton.onClick.listen((_) => CreditsDialog("TRANSLATORS", 8).openModal());
    return open(w_dialog);
  }

  void _onLinkClick(MouseEvent e) {
    stopFurtherProcessing(e);
    shell_openExternal(w_link.text);
  }
}

class CreditsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-credits-dialog";

  DialogElement w_dialog;
  TextAreaElement w_credits;

  final String _fileName;
  final int _rows;

  CreditsDialog(this._fileName, this._rows) : super(TAG);

  Future<void> openModal() {
    w_credits
        ..rows = _rows
        ..value = fs_readFileSync_String(path_normalize(getAbsoluteAppPath(
            "${developmentMode ? "" : "../"}../../$_fileName")), "utf8")
        ..setSelectionRange(0, 0);
    return open(w_dialog);
  }
}
