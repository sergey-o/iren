/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/match_question_editor.pb.dart';
import 'package:iren_proto/question/match_question.pb.dart';
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'pad_editor.dart';
import 'registries.dart';

class MatchQuestionEditor extends Widget<XMatchQuestionEditor> {
  static const String TAG = "ru-irenproject-match-question-editor";

  DivElement w_toolbar;
  ButtonElement w_addPairButton;
  ButtonElement w_addDistractorButton;
  ButtonElement w_deleteRowButton;
  ButtonElement w_moveRowUpButton;
  ButtonElement w_moveRowDownButton;
  ButtonElement w_optionsButton;
  DivElement w_itemPanel;

  PadEditor _formulation;

  /* nullable */int _selectRow;
  /* nullable */int _focusedRow;

  MatchQuestionEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addPairButton.onClick.listen((_) => _addPair());
    w_addDistractorButton.onClick.listen((_) => _addDistractor());
    w_deleteRowButton.onClick.listen((_) => _deleteRow());
    w_moveRowUpButton.onClick.listen((_) => _moveRowUp());
    w_moveRowDownButton.onClick.listen((_) => _moveRowDown());
    w_optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addPair)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addDistractor)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteRow);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveRowUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveRowDown);

    _formulation = PadEditor(shape.formulationEditor, dispatcher)
        ..element.id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation.element);

    Element.focusEvent.forTarget(element.shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(element.shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedRow = null;
      w_itemPanel.nodes.clear();

      shape.leftEditor.asMap().forEach((i, leftName) => w_itemPanel.append(DivElement()
          ..classes.add("pair")
          ..append(PadEditor(leftName, dispatcher).element)
          ..append(PadEditor(shape.rightEditor[i], dispatcher).element)));

      for (String rightName in shape.rightEditor.sublist(shape.leftEditor.length)) {
        w_itemPanel.append(PadEditor(rightName, dispatcher).element
            ..classes.add("distractor"));
      }

      w_addPairButton.disabled = w_addDistractorButton.disabled = !_canAdd();
      w_optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (var e in w_itemPanel.querySelectorAll(PadEditor.TAG)) {
      getWidget(e).render();
    }

    if ((_selectRow != null) && shape.rightEditor.isNotEmpty) {
      _selectRow = _selectRow.clamp(0, shape.rightEditor.length - 1) as int;
      Element e = w_itemPanel.children[_selectRow];
      if (!_isDistractorRow(_selectRow)) {
        e = e.children.first;
      }
      getWidget(e).focusComponent();
    }
    _selectRow = null;

    _updateUi();
  }

  void _addPair() {
    if (_canAdd()) {
      performAction(XMatchQuestionEditor_AddPair());
      _selectRow = shape.leftEditor.length;
    }
  }

  bool _canAdd() => shape.rightEditor.length < shape.maxRows;

  void _addDistractor() {
    if (_canAdd()) {
      performAction(XMatchQuestionEditor_AddDistractor());
      _selectRow = shape.rightEditor.length;
    }
  }

  void _deleteRow() {
    if (_focusedRow != null) {
      performAction(XMatchQuestionEditor_DeleteRow()
          ..index = _focusedRow);
      _selectRow = _focusedRow;
    }
  }

  void _moveRowUp() {
    if (_canMoveRowUp()) {
      performAction(XMatchQuestionEditor_MoveRow()
          ..index = _focusedRow
          ..forward = false);
      _selectRow = _focusedRow - 1;
    }
  }

  void _moveRowDown() {
    if (_canMoveRowDown()) {
      performAction(XMatchQuestionEditor_MoveRow()
          ..index = _focusedRow
          ..forward = true);
     _selectRow = _focusedRow + 1;
    }
  }

  bool _canMoveRowUp() => (_focusedRow != null) && (_focusedRow > 0)
      && (_isDistractorRow(_focusedRow) == _isDistractorRow(_focusedRow - 1));

  bool _isDistractorRow(int row) => row >= shape.leftEditor.length;

  bool _canMoveRowDown() => (_focusedRow != null) && (_focusedRow < shape.rightEditor.length - 1)
      && (_isDistractorRow(_focusedRow) == _isDistractorRow(_focusedRow + 1));

  void _onFocus() {
    int index = w_itemPanel.children.indexOf(element.shadowRoot.activeElement?.parent);
    if (index == -1) {
      index = w_itemPanel.children.indexOf(element.shadowRoot.activeElement);
    }
    if (index != -1) {
      _focusedRow = index;
    }

    _updateUi();
  }

  void _onBlur(Event e) {
    if (!w_toolbar.contains((e as FocusEvent).relatedTarget as Node)) {
      _focusedRow = null;
    }
    _updateUi();
  }

  void _updateUi() {
    w_deleteRowButton.disabled = (_focusedRow == null);
    w_moveRowUpButton.disabled = !_canMoveRowUp();
    w_moveRowDownButton.disabled = !_canMoveRowDown();
  }

  void _showOptionsDialog() {
    MatchQuestionOptionsDialog(shape, dispatcher, name).openModal();
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

class MatchQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-match-question-options-dialog";

  DialogElement w_dialog;
  RadioButtonInputElement w_randomPairs;
  NumberInputElement w_pairLimit;
  RadioButtonInputElement w_randomDistractors;
  NumberInputElement w_distractorLimit;

  final XMatchQuestionEditor _source;
  final Dispatcher _dispatcher;
  final String _targetName;

  MatchQuestionOptionsDialog(this._source, this._dispatcher, this._targetName) : super(TAG);

  Future<void> openModal() async {
    w_pairLimit.max = w_distractorLimit.max = _source.maxRows.toString();

    if (_source.hasPairLimit()) {
      w_randomPairs.checked = true;
      w_pairLimit.valueAsNumber = _source.pairLimit;
    }

    if (_source.hasDistractorLimit()) {
      w_randomDistractors.checked = true;
      w_distractorLimit.valueAsNumber = _source.distractorLimit;
    }

    w_randomPairs.onClick.listen((_) => focusInputElement(w_pairLimit));
    w_randomDistractors.onClick.listen((_) => focusInputElement(w_distractorLimit));

    _updateUi();

    w_dialog
        ..onChange.listen((_) => _updateUi())
        ..onClick.listen(_onDialogClick);

    await open(w_dialog);

    if (w_dialog.returnValue.isNotEmpty) {
      var action = XMatchQuestionEditor_SetOptions();
      if (w_randomPairs.checked) {
        action.pairLimit = w_pairLimit.valueAsNumber.toInt();
      }
      if (w_randomDistractors.checked) {
        action.distractorLimit = w_distractorLimit.valueAsNumber.toInt();
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    w_pairLimit
        ..required = w_randomPairs.checked
        ..disabled = !w_randomPairs.checked;
    w_distractorLimit
        ..required = w_randomDistractors.checked
        ..disabled = !w_randomDistractors.checked;
  }

  void _onDialogClick(MouseEvent e) {
    if ((e.target == w_pairLimit) && w_pairLimit.disabled) {
      w_randomPairs.dispatchEvent(MouseEvent("click"));
    } else if ((e.target == w_distractorLimit) && w_distractorLimit.disabled) {
      w_randomDistractors.dispatchEvent(MouseEvent("click"));
    }
  }
}

const String _LIB = "match_question_editor";

void initialize() {
  registerShapeParser((b) => XMatchQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(QuestionDescriptor(
      type: MatchQuestionType.match.name,
      title: translate(_LIB, "Matching Question") as String,
      priority: 3000,
      icon: "match_question_editor.resources/matchQuestion.png",
      initialText: translate(_LIB, "Match the items.") as String));

  registerQuestionEditorFactory(
      XMatchQuestionEditor,
      (name, dispatcher) => MatchQuestionEditor(name, dispatcher));
}
