/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'archive_editor.dart' as archive_editor;
import 'classify_question_editor.dart' as classify_question_editor;
import 'editor_infra.dart' as editor_infra;
import 'file_manager.dart' as file_manager;
import 'input_question_editor.dart' as input_question_editor;
import 'match_question_editor.dart' as match_question_editor;
import 'modifier_screen.dart' as modifier_screen;
import 'order_question_editor.dart' as order_question_editor;
import 'pad_editor.dart' as pad_editor;
import 'profile_screen.dart' as profile_screen;
import 'question_list_editor.dart' as question_list_editor;
import 'question_viewer.dart' as question_viewer;
import 'script_modifier_editor.dart' as script_modifier_editor;
import 'section_tree_editor.dart' as section_tree_editor;
import 'select_question_editor.dart' as select_question_editor;
import 'set_evaluation_model_modifier_editor.dart' as set_evaluation_model_modifier_editor;
import 'supervisor_screen.dart' as supervisor_screen;
import 'test_document_editor.dart' as test_document_editor;
import 'test_screen.dart' as test_screen;

void initializeProgramLibraries() {
  editor_infra.initialize();
  file_manager.initialize();
  test_document_editor.initialize();
  test_screen.initialize();
  question_list_editor.initialize();
  section_tree_editor.initialize();
  question_viewer.initialize();
  pad_editor.initialize();
  modifier_screen.initialize();
  profile_screen.initialize();
  archive_editor.initialize();
  supervisor_screen.initialize();

  select_question_editor.initialize();
  input_question_editor.initialize();
  match_question_editor.initialize();
  order_question_editor.initialize();
  classify_question_editor.initialize();

  set_evaluation_model_modifier_editor.initialize();
  script_modifier_editor.initialize();
}
