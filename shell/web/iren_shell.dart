/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_client/iren_client.dart';
import 'package:web_helpers/translator.dart';

import 'electron.dart';
import 'initialization.dart';
import 'iren_editor.dart';
import 'iren_master.dart';
import 'iren_shell.embeddedResources.dart';
import 'iren_shell.reflectable.dart';
import 'languages.dart';
import 'misc_utils.dart';

void main() {
  initializeReflectable();
  initializeEmbeddedResources();
  initializeElectronLibrary();

  document.documentElement.dataset["platform"] = process_platform;

  disableDragAndDrop(document.documentElement);
  preventOpeningLinksInNewWindowWithMiddleClick();

  registerShellDictionaries();
  registerClientDictionaries();
  selectDictionary(window.navigator.language);

  initializeProgramLibraries();
  registerAreaWidgets();

  switch (ipcRenderer_sendSync("getProgramName") as String) {
    case "editor":
      runEditor();
      break;
    case "master":
      runMaster();
      break;
  }
}
