/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/editor.pb.dart';
import 'package:iren_proto/export.pb.dart';

import 'about_dialog.dart';
import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';

class FileManager extends Widget<XFileManager> {
  static const String TAG = "ru-irenproject-file-manager";
  static const String _WIDGET_NAME = "";
  static const List<String> _FILE_EXTENSIONS = ["itx", "it3", "it2", "itar"];
  static const List<String> _IMPORTED_TEST_EXTENSIONS = ["it3", "it2"];

  static Future<FileManager> create(EditorConnection connection) async {
    var dispatcher = Dispatcher(connection, Scene(), FileManagerChannel.fileManager.name);
    await dispatcher.performAction(_WIDGET_NAME, XFileManager_Nop());
    await dispatcher.onChangeScene.first;

    return FileManager._create(_WIDGET_NAME, dispatcher);
  }

  ButtonElement w_createButton;
  ButtonElement w_openButton;
  ButtonElement w_saveButton;
  ButtonElement w_exportToHtmlButton;
  ButtonElement w_aboutButton;
  ButtonElement w_exitButton;
  ButtonElement w_closeButton;
  SpanElement w_fileSwitcher;

  final Map<String, Dispatcher> _dispatchersByChannel = {};
  ChannelDispatcherFactory channelDispatcherFactory;
  /* nullable */String _selectedChannel;
  /* nullable */String get selectedChannel => _selectedChannel;
  bool _selectedFileModified = false;
  bool _hasAutoClosableFile = false;

  final StreamController<void> _onSelectChannel = StreamController.broadcast(sync: true);
  Stream<void> get onSelectChannel => _onSelectChannel.stream;

  final StreamController<String> _onCloseChannel = StreamController.broadcast(sync: true);
  Stream<String> get onCloseChannel => _onCloseChannel.stream;

  final StreamController<void> _onUpdateTitle = StreamController.broadcast(sync: true);
  Stream<void> get onUpdateTitle => _onUpdateTitle.stream;

  final StreamController<void> _onAfterSave = StreamController.broadcast(sync: true);
  Stream<void> get onAfterSave => _onAfterSave.stream;

  FileManager._create(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_createButton.onClick.listen((_) => _create());
    w_openButton.onClick.listen((_) => _open());
    w_saveButton.onClick.listen((_) => _save());
    w_exportToHtmlButton.onClick.listen((_) => _exportToHtml());
    w_aboutButton.onClick.listen((_) => AboutDialog().openModal());
    w_exitButton.onClick.listen((_) => _exit());
    w_closeButton.onClick.listen((_) => _close());

    windowShortcutHandler
        ..add(KeyCode.N, KeyModifierState.CTRL, _create)
        ..add(KeyCode.O, KeyModifierState.CTRL, _open)
        ..add(KeyCode.S, KeyModifierState.CTRL, _save)
        ..add(KeyCode.Q, KeyModifierState.ALT_SHIFT, _exit)
        ..add(KeyCode.F4, KeyModifierState.CTRL, _close)
        ..add(KeyCode.PAGE_UP, KeyModifierState.CTRL, () => _selectAdjacentFile(-1))
        ..add(KeyCode.PAGE_DOWN, KeyModifierState.CTRL, () => _selectAdjacentFile(1));
  }

  Future<void> activate() async {
    render();
    dispatcher.onChangeScene.listen((_) {
      if (!dispatcher.waitingForReply) {
        render();
      }
    });

    for (var f in shape.file) {
      _dispatchersByChannel[f.channel] = await channelDispatcherFactory(f.channel);
    }

    _selectChannel(shape.file.isEmpty ? null : shape.file.last.channel);
  }

  void _selectChannel(/* nullable */String channel) {
    _selectedChannel = channel;
    _updateUiForSelectedChannel();
    _onSelectChannel.add(null);
  }

  @override void render() {
    super.render();

    shape.file.asMap().forEach((i, f) {
      ButtonElement button = ((i < w_fileSwitcher.children.length) ?
          w_fileSwitcher.children[i] : w_fileSwitcher.append(_createFileButton())) as ButtonElement;
      button
          ..dataset["channel"] = f.channel
          ..text = getFileTitle(f.name, _FILE_EXTENSIONS)
          ..title = f.name;
    });

    while (w_fileSwitcher.children.length > shape.file.length) {
      w_fileSwitcher.children.removeLast();
    }

    _updateUiForSelectedChannel();
    w_closeButton.disabled = shape.file.isEmpty;
  }

  /* nullable */String get selectedFileTitle {
    OpenFile f = _selectedFile();
    return (f == null) ? null : getFileTitle(f.name, _FILE_EXTENSIONS);
  }

  void _updateUiForSelectedChannel() {
    for (var button in w_fileSwitcher.children.cast<ButtonElement>()) {
      bool selected = (button.dataset["channel"] == _selectedChannel);
      button.classes.toggle("selected", selected);
      if (selected && w_fileSwitcher.contains(element.shadowRoot.activeElement)) {
        button.focus();
      }
    }
    _onUpdateTitle.add(null);
  }

  ButtonElement _createFileButton() => ButtonElement()
      ..onClick.listen(_onFileButtonClick)
      ..on["auxclick"].listen((e) => _onFileButtonAuxClick(e as MouseEvent));

  void _onFileButtonClick(MouseEvent e) {
    _selectChannel((e.currentTarget as ButtonElement).dataset["channel"]);
  }

  void _onFileButtonAuxClick(MouseEvent e) {
    if (e.button == 1) {
      _selectChannel((e.currentTarget as ButtonElement).dataset["channel"]);
      postAnimationFrame.then((_) => _close());
    }
  }

  Future<void> _open() async {
    List<String> fileNames = dialog_showOpenDialogSync(getCurrentWindow(), ShowOpenDialogOptions(
        filters: [
            FileDialogFilter(name: tr("Tests and Archives"), extensions: _FILE_EXTENSIONS),
            FileDialogFilter(name: tr("All Files"), extensions: ["*"])],
        properties: ["openFile", "multiSelections"])) ?? [];

    for (String fileName in fileNames) {
      await openFile(fileName);
    }
  }

  Future<void> openFile(String fileName) async {
    OpenFile existing = shape.file.firstWhere((f) => f.name == fileName, orElse: () => null);
    if (existing == null) {
      try {
        if (_hasAutoClosableFile) {
          _hasAutoClosableFile = false;
          await _close();
        }

        _selectChannel(await _openOrCreate(XFileManager_Open()
            ..fileName = fileName
            ..createNew = false
            ..language = window.navigator.language));
      } catch (e) {
        showErrorMessage(e.toString());
      }
    } else {
      _selectChannel(existing.channel);
    }
  }

  Future<String> _openOrCreate(XFileManager_Open action) async {
    var output = XFileManager_Open_Reply.fromBuffer((await performAction(action)).output);

    switch (output.result) {
      case XFileManager_Open_Reply_Result.OK: break;
      case XFileManager_Open_Reply_Result.INCORRECT_FILE: throw trD("CANNOT_OPEN_FILE")(action.fileName);
      case XFileManager_Open_Reply_Result.UNKNOWN_FILE_VERSION: throw getUnknownFileVersionMessage(action.fileName);
      default: throw false;
    }

    await dispatcher.onChangeScene.first;

    String res = shape.file.last.channel;
    _dispatchersByChannel[res] = await channelDispatcherFactory(res);

    return res;
  }

  Future<void> _create() async {
    _selectChannel(await _openOrCreate(XFileManager_Open()
        ..fileName = ""
        ..createNew = true
        ..language = window.navigator.language));
    _hasAutoClosableFile = false;
  }

  Future<bool> _close() async {
    bool res;
    OpenFile f = _selectedFile();
    if (f == null) {
      res = false;
    } else {
      if (!_selectedFileModified) {
        res = true;
      } else {
        int response = showMessageBox(ShowMessageBoxOptions(
            type: "question",
            buttons: [tr("Save"), tr("Don't Save"), tr("Cancel")],
            message: trD("SAVE_CHANGES")(getFileTitle(f.name, _FILE_EXTENSIONS)) as String,
            cancelId: 2));
        res = (response == 0) ? await _save() : (response == 1);
      }

      if (res) {
        int index = shape.file.indexOf(f);
        String channelToSelect;
        if (index < shape.file.length - 1) {
          channelToSelect = shape.file[index + 1].channel;
        } else if (index > 0) {
          channelToSelect = shape.file[index - 1].channel;
        } else {
          channelToSelect = null;
        }

        await performAction(XFileManager_Close()
            ..channel = _selectedChannel);
        _onCloseChannel.add(_selectedChannel);
        _dispatchersByChannel.remove(_selectedChannel)?.stop();

        _selectChannel(channelToSelect);
        _hasAutoClosableFile = false;
        await dispatcher.onChangeScene.first;
      }
    }

    return res;
  }

  Future<bool> closeAll() async {
    while (await _close()) {
      await lockUiUntilRepainted();
    }

    return shape.file.isEmpty;
  }

  /* nullable */OpenFile _selectedFile() => shape.file.firstWhere((f) => f.channel == _selectedChannel,
      orElse: () => null);

  void _selectAdjacentFile(int offset) {
    OpenFile f = _selectedFile();
    if ((f != null) && (shape.file.length >= 2)) {
      _selectChannel(shape.file[(shape.file.indexOf(f) + offset) % shape.file.length].channel);
    }
  }

  Future<bool> _save() async {
    bool res;
    OpenFile f = _selectedFile();
    if ((f != null) && !w_saveButton.disabled) {
      String fileName = f.name;

      String defaultNewName;
      bool queryName;
      if (fileName.isEmpty) {
        defaultNewName = null;
        queryName = true;
      } else {
        defaultNewName = dropFileExtension(fileName, _IMPORTED_TEST_EXTENSIONS);
        queryName = (defaultNewName != fileName);
      }

      if (queryName) {
        fileName = showSaveFileDialog("itx", testFilterName, defaultPathWithoutExtension: defaultNewName);
      }

      if (fileName == null) {
        res = false;
      } else {
        try {
          await performAction(XFileManager_Save()
              ..channel = _selectedChannel
              ..fileName = fileName);
          _hasAutoClosableFile = false;
          _onAfterSave.add(null);
          res = true;
        } on ActionException {
          res = false;
          showErrorMessage(trD("CANNOT_SAVE_FILE")(fileName) as String);
        }
      }
    } else {
      res = false;
    }

    return res;
  }

  set selectedFileModified(bool value) {
    _selectedFileModified = value;
    _updateSaveButton();
  }

  void _updateSaveButton() {
    OpenFile f = _selectedFile();
    w_saveButton.disabled = (f == null) || (f.name.isNotEmpty && !_selectedFileModified);
  }

  Dispatcher getDispatcher(String channel) => _dispatchersByChannel[channel];

  Future<void> createAutoClosableFileIfEmpty() async {
    if (shape.file.isEmpty) {
      await _create();
      _hasAutoClosableFile = true;
    }
  }

  void removeAutoClosableMark() {
    _hasAutoClosableFile = false;
  }

  void _exit() {
    getCurrentWindow().destroy();
  }

  set selectedFileExportableToHtml(bool value) {
    w_exportToHtmlButton.disabled = !value;
  }

  Future<void> _exportToHtml() async {
    OpenFile f = _selectedFile();
    if ((f != null) && !w_exportToHtmlButton.disabled) {
      String fileName = showSaveFileDialog("html", tr("HTML Files"),
          defaultPathWithoutExtension: f.name.isEmpty ? null : getFileTitle(f.name, _FILE_EXTENSIONS));
      if ((fileName != null) && (await _canContinueAfterHtmlExportNotice())) {
        try {
          var output = XFileManager_ExportToHtml_Reply.fromBuffer((await performAction(XFileManager_ExportToHtml()
              ..channel = _selectedChannel
              ..outputFileName = fileName
              ..options = (ExportOptions()
                  ..language = window.navigator.language
                  ..title = "${getFileTitle(fileName, ["html"])} [${tr("offline")}]"))).output);

          switch (output.result.whichResult()) {
            case ExportResult_Result.ok: break;
            case ExportResult_Result.ioError: throw trD("CANNOT_SAVE_FILE")(fileName) as String;
            case ExportResult_Result.customErrorMessage: throw output.result.customErrorMessage;
            case ExportResult_Result.notSet: throw false;
          }
        } catch (e) {
          showErrorMessage(e.toString());
        }
      }
    }
  }

  Future<bool> _canContinueAfterHtmlExportNotice() async {
    String skipHtmlExportNoticeFile = path_join(dataDirectory, "skipHtmlExportNotice");

    bool res;
    if (fs_existsSync(skipHtmlExportNoticeFile)) {
      res = true;
    } else {
      ShowMessageBoxResult noticeDialogResult;
      lockUi(autoDim: false);
      try {
        noticeDialogResult = (await showMessageBoxAsync(ShowMessageBoxOptions(
            type: "info",
            buttons: [tr("Create Stand-Alone Test"), tr("Cancel")],
            message: tr(
                "Stand-alone tests are intended for self-assessment purposes and don't include security measures"
                " present in online mode. In particular, by using tools for data unpacking and conversion,"
                " a stand-alone HTML test can be turned into a source form and opened in the editor."
                " Implementing a reliable protection for such offline files is technically impossible.\n\n"
                "For carrying out formal assessments, and generally in all cases where the confidentiality"
                " of the source test files must be ensured, the online testing mode (available via the \"Iren Server\""
                " shortcut) should be used."),
            checkboxLabel: tr("Don't show again"),
            cancelId: 1)));
      } finally {
        unlockUi();
      }

      res = (noticeDialogResult.response == 0);

      if (res && noticeDialogResult.checkboxChecked) {
        try {
          fs_writeFileSync(skipHtmlExportNoticeFile, "");
        } catch (_) {}
      }
    }

    return res;
  }
}

typedef ChannelDispatcherFactory = Future<Dispatcher> Function(String channel);

void initialize() {
  registerShapeParser((b) => XFileManager.fromBuffer(b));
}
