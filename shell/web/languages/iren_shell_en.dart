/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> en = {
  "iren_master": {
    "DIFFERENT_VERSION_REQUIRED":
      (String version) => "Version $version of the program is required for working with this server.",
    "WINDOW_TITLE": (String serverAddress) => "$serverAddress — Iren",
  },

  "iren_utils": {
    "UNKNOWN_FILE_VERSION":
      (String fileName) => 'A newer version of the program is required for working with the file "$fileName".',
  },

  "ru-irenproject-editor-main-screen": {
    "WINDOW_TITLE": (String fileTitle) => "$fileTitle — Iren",
  },

  "ru-irenproject-file-manager": {
    "CANNOT_OPEN_FILE": (String fileName) => 'Cannot open the file "$fileName".',
    "CANNOT_SAVE_FILE": (String fileName) => 'Cannot save the file "$fileName".',
    "SAVE_CHANGES": (String fileName) => 'Save changes in "$fileName"?',
  },

  "ru-irenproject-modifier-list-editor": {
    "SECTION_MODIFIERS": (String sectionName) => 'Section "$sectionName" Modifiers',
  },

  "ru-irenproject-question-list-editor": {
    "ADD_QUESTION": (String title) => "Add $title (F2)",
    "USE_TITLE_CASE": "true",
  },

  "ru-irenproject-section-tree-editor": {
    "DELETE_SECTION": (String name) => 'Delete section "$name"?',
  },

  "ru-irenproject-supervisor-screen": {
    "CANNOT_LOAD_TEST": (String fileName) => 'Cannot load the test from the file "$fileName".',
  }
};
