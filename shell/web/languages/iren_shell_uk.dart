/*
  © Translation contributors, see the TRANSLATORS file

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> uk = {
  "classify_question_editor": {
    "Classification Question": "Питання на класифікацію",
    "Classify the items.": "Виконати класифікацію.",
  },

  "input_question_editor": {
    "Text Input Question": "Питання з введенням відповіді",
    "Text Pattern": "Текстовий еталон",
    "Add Text Pattern": "Додати текстовий еталон",
    "Regular Expression": "Регулярний вираз",
    "Add Regular Expression": "Додати регулярний вираз",
  },

  "iren_master": {
    //TR "DIFFERENT_VERSION_REQUIRED":
    //TR   (String version) => "Для работы с этим сервером нужна версия $version программы.",
    //TR "WINDOW_TITLE": (String serverAddress) => "$serverAddress — Айрен",
    "Iren": "Айрен",
    //TR "The password is incorrect.": "Неправильный пароль.",
    //TR "Logging in is temporarily disabled due to many failed attempts.":
    //TR   "Вход в систему временно запрещен из-за большого количества неудачных попыток.",
    //TR "Connect": "Подключиться",
    //TR "Cancel": "Отмена",
    //TR "You are connecting to this server for the first time.": "Вы впервые подключаетесь к этому серверу.",
    //TR "Server address:": "Адрес сервера:",
    //TR "Server identifier:": "Идентификатор сервера:",
    //TR "For security reasons, it is recommended that you verify the identifier"
    //TR ' before clicking "Connect" by contacting the server administrator.':
    //TR   'В целях безопасности рекомендуется сверить идентификатор перед нажатием кнопки "Подключиться",'
    //TR   " обратившись к администратору сервера.",
    //TR "THE IDENTIFIER OF THIS SERVER HAS CHANGED!": "ИДЕНТИФИКАТОР ЭТОГО СЕРВЕРА ИЗМЕНИЛСЯ!",
    //TR "A newer version of the program is required for working with this server.":
    //TR   "Для работы с этим сервером нужна новая версия программы.",
    //TR "Go to Download Page": "Перейти на страницу загрузки",
  },

  "iren_utils": {
    //TR "UNKNOWN_FILE_VERSION":
    //TR   (String fileName) => 'Для работы с файлом "$fileName" нужна новая версия программы.',
    "Iren": "Айрен",
    "Close": "Закрити",
    "Untitled": "Безіменний",
    "Tests": "Тести",
    "All Files": "Усі файли",
    "untitled": "безіменний",
  },

  "match_question_editor": {
    "Matching Question": "Питання на відповідність",
    "Match the items.": "Встановити відповідність.",
  },

  "modifier_screen": {
    "Allow Multiple Selection for Questions with a Single Correct Answer":
      "Заборона підказки про єдину правильну відповідь",
    'Add the "No Correct Answers" Choice': 'Додавання варіанту "Немає правильних відповідей"',
    'Set the "No Correct Answers" Choice Text': 'Зміна тексту варіанту "Немає правильних відповідей"',
    "Shuffle Choices": "Перемішування варіантів відповідей",
    "Set Scoring Mode": "Параметри моделі оцінювання",
    "Set Question Weight": "Зміна ваги питання",
    "Script": "Сценарій",
  },

  "order_question_editor": {
    "Ordering Question": "Питання на впорядкування",
    "Order the items.": "Встановити правильну послідовність.",
  },

  "select_question_editor": {
    "Multiple Choice Question": "Питання з вибором відповіді",
  },

  "ru-irenproject-about-dialog": {
    "Iren": "Айрен",
    "Version": "Версія",
    "© 2012–2020 Sergey Ostanin": "© 2012–2020 Сергій Останін",
    "License...": "Ліцензія...",
    "Libraries...": "Бібліотеки...",
    "Translators...": "Перекладачі...",
    "Close": "Закрити",
  },

  "ru-irenproject-archive-dialog": {
    //TR "Archive file:": "Файл архива:",
    //TR "Archive on the server": "Архив на сервере",
    //TR "Choose...": "Выбрать...",
    //TR "If the file exists, new data will be appended to it":
    //TR   "Если файл существует, новые данные будут добавлены к уже имеющимся",
    //TR "Send to Archive": "Отправить в архив",
    //TR "Cancel": "Отмена",
    //TR "Archives": "Архивы",
  },

  "ru-irenproject-archive-session-screen": {
    //TR "Student's Answer": "Ответ тестируемого",
    //TR "Correct Answer": "Верный ответ",
    //TR "Result:": "Оценка ответа:",
    //TR "Question weight:": "Вес вопроса:",
    //TR "Points earned:": "Набранные баллы:",
    //TR "Previous": "Назад",
    //TR "Next": "Вперед",
    //TR "Close": "Закрыть",
  },

  "ru-irenproject-archive-work-screen": {
    //TR "Close": "Закрыть",
    //TR "Student": "Тестируемый",
    //TR "Group": "Группа",
    //TR "Result, %": "Результат, %",
    //TR "Grade": "Оценка",
    //TR "Session Start": "Начало сеанса",
    //TR "IP Address": "IP-адрес",
    //TR "Save Table...": "Сохранить таблицу...",
    //TR "TSV Files": "Файлы TSV",
  },

  "ru-irenproject-change-time-limit-dialog": {
    //TR "New limit:": "Новое ограничение:",
    //TR "minutes": "мин.",
    //TR "Examples:": "Примеры:",
    //TR "Set limit to 5 minutes": "Установить ограничение в 5 минут",
    //TR "Increase time by 5 minutes": "Увеличить время на 5 минут",
    //TR "Reduce time by 5 minutes": "Сократить время на 5 минут",
    //TR "Remove time limit": "Убрать ограничение времени",
    //TR "Set": "Установить",
    //TR "Cancel": "Отмена",
  },

  "ru-irenproject-classify-question-editor": {
    "Add Item to Selected Category (F5)": "Додати елемент в обрану категорію (F5)",
    "Add Category (F6)": "Додати категорію (F6)",
    "Delete Item or Category (F8)": "Видалити елемент чи категорію (F8)",
    "Move Item or Category Up (Alt+Up)": "Перемістити елемент чи категорію вище (Alt+Вгору)",
    "Move Item or Category Down (Alt+Down)": "Перемістити елемент чи категорію нижче (Alt+Вниз)",
    "Options": "Параметри",
  },

  "ru-irenproject-classify-question-options-dialog": {
    "Items": "Елементи",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "From each category, select at least": "З кожної категорії не менше",
    "Cancel": "Відміна",
  },

  "ru-irenproject-connection-lost-dialog": {
    "No connection to the server": "Немає з'єднання з сервером",
    "Reconnect": "Підключитися",
  },

  "ru-irenproject-create-work-dialog": {
    //TR "Test:": "Тест:",
    //TR "Assign": "Назначить",
    //TR "Cancel": "Отмена",
  },

  "ru-irenproject-credits-dialog": {
    "Close": "Закрити",
  },

  "ru-irenproject-editor-main-screen": {
    "WINDOW_TITLE": (String fileTitle) => "$fileTitle — Айрен",
    "Iren": "Айрен",
  },

  "ru-irenproject-file-manager": {
    "CANNOT_OPEN_FILE": (String fileName) => 'Помилка при відкритті файлу "$fileName".',
    "CANNOT_SAVE_FILE": (String fileName) => 'Помилка при збереженні файлу "$fileName".',
    "SAVE_CHANGES": (String fileName) => 'Зберегти зміни в "$fileName"?',
    "New (Ctrl+N)": "Створити (Ctrl+N)",
    "Open (Ctrl+O)": "Відкрити (Ctrl+O)",
    "Save (Ctrl+S)": "Зберегти (Ctrl+S)",
    //TR "Create Stand-Alone (.html) Test": "Создать автономный (.html) тест",
    "About": "Про програму",
    "Remember Current State and Exit (Alt+Shift+Q)": "Запам'ятати поточний стан і вийти (Alt+Shift+Q)",
    //TR "Close File (Ctrl+F4)": "Закрыть файл (Ctrl+F4)",
    "Save": "Зберегти",
    "Don't Save": "Не зберігати",
    "Cancel": "Відміна",
    //TR "Tests and Archives": "Тесты и архивы",
    //TR "All Files": "Все файлы",
    //TR "HTML Files": "Файлы HTML",
    //TR "offline": "офлайн",
    //TR "Stand-alone tests are intended for self-assessment purposes and don't include security measures"
    //TR " present in online mode. In particular, by using tools for data unpacking and conversion,"
    //TR " a stand-alone HTML test can be turned into a source form and opened in the editor."
    //TR " Implementing a reliable protection for such offline files is technically impossible.\n\n"
    //TR "For carrying out formal assessments, and generally in all cases where the confidentiality"
    //TR " of the source test files must be ensured, the online testing mode (available via the \"Iren Server\""
    //TR " shortcut) should be used.":
    //TR   "Автономные тесты предназначены для самопроверки и не включают защитных мер, предусмотренных"
    //TR   " при тестировании в сетевом режиме. В частности, с помощью инструментов для распаковки"
    //TR   " и конвертирования данных автономный html-тест может быть преобразован в исходный вид и открыт"
    //TR   " в редакторе. Реализовать надежную защиту для таких файлов, работающих без использования сервера,"
    //TR   " технически невозможно.\n\n"
    //TR   "Для проведения контрольных мероприятий и в целом во всех случаях, когда важна конфиденциальность"
    //TR   " исходных файлов с тестами, следует использовать сетевой режим тестирования (ярлык \"Сервер Айрен\").",
    //TR "Don't show again": "Больше не показывать",
    //TR "Create Stand-Alone Test": "Создать автономный тест",
  },

  "ru-irenproject-formula-dialog": {
    //TR "Example": "Пример",
    //TR "Help": "Справка",
    //TR "Close": "Закрыть",
  },

  "ru-irenproject-input-question-editor": {
    "Delete Pattern (F8)": "Видалити еталон (F8)",
    "Move Pattern Up (Alt+Up)": "Перемістити еталон вище (Alt+Вгору)",
    "Move Pattern Down (Alt+Down)": "Перемістити еталон нижче (Alt+Вниз)",
  },

  "ru-irenproject-input-question-pattern-editor": {
    "Correctness": "Ступінь коректності",
    "Options": "Параметри",
  },

  "ru-irenproject-input-string-dialog": {
    "Cancel": "Відміна",
  },

  "ru-irenproject-login-dialog": {
    //TR "Server:": "Сервер:",
    //TR "Password:": "Пароль:",
    //TR "Log In": "Войти",
  },

  "ru-irenproject-mark-dialog": {
    "Lower bound:": "Нижня межа:",
    "Grade:": "Оцінка:",
    "Add": "Додати",
    "Update": "Змінити",
    "Delete": "Видалити",
    "Cancel": "Відміна",
  },

  "ru-irenproject-match-question-editor": {
    "Add Pair (F5)": "Додати пару елементів (F5)",
    "Add Unpaired Item (F6)": "Додати зайвий елемент (F6)",
    "Delete Pair or Item (F8)": "Видалити один чи пару елементів (F8)",
    "Move Pair or Item Up (Alt+Up)": "Перемістити один чи пару елементів вище (Alt+Вгору)",
    "Move Pair or Item Down (Alt+Down)": "Перемістити один чи пару елементів нижче (Alt+Вниз)",
    "Options": "Параметри",
  },

  "ru-irenproject-match-question-options-dialog": {
    "Pairs": "Пари",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "Unpaired Items": "Зайві елементи",
    "Cancel": "Відміна",
  },

  "ru-irenproject-modifier-list-editor": {
    "SECTION_MODIFIERS": (String sectionName) => 'Модифікатори розділу "$sectionName"',
    "Add Modifier": "Додати модифікатор",
    "Delete Modifier (Del)": "Видалити модифікатор (Del)",
    "Move Modifier Up (Alt+Up)": "Перемістити модифікатор вище (Alt+Вгору)",
    "Move Modifier Down (Alt+Down)": "Перемістити модифікатор нижче (Alt+Вниз)",
    "Question Modifiers": "Модифікатори питання",
  },

  "ru-irenproject-modifier-screen": {
    "Close": "Закрити",
  },

  "ru-irenproject-options-dialog": {
    //TR "Student Registration": "Регистрация тестируемых",
    //TR "Open registration": "Свободная регистрация",
    //TR "Only listed users can log in": "Разрешить вход только пользователям из списка",
    //TR "Use external LDAP server": "Использовать внешний LDAP-сервер",
    //TR "Available only for the standalone Iren server (the `iren-system-service` package for Linux).\n\n"
    //TR "LDAP settings are specified in the `/etc/iren.conf` file.":
    //TR   "Доступно только для отдельно установленного сервера Айрен (пакет `iren-system-service` для Linux).\n\n"
    //TR   "Настройки LDAP задаются в файле `/etc/iren.conf`.",
    //TR "Applies to newly assigned works": "Применяется при назначении новых работ",
    //TR "Save": "Сохранить",
    //TR "Cancel": "Отмена",
  },

  "ru-irenproject-order-question-editor": {
    "Add Item (F5)": "Додати елемент (F5)",
    "Delete Item (F8)": "Видалити елемент (F8)",
    "Move Item Up (Alt+Up)": "Перемістити елемент вище (Alt+Вгору)",
    "Move Item Down (Alt+Down)": "Перемістити елемент нижче (Alt+Вниз)",
    "Options": "Параметри",
    "Correct Sequence": "Правильна послідовність",
    "Wrong Items": "Зайві елементи",
  },

  "ru-irenproject-order-question-options-dialog": {
    "Sequence Items": "Елементи послідовності",
    "Offer all": "Запропонувати все",
    "Randomly select": "Обрати випадково",
    "Wrong Items": "Зайві елементи",
    "Cancel": "Відміна",
  },

  "ru-irenproject-pad-editor": {
    "Insert Picture from File...": "Вставити рисунок з файлу...",
    "Pictures": "Рисунки",
    "All Files": "Усі файли",
    "Unknown image type.": "Невідомий тип зображення.",
    "Save Picture to File...": "Зберегти рисунок у файл...",
    //TR "Insert Formula...": "Вставить формулу...",
    //TR "Edit Formula...": "Изменить формулу...",
  },

  "ru-irenproject-profile-editor": {
    "Profile:": "Профіль:",
    "Question Selection": "Вибір питань",
    "All": "Усі",
    "Randomly select": "По",
    "from each section": "з кожного розділу",
    //TR "Detailed section setup": "Подробная настройка разделов",
    //TR "Example values: 50%, 3, 0": "Примеры значений: 50%, 3, 0",
    "With labels": "З позначками",
    "Shuffle questions": "Перемішувати питання",
    "Time Limit": "Обмеження часу",
    "None": "Немає",
    "minutes": "хв.",
    "Test Process": "Хід тестування",
    "Allow editing answers": "Дозволити виправлення відповідей",
    "Instantly show if the answer is correct": "Повідомляти про правильність відповідей",
    "Show current result in percent": "Показувати поточний результат у відсотках",
    "Display": "Вигляд екрану студента",
    "Allow browsing through questions": "Дозволити огляд питань",
    "Reflect question weights on diagram": "Будувати діаграму з врахуванням ваги питань",
    "Results": "Результати",
    "Show results after testing": "Показати результати після закінчення тестування",
    "Result in percent": "Підсумок у відсотках",
    "Grade": "Оцінка",
    "Points earned": "Сума набраних балів",
    "Show question details": "Показати подробиці за питаннями",
    "Outcome (right/wrong)": "Правильність відповіді студента",
    "Correct answer": "Правильна відповідь",
    "Question weight": "Вага питання",
    "Show section details": "Показати подробиці за розділами",
    "Number of questions": "Кількість питань",
    "Question list": "Перелік питань",
    "Grading Scale": "Шкала оцінок",
    "Lower Bound, %": "Нижня межа, %",
    "Add...": "Додати...",
    "Modifiers": "Модифікатори",
  },

  "ru-irenproject-profile-list-editor": {
    "Profiles": "Профілі",
    "untitled": "безіменний",
    "Add Profile (F2)": "Додати профіль (F2)",
    "Delete Profile (Del)": "Видалити профіль (Del)",
    "Move Profile Up (Alt+Up)": "Перемістити профіль вище (Alt+Вгору)",
    "Move Profile Down (Alt+Down)": "Перемістити профіль нижче (Alt+Вниз)",
    //TR "Add": "Добавить",
    //TR "Delete": "Удалить",
    //TR "Move Up": "Переместить выше",
    //TR "Move Down": "Переместить ниже",
  },

  "ru-irenproject-profile-screen": {
    "Close": "Закрити",
  },

  "ru-irenproject-question-list-editor": {
    "ADD_QUESTION": (String title) => "Додати $title (F2)",
    "USE_TITLE_CASE": "false",
    "Delete Question (Del)": "Видалити питання (Del)",
    "Set Question Weight (F3)": "Задати вагу питання (F3)",
    "Question Modifiers (F4)": "Модифікатори питання (F4)",
    "Forbid Use": "Заборонити використання",
    "Move Question Up (Alt+Up)": "Перемістити питання вище (Alt+Вгору)",
    "Move Question Down (Alt+Down)": "Перемістити питання нижче (Alt+Вниз)",
    "Questions": "Питання",
    "Text": "Текст",
    "Weight": "Вага",
    "Question weight:": "Вага питання:",
    "Profiles": "Профілі",
    //TR "Add": "Добавить",
    //TR "Cut": "Вырезать",
    //TR "Copy": "Скопировать",
    //TR "Paste": "Вставить",
    //TR "Delete": "Удалить",
    //TR "Select All": "Выделить все",
    //TR "Set Weight...": "Задать вес...",
    //TR "Modifiers": "Модификаторы",
    //TR "Allow Use": "Разрешить использование",
    //TR "Move Up": "Переместить выше",
    //TR "Move Down": "Переместить ниже",
  },

  "ru-irenproject-question-viewer-dashboard": {
    "Answer Correctness": "Правильність відповіді",
    "Show Correct Answer": "Показати правильну відповідь",
    "Return Question to Initial State": "Повернути питання до вихідного стану",
    "Offer Question Anew (F9)": "Запропонувати питання заново (F9)",
  },

  "ru-irenproject-regexp-pattern-options-dialog": {
    "Space removal from the examinee's answer before matching":
      "Видалення пробілів з відповіді студента перед співставленням",
    "Remove leading and trailing spaces, leave one space between words":
      "Видалити початкові і кінцеві пробіли, залишити по одному пробілу між словами",
    "Remove all spaces": "Видалити усі пробіли",
    "Keep spaces": "Зберегти пробіли",
    "Ignore case": "Ігнорувати реєстр літер",
    "Cancel": "Відміна",
  },

  "ru-irenproject-section-tree-editor": {
    "DELETE_SECTION": (String name) => 'Видалити розділ "$name"?',
    "Section name:": "Назва розділу:",
    "Delete": "Видалити",
    "Cancel": "Відміна",
    "Sections": "Розділи",
    "Add Section (Shift+F2)": "Додати розділ (Shift+F2)",
    "Delete Section (Del)": "Видалити розділ (Del)",
    "Rename Section (Shift+F3)": "Перейменувати розділ (Shift+F3)",
    "Section Modifiers (Shift+F4)": "Модифікатори розділу (Shift+F4)",
    "Move Section Up (Alt+Up)": "Перемістити розділ вище (Alt+Вгору)",
    "Move Section Down (Alt+Down)": "Перемістити розділ нижче (Alt+Вниз)",
    //TR "Add...": "Добавить...",
    //TR "Paste": "Вставить",
    //TR "Rename...": "Переименовать...",
    //TR "Modifiers": "Модификаторы",
    //TR "Move Up": "Переместить выше",
    //TR "Move Down": "Переместить ниже",
  },

  "ru-irenproject-select-question-choice-editor": {
    "Correct choice marker": "Індикатор правильної відповіді",
    'This is the "No Correct Answers" choice': 'Варіант "Немає правильних відповідей"',
    "This choice is pinned (won't move when shuffling the choices)":
      "Положення цього варіанту відповіді фіксоване (не змінюватиметься при перемішуванні відповідей)",
  },

  "ru-irenproject-select-question-editor": {
    "None of the above are correct.": "Немає правильного варіанту серед запропонованих.",
    "Add Choice (F5)": "Додати варіант відповіді (F5)",
    "Delete Choice (F8)": "Видалити варіант відповіді (F8)",
    "Move Choice Up (Alt+Up)": "Перемістити варіант відповіді вище (Alt+Вгору)",
    "Move Choice Down (Alt+Down)": "Перемістити варіант відповіді нижче (Alt+Вниз)",
    'Add the "No Correct Answers" Choice (F6)': 'Додати варіант "Немає правильних відповідей" (F6)',
    "Pin Choice": "Зафіксувати положення варіанту відповіді",
  },

  "ru-irenproject-set-evaluation-model-modifier-editor": {
    "Strict scoring (right/wrong)": "Жорстке оцінювання (правильно/неправильно)",
    "Lax scoring (accept partially correct answers)": "М'яке оцінювання (врахування ступеня правильності відповіді)",
    //TR "Apply only to questions of chosen types": "Применять только к вопросам указанных типов",
  },

  "ru-irenproject-supervisor-screen": {
    "CANNOT_LOAD_TEST": (String fileName) => 'Помилка під час завантаження теста з файлу "$fileName".',
    //TR "Assign": "Назначить",
    //TR "Work": "Работа",
    //TR "Open": "Открыть",
    "Assign...": "Призначити...",
    //TR "[new]": "[новый]",
    //TR "Tools": "Инструменты",
    //TR "Users...": "Пользователи...",
    //TR "Download Archive...": "Загрузить архив...",
    //TR "Archives": "Архивы",
    //TR "There is no archive data on the server.": "На сервере нет архивных данных.",
    //TR "Cannot download archive.": "Ошибка при загрузке архива.",
    //TR "Options...": "Настройка...",
    "Server address:": "Адреса сервера:",
    //TR "About": "О программе",
    //TR "Assigned": "Назначена",
  },

  "ru-irenproject-test-screen": {
    "Edit": "Розробка",
    "Edit Question (Ctrl+1)": "Редагування питання (Ctrl+1)",
    "View": "Перегляд",
    "View Question (Ctrl+2)": "Перегляд питання (Ctrl+2)",
  },

  "ru-irenproject-text-pattern-options-dialog": {
    "* matches any number of characters (including none at all), ? matches any single character":
      "Вважати * і ? в еталоні спеціальними символами, що дозволяють враховувати різні варіанти"
      " написання правильної відповіді.\n\n"
      "* означає, що в даному місці слова може знаходитись будь-яка кількість довільних символів"
      " (в тому числі жодного).\n\n"
      "? замінює собою один довільний символ.\n\n"
      'Наприклад, еталону "м*н* вар*" відповідають відповіді "малинове варення" і "мінімальна варіація",'
      ' еталону "гра?іт" — "граніт" і "графіт".',
    "Interpret * and ? as wildcards": "Вважати * і ? символами підстановки",
    'Allow both "," and "." for a decimal mark, ignore leading and trailing (in fractional part) zeros.\n\n'
    'For example, "9,03" = "9.03" = "009,030".':
      "Порівнювати числові відповіді з еталоном за їхнім значенням, ігноруючи відмінності у формі запису.\n\n"
      'Наприклад, "9,03" = "9.03" = "009,030".',
    "Recognize numbers": "Розпізнавати числові відповіді",
    "Tolerance:": "Допустима похибка:",
    "Compare word-by-word": "Вважати пробіли розділювачами слів",
    "Take spaces into account": "Враховувати пробіли",
    "Don't distinguish between upper and lower case letters":
      "Не розрізняти прописні і рядкові букви при порівнянні відповіді з еталоном",
    "Ignore case": "Ігнорувати реєстр літер",
    "Cancel": "Відміна",
  },

  "ru-irenproject-users-dialog": {
    //TR "Group 1:\nUser 1\nUser 2\nUser 3\n\nGroup 2:\nUser 4\nUser 5":
    //TR   "Группа 1:\nПользователь 1\nПользователь 2\nПользователь 3\n\nГруппа 2:\nПользователь 4\nПользователь 5",
    //TR "Save": "Сохранить",
    //TR "Cancel": "Отмена",
    //TR "Error on save: the list has been changed by another user.":
    //TR   "Ошибка при сохранении: список изменен другим пользователем.",
    //TR "Group name is not specified (must end with ':').":
    //TR   "Не указано название группы (должно оканчиваться знаком ':').",
    //TR "Incorrect group name.": "Недопустимое название группы.",
    //TR "Incorrect user name.": "Недопустимое имя пользователя.",
    //TR "The user is already present in the group.": "Такой пользователь уже есть в группе.",
  },

  "ru-irenproject-watch-screen": {
    //TR "Close (Ctrl+F4)": "Закрыть (Ctrl+F4)",
    //TR "Send to Archive": "Отправить в архив",
    //TR "Work": "Работа",
    //TR "Send to Archive...": "Отправить в архив...",
    //TR "The specified file is not an archive.": "Указанный файл не является архивом.",
    //TR "A newer version of the program is required for working with the specified archive file.":
    //TR   "Для работы с указанным файлом архива нужна новая версия программы.",
    //TR "Cannot send to archive.": "Ошибка при отправке в архив.",
    "Delete": "Видалити",
    "The results of all students will be deleted.": "Результати усіх студентів буде видалено.",
    "Cancel": "Відміна",
    //TR "Save Table...": "Сохранить таблицу...",
    "TSV Files": "Файли TSV",
    //TR "Session": "Сеанс",
    //TR "Show Details": "Показать подробности",
    //TR "Ban": "Заблокировать",
    //TR "Unban": "Разблокировать",
    //TR "Resume": "Возобновить",
    //TR "Change Time Limit...": "Изменить ограничение времени...",
    //TR "Disconnect and Allow Logging in Again": "Отключить и разрешить повторный вход",
    //TR "Sessions:": "Сеансов:",
    "Student": "Студент",
    //TR "Group": "Группа",
    "Result, %": "Результат, %",
    "Grade": "Оцінка",
    "Time": "Час",
    //TR "Session Start": "Начало сеанса",
    //TR "IP Address": "IP-адрес",
  },

  "ru-irenproject-watched-session-panel": {
    "Student's Answer": "Відповідь студента",
    "Correct Answer": "Правильна відповідь",
    "Result:": "Оцінка відповіді:",
    "Question weight:": "Вага відповіді:",
    "Points earned:": "Набрані бали:",
    "Previous": "Назад",
    "Next": "Вперед",
    "Close": "Закрити",
  }
};
