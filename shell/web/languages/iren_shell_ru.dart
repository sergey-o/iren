/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> ru = {
  "classify_question_editor": {
    "Classification Question": "Вопрос на классификацию",
    "Classify the items.": "Провести классификацию.",
  },

  "input_question_editor": {
    "Text Input Question": "Вопрос с вводом ответа",
    "Text Pattern": "Текстовый эталон",
    "Add Text Pattern": "Добавить текстовый эталон",
    "Regular Expression": "Регулярное выражение",
    "Add Regular Expression": "Добавить регулярное выражение",
  },

  "iren_master": {
    "DIFFERENT_VERSION_REQUIRED":
      (String version) => "Для работы с этим сервером нужна версия $version программы.",
    "WINDOW_TITLE": (String serverAddress) => "$serverAddress — Айрен",
    "Iren": "Айрен",
    "The password is incorrect.": "Неправильный пароль.",
    "Logging in is temporarily disabled due to many failed attempts.":
      "Вход в систему временно запрещен из-за большого количества неудачных попыток.",
    "Connect": "Подключиться",
    "Cancel": "Отмена",
    "You are connecting to this server for the first time.": "Вы впервые подключаетесь к этому серверу.",
    "Server address:": "Адрес сервера:",
    "Server identifier:": "Идентификатор сервера:",
    "For security reasons, it is recommended that you verify the identifier"
    ' before clicking "Connect" by contacting the server administrator.':
      'В целях безопасности рекомендуется сверить идентификатор перед нажатием кнопки "Подключиться",'
      " обратившись к администратору сервера.",
    "THE IDENTIFIER OF THIS SERVER HAS CHANGED!": "ИДЕНТИФИКАТОР ЭТОГО СЕРВЕРА ИЗМЕНИЛСЯ!",
    "A newer version of the program is required for working with this server.":
      "Для работы с этим сервером нужна новая версия программы.",
    "Go to Download Page": "Перейти на страницу загрузки",
  },

  "iren_utils": {
    "UNKNOWN_FILE_VERSION":
      (String fileName) => 'Для работы с файлом "$fileName" нужна новая версия программы.',
    "Iren": "Айрен",
    "Close": "Закрыть",
    "Untitled": "Безымянный",
    "Tests": "Тесты",
    "All Files": "Все файлы",
    "untitled": "безымянный",
  },

  "match_question_editor": {
    "Matching Question": "Вопрос на соответствие",
    "Match the items.": "Установить соответствие.",
  },

  "modifier_screen": {
    "Allow Multiple Selection for Questions with a Single Correct Answer":
      "Запрет подсказки о единственном верном ответе",
    'Add the "No Correct Answers" Choice': 'Добавление варианта "Верных ответов нет"',
    'Set the "No Correct Answers" Choice Text': 'Изменение текста варианта "Верных ответов нет"',
    "Shuffle Choices": "Перемешивание вариантов ответов",
    "Set Scoring Mode": "Настройка модели оценивания",
    "Set Question Weight": "Изменение веса вопроса",
    "Script": "Сценарий",
  },

  "order_question_editor": {
    "Ordering Question": "Вопрос на упорядочение",
    "Order the items.": "Установить верную последовательность.",
  },

  "select_question_editor": {
    "Multiple Choice Question": "Вопрос с выбором ответа",
  },

  "ru-irenproject-about-dialog": {
    "Iren": "Айрен",
    "Version": "Версия",
    "© 2012–2020 Sergey Ostanin": "© 2012–2020 Сергей Останин",
    "License...": "Лицензия...",
    "Libraries...": "Библиотеки...",
    "Translators...": "Переводчики...",
    "Close": "Закрыть",
  },

  "ru-irenproject-archive-dialog": {
    "Archive file:": "Файл архива:",
    "Archive on the server": "Архив на сервере",
    "Choose...": "Выбрать...",
    "If the file exists, new data will be appended to it":
      "Если файл существует, новые данные будут добавлены к уже имеющимся",
    "Send to Archive": "Отправить в архив",
    "Cancel": "Отмена",
    "Archives": "Архивы",
  },

  "ru-irenproject-archive-session-screen": {
    "Student's Answer": "Ответ тестируемого",
    "Correct Answer": "Верный ответ",
    "Result:": "Оценка ответа:",
    "Question weight:": "Вес вопроса:",
    "Points earned:": "Набранные баллы:",
    "Previous": "Назад",
    "Next": "Вперед",
    "Close": "Закрыть",
  },

  "ru-irenproject-archive-work-screen": {
    "Close": "Закрыть",
    "Student": "Тестируемый",
    "Group": "Группа",
    "Result, %": "Результат, %",
    "Grade": "Оценка",
    "Session Start": "Начало сеанса",
    "IP Address": "IP-адрес",
    "Save Table...": "Сохранить таблицу...",
    "TSV Files": "Файлы TSV",
  },

  "ru-irenproject-change-time-limit-dialog": {
    "New limit:": "Новое ограничение:",
    "minutes": "мин.",
    "Examples:": "Примеры:",
    "Set limit to 5 minutes": "Установить ограничение в 5 минут",
    "Increase time by 5 minutes": "Увеличить время на 5 минут",
    "Reduce time by 5 minutes": "Сократить время на 5 минут",
    "Remove time limit": "Убрать ограничение времени",
    "Set": "Установить",
    "Cancel": "Отмена",
  },

  "ru-irenproject-classify-question-editor": {
    "Add Item to Selected Category (F5)": "Добавить элемент в выбранную категорию (F5)",
    "Add Category (F6)": "Добавить категорию (F6)",
    "Delete Item or Category (F8)": "Удалить элемент или категорию (F8)",
    "Move Item or Category Up (Alt+Up)": "Переместить элемент или категорию выше (Alt+Вверх)",
    "Move Item or Category Down (Alt+Down)": "Переместить элемент или категорию ниже (Alt+Вниз)",
    "Options": "Параметры",
  },

  "ru-irenproject-classify-question-options-dialog": {
    "Items": "Элементы",
    "Offer all": "Предложить все",
    "Randomly select": "Случайно выбрать",
    "From each category, select at least": "Из каждой категории не менее",
    "Cancel": "Отмена",
  },

  "ru-irenproject-connection-lost-dialog": {
    "No connection to the server": "Нет соединения с сервером",
    "Reconnect": "Подключиться",
  },

  "ru-irenproject-create-work-dialog": {
    "Test:": "Тест:",
    "Assign": "Назначить",
    "Cancel": "Отмена",
  },

  "ru-irenproject-credits-dialog": {
    "Close": "Закрыть",
  },

  "ru-irenproject-editor-main-screen": {
    "WINDOW_TITLE": (String fileTitle) => "$fileTitle — Айрен",
    "Iren": "Айрен",
  },

  "ru-irenproject-file-manager": {
    "CANNOT_OPEN_FILE": (String fileName) => 'Ошибка при открытии файла "$fileName".',
    "CANNOT_SAVE_FILE": (String fileName) => 'Ошибка при сохранении файла "$fileName".',
    "SAVE_CHANGES": (String fileName) => 'Сохранить изменения в "$fileName"?',
    "New (Ctrl+N)": "Cоздать (Ctrl+N)",
    "Open (Ctrl+O)": "Открыть (Ctrl+O)",
    "Save (Ctrl+S)": "Сохранить (Ctrl+S)",
    "Create Stand-Alone (.html) Test": "Создать автономный (.html) тест",
    "About": "О программе",
    "Remember Current State and Exit (Alt+Shift+Q)": "Запомнить состояние и выйти (Alt+Shift+Q)",
    "Close File (Ctrl+F4)": "Закрыть файл (Ctrl+F4)",
    "Save": "Сохранить",
    "Don't Save": "Не сохранять",
    "Cancel": "Отмена",
    "Tests and Archives": "Тесты и архивы",
    "All Files": "Все файлы",
    "HTML Files": "Файлы HTML",
    "offline": "офлайн",
    "Stand-alone tests are intended for self-assessment purposes and don't include security measures"
    " present in online mode. In particular, by using tools for data unpacking and conversion,"
    " a stand-alone HTML test can be turned into a source form and opened in the editor."
    " Implementing a reliable protection for such offline files is technically impossible.\n\n"
    "For carrying out formal assessments, and generally in all cases where the confidentiality"
    " of the source test files must be ensured, the online testing mode (available via the \"Iren Server\""
    " shortcut) should be used.":
      "Автономные тесты предназначены для самопроверки и не включают защитных мер, предусмотренных"
      " при тестировании в сетевом режиме. В частности, с помощью инструментов для распаковки"
      " и конвертирования данных автономный html-тест может быть преобразован в исходный вид и открыт"
      " в редакторе. Реализовать надежную защиту для таких файлов, работающих без использования сервера,"
      " технически невозможно.\n\n"
      "Для проведения контрольных мероприятий и в целом во всех случаях, когда важна конфиденциальность"
      " исходных файлов с тестами, следует использовать сетевой режим тестирования (ярлык \"Сервер Айрен\").",
    "Don't show again": "Больше не показывать",
    "Create Stand-Alone Test": "Создать автономный тест",
  },

  "ru-irenproject-formula-dialog": {
    "Example": "Пример",
    "Help": "Справка",
    "Close": "Закрыть",
  },

  "ru-irenproject-input-question-editor": {
    "Delete Pattern (F8)": "Удалить эталон (F8)",
    "Move Pattern Up (Alt+Up)": "Переместить эталон выше (Alt+Вверх)",
    "Move Pattern Down (Alt+Down)": "Переместить эталон ниже (Alt+Вниз)",
  },

  "ru-irenproject-input-question-pattern-editor": {
    "Correctness": "Степень корректности",
    "Options": "Настройка",
  },

  "ru-irenproject-input-string-dialog": {
    "Cancel": "Отмена",
  },

  "ru-irenproject-login-dialog": {
    "Server:": "Сервер:",
    "Password:": "Пароль:",
    "Log In": "Войти",
  },

  "ru-irenproject-mark-dialog": {
    "Lower bound:": "Нижняя граница:",
    "Grade:": "Оценка:",
    "Add": "Добавить",
    "Update": "Изменить",
    "Delete": "Удалить",
    "Cancel": "Отмена",
  },

  "ru-irenproject-match-question-editor": {
    "Add Pair (F5)": "Добавить пару элементов (F5)",
    "Add Unpaired Item (F6)": "Добавить лишний элемент (F6)",
    "Delete Pair or Item (F8)": "Удалить пару или элемент (F8)",
    "Move Pair or Item Up (Alt+Up)": "Переместить пару или элемент выше (Alt+Вверх)",
    "Move Pair or Item Down (Alt+Down)": "Переместить пару или элемент ниже (Alt+Вниз)",
    "Options": "Параметры",
  },

  "ru-irenproject-match-question-options-dialog": {
    "Pairs": "Пары",
    "Offer all": "Предложить все",
    "Randomly select": "Случайно выбрать",
    "Unpaired Items": "Лишние элементы",
    "Cancel": "Отмена",
  },

  "ru-irenproject-modifier-list-editor": {
    "SECTION_MODIFIERS": (String sectionName) => 'Модификаторы раздела "$sectionName"',
    "Add Modifier": "Добавить модификатор",
    "Delete Modifier (Del)": "Удалить модификатор (Del)",
    "Move Modifier Up (Alt+Up)": "Переместить модификатор выше (Alt+Вверх)",
    "Move Modifier Down (Alt+Down)": "Переместить модификатор ниже (Alt+Вниз)",
    "Question Modifiers": "Модификаторы вопроса",
  },

  "ru-irenproject-modifier-screen": {
    "Close": "Закрыть",
  },

  "ru-irenproject-options-dialog": {
    "Student Registration": "Регистрация тестируемых",
    "Open registration": "Свободная регистрация",
    "Only listed users can log in": "Разрешить вход только пользователям из списка",
    "Use external LDAP server": "Использовать внешний LDAP-сервер",
    "Available only for the standalone Iren server (the `iren-system-service` package for Linux).\n\n"
    "LDAP settings are specified in the `/etc/iren.conf` file.":
      "Доступно только для отдельно установленного сервера Айрен (пакет `iren-system-service` для Linux).\n\n"
      "Настройки LDAP задаются в файле `/etc/iren.conf`.",
    "Applies to newly assigned works": "Применяется при назначении новых работ",
    "Save": "Сохранить",
    "Cancel": "Отмена",
  },

  "ru-irenproject-order-question-editor": {
    "Add Item (F5)": "Добавить элемент (F5)",
    "Delete Item (F8)": "Удалить элемент (F8)",
    "Move Item Up (Alt+Up)": "Переместить элемент выше (Alt+Вверх)",
    "Move Item Down (Alt+Down)": "Переместить элемент ниже (Alt+Вниз)",
    "Options": "Параметры",
    "Correct Sequence": "Верная последовательность",
    "Wrong Items": "Лишние элементы",
  },

  "ru-irenproject-order-question-options-dialog": {
    "Sequence Items": "Элементы последовательности",
    "Offer all": "Предложить все",
    "Randomly select": "Случайно выбрать",
    "Wrong Items": "Лишние элементы",
    "Cancel": "Отмена",
  },

  "ru-irenproject-pad-editor": {
    "Insert Picture from File...": "Вставить рисунок из файла...",
    "Pictures": "Рисунки",
    "All Files": "Все файлы",
    "Unknown image type.": "Неизвестный тип изображения.",
    "Save Picture to File...": "Сохранить рисунок в файл...",
    "Insert Formula...": "Вставить формулу...",
    "Edit Formula...": "Изменить формулу...",
  },

  "ru-irenproject-profile-editor": {
    "Profile:": "Профиль:",
    "Question Selection": "Выбор вопросов",
    "All": "Все",
    "Randomly select": "По",
    "from each section": "из каждого раздела",
    "Detailed section setup": "Подробная настройка разделов",
    "Example values: 50%, 3, 0": "Примеры значений: 50%, 3, 0",
    "With labels": "С метками",
    "Shuffle questions": "Перемешивать вопросы",
    "Time Limit": "Ограничение времени",
    "None": "Нет",
    "minutes": "мин.",
    "Test Process": "Ход тестирования",
    "Allow editing answers": "Разрешить исправление ответов",
    "Instantly show if the answer is correct": "Сообщать о правильности ответов",
    "Show current result in percent": "Показывать текущий результат в процентах",
    "Display": "Вид экрана тестируемого",
    "Allow browsing through questions": "Разрешить обзор вопросов",
    "Reflect question weights on diagram": "Строить диаграмму с учетом весов вопросов",
    "Results": "Результаты",
    "Show results after testing": "Показать результаты по окончании тестирования",
    "Result in percent": "Итог в процентах",
    "Grade": "Оценка",
    "Points earned": "Сумма набранных баллов",
    "Show question details": "Показать подробности по вопросам",
    "Outcome (right/wrong)": "Правильность ответа тестируемого",
    "Correct answer": "Верный ответ",
    "Question weight": "Вес вопроса",
    "Show section details": "Показать подробности по разделам",
    "Number of questions": "Количество вопросов",
    "Question list": "Список вопросов",
    "Grading Scale": "Шкала оценок",
    "Lower Bound, %": "Нижняя граница, %",
    "Add...": "Добавить...",
    "Modifiers": "Модификаторы",
  },

  "ru-irenproject-profile-list-editor": {
    "Profiles": "Профили",
    "untitled": "безымянный",
    "Add Profile (F2)": "Добавить профиль (F2)",
    "Delete Profile (Del)": "Удалить профиль (Del)",
    "Move Profile Up (Alt+Up)": "Переместить профиль выше (Alt+Вверх)",
    "Move Profile Down (Alt+Down)": "Переместить профиль ниже (Alt+Вниз)",
    "Add": "Добавить",
    "Delete": "Удалить",
    "Move Up": "Переместить выше",
    "Move Down": "Переместить ниже",
  },

  "ru-irenproject-profile-screen": {
    "Close": "Закрыть",
  },

  "ru-irenproject-question-list-editor": {
    "ADD_QUESTION": (String title) => "Добавить $title (F2)",
    "USE_TITLE_CASE": "false",
    "Delete Question (Del)": "Удалить вопрос (Del)",
    "Set Question Weight (F3)": "Задать вес вопроса (F3)",
    "Question Modifiers (F4)": "Модификаторы вопроса (F4)",
    "Forbid Use": "Запретить использование",
    "Move Question Up (Alt+Up)": "Переместить вопрос выше (Alt+Вверх)",
    "Move Question Down (Alt+Down)": "Переместить вопрос ниже (Alt+Вниз)",
    "Questions": "Вопросы",
    "Text": "Текст",
    "Weight": "Вес",
    "Question weight:": "Вес вопроса:",
    "Profiles": "Профили",
    "Add": "Добавить",
    "Cut": "Вырезать",
    "Copy": "Скопировать",
    "Paste": "Вставить",
    "Delete": "Удалить",
    "Select All": "Выделить все",
    "Set Weight...": "Задать вес...",
    "Modifiers": "Модификаторы",
    "Allow Use": "Разрешить использование",
    "Move Up": "Переместить выше",
    "Move Down": "Переместить ниже",
  },

  "ru-irenproject-question-viewer-dashboard": {
    "Answer Correctness": "Правильность ответа",
    "Show Correct Answer": "Показать верный ответ",
    "Return Question to Initial State": "Вернуть вопрос в исходное состояние",
    "Offer Question Anew (F9)": "Предложить вопрос заново (F9)",
  },

  "ru-irenproject-regexp-pattern-options-dialog": {
    "Space removal from the examinee's answer before matching":
      "Удаление пробелов из ответа тестируемого перед сопоставлением",
    "Remove leading and trailing spaces, leave one space between words":
      "Удалить начальные и конечные пробелы, оставить по одному пробелу между словами",
    "Remove all spaces": "Удалить все пробелы",
    "Keep spaces": "Сохранить пробелы",
    "Ignore case": "Игнорировать регистр букв",
    "Cancel": "Отмена",
  },

  "ru-irenproject-section-tree-editor": {
    "DELETE_SECTION": (String name) => 'Удалить раздел "$name"?',
    "Section name:": "Название раздела:",
    "Delete": "Удалить",
    "Cancel": "Отмена",
    "Sections": "Разделы",
    "Add Section (Shift+F2)": "Добавить раздел (Shift+F2)",
    "Delete Section (Del)": "Удалить раздел (Del)",
    "Rename Section (Shift+F3)": "Переименовать раздел (Shift+F3)",
    "Section Modifiers (Shift+F4)": "Модификаторы раздела (Shift+F4)",
    "Move Section Up (Alt+Up)": "Переместить раздел выше (Alt+Вверх)",
    "Move Section Down (Alt+Down)": "Переместить раздел ниже (Alt+Вниз)",
    "Add...": "Добавить...",
    "Paste": "Вставить",
    "Rename...": "Переименовать...",
    "Modifiers": "Модификаторы",
    "Move Up": "Переместить выше",
    "Move Down": "Переместить ниже",
  },

  "ru-irenproject-select-question-choice-editor": {
    "Correct choice marker": "Индикатор правильного ответа",
    'This is the "No Correct Answers" choice': 'Вариант "Верных ответов нет"',
    "This choice is pinned (won't move when shuffling the choices)":
      "Положение этого варианта ответа фиксировано (не будет изменяться при перемешивании ответов)",
  },

  "ru-irenproject-select-question-editor": {
    "None of the above are correct.": "Среди предложенных вариантов нет верного.",
    "Add Choice (F5)": "Добавить вариант ответа (F5)",
    "Delete Choice (F8)": "Удалить вариант ответа (F8)",
    "Move Choice Up (Alt+Up)": "Переместить вариант ответа выше (Alt+Вверх)",
    "Move Choice Down (Alt+Down)": "Переместить вариант ответа ниже (Alt+Вниз)",
    'Add the "No Correct Answers" Choice (F6)': 'Добавить вариант "Верных ответов нет" (F6)',
    "Pin Choice": "Зафиксировать положение варианта ответа",
  },

  "ru-irenproject-set-evaluation-model-modifier-editor": {
    "Strict scoring (right/wrong)": "Жесткое оценивание (верно/неверно)",
    "Lax scoring (accept partially correct answers)": "Мягкое оценивание (учет степени правильности ответа)",
    "Apply only to questions of chosen types": "Применять только к вопросам указанных типов",
  },

  "ru-irenproject-supervisor-screen": {
    "CANNOT_LOAD_TEST": (String fileName) => 'Ошибка при загрузке теста из файла "$fileName".',
    "Assign": "Назначить",
    "Work": "Работа",
    "Open": "Открыть",
    "Assign...": "Назначить...",
    "[new]": "[новый]",
    "Tools": "Инструменты",
    "Users...": "Пользователи...",
    "Download Archive...": "Загрузить архив...",
    "Archives": "Архивы",
    "There is no archive data on the server.": "На сервере нет архивных данных.",
    "Cannot download archive.": "Ошибка при загрузке архива.",
    "Options...": "Настройка...",
    "Server address:": "Адрес сервера:",
    "About": "О программе",
    "Assigned": "Назначена",
  },

  "ru-irenproject-test-screen": {
    "Edit": "Разработка",
    "Edit Question (Ctrl+1)": "Редактирование вопроса (Ctrl+1)",
    "View": "Просмотр",
    "View Question (Ctrl+2)": "Просмотр вопроса (Ctrl+2)",
  },

  "ru-irenproject-text-pattern-options-dialog": {
    "* matches any number of characters (including none at all), ? matches any single character":
      "Рассматривать * и ? в эталоне как специальные символы, позволяющие учесть различные варианты"
      " написания верного ответа.\n\n"
      "* означает, что в данном месте слова может находиться любое количество произвольных символов"
      " (в том числе ни одного).\n\n"
      "? заменяет собой один произвольный символ.\n\n"
      'Например, эталону "м*ин* вар*" соответствуют ответы "малиновое варенье" и "минимальная вариация",'
      ' эталону "гра?ит" — "гранит" и "графит".',
    "Interpret * and ? as wildcards": "Считать * и ? подстановочными символами",
    'Allow both "," and "." for a decimal mark, ignore leading and trailing (in fractional part) zeros.\n\n'
    'For example, "9,03" = "9.03" = "009,030".':
      "Сравнивать числовые ответы с эталоном по их значению, игнорируя различия в форме записи.\n\n"
      'Например, "9,03" = "9.03" = "009,030".',
    "Recognize numbers": "Распознавать числовые ответы",
    "Tolerance:": "Допустимая погрешность:",
    "Compare word-by-word": "Считать пробелы разделителями слов",
    "Take spaces into account": "Учитывать пробелы",
    "Don't distinguish between upper and lower case letters":
      "Не различать прописные и строчные буквы при сравнении ответа с эталоном",
    "Ignore case": "Игнорировать регистр букв",
    "Cancel": "Отмена",
  },

  "ru-irenproject-users-dialog": {
    "Group 1:\nUser 1\nUser 2\nUser 3\n\nGroup 2:\nUser 4\nUser 5":
      "Группа 1:\nПользователь 1\nПользователь 2\nПользователь 3\n\nГруппа 2:\nПользователь 4\nПользователь 5",
    "Save": "Сохранить",
    "Cancel": "Отмена",
    "Error on save: the list has been changed by another user.":
      "Ошибка при сохранении: список изменен другим пользователем.",
    "Group name is not specified (must end with ':').": "Не указано название группы (должно оканчиваться знаком ':').",
    "Incorrect group name.": "Недопустимое название группы.",
    "Incorrect user name.": "Недопустимое имя пользователя.",
    "The user is already present in the group.": "Такой пользователь уже есть в группе.",
  },

  "ru-irenproject-watch-screen": {
    "Close (Ctrl+F4)": "Закрыть (Ctrl+F4)",
    "Send to Archive": "Отправить в архив",
    "Work": "Работа",
    "Send to Archive...": "Отправить в архив...",
    "The specified file is not an archive.": "Указанный файл не является архивом.",
    "A newer version of the program is required for working with the specified archive file.":
      "Для работы с указанным файлом архива нужна новая версия программы.",
    "Cannot send to archive.": "Ошибка при отправке в архив.",
    "Delete": "Удалить",
    "The results of all students will be deleted.": "Результаты всех тестируемых будут удалены.",
    "Cancel": "Отмена",
    "Save Table...": "Сохранить таблицу...",
    "TSV Files": "Файлы TSV",
    "Session": "Сеанс",
    "Show Details": "Показать подробности",
    "Ban": "Заблокировать",
    "Unban": "Разблокировать",
    "Resume": "Возобновить",
    "Change Time Limit...": "Изменить ограничение времени...",
    "Disconnect and Allow Logging in Again": "Отключить и разрешить повторный вход",
    "Sessions:": "Сеансов:",
    "Student": "Тестируемый",
    "Group": "Группа",
    "Result, %": "Результат, %",
    "Grade": "Оценка",
    "Time": "Время",
    "Session Start": "Начало сеанса",
    "IP Address": "IP-адрес",
  },

  "ru-irenproject-watched-session-panel": {
    "Student's Answer": "Ответ тестируемого",
    "Correct Answer": "Верный ответ",
    "Result:": "Оценка ответа:",
    "Question weight:": "Вес вопроса:",
    "Points earned:": "Набранные баллы:",
    "Previous": "Назад",
    "Next": "Вперед",
    "Close": "Закрыть",
  }
};
