/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:collection';

import 'package:meta/meta.dart';
import 'package:web_helpers/web_helpers.dart';

import 'editor_infra.dart';

class QuestionDescriptor {
  final String type;
  final String title;
  final int priority;
  final String icon;
  final String initialText;

  QuestionDescriptor({@required this.type, @required this.title, @required this.priority, @required this.icon,
      @required this.initialText});
}

class ModifierDescriptor {
  final String type;
  final String title;
  final /* nullable */List<String> questionTypes;

  ModifierDescriptor({@required this.type, @required this.title, this.questionTypes});
}

final Map<String, QuestionDescriptor> _questionDescriptorsByType = {};
final SplayTreeMap<int, QuestionDescriptor> _questionDescriptorsByPriority = SplayTreeMap();
final Map<Type, WidgetFactory> _questionEditorFactoriesByShapeClass = {};
final LinkedHashMap<String, ModifierDescriptor> _modifierDescriptorsByType = LinkedHashMap();
final Map<Type, WidgetFactory> _modifierEditorFactoriesByShapeClass = {};

void registerQuestionDescriptor(QuestionDescriptor d) {
  check(!_questionDescriptorsByType.containsKey(d.type));
  check(!_questionDescriptorsByPriority.containsKey(d.priority));

  _questionDescriptorsByType[d.type] = d;
  _questionDescriptorsByPriority[d.priority] = d;
}

/* nullable */QuestionDescriptor getQuestionDescriptor(String type) => _questionDescriptorsByType[type];

Iterable<QuestionDescriptor> get questionDescriptors => _questionDescriptorsByPriority.values;

void registerQuestionEditorFactory(Type shapeClass, WidgetFactory factory) {
  check(!_questionEditorFactoriesByShapeClass.containsKey(shapeClass));
  _questionEditorFactoriesByShapeClass[shapeClass] = factory;
}

WidgetFactory getQuestionEditorFactory(Type shapeClass) {
  WidgetFactory res = _questionEditorFactoriesByShapeClass[shapeClass];
  if (res == null) {
    throw "No question editor for '$shapeClass'.";
  }
  return res;
}

void registerModifierDescriptor(ModifierDescriptor d) {
  check(!_modifierDescriptorsByType.containsKey(d.type));
  _modifierDescriptorsByType[d.type] = d;
}

/* nullable */ModifierDescriptor getModifierDescriptor(String type) => _modifierDescriptorsByType[type];

Iterable<ModifierDescriptor> get modifierDescriptors => _modifierDescriptorsByType.values;

void registerModifierEditorFactory(Type shapeClass, WidgetFactory factory) {
  check(!_modifierEditorFactoriesByShapeClass.containsKey(shapeClass));
  _modifierEditorFactoriesByShapeClass[shapeClass] = factory;
}

WidgetFactory getModifierEditorFactory(Type shapeClass) {
  WidgetFactory res = _modifierEditorFactoriesByShapeClass[shapeClass];
  if (res == null) {
    throw "No modifier editor for '$shapeClass'.";
  }
  return res;
}
