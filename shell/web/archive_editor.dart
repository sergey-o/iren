/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/editor/archive_editor.pb.dart';

import 'correctness_bar.dart';
import 'editor_infra.dart';
import 'embedded_resources.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'selector_bar_wrapper.dart';
import 'web_definitions.dart';

class ArchiveEditor extends DocumentEditor<XArchiveEditor> {
  static const String TAG = "ru-irenproject-archive-editor";

  static final Map<Type, WidgetFactory> _screens = {
      XArchiveWorkSelectorScreen: (name, dispatcher) => ArchiveWorkSelectorScreen(name, dispatcher),
      XArchiveWorkScreen: (name, dispatcher) => ArchiveWorkScreen(name, dispatcher),
      XArchiveSessionScreen: (name, dispatcher) => ArchiveSessionScreen(name, dispatcher)};

  @override final bool modified = false;

  ArchiveEditor(String name, Dispatcher dispatcher) : super(TAG, name, dispatcher);

  @override void render() {
    super.render();
    renderScreen(shape.screen, _screens);
  }

  @override void markAsUnmodified() {}

  @override bool get exportableToHtml => false;
}

class ArchiveWorkSelectorScreen extends Widget<XArchiveWorkSelectorScreen> {
  static const String TAG = "ru-irenproject-archive-work-selector-screen";

  DivElement w_scrollable;
  TableElement w_works;

  ArchiveWorkSelectorScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_scrollable.onScroll.listen((_) => saveState(getScrollPosition(w_scrollable)));
  }

  @override void attached() {
    super.attached();
    window.animationFrame.then((_) => setScrollPosition(w_scrollable, recallState() as Point<int>));
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      TableSectionElement tbody = w_works.tBodies.first
          ..nodes.clear();
      for (var w in shape.work) {
        tbody.addRow()
            ..addCell().text = formatTimestamp(w.archivedAt)
            ..addCell().text = w.title
            ..onClick.listen(_onRowClick);
      }
    }
  }

  void _onRowClick(MouseEvent e) {
    int index = (e.currentTarget as TableRowElement).sectionRowIndex;
    performAction(XArchiveWorkSelectorScreen_OpenWork()
        ..name = shape.work[index].name);
  }
}

class ArchiveWorkScreen extends Widget<XArchiveWorkScreen> {
  static const String TAG = "ru-irenproject-archive-work-screen";

  static const String _ROW_INDEX_ATTRIBUTE = "index"; // used instead of slow TableRowElement.sectionRowIndex
  static const String _OFFSCREEN_RENDER_AMOUNT = "100px";

  static const List<_Column> _COLUMNS_WITH_TOOLTIPS = [
      _Column.USER_NAME, _Column.GROUP_NAME, _Column.MARK, _Column.ADDRESS];

  static const Map<SessionSortField, _Column> _SORTABLE = {
      SessionSortField.USER_NAME: _Column.USER_NAME,
      SessionSortField.GROUP_NAME: _Column.GROUP_NAME,
      SessionSortField.RESULT: _Column.RESULT,
      SessionSortField.OPENED_AT: _Column.OPENED_AT,
      SessionSortField.ADDRESS: _Column.ADDRESS};

  static const List<SessionSortField> _INITIALLY_SORT_ASCENDING = [
      SessionSortField.USER_NAME, SessionSortField.GROUP_NAME, SessionSortField.ADDRESS];

  SpanElement w_title;
  DivElement w_scrollable;
  TableElement w_sessions;
  ButtonElement w_saveTableButton;
  ButtonElement w_closeButton;

  XArchiveSessionOrder _order;

  IntersectionObserver _intersectionObserver;
  final Set<int> _populatedRows = {};

  ArchiveWorkScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    _intersectionObserver = IntersectionObserver(
        (entries, _) => _updatePopulatedRowsSet(entries.cast()), {
            INTERSECTION_OBSERVER_ROOT: w_scrollable,
            INTERSECTION_OBSERVER_ROOT_MARGIN: "$_OFFSCREEN_RENDER_AMOUNT 0px"});

    TableSectionElement tbody = w_sessions.tBodies.first;

    for (int count = shape.session.length, i = 0; i < count; ++i) {
      var row = TableRowElement()
          ..append(_createPlaceholderCell())
          ..dataset[_ROW_INDEX_ATTRIBUTE] = i.toString();
      _intersectionObserver.observe(row);
      tbody.append(row);
    }

    _SORTABLE.forEach((sortField, column) {
      w_sessions.tHead.rows.first.cells[column.index].querySelector(".headerText")
          ..classes.add("sortable")
          ..onClick.listen((_) => _sort(sortField));
    });

    tbody.onClick.listen(_onTbodyClick);

    w_saveTableButton.onClick.listen((_) => _saveTable());
    w_closeButton.onClick.listen((_) => _close());
    windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);

    w_scrollable.onScroll.listen((_) => saveState(getScrollPosition(w_scrollable)));
  }

  static TableCellElement _createPlaceholderCell() => TableCellElement()
      ..colSpan = _Column.values.length
      ..text = "\u00a0";

  @override void attached() {
    super.attached();
    window.animationFrame.then((_) => setScrollPosition(w_scrollable, recallState() as Point<int>));
  }

  @override void detached() {
    _intersectionObserver.disconnect();
    super.detached();
  }

  @override void render() {
    super.render();
    if (!identical(_order, scene.shapes(shape.order))) {
      _order = scene.shapes(shape.order);
      w_title.text = "${formatTimestamp(shape.archivedAt)}\u2003${shape.testName}";
      _populatedRows.forEach(_updateRow);
      _displaySortIndicator();
    }
  }

  void _updatePopulatedRowsSet(List<IntersectionObserverEntry> entries) {
    for (var e in entries) {
      int index = int.parse(e.target.dataset[_ROW_INDEX_ATTRIBUTE]);
      if (e.isIntersecting && !_populatedRows.contains(index)) {
        _populatedRows.add(index);
        _updateRow(index);
      } else if (!e.isIntersecting && _populatedRows.contains(index)) {
        _populatedRows.remove(index);
        w_sessions.tBodies.first.rows[index]
            ..nodes = [_createPlaceholderCell()]
            ..classes.clear();
      }
    }
  }

  void _updateRow(int rowIndex) {
    TableRowElement row = w_sessions.tBodies.first.rows[rowIndex];

    if (row.cells.length != _Column.values.length) {
      row.nodes.clear();
      for (var column in _Column.values) {
        var cell = TableCellElement();
        if (_COLUMNS_WITH_TOOLTIPS.contains(column)) {
          cell.onMouseEnter.listen(_onCellMouseEnter);
        }
        row.append(cell);
      }
    }

    SessionCard s = shape.session[_order.index[rowIndex]];
    row
        ..cells[_Column.USER_NAME.index].text = s.userDisplayName
        ..cells[_Column.GROUP_NAME.index].text = s.groupName
        ..cells[_Column.BAR.index].children = s.hasPercentCorrect() ? [(CorrectnessBar()
            ..percentCorrect = s.percentCorrect
            ..percentWrong = s.percentWrong).element] : []
        ..cells[_Column.RESULT.index].text = dotToUiDecimalMark(s.percentCorrect)
        ..cells[_Column.MARK.index].text = s.mark
        ..cells[_Column.OPENED_AT.index].text = s.hasOpenedAt() ? formatTimestamp(s.openedAt) : ""
        ..cells[_Column.ADDRESS.index].text = s.address
        ..classes.toggle("banned", s.banned);

    for (var column in _COLUMNS_WITH_TOOLTIPS) {
      TableCellElement cell = row.cells[column.index];
      if (cell.title != cell.text) {
        cell.title = "";
      }
    }
  }

  void _displaySortIndicator() {
    w_sessions.tHead.rows.first
        ..querySelectorAll(".sortable").classes.removeAll(["sortedUp", "sortedDown"])
        ..cells[_SORTABLE[_order.sortField].index].querySelector(".sortable")
            .classes.add(_order.sortAscending ? "sortedUp" : "sortedDown");
  }

  void _sort(SessionSortField sortField) {
    bool sortAscending = (sortField == _order.sortField) ?
        !_order.sortAscending :
        _INITIALLY_SORT_ASCENDING.contains(sortField);
    performAction(XArchiveWorkScreen_Sort()
        ..sortField = sortField
        ..sortAscending = sortAscending);
  }

  void _onCellMouseEnter(MouseEvent e) {
    TableCellElement cell = e.currentTarget as TableCellElement;
    cell.title = (cell.scrollWidth > cell.clientWidth) ? cell.text : "";
  }

  void _onTbodyClick(MouseEvent e) {
    int n = e.path.indexOf(w_sessions.tBodies.first);
    if (n > 0) {
      TableRowElement row = e.path[n - 1] as TableRowElement;
      performAction(XArchiveWorkScreen_OpenSession()
          ..index = _order.index[int.parse(row.dataset[_ROW_INDEX_ATTRIBUTE])]);
    }
  }

  Future<void> _saveTable() async {
    String fileName = showSaveFileDialog("tsv", tr("TSV Files"));
    if (fileName != null) {
      try {
        ActionResult result = await performAction(XArchiveWorkScreen_ExportToTsv()
            ..header.addAll([tr("Student"), tr("Group"), tr("Result, %"), tr("Grade"), tr("Session Start")])
            ..timeZone = userTimeZone);
        var reply = XArchiveWorkScreen_ExportToTsv_Reply.fromBuffer(result.output);

        writeViaTempFile(reply.tsv, fileName);
      } catch (e) {
        showErrorMessage(e.toString());
      }
    }
  }

  void _close() {
    performAction(XArchiveWorkScreen_Close());
  }
}

enum _Column { USER_NAME, GROUP_NAME, BAR, RESULT, MARK, OPENED_AT, ADDRESS }

class ArchiveSessionScreen extends Widget<XArchiveSessionScreen> {
  static const String TAG = "ru-irenproject-archive-session-screen";

  StyleElement w_clientStyle;
  SpanElement w_title;
  ButtonElement w_userResponseButton;
  ButtonElement w_correctResponseButton;
  DivElement w_scrollable;
  SpanElement w_result;
  SpanElement w_weight;
  SpanElement w_score;
  ButtonElement w_previousButton;
  ButtonElement w_nextButton;
  ButtonElement w_closeButton;

  SelectorBarWrapper _selectorBar;

  XArchiveQuestionViewer _questionViewer;
  /* nullable */DialogWidget _dialog;
  Page _page;

  ResizeObserver _resizeObserver;

  ArchiveSessionScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_clientStyle.text = _irenCss;

    w_title.text = "${formatTimestamp(shape.archivedAt)}\u2003${shape.testName} \u25ba ${shape.userDescription}";
    _selectorBar = SelectorBarWrapper(shape.questionDescriptor)
        ..element.id = "selectorBar"
        ..onSelectItem.listen((e) => _selectQuestion(e.itemIndex));
    findElement("selectorBarPlaceholder").replaceWith(_selectorBar.element);

    w_userResponseButton.onClick.listen((_) => _selectPage(Page.USER_RESPONSE));
    w_correctResponseButton.onClick.listen((_) => _selectPage(Page.CORRECT_RESPONSE));

    w_closeButton.onClick.listen((_) => _close());
    windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);

    w_previousButton.onClick.listen((_) => _selectQuestion(_questionViewer.questionIndex - 1));
    w_nextButton.onClick.listen((_) => _selectQuestion(_questionViewer.questionIndex + 1));

    _resizeObserver = ResizeObserver((_, __) => _layOutDialogIfShown())
        ..observe(w_scrollable);
  }

  void _layOutDialogIfShown() {
    if (element.offsetParent != null) {
      _dialog?.layOut();
    }
  }

  @override void attached() {
    super.attached();
    _selectorBar.layOut();
  }

  @override void detached() {
    _resizeObserver.disconnect();
    super.detached();
  }

  void _close() {
    performAction(XArchiveSessionScreen_Close());
  }

  @override void render() {
    super.render();

    bool pageChanged = (_page != _state().page);
    if (pageChanged) {
      _page = _state().page;
      w_userResponseButton.classes.toggle("selected", _page == Page.USER_RESPONSE);
      w_correctResponseButton.classes.toggle("selected", _page == Page.CORRECT_RESPONSE);
    }

    bool questionViewerChanged = !identical(_questionViewer, scene.shapes(shape.questionViewer));
    if (questionViewerChanged) {
      _questionViewer = scene.shapes(shape.questionViewer);
      _selectorBar.setCurrentItem(_questionViewer.questionIndex);

      w_result.text = "${dotToUiDecimalMark(_questionViewer.resultPercent)}%";
      w_weight.text = "${_questionViewer.weight}";
      w_score.text = "${dotToUiDecimalMark(_questionViewer.score)}";

      w_previousButton.disabled = (_questionViewer.questionIndex == 0);
      w_nextButton.disabled = (_questionViewer.questionIndex == shape.questionDescriptor.length - 1);
    }

    if (pageChanged || questionViewerChanged) {
      _dialog?.element?.remove();
      _dialog = null;
      if (_questionViewer.hasContent()) {
        _dialog = DialogWidget(_questionViewer.content.dialog, (_page == Page.USER_RESPONSE) ?
            _questionViewer.content.userOrEmptyResponse : _questionViewer.content.correctResponse);
        _dialog.setReadOnly(true);
        w_scrollable.append(_dialog.element);
        _dialog.layOut();
        w_scrollable.scrollTop = 0;
      }
    }
  }

  XArchiveSessionScreenState _state() => scene.shapes(shape.state);

  void _selectQuestion(int index) {
    performAction(XArchiveSessionScreen_SelectQuestion()
        ..index = index);
  }

  void _selectPage(Page page) {
    performAction(XArchiveSessionScreen_SelectPage()
        ..page = page);
  }
}

const String _IREN_CSS_LOCATION = "package:iren_client/iren.css";

@EmbeddedResource(_IREN_CSS_LOCATION)
final String _irenCss = getEmbeddedResource(_IREN_CSS_LOCATION);

void initialize() {
  registerShapeParsers([
      (b) => XArchiveEditor.fromBuffer(b),
      (b) => XArchiveWorkSelectorScreen.fromBuffer(b),
      (b) => XArchiveWorkScreen.fromBuffer(b),
      (b) => XArchiveSessionOrder.fromBuffer(b),
      (b) => XArchiveSessionScreen.fromBuffer(b),
      (b) => XArchiveQuestionViewer.fromBuffer(b),
      (b) => XArchiveSessionScreenState.fromBuffer(b)]);
}
