/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:js';
import 'dart:js_util' as js_util;
import 'dart:typed_data';

import 'package:collection/collection.dart' show ListEquality;
import 'package:convert/convert.dart' show hex;
import 'package:html5_dnd/html5_dnd.dart' as dnd;
import 'package:meta/meta.dart';
import 'package:protobuf/protobuf.dart' show ProtobufEnum;
import 'package:quiver/core.dart' show hash2;
import 'package:reflectable/reflectable.dart';
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

import 'electron.dart';
import 'web_definitions.dart';

@_webComponentReflector class WebComponent {
  static Element _createElement(String tag) {
    if (window.customElements.get(tag) == null) {
      window.customElements.define(tag, _createCustomElementClass());
    }
    return Element.tag(tag);
  }

  static dynamic _createCustomElementClass() => evalInGlobalScope(r"""
      (class extends HTMLElement {
        connectedCallback() {
          this.$webComponent.attached();
        }

        disconnectedCallback() {
          this.$webComponent.detached();
        }
      })
      """);

  final String _tag;
  final Element element;

  @protected final ShortcutHandler windowShortcutHandler = ShortcutHandler.detached();
  /* nullable */StreamSubscription<KeyboardEvent> _windowShortcutSubscription;

  /* nullable */Menu _activePopupMenu;

  WebComponent(this._tag)
      : element = _createElement(_tag) {
    js_util.setProperty(element, r"$webComponent", this);

    TemplateElement template = document.getElementById("template-$_tag") as TemplateElement;
    if (template == null) {
      throw "No template for '$_tag'.";
    }

    DocumentFragment content = document.importNode(template.content, true) as DocumentFragment;
    _translateFragment(content);

    element.attachShadow({"mode": "open"}).append(content);

    InstanceMirror instanceMirror = _webComponentReflector.reflect(this);
    instanceMirror.type.instanceMembers.forEach((name, methodMirror) {
      if (methodMirror.isSetter) {
        String id = name.substring(2, name.length - 1); // strip leading `w_` and trailing `=`
        instanceMirror.invokeSetter(name, findElement(id));
      }
    });
  }

  void _translateFragment(DocumentFragment content) {
    for (var t in _traverseNodeIterator(NodeIterator(content, NodeFilter.SHOW_TEXT))
        .where((n) => n.parent is! StyleElement)
        .cast<Text>()) {
      _translate(t.data, (newValue) => t.data = newValue);
    }

    for (var e in content.querySelectorAll("[title]")) {
      _translate(e.title, (newValue) => e.title = newValue);
    }

    for (var e in content.querySelectorAll<InputElement>("input[placeholder]")) {
      _translate(e.placeholder, (newValue) => e.placeholder = newValue);
    }

    for (var e in content.querySelectorAll<TextAreaElement>("textarea[placeholder]")) {
      _translate(e.placeholder, (newValue) => e.placeholder = newValue);
    }
  }

  static Iterable<Node> _traverseNodeIterator(NodeIterator i) sync* {
    Node node;
    while ((node = i.nextNode()) != null) {
      yield node;
    }
  }

  void _translate(String s, void Function(String newValue) setter) {
    String trimmedLeft = s.trimLeft();
    String trimmed = trimmedLeft.trimRight();
    if (trimmed.isNotEmpty) {
      int leftSpaces = s.length - trimmedLeft.length;
      setter(s.replaceRange(leftSpaces, leftSpaces + trimmed.length, tr(trimmed)));
    }
  }

  @protected Element findElement(String id) {
    Element res = element.shadowRoot.getElementById(id);
    if (res == null) {
      throw "'$id' is not defined in '$_tag'.";
    }
    return res;
  }

  @protected void attached() {
    if (!windowShortcutHandler.isEmpty) {
      _windowShortcutSubscription = window.onKeyDown.listen((e) {
        if ((element.offsetParent != null) && !modalDialogOpen) {
          windowShortcutHandler.handle(e);
        }
      });
    }
  }

  @protected void detached() {
    _windowShortcutSubscription?.cancel();
    _windowShortcutSubscription = null;

    _activePopupMenu?.closePopup();
  }

  void focusComponent() {}

  @protected String tr(String key) => trD(key) as String;

  @protected dynamic trD(String key) => translate(_tag, key);

  @protected void showPopupMenu(Menu menu, MenuPopupOptions options) {
    if (_activePopupMenu == null) {
      dnd.currentDraggableGroup?.cancelDrag();

      check(options.callback == null);
      options.callback = allowInterop(_quitPopupMenuMode);

      _activePopupMenu = menu;
      lockUi(autoDim: false);

      menu.popup(options);
    }
  }

  void _quitPopupMenuMode() {
    if (_activePopupMenu != null) {
      _activePopupMenu = null;
      unlockUi();
    }
  }

  @protected void Function(dynamic menuItem, dynamic browserWindow, dynamic event) toMenuHandler(void Function() f) =>
      allowInterop((_, __, ___) {
        _quitPopupMenuMode();
        if (element.offsetParent != null) {
          f();
        }
      });
}

class WebComponentReflector extends Reflectable {
  const WebComponentReflector() : super(
      const InstanceInvokeCapability("^w_"),
      declarationsCapability,
      subtypeQuantifyCapability);
}

class _Shortcut {
  final int keyCode;
  final KeyModifierState modifiers;

  _Shortcut(this.keyCode, this.modifiers);

  @override bool operator ==(Object other) => (other is _Shortcut)
      && (keyCode == other.keyCode) && (modifiers == other.modifiers);

  @override int get hashCode => hash2(keyCode, modifiers);
}

class ShortcutHandler {
  final Map<_Shortcut, void Function()> _registry = {};

  ShortcutHandler(EventTarget target) {
    Element.keyDownEvent.forTarget(target).listen(handle);
  }

  ShortcutHandler.detached();

  void add(int keyCode, KeyModifierState modifiers, void Function() handler) {
    var shortcut = _Shortcut(keyCode, modifiers);
    check(!_registry.containsKey(shortcut));
    _registry[shortcut] = handler;
  }

  void handle(KeyboardEvent e) {
    var handler = _registry[_Shortcut(e.keyCode, getKeyModifiers(e))];
    if (handler != null) {
      stopFurtherProcessing(e);
      dnd.currentDraggableGroup?.cancelDrag();
      handler();
    }
  }

  bool get isEmpty => _registry.isEmpty;
}

enum KeyModifierState {
  NONE, SHIFT, ALT, ALT_SHIFT, CTRL, CTRL_SHIFT, CTRL_ALT, CTRL_ALT_SHIFT
}

class ModalDialog extends WebComponent {
  DialogElement _dialogElement;

  ModalDialog(String tag) : super(tag);

  @override void attached() {
    super.attached();
    _openModalDialogs.add(_dialogElement);
  }

  @override void detached() {
    _openModalDialogs.remove(_dialogElement);
    super.detached();
  }

  @protected Future<void> open(DialogElement dialogElement, {Node dialogParent}) async {
    _dialogElement = dialogElement;

    (dialogParent ?? document.body).append(element);
    try {
      _dialogElement.showModal();
      await _dialogElement.on["close"].first;
    } finally {
      element.remove();
    }
  }
}

final DivElement _glass = DivElement()
    ..style.position = "fixed"
    ..style.top = "0"
    ..style.left = "0"
    ..style.bottom = "0"
    ..style.right = "0"
    ..onMouseDown.listen((MouseEvent e) {
      _lockedUiMouseDownEvent ??= e;
    });

int _uiLockCount = 0;
bool _uiDimmed = false;
/* nullable */Timer _uiDimTimer;
bool _globalKeyHandlerInstalled = false;

/* nullable */MouseEvent _lockedUiMouseDownEvent;
/* nullable */String lockedUiCursor;

/* nullable */Element _elementWithInputActivity;

const WebComponentReflector _webComponentReflector = WebComponentReflector();

final List<DialogElement> _openModalDialogs = [];

const ListEquality<Object> DEFAULT_LIST_EQUALITY = ListEquality();

WebComponent getWebComponent(Element e) {
  WebComponent res = js_util.getProperty(e, r"$webComponent") as WebComponent;
  check(res != null);
  return res;
}

void stopFurtherProcessing(Event event) {
  event
      ..preventDefault()
      ..stopImmediatePropagation();
}

void lockUi({bool autoDim: true}) {
  ++_uiLockCount;
  if (_uiLockCount == 1) {
    _installGlobalKeyHandler();
    if (lockedUiCursor != null) {
      _glass.style.cursor = lockedUiCursor;
    }
    (_openModalDialogs.isEmpty ? document.body : _openModalDialogs.last).append(_glass);
    if (autoDim) {
      _uiDimTimer = Timer(const Duration(milliseconds: 400), _onUiDimTimer);
    }
    _lockedUiMouseDownEvent = null;
  }
}

void unlockUi() {
  if (_uiLockCount > 0) {
    --_uiLockCount;
    if (_uiLockCount == 0) {
      _glass.remove();
      _glass.style
          ..backgroundColor = ""
          ..opacity = ""
          ..cursor = "";
      _cancelUiDimTimer();
      _uiDimmed = false;
    }
  }
}

void _onUiDimTimer() {
  _uiDimTimer = null;
  _dimUi();
}

void _dimUi() {
  _glass.style
      ..backgroundColor = "#000000"
      ..opacity = "0.4";
  _uiDimmed = true;
}

void _cancelUiDimTimer() {
  _uiDimTimer?.cancel();
  _uiDimTimer = null;
}

void dimLockedUi() {
  if (uiLocked && !_uiDimmed) {
    _cancelUiDimTimer();
    _dimUi();
  }
}

bool get uiLocked => _uiLockCount > 0;

bool get uiDimmed => _uiDimmed;

bool get doubleClickedWhileUiWasLocked => (_lockedUiMouseDownEvent != null)
    && (_lockedUiMouseDownEvent.button == 0)
    && (_lockedUiMouseDownEvent.detail == 2);

void _installGlobalKeyHandler() {
  if (!_globalKeyHandlerInstalled) {
    Element.keyDownEvent.forTarget(window, useCapture: true).listen(_globalKeyHandler);
    Element.keyPressEvent.forTarget(window, useCapture: true).listen(_globalKeyHandler);

    _globalKeyHandlerInstalled = true;
  }
}

void _globalKeyHandler(KeyboardEvent e) {
  if (uiLocked) {
    stopFurtherProcessing(e);
  }
}

KeyModifierState getKeyModifiers(KeyboardEvent e) =>
    KeyModifierState.values[(e.ctrlKey ? 4 : 0) + (e.altKey ? 2 : 0) + (e.shiftKey ? 1 : 0)];

bool get modalDialogOpen => _openModalDialogs.isNotEmpty;

void disableDragAndDrop(Element element) {
  element
      ..onDragStart.listen((e) => e.preventDefault())
      ..onDragOver.listen((e) {
        e
            ..preventDefault()
            ..dataTransfer.dropEffect = "none";
      });
}

Iterable<int> intRange(int fromInclusive, int toExclusive) sync* {
  for (int n = fromInclusive; n < toExclusive; ++n) {
    yield n;
  }
}

Uint8List castOrCopyToUint8List(List<int> list) => (list is Uint8List) ? list : Uint8List.fromList(list);

Point<int> getScrollPosition(Element e) => Point(e.scrollLeft, e.scrollTop);

void setScrollPosition(Element e, /* nullable */Point<int> p) {
  if (p != null) {
    e
        ..scrollLeft = p.x
        ..scrollTop = p.y;
  }
}

void focusInputElement(Element e) {
  // `e` cannot be conveniently declared as `InputElement` due to unusual place of that class in the hierarchy.
  // (It is a _subtype_ of `TextInputElement`, `NumberInputElement` and so on.)
  if (e is InputElement) {
    Timer.run(() {
      e
          ..focus()
          ..select();
    });
  }
}

void selectTextAreaLine(TextAreaElement textArea, int oneBasedLineNumber) {
  String s = textArea.value;
  int line = 0;
  int lineStart = 0;
  int lineEnd;

  while (true) {
    ++line;
    lineEnd = s.indexOf("\n", lineStart);
    if (lineEnd == -1) {
      lineEnd = s.length;
      break;
    }
    if (line == oneBasedLineNumber) {
      break;
    }
    lineStart = lineEnd + 1;
  }

  if (line == oneBasedLineNumber) {
    textArea.setSelectionRange(lineStart, lineEnd);
  }
}

String normalizeUri(String uri) => JsObject(context["URL"] as JsFunction, [uri])["href"] as String;

String generateTempFileName(String prefix) {
  var random = Uint8List(8);
  window.crypto.getRandomValues(random);
  return "$prefix~${hex.encode(random)}";
}

void writeToFile(Uint8List data, int fileDescriptor) {
  dynamic buffer = Buffer_from(data.buffer, data.offsetInBytes, data.lengthInBytes);
  int offset = 0;
  int remaining = data.length;
  while (remaining > 0) {
    int written = fs_writeSync(fileDescriptor, buffer, offset, remaining);
    check(written > 0);
    offset += written;
    remaining -= written;
  }
}

void writeViaTempFile(dynamic data, String fileName) {
  String tempFileName = generateTempFileName(fileName);
  try {
    fs_writeFileSync(tempFileName, data, WriteFileOptions(flag: "wx"));
    fs_renameSync(tempFileName, fileName);
  } catch (_) {
    try {
      fs_unlinkSync(tempFileName);
    } catch (_) {}
    rethrow;
  }
}

void preventOpeningLinksInNewWindowWithMiddleClick() {
  document.on["auxclick"].listen((e) => e.preventDefault());
}

// https://bugs.chromium.org/p/chromium/issues/detail?id=566343
bool buggyInputEventFor(EventTarget e) => (e is Element) && (e.offsetParent == null);

bool textlikeInputElement(Object e) => (e is InputElement) && ["text", "password", "number"].contains(e.type);

String collapseSpaces(String s) => s.trim().replaceAll(RegExp(r"\s+"), " ");

String dotToUiDecimalMark(String s) => s.replaceFirst(".", uiDecimalMark);

String anyDecimalMarkToDot(String s) => s.replaceFirst(",", ".");

T getProtobufEnumByName<T extends ProtobufEnum>(Iterable<T> values, String name) =>
    values.firstWhere((v) => v.name == name);

void normalizeNode(Node node) {
  JsObject.fromBrowserObject(node).callMethod("normalize");
}

void scrollElementIntoViewIfNeeded(Element e, bool center) {
  JsObject.fromBrowserObject(e).callMethod("scrollIntoViewIfNeeded", [center]);
}

void updateTextInput(TextInputElement element, String value, [String Function(String) oldValueNormalizer]) {
  String oldValue = (oldValueNormalizer == null) ? element.value : oldValueNormalizer(element.value);
  if (oldValue != value) {
    element.value = value;
  }
}

void updateNumberInput(NumberInputElement element, num value) {
  if (element.valueAsNumber != value) {
    element.valueAsNumber = value;
  }
}

E trackInputActivity<E extends Element>(E element) {
  return element
      ..onInput.listen(_onTrackedElementInput)
      ..onBlur.listen((_) => _elementWithInputActivity = null);
}

void _onTrackedElementInput(Event e) {
  if (!buggyInputEventFor(e.currentTarget)) {
    _elementWithInputActivity = e.currentTarget as Element;
  }
}

bool updateInactiveTextInput(TextInputElement element, String value) {
  bool res = (element != _elementWithInputActivity);
  if (res) {
    element.value = value;
  }
  return res;
}

bool updateInactiveNumberInput(NumberInputElement element, num value) {
  bool res = (element != _elementWithInputActivity);
  if (res) {
    element.valueAsNumber = value;
  }
  return res;
}

void scheduleScrollIntoView(Element e) {
  window.animationFrame.then((_) => e.scrollIntoView());
}

Future<void> get postAnimationFrame async {
  await window.animationFrame;
  await Future<void>.delayed(Duration.zero);
}

Future<void> lockUiUntilRepainted() async {
  lockUi();
  try {
    await postAnimationFrame;
  } finally {
    unlockUi();
  }
}

MenuPopupOptions getDropDownMenuPosition(Element e) => MenuPopupOptions(
    x: e.borderEdge.left.round(),
    y: e.borderEdge.bottom.round());

MenuItem createMenuSeparator() => MenuItem(MenuItemOptions(type: "separator"));

String getAbsoluteAppPath(String path) => "${app_getAppPath()}/$path";

String get userTimeZone => evalInGlobalScope("new Intl.DateTimeFormat().resolvedOptions().timeZone") as String;
