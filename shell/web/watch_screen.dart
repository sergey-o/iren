/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/watcher.pb.dart';
import 'package:iren_proto/work_controller.pb.dart';
import 'package:meta/meta.dart';
import 'package:web_helpers/web_helpers.dart';

import 'archive_dialog.dart';
import 'correctness_bar.dart';
import 'electron.dart';
import 'embedded_resources.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'web_definitions.dart';

class WatchScreen extends WebComponent {
  static const String TAG = "ru-irenproject-watch-screen";

  static const String _ROW_INDEX_ATTRIBUTE = "index"; // used instead of slow TableRowElement.sectionRowIndex
  static const String _OFFSCREEN_RENDER_AMOUNT = "100px";

  static const List<_Column> _COLUMNS_WITH_TOOLTIPS = [
      _Column.USER_NAME, _Column.GROUP_NAME, _Column.MARK, _Column.ADDRESS];

  static const Map<SessionOverviewSortField, _Column> _SORTABLE = {
      SessionOverviewSortField.USER_NAME: _Column.USER_NAME,
      SessionOverviewSortField.GROUP_NAME: _Column.GROUP_NAME,
      SessionOverviewSortField.RESULT: _Column.RESULT,
      SessionOverviewSortField.DEADLINE: _Column.TIME,
      SessionOverviewSortField.OPENED_AT: _Column.OPENED_AT,
      SessionOverviewSortField.ADDRESS: _Column.ADDRESS};

  static const List<SessionOverviewSortField> _INITIALLY_SORT_ASCENDING = [
      SessionOverviewSortField.USER_NAME, SessionOverviewSortField.GROUP_NAME, SessionOverviewSortField.ADDRESS];

  SpanElement w_workTitle;
  ButtonElement w_closeButton;
  ButtonElement w_sendToArchive;
  ButtonElement w_workMenuButton;
  ButtonElement w_sessionMenuButton;
  SpanElement w_sessionCount;
  DivElement w_sideBySide;
  DivElement w_main;
  TableElement w_table;
  TableSectionElement w_tableBody;

  final Connection _connection;
  final Int64 _watcherChannelId;
  final bool _remoteServer;
  final Int64 _workControllerChannelId;
  final String _workId;

  IntersectionObserver _intersectionObserver;
  final Map<int, /* nullable */SessionOverview> _watchedSessionOverviewsByIndex = {};

  int _selectedSessionCount = 0;

  CancelableRequestPerformer _requestPerformer;

  StreamSubscription<WatcherNotice> _noticeSubscription;

  num _referenceServerTimeMilliseconds;
  num _referenceServerTimeArrivalMilliseconds;
  Timer _timer;

  SessionOverviewOrder _order;

  /* nullable */WatchedSessionPanel _watchedSessionPanel;

  bool _closedExplicitly = false;
  bool get closedExplicitly => _closedExplicitly;

  WatchScreen(
      this._connection,
      this._watcherChannelId,
      Stream<WatcherNotice> noticeStream,
      this._remoteServer,
      this._workControllerChannelId,
      this._workId)
      : super(TAG) {
    _intersectionObserver = IntersectionObserver(
        (entries, _) => _updateWatchedSessionOverviewSet(entries.cast()), {
            INTERSECTION_OBSERVER_ROOT: w_main,
            INTERSECTION_OBSERVER_ROOT_MARGIN: "$_OFFSCREEN_RENDER_AMOUNT 0px"});

    _requestPerformer = CancelableRequestPerformer(_connection);

    element.shadowRoot.querySelectorAll(":host > *").style.display = "none";

    _SORTABLE.forEach((sortField, column) {
      w_table.tHead.rows.first.cells[column.index].querySelector(".headerText")
          ..classes.add("sortable")
          ..onClick.listen((_) => _sort(sortField));
    });

    w_sendToArchive.onClick.listen((_) => _sendWorkToArchive());

    w_tableBody.onMouseDown.listen(_onTableBodyMouseDown);

    w_main.onContextMenu.listen(_onMainContextMenu);
    w_table.tHead.onContextMenu.listen(stopFurtherProcessing);

    w_closeButton.onClick.listen((_) => _close());

    windowShortcutHandler
        ..add(KeyCode.F4, KeyModifierState.CTRL, _close)
        ..add(KeyCode.ESC, KeyModifierState.NONE, _closeWatchedSessionPanel);

    w_workMenuButton.onClick.listen((_) => _onWorkMenuButtonClick());
    w_sessionMenuButton.onClick.listen((_) => _onSessionMenuButtonClick());

    _noticeSubscription = noticeStream.listen(_onNotice);
  }

  @override void detached() {
    _intersectionObserver.disconnect();
    _noticeSubscription?.cancel();
    _noticeSubscription = null;
    _timer?.cancel();
    _timer = null;
    _requestPerformer.cancel();
    super.detached();
  }

  void _updateWatchedSessionOverviewSet(List<IntersectionObserverEntry> entries) {
    var request = UpdateWatchedSessionOverviewSet();

    for (var e in entries) {
      int index = int.parse(e.target.dataset[_ROW_INDEX_ATTRIBUTE]);
      if (e.isIntersecting && !_watchedSessionOverviewsByIndex.containsKey(index)) {
        _watchedSessionOverviewsByIndex[index] = null;
        request.item.add(UpdateWatchedSessionOverviewSet_Item()
            ..watch = true
            ..index = index);
      } else if (!e.isIntersecting && _watchedSessionOverviewsByIndex.containsKey(index)) {
        _watchedSessionOverviewsByIndex.remove(index);
        w_tableBody.rows[index]
            ..nodes = [_createPlaceholderCell()]
            ..classes.clear();
        request.item.add(UpdateWatchedSessionOverviewSet_Item()
            ..watch = false
            ..index = index);
      }
    }

    if (request.item.isNotEmpty) {
      _send(WatcherRequest()
          ..updateWatchedSessionOverviewSet = request);
    }
  }

  static TableCellElement _createPlaceholderCell() => TableCellElement()
      ..colSpan = _Column.values.length
      ..text = "\u00a0";

  void _onNotice(WatcherNotice m) {
    if (m.hasWatcherHello()) {
      _onWatcherHello(m.watcherHello);
    } else if (m.hasSessionCount()) {
      _onSessionCount(m.sessionCount);
    } else if (m.hasSessionOverviewDelta()) {
      _onSessionOverviewDelta(m.sessionOverviewDelta);
    }
  }

  void _onWatcherHello(WatcherHello m) {
    w_workTitle.text = m.workTitle;
    _showSessionCount();

    _referenceServerTimeMilliseconds = m.serverTimeMilliseconds;
    _referenceServerTimeArrivalMilliseconds = window.performance.now();

    check(_timer == null);
    _timer = Timer.periodic(const Duration(seconds: 1),
        (_) => _displayRemainingTime(_watchedSessionOverviewsByIndex));

    _order = m.order;
    _displaySortIndicator();

    element.shadowRoot.querySelectorAll(":host > *").style.display = "";
  }

  void _onSessionCount(SessionCount m) {
    for (int count = m.value, i = w_tableBody.rows.length; i < count; ++i) {
      var row = TableRowElement()
          ..append(_createPlaceholderCell())
          ..dataset[_ROW_INDEX_ATTRIBUTE] = i.toString();
      _intersectionObserver.observe(row);
      w_tableBody.append(row);
    }

    _showSessionCount();
  }

  void _showSessionCount() {
    w_sessionCount.text = "${(_selectedSessionCount >= 2) ? "$_selectedSessionCount / " : ""}"
        "${w_tableBody.rows.length}";
  }

  void _onSessionOverviewDelta(SessionOverviewDelta m) {
    Map<int, SessionOverview> updated = {};

    for (var item in m.item) {
      if (_watchedSessionOverviewsByIndex.containsKey(item.index)) {
        SessionOverview so = item.sessionOverview;
        _watchedSessionOverviewsByIndex[item.index] = so;
        updated[item.index] = so;

        TableRowElement row = w_tableBody.rows[item.index];

        if (row.cells.length != _Column.values.length) {
          row.nodes.clear();
          for (var column in _Column.values) {
            var cell = TableCellElement();
            if (_COLUMNS_WITH_TOOLTIPS.contains(column)) {
              cell.onMouseEnter.listen(_onCellMouseEnter);
            }
            row.append(cell);
          }
        }

        row
            ..cells[_Column.USER_NAME.index].text = so.userDisplayName
            ..cells[_Column.GROUP_NAME.index].text = so.groupName
            ..cells[_Column.BAR.index].children = [(CorrectnessBar()
                ..percentCorrect = "${scaledDecimalToPercent(so.scaledResult)}"
                ..percentWrong = "${scaledDecimalToPercent(so.scaledAttemptedResult - so.scaledResult)}").element]
            ..cells[_Column.RESULT.index].text = "${scaledDecimalToIntegerPercent(so.scaledResult)}"
            ..cells[_Column.MARK.index].text = so.mark
            ..cells[_Column.OPENED_AT.index].text = formatTimestamp(so.openedAt)
            ..cells[_Column.ADDRESS.index].text = so.address
            ..classes.toggle("finished", so.finished)
            ..classes.toggle("banned", so.banned)
            ..classes.toggle("selected", item.selected);

        for (var column in _COLUMNS_WITH_TOOLTIPS) {
          TableCellElement cell = row.cells[column.index];
          if (cell.title != cell.text) {
            cell.title = "";
          }
        }
      }
    }

    _displayRemainingTime(updated);
  }

  void _displayRemainingTime(Map<int, /* nullable */SessionOverview> sessionOverviewsByIndex) {
    num now = _referenceServerTimeMilliseconds + (window.performance.now() - _referenceServerTimeArrivalMilliseconds);

    sessionOverviewsByIndex.forEach((index, so) {
      if (so != null) {
        if (so.hasDeadlineMilliseconds()) {
          num remaining = math.max(so.deadlineMilliseconds - now, 0);
          w_tableBody.rows[index]
              ..cells[_Column.TIME.index].text = formatRemainingTime(remaining)
              ..classes.toggle("timedOut", remaining == 0);
        } else {
          w_tableBody.rows[index]
              ..cells[_Column.TIME.index].text = ""
              ..classes.remove("timedOut");
        }
      }
    });
  }

  void _onCellMouseEnter(MouseEvent e) {
    TableCellElement cell = e.currentTarget as TableCellElement;
    cell.title = (cell.scrollWidth > cell.clientWidth) ? cell.text : "";
  }

  void _onTableBodyMouseDown(MouseEvent e) {
    int n = e.path.indexOf(w_tableBody);
    if (n > 0) {
      _handleRowMouseDown(e.path[n - 1] as TableRowElement, e);
    }
  }

  Future<void> _handleRowMouseDown(TableRowElement row, MouseEvent e) async {
    if (((e.button == 0) && (e.detail == 1)) || ((e.button == 2) && !row.classes.contains("selected"))) {
      int index = int.parse(row.dataset[_ROW_INDEX_ATTRIBUTE]);
      SessionOverview sessionOverview = _watchedSessionOverviewsByIndex[index];
      if (sessionOverview != null) {
        _selectedSessionCount = (await _perform(WatcherRequest()
            ..modifySessionSelection = (ModifySessionSelection()
                ..sessionId = sessionOverview.id
                ..range = e.shiftKey
                ..toggle = e.ctrlKey))).modifySessionSelectionReply.selectedSessionCount;
        _showSessionCount();

        if (_selectedSessionCount == 1) {
          if ((_watchedSessionPanel != null) || doubleClickedWhileUiWasLocked) {
            await _openWatchedSessionPanel();
          }
        } else {
          _closeWatchedSessionPanel();
        }

        if (e.button == 2) {
          await _showSessionPopupMenu(MenuPopupOptions());
        }
      }
    } else if ((e.button == 0) && (e.detail == 2) && (_selectedSessionCount == 1) && row.classes.contains("selected")) {
      await _openWatchedSessionPanel();
    }
  }

  Future<void> _openWatchedSessionPanel() async {
    WatchSelectedSession_Reply reply = (await _perform(WatcherRequest()
        ..watchSelectedSession = WatchSelectedSession())).watchSelectedSessionReply;
    Stream<SessionDelta> sessionDeltaStream = _connection.tune(_requestPerformer.lastRequestId)
        .map((m) => m.watcherNotice.sessionDelta);

    _watchedSessionPanel?.element?.remove();
    _watchedSessionPanel = null;

    _watchedSessionPanel = WatchedSessionPanel(reply, _connection, _send, sessionDeltaStream)
        ..element.id = "watchedSessionPanel"
        ..onClose.then((_) => _closeWatchedSessionPanel());
    w_sideBySide.append(_watchedSessionPanel.element);
  }

  void _closeWatchedSessionPanel() {
    if (_watchedSessionPanel != null) {
      _watchedSessionPanel.element.remove();
      _watchedSessionPanel = null;

      _send(WatcherRequest()
          ..unwatchSession = UnwatchSession());

      _send(WatcherRequest()
          ..unwatchQuestion = UnwatchQuestion());
    }
  }

  Future<void> _showSessionPopupMenu(MenuPopupOptions options) async {
    DiscoverSessionActions_Reply reply = (await _perform(WatcherRequest()
        ..discoverSessionActions = DiscoverSessionActions())).discoverSessionActionsReply;

    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_openWatchedSessionPanel),
            label: tr("Show Details"),
            enabled: _selectedSessionCount == 1)))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _setBanned(true)),
            label: tr("Ban"),
            enabled: reply.canBan)))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _setBanned(false)),
            label: tr("Unban"),
            enabled: reply.canUnban)))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_resume),
            label: tr("Resume"),
            enabled: reply.canResume)))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_changeTimeLimit),
            label: tr("Change Time Limit..."),
            enabled: _selectedSessionCount > 0)))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_detach),
            label: tr("Disconnect and Allow Logging in Again"),
            enabled: reply.canDetach)));

    showPopupMenu(menu, options);
  }

  Future<void> _onSessionMenuButtonClick() async {
    await _showSessionPopupMenu(getDropDownMenuPosition(w_sessionMenuButton));
  }

  Future<void> _onMainContextMenu(MouseEvent e) async {
    stopFurtherProcessing(e);
    await _showSessionPopupMenu(MenuPopupOptions());
  }

  void _displaySortIndicator() {
    w_table.tHead.rows.first
        ..querySelectorAll(".sortable").classes.removeAll(["sortedUp", "sortedDown"])
        ..cells[_SORTABLE[_order.sortField].index].querySelector(".sortable")
            .classes.add(_order.sortAscending ? "sortedUp" : "sortedDown");
  }

  Future<void> _sort(SessionOverviewSortField sortField) async {
    var newOrder = SessionOverviewOrder()
        ..sortField = sortField
        ..sortAscending = (sortField == _order.sortField) ?
            !_order.sortAscending :
            _INITIALLY_SORT_ASCENDING.contains(sortField);

    await _perform(WatcherRequest()
        ..setSessionOverviewOrder = (SetSessionOverviewOrder()
            ..order = newOrder));

    _order = newOrder;
    _displaySortIndicator();
  }

  Future<void> _saveTable() async {
    ExportToTsv_Reply reply = (await _perform(WatcherRequest()
        ..exportToTsv = (ExportToTsv()
            ..header.addAll([tr("Student"), tr("Group"), tr("Result, %"), tr("Grade"), tr("Session Start")])
            ..timeZone = userTimeZone))).exportToTsvReply;

    String fileName = showSaveFileDialog("tsv", tr("TSV Files"));
    if (fileName != null) {
      try {
        writeViaTempFile(reply.tsv, fileName);
      } catch (e) {
        showErrorMessage(e.toString());
      }
    }
  }

  void _onWorkMenuButtonClick() {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_sendWorkToArchive),
            label: tr("Send to Archive..."),
            icon: getAbsoluteAppPath("watch_screen.resources/sendToArchive.png"))))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_deleteWork),
            label: tr("Delete"))))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_saveTable),
            label: tr("Save Table..."))));

    showPopupMenu(menu, getDropDownMenuPosition(w_workMenuButton));
  }

  Future<void> _deleteWork() async {
    if (showMessageBox(ShowMessageBoxOptions(
        type: "warning",
        buttons: [tr("Delete"), tr("Cancel")],
        message: tr("The results of all students will be deleted."),
        cancelId: 1)) == 0) {
      lockUi();
      try {
        DeleteWork_Reply reply = (await _connection.perform(ClientMessage()
            ..channelId = _workControllerChannelId
            ..workControllerRequest = (WorkControllerRequest()
                ..deleteWork = (DeleteWork()
                    ..workId = _workId)))).workControllerReply.deleteWorkReply;
        _notifyIfFailed(reply.result);
      } finally {
        unlockUi();
      }
    }
  }

  Future<void> _sendWorkToArchive() async {
    if (await ArchiveDialog(_remoteServer, element.shadowRoot).openModal()) {
      lockUi();
      try {
        SendWorkToArchive_Reply reply = (await _connection.perform(ClientMessage()
            ..channelId = _workControllerChannelId
            ..workControllerRequest = (WorkControllerRequest()
                ..sendWorkToArchive = (SendWorkToArchive()
                    ..workId = _workId)))).workControllerReply.sendWorkToArchiveReply;
        _notifyIfFailed(reply.result);
      } finally {
        unlockUi();
      }
    }
  }

  void _notifyIfFailed(ShutDownWorkResult result) {
    String message() {
      switch (result) {
        case ShutDownWorkResult.OK:
        case ShutDownWorkResult.WORK_NOT_FOUND:
          return null;
        case ShutDownWorkResult.INCORRECT_ARCHIVE_FILE:
          return tr("The specified file is not an archive.");
        case ShutDownWorkResult.UNKNOWN_ARCHIVE_FILE_VERSION:
          return tr("A newer version of the program is required for working with the specified archive file.");
        case ShutDownWorkResult.SEND_TO_ARCHIVE_FAILED:
          return tr("Cannot send to archive.");
        default: return result.name;
      }
    }

    String s = message();
    if (s != null) {
      showErrorMessage(s);
    }
  }

  void _close() {
    _send(WatcherRequest()
        ..close = Close());
    _closedExplicitly = true;
  }

  Int64 _send(WatcherRequest request) => _connection.send(ClientMessage()
      ..channelId = _watcherChannelId
      ..watcherRequest = request);

  Future<WatcherReply> _perform(WatcherRequest request) async => (await _requestPerformer.perform(ClientMessage()
      ..channelId = _watcherChannelId
      ..watcherRequest = request)).watcherReply;

  Future<void> _setBanned(bool banned) async {
    await _perform(WatcherRequest()
        ..setBanned = (SetBanned()
            ..banned = banned));
  }

  Future<void> _resume() async {
    await _perform(WatcherRequest()
        ..resume = Resume());
  }

  Future<void> _changeTimeLimit() async {
    ChangeTimeLimit request = await ChangeTimeLimitDialog(element.shadowRoot).openModal();
    if (request != null) {
      await _perform(WatcherRequest()
          ..changeTimeLimit = request);
    }
  }

  Future<void> _detach() async {
    await _perform(WatcherRequest()
        ..detach = Detach());
  }
}

enum _Column { USER_NAME, GROUP_NAME, BAR, RESULT, MARK, TIME, OPENED_AT, ADDRESS }

class ChangeTimeLimitDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-change-time-limit-dialog";

  DialogElement w_dialog;
  TextInputElement w_timeLimit;

  final Node _dialogParent;

  ChangeTimeLimitDialog(this._dialogParent) : super(TAG);

  Future</* nullable */ChangeTimeLimit> openModal() async {
    var timeLimitPattern = RegExp(r"^\s*([+-]?[0-9]+)\s*$");
    w_timeLimit.pattern = timeLimitPattern.pattern;

    await open(w_dialog, dialogParent: _dialogParent);

    ChangeTimeLimit res;
    if (w_dialog.returnValue.isEmpty) {
      res = null;
    } else {
      String timeLimit = timeLimitPattern.firstMatch(w_timeLimit.value).group(1);
      bool relative = timeLimit.startsWith("+") || timeLimit.startsWith("-");
      int limitMinutes = int.parse(timeLimit);

      res = ChangeTimeLimit();
      if (relative || (limitMinutes != 0)) {
        res.value = ChangeTimeLimit_Value()
            ..relative = relative
            ..limitMinutes = limitMinutes;
      }
    }

    return res;
  }
}

class WatchedSessionPanel extends WebComponent {
  static const String TAG = "ru-irenproject-watched-session-panel";

  StyleElement w_clientStyle;
  ButtonElement w_userResponseButton;
  ButtonElement w_correctResponseButton;
  DivElement w_scrollable;
  SpanElement w_result;
  SpanElement w_weight;
  SpanElement w_score;
  ButtonElement w_previousButton;
  ButtonElement w_nextButton;
  ButtonElement w_closeButton;

  final Int64 _sessionId;

  final Completer<void> _onClose = Completer();
  Future<void> get onClose => _onClose.future;

  final Connection _connection;
  final Int64 Function(WatcherRequest) _send;
  final int _questionCount;

  ResizeObserver _resizeObserver;

  StreamSubscription<SessionDelta> _sessionDeltaSubscription;

  SelectorBar _selectorBar;

  Int64 _watchQuestionRequestId;
  /* nullable */StreamSubscription<WatchQuestion_Reply> _watchQuestionReplySubscription;
  /* nullable */StreamSubscription<QuestionDelta> _questionDeltaSubscription;

  int _questionIndex;
  WatchQuestion_Reply _watchQuestionReply;
  QuestionDelta _questionDelta;

  /* nullable */int _newQuestionIndex;
  /* nullable */WatchQuestion_Reply _newWatchQuestionReply;

  DialogWidget _dialog;
  bool _showingCorrectResponse = false;

  bool _questionControlsEnabled = false;

  WatchedSessionPanel(
      WatchSelectedSession_Reply m,
      this._connection,
      this._send,
      Stream<SessionDelta> sessionDeltaStream)
      : _sessionId = m.sessionId,
        _questionCount = m.questionWeight.length,
        super(TAG) {
    w_clientStyle.text = _irenCss;

    _resizeObserver = ResizeObserver((_, __) => _layOutDialogIfShown())
        ..observe(w_scrollable);

    _selectorBar = SelectorBar(_questionCount)
        ..element.id = "selectorBar"
        ..element.style.visibility = "hidden"
        ..onSelectItem.listen((e) => _watchQuestion(e.itemIndex));
    m.questionWeight.asMap().forEach(_selectorBar.setItemWeight);

    findElement("selectorBarPlaceholder").replaceWith(_selectorBar.element);

    w_closeButton.onClick.listen((_) => _onCloseClick());

    _sessionDeltaSubscription = sessionDeltaStream.listen(_onSessionDelta);

    _watchQuestion(m.currentQuestionIndex);
  }

  void _layOutDialogIfShown() {
    if (element.offsetParent != null) {
      _dialog?.layOut();
    }
  }

  @override void attached() {
    super.attached();
    _selectorBar.layOut();
  }

  @override void detached() {
    _resizeObserver.disconnect();
    _sessionDeltaSubscription?.cancel();
    _sessionDeltaSubscription = null;
    _cancelQuestionSubscriptions();
    super.detached();
  }

  void _cancelQuestionSubscriptions() {
    _watchQuestionReplySubscription?.cancel();
    _watchQuestionReplySubscription = null;

    _questionDeltaSubscription?.cancel();
    _questionDeltaSubscription = null;
  }

  void _onSessionDelta(SessionDelta m) {
    m.questionStatus.forEach((questionIndex, questionStatus) =>
        _selectorBar.setItemColor(questionIndex, QUESTION_COLORS[questionStatus]));
    _selectorBar.element.style.visibility = "";
  }

  void _watchQuestion(int questionIndex) {
    _cancelQuestionSubscriptions();
    _newQuestionIndex = questionIndex;

    _watchQuestionRequestId = _send(WatcherRequest()
        ..watchQuestion = (WatchQuestion()
            ..sessionId = _sessionId
            ..questionIndex = questionIndex));
    _watchQuestionReplySubscription = _connection.tune(_watchQuestionRequestId)
        .map((m) => m.watcherReply.watchQuestionReply)
        .listen(_onWatchQuestionReply);
  }

  void _onWatchQuestionReply(WatchQuestion_Reply m) {
    _watchQuestionReplySubscription.cancel();
    _watchQuestionReplySubscription = null;

    _newWatchQuestionReply = m;

    _questionDeltaSubscription = _connection.tune(_watchQuestionRequestId)
        .map((m) => m.watcherNotice.questionDelta)
        .listen(_onQuestionDelta);
  }

  void _onQuestionDelta(QuestionDelta m) {
    bool newQuestion = (_newQuestionIndex != null);

    if (newQuestion) {
      _questionIndex = _newQuestionIndex;
      _watchQuestionReply = _newWatchQuestionReply;
      _newQuestionIndex = null;

      _selectorBar.setCurrentItem(_questionIndex);
      w_previousButton.disabled = (_questionIndex == 0);
      w_nextButton.disabled = (_questionIndex == _questionCount - 1);
    }

    _questionDelta = m;
    _showQuestion(retainScrollPosition: !newQuestion);

    _enableQuestionControls();
  }

  void _showQuestion({bool retainScrollPosition: false}) {
    w_userResponseButton.classes.toggle("selected", !_showingCorrectResponse);
    w_correctResponseButton.classes.toggle("selected", _showingCorrectResponse);

    _dialog = DialogWidget(_watchQuestionReply.dialog, _showingCorrectResponse ?
        _watchQuestionReply.correctResponse : _questionDelta.response);
    _dialog.setReadOnly(true);

    int scrollPosition = retainScrollPosition ? w_scrollable.scrollTop : 0;
    w_scrollable.children = [_dialog.element];
    _dialog.layOut();
    w_scrollable.scrollTop = scrollPosition;

    w_result.text = "${scaledDecimalToIntegerPercent(_questionDelta.scaledResult)}%";
    w_weight.text = "${_watchQuestionReply.weight}";
    w_score.text = formatScaledDecimal(_questionDelta.scaledScore);
  }

  void _onCloseClick() {
    if (!_onClose.isCompleted) {
      _onClose.complete();
    }
  }

  void _selectPage({@required bool correctResponse}) {
    if (_showingCorrectResponse != correctResponse) {
      _showingCorrectResponse = correctResponse;
      _showQuestion(retainScrollPosition: true);
    }
  }

  void _enableQuestionControls() {
    if (!_questionControlsEnabled) {
      _questionControlsEnabled = true;

      w_userResponseButton.onClick.listen((_) => _selectPage(correctResponse: false));
      w_correctResponseButton.onClick.listen((_) => _selectPage(correctResponse: true));
      w_userResponseButton.disabled = w_correctResponseButton.disabled = false;

      w_previousButton.onClick.listen((_) => _watchQuestion(_questionIndex - 1));
      w_nextButton.onClick.listen((_) => _watchQuestion(_questionIndex + 1));
    }
  }
}

//TODO move to some common library
class CancelableRequestPerformer {
  final Connection _connection;

  /* nullable */StreamSubscription<ServerMessage> _replySubscription;

  Int64 _lastRequestId;
  Int64 get lastRequestId => _lastRequestId;

  CancelableRequestPerformer(this._connection);

  Future<ServerMessage> perform(ClientMessage request) async {
    check(_replySubscription == null);

    _lastRequestId = _connection.send(request);

    var completer = Completer<ServerMessage>();
    _replySubscription = _connection.tune(_lastRequestId).listen(completer.complete);
    lockUi();
    try {
      return await completer.future;
    } finally {
      cancel();
    }
  }

  void cancel() {
    if (_replySubscription != null) {
      unlockUi();
      _replySubscription.cancel();
      _replySubscription = null;
    }
  }
}

const String _IREN_CSS_LOCATION = "package:iren_client/iren.css";

@EmbeddedResource(_IREN_CSS_LOCATION)
final String _irenCss = getEmbeddedResource(_IREN_CSS_LOCATION);
