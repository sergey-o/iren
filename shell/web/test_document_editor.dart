/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'package:iren_proto/editor/editor.pb.dart';
import 'package:iren_proto/editor/modifier_screen.pb.dart';
import 'package:iren_proto/editor/profile_screen.pb.dart';

import 'editor_infra.dart';
import 'modifier_screen.dart';
import 'profile_screen.dart';
import 'test_screen.dart';

class TestDocumentEditor extends DocumentEditor<XTestDocumentEditor> {
  static const String TAG = "ru-irenproject-test-document-editor";

  static final Map<Type, WidgetFactory> _screens = {
      XTestScreen: (name, dispatcher) => TestScreen(name, dispatcher),
      XProfileScreen: (name, dispatcher) => ProfileScreen(name, dispatcher),
      XModifierScreen: (name, dispatcher) => ModifierScreen(name, dispatcher)};

  TestDocumentEditor(String name, Dispatcher dispatcher) : super(TAG, name, dispatcher);

  @override void render() {
    super.render();
    renderScreen(shape.screen, _screens);
  }

  @override bool get modified => shape.modified;

  @override void markAsUnmodified() {
    performAction(XTestDocumentEditor_MarkAsUnmodified());
  }

  @override bool get exportableToHtml => true;
}

void initialize() {
  registerShapeParser((b) => XTestDocumentEditor.fromBuffer(b));
}
