/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/editor.pb.dart';

import 'common_dialogs.dart';
import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';

class SectionTreeEditor extends Widget<XSectionTreeEditor> {
  static const String TAG = "ru-irenproject-section-tree-editor";

  DivElement w_main;
  ButtonElement w_addButton;
  ButtonElement w_deleteButton;
  ButtonElement w_setNameButton;
  ButtonElement w_modifiersButton;
  ButtonElement w_moveUpButton;
  ButtonElement w_moveDownButton;

  List<XSectionEditor> _rendered;

  SectionTreeEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addButton.onClick.listen((_) => _add());
    w_deleteButton.onClick.listen((_) => _delete());
    w_setNameButton.onClick.listen((_) => _setName());
    w_modifiersButton.onClick.listen((_) => _showModifiers());
    w_moveUpButton.onClick.listen((_) => _moveUp());
    w_moveDownButton.onClick.listen((_) => _moveDown());

    windowShortcutHandler
        ..add(KeyCode.F2, KeyModifierState.SHIFT, _add)
        ..add(KeyCode.F3, KeyModifierState.SHIFT, _setName)
        ..add(KeyCode.F4, KeyModifierState.SHIFT, _showModifiers);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);

    w_main.onContextMenu.listen(_onContextMenu);
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      w_main.nodes.clear();
      _createNode(shape.root, 0);
      _rendered = List.filled(w_main.children.length, null);
    }

    _renderNodes();

    w_deleteButton.disabled = _rootSelected();
    w_moveUpButton.disabled = !_selection().canMoveUp;
    w_moveDownButton.disabled = !_selection().canMoveDown;
  }

  void _createNode(SectionNode node, int level) {
    w_main.append(DivElement()
        ..dataset["name"] = node.editor
        ..style.marginLeft = "${level * 2}em"
        ..onMouseDown.listen(_onSectionMouseDown)
        ..append(SpanElement()
            ..classes.add("icon"))
        ..append(SpanElement()
            ..classes.add("name")
            ..dataset["untitled"] = getPrintableSectionName("")));
    for (var child in node.child) {
      _createNode(child, level + 1);
    }
  }

  Future<void> _onSectionMouseDown(MouseEvent e) async {
    DivElement row = e.currentTarget as DivElement;
    if (((e.button == 0) && (e.detail == 1)) || ((e.button == 2) && !row.classes.contains("selected"))) {
      await dispatcher.performAction(row.dataset["name"], XSectionEditor_Select());
      await dispatcher.onChangeScene.first;

      if (e.button == 2) {
        _showContextMenu();
      } else if (doubleClickedWhileUiWasLocked) {
        await _setName();
      }
    } else if ((e.button == 0) && (e.detail == 2) && row.classes.contains("selected")) {
      await _setName();
    }
  }

  void _renderNodes() {
    int rowIndex = 0;
    String selectedEditorName = _selection().selectedEditor;

    void renderNode(SectionNode node) {
      XSectionEditor editor = scene.shapes(node.editor);
      DivElement row = w_main.children[rowIndex] as DivElement;

      if (!identical(editor, _rendered[rowIndex])) {
        _rendered[rowIndex] = editor;
        row.children.first.classes.toggle("hasModifiers", editor.hasModifiers);
        row.children[1]
            ..text = editor.name
            ..classes.toggle("populated", editor.questionCount > 0);
      }

      bool selected = (node.editor == selectedEditorName);
      if (selected && !row.classes.contains("selected")) {
        scheduleScrollIntoView(row);
      }
      row.classes.toggle("selected", selected);

      ++rowIndex;

      node.child.forEach(renderNode);
    }

    renderNode(shape.root);
  }

  XSectionTreeSelection _selection() => scene.shapes(shape.selection);

  Future<void> _add() async {
    String name = await inputString(prompt: tr("Section name:"), maxLength: shape.maxSectionNameLength);
    if (name != null) {
      performAction(XSectionTreeEditor_Add()
          ..name = collapseSpaces(name));
    }
  }

  void _delete() {
    if (!_rootSelected() && showMessageBox(ShowMessageBoxOptions(
        type: "question",
        buttons: [tr("Delete"), tr("Cancel")],
        message: trD("DELETE_SECTION")(getPrintableSectionName(_selectedEditor().name)) as String,
        cancelId: 1)) == 0) {
      performAction(XSectionTreeEditor_Delete());
    }
  }

  XSectionEditor _selectedEditor() => scene.shapes(_selection().selectedEditor);

  bool _rootSelected() => _selection().selectedEditor == shape.root.editor;

  void _moveUp() {
    if (_selection().canMoveUp) {
      performAction(XSectionTreeEditor_Move()
          ..forward = false);
    }
  }

  void _moveDown() {
    if (_selection().canMoveDown) {
      performAction(XSectionTreeEditor_Move()
          ..forward = true);
    }
  }

  Future<void> _setName() async {
    String name = await inputString(prompt: tr("Section name:"), maxLength: shape.maxSectionNameLength,
        initial: _selectedEditor().name);
    if (name != null) {
      dispatcher.performAction(_selection().selectedEditor, XSectionEditor_SetName()
          ..name = collapseSpaces(name));
    }
  }

  void _showModifiers() {
    dispatcher.performAction(_selection().selectedEditor, XSectionEditor_ShowModifiers());
  }

  void _onContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);
    _showContextMenu();
  }

  void _showContextMenu() {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_add),
            label: tr("Add..."),
            icon: getAbsoluteAppPath("section_tree_editor.resources/add.png"),
            accelerator: "Shift+F2")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_paste),
            label: tr("Paste"),
            enabled: clipboard_availableFormats().contains(QUESTION_LIST_MIME_TYPE),
            accelerator: "CmdOrCtrl+V")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_delete),
            label: tr("Delete"),
            icon: getAbsoluteAppPath("section_tree_editor.resources/delete.png"),
            enabled: !w_deleteButton.disabled,
            accelerator: "Delete")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_setName),
            label: tr("Rename..."),
            icon: getAbsoluteAppPath("section_tree_editor.resources/rename.png"),
            accelerator: "Shift+F3")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_showModifiers),
            label: tr("Modifiers"),
            icon: getAbsoluteAppPath("section_tree_editor.resources/modifiers.png"),
            accelerator: "Shift+F4")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveUp),
            label: tr("Move Up"),
            icon: getAbsoluteAppPath("section_tree_editor.resources/moveUp.png"),
            enabled: !w_moveUpButton.disabled,
            accelerator: "Alt+Up")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveDown),
            label: tr("Move Down"),
            icon: getAbsoluteAppPath("section_tree_editor.resources/moveDown.png"),
            enabled: !w_moveDownButton.disabled,
            accelerator: "Alt+Down")));

    showPopupMenu(menu, MenuPopupOptions());
  }

  void _paste() {
    w_main.focus();
    document.execCommand("paste");
  }
}

void initialize() {
  registerShapeParsers([
      (b) => XSectionTreeEditor.fromBuffer(b),
      (b) => XSectionTreeSelection.fromBuffer(b),
      (b) => XSectionEditor.fromBuffer(b)]);
}
