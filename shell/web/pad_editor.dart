/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:async/async.dart';
import 'package:image/image.dart' show DecodeInfo, Decoder, JpegDecoder, PngDecoder;
import 'package:iren_proto/editor/infra.pb.dart';
import 'package:iren_proto/editor/pad_editor.pb.dart';
import 'package:meta/meta.dart';
import 'package:web_helpers/web_helpers.dart';

import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';

class PadEditor extends Widget<XPadEditor> {
  static const String TAG = "ru-irenproject-pad-editor";
  static const String _PAD_MIME_TYPE = "application/x-irenproject.ru-pad";
  static const String _TEXT_MIME_TYPE = "text/plain";
  static const String _PNG_MIME_TYPE = "image/png";

  static const int _FORMULA_SHORTCUT_KEY = KeyCode.M;
  static const KeyModifierState _FORMULA_SHORTCUT_MODIFIERS = KeyModifierState.CTRL;
  static const String _FORMULA_SHORTCUT_NAME = "CmdOrCtrl+M";

  static final Map<String, Decoder Function()> _imageDecoders = {
      "image/png": () => PngDecoder(),
      "image/jpeg": () => JpegDecoder()};

  DivElement w_root;

  /* nullable */int _desiredCaretPosition;
  /* nullable */int _desiredCaretOffsetFromEnd;
  final Map<String, _Image> _imageCache = {};

  final SpanElement _caretHelper = SpanElement()
      ..id = "caretHelper"
      ..text = "|";

  /* nullable */int _selectionStartBeforeComposition;
  /* nullable */int _selectionEndBeforeComposition;

  bool _lockedUiCursorSet = false;

  PadEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_root
        ..onKeyPress.listen(_onKeyPress)
        ..onKeyDown.listen(_onKeyDown)
        ..onMouseEnter.listen((_) => _setLockedUiCursor())
        ..onMouseLeave.listen((_) => _unsetLockedUiCursor())
        ..onBlur.listen((_) => _onBlur())
        ..onCopy.listen(_onCopy)
        ..onCut.listen(_onCut)
        ..onPaste.listen(_onPaste)
        ..onMouseDown.listen(_onMouseDown)
        ..onDoubleClick.listen(_onDoubleClick)
        ..onContextMenu.listen(_onContextMenu)
        ..on["compositionstart"].listen((_) => _onCompositionStart())
        ..on["compositionend"].listen((e) => _onCompositionEnd(e as CompositionEvent));

    ShortcutHandler(w_root)
        ..add(_FORMULA_SHORTCUT_KEY, _FORMULA_SHORTCUT_MODIFIERS, _insertOrEditFormula)
        ..add(KeyCode.Z, KeyModifierState.CTRL, () {})
        ..add(KeyCode.Z, KeyModifierState.CTRL_SHIFT, () {})
        ..add(KeyCode.Y, KeyModifierState.CTRL, () {})
        ..add(KeyCode.A, KeyModifierState.CTRL_SHIFT, () {});
  }

  @override void detached() {
    for (var image in _imageCache.values) {
      image._release();
    }
    _imageCache.clear();
    _unsetLockedUiCursor();
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      for (var image in _imageCache.values) {
        image.used = false;
      }

      int i = 0;
      int p = 0;
      Node caretNode = null;
      int caretOffset = null;

      if (_desiredCaretOffsetFromEnd != null) {
        int totalLength = 0;
        for (var block in shape.block) {
          totalLength += (block.type == BlockType.TEXT) ? block.textBlock.text.length : 1;
        }
        _desiredCaretPosition = math.max(totalLength - _desiredCaretOffsetFromEnd, 0);
        _desiredCaretOffsetFromEnd = null;
      }

      if (_desiredCaretPosition == 0) {
        caretNode = w_root;
        caretOffset = 0;
        _desiredCaretPosition = null;
      }

      for (var block in shape.block) {
        Node existing = (i < w_root.nodes.length) ? w_root.nodes[i] : null;

        switch (block.type) {
          case BlockType.TEXT:
            String text = block.textBlock.text;
            if (existing is Text) {
              existing.text = text;
            } else {
              _appendOrReplaceWith(existing, Text(text));
            }
            break;
          case BlockType.IMAGE:
            _Image cachedImage = _getCachedImage(block.imageBlock.data, block.imageBlock.mimeType)
                ..used = true;
            bool alreadyRendered = (existing is ImageElement) && (existing.src == cachedImage.blobUrl);
            if (!alreadyRendered) {
              _appendOrReplaceWith(existing, ImageElement()
                  ..style.width = "${cachedImage.size.width}px"
                  ..style.height = "${cachedImage.size.height}px"
                  ..dataset["blob"] = block.imageBlock.data
                  ..dataset["mimeType"] = block.imageBlock.mimeType
                  ..src = cachedImage.blobUrl);
            }
            break;
          case BlockType.LINE_FEED:
            if (existing is! BRElement) {
              _appendOrReplaceWith(existing, BRElement());
            }
            break;
          case BlockType.FORMULA:
            _Image cachedFormulaImage = _getCachedFormulaImage(block.formulaBlock.svg)
                ..used = true;
            bool alreadyRendered = (existing is ImageElement) && (existing.src == cachedFormulaImage.blobUrl);
            if (!alreadyRendered) {
              var image = ImageElement()
                  ..dataset["origin"] = _ImageOrigin.FORMULA.toString()
                  ..src = cachedFormulaImage.blobUrl;
              applyFormulaStyle(block.formulaBlock.style, image.style);
              _appendOrReplaceWith(existing, image);
            }
            break;
          default:
            throw "Unknown block type '${block.type}'.";
        }

        if (_desiredCaretPosition != null) {
          Node current = w_root.nodes[i];
          if ((current is Text) && (p + current.length > _desiredCaretPosition)) {
            caretNode = current;
            caretOffset = _desiredCaretPosition - p;
            _desiredCaretPosition = null;
          }

          if (_desiredCaretPosition != null) {
            p += _getNodeLength(current);
            if (p >= _desiredCaretPosition) {
              caretNode = w_root;
              caretOffset = i + 1;
              _desiredCaretPosition = null;
            }
          }
        }

        ++i;
      }

      Node existing = (i < w_root.nodes.length) ? w_root.nodes[i] : null;
      if (existing is! BRElement) {
        _appendOrReplaceWith(existing, BRElement());
      }
      ++i;

      while (w_root.nodes.length > i) {
        w_root.lastChild.remove();
      }

      _desiredCaretPosition = null;

      if (caretNode != null) {
        element.shadowRoot.getSelection()
            ..collapse(caretNode, caretOffset)
            ..getRangeAt(0).insertNode(_caretHelper);
        _caretHelper
            ..scrollIntoView()
            ..remove();
        normalizeNode(w_root);
        element.shadowRoot.getSelection().collapse(caretNode, caretOffset);
      }

      _removeUnusedCachedImages();
    }
  }

  void _appendOrReplaceWith(/* nullable */Node existing, Node fresh) {
    if (existing == null) {
      w_root.append(fresh);
    } else {
      existing.replaceWith(fresh);
    }
  }

  _Image _getCachedImage(String blobName, String mimeType) {
    var decoderFactory = _imageDecoders[mimeType];
    if (decoderFactory == null) {
      throw "No image decoder for '$mimeType'.";
    }

    return _imageCache.putIfAbsent("$mimeType:$blobName", () {
      List<int> data = (scene.shapes(blobName) as XBlob).blob;
      var blob = Blob([data], mimeType);
      DecodeInfo info = decoderFactory().startDecode(data);
      return _Image(Url.createObjectUrlFromBlob(blob), _ImageSize(info.width, info.height));
    });
  }

  _Image _getCachedFormulaImage(String svgBlobName) => _imageCache.putIfAbsent("formula:$svgBlobName", () {
    List<int> data = (scene.shapes(svgBlobName) as XBlob).blob;
    var blob = Blob([data], "image/svg+xml");
    return _Image(Url.createObjectUrlFromBlob(blob), null);
  });

  void _removeUnusedCachedImages() {
    List<String> unused = [];
    _imageCache.forEach((key, image) {
      if (!image.used) {
        image._release();
        unused.add(key);
      }
    });
    unused.forEach(_imageCache.remove);
  }

  void _setLockedUiCursor() {
    lockedUiCursor = "text";
    _lockedUiCursorSet = true;
  }

  void _unsetLockedUiCursor() {
    if (_lockedUiCursorSet) {
      lockedUiCursor = null;
      _lockedUiCursorSet = false;
    }
  }

  void _onKeyPress(KeyboardEvent e) {
    if (e.charCode < 32) {
      stopFurtherProcessing(e);
    } else {
      _deleteSelection();

      int p = _caretPosition();
      String text = String.fromCharCode(e.charCode);

      performAction(XPadEditor_InsertText()
          ..position = p
          ..text = text, disableUi: false);

      _desiredCaretPosition = p + text.length;
    }
  }

  int _caretPosition() {
    int res;

    Selection s = element.shadowRoot.getSelection();
    if (s.rangeCount == 1) {
      Range r = s.getRangeAt(0);
      res = _computePosition(r.startContainer, r.startOffset);
    } else {
      res = 0;
    }

    return res;
  }

  int _computePosition(Node node, int offset) {
    int res;
    Node previous;

    if (node is Text) {
      res = offset;
      previous = node.previousNode;
    } else if (node == w_root) {
      res = 0;
      previous = (offset > 0 ) ? w_root.nodes[offset - 1] : null;
    } else { // should not happen
      res = 0;
      previous = null;
    }

    while (previous != null) {
      res += _getNodeLength(previous);
      previous = previous.previousNode;
    }

    return res;
  }

  int _getNodeLength(Node node) => (node is Text) ? node.length : 1;

  void _onCompositionStart() {
    Range r = _selectedRange();
    if (r == null) {
      _selectionStartBeforeComposition = _selectionEndBeforeComposition = null;
    } else {
      _selectionStartBeforeComposition = _computePosition(r.startContainer, r.startOffset);
      _selectionEndBeforeComposition = _computePosition(r.endContainer, r.endOffset);
    }
  }

  void _onCompositionEnd(CompositionEvent e) {
    if (_selectionStartBeforeComposition != null) {
      performAction(XPadEditor_Delete()
          ..position = _selectionStartBeforeComposition
          ..length = _selectionEndBeforeComposition - _selectionStartBeforeComposition);
      _selectionStartBeforeComposition = _selectionEndBeforeComposition = null;
    }

    int p = _caretPosition();
    performAction(XPadEditor_InsertText()
        ..position = p
        ..text = e.data);
    _desiredCaretPosition = p + e.data.length;
  }

  void _onKeyDown(KeyboardEvent e) {
    switch (e.keyCode) {
      case KeyCode.ENTER:
        _handleEnter(e);
        break;
      case KeyCode.BACKSPACE:
        _handleBackspace(e);
        break;
      case KeyCode.DELETE:
        _handleDelete(e);
        break;
      case KeyCode.PAGE_DOWN:
        _handlePageDown(e);
        break;
      case KeyCode.PAGE_UP:
        _handlePageUp(e);
        break;
    }
  }

  void _handleEnter(KeyboardEvent e) {
    stopFurtherProcessing(e);
    _deleteSelection();

    int p = _caretPosition();
    performAction(XPadEditor_InsertText()
        ..position = p
        ..text = "\n");

    _desiredCaretPosition = p + 1;
  }

  void _handleBackspace(KeyboardEvent e) {
    stopFurtherProcessing(e);
    if (!_deleteSelection()) {
      int p = _caretPosition();
      if (p > 0) {
        //TODO There may be more than one code unit to delete
        performAction(XPadEditor_Delete()
            ..position = p - 1
            ..length = 1);
        _desiredCaretPosition = p - 1;
      }
    }
  }

  void _handleDelete(KeyboardEvent e) {
    if (getKeyModifiers(e) != KeyModifierState.SHIFT) {
      stopFurtherProcessing(e);
      if (!_deleteSelection()) {
        int p = _caretPosition();
        if (p < _totalLength()) {
          //TODO There may be more than one code unit to delete
          performAction(XPadEditor_Delete()
              ..position = p
              ..length = 1);
          _desiredCaretPosition = p;
        }
      }
    }
  }

  int _totalLength() {
    int res = -1; // adjustment for synthetic trailing <br>
    for (var node in w_root.nodes) {
      res += _getNodeLength(node);
    }
    res = math.max(res, 0);
    return res;
  }

  bool _deleteSelection() {
    Range r = _selectedRange();
    bool res = (r != null);
    if (res) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(XPadEditor_Delete()
          ..position = start
          ..length = end - start);
      _deleteSelectedRange(r, start);
    }
    return res;
  }

  /* nullable */Range _selectedRange() {
    Range res;

    Selection s = element.shadowRoot.getSelection();
    if (s.rangeCount == 1) {
      res = s.getRangeAt(0);
      if (res.collapsed) {
        res = null;
      }
    } else {
      res = null;
    }

    return res;
  }

  void _deleteSelectedRange(Range r, int start) {
      r.deleteContents();
      element.shadowRoot.getSelection().collapseToStart();
      _desiredCaretPosition = start;

      // work around occasional selection/caret rendering artifacts (Chromium bug?)
      w_root.style.backgroundColor = "#00000001";
      window.animationFrame.then((_) => window.animationFrame.then((_) => w_root.style.backgroundColor = ""));
  }

  void _handlePageDown(KeyboardEvent e) {
    if ((getKeyModifiers(e) == KeyModifierState.NONE) && (_selectedRange() == null)
        && (_caretPosition() == _totalLength())) {
      stopFurtherProcessing(e);
    }
  }

  void _handlePageUp(KeyboardEvent e) {
    if ((getKeyModifiers(e) == KeyModifierState.NONE) && (_selectedRange() == null) && (_caretPosition() == 0)) {
      stopFurtherProcessing(e);
    }
  }

  @override void focusComponent() {
    w_root.focus();
  }

  void _onBlur() {
    if ((element.shadowRoot.activeElement != w_root) && (element.shadowRoot.getSelection().anchorNode != null)) {
      element.shadowRoot.getSelection().removeAllRanges();
    }
  }

  void _onCopy(ClipboardEvent e) {
    stopFurtherProcessing(e);
    Range r = _selectedRange();
    if (r != null) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(XPadEditor_Copy()
          ..position = start
          ..length = end - start);
      e.clipboardData
          ..setData(_PAD_MIME_TYPE, "")
          ..setData(_TEXT_MIME_TYPE, _rangeToText(r));
    }
  }

  void _onCut(ClipboardEvent e) {
    stopFurtherProcessing(e);
    Range r = _selectedRange();
    if (r != null) {
      int start = _computePosition(r.startContainer, r.startOffset);
      int end = _computePosition(r.endContainer, r.endOffset);
      performAction(XPadEditor_Cut()
          ..position = start
          ..length = end - start);
      e.clipboardData
          ..setData(_PAD_MIME_TYPE, "")
          ..setData(_TEXT_MIME_TYPE, _rangeToText(r));
      _deleteSelectedRange(r, start);
    }
  }

  void _onPaste(ClipboardEvent e) {
    stopFurtherProcessing(e);

    if (e.clipboardData.types.contains(_PAD_MIME_TYPE)) {
      _deleteSelection();
      int p = _caretPosition();
      performAction(XPadEditor_Paste()
          ..position = p);
      _desiredCaretOffsetFromEnd = _totalLength() - p;
    } else {
      bool imagePasted = false;

      for (int i = 0; i < e.clipboardData.items.length; ++i) {
        if (e.clipboardData.items[i].type == _PNG_MIME_TYPE) {
          lockUi();
          var reader = FileReader();
          reader
              ..onLoad.listen((_) {
                Uint8List data = reader.result as Uint8List;
                if (_supportedImage(data, PngDecoder())) {
                  _deleteSelection();
                  int p = _caretPosition();
                  performAction(XPadEditor_InsertImage()
                      ..position = p
                      ..mimeType = _PNG_MIME_TYPE
                      ..data = data);
                  _desiredCaretPosition = p + 1;
                }
              })
              ..onLoadEnd.listen((_) => unlockUi())
              ..readAsArrayBuffer(e.clipboardData.items[i].getAsFile());
          imagePasted = true;
          break;
        }
      }

      if (!imagePasted) {
        String s = e.clipboardData.getData(_TEXT_MIME_TYPE);
        if (s.isNotEmpty) {
          _deleteSelection();
          int p = _caretPosition();
          performAction(XPadEditor_InsertText()
              ..position = p
              ..text = s);
          _desiredCaretOffsetFromEnd = _totalLength() - p;
        }
      }
    }
  }

  String _rangeToText(Range r) {
    var sb = StringBuffer();
    Node previous = null;
    String spacing = "";

    for (var n in r.cloneContents().nodes) {
      if (n is Text) {
        sb.writeAll([spacing, n.data]);
        spacing = "";
      } else if (n is BRElement) {
        sb.write(os_EOL);
        spacing = "";
      } else if (n is ImageElement) {
        if (previous is Text) {
          spacing = " ";
        }
      }

      previous = n;
    }

    return sb.toString();
  }

  void _onMouseDown(MouseEvent e) {
    if (e.button == 2) {
      if (element.shadowRoot.activeElement == w_root) {
        stopFurtherProcessing(e);
      }
    } else if (e.target is ImageElement) {
      stopFurtherProcessing(e);

      ImageElement image = e.target as ImageElement;
      if (e.offset.x < image.clientWidth / 2) {
        element.shadowRoot.getSelection().collapse(image);
      } else {
        element.shadowRoot.getSelection().collapse(w_root, w_root.nodes.indexOf(image) + 1);
      }
    }
  }

  void _onDoubleClick(Event e) {
    if (e.target is ImageElement) {
      stopFurtherProcessing(e);
      element.shadowRoot.getSelection()
          ..removeAllRanges()
          ..addRange(Range()
              ..selectNode(e.target as Node));
      if (_canEditFormula()) {
        _showFormulaDialog(insertNewFormula: false);
      }
    }
  }

  void _onContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);

    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_insertImageFromFile),
            label: tr("Insert Picture from File..."))))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_saveImageToFile),
            label: tr("Save Picture to File..."),
            enabled: _selectedImageDataset()?.containsKey("mimeType") ?? false)));

    if (_canInsertFormula() || _canEditFormula()) {
      menu.append(MenuItem(MenuItemOptions(
          click: toMenuHandler(_insertOrEditFormula),
          label: _canInsertFormula() ? tr("Insert Formula...") : tr("Edit Formula..."),
          accelerator: _FORMULA_SHORTCUT_NAME)));
    }

    showPopupMenu(menu, MenuPopupOptions());
  }

  void _insertImageFromFile() {
    List<String> fileNames = dialog_showOpenDialogSync(getCurrentWindow(), ShowOpenDialogOptions(
        filters: [
            FileDialogFilter(name: tr("Pictures"), extensions: ["png", "jpeg", "jpg"]),
            FileDialogFilter(name: tr("All Files"), extensions: ["*"])],
        properties: ["openFile"]));
    if (fileNames != null) {
      Uint8List data = null;
      try {
        data = fs_readFileSync(fileNames.first);
      } catch (e) {
        showErrorMessage(e.toString());
      }

      if (data != null) {
        String mimeType = _detectImageMimeType(data);
        if (mimeType == null) {
          showErrorMessage(tr("Unknown image type."));
        } else {
          _deleteSelection();
          int p = _caretPosition();
          performAction(XPadEditor_InsertImage()
              ..position = p
              ..mimeType = mimeType
              ..data = data);
          _desiredCaretPosition = p + 1;
        }
      }
    }
  }

  /* nullable */String _detectImageMimeType(List<int> data) {
    for (String mimeType in _imageDecoders.keys) {
      if (_supportedImage(data, _imageDecoders[mimeType]())) {
        return mimeType;
      }
    }
    return null;
  }

  static bool _supportedImage(List<int> data, Decoder decoder) {
    bool res;
    try {
      DecodeInfo info = decoder.startDecode(data);
      res = (info != null) && (info.width > 0) && (info.height > 0) && (info.numFrames == 1);
    } catch (_) {
      res = false;
    }

    return res;
  }

  void _saveImageToFile() {
    Map<String, String> dataset = _selectedImageDataset();
    if (dataset?.containsKey("mimeType") ?? false) {
      String fileName = showSaveFileDialog(dataset["mimeType"].split("/")[1], tr("Pictures"));
      if (fileName != null) {
        Uint8List data = castOrCopyToUint8List((scene.shapes(dataset["blob"]) as XBlob).blob);
        try {
          writeViaTempFile(Buffer_from(data.buffer, data.offsetInBytes, data.lengthInBytes), fileName);
        } catch (e) {
          showErrorMessage(e.toString());
        }
      }
    }
  }

  /* nullable */Map<String, String> _selectedImageDataset() {
    Map<String, String> res = null;
    Range r = _selectedRange();
    if ((r != null)
        && (_computePosition(r.endContainer, r.endOffset) - _computePosition(r.startContainer, r.startOffset) == 1)) {
      Node node = r.cloneContents().nodes.first;
      if (node is ImageElement) {
        res = node.dataset;
      }
    }
    return res;
  }

  /* nullable */_ImageOrigin _selectedImageOrigin() {
    Map<String, String> dataset = _selectedImageDataset();
    String origin = (dataset == null) ? null : dataset["origin"];
    return (origin == null) ? null : _ImageOrigin.values.firstWhere((v) => v.toString() == origin);
  }

  bool _canInsertFormula() => _selectedRange() == null;

  bool _canEditFormula() => _selectedImageOrigin() == _ImageOrigin.FORMULA;

  void _insertOrEditFormula() {
    if (_canInsertFormula()) {
      _showFormulaDialog(insertNewFormula: true);
    } else if (_canEditFormula()) {
      _showFormulaDialog(insertNewFormula: false);
    }
  }

  Future<void> _showFormulaDialog({@required bool insertNewFormula}) async {
    int p = _caretPosition();

    String source;
    if (insertNewFormula) {
      source = "";
      await performAction(XPadEditor_InsertFormula()
          ..position = p
          ..deleteExistingFormula = false
          ..source = "");
    } else {
      source = XPadEditor_GetFormulaSource_Reply.fromBuffer((await performAction(XPadEditor_GetFormulaSource()
          ..position = p)).output).source;
    }

    source = await FormulaDialog(dispatcher, name, source).openModal();

    if (source.isEmpty) {
      await performAction(XPadEditor_Delete()
          ..position = p
          ..length = 1);
      _desiredCaretPosition = p;
    } else {
      await performAction(XPadEditor_InsertFormula()
          ..position = p
          ..deleteExistingFormula = true
          ..source = source);
      if (insertNewFormula) {
        _desiredCaretPosition = p + 1;
      } else {
        _desiredCaretPosition = p;
        await dispatcher.onChangeScene.first;
        element.shadowRoot.getSelection().modify("extend", "right", "character");
      }
    }
  }
}

class _Image {
  /* nullable */String blobUrl;
  final /* nullable */_ImageSize size;
  bool used = true;

  _Image(this.blobUrl, this.size);

  void _release() {
    if (blobUrl != null) {
      Url.revokeObjectUrl(blobUrl);
      blobUrl = null;
    }
  }
}

class _ImageSize {
  final int width;
  final int height;
  _ImageSize(this.width, this.height);
}

enum _ImageOrigin { FORMULA }

class FormulaDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-formula-dialog";

  static const Duration _PREVIEW_DELAY = Duration(milliseconds: 200);
  static const String _EXAMPLE_FORMULA = r"e=\lim_{n\to\infty}\left(1+\frac{1}{n}\right)^n";

  DialogElement w_dialog;
  TextAreaElement w_source;
  DivElement w_previewPanel;
  AnchorElement w_exampleLink;
  AnchorElement w_helpLink;

  final Dispatcher _dispatcher;
  final String _targetName;
  final String _initialSource;

  /* nullable */Timer _previewDelayTimer;
  _PreviewState _previewState = _PreviewState.IDLE;

  FormulaDialog(this._dispatcher, this._targetName, this._initialSource) : super(TAG);

  Future<String> openModal() async {
    w_source
        ..value = _initialSource
        ..onInput.listen((_) => _onSourceInput());
    _startPreviewDelayTimer(Duration.zero);

    w_exampleLink.onClick.listen(_onExampleLinkClick);
    w_helpLink.onClick.listen(_onHelpLinkClick);

    await open(w_dialog);

    _cancelPreviewDelayTimer();
    return _trimmedSource();
  }

  String _trimmedSource() => w_source.value.trim();

  void _onSourceInput() {
    if (buggyInputEventFor(w_source)) {
      return;
    }
    if (_previewState == _PreviewState.IDLE) {
      _startPreviewDelayTimer(_PREVIEW_DELAY);
    } else {
      _previewState = _PreviewState.REQUEST_OUTDATED;
    }
  }

  void _startPreviewDelayTimer(Duration delay) {
    _cancelPreviewDelayTimer();
    _previewDelayTimer = Timer(delay, _onPreviewDelayTimer);
  }

  void _cancelPreviewDelayTimer() {
    _previewDelayTimer?.cancel();
    _previewDelayTimer = null;
  }

  Future<void> _onPreviewDelayTimer() async {
    _previewDelayTimer = null;
    check(_previewState == _PreviewState.IDLE);

    ImageElement image;
    _previewState = _PreviewState.REQUEST_SENT;
    try {
      ActionResult result = await _dispatcher.performAction(_targetName, XPadEditor_RenderFormula()
          ..source = _trimmedSource(), disableUi: false);
      var reply = XPadEditor_RenderFormula_Reply.fromBuffer(result.output);

      String previewUrl = Url.createObjectUrlFromBlob(Blob([reply.svg], "image/svg+xml"));
      try {
        image = ImageElement(src: previewUrl);
        await StreamGroup.merge([image.onLoad, image.onError]).first;
      } finally {
        Url.revokeObjectUrl(previewUrl);
      }

      applyFormulaStyle(reply.style, image.style);
    } finally {
      if (_previewState == _PreviewState.REQUEST_OUTDATED) {
        image = null;
      }
      _previewState = _PreviewState.IDLE;
    }

    if (w_dialog.open) {
      if (image == null) {
        _startPreviewDelayTimer(_PREVIEW_DELAY);
      } else {
        w_previewPanel.children = [image];
      }
    }
  }

  void _onExampleLinkClick(MouseEvent e) {
    stopFurtherProcessing(e);
    w_source
        ..focus()
        ..setSelectionRange(w_source.value.length, w_source.value.length);
    document.execCommand("insertText", false, (w_source.value.isEmpty ? "" : "\n") + _EXAMPLE_FORMULA);

    // scroll to caret (work around https://bugs.chromium.org/p/chromium/issues/detail?id=331233)
    getCurrentWebContents().sendInputEvent(InputEvent(type: "keyDown", keyCode: "Left"));
    getCurrentWebContents().sendInputEvent(InputEvent(type: "keyUp", keyCode: "Left"));
    getCurrentWebContents().sendInputEvent(InputEvent(type: "keyDown", keyCode: "Right"));
    getCurrentWebContents().sendInputEvent(InputEvent(type: "keyUp", keyCode: "Right"));
  }

  void _onHelpLinkClick(MouseEvent e) {
    stopFurtherProcessing(e);
    shell_openExternal("https://en.wikipedia.org/wiki/Help:Displaying_a_formula#Formatting_using_TeX");
  }
}

enum _PreviewState { IDLE, REQUEST_SENT, REQUEST_OUTDATED }

void initialize() {
  registerShapeParser((b) => XPadEditor.fromBuffer(b));
}
