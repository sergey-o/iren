/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:convert/convert.dart' show hex;
import 'package:meta/meta.dart';

import 'misc_utils.dart';

class LoginInfo {
  final String serverAddress;
  final String keyHex;

  LoginInfo({@required this.serverAddress, @required this.keyHex});
}

class LoginDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-login-dialog";

  DialogElement w_dialog;
  TextInputElement w_server;
  TextInputElement w_password;
  ButtonElement w_logInButton;

  final /* nullable */String _serverAddress;

  LoginDialog({String serverAddress})
      : _serverAddress = serverAddress,
        super(TAG);

  Future<LoginInfo> openModal() async {
    w_dialog.on["cancel"].listen((e) => e.preventDefault());
    w_logInButton.onClick.listen(_onLogInClick);

    if (_serverAddress != null) {
      w_server.value = _serverAddress;
      w_password.autofocus = true;
    }

    await open(w_dialog);

    return LoginInfo(serverAddress: w_server.value.trim(), keyHex: hex.encode(utf8.encode(w_password.value)));
  }

  void _onLogInClick(MouseEvent e) {
    if (!_validServer()) {
      e.preventDefault();
      focusInputElement(w_server);
    }
  }

  bool _validServer() {
    String s = w_server.value.trim();

    Uri uri;
    try {
      uri = Uri.https(s, "");
    } catch (_) {
      uri = null;
    }

    return !s.contains("@") && (uri != null) && uri.host.isNotEmpty && (uri.port >= 1) && (uri.port <= 65535);
  }
}
