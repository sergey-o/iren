/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/misc_types.pb.dart';

import 'embedded_resources.dart';
import 'misc_utils.dart';

export 'package:iren_client/iren_client.dart' show SelectItemEvent;

class SelectorBarWrapper extends WebComponent {
  static const String TAG = "ru-irenproject-selector-bar-wrapper";

  StyleElement w_clientStyle;

  SelectorBar _selectorBar;

  SelectorBarWrapper(List<QuestionDescriptor> questionDescriptors)
      : super(TAG) {
    w_clientStyle.text = _irenCss;

    _selectorBar = SelectorBar(questionDescriptors.length);
    setUpSelectorBar(questionDescriptors, _selectorBar);
    element.shadowRoot.append(_selectorBar.element);
  }

  void layOut() {
    _selectorBar.layOut();
  }

  void setCurrentItem(int value) {
    _selectorBar.setCurrentItem(value);
  }

  Stream<SelectItemEvent> get onSelectItem => _selectorBar.onSelectItem;
}

const String _IREN_CSS_LOCATION = "package:iren_client/iren.css";

@EmbeddedResource(_IREN_CSS_LOCATION)
final String _irenCss = getEmbeddedResource(_IREN_CSS_LOCATION);
