/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/classify_question_editor.pb.dart';
import 'package:iren_proto/question/classify_question.pb.dart';
import 'package:meta/meta.dart';
import 'package:protobuf/protobuf.dart' show GeneratedMessage;
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'pad_editor.dart';
import 'registries.dart';

class ClassifyQuestionEditor extends Widget<XClassifyQuestionEditor> {
  static const String TAG = "ru-irenproject-classify-question-editor";

  DivElement w_toolbar;
  ButtonElement w_addItemButton;
  ButtonElement w_addCategoryButton;
  ButtonElement w_deleteElementButton;
  ButtonElement w_moveElementUpButton;
  ButtonElement w_moveElementDownButton;
  ButtonElement w_optionsButton;
  DivElement w_elementPanel;

  PadEditor _formulation;

  /* nullable */int _selectElement;
  /* nullable */String _selectElementName;
  /* nullable */int _focusedElement;

  ClassifyQuestionEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addItemButton.onClick.listen((_) => _addItem());
    w_addCategoryButton.onClick.listen((_) => _addCategory());
    w_deleteElementButton.onClick.listen((_) => _deleteElement());
    w_moveElementUpButton.onClick.listen((_) => _moveElementUp());
    w_moveElementDownButton.onClick.listen((_) => _moveElementDown());
    w_optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addItem)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addCategory)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteElement);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveElementUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveElementDown);

    _formulation = PadEditor(shape.formulationEditor, dispatcher)
        ..element.id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation.element);

    Element.focusEvent.forTarget(element.shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(element.shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedElement = null;

      w_elementPanel.nodes = shape.element.map((e) => PadEditor(e.editor, dispatcher).element
          ..classes.toggle("category", !e.hasItemIndex()));

      w_addCategoryButton.disabled = !shape.canAddCategory;
      w_optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (var e in w_elementPanel.children) {
      getWidget(e).render();
    }

    if ((_selectElement != null) && shape.element.isNotEmpty) {
      getWidget(w_elementPanel.children[_selectElement.clamp(0, shape.element.length - 1) as int]).focusComponent();
    }
    _selectElement = null;

    if (_selectElementName != null) {
      w_elementPanel.children
          .map(getWidget)
          .firstWhere((w) => w.name == _selectElementName, orElse: () => null)?.focusComponent();
    }
    _selectElementName = null;

    _updateUi();
  }

  void _onFocus() {
    int index = w_elementPanel.children.indexOf(element.shadowRoot.activeElement);
    if (index != -1) {
      _focusedElement = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!w_toolbar.contains((e as FocusEvent).relatedTarget as Node)) {
      _focusedElement = null;
    }
    _updateUi();
  }

  void _updateUi() {
    w_addItemButton.disabled = !_canAddItem();
    w_deleteElementButton.disabled = (_focusedElement == null);
    w_moveElementUpButton.disabled = !_canMoveElementUp();
    w_moveElementDownButton.disabled = !_canMoveElementDown();
  }

  void _addItem() {
    if (_canAddItem()) {
      performAction(XClassifyQuestionEditor_AddItem()
          ..categoryIndex = shape.element[_focusedElement].categoryIndex);

      _selectElement = _focusedElement;
      do {
        ++_selectElement;
      } while ((_selectElement < shape.element.length) && shape.element[_selectElement].hasItemIndex());
    }
  }

  bool _canAddItem() => shape.canAddItem && (_focusedElement != null);

  void _addCategory() {
    if (shape.canAddCategory) {
      performAction(XClassifyQuestionEditor_AddCategory());
      _selectElement = shape.element.length;
    }
  }

  void _deleteElement() {
    if (_focusedElement != null) {
      ClassifyElement e = shape.element[_focusedElement];
      GeneratedMessage action = e.hasItemIndex() ?
          (XClassifyQuestionEditor_DeleteItem()
              ..categoryIndex = e.categoryIndex
              ..itemIndex = e.itemIndex) :
          (XClassifyQuestionEditor_DeleteCategory()
              ..index = e.categoryIndex);
      performAction(action);

      bool deletedItemInFrontOfNextCategory = e.hasItemIndex() && (_focusedElement < shape.element.length - 1)
          && !shape.element[_focusedElement + 1].hasItemIndex();
      _selectElement = deletedItemInFrontOfNextCategory ? (_focusedElement - 1) : _focusedElement;
    }
  }

  void _moveElementUp() {
    if (_canMoveElementUp()) {
      _moveElement(forward: false);
    }
  }

  bool _canMoveElementUp() => (_focusedElement != null) && shape.element[_focusedElement].canMoveUp;

  void _moveElementDown() {
    if (_canMoveElementDown()) {
      _moveElement(forward: true);
    }
  }

  bool _canMoveElementDown() => (_focusedElement != null) && shape.element[_focusedElement].canMoveDown;

  void _moveElement({@required bool forward}) {
    ClassifyElement e = shape.element[_focusedElement];
    GeneratedMessage action = e.hasItemIndex() ?
        (XClassifyQuestionEditor_MoveItem()
            ..categoryIndex = e.categoryIndex
            ..itemIndex = e.itemIndex
            ..forward = forward) :
        (XClassifyQuestionEditor_MoveCategory()
            ..index = e.categoryIndex
            ..forward = forward);
    performAction(action);

    _selectElementName = e.editor;
  }

  void _showOptionsDialog() {
    ClassifyQuestionOptionsDialog(shape, dispatcher, name).openModal();
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

class ClassifyQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-classify-question-options-dialog";

  DialogElement w_dialog;
  RadioButtonInputElement w_randomItems;
  NumberInputElement w_itemLimit;
  NumberInputElement w_minItemsPerCategory;

  final XClassifyQuestionEditor _source;
  final Dispatcher _dispatcher;
  final String _targetName;

  ClassifyQuestionOptionsDialog(this._source, this._dispatcher, this._targetName) : super(TAG);

  Future<void> openModal() async {
    w_itemLimit.max = w_minItemsPerCategory.max = _source.maxItems.toString();

    if (_source.hasItemLimit()) {
      w_randomItems.checked = true;
      w_itemLimit.valueAsNumber = _source.itemLimit;
      w_minItemsPerCategory.valueAsNumber = _source.minItemsPerCategory;
    }

    w_randomItems.onClick.listen((_) => focusInputElement(w_itemLimit));

    _updateUi();

    w_dialog
        ..onChange.listen((_) => _updateUi())
        ..onClick.listen(_onDialogClick);

    await open(w_dialog);

    if (w_dialog.returnValue.isNotEmpty) {
      var action = XClassifyQuestionEditor_SetOptions();
      if (w_randomItems.checked) {
        action
            ..itemLimit = w_itemLimit.valueAsNumber.toInt()
            ..minItemsPerCategory = w_minItemsPerCategory.valueAsNumber.toInt();
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    w_itemLimit.required = w_minItemsPerCategory.required = w_randomItems.checked;
    w_itemLimit.disabled = w_minItemsPerCategory.disabled = !w_randomItems.checked;
  }

  void _onDialogClick(MouseEvent e) {
    if ((e.target == w_itemLimit) && w_itemLimit.disabled) {
      w_randomItems.dispatchEvent(MouseEvent("click"));
    }
  }
}

const String _LIB = "classify_question_editor";

void initialize() {
  registerShapeParser((b) => XClassifyQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(QuestionDescriptor(
      type: ClassifyQuestionType.classify.name,
      title: translate(_LIB, "Classification Question") as String,
      priority: 5000,
      icon: "classify_question_editor.resources/classifyQuestion.png",
      initialText: translate(_LIB, "Classify the items.") as String));

  registerQuestionEditorFactory(
      XClassifyQuestionEditor,
      (name, dispatcher) => ClassifyQuestionEditor(name, dispatcher));
}
