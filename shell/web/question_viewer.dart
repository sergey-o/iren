/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/dialog.pb.dart';
import 'package:iren_proto/editor/editor.pb.dart';

import 'editor_infra.dart';
import 'embedded_resources.dart';
import 'misc_utils.dart';

class QuestionViewer extends Widget<XQuestionViewer> {
  static const String TAG = "ru-irenproject-question-viewer";

  DivElement w_contentPanel;
  StyleElement w_clientStyle;

  /* nullable */DialogWidget _dialog;

  bool _canShowCorrectAnswer;
  bool get canShowCorrectAnswer => _canShowCorrectAnswer;

  bool _canResetAnswer;
  bool get canResetAnswer => _canResetAnswer;

  ResizeObserver _resizeObserver;

  QuestionViewer(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_clientStyle.text = _irenCss;
    _resizeObserver = ResizeObserver((_, __) => _layOutDialogIfShown())
        ..observe(w_contentPanel);
  }

  void _layOutDialogIfShown() {
    if (element.offsetParent != null) {
      _dialog?.layOut();
    }
  }

  @override void detached() {
    _resizeObserver.disconnect();
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      if (shape.hasContent()) {
        _showDialog(_content().emptyResponse);
      } else {
        _dialog = null;
        w_contentPanel.nodes = [DivElement()
            ..id = "errorMessage"
            ..text = shape.errorMessage];
      }

      _canShowCorrectAnswer = shape.hasContent();
      _canResetAnswer = false;

      w_contentPanel.scrollTop = 0;
    }
  }

  XQuestionViewerContent _content() => scene.shapes(shape.content);

  void _showDialog(DialogResponse response) {
    _dialog = DialogWidget(_content().dialog, response)
        ..onResponse.listen(_onDialogResponse);
    w_contentPanel.nodes = [_dialog.element];
    _dialog.layOut();
  }

  void _onDialogResponse(DialogEvent e) {
    if (identical(e.source, _dialog)) {
      _canShowCorrectAnswer = _canResetAnswer = true;
      _evaluateResponse();
    }
  }

  void _evaluateResponse() {
    dispatcher.performAction(shape.content, XQuestionViewerContent_Evaluate()
        ..response = _dialog.response, disableUi: false);
  }

  void showCorrectAnswer() {
    if (_canShowCorrectAnswer) {
      _showDialog(_content().correctResponse);
      _canShowCorrectAnswer = false;
      _canResetAnswer = true;
      _evaluateResponse();
    }
  }

  void resetAnswer() {
    if (_canResetAnswer) {
      _showDialog(_content().emptyResponse);
      _canShowCorrectAnswer = true;
      _canResetAnswer = false;
      _evaluateResponse();
    }
  }

  void offerAnew() {
    performAction(XQuestionViewer_Issue());
  }

  String get dashboard => shape.dashboard;
}

class QuestionViewerDashboard extends Widget<XQuestionViewerDashboard> {
  static const String TAG = "ru-irenproject-question-viewer-dashboard";

  SpanElement w_result;
  ButtonElement w_showCorrectAnswerButton;
  ButtonElement w_resetAnswerButton;
  ButtonElement w_offerAnewButton;

  final QuestionViewer _questionViewer;

  QuestionViewerDashboard(this._questionViewer)
      : super(TAG, _questionViewer.dashboard, _questionViewer.dispatcher) {
    w_showCorrectAnswerButton.onClick.listen((_) => _questionViewer.showCorrectAnswer());
    w_resetAnswerButton.onClick.listen((_) => _questionViewer.resetAnswer());
    w_offerAnewButton.onClick.listen((_) => _questionViewer.offerAnew());

    windowShortcutHandler.add(KeyCode.F9, KeyModifierState.NONE, () => _questionViewer.offerAnew());
  }

  @override void render() {
    super.render();

    w_showCorrectAnswerButton.disabled = !_questionViewer.canShowCorrectAnswer;
    w_resetAnswerButton.disabled = !_questionViewer.canResetAnswer;

    if (shapeChanged) {
      w_result.text = shape.hasResultPercent() ? "${shape.resultPercent}%" : "";

      if (!shape.hasResultPercent()) {
        w_result.classes.clear();
      } else if (shape.resultPercent == 0) {
        w_result.className = "incorrect";
      } else {
        w_result.className = (shape.resultPercent == 100) ? "correct" : "partiallyCorrect";
      }
    }
  }
}

const String _IREN_CSS_LOCATION = "package:iren_client/iren.css";

@EmbeddedResource(_IREN_CSS_LOCATION)
final String _irenCss = getEmbeddedResource(_IREN_CSS_LOCATION);

void initialize() {
  registerShapeParsers([
      (b) => XQuestionViewer.fromBuffer(b),
      (b) => XQuestionViewerContent.fromBuffer(b),
      (b) => XQuestionViewerDashboard.fromBuffer(b)]);
}
