/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:js';

import 'package:convert/convert.dart' show hex;
import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/login.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/work_controller.pb.dart';
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

import 'common_dialogs.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'login_dialog.dart';
import 'misc_utils.dart';
import 'supervisor_screen.dart';
import 'watch_screen.dart';

const String _LIB = "iren_master";

final String _recentConnectionFile = path_join(dataDirectory, "recentConnection");

void runMaster() {
  document.title = _tr("Iren");

  ipcRenderer_once("connectionConfig", allowInterop(
      (_, [dynamic connectionConfig]) => _onConnectionConfigReady(connectionConfig as String)));
  ipcRenderer_send("getConnectionConfig");
}

String _tr(String key) => _trD(key) as String;

dynamic _trD(String key) => translate(_LIB, key);

Future<void> _onConnectionConfigReady(String connectionConfig) async {
  try {
    Connection connection = null;

    ipcRenderer_on("requestClose", allowInterop((_, [__]) {
      bool cancelClose = (connection != null) && connection.connected && (modalDialogOpen || (uiLocked && !uiDimmed));
      if (!cancelClose) {
        getCurrentWindow().destroy();
      }
    }));
    ipcRenderer_sendSync("relayCloseRequests");

    dynamic config = jsonDecode(connectionConfig);
    bool remoteServer = (config == null);

    LoginInfo loginInfo;
    if (remoteServer) {
      dynamic saved = jsonDecode(ipcRenderer_sendSync("getSavedLoginInfo") as String);
      loginInfo = (saved["keyHex"] == null) ?
          await LoginDialog(serverAddress: (saved["serverAddress"] as String) ?? _recentServerAddress()).openModal() :
          LoginInfo(serverAddress: saved["serverAddress"] as String, keyHex: saved["keyHex"] as String);
      ipcRenderer_sendSync("saveLoginInfo", jsonEncode({
          "serverAddress": loginInfo.serverAddress}));
    }

    connection = await _connect(
        serverAddressIfRemote: remoteServer ? loginInfo.serverAddress : null,
        serverPortIfLocal: remoteServer ? null : config["port"] as int);

    Stream<ServerMessage> bootstrapMessageStream = connection.tune(Int64(
        ProtocolConstants.BOOTSTRAP_CHANNEL_ID.value));

    if (_serverVersionCompatible(await bootstrapMessageStream.first)) {
      BootstrapNotice m = (await bootstrapMessageStream.first).bootstrapNotice;
      check(m.hasServerHello());
      check(m.serverHello.hasLoginChannelId());

      Supervise_Reply reply = (await _performLoginRequest(connection, m.serverHello.loginChannelId, LoginRequest()
          ..supervise = (Supervise()
              ..supervisorKey = hex.decode(remoteServer ?
                  loginInfo.keyHex :
                  config["supervisorKey"] as String)))).superviseReply;

      if (reply.hasOk()) {
        if (remoteServer) {
          document.title = _trD("WINDOW_TITLE")(loginInfo.serverAddress) as String;
          ipcRenderer_sendSync("saveLoginInfo", jsonEncode({
              "serverAddress": loginInfo.serverAddress,
              "keyHex": loginInfo.keyHex}));
        }

        await _displaySupervisorUi(reply.ok, connection, remoteServer);
      } else {
        switch (reply.failure.reason) {
          case Supervise_Reply_Failure_Reason.INCORRECT_KEY: throw _tr("The password is incorrect.");
          case Supervise_Reply_Failure_Reason.TOO_MANY_FAILURES: throw _tr(
              "Logging in is temporarily disabled due to many failed attempts.");
        }
        throw false;
      }
    }
  } catch (e) {
    showErrorMessage(e.toString());
    window.location.reload();
  }
}

Future<Connection> _connect({String serverAddressIfRemote, int serverPortIfLocal}) async {
  bool remote = (serverAddressIfRemote != null);

  String server = remote ? "wss://$serverAddressIfRemote" : "ws://127.0.0.1:$serverPortIfLocal";
  String url = "$server/websocket/";

  if (remote) {
    ipcRenderer_sendSync("setCertificateDialog", jsonEncode({
        "serverAddress": serverAddressIfRemote,
        "url": normalizeUri(url),
        "connectMessage": _tr("Connect"),
        "cancelMessage": _tr("Cancel"),
        "newServerMessage": _tr("You are connecting to this server for the first time."),
        "addressMessage": _tr("Server address:"),
        "idMessage": _tr("Server identifier:"),
        "verifyIdMessage": _tr(
            "For security reasons, it is recommended that you verify the identifier"
            ' before clicking "Connect" by contacting the server administrator.'),
        "idChangedMessage": _tr("THE IDENTIFIER OF THIS SERVER HAS CHANGED!")}));
  }

  var socket = WebSocket(url)
      ..binaryType = "arraybuffer"
      ..onClose.listen((_) => ConnectionLostDialog().openModal());

  await socket.onOpen.first;

  if (remote) {
    _saveRecentServerAddress(serverAddressIfRemote);
  }

  return Connection(WebSocketTransport(socket));
}

Future<LoginReply> _performLoginRequest(Connection connection, Int64 channelId, LoginRequest request) async =>
    (await connection.perform(ClientMessage()
        ..channelId = channelId
        ..loginRequest = request)).loginReply;

/* nullable */String _recentServerAddress() {
  Object m;
  try {
    m = jsonDecode(fs_readFileSync_String(_recentConnectionFile, "utf8"));
  } catch (_) {
    m = null;
  }
  Object server = (m is Map) ? m["server"] : null;
  return (server is String) ? server : null;
}

void _saveRecentServerAddress(String serverAddress) {
  try {
    writeViaTempFile(jsonEncode({"server": serverAddress}), _recentConnectionFile);
  } catch (_) {}
}

bool _serverVersionCompatible(ServerMessage m) {
  check(m.hasServerVersion(), () => "Cannot determine server version.");
  ServerVersion sv = m.serverVersion;

  String requiredVersion;
  bool res;

  if (sv.epoch == 1) {
    check(sv.hasVersion() && sv.version.contains(RegExp(r"^[a-z0-9.+-]{1,50}$")),
        () => "Illegal server version format.");
    requiredVersion = sv.version;
    res = (programVersion == requiredVersion);
  } else {
    requiredVersion = null;
    res = false;
  }

  if (!res) {
    if (showMessageBox(ShowMessageBoxOptions(
        type: "info",
        buttons: [_tr("Go to Download Page"), _tr("Cancel")],
        message: (requiredVersion == null) ?
            _tr("A newer version of the program is required for working with this server.") :
            _trD("DIFFERENT_VERSION_REQUIRED")(requiredVersion) as String,
        cancelId: 1)) == 0) {
      String fragment = (requiredVersion == null) ? "" : "#iren-$requiredVersion";
      promiseToFuture(shell_openExternal("https://irenproject.bitbucket.io/get/$fragment"))
          .whenComplete(getCurrentWindow().destroy);
    } else {
      window.location.reload();
    }
  }

  return res;
}

Future<void> _displaySupervisorUi(Supervise_Reply_Ok m, Connection connection, bool remoteServer) async {
  String selectedWorkId = null;

  while (true) {
    var supervisorScreen = SupervisorScreen(m, connection, remoteServer, selectedWorkId);

    document.body.children.insert(0, supervisorScreen.element);
    selectedWorkId = await supervisorScreen.activatedWorkId;
    supervisorScreen.element.remove();

    Int64 watcherNoticeChannelId = connection.openClientChannel();

    Int64 requestId = connection.send(ClientMessage()
        ..channelId = m.workControllerChannelId
        ..workControllerRequest = (WorkControllerRequest()
            ..watchWork = (WatchWork()
                ..workId = selectedWorkId
                ..watcherNoticeChannelId = watcherNoticeChannelId
                ..language = window.navigator.language)));
    WatchWork_Reply watchWorkReply = (await connection.tune(requestId).first).workControllerReply.watchWorkReply;

    var watchScreen = WatchScreen(
        connection,
        watchWorkReply.watcherChannelId,
        connection.tune(watcherNoticeChannelId).map((m) => m.watcherNotice),
        remoteServer,
        m.workControllerChannelId,
        selectedWorkId);

    document.body.children.insert(0, watchScreen.element);
    Future<ServerMessage> watcherChannelClosedNotice = connection.tune(requestId).first;
    await watcherChannelClosedNotice;

    if (!watchScreen.closedExplicitly) {
      selectedWorkId = null;
    }
    watchScreen.element.remove();
  }
}
