/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:fixnum/fixnum.dart';
import 'package:iren_proto/editor/profile_screen.pb.dart';
import 'package:iren_proto/profile.pb.dart';
import 'package:web_helpers/web_helpers.dart';

import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'modifier_screen.dart';

class ProfileScreen extends Widget<XProfileScreen> {
  static const String TAG = "ru-irenproject-profile-screen";

  DivElement w_profilePanel;
  DivElement w_bottomPanel;
  ButtonElement w_closeButton;

  ProfileListEditor _profileListEditor;
  /* nullable */ProfileEditor _profileEditor;

  ProfileScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    _profileListEditor = ProfileListEditor(shape.profileListEditor, dispatcher)
        ..element.id = "list";
    findElement("listPlaceholder").replaceWith(_profileListEditor.element);

    if (shape.allowClose) {
      w_closeButton.onClick.listen((_) => _close());
      windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);
    } else {
      w_bottomPanel.style.display = "none";
    }
  }

  @override void render() {
    super.render();
    _profileListEditor.render();

    if (shape.hasProfileEditor()) {
      if (shape.profileEditor != _profileEditor?.name) {
        _profileEditor = ProfileEditor(shape.profileEditor, dispatcher);
        w_profilePanel.nodes = [_profileEditor.element];
      }
      _profileEditor.render();
    } else {
      _profileEditor?.element?.remove();
      _profileEditor = null;
    }
  }

  void _close() {
    performAction(XProfileScreen_Close());
  }
}

class ProfileListEditor extends Widget<XProfileListEditor> {
  static const String TAG = "ru-irenproject-profile-list-editor";

  DivElement w_toolbar;
  ButtonElement w_addButton;
  ButtonElement w_deleteButton;
  ButtonElement w_moveUpButton;
  ButtonElement w_moveDownButton;
  DivElement w_scrollable;
  DivElement w_main;

  ProfileListEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    if (shape.allowEdit) {
      w_addButton.onClick.listen((_) => _add());
      w_deleteButton.onClick.listen((_) => _delete());
      w_moveUpButton.onClick.listen((_) => _moveUp());
      w_moveDownButton.onClick.listen((_) => _moveDown());

      windowShortcutHandler.add(KeyCode.F2, KeyModifierState.NONE, _add);

      ShortcutHandler(element.shadowRoot)
          ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
          ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
          ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);

      w_scrollable.onContextMenu.listen(_onContextMenu);
    } else {
      w_toolbar.style.display = "none";
    }
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      w_main.nodes.clear();
      for (String title in shape.title) {
        w_main.append(DivElement()
            ..text = title
            ..dataset["untitled"] = tr("untitled")
            ..title = title
            ..onMouseDown.listen(_onProfileMouseDown));
      }

      if (shape.hasSelectedIndex()) {
        w_main.children[shape.selectedIndex].classes.add("selected");
        scheduleScrollIntoView(w_main.children[shape.selectedIndex]);
      }

      w_deleteButton.disabled = !shape.hasSelectedIndex();
      w_moveUpButton.disabled = !_canMoveUp();
      w_moveDownButton.disabled = !_canMoveDown();
    }
  }

  Future<void> _onProfileMouseDown(MouseEvent e) async {
    DivElement row = e.currentTarget as DivElement;
    if ((e.button == 0) || ((e.button == 2) && !row.classes.contains("selected"))) {
      await performAction(XProfileListEditor_Select()
          ..index = w_main.children.indexOf(row));
      await dispatcher.onChangeScene.first;

      if ((e.button == 2) && shape.allowEdit) {
        _showContextMenu();
      }
    }
  }

  void _add() {
    performAction(XProfileListEditor_Add());
  }

  void _delete() {
    if (shape.hasSelectedIndex()) {
      performAction(XProfileListEditor_Delete());
    }
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(XProfileListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => shape.hasSelectedIndex() && (shape.selectedIndex > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(XProfileListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => shape.hasSelectedIndex() && (shape.selectedIndex < shape.title.length - 1);

  void _onContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);
    _showContextMenu();
  }

  void _showContextMenu() {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_add),
            label: tr("Add"),
            icon: getAbsoluteAppPath("profile_screen.resources/add.png"),
            accelerator: "F2")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_delete),
            label: tr("Delete"),
            icon: getAbsoluteAppPath("profile_screen.resources/delete.png"),
            enabled: !w_deleteButton.disabled,
            accelerator: "Delete")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveUp),
            label: tr("Move Up"),
            icon: getAbsoluteAppPath("profile_screen.resources/moveUp.png"),
            enabled: !w_moveUpButton.disabled,
            accelerator: "Alt+Up")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveDown),
            label: tr("Move Down"),
            icon: getAbsoluteAppPath("profile_screen.resources/moveDown.png"),
            enabled: !w_moveDownButton.disabled,
            accelerator: "Alt+Down")));

    showPopupMenu(menu, MenuPopupOptions());
  }
}

class ProfileEditor extends Widget<XProfileEditor> {
  static const String TAG = "ru-irenproject-profile-editor";

  LabelElement w_titleLabel;
  TextInputElement w_title;
  FormElement w_options;
  RadioButtonInputElement w_selectAll;
  RadioButtonInputElement w_selectRandom;
  RadioButtonInputElement w_selectCustom;
  NumberInputElement w_questionsPerSection;
  DivElement w_sectionPanel;
  DivElement w_sectionGrid;
  CheckboxInputElement w_useLabelFilter;
  TextInputElement w_labelFilter;
  CheckboxInputElement w_shuffleQuestions;
  RadioButtonInputElement w_unlimitedTime;
  RadioButtonInputElement w_limitedTime;
  NumberInputElement w_timeLimit;
  CheckboxInputElement w_editableAnswers;
  CheckboxInputElement w_instantAnswerCorrectness;
  CheckboxInputElement w_instantTotalPercentCorrect;
  CheckboxInputElement w_browsableQuestions;
  CheckboxInputElement w_weightCues;
  FormElement w_score;
  CheckboxInputElement w_showScore;
  CheckboxInputElement w_percentCorrect;
  CheckboxInputElement w_mark;
  CheckboxInputElement w_points;
  CheckboxInputElement w_showQuestionScore;
  CheckboxInputElement w_questionPercentCorrect;
  CheckboxInputElement w_questionCorrectAnswer;
  CheckboxInputElement w_questionPoints;
  CheckboxInputElement w_showSectionScore;
  CheckboxInputElement w_sectionPercentCorrect;
  CheckboxInputElement w_sectionPoints;
  CheckboxInputElement w_sectionQuestionCount;
  CheckboxInputElement w_sectionQuestionList;
  TableElement w_markScale;
  ButtonElement w_addMarkButton;

  bool _forceRenderOptions = false;
  bool _forceRenderSectionTree = false;

  ModifierListEditor _modifierListEditor;
  /* nullable */String _renderedSectionTreeName;

  ProfileEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    Element.submitEvent.forTarget(element.shadowRoot).listen((e) => e.preventDefault());

    if (shape.allowEditTitle) {
      w_title.onInput.listen((_) => _onTitleInput());
    } else {
      w_titleLabel.style.display = "none";
    }

    w_options
        ..onInput.listen(_onOptionsInput)
        ..onClick.listen(_onOptionsClick);

    w_selectRandom.onClick.listen((_) => focusInputElement(w_questionsPerSection));
    trackInputActivity(w_questionsPerSection);
    w_questionsPerSection.onBlur.listen((_) => _handleOptionsFieldBlur());

    w_limitedTime.onClick.listen((_) => focusInputElement(w_timeLimit));
    trackInputActivity(w_timeLimit);
    w_timeLimit.onBlur.listen((_) => _handleOptionsFieldBlur());

    _modifierListEditor = ModifierListEditor(shape.modifierListEditor, dispatcher);
    findElement("modifierListEditorPlaceholder").replaceWith(_modifierListEditor.element);

    w_score.onChange.listen((_) => _onScoreChange());
    w_showScore.onClick.listen((_) => _onShowScoreClick());
    w_showQuestionScore.onClick.listen((_) => _onShowQuestionScoreClick());
    w_showSectionScore.onClick.listen((_) => _onShowSectionScoreClick());

    w_addMarkButton.onClick.listen((_) => _addMark());
  }

  @override void render() {
    super.render();

    if (shapeChanged) {
      updateTextInput(w_title, shape.title, collapseSpaces);
    }
    if (shapeChanged || _forceRenderOptions) {
      _renderOptions();
    }
    if (shapeChanged || _forceRenderSectionTree) {
      _renderSectionTree();
    }
    if (shapeChanged) {
      _renderScore();
      _renderMarkScale();
    }

    _modifierListEditor
        ..render()
        ..autoScrollToSelection = true;

    _forceRenderOptions = _forceRenderSectionTree = false;
  }

  void _onTitleInput() {
    if (buggyInputEventFor(w_title)) {
      return;
    }
    performAction(XProfileEditor_SetTitle()
        ..title = collapseSpaces(w_title.value), disableUi: false);
  }

  void _renderOptions() {
    if (shape.hasSectionTree()) {
      w_selectCustom.checked = true;
    } else if (shape.options.hasQuestionsPerSection()) {
      w_selectRandom.checked = true;
      updateInactiveNumberInput(w_questionsPerSection, shape.options.questionsPerSection);
    } else {
      w_selectAll.checked = true;
    }

    w_useLabelFilter.checked = shape.options.hasLabelFilter();
    if (shape.options.hasLabelFilter()) {
      updateTextInput(w_labelFilter, shape.options.labelFilter);
    }

    w_shuffleQuestions.checked = shape.options.shuffleQuestions;

    if (shape.options.hasDurationMinutes()) {
      w_limitedTime.checked = true;
      updateInactiveNumberInput(w_timeLimit, shape.options.durationMinutes);
    } else {
      w_unlimitedTime.checked = true;
    }

    w_editableAnswers.checked = shape.options.editableAnswers;
    w_instantAnswerCorrectness.checked = shape.options.instantAnswerCorrectness;
    w_instantTotalPercentCorrect.checked = shape.options.instantTotalPercentCorrect;
    w_browsableQuestions.checked = shape.options.browsableQuestions;
    w_weightCues.checked = shape.options.weightCues;

    _updateOptionsUi();
  }

  void _updateOptionsUi() {
    w_questionsPerSection
        ..required = w_selectRandom.checked
        ..disabled = !w_selectRandom.checked;
    w_useLabelFilter.disabled = !w_useLabelFilter.checked; //TODO
    w_timeLimit
        ..required = w_limitedTime.checked
        ..disabled = !w_limitedTime.checked;
    w_instantAnswerCorrectness.disabled = w_instantTotalPercentCorrect.disabled = w_editableAnswers.checked;
  }

  void _handleOptionsFieldBlur() {
    if (dispatcher.waitingForReply) {
      dispatcher.dimUiIfWaitingForReply();
      _forceRenderOptions = true;
    } else {
      _renderOptions();
    }
  }

  void _onOptionsInput(Event e) {
    if (buggyInputEventFor(e.target)) {
      return;
    }

    _updateOptionsUi();
    if (w_options.checkValidity()) {
      var options = ProfileOptions();

      if (w_selectRandom.checked) {
        options.questionsPerSection = w_questionsPerSection.valueAsNumber.toInt();
      }

      if (w_useLabelFilter.checked) {
        options.labelFilter = w_labelFilter.value;
      }

      options.shuffleQuestions = w_shuffleQuestions.checked;

      if (w_limitedTime.checked) {
        options.durationMinutes = w_timeLimit.valueAsNumber.toInt();
      }

      options
          ..editableAnswers = w_editableAnswers.checked
          ..instantAnswerCorrectness = !w_editableAnswers.checked && w_instantAnswerCorrectness.checked
          ..instantTotalPercentCorrect = !w_editableAnswers.checked && w_instantTotalPercentCorrect.checked
          ..browsableQuestions = w_browsableQuestions.checked
          ..weightCues = w_weightCues.checked;

      performAction(XProfileEditor_SetOptions()
          ..options = options
          ..useSectionProfile = w_selectCustom.checked, disableUi: !textlikeInputElement(e.target));
    }
  }

  void _onOptionsClick(MouseEvent e) {
    if ((e.target == w_questionsPerSection) && w_questionsPerSection.disabled) {
      w_selectRandom.dispatchEvent(MouseEvent("click"));
    } else if ((e.target == w_timeLimit) && w_timeLimit.disabled) {
      w_limitedTime.dispatchEvent(MouseEvent("click"));
    }
  }

  void _renderScore() {
    w_showScore.checked = shape.hasScore();
    w_percentCorrect.checked = shape.hasScore() && shape.score.percentCorrect;
    w_mark.checked = shape.hasScore() && shape.score.mark;
    w_points.checked = shape.hasScore() && shape.score.points;

    AvailableQuestionScore q = (shape.hasScore() && shape.score.hasForQuestions()) ? shape.score.forQuestions : null;
    w_showQuestionScore.checked = (q != null);
    w_questionPercentCorrect.checked = q?.percentCorrect ?? false;
    w_questionCorrectAnswer.checked = q?.correctAnswer ?? false;
    w_questionPoints.checked = q?.points ?? false;

    AvailableSectionScore s = (shape.hasScore() && shape.score.hasForSections()) ? shape.score.forSections : null;
    w_showSectionScore.checked = (s != null);
    w_sectionPercentCorrect.checked = s?.percentCorrect ?? false;
    w_sectionPoints.checked = s?.points ?? false;
    w_sectionQuestionCount.checked = s?.questionCount ?? false;
    w_sectionQuestionList.checked = s?.questionList ?? false;

    _updateScoreUi();
  }

  void _updateScoreUi() {
    w_percentCorrect.disabled = w_mark.disabled = w_points.disabled = w_showQuestionScore.disabled =
        w_showSectionScore.disabled = !w_showScore.checked;
    w_questionPercentCorrect.disabled = w_questionCorrectAnswer.disabled = w_questionPoints.disabled =
        !w_showQuestionScore.checked;
    w_sectionPercentCorrect.disabled = w_sectionPoints.disabled = w_sectionQuestionCount.disabled =
        !w_showSectionScore.checked;
    w_sectionQuestionList.disabled = !(w_showQuestionScore.checked && w_showSectionScore.checked);
  }

  void _onScoreChange() {
    var action = XProfileEditor_SetScore();
    if (w_showScore.checked) {
      var score = AvailableScore()
          ..percentCorrect = w_percentCorrect.checked
          ..mark = w_mark.checked
          ..points = w_points.checked;
      if (w_showQuestionScore.checked) {
        score.forQuestions = AvailableQuestionScore()
            ..percentCorrect = w_questionPercentCorrect.checked
            ..correctAnswer = w_questionCorrectAnswer.checked
            ..points = w_questionPoints.checked;
      }
      if (w_showSectionScore.checked) {
        score.forSections = AvailableSectionScore()
            ..percentCorrect = w_sectionPercentCorrect.checked
            ..points = w_sectionPoints.checked
            ..questionCount = w_sectionQuestionCount.checked
            ..questionList = w_sectionQuestionList.checked && w_showQuestionScore.checked;
      }
      action.score = score;
    }
    performAction(action);
  }

  void _onShowScoreClick() {
    if (w_showScore.checked) {
      w_percentCorrect.checked = w_mark.checked = true;
    }
  }

  void _onShowQuestionScoreClick() {
    if (w_showQuestionScore.checked) {
      w_questionPercentCorrect.checked = true;
    }
  }

  void _onShowSectionScoreClick() {
    if (w_showSectionScore.checked) {
      w_sectionPercentCorrect.checked = true;
    }
  }

  void _renderMarkScale() {
    TableSectionElement tbody = w_markScale.tBodies.first
        ..nodes.clear();
    for (var mark in shape.markScale.mark) {
      tbody.addRow()
          ..addCell().text = _normalizedDecimalToUiPercent(mark.lowerBound)
          ..addCell().text = mark.title
          ..onClick.listen(_onMarkRowClick);
    }
    w_addMarkButton.disabled = (shape.markScale.mark.length == shape.maxMarks);
  }

  Future<void> _addMark() async {
    if (await MarkDialog(dispatcher, name, index: shape.markScale.mark.isEmpty ? 0 : null).openModal()) {
      scheduleScrollIntoView(w_addMarkButton);
      w_addMarkButton.focus();
    }
  }

  void _onMarkRowClick(MouseEvent e) {
    int index = (e.currentTarget as TableRowElement).sectionRowIndex;
    MarkDialog(dispatcher, name, mark: shape.markScale.mark[index], index: index).openModal();
  }

  void _renderSectionTree() {
    if (shape.hasSectionTree()) {
      SectionNode rootNode = (scene.shapes(shape.sectionTree) as XSectionTree).root;

      if (_renderedSectionTreeName != shape.sectionTree) {
        _renderedSectionTreeName = shape.sectionTree;
        w_sectionGrid.nodes.clear();
        _renderSectionNode(rootNode, 0);
        w_sectionPanel.style.display = "";
      }

      _renderSectionQuestionCounts(rootNode, shape.sectionQuestionCount.iterator,
          w_sectionGrid.querySelectorAll<TextInputElement>(".sectionQuestionCount").iterator, "0");
    } else {
      _renderedSectionTreeName = null;
      w_sectionPanel.style.display = "none";
    }
  }

  void _renderSectionNode(SectionNode node, int level) {
    w_sectionGrid
        ..append(SpanElement()
            ..classes.add("sectionName")
            ..text = getPrintableSectionName(node.name)
            ..style.marginLeft = "${level * 2}em")
        ..append(trackInputActivity<TextInputElement>(TextInputElement())
            ..classes.add("sectionQuestionCount")
            ..dataset["sectionId"] = node.sectionId.toString()
            ..maxLength = 4
            ..onInput.listen(_onSectionQuestionCountInput)
            ..onBlur.listen(_onSectionQuestionCountBlur));
    for (var child in node.section) {
      _renderSectionNode(child, level + 1);
    }
  }

  void _renderSectionQuestionCounts(SectionNode node, Iterator<String> values, Iterator<TextInputElement> elements,
      String inheritedValue) {
    values.moveNext();
    elements.moveNext();

    if (updateInactiveTextInput(elements.current, values.current)) {
      elements.current.classes.remove("incorrect");
    }
    elements.current
        ..dataset["originalValue"] = values.current
        ..placeholder = inheritedValue;

    String childInheritedValue = values.current.isEmpty ? inheritedValue : values.current;
    for (var child in node.section) {
      _renderSectionQuestionCounts(child, values, elements, childInheritedValue);
    }
  }

  void _onSectionQuestionCountInput(Event e) {
    e.stopImmediatePropagation();
    if (buggyInputEventFor(e.target)) {
      return;
    }
    _setSectionQuestionCount(e.target as TextInputElement);
  }

  void _onSectionQuestionCountBlur(Event e) {
    if (dispatcher.waitingForReply) {
      dispatcher.dimUiIfWaitingForReply();
      _forceRenderSectionTree = true;
    } else {
      TextInputElement input = e.target as TextInputElement;
      input
          ..value = input.dataset["originalValue"]
          ..classes.remove("incorrect");
    }
  }

  Future<void> _setSectionQuestionCount(TextInputElement input) async {
    try {
      await performAction(XProfileEditor_SetSectionQuestionCount()
          ..sectionId = Int64.parseInt(input.dataset["sectionId"])
          ..sectionQuestionCount = input.value.trim(), disableUi: false);
      input.classes.remove("incorrect");
    } on ActionException {
      input.classes.add("incorrect");
    }
  }
}

class MarkDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-mark-dialog";

  DialogElement w_dialog;
  NumberInputElement w_lowerBound;
  TextInputElement w_title;
  DivElement w_buttons;

  final Dispatcher _dispatcher;
  final String _targetName;
  final /* nullable */Mark _mark;
  final /* nullable */int _index;

  MarkDialog(this._dispatcher, this._targetName, {Mark mark, int index})
      : _mark = mark,
        _index = index,
        super(TAG);

  Future<bool> openModal() async {
    bool res;

    if (_mark == null) {
      w_buttons.children.insert(0, ButtonElement()
          ..value = "add"
          ..text = tr("Add"));
    } else {
      w_lowerBound.valueAsNumber = num.parse(anyDecimalMarkToDot(_normalizedDecimalToUiPercent(_mark.lowerBound)));
      w_title.value = _mark.title;

      w_buttons.children
          ..insert(0, ButtonElement()
              ..value = "update"
              ..text = tr("Update"))
          ..insert(1, ButtonElement()
              ..value = "delete"
              ..text = tr("Delete")
              ..formNoValidate = true);
    }

    if (_index == 0) {
      w_lowerBound
          ..valueAsNumber = 0
          ..disabled = true;
    }

    await open(w_dialog);

    switch (w_dialog.returnValue) {
      case "add":
        await _dispatcher.performAction(_targetName, XProfileEditor_AddMark()
            ..mark = _editedMark());
        res = true;
        break;
      case "update":
      case "delete":
        var action = XProfileEditor_UpdateMark()
            ..index = _index;
        if (w_dialog.returnValue == "update") {
          action.mark = _editedMark();
        }
        await _dispatcher.performAction(_targetName, action);
        res = true;
        break;
      default:
        res = false;
    }

    return res;
  }

  Mark _editedMark() => Mark()
      ..title = collapseSpaces(w_title.value)
      ..lowerBound = _percentToNormalizedDecimal(w_lowerBound.valueAsNumber);
}

String _normalizedDecimalToUiPercent(String d) =>
    "${int.parse(d.substring(0, 1) + d.substring(2, 4))}$uiDecimalMark${d.substring(4, 6)}";

String _percentToNormalizedDecimal(num value) => (value / 100).toStringAsFixed(4);

void initialize() {
  registerShapeParsers([
      (b) => XProfileScreen.fromBuffer(b),
      (b) => XProfileListEditor.fromBuffer(b),
      (b) => XProfileEditor.fromBuffer(b),
      (b) => XSectionTree.fromBuffer(b)]);
}
