/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'misc_utils.dart';

class ConnectionLostDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-connection-lost-dialog";

  DialogElement w_dialog;

  ConnectionLostDialog() : super(TAG);

  Future<void> openModal() async {
    await open(w_dialog);
    window.location.reload();
  }
}

class InputStringDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-input-string-dialog";

  DialogElement w_dialog;
  SpanElement w_prompt;
  InputElement w_value;

  InputStringDialog() : super(TAG);

  Future<void> _open({String prompt}) {
    if (prompt != null) {
      w_prompt.text = prompt;
    }

    focusInputElement(w_value);
    return open(w_dialog);
  }
}

Future</* nullable */int> inputInteger({String prompt, int min, int max, int initial}) async {
  var d = InputStringDialog();
  d.w_value
      ..type = "number"
      ..required = true;
  NumberInputElement value = d.w_value;

  if (min != null) {
    value.min = min.toString();
  }

  if (max != null) {
    value.max = max.toString();
  }

  if (initial != null) {
    value.valueAsNumber = initial;
  }

  await d._open(prompt: prompt);
  return d.w_dialog.returnValue.isEmpty ? null : value.valueAsNumber.toInt();
}

Future</* nullable */String> inputString({String prompt, int maxLength, String initial}) async {
  var d = InputStringDialog();

  if (maxLength != null) {
    d.w_value
        ..maxLength = maxLength
        ..size = math.min(maxLength, 50);
  }

  if (initial != null) {
    d.w_value.value = initial;
  }

  await d._open(prompt: prompt);
  return d.w_dialog.returnValue.isEmpty ? null : d.w_value.value;
}
