/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/authenticationMode/account_login.pb.dart';
import 'package:iren_proto/authenticationMode/self_registration.pb.dart';
import 'package:iren_proto/authenticationMode/text_file.pb.dart';
import 'package:iren_proto/google/protobuf/any.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/user_manager.pb.dart';
import 'package:web_helpers/web_helpers.dart';

import 'misc_utils.dart';

class OptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-options-dialog";

  static final Any _selfRegistration = toAny(SelfRegistration.getDefault());
  static final Any _textFile = toAny(TextFile.getDefault());
  static final Any _accountLogin = toAny(AccountLogin.getDefault());

  DialogElement w_dialog;
  RadioButtonInputElement w_selfRegistration;
  RadioButtonInputElement w_textFile;
  RadioButtonInputElement w_accountLogin;

  final Connection _connection;
  final Int64 _userManagerChannelId;

  Any _authenticationMode;

  OptionsDialog(this._connection, this._userManagerChannelId) : super(TAG);

  Future<void> openModal() async {
    _authenticationMode = (await _perform(UserManagerRequest()
        ..getAuthenticationMode = GetAuthenticationMode())).getAuthenticationModeReply.mode;

    if (_authenticationMode.typeUrl == _selfRegistration.typeUrl) {
      w_selfRegistration.checked = true;
    } else if (_authenticationMode.typeUrl == _textFile.typeUrl) {
      w_textFile.checked = true;
    } else if (_authenticationMode.typeUrl == _accountLogin.typeUrl) {
      w_accountLogin.checked = true;
    }

    w_selfRegistration.onClick.listen((_) => _authenticationMode = _selfRegistration);
    w_textFile.onClick.listen((_) => _authenticationMode = _textFile);
    w_accountLogin.onClick.listen((_) => _authenticationMode = _accountLogin);

    await open(w_dialog);

    if (w_dialog.returnValue.isNotEmpty) {
      await _perform(UserManagerRequest()
          ..setAuthenticationMode = (SetAuthenticationMode()
              ..mode = _authenticationMode));
    }
  }

  Future<UserManagerReply> _perform(UserManagerRequest request) async {
    lockUi();
    try {
      return (await _connection.perform(ClientMessage()
          ..channelId = _userManagerChannelId
          ..userManagerRequest = request)).userManagerReply;
    } finally {
      unlockUi();
    }
  }
}
