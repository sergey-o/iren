/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'package:iren_proto/google/protobuf/timestamp.pb.dart';
import 'package:js/js_util.dart';
import 'package:web_helpers/translator.dart';

import 'electron.dart';
import 'embedded_resources.dart';

const String _LIB = "iren_utils";

const List<String> TEST_EXTENSIONS = ["itx", "it3", "it2"];
const String QUESTION_LIST_MIME_TYPE = "application/x-irenproject.ru-question-list";

final bool developmentMode = remote_process_argv.contains("--devel");
final String dataDirectory = ipcRenderer_sendSync("getDataDirectory") as String;

final String untitledFile = _tr("Untitled");
final String testFilterName = _tr("Tests");

const String _PROGRAM_VERSION_LOCATION = "package:iren_version/VERSION";

@EmbeddedResource(_PROGRAM_VERSION_LOCATION)
final String programVersion = getEmbeddedResource(_PROGRAM_VERSION_LOCATION);

String _tr(String key) => _trD(key) as String;

dynamic _trD(String key) => translate(_LIB, key);

String getFileTitle(String fileName, List<String> extensions) {
  String res;
  if (fileName.isEmpty) {
    res = untitledFile;
  } else {
    int p = fileName.lastIndexOf(RegExp(r"/|\\"));
    String s = (p == -1) ? fileName : fileName.substring(p + 1);
    res = dropFileExtension(s, extensions);
  }
  return res;
}

String dropFileExtension(String s, List<String> extensions) {
  String res = s;
  for (String ext in extensions) {
    String suffix = ".$ext";
    if ((s.length >= suffix.length) && (s.substring(s.length - suffix.length).toLowerCase() == suffix)) {
      res = s.substring(0, s.length - suffix.length);
      break;
    }
  }
  return res;
}

List<String> showOpenTestDialog() => dialog_showOpenDialogSync(getCurrentWindow(), ShowOpenDialogOptions(
    filters: [
        FileDialogFilter(name: testFilterName, extensions: TEST_EXTENSIONS),
        FileDialogFilter(name: _tr("All Files"), extensions: ["*"])],
    properties: ["openFile", "multiSelections"])) ?? [];

/* nullable */String showSaveFileDialog(String extension, String filterName, {String defaultPathWithoutExtension}) {
  String defaultPath;
  if (defaultPathWithoutExtension == null) {
    defaultPath = (process_platform == "linux") ? "$untitledFile.$extension" : "";
  } else {
    defaultPath = "$defaultPathWithoutExtension.$extension";
  }

  return dialog_showSaveDialogSync(getCurrentWindow(), ShowSaveDialogOptions(
      defaultPath: defaultPath,
      filters: (process_platform == "linux") ? [] : [FileDialogFilter(name: filterName, extensions: [extension])]));
}

int showMessageBox(ShowMessageBoxOptions options) => dialog_showMessageBoxSync(getCurrentWindow(), options
    ..title ??= _tr("Iren")
    ..noLink ??= true);

Future<ShowMessageBoxResult> showMessageBoxAsync(ShowMessageBoxOptions options) => promiseToFuture(
    dialog_showMessageBoxAsync(getCurrentWindow(), options
        ..title ??= _tr("Iren")
        ..noLink ??= true));

void showErrorMessage(String message) {
  showMessageBox(ShowMessageBoxOptions(
      type: "error",
      buttons: [_tr("Close")],
      message: message));
}

void showInformationMessage(String message) {
  showMessageBox(ShowMessageBoxOptions(
      type: "info",
      buttons: [_tr("Close")],
      message: message));
}

String getPrintableSectionName(String s) => s.isEmpty ? _tr("untitled") : s;

String formatTimestamp(Timestamp t, {bool includeSeconds: true}) {
  String pad(int n) => n.toString().padLeft(2, "0");

  DateTime d = t.toDateTime().toLocal();
  String seconds = includeSeconds ? ":${pad(d.second)}" : "";
  return "${d.year}-${pad(d.month)}-${pad(d.day)}\u2003${pad(d.hour)}:${pad(d.minute)}$seconds";
}

String getUnknownFileVersionMessage(String fileName) => _trD("UNKNOWN_FILE_VERSION")(fileName) as String;
