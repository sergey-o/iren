/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_proto/editor/select_question_editor.pb.dart';
import 'package:iren_proto/question/select_question.pb.dart';
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'pad_editor.dart';
import 'registries.dart';

class SelectQuestionEditor extends Widget<XSelectQuestionEditor> {
  static const String TAG = "ru-irenproject-select-question-editor";

  DivElement w_toolbar;
  ButtonElement w_addChoiceButton;
  ButtonElement w_deleteChoiceButton;
  ButtonElement w_moveChoiceUpButton;
  ButtonElement w_moveChoiceDownButton;
  ButtonElement w_addNegativeChoiceButton;
  ButtonElement w_fixChoiceButton;
  DivElement w_choicePanel;

  PadEditor _formulation;

  /* nullable */int _selectChoice;
  /* nullable */int _focusedChoice;

  SelectQuestionEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addChoiceButton.onClick.listen((_) => _addChoice());
    w_deleteChoiceButton.onClick.listen((_) => _deleteChoice());
    w_moveChoiceUpButton.onClick.listen((_) => _moveChoiceUp());
    w_moveChoiceDownButton.onClick.listen((_) => _moveChoiceDown());
    w_addNegativeChoiceButton.onClick.listen((_) => _addNegativeChoice());
    w_fixChoiceButton.onClick.listen((_) => _fixChoice());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addChoice)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteChoice)
        ..add(KeyCode.F6, KeyModifierState.NONE, _addNegativeChoice);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveChoiceUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveChoiceDown);

    _formulation = PadEditor(shape.formulationEditor, dispatcher)
        ..element.id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation.element);

    Element.focusEvent.forTarget(element.shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(element.shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedChoice = null;
      w_choicePanel.nodes = shape.choiceEditor.map((name) => ChoiceEditor(name, dispatcher).element);
      w_addChoiceButton.disabled = !shape.canAddChoice;
      w_addNegativeChoiceButton.disabled = !shape.canAddNegativeChoice;
    }

    for (var e in w_choicePanel.children) {
      getWidget(e).render();
    }

    if ((_selectChoice != null) && shape.choiceEditor.isNotEmpty) {
      getWidget(w_choicePanel.children[_selectChoice.clamp(0, shape.choiceEditor.length - 1) as int]).focusComponent();
    }
    _selectChoice = null;

    _updateUi();
  }

  void _addChoice() {
    if (shape.canAddChoice) {
      performAction(XSelectQuestionEditor_AddChoice());
      _selectChoice = shape.choiceEditor.length;
    }
  }

  void _deleteChoice() {
    if (_focusedChoice != null) {
      performAction(XSelectQuestionEditor_DeleteChoice()
          ..index = _focusedChoice);
      _selectChoice = _focusedChoice;
    }
  }

  void _onFocus() {
    int index = w_choicePanel.children.indexOf(element.shadowRoot.activeElement);
    if (index != -1) {
      _focusedChoice = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!w_toolbar.contains((e as FocusEvent).relatedTarget as Node)) {
      _focusedChoice = null;
    }
    _updateUi();
  }

  void _updateUi() {
    w_deleteChoiceButton.disabled = (_focusedChoice == null);
    w_moveChoiceUpButton.disabled = !_canMoveChoiceUp();
    w_moveChoiceDownButton.disabled = !_canMoveChoiceDown();

    w_fixChoiceButton
        ..disabled = (_focusedChoice == null)
        ..classes.toggle("highlighted",
            (_focusedChoice != null) && (getWidget(w_choicePanel.children[_focusedChoice]) as ChoiceEditor).fixed);
  }

  void _moveChoiceUp() {
    if (_canMoveChoiceUp()) {
      performAction(XSelectQuestionEditor_MoveChoice()
          ..index = _focusedChoice
          ..forward = false);
     _selectChoice = _focusedChoice - 1;
    }
  }

  bool _canMoveChoiceUp() => (_focusedChoice != null) && (_focusedChoice > 0);

  void _moveChoiceDown() {
    if (_canMoveChoiceDown()) {
      performAction(XSelectQuestionEditor_MoveChoice()
          ..index = _focusedChoice
          ..forward = true);
     _selectChoice = _focusedChoice + 1;
    }
  }

  bool _canMoveChoiceDown() => (_focusedChoice != null) && (_focusedChoice < shape.choiceEditor.length - 1);

  void _addNegativeChoice() {
    if (shape.canAddNegativeChoice) {
      performAction(XSelectQuestionEditor_AddNegativeChoice()
          ..text = tr("None of the above are correct."));
      _selectChoice = shape.choiceEditor.length;
    }
  }

  void _fixChoice() {
    if (_focusedChoice != null) {
      ChoiceEditor choiceEditor = getWidget(w_choicePanel.children[_focusedChoice]) as ChoiceEditor;
      choiceEditor.performSetFixed(!choiceEditor.fixed);
    }
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

class ChoiceEditor extends Widget<XSelectQuestionChoiceEditor> {
  static const String TAG = "ru-irenproject-select-question-choice-editor";

  CheckboxInputElement w_correct;
  ImageElement w_negativeChoiceIndicator;
  ImageElement w_fixedChoiceIndicator;

  PadEditor _padEditor;

  ChoiceEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_correct.onClick.listen((_) => _onCorrectClick());

    _padEditor = PadEditor(shape.padEditor, dispatcher)
        ..element.id = "padEditor";
    findElement("padEditorPlaceholder").replaceWith(_padEditor.element);
  }

  @override void render() {
    super.render();

    if (shapeChanged) {
      w_correct.checked = shape.correct;
      _padEditor.element.classes.toggle("correct", shape.correct);

      w_negativeChoiceIndicator.style.visibility = shape.negative ? "" : "hidden";
      w_fixedChoiceIndicator.style.visibility = shape.fixed ? "" : "hidden";
    }

    _padEditor.render();
  }

  void _onCorrectClick() {
    performAction(XSelectQuestionChoiceEditor_SetCorrect()
        ..correct = w_correct.checked);
  }

  @override void focusComponent() {
    _padEditor.focusComponent();
  }

  bool get fixed => shape.fixed;

  void performSetFixed(bool value) {
    performAction(XSelectQuestionChoiceEditor_SetFixed()
        ..fixed = value);
  }
}

const String _LIB = "select_question_editor";

void initialize() {
  registerShapeParsers([
      (b) => XSelectQuestionEditor.fromBuffer(b),
      (b) => XSelectQuestionChoiceEditor.fromBuffer(b)]);

  registerQuestionDescriptor(QuestionDescriptor(
      type: SelectQuestionType.select.name,
      title: translate(_LIB, "Multiple Choice Question") as String,
      priority: 1000,
      icon: "select_question_editor.resources/selectQuestion.png",
      initialText: ""));

  registerQuestionEditorFactory(
      XSelectQuestionEditor,
      (name, dispatcher) => SelectQuestionEditor(name, dispatcher));
}
