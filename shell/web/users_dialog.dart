/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/user_manager.pb.dart';

import 'electron.dart';
import 'misc_utils.dart';

class UsersDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-users-dialog";

  DialogElement w_dialog;
  TextAreaElement w_users;
  DivElement w_message;
  ButtonElement w_saveButton;

  final Connection _connection;
  final Int64 _userManagerChannelId;

  String _language;
  List<int> _oldHash;

  Map<TextFileUserRegistryError_Reason, String> _messages;

  UsersDialog(this._connection, this._userManagerChannelId) : super(TAG);

  Future<void> openModal() async {
    _language = window.navigator.language;
    _messages = {
        TextFileUserRegistryError_Reason.GROUP_NAME_EXPECTED: tr("Group name is not specified (must end with ':')."),
        TextFileUserRegistryError_Reason.INCORRECT_GROUP_NAME: tr("Incorrect group name."),
        TextFileUserRegistryError_Reason.INCORRECT_USER_NAME: tr("Incorrect user name."),
        TextFileUserRegistryError_Reason.DUPLICATE_USER_NAME: tr("The user is already present in the group.")};

    GetUserRegistry_Reply userRegistry = (await _perform(UserManagerRequest()
        ..getUserRegistry = (GetUserRegistry()
            ..language = _language))).getUserRegistryReply;
    _oldHash = userRegistry.hash;
    w_users.value = userRegistry.text;

    w_saveButton.onClick.listen((e) {
      e.preventDefault();
      _save();
    });

    _hideMessage();
    w_users.onInput.listen((_) => _hideMessage());

    await open(w_dialog);
  }

  Future<void> _save() async {
    _hideMessage();

    SetUserRegistry_Reply reply = (await _perform(UserManagerRequest()
        ..setUserRegistry = (SetUserRegistry()
            ..text = w_users.value
            ..expectedOldHash = _oldHash
            ..language = _language))).setUserRegistryReply;

    if (!reply.oldHashMatches) {
      _showMessage(tr("Error on save: the list has been changed by another user."));
    } else if (reply.hasError()) {
      _showMessage(_messages[reply.error.reason]);
      selectTextAreaLine(w_users, reply.error.line);
      w_users.focus();

      // scroll to caret (work around https://bugs.chromium.org/p/chromium/issues/detail?id=331233)
      getCurrentWebContents().sendInputEvent(InputEvent(type: "keyDown", keyCode: "Right"));
      getCurrentWebContents().sendInputEvent(InputEvent(type: "keyUp", keyCode: "Right"));
      Timer.run(() => selectTextAreaLine(w_users, reply.error.line));
    } else {
      w_dialog.close("");
    }
  }

  Future<UserManagerReply> _perform(UserManagerRequest request) async {
    lockUi();
    try {
      return (await _connection.perform(ClientMessage()
          ..channelId = _userManagerChannelId
          ..userManagerRequest = request)).userManagerReply;
    } finally {
      unlockUi();
    }
  }

  void _showMessage(String message) {
    w_message
        ..text = message
        ..style.visibility = "";
  }

  void _hideMessage() {
    w_message.style.visibility = "hidden";
  }
}
