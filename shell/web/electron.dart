/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@JS()
library iren_shell.electron;

import 'dart:js_util' as js_util;
import 'dart:typed_data';

import 'package:js/js.dart';
import 'package:meta/meta.dart';

@JS() abstract class BrowserWindow {
  external void destroy();
}

@JS() abstract class WebContents {
  external void sendInputEvent(InputEvent event);
}

@JS("remote.Menu")
class Menu {
  external Menu();
  external void append(MenuItem item);
  external void popup(MenuPopupOptions options);
  external void closePopup();
}

@JS() @anonymous class MenuPopupOptions {
  external factory MenuPopupOptions({
      num x,
      num y});
  external void Function() get callback;
  external set callback(void Function() value);
}

@JS("remote.MenuItem")
class MenuItem {
  external MenuItem(MenuItemOptions options);
}

@JS() @anonymous class MenuItemOptions {
  external factory MenuItemOptions({
      void Function(dynamic menuItem, dynamic browserWindow, dynamic event) click,
      String type,
      String label,
      String accelerator,
      String icon,
      bool enabled,
      bool checked,
      Menu submenu});
}

@JS() @anonymous class ShowMessageBoxOptions {
  external factory ShowMessageBoxOptions({
      String type,
      List<String> buttons,
      String message,
      String checkboxLabel,
      int cancelId});
  external String get title;
  external set title(String value);
  external bool get noLink;
  external set noLink(bool value);
}

@JS() @anonymous class ShowMessageBoxResult {
  external int get response;
  external bool get checkboxChecked;
}

@JS() @anonymous class ShowOpenDialogOptions {
  external factory ShowOpenDialogOptions({
      List<FileDialogFilter> filters,
      List<String> properties});
}

@JS() @anonymous class FileDialogFilter {
  external factory FileDialogFilter({
      @required String name,
      @required List<String> extensions});
}

@JS() @anonymous class ShowSaveDialogOptions {
  external factory ShowSaveDialogOptions({
      String defaultPath,
      List<FileDialogFilter> filters});
}

@JS() @anonymous class WriteFileOptions {
  external factory WriteFileOptions({String flag});
}

@JS() @anonymous class InputEvent {
  external factory InputEvent({
      @required String type,
      String keyCode});
}

@JS("remote.getCurrentWindow")
external BrowserWindow getCurrentWindow();

@JS("remote.getCurrentWebContents")
external WebContents getCurrentWebContents();

@JS("remote.dialog.showMessageBoxSync")
external int dialog_showMessageBoxSync(BrowserWindow browserWindow, ShowMessageBoxOptions options);

@JS("remote.dialog.showMessageBox")
external dynamic/* Promise<ShowMessageBoxResult> */ dialog_showMessageBoxAsync(
    BrowserWindow browserWindow,
    ShowMessageBoxOptions options);

@JS("remote.dialog.showOpenDialogSync")
external /* nullable */List<String> dialog_showOpenDialogSync(
    BrowserWindow browserWindow,
    ShowOpenDialogOptions options);

@JS("remote.dialog.showSaveDialogSync")
external /* nullable */String dialog_showSaveDialogSync(BrowserWindow browserWindow, ShowSaveDialogOptions options);

@JS("ipcRenderer.on")
external void ipcRenderer_on(String channel, void Function(dynamic event, [dynamic data]) listener);

@JS("ipcRenderer.once")
external void ipcRenderer_once(String channel, void Function(dynamic event, [dynamic data]) listener);

@JS("ipcRenderer.send")
external void ipcRenderer_send(String channel);

@JS("ipcRenderer.sendSync")
external dynamic ipcRenderer_sendSync(String channel, [dynamic data]);

@JS("remote.app.getAppPath")
external String app_getAppPath();

@JS("Buffer.from")
external dynamic Buffer_from(ByteBuffer arrayBuffer, int byteOffset, int length);

@JS("node_fs.writeFileSync")
external void fs_writeFileSync(String fileName, dynamic data, [WriteFileOptions options]);

@JS("node_fs.readFileSync")
external Uint8List fs_readFileSync(String fileName);

@JS("node_fs.readFileSync")
external String fs_readFileSync_String(String fileName, String encoding);

@JS("node_fs.renameSync")
external void fs_renameSync(String oldPath, String newPath);

@JS("node_fs.unlinkSync")
external void fs_unlinkSync(String path);

@JS("node_fs.openSync")
external int fs_openSync(String path, String flags);

@JS("node_fs.closeSync")
external void fs_closeSync(int fd);

@JS("node_fs.writeSync")
external int fs_writeSync(int fd, dynamic buffer, int offset, int length);

@JS("node_fs.existsSync")
external bool fs_existsSync(String path);

@JS("node_path.join")
external String path_join(String path1, String path2);

@JS("node_path.normalize")
external String path_normalize(String path);

@JS("node_path.resolve")
external String path_resolve(String path);

@JS("process.platform")
external String get process_platform;

@JS("remote.process.argv")
external List<String> get remote_process_argv;

@JS("node_os.EOL")
external String get os_EOL;

@JS("shell.openExternal")
external dynamic/* Promise<void> */ shell_openExternal(String url);

@JS("clipboard.availableFormats")
external List<String> clipboard_availableFormats();

@JS("require")
external dynamic _require(String name);

@JS("clipboard")
external set _clipboard(dynamic value);

@JS("ipcRenderer")
external set _ipcRenderer(dynamic value);

@JS("remote")
external set _remote(dynamic value);

@JS("shell")
external set _shell(dynamic value);

@JS("node_fs")
external set _node_fs(dynamic value);

@JS("node_os")
external set _node_os(dynamic value);

@JS("node_path")
external set _node_path(dynamic value);

void initializeElectronLibrary() {
  dynamic electron = _require("electron");
  _clipboard = js_util.getProperty(electron, "clipboard");
  _ipcRenderer = js_util.getProperty(electron, "ipcRenderer");
  _remote = js_util.getProperty(electron, "remote");
  _shell = js_util.getProperty(electron, "shell");

  _node_fs = _require("original-fs");
  _node_os = _require("os");
  _node_path = _require("path");
}
