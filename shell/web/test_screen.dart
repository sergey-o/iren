/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/editor.pb.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'question_list_editor.dart';
import 'question_viewer.dart';
import 'registries.dart';
import 'section_tree_editor.dart';

class TestScreen extends Widget<XTestScreen> {
  static const String TAG = "ru-irenproject-test-screen";

  DivElement w_left;
  ButtonElement w_editButton;
  ButtonElement w_viewButton;
  DivElement w_questionPanel;

  QuestionListEditor _questionListEditor;
  SectionTreeEditor _sectionTreeEditor;
  /* nullable */Widget _questionEditor;
  bool _focusQuestionEditor = false;
  StreamSubscription<ClipboardEvent> _pasteSubscription;

  /* nullable */QuestionViewer _questionViewer;
  /* nullable */QuestionViewerDashboard _questionViewerDashboard;

  TestScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    _sectionTreeEditor = SectionTreeEditor(shape.sectionTreeEditor, dispatcher)
        ..element.id = "sectionTreeEditor";
    w_left.append(_sectionTreeEditor.element);

    w_editButton.onClick.listen((_) => _setViewMode(false));
    w_viewButton.onClick.listen((_) => _setViewMode(true));

    windowShortcutHandler
        ..add(KeyCode.ONE, KeyModifierState.CTRL, () => _setViewMode(false))
        ..add(KeyCode.TWO, KeyModifierState.CTRL, () => _setViewMode(true));
  }

  @override void attached() {
    super.attached();
    _pasteSubscription = document.onPaste.listen(_paste);
  }

  @override void detached() {
    _pasteSubscription?.cancel();
    _pasteSubscription = null;
    super.detached();
  }

  @override void render() {
    super.render();

    if (shape.questionListEditor != _questionListEditor?.name) {
      _questionListEditor = QuestionListEditor(shape.questionListEditor, dispatcher)
          ..element.id = "questionListEditor"
          ..onAddQuestion.listen((_) => _focusQuestionEditor = true)
          ..onShowProfiles.listen((_) => performAction(XTestScreen_ShowProfiles()));
      findElement("questionListEditor").replaceWith(_questionListEditor.element);
    }
    _questionListEditor.render();

    _sectionTreeEditor.render();

    w_editButton.classes.toggle("selected", !shape.viewMode);
    w_viewButton.classes.toggle("selected", shape.viewMode);

    if (shape.hasQuestionEditor()) {
      if (shape.questionEditor != _questionEditor?.name) {
        _questionEditor?.element?.remove();
        WidgetFactory factory = getQuestionEditorFactory(scene.shapes(shape.questionEditor).runtimeType);
        _questionEditor = factory(shape.questionEditor, dispatcher);
        w_questionPanel.append(_questionEditor.element);
      }
      _questionEditor
          ..element.style.display = shape.viewMode ? "none" : ""
          ..render();
    } else {
      _questionEditor?.element?.remove();
      _questionEditor = null;
    }

    if (shape.hasQuestionViewer()) {
      if (shape.questionViewer != _questionViewer?.name) {
        _questionViewer?.element?.remove();
        _questionViewer = QuestionViewer(shape.questionViewer, dispatcher);
        w_questionPanel.append(_questionViewer.element);

        _questionViewerDashboard?.element?.remove();
        _questionViewerDashboard = QuestionViewerDashboard(_questionViewer);
        w_viewButton.insertAdjacentElement("afterend", _questionViewerDashboard.element);
      }

      _questionViewer.render();
      _questionViewerDashboard.render();
    } else {
      _questionViewer?.element?.remove();
      _questionViewer = null;
      _questionViewerDashboard?.element?.remove();
      _questionViewerDashboard = null;
    }

    if (_focusQuestionEditor) {
      _focusQuestionEditor = false;
      Timer.run(() {
        if (_questionEditor != null) {
          _questionEditor.focusComponent();
          document.execCommand("selectAll", false, null);
        }
      });
    }
  }

  void _paste(ClipboardEvent e) {
    if (element.shadowRoot.activeElement == _sectionTreeEditor.element) {
      stopFurtherProcessing(e);
      _questionListEditor.pasteQuestions(e);
    }
  }

  void _setViewMode(bool viewMode) {
    performAction(XTestScreen_SetViewMode()
        ..viewMode = viewMode);
  }
}

void initialize() {
  registerShapeParser((b) => XTestScreen.fromBuffer(b));
}
