/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/modifier_screen.pb.dart';
import 'package:iren_proto/modifier/add_negative_choice_modifier.pb.dart';
import 'package:iren_proto/modifier/script_modifier.pb.dart';
import 'package:iren_proto/modifier/set_evaluation_model_modifier.pb.dart';
import 'package:iren_proto/modifier/set_negative_choice_content_modifier.pb.dart';
import 'package:iren_proto/modifier/set_weight_modifier.pb.dart';
import 'package:iren_proto/modifier/shuffle_choices_modifier.pb.dart';
import 'package:iren_proto/modifier/suppress_single_choice_hint_modifier.pb.dart';
import 'package:iren_proto/question/select_question.pb.dart';
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'registries.dart';

class ModifierScreen extends Widget<XModifierScreen> {
  static const String TAG = "ru-irenproject-modifier-screen";

  ButtonElement w_closeButton;

  ModifierListEditor _modifierListEditor;

  ModifierScreen(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    _modifierListEditor = ModifierListEditor(shape.modifierListEditor, dispatcher)
        ..element.id = "list"
        ..autoScrollToSelection = true;
    findElement("listPlaceholder").replaceWith(_modifierListEditor.element);

    w_closeButton.onClick.listen((_) => _close());

    windowShortcutHandler.add(KeyCode.ESC, KeyModifierState.NONE, _close);
  }

  @override void render() {
    super.render();
    _modifierListEditor.render();
  }

  void _close() {
    performAction(XModifierScreen_Close());
  }

  @override void focusComponent() {
    _modifierListEditor.focusComponent();
  }
}

class ModifierListEditor extends Widget<XModifierListEditor> {
  static const String TAG = "ru-irenproject-modifier-list-editor";

  static final List<String> _hiddenModifierTypes = [
      SetNegativeChoiceContentModifierType.setNegativeChoiceContent.name,
      SetWeightModifierType.setWeight.name];

  ButtonElement w_addButton;
  ButtonElement w_deleteButton;
  ButtonElement w_moveUpButton;
  ButtonElement w_moveDownButton;
  SpanElement w_caption;
  DivElement w_scrollable;
  DivElement w_main;

  bool autoScrollToSelection = false;
  bool _suppressAutoScroll = false;
  List<Item> _rendered;

  ModifierListEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addButton.onClick.listen((_) => _onAddClick());
    w_deleteButton.onClick.listen((_) => _delete());
    w_moveUpButton.onClick.listen((_) => _moveUp());
    w_moveDownButton.onClick.listen((_) => _moveDown());

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown);

    if (shape.hasQuestionType()) {
      w_caption.text = tr("Question Modifiers");
    } else if (!shape.hasSectionName()) {
      w_caption.style.display = "none";
    }
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      if (shape.hasSectionName()) {
        w_caption.text = trD("SECTION_MODIFIERS")(getPrintableSectionName(shape.sectionName)) as String;
      }

      if (!DEFAULT_LIST_EQUALITY.equals(_rendered, shape.item)) {
        w_main.nodes.clear();

        for (var item in shape.item) {
          ModifierDescriptor d = getModifierDescriptor(item.type);
          w_main.append(DivElement()
              ..classes.add("title")
              ..text = d.title
              ..title = d.title
              ..onClick.listen(_onModifierClick));
          if (item.hasEditor()) {
            WidgetFactory factory = getModifierEditorFactory(scene.shapes(item.editor).runtimeType);
            w_main.append(factory(item.editor, dispatcher).element
                ..classes.add("editor")
                ..onFocus.listen(_onEditorFocus));
          }
        }

        _rendered = shape.item;
      }

      w_main.querySelectorAll(".title").asMap().forEach((i, title) {
        bool selected = shape.hasSelectedIndex() && (shape.selectedIndex == i);
        title.classes.toggle("selected", selected);
        if (selected && autoScrollToSelection && !_suppressAutoScroll) {
          scheduleScrollIntoView(title);
        }
      });

      w_deleteButton.disabled = !shape.hasSelectedIndex();
      w_moveUpButton.disabled = !_canMoveUp();
      w_moveDownButton.disabled = !_canMoveDown();
    }

    for (var e in w_main.querySelectorAll(".editor")) {
      getWidget(e).render();
    }

    _suppressAutoScroll = false;
  }

  void _onModifierClick(MouseEvent e) {
    performAction(XModifierListEditor_Select()
        ..index = w_main.querySelectorAll(".title").indexOf(e.currentTarget));
  }

  void _onAddClick() {
    var menu = Menu();

    for (var d in modifierDescriptors) {
      bool applicable = (d.questionTypes == null) || !shape.hasQuestionType()
          || d.questionTypes.contains(shape.questionType);
      if (applicable && !_hiddenModifierTypes.contains(d.type)) {
        menu.append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _add(d)),
            label: d.title,
            enabled: !shape.item.any((item) => item.type == d.type))));
      }
    }

    showPopupMenu(menu, getDropDownMenuPosition(w_addButton));
  }

  Future<void> _add(ModifierDescriptor d) async {
    await performAction(XModifierListEditor_Add()
        ..type = d.type);
    await dispatcher.onChangeScene.first;
    _selectedEditor()?.focusComponent();
  }

  void _delete() {
    if (shape.hasSelectedIndex()) {
      performAction(XModifierListEditor_Delete());
    }
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(XModifierListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => shape.hasSelectedIndex() && (shape.selectedIndex > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(XModifierListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => shape.hasSelectedIndex() && (shape.selectedIndex < shape.item.length - 1);

  @override void focusComponent() {
    Widget selectedEditor = _selectedEditor();
    if (selectedEditor == null) {
      w_scrollable.focus();
    } else {
      selectedEditor.focusComponent();
    }
  }

  /* nullable */Widget _selectedEditor() {
    Element e = w_main.querySelector(".selected + .editor");
    return (e == null) ? null : getWidget(e);
  }

  void _onEditorFocus(Event e) {
    int modifierIndex = w_main.querySelectorAll(".title").indexOf((e.target as Node).previousNode);
    if (shape.selectedIndex != modifierIndex) {
      _suppressAutoScroll = true;
      performAction(XModifierListEditor_Select()
          ..index = modifierIndex, disableUi: false);
    }
  }
}

const String _LIB = "modifier_screen";

String _tr(String key) => translate(_LIB, key) as String;

void initialize() {
  registerShapeParsers([
      (b) => XModifierScreen.fromBuffer(b),
      (b) => XModifierListEditor.fromBuffer(b)]);

  registerModifierDescriptor(ModifierDescriptor(
      type: SuppressSingleChoiceHintModifierType.suppressSingleChoiceHint.name,
      title: _tr("Allow Multiple Selection for Questions with a Single Correct Answer"),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(ModifierDescriptor(
      type: AddNegativeChoiceModifierType.addNegativeChoice.name,
      title: _tr('Add the "No Correct Answers" Choice'),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(ModifierDescriptor(
      type: SetNegativeChoiceContentModifierType.setNegativeChoiceContent.name,
      title: _tr('Set the "No Correct Answers" Choice Text'),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(ModifierDescriptor(
      type: ShuffleChoicesModifierType.shuffleChoices.name,
      title: _tr("Shuffle Choices"),
      questionTypes: [SelectQuestionType.select.name]));

  registerModifierDescriptor(ModifierDescriptor(
      type: SetEvaluationModelModifierType.setEvaluationModel.name,
      title: _tr("Set Scoring Mode")));

  registerModifierDescriptor(ModifierDescriptor(
      type: SetWeightModifierType.setWeight.name,
      title: _tr("Set Question Weight")));

  registerModifierDescriptor(ModifierDescriptor(
      type: ScriptModifierType.script.name,
      title: _tr("Script")));
}
