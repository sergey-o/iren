/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:js';

import 'package:codemirror/codemirror.dart';
import 'package:iren_proto/editor/script_modifier_editor.pb.dart';
import 'package:meta/meta.dart';

import 'editor_infra.dart';
import 'embedded_resources.dart';
import 'misc_utils.dart';
import 'registries.dart';

class ScriptModifierEditor extends Widget<XScriptModifierEditor> {
  static const String TAG = "ru-irenproject-script-modifier-editor";

  static const Map<String, String> _EXTRA_KEYS = {
      "Ctrl-A": "selectAll",
      "Ctrl-Z": "undo",
      "Shift-Ctrl-Z": "redo",
      "Ctrl-Home": "goDocStart",
      "Ctrl-End": "goDocEnd",
      "Ctrl-Left": "goGroupLeft",
      "Ctrl-Right": "goGroupRight",
      "Ctrl-Backspace": "delGroupBefore",
      "Ctrl-Delete": "delGroupAfter",
      "Tab": "indentMore",
      "Shift-Tab": "indentLess"};

  DivElement w_editorPanel;
  StyleElement w_codeMirrorStyle;

  CodeMirror _editor;
  bool _modified = false;

  ScriptModifierEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_codeMirrorStyle.text = _codeMirrorCss;

    _editor = CodeMirror.fromElement(w_editorPanel, options: {
        "viewportMargin": double.infinity,
        "mode": "pascal",
        "dragDrop": false,
        "lineWiseCopyCut": false,
        "electricChars": false,
        "scrollbarStyle": "null",
        "keyMap": "basic",
        "extraKeys": _EXTRA_KEYS,
        "lineNumbers": true,
        "matchBrackets": true,
        "autoCloseBrackets": true});
    _editor
        ..onChange.listen((_) => _modified = true)
        ..onEvent<JsObject>("scrollCursorIntoView", argCount: 2).listen(_onEditorScrollCursorIntoView)
        ..onEvent("blur").listen((_) => _save())
        ..onEvent<KeyboardEvent>("keydown", argCount: 2).listen(_onEditorKeyDown);
    w_editorPanel.onKeyDown.listen((e) => e.stopImmediatePropagation());
  }

  @override void attached() {
    super.attached();
    addWindowCloseListener(_onWindowClose);
    _editor.refresh();
  }

  @override void detached() {
    _editor.dispose();
    removeWindowCloseListener(_onWindowClose);
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      String newValue = shape.script.join("\n");
      if (_editor.getDoc().getValue() != newValue) {
        _editor.getDoc()
            ..setValue(newValue)
            ..setCursor(_editor.getDoc().posFromIndex(shape.caretPosition))
            ..clearHistory();
      }
      _modified = false;
    }
  }

  void _onEditorScrollCursorIntoView(JsObject event) {
    event.callMethod("preventDefault");
    if (_codeMirrorFocused()) {
      _scrollCursorIntoView(center: false);
    }
  }

  void _scrollCursorIntoView({@required bool center}) {
    JsObject r = _editor.callArgs("charCoords", [_editor.getCursor().toProxy(), "local"]) as JsObject;
    var cursorOverlay = DivElement();
    cursorOverlay.style
        ..position = "absolute"
        ..top = "${r["top"]}px"
        ..left = "${r["left"]}px"
        ..width = "1px"
        ..height = "${r["bottom"] - r["top"]}px";
    w_editorPanel.querySelector(".CodeMirror-lines").append(cursorOverlay);
    scrollElementIntoViewIfNeeded(cursorOverlay, center);
    cursorOverlay.remove();
  }

  Future<void> _save() async {
    if (_modified) {
      var action = XScriptModifierEditor_SetScript()
          ..caretPosition = _editor.getDoc().indexFromPos(_editor.getCursor());
      _editor.getDoc().eachLine((line) => action.script.add(line.text));

      await performAction(action, disableUi: false);
      await dispatcher.onChangeScene.first;
    }
  }

  void _onEditorKeyDown(KeyboardEvent e) {
    if (e.keyCode == KeyCode.ESC) {
      stopFurtherProcessing(e);
      element.blur();
    }
  }

  Future<void> _onWindowClose() async {
    if (_codeMirrorFocused()) {
      await _save();
    }
  }

  bool _codeMirrorFocused() => w_editorPanel.querySelector(".CodeMirror-focused") != null;

  @override void focusComponent() {
    _editor.focus();
    window.animationFrame.then((_) => _scrollCursorIntoView(center: true));
  }
}

const String _CODE_MIRROR_CSS_LOCATION = "package:codemirror/css/codemirror.css";

@EmbeddedResource(_CODE_MIRROR_CSS_LOCATION)
final String _codeMirrorCss = getEmbeddedResource(_CODE_MIRROR_CSS_LOCATION);

void initialize() {
  registerShapeParser((b) => XScriptModifierEditor.fromBuffer(b));

  registerModifierEditorFactory(
      XScriptModifierEditor,
      (name, dispatcher) => ScriptModifierEditor(name, dispatcher));
}
