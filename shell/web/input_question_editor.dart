/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/input_question_editor.pb.dart';
import 'package:iren_proto/question/input_question.pb.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'electron.dart';
import 'misc_utils.dart';
import 'pad_editor.dart';
import 'registries.dart';

class InputQuestionEditor extends Widget<XInputQuestionEditor> {
  static const String TAG = "ru-irenproject-input-question-editor";

  DivElement w_toolbar;
  ButtonElement w_addPatternButton;
  ButtonElement w_addPatternDropDown;
  ButtonElement w_deletePatternButton;
  ButtonElement w_movePatternUpButton;
  ButtonElement w_movePatternDownButton;
  ImageElement w_patternTypeImage;
  DivElement w_patternPanel;

  PadEditor _formulation;

  /* nullable */int _selectPattern;
  /* nullable */int _focusedPattern;
  _PatternType _selectedPatternType;

  InputQuestionEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addPatternButton.onClick.listen((_) => _addPattern());
    w_addPatternDropDown.onClick.listen((_) => _onAddPatternDropDownClick());
    w_deletePatternButton.onClick.listen((_) => _deletePattern());
    w_movePatternUpButton.onClick.listen((_) => _movePatternUp());
    w_movePatternDownButton.onClick.listen((_) => _movePatternDown());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addPattern)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deletePattern);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _movePatternUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _movePatternDown);

    _formulation = PadEditor(shape.formulationEditor, dispatcher)
        ..element.id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation.element);

    Element.focusEvent.forTarget(element.shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(element.shadowRoot, useCapture: true).listen(_onBlur);

    _selectPatternType(_textPatternType);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedPattern = null;
      w_patternPanel.nodes = shape.patternEditor.map((name) => PatternEditor(name, dispatcher).element);
      w_addPatternButton.disabled = w_addPatternDropDown.disabled = !_canAddPattern();
    }

    for (var e in w_patternPanel.children) {
      getWidget(e).render();
    }

    if ((_selectPattern != null) && shape.patternEditor.isNotEmpty) {
      getWidget(w_patternPanel.children[_selectPattern.clamp(0, shape.patternEditor.length - 1) as int])
          .focusComponent();
    }
    _selectPattern = null;

    _updateUi();
  }

  bool _canAddPattern() => shape.patternEditor.length < shape.maxPatterns;

  void _addPattern() {
    if (_canAddPattern()) {
      performAction(_selectedPatternType.addAction);
      _selectPattern = shape.patternEditor.length;
    }
  }

  void _onAddPatternDropDownClick() {
    var menu = Menu();

    for (var t in _patternTypes) {
      menu.append(MenuItem(MenuItemOptions(
          click: toMenuHandler(() {
            _selectPatternType(t);
            _addPattern();
          }),
          label: t.title,
          icon: getAbsoluteAppPath(t.icon))));
    }

    showPopupMenu(menu, getDropDownMenuPosition(w_addPatternButton));
  }

  void _selectPatternType(_PatternType t) {
    _selectedPatternType = t;
    w_patternTypeImage.src = t.icon;
    w_addPatternButton.title = "${t.addTitle} (F5)";
  }

  void _deletePattern() {
    if (_focusedPattern != null) {
      performAction(XInputQuestionEditor_DeletePattern()
          ..index = _focusedPattern);
      _selectPattern = _focusedPattern;
    }
  }

  void _movePatternUp() {
    if (_canMovePatternUp()) {
      performAction(XInputQuestionEditor_MovePattern()
          ..index = _focusedPattern
          ..forward = false);
      _selectPattern = _focusedPattern - 1;
    }
  }

  bool _canMovePatternUp() => (_focusedPattern != null) && (_focusedPattern > 0);

  void _movePatternDown() {
    if (_canMovePatternDown()) {
      performAction(XInputQuestionEditor_MovePattern()
          ..index = _focusedPattern
          ..forward = true);
     _selectPattern = _focusedPattern + 1;
    }
  }

  bool _canMovePatternDown() => (_focusedPattern != null) && (_focusedPattern < shape.patternEditor.length - 1);

  void _onFocus() {
    int index = w_patternPanel.children.indexOf(element.shadowRoot.activeElement);
    if (index != -1) {
      _focusedPattern = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!w_toolbar.contains((e as FocusEvent).relatedTarget as Node)) {
      _focusedPattern = null;
    }
    _updateUi();
  }

  void _updateUi() {
    w_deletePatternButton.disabled = (_focusedPattern == null);
    w_movePatternUpButton.disabled = !_canMovePatternUp();
    w_movePatternDownButton.disabled = !_canMovePatternDown();
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

class PatternEditor extends Widget<XPatternEditor> {
  static const String TAG = "ru-irenproject-input-question-pattern-editor";

  ImageElement w_patternTypeImage;
  TextInputElement w_pattern;
  NumberInputElement w_quality;
  ButtonElement w_optionsButton;

  PatternEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_patternTypeImage
        ..src = _patternType().icon
        ..title = _patternType().title;
    w_pattern
        ..maxLength = shape.maxLength
        ..onInput.listen((_) => _onPatternInput());
    w_quality
        ..onInput.listen((_) => _onQualityInput())
        ..onBlur.listen((_) => _showQuality());
    w_optionsButton.onClick.listen((_) => _showOptionsDialog());
  }

  _PatternType _patternType() => shape.hasRegexpSupplement() ? _regexpPatternType : _textPatternType;

  @override void render() {
    super.render();
    if (shapeChanged) {
      updateTextInput(w_pattern, shape.value);
      _showQuality();
      w_optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }
  }

  void _showQuality() {
    updateNumberInput(w_quality, shape.qualityPercent);
    w_quality.classes.toggle("fullyCorrect", shape.qualityPercent == 100);
  }

  void _onPatternInput() {
    if (buggyInputEventFor(w_pattern)) {
      return;
    }
    performAction(XPatternEditor_SetValue()
        ..value = w_pattern.value, disableUi: false);
  }

  void _onQualityInput() {
    if (buggyInputEventFor(w_quality)) {
      return;
    }
    if (w_quality.validity.valid) {
      performAction(XPatternEditor_SetQuality()
          ..qualityPercent = w_quality.valueAsNumber.toInt(), disableUi: false);
    }
  }

  @override void focusComponent() {
    w_pattern.focus();
  }

  void _showOptionsDialog() {
    if (_patternType() == _textPatternType) {
      TextPatternOptionsDialog(shape.textSupplement, dispatcher, name).openModal();
    } else {
      RegexpPatternOptionsDialog(shape.regexpSupplement, dispatcher, name).openModal();
    }
  }
}

class TextPatternOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-text-pattern-options-dialog";

  DialogElement w_dialog;
  CheckboxInputElement w_wildcard;
  CheckboxInputElement w_recognizeNumbers;
  TextInputElement w_precision;
  CheckboxInputElement w_spaceDelimited;
  CheckboxInputElement w_ignoreCase;
  ButtonElement w_okButton;

  final TextSupplement _source;
  final Dispatcher _dispatcher;
  final String _targetName;

  TextPatternOptionsDialog(this._source, this._dispatcher, this._targetName) : super(TAG);

  Future<void> openModal() {
    w_wildcard.checked = _source.wildcard;
    w_recognizeNumbers
        ..checked = _source.hasPrecision()
        ..onClick.listen((_) => _updateUi());
    w_spaceDelimited.checked = _source.spaceDelimited;
    w_ignoreCase.checked = !_source.caseSensitive;
    w_precision
        ..maxLength = _source.maxPrecisionLength
        ..size = _source.maxPrecisionLength
        ..value = _source.hasPrecision() ? dotToUiDecimalMark(_source.precision) : "0";
    w_okButton.onClick.listen(_onOkClick);

    _updateUi();
    return open(w_dialog);
  }

  void _onOkClick(Event e) {
    e.preventDefault();
    _continueOnOkClick();
  }

  Future<void> _continueOnOkClick() async {
    var action = XPatternEditor_SetTextPatternOptions()
        ..wildcard = w_wildcard.checked
        ..spaceDelimited = w_spaceDelimited.checked
        ..caseSensitive = !w_ignoreCase.checked;
    if (w_recognizeNumbers.checked) {
      action.precision = anyDecimalMarkToDot(w_precision.value.trim());
    }

    try {
      await _dispatcher.performAction(_targetName, action);
      w_dialog.close("");
    } on ActionException {
      focusInputElement(w_precision);
    }
  }

  void _updateUi() {
    w_precision.disabled = !w_recognizeNumbers.checked;
  }
}

class RegexpPatternOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-regexp-pattern-options-dialog";

  DialogElement w_dialog;
  DivElement w_spacePreprocessModePanel;
  CheckboxInputElement w_ignoreCase;

  final RegexpSupplement _source;
  final Dispatcher _dispatcher;
  final String _targetName;

  RegexpPatternOptionsDialog(this._source, this._dispatcher, this._targetName) : super(TAG);

  Future<void> openModal() async {
    (w_spacePreprocessModePanel.querySelector("[value=${_source.spacePreprocessMode}]") as RadioButtonInputElement)
        .checked = true;
    w_ignoreCase.checked = !_source.caseSensitive;

    await open(w_dialog);

    if (w_dialog.returnValue.isNotEmpty) {
      await _dispatcher.performAction(_targetName, XPatternEditor_SetRegexpPatternOptions()
          ..spacePreprocessMode = getProtobufEnumByName(SpacePreprocessMode.values,
              (w_spacePreprocessModePanel.querySelector(":checked") as RadioButtonInputElement).value)
          ..caseSensitive = !w_ignoreCase.checked);
    }
  }
}

class _PatternType {
  final String title;
  final String addTitle;
  final String icon;
  final GeneratedMessage addAction;

  _PatternType(this.title, this.addTitle, this.icon, this.addAction);
}

const String _LIB = "input_question_editor";

final _PatternType _textPatternType = _PatternType(
    _tr("Text Pattern"),
    _tr("Add Text Pattern"),
    "input_question_editor.resources/textPattern.png",
    XInputQuestionEditor_AddTextPattern());

final _PatternType _regexpPatternType = _PatternType(
    _tr("Regular Expression"),
    _tr("Add Regular Expression"),
    "input_question_editor.resources/regexpPattern.png",
    XInputQuestionEditor_AddRegexpPattern());

final List<_PatternType> _patternTypes = [_textPatternType, _regexpPatternType];

String _tr(String key) => translate(_LIB, key) as String;

void initialize() {
  registerShapeParsers([
      (b) => XInputQuestionEditor.fromBuffer(b),
      (b) => XPatternEditor.fromBuffer(b)]);

  registerQuestionDescriptor(QuestionDescriptor(
      type: InputQuestionType.input.name,
      title: _tr("Text Input Question"),
      priority: 2000,
      icon: "input_question_editor.resources/inputQuestion.png",
      initialText: ""));

  registerQuestionEditorFactory(
      XInputQuestionEditor,
      (name, dispatcher) => InputQuestionEditor(name, dispatcher));
}
