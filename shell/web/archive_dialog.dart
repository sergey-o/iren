/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';

class ArchiveDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-archive-dialog";

  DialogElement w_dialog;
  TextInputElement w_fileName;
  ButtonElement w_chooseButton;
  SpanElement w_note;
  ButtonElement w_okButton;

  final bool _remoteServer;
  final Node _dialogParent;

  ArchiveDialog(this._remoteServer, this._dialogParent) : super(TAG);

  Future<bool> openModal() async {
    String archiveLocationFile;

    if (_remoteServer) {
      w_fileName
          ..value = tr("Archive on the server")
          ..disabled = true;
      w_note.style.display = "none";
      w_chooseButton.disabled = true;
    } else {
      archiveLocationFile = path_join(dataDirectory, "archiveLocation");
      try {
        w_fileName.value = fs_readFileSync_String(archiveLocationFile, "utf8");
      } catch (_) {}
      w_chooseButton.onClick.listen((_) => _chooseFile());
      _updateUi();
    }

    await open(w_dialog, dialogParent: _dialogParent);

    bool res = w_dialog.returnValue.isNotEmpty;
    if (res && !_remoteServer) {
      writeViaTempFile(w_fileName.value, archiveLocationFile);
    }

    return res;
  }

  void _updateUi() {
    w_fileName.title = w_fileName.value;
    w_okButton.disabled = w_fileName.value.isEmpty;
  }

  void _chooseFile() {
    String defaultPath = w_fileName.value.isEmpty ? null : dropFileExtension(w_fileName.value, ["itar"]);
    String fileName = showSaveFileDialog("itar", tr("Archives"), defaultPathWithoutExtension: defaultPath);
    if (fileName != null) {
      w_fileName.value = fileName;
      _updateUi();
    }
  }
}
