/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_proto/editor/set_evaluation_model_modifier_editor.pb.dart';
import 'package:iren_proto/test.pb.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'registries.dart';

class SetEvaluationModelModifierEditor extends Widget<XSetEvaluationModelModifierEditor> {
  static const String TAG = "ru-irenproject-set-evaluation-model-modifier-editor";

  SelectElement w_modelType;
  DivElement w_questionTypeSwitchPanel;
  CheckboxInputElement w_questionTypeSwitch;
  DivElement w_questionTypePanel;

  SetEvaluationModelModifierEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_modelType.onChange.listen((_) => _onModelTypeChange());
    w_questionTypeSwitch.onClick.listen((_) => _onQuestionTypeSwitchClick());

    for (var d in questionDescriptors) {
      w_questionTypePanel.append(LabelElement()
          ..tabIndex = -1
          ..append(CheckboxInputElement()
              ..value = d.type
              ..onClick.listen((_) => _setQuestionTypes()))
          ..append(ImageElement()
              ..classes.add("icon")
              ..src = d.icon)
          ..appendText(d.title));
    }

    _setQuestionTypeVisibility(false);
    if (shape.singleQuestionModifier && shape.questionType.isEmpty) {
      w_questionTypeSwitchPanel.style.display = "none";
    }
  }

  void _setQuestionTypeVisibility(bool visible) {
    w_questionTypeSwitch.checked = visible;
    w_questionTypePanel.style.display = visible ? "" : "none";
  }

  void _onQuestionTypeSwitchClick() {
    if (!w_questionTypeSwitch.checked && shape.questionType.isNotEmpty) {
      performAction(XSetEvaluationModelModifierEditor_SetQuestionTypes());
    }
    _setQuestionTypeVisibility(w_questionTypeSwitch.checked);
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      (w_modelType.querySelector("[value=${shape.modelType}]") as OptionElement).selected = true;

      if (shape.questionType.isNotEmpty) {
        _setQuestionTypeVisibility(true);
      }

      for (var cb in w_questionTypePanel.querySelectorAll<CheckboxInputElement>("input")) {
        cb.checked = shape.questionType.contains(cb.value);
      }
    }
  }

  void _onModelTypeChange() {
    performAction(XSetEvaluationModelModifierEditor_SetModelType()
        ..modelType = getProtobufEnumByName(EvaluationModelType.values, w_modelType.selectedOptions.first.value));
  }

  void _setQuestionTypes() {
    performAction(XSetEvaluationModelModifierEditor_SetQuestionTypes()
        ..questionType.addAll(w_questionTypePanel.querySelectorAll(":checked")
            .map((e) => (e as CheckboxInputElement).value)));
  }

  @override void focusComponent() {
    w_modelType.focus();
  }
}

void initialize() {
  registerShapeParser((b) => XSetEvaluationModelModifierEditor.fromBuffer(b));

  registerModifierEditorFactory(
      XSetEvaluationModelModifierEditor,
      (name, dispatcher) => SetEvaluationModelModifierEditor(name, dispatcher));
}
