/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:math' as math;

import 'package:iren_proto/editor/editor.pb.dart';
import 'package:protobuf/protobuf.dart';

import 'common_dialogs.dart';
import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'registries.dart';

class QuestionListEditor extends Widget<XQuestionListEditor> {
  static const String TAG = "ru-irenproject-question-list-editor";

  ButtonElement w_addButton;
  ImageElement w_questionTypeImage;
  ButtonElement w_addDropDown;
  ButtonElement w_deleteButton;
  ButtonElement w_setWeightButton;
  ButtonElement w_setEnabledButton;
  ButtonElement w_moveUpButton;
  ButtonElement w_moveDownButton;
  ButtonElement w_modifiersButton;
  ButtonElement w_profilesButton;
  DivElement w_scrollable;
  TableElement w_table;
  TableSectionElement w_tableBody;

  List<XQuestionItemEditor> _rendered;
  QuestionDescriptor _selectedQuestionType;
  bool _scrollToSelectionStart = false;
  StreamSubscription<ClipboardEvent> _copySubscription;
  StreamSubscription<ClipboardEvent> _cutSubscription;
  StreamSubscription<ClipboardEvent> _pasteSubscription;

  final StreamController<void> _onAddQuestion = StreamController.broadcast(sync: true);
  Stream<void> get onAddQuestion => _onAddQuestion.stream;

  final StreamController<void> _onShowProfiles = StreamController.broadcast(sync: true);
  Stream<void> get onShowProfiles => _onShowProfiles.stream;

  ResizeObserver _headerResizeObserver;

  QuestionListEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addButton.onClick.listen((_) => _add());
    w_addDropDown.onClick.listen((_) => _onAddDropDownClick());
    w_deleteButton.onClick.listen((_) => _delete());
    w_setWeightButton.onClick.listen((_) => _setWeight());
    w_modifiersButton.onClick.listen((_) => _showModifiers());
    w_setEnabledButton.onClick.listen((_) => _setEnabled(_disabledItemsSelected()));
    w_moveUpButton.onClick.listen((_) => _moveUp());
    w_moveDownButton.onClick.listen((_) => _moveDown());
    w_profilesButton.onClick.listen((_) => _onShowProfiles.add(null));

    windowShortcutHandler
        ..add(KeyCode.F2, KeyModifierState.NONE, _add)
        ..add(KeyCode.F3, KeyModifierState.NONE, _setWeight)
        ..add(KeyCode.F4, KeyModifierState.NONE, _showModifiers);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.DELETE, KeyModifierState.NONE, _delete)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveDown)
        ..add(KeyCode.A, KeyModifierState.CTRL, _selectAll);

    ShortcutHandler(w_scrollable)
        ..add(KeyCode.UP, KeyModifierState.NONE, () => _shiftSingleSelection(-1))
        ..add(KeyCode.DOWN, KeyModifierState.NONE, () => _shiftSingleSelection(1))
        ..add(KeyCode.PAGE_UP, KeyModifierState.NONE, () => _shiftSingleSelection(-_rowsVisible()))
        ..add(KeyCode.PAGE_DOWN, KeyModifierState.NONE, () => _shiftSingleSelection(_rowsVisible()))
        ..add(KeyCode.HOME, KeyModifierState.NONE, _handleHome)
        ..add(KeyCode.HOME, KeyModifierState.CTRL, () {})
        ..add(KeyCode.END, KeyModifierState.NONE, _handleEnd)
        ..add(KeyCode.END, KeyModifierState.CTRL, () {})
        ..add(KeyCode.SPACE, KeyModifierState.NONE, () {})
        ..add(KeyCode.SPACE, KeyModifierState.SHIFT, () {});

    if (_lastSelectedQuestionType == null) {
      if (questionDescriptors.isNotEmpty) {
        _selectQuestionType(questionDescriptors.first);
      }
    } else {
      _selectQuestionType(_lastSelectedQuestionType);
    }

    _headerResizeObserver = ResizeObserver((_, __) => _onHeaderResize())
        ..observe(w_table.tHead);

    w_tableBody.onMouseDown.listen(_onTableBodyMouseDown);
    w_scrollable.onContextMenu.listen(_onContextMenu);
  }

  @override void attached() {
    super.attached();
    _copySubscription = document.onCopy.listen((e) => _copyOrCut(e, XQuestionListEditor_Copy()));
    _cutSubscription = document.onCut.listen((e) => _copyOrCut(e, XQuestionListEditor_Cut()));
    _pasteSubscription = document.onPaste.listen(_paste);
  }

  @override void detached() {
    _copySubscription?.cancel();
    _copySubscription = null;
    _cutSubscription?.cancel();
    _cutSubscription = null;
    _pasteSubscription?.cancel();
    _pasteSubscription = null;
    _headerResizeObserver.disconnect();
    super.detached();
  }

  @override void render() {
    super.render();
    if (shapeChanged) {
      int rowsNeeded = shape.itemEditor.length;

      if (w_tableBody.rows.length < rowsNeeded) {
        for (int i = w_tableBody.rows.length + 1; i <= rowsNeeded; ++i) {
          w_tableBody.addRow()
              ..addCell().append(DivElement()
                  ..classes.add("typeIcon"))
              ..addCell().text = i.toString()
              ..addCell()
              ..addCell();
        }
      } else {
         for (int rowsToDelete = w_tableBody.rows.length - rowsNeeded, i = 0; i < rowsToDelete; ++i) {
           w_tableBody.deleteRow(-1);
         }
      }

      _rendered = List.filled(shape.itemEditor.length, null);
    }

    List<int> selectedIndices = _selection().selectedIndex;
    int s = 0;

    shape.itemEditor.asMap().forEach((index, name) {
      XQuestionItemEditor itemEditor = scene.shapes(name);
      TableRowElement row = w_tableBody.rows[index];

      if (!identical(itemEditor, _rendered[index])) {
        _rendered[index] = itemEditor;
        QuestionDescriptor d = getQuestionDescriptor(itemEditor.type);
        row.cells.first.children.first
            ..style.backgroundImage = "url(${d.icon})"
            ..classes.toggle("hasModifiers", itemEditor.hasModifiers)
            ..title = d.title;
        row
            ..cells[2].text = itemEditor.text
            ..cells[3].text = itemEditor.weight.toString()
            ..title = itemEditor.text
            ..classes.toggle("disabled", !itemEditor.enabled);
      }

      bool selected = (s < selectedIndices.length) && (selectedIndices[s] == index);
      if (selected) {
        if ((s == 0) && (_scrollToSelectionStart || shapeChanged)) {
          scheduleScrollIntoView(row);
        }
        ++s;
      }
      row.classes.toggle("selected", selected);
    });

    w_deleteButton.disabled = w_setWeightButton.disabled = w_setEnabledButton.disabled =
        _selection().selectedIndex.isEmpty;
    w_moveUpButton.disabled = !_canMoveUp();
    w_moveDownButton.disabled = !_canMoveDown();
    w_modifiersButton.disabled = (_selection().selectedIndex.length != 1);

    w_setEnabledButton.classes.toggle("highlighted", _disabledItemsSelected());

    _scrollToSelectionStart = false;
  }

  XQuestionListSelection _selection() => scene.shapes(shape.selection);

  void _onTableBodyMouseDown(MouseEvent e) {
    int n = e.path.indexOf(w_tableBody);
    if (n > 0) {
      _handleRowMouseDown(e.path[n - 1] as TableRowElement, e);
    }
  }

  Future<void> _handleRowMouseDown(TableRowElement row, MouseEvent e) async {
    if ((e.button == 0) || ((e.button == 2) && !row.classes.contains("selected"))) {
      int index = row.sectionRowIndex;

      if (e.ctrlKey) {
        var indices = SplayTreeSet<int>.from(_selection().selectedIndex);
        if (!indices.add(index)) {
          indices.remove(index);
        }
        await _select(indices);
      } else if (e.shiftKey) {
        if (_selection().selectedIndex.length == 1) {
          int s = _selection().selectedIndex.first;
          if (index < s) {
            await _select(intRange(index, s + 1));
          } else if (index > s) {
            await _select(intRange(s, index + 1));
          }
        }
      } else {
        await _select([index]);
      }

      if (e.button == 2) {
        _showContextMenu();
      }
    }
  }

  Future<void> _select(Iterable<int> sortedIndices) async {
    await performAction(XQuestionListEditor_Select()
        ..index.addAll(sortedIndices));
    await dispatcher.onChangeScene.first;
  }

  void _shiftSingleSelection(int offset) {
    if (_selection().selectedIndex.length == 1) {
      _select([(_selection().selectedIndex.first + offset).clamp(0, shape.itemEditor.length - 1) as int]);
      _scrollToSelectionStart = true;
    }
  }

  int _rowsVisible() {
    int res;
    if (w_tableBody.rows.isEmpty) {
      res = 0;
    } else {
      int h = w_tableBody.rows.first.offsetHeight;
      res = (h == 0) ? 0 : math.max(w_scrollable.clientHeight - w_table.tHead.offsetHeight, 0) ~/ h;
    }
    return res;
  }

  void _onHeaderResize() {
    w_scrollable.style.setProperty("scroll-padding-top", "${w_table.tHead.offsetHeight}px");
  }

  void _handleHome() {
    if (shape.itemEditor.isNotEmpty) {
      _select([0]);
      _scrollToSelectionStart = true;
    }
  }

  void _handleEnd() {
    if (shape.itemEditor.isNotEmpty) {
      _select([shape.itemEditor.length - 1]);
      _scrollToSelectionStart = true;
    }
  }

  void _onAddDropDownClick() {
    showPopupMenu(_createAddQuestionMenu(), getDropDownMenuPosition(w_addButton));
  }

  Menu _createAddQuestionMenu() {
    var res = Menu();

    for (var d in questionDescriptors) {
      res.append(MenuItem(MenuItemOptions(
          click: toMenuHandler(() {
            _selectQuestionType(d);
            _add();
          }),
          label: d.title,
          icon: getAbsoluteAppPath(d.icon),
          accelerator: (d == _selectedQuestionType) ? "F2" : null)));
    }

    return res;
  }

  void _selectQuestionType(QuestionDescriptor d) {
    _selectedQuestionType = d;
    w_questionTypeImage.src = d.icon;
    w_addButton.title = trD("ADD_QUESTION")(
        (tr("USE_TITLE_CASE") == "true") ? d.title : d.title.toLowerCase()) as String;

    _lastSelectedQuestionType = d;
  }

  void _add() {
    if (_selectedQuestionType != null) {
      performAction(XQuestionListEditor_Add()
          ..type = _selectedQuestionType.type
          ..initialText = _selectedQuestionType.initialText);
      _onAddQuestion.add(null);
    }
  }

  void _delete() {
    if (_selection().selectedIndex.isNotEmpty) {
      performAction(XQuestionListEditor_Delete());
    }
  }

  void _selectAll() {
    _select(intRange(0, shape.itemEditor.length));
  }

  void _moveUp() {
    if (_canMoveUp()) {
      performAction(XQuestionListEditor_Move()
          ..forward = false);
    }
  }

  bool _canMoveUp() => _selection().selectedIndex.isNotEmpty && (_selection().selectedIndex.first > 0);

  void _moveDown() {
    if (_canMoveDown()) {
      performAction(XQuestionListEditor_Move()
          ..forward = true);
    }
  }

  bool _canMoveDown() => _selection().selectedIndex.isNotEmpty
      && (_selection().selectedIndex.last < shape.itemEditor.length - 1);

  Future<void> _setWeight() async {
    if (_selection().selectedIndex.isNotEmpty) {
      int oldWeight = _selectedItems().first.weight;
      if (!_selectedItems().every((item) => item.weight == oldWeight)) {
        oldWeight = null;
      }

      int newWeight = await inputInteger(prompt: tr("Question weight:"), min: 1, max: shape.maxWeight,
          initial: oldWeight);

      if (newWeight != null) {
        performAction(XQuestionListEditor_SetWeight()
            ..weight = newWeight);
      }
    }
  }

  Iterable<XQuestionItemEditor> _selectedItems() => _selection().selectedIndex
      .map((index) => scene.shapes(shape.itemEditor[index]));

  void _setEnabled(bool enabled) {
    performAction(XQuestionListEditor_SetEnabled()
        ..enabled = enabled);
  }

  bool _disabledItemsSelected() => _selectedItems().any((item) => !item.enabled);

  bool _enabledItemsSelected() => _selectedItems().any((item) => item.enabled);

  void _showModifiers() {
    if (_selection().selectedIndex.length == 1) {
      dispatcher.performAction(shape.itemEditor[_selection().selectedIndex.first],
          XQuestionItemEditor_ShowModifiers());
    }
  }

  void _copyOrCut(ClipboardEvent e, GeneratedMessage action) {
    if (element.shadowRoot.activeElement != null) {
      stopFurtherProcessing(e);
      if (_selection().selectedIndex.isNotEmpty) {
        performAction(action);
        e.clipboardData.setData(QUESTION_LIST_MIME_TYPE, "");
      }
    }
  }

  void _paste(ClipboardEvent e) {
    if (element.shadowRoot.activeElement != null) {
      stopFurtherProcessing(e);
      pasteQuestions(e);
    }
  }

  void pasteQuestions(ClipboardEvent e) {
    if (e.clipboardData.types.contains(QUESTION_LIST_MIME_TYPE)) {
      performAction(XQuestionListEditor_Paste());
    }
  }

  void _onContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);
    _showContextMenu();
  }

  void _showContextMenu() {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            label: tr("Add"),
            submenu: _createAddQuestionMenu())))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _executeCommand("cut")),
            label: tr("Cut"),
            enabled: _selection().selectedIndex.isNotEmpty,
            accelerator: "CmdOrCtrl+X")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _executeCommand("copy")),
            label: tr("Copy"),
            enabled: _selection().selectedIndex.isNotEmpty,
            accelerator: "CmdOrCtrl+C")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _executeCommand("paste")),
            label: tr("Paste"),
            enabled: clipboard_availableFormats().contains(QUESTION_LIST_MIME_TYPE),
            accelerator: "CmdOrCtrl+V")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_delete),
            label: tr("Delete"),
            icon: getAbsoluteAppPath("question_list_editor.resources/delete.png"),
            enabled: !w_deleteButton.disabled,
            accelerator: "Delete")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_selectAll),
            label: tr("Select All"),
            enabled: shape.itemEditor.isNotEmpty,
            accelerator: "CmdOrCtrl+A")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_setWeight),
            label: tr("Set Weight..."),
            icon: getAbsoluteAppPath("question_list_editor.resources/setWeight.png"),
            enabled: !w_setWeightButton.disabled,
            accelerator: "F3")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_showModifiers),
            label: tr("Modifiers"),
            icon: getAbsoluteAppPath("question_list_editor.resources/modifiers.png"),
            enabled: !w_modifiersButton.disabled,
            accelerator: "F4")))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _setEnabled(false)),
            label: tr("Forbid Use"),
            icon: getAbsoluteAppPath("question_list_editor.resources/setEnabled.png"),
            enabled: _enabledItemsSelected())))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => _setEnabled(true)),
            label: tr("Allow Use"),
            enabled: _disabledItemsSelected())))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveUp),
            label: tr("Move Up"),
            icon: getAbsoluteAppPath("question_list_editor.resources/moveUp.png"),
            enabled: !w_moveUpButton.disabled,
            accelerator: "Alt+Up")))
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_moveDown),
            label: tr("Move Down"),
            icon: getAbsoluteAppPath("question_list_editor.resources/moveDown.png"),
            enabled: !w_moveDownButton.disabled,
            accelerator: "Alt+Down")));

    showPopupMenu(menu, MenuPopupOptions());
  }

  void _executeCommand(String command) {
    w_scrollable.focus();
    document.execCommand(command);
  }
}

QuestionDescriptor _lastSelectedQuestionType;

void initialize() {
  registerShapeParsers([
      (b) => XQuestionListEditor.fromBuffer(b),
      (b) => XQuestionListSelection.fromBuffer(b),
      (b) => XQuestionItemEditor.fromBuffer(b)]);
}
