/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:js';
import 'dart:typed_data';

import 'package:convert/convert.dart' show hex;
import 'package:iren_proto/editor/archive_editor.pb.dart';
import 'package:iren_proto/editor/editor.pb.dart';

import 'archive_editor.dart';
import 'common_dialogs.dart';
import 'editor_infra.dart';
import 'electron.dart';
import 'file_manager.dart';
import 'misc_utils.dart';
import 'test_document_editor.dart';

class EditorMainScreen extends WebComponent {
  static const String TAG = "ru-irenproject-editor-main-screen";

  static final Map<Type, _DocumentEditorFactory> _documentEditors = {
      XTestDocumentEditor: (name, dispatcher) => TestDocumentEditor(name, dispatcher),
      XArchiveEditor: (name, dispatcher) => ArchiveEditor(name, dispatcher)};

  static Future<EditorMainScreen> create(PlainEditorConnection connection, Element host) async {
    var res = EditorMainScreen._create(connection);
    await res._initialize(host);
    return res;
  }

  DivElement w_main;

  final PlainEditorConnection _connection;
  FileManager _fileManager;
  final Map<String, DocumentEditor> _editorsByChannel = {};
  /* nullable */DocumentEditor _currentEditor;

  EditorMainScreen._create(this._connection) : super(TAG);

  Future<void> _initialize(Element host) async {
    _fileManager = await FileManager.create(_connection)
        ..element.id = "fileManager"
        ..channelDispatcherFactory = _createChannelDispatcher
        ..onSelectChannel.listen((_) => _showEditor())
        ..onCloseChannel.listen(_closeEditor)
        ..onUpdateTitle.listen((_) => _updateTitle())
        ..onAfterSave.listen((_) => _currentEditor?.markAsUnmodified());

    host.append(element);

    await _fileManager.activate();
    findElement("fileManagerPlaceholder").replaceWith(_fileManager.element);

    if ((remote_process_argv.length >= 2) && !remote_process_argv[1].startsWith("-")) {
      await _fileManager.openFile(path_resolve(remote_process_argv[1]));
    } else {
      await _fileManager.createAutoClosableFileIfEmpty();
    }

    ipcRenderer_on("openFile", allowInterop((_, [dynamic fileName]) {
      if (!modalDialogOpen && !uiLocked) {
        _fileManager.openFile(fileName as String);
      }
    }));

    ipcRenderer_on("requestClose", allowInterop((_, [__]) => _closeWindow()));
    ipcRenderer_sendSync("relayCloseRequests");
  }

  void _showEditor() {
    String newChannel = _fileManager.selectedChannel;
    if (_currentEditor?.dispatcher?.channel != newChannel) {
      _currentEditor?.element?.style?.display = "none";
      _currentEditor = null;

      if (newChannel != null) {
        _currentEditor = _editorsByChannel.putIfAbsent(newChannel, () => _createEditor(newChannel))
            ..element.style.display = "";
      }
    }

    _updateFileManager();
  }

  DocumentEditor _createEditor(String channel) {
    Dispatcher editorDispatcher = _fileManager.getDispatcher(channel);

    _DocumentEditorFactory factory = _getDocumentEditorFactory(
        editorDispatcher.scene.shapes(DocumentEditor.WIDGET_NAME).runtimeType);
    DocumentEditor res = factory(DocumentEditor.WIDGET_NAME, editorDispatcher);
    w_main.append(res.element);

    res.enableRendering(afterRender: () {
      _fileManager.removeAutoClosableMark();
      _updateFileManager();
    });

    return res;
  }

  static _DocumentEditorFactory _getDocumentEditorFactory(Type shapeClass) {
    _DocumentEditorFactory res = _documentEditors[shapeClass];
    if (res == null) {
      throw "No document editor for '$shapeClass'.";
    }
    return res;
  }

  void _closeEditor(String channel) {
    DocumentEditor editor = _editorsByChannel.remove(channel);
    if (editor != null) {
      editor.element.remove();
      if (identical(_currentEditor, editor)) {
        _currentEditor = null;
      }
    }
  }

  void _updateTitle() {
    String title = _fileManager.selectedFileTitle;
    document.title = (title == null) ? tr("Iren") : trD("WINDOW_TITLE")(title) as String;
  }

  void _updateFileManager() {
    _fileManager
        ..selectedFileModified = (_currentEditor != null) && _currentEditor.modified
        ..selectedFileExportableToHtml = (_currentEditor != null) && _currentEditor.exportableToHtml;
  }

  Future<Dispatcher> _createChannelDispatcher(String channel) async {
    var res = Dispatcher(_connection, Scene(), channel);
    try {
      await DocumentEditor.loadScene(res, disableUi: false);
    } catch (_) {
      res.stop();
      rethrow;
    }
    return res;
  }

  Future<void> _closeWindow() async {
    if (_connection.connected) {
      if (!modalDialogOpen) {
        if (uiLocked) {
          if (uiDimmed) {
            getCurrentWindow().destroy();
          }
        } else {
          await runWindowCloseListeners();
          if (await _fileManager.closeAll()) {
            getCurrentWindow().destroy();
          }
        }
      }
    } else {
      getCurrentWindow().destroy();
    }
  }
}

typedef _DocumentEditorFactory = DocumentEditor Function(String name, Dispatcher dispatcher);

void runEditor() {
  ipcRenderer_once("connectionConfig", allowInterop(
      (_, [dynamic connectionConfig]) => _connect(connectionConfig as String)));
  ipcRenderer_send("getConnectionConfig");
}

Future<void> _connect(String connectionConfig) async {
  dynamic config = jsonDecode(connectionConfig);

  var socket = WebSocket("ws://127.0.0.1:${config["port"]}/websocket/")
      ..binaryType = "arraybuffer"
      ..onClose.listen((_) => ConnectionLostDialog().openModal());
  await socket.onOpen.first;
  socket.sendTypedData(Uint8List.fromList(hex.decode(config["editorKey"] as String)));

  await EditorMainScreen.create(PlainEditorConnection(socket), document.body);
}
