/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/editor/order_question_editor.pb.dart';
import 'package:iren_proto/question/order_question.pb.dart';
import 'package:web_helpers/translator.dart';

import 'editor_infra.dart';
import 'misc_utils.dart';
import 'pad_editor.dart';
import 'registries.dart';

class OrderQuestionEditor extends Widget<XOrderQuestionEditor> {
  static const String TAG = "ru-irenproject-order-question-editor";

  DivElement w_toolbar;
  ButtonElement w_addElementButton;
  ButtonElement w_deleteElementButton;
  ButtonElement w_moveElementUpButton;
  ButtonElement w_moveElementDownButton;
  ButtonElement w_optionsButton;
  ButtonElement w_itemsButton;
  ButtonElement w_distractorsButton;
  DivElement w_elementPanel;

  PadEditor _formulation;

  /* nullable */int _selectElement;
  /* nullable */int _focusedElement;

  OrderQuestionEditor(String name, Dispatcher dispatcher)
      : super(TAG, name, dispatcher) {
    w_addElementButton.onClick.listen((_) => _addElement());
    w_deleteElementButton.onClick.listen((_) => _deleteElement());
    w_moveElementUpButton.onClick.listen((_) => _moveElementUp());
    w_moveElementDownButton.onClick.listen((_) => _moveElementDown());
    w_optionsButton.onClick.listen((_) => _showOptionsDialog());

    windowShortcutHandler
        ..add(KeyCode.F5, KeyModifierState.NONE, _addElement)
        ..add(KeyCode.F8, KeyModifierState.NONE, _deleteElement);

    ShortcutHandler(element.shadowRoot)
        ..add(KeyCode.UP, KeyModifierState.ALT, _moveElementUp)
        ..add(KeyCode.DOWN, KeyModifierState.ALT, _moveElementDown);

    w_itemsButton.onClick.listen((_) => _selectPage(Page.ITEMS));
    w_distractorsButton.onClick.listen((_) => _selectPage(Page.DISTRACTORS));

    _formulation = PadEditor(shape.formulationEditor, dispatcher)
        ..element.id = "formulation";
    findElement("formulationPlaceholder").replaceWith(_formulation.element);

    Element.focusEvent.forTarget(element.shadowRoot, useCapture: true).listen((_) => _onFocus());
    Element.blurEvent.forTarget(element.shadowRoot, useCapture: true).listen(_onBlur);
  }

  @override void render() {
    super.render();
    _formulation.render();

    if (shapeChanged) {
      _focusedElement = null;

      w_itemsButton.classes
          ..toggle("selected", shape.page == Page.ITEMS)
          ..toggle("notEmpty", shape.itemCount > 0);
      w_distractorsButton.classes
          ..toggle("selected", shape.page == Page.DISTRACTORS)
          ..toggle("notEmpty", shape.distractorCount > 0);

      w_elementPanel.nodes = shape.elementEditor.map((name) => PadEditor(name, dispatcher).element);

      w_addElementButton.disabled = !_canAddElement();
      w_optionsButton.classes.toggle("edited", !shape.hasDefaultOptions);
    }

    for (var e in w_elementPanel.children) {
      getWidget(e).render();
    }

    if ((_selectElement != null) && shape.elementEditor.isNotEmpty) {
      getWidget(w_elementPanel.children[_selectElement.clamp(0, shape.elementEditor.length - 1) as int])
          .focusComponent();
    }
    _selectElement = null;

    _updateUi();
  }

  void _selectPage(Page page) {
    performAction(XOrderQuestionEditor_SelectPage()
        ..page = page);
  }

  void _addElement() {
    if (_canAddElement()) {
      performAction(XOrderQuestionEditor_AddElement());
      _selectElement = shape.elementEditor.length;
    }
  }

  bool _canAddElement() => shape.elementEditor.length < shape.maxElements;

  void _deleteElement() {
    if (_focusedElement != null) {
      performAction(XOrderQuestionEditor_DeleteElement()
          ..index = _focusedElement);
      _selectElement = _focusedElement;
    }
  }

  void _onFocus() {
    int index = w_elementPanel.children.indexOf(element.shadowRoot.activeElement);
    if (index != -1) {
      _focusedElement = index;
    }
    _updateUi();
  }

  void _onBlur(Event e) {
    if (!w_toolbar.contains((e as FocusEvent).relatedTarget as Node)) {
      _focusedElement = null;
    }
    _updateUi();
  }

  void _updateUi() {
    w_deleteElementButton.disabled = (_focusedElement == null);
    w_moveElementUpButton.disabled = !_canMoveElementUp();
    w_moveElementDownButton.disabled = !_canMoveElementDown();
  }

  void _moveElementUp() {
    if (_canMoveElementUp()) {
      performAction(XOrderQuestionEditor_MoveElement()
          ..index = _focusedElement
          ..forward = false);
      _selectElement = _focusedElement - 1;
    }
  }

  void _moveElementDown() {
    if (_canMoveElementDown()) {
      performAction(XOrderQuestionEditor_MoveElement()
          ..index = _focusedElement
          ..forward = true);
     _selectElement = _focusedElement + 1;
    }
  }

  bool _canMoveElementUp() => (_focusedElement != null) && (_focusedElement > 0);

  bool _canMoveElementDown() => (_focusedElement != null) && (_focusedElement < shape.elementEditor.length - 1);

  void _showOptionsDialog() {
    OrderQuestionOptionsDialog(shape, dispatcher, name).openModal();
  }

  @override void focusComponent() {
    _formulation.focusComponent();
  }
}

class OrderQuestionOptionsDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-order-question-options-dialog";

  DialogElement w_dialog;
  RadioButtonInputElement w_randomItems;
  NumberInputElement w_itemLimit;
  RadioButtonInputElement w_randomDistractors;
  NumberInputElement w_distractorLimit;

  final XOrderQuestionEditor _source;
  final Dispatcher _dispatcher;
  final String _targetName;

  OrderQuestionOptionsDialog(this._source, this._dispatcher, this._targetName) : super(TAG);

  Future<void> openModal() async {
    w_itemLimit.max = w_distractorLimit.max = _source.maxElements.toString();

    if (_source.hasItemLimit()) {
      w_randomItems.checked = true;
      w_itemLimit.valueAsNumber = _source.itemLimit;
    }

    if (_source.hasDistractorLimit()) {
      w_randomDistractors.checked = true;
      w_distractorLimit.valueAsNumber = _source.distractorLimit;
    }

    w_randomItems.onClick.listen((_) => focusInputElement(w_itemLimit));
    w_randomDistractors.onClick.listen((_) => focusInputElement(w_distractorLimit));

    _updateUi();

    w_dialog
        ..onChange.listen((_) => _updateUi())
        ..onClick.listen(_onDialogClick);

    await open(w_dialog);

    if (w_dialog.returnValue.isNotEmpty) {
      var action = XOrderQuestionEditor_SetOptions();
      if (w_randomItems.checked) {
        action.itemLimit = w_itemLimit.valueAsNumber.toInt();
      }
      if (w_randomDistractors.checked) {
        action.distractorLimit = w_distractorLimit.valueAsNumber.toInt();
      }
      await _dispatcher.performAction(_targetName, action);
    }
  }

  void _updateUi() {
    w_itemLimit
        ..required = w_randomItems.checked
        ..disabled = !w_randomItems.checked;
    w_distractorLimit
        ..required = w_randomDistractors.checked
        ..disabled = !w_randomDistractors.checked;
  }

  void _onDialogClick(MouseEvent e) {
    if ((e.target == w_itemLimit) && w_itemLimit.disabled) {
      w_randomItems.dispatchEvent(MouseEvent("click"));
    } else if ((e.target == w_distractorLimit) && w_distractorLimit.disabled) {
      w_randomDistractors.dispatchEvent(MouseEvent("click"));
    }
  }
}

const String _LIB = "order_question_editor";

void initialize() {
  registerShapeParser((b) => XOrderQuestionEditor.fromBuffer(b));

  registerQuestionDescriptor(QuestionDescriptor(
      type: OrderQuestionType.order.name,
      title: translate(_LIB, "Ordering Question") as String,
      priority: 4000,
      icon: "order_question_editor.resources/orderQuestion.png",
      initialText: translate(_LIB, "Order the items.") as String));

  registerQuestionEditorFactory(
      XOrderQuestionEditor,
      (name, dispatcher) => OrderQuestionEditor(name, dispatcher));
}
