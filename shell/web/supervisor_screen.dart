/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_proto/archive_manager.pb.dart';
import 'package:iren_proto/editor/profile_screen.pb.dart';
import 'package:iren_proto/login.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/work_controller.pb.dart';
import 'package:iren_proto/work_creator.pb.dart';

import 'about_dialog.dart';
import 'editor_infra.dart';
import 'electron.dart';
import 'iren_utils.dart';
import 'misc_utils.dart';
import 'options_dialog.dart';
import 'profile_screen.dart';
import 'users_dialog.dart';

class SupervisorScreen extends WebComponent {
  static const String TAG = "ru-irenproject-supervisor-screen";

  static const String _WORK_ID_ATTRIBUTE = "id";

  ButtonElement w_assign;
  ButtonElement w_workMenuButton;
  ButtonElement w_toolsMenuButton;
  DivElement w_serverAddressPanel;
  SpanElement w_serverAddress;
  ButtonElement w_about;
  DivElement w_scrollable;
  TableElement w_table;
  TableSectionElement w_tableBody;

  final Connection _connection;
  final bool _remoteServer;
  final Int64 _userManagerChannelId;
  final Int64 _workCreatorChannelId;
  final Int64 _workControllerChannelId;
  final Int64 _archiveManagerChannelId;

  /* nullable */String _initiallySelectedWorkId;

  final Completer<String> _activatedWorkId = Completer();
  Future<String> get activatedWorkId => _activatedWorkId.future;

  StreamSubscription<ServerMessage> _workListSubscription;
  /* nullable */String _selectedWorkId;

  SupervisorScreen(Supervise_Reply_Ok message, this._connection, this._remoteServer, this._initiallySelectedWorkId)
      : _userManagerChannelId = message.userManagerChannelId,
        _workCreatorChannelId = message.workCreatorChannelId,
        _workControllerChannelId = message.workControllerChannelId,
        _archiveManagerChannelId = message.archiveManagerChannelId,
        super(TAG) {
    w_assign.onClick.listen((_) => _assign());

    w_workMenuButton.onClick.listen((_) => _onWorkMenuButtonClick());
    w_toolsMenuButton.onClick.listen((_) => _onToolsMenuButtonClick());

    w_about.onClick.listen((_) => AboutDialog().openModal());

    w_tableBody.onMouseDown.listen(_onTableBodyMouseDown);

    w_scrollable.onContextMenu.listen(_onScrollableContextMenu);
    w_table.tHead.onContextMenu.listen(stopFurtherProcessing);

    if (message.hasServerAddress()) {
      w_serverAddress.text = message.serverAddress;
    } else {
      w_serverAddressPanel.remove();
    }

    Int64 noticeChannelId = _sendWorkControllerRequest(WorkControllerRequest()
        ..watchWorkList = WatchWorkList());
    _workListSubscription = _connection.tune(noticeChannelId)
        .listen((m) => _handleWorkList(m.workControllerNotice.workList));
  }

  @override void detached() {
    _workListSubscription.cancel();
    _sendWorkControllerRequest(WorkControllerRequest()
        ..unwatchWorkList = UnwatchWorkList());
    super.detached();
  }

  Int64 _sendWorkControllerRequest(WorkControllerRequest request) => _connection.send(ClientMessage()
      ..channelId = _workControllerChannelId
      ..workControllerRequest = request);

  void _handleWorkList(WorkList m) {
    w_tableBody.nodes.clear();
    String newSelectedWorkId = null;

    for (var item in m.item) {
      TableRowElement row = w_tableBody.addRow()
          ..dataset[_WORK_ID_ATTRIBUTE] = item.id
          ..addCell().text = item.title
          ..addCell().text = formatTimestamp(item.startedAt, includeSeconds: false);

      if (item.id == _initiallySelectedWorkId) {
        newSelectedWorkId = _initiallySelectedWorkId;
        scheduleScrollIntoView(row);
      } else if (item.id == _selectedWorkId) {
        newSelectedWorkId = _selectedWorkId;
      }
    }

    _initiallySelectedWorkId = null;
    _selectedWorkId = newSelectedWorkId;

    _renderSelection();
  }

  void _renderSelection() {
    for (var row in w_tableBody.rows) {
      row.classes.toggle("selected", row.dataset[_WORK_ID_ATTRIBUTE] == _selectedWorkId);
    }
  }

  void _onTableBodyMouseDown(MouseEvent e) {
    int n = e.path.indexOf(w_tableBody);
    if (n > 0) {
      _handleRowMouseDown(e.path[n - 1] as TableRowElement, e);
    }
  }

  void _handleRowMouseDown(TableRowElement row, MouseEvent e) {
    String workId = row.dataset[_WORK_ID_ATTRIBUTE];
    if (((e.button == 0) && (e.detail == 1)) || ((e.button == 2) && (workId != _selectedWorkId))) {
      _selectedWorkId = workId;
      _renderSelection();
    } else if ((e.button == 0) && (e.detail == 2) && (workId == _selectedWorkId)) {
      _open();
    }
  }

  void _onWorkMenuButtonClick() {
    _showWorkPopupMenu(getDropDownMenuPosition(w_workMenuButton));
  }

  void _showWorkPopupMenu(MenuPopupOptions options) {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_open),
            label: tr("Open"),
            enabled: _selectedWorkId != null)))
        ..append(createMenuSeparator())
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(_assign),
            label: tr("Assign..."),
            icon: getAbsoluteAppPath("supervisor_screen.resources/assign.png"))));

    showPopupMenu(menu, options);
  }

  void _onScrollableContextMenu(MouseEvent e) {
    stopFurtherProcessing(e);
    _showWorkPopupMenu(MenuPopupOptions());
  }

  void _open() {
    if ((_selectedWorkId != null) && !_activatedWorkId.isCompleted) {
      _activatedWorkId.complete(_selectedWorkId);
    }
  }

  Future<void> _assign() async {
    for (String fileName in showOpenTestDialog()) {
      Uint8List data = null;
      try {
        data = fs_readFileSync(fileName);
      } catch (e) {
        showErrorMessage(e.toString());
      }

      if (data != null) {
        String title = getFileTitle(fileName, TEST_EXTENSIONS);

        BeginCreateWork_Reply_Result result = (await _performWorkCreatorRequest(WorkCreatorRequest()
            ..beginCreateWork = (BeginCreateWork()
                ..test = data
                ..title = title
                ..language = window.navigator.language
                ..defaultProfileTitle = tr("[new]")))).beginCreateWorkReply.result;

        if (result == BeginCreateWork_Reply_Result.OK) {
          var editorConnection = EncapsulatedEditorConnection(_connection,
              (request) => _wrapWorkCreatorRequest(WorkCreatorRequest()
                  ..performProfileSelectorAction = (PerformProfileSelectorAction()
                      ..request = request)),
              (m) => m.workCreatorReply.performProfileSelectorActionReply.reply);

          bool commit;
          var dispatcher = Dispatcher(editorConnection, Scene(), "");
          try {
            commit = await CreateWorkDialog(dispatcher, title).openModal();
          } finally {
            dispatcher.stop();
          }

          await _performWorkCreatorRequest(WorkCreatorRequest()
              ..completeCreateWork = (CompleteCreateWork()
                  ..commit = commit));

          if (!commit) {
            break;
          }
        } else {
          showErrorMessage((result == BeginCreateWork_Reply_Result.UNKNOWN_TEST_FILE_VERSION) ?
              getUnknownFileVersionMessage(fileName) : trD("CANNOT_LOAD_TEST")(fileName) as String);
        }
      }
    }
  }

  Future<WorkCreatorReply> _performWorkCreatorRequest(WorkCreatorRequest request) async {
    lockUi();
    try {
      return (await _connection.perform(_wrapWorkCreatorRequest(request))).workCreatorReply;
    } finally {
      unlockUi();
    }
  }

  ClientMessage _wrapWorkCreatorRequest(WorkCreatorRequest request) => ClientMessage()
      ..channelId = _workCreatorChannelId
      ..workCreatorRequest = request;

  void _onToolsMenuButtonClick() {
    var menu = Menu()
        ..append(MenuItem(MenuItemOptions(
            click: toMenuHandler(() => UsersDialog(_connection, _userManagerChannelId).openModal()),
            label: tr("Users..."))));

    if (_remoteServer) {
      menu.append(MenuItem(MenuItemOptions(
          click: toMenuHandler(_downloadArchive),
          label: tr("Download Archive..."))));
    }

    menu.append(MenuItem(MenuItemOptions(
        click: toMenuHandler(() => OptionsDialog(_connection, _userManagerChannelId).openModal()),
        label: tr("Options..."))));

    showPopupMenu(menu, getDropDownMenuPosition(w_toolsMenuButton));
  }

  Future<void> _downloadArchive() async {
    String fileName = showSaveFileDialog("itar", tr("Archives"));
    if (fileName != null) {
      lockUi();
      try {
        String tempFileName = generateTempFileName(fileName);
        int fd = fs_openSync(tempFileName, "wx");
        bool closeTempFile = true;
        bool deleteTempFile = true;
        GetArchive_Reply reply;
        try {
          Int64 requestId = _sendArchiveManagerRequest(ArchiveManagerRequest()
              ..getArchive = GetArchive());
          reply = (await _connection.tune(requestId).first).archiveManagerReply.getArchiveReply;

          if (reply.archiveAvailable) {
            await _receiveArchive(_connection.emitErrorOnClose(
                _connection.tune(requestId).map((m) => m.archiveManagerNotice)), fd);
            closeTempFile = false;
            fs_closeSync(fd);
            fs_renameSync(tempFileName, fileName);
            deleteTempFile = false;
          }
        } finally {
          if (closeTempFile) {
            try {
              fs_closeSync(fd);
            } catch (_) {}
          }
          if (deleteTempFile) {
            try {
              fs_unlinkSync(tempFileName);
            } catch (_) {}
          }
        }

        if (!reply.archiveAvailable) {
          showInformationMessage(tr("There is no archive data on the server."));
        }
      } catch (e) {
        if (e is! ClosedConnectionException) {
          showErrorMessage(e.toString());
        }
      } finally {
        unlockUi();
      }
    }
  }

  Future<void> _receiveArchive(Stream<ArchiveManagerNotice> noticeStream, int targetFileDescriptor) async {
    Object exception = null;

    await for (var notice in noticeStream) {
      if (notice.hasArchivePart()) {
        if (exception == null) {
          try {
            writeToFile(castOrCopyToUint8List(notice.archivePart.data), targetFileDescriptor);
          } catch (e) {
            exception = e;
            _sendArchiveManagerRequest(ArchiveManagerRequest()
                ..cancelGetArchive = CancelGetArchive());
          }
        }
      } else if (notice.hasEndOfArchive()) {
        if (exception == null) {
          if (notice.endOfArchive.ok) {
            break;
          } else {
            throw tr("Cannot download archive.");
          }
        } else {
          throw exception;
        }
      }
    }
  }

  Int64 _sendArchiveManagerRequest(ArchiveManagerRequest request) => _connection.send(ClientMessage()
      ..channelId = _archiveManagerChannelId
      ..archiveManagerRequest = request);
}

class CreateWorkDialog extends ModalDialog {
  static const String TAG = "ru-irenproject-create-work-dialog";

  DialogElement w_dialog;
  SpanElement w_title;

  final Dispatcher _dispatcher;
  final String _title;

  CreateWorkDialog(this._dispatcher, this._title) : super(TAG);

  Future<bool> openModal() async {
    w_title.text = _title;

    await DocumentEditor.loadScene(_dispatcher, disableUi: true);

    Future<void> whenClosed = open(w_dialog);
    var profileSelector = WorkProfileSelector(DocumentEditor.WIDGET_NAME, _dispatcher)
        ..element.id = "profileSelector";
    findElement("profileSelectorPlaceholder").replaceWith(profileSelector.element);

    profileSelector.enableRendering();
    await whenClosed;

    return w_dialog.returnValue.isNotEmpty;
  }
}

class WorkProfileSelector extends DocumentEditor<XWorkProfileSelector> {
  static const String TAG = "ru-irenproject-work-profile-selector";

  static final Map<Type, WidgetFactory> _screens = {
      XProfileScreen: (name, dispatcher) => ProfileScreen(name, dispatcher)};

  @override final bool modified = false;

  WorkProfileSelector(String name, Dispatcher dispatcher) : super(TAG, name, dispatcher);

  @override void render() {
    super.render();
    renderScreen(shape.screen, _screens);
  }

  @override void markAsUnmodified() {}

  @override bool get exportableToHtml => false;
}

void initialize() {
  registerShapeParser((b) => XWorkProfileSelector.fromBuffer(b));
}
