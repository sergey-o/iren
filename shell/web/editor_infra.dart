/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:typed_data';

import 'package:fixnum/fixnum.dart' show Int64;
import 'package:iren_client/iren_client.dart' show Connection;
import 'package:iren_proto/editor/infra.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:meta/meta.dart';
import 'package:protobuf/protobuf.dart' show GeneratedMessage;
import 'package:quiver/collection.dart' show LruMap;
import 'package:web_helpers/web_helpers.dart';

import 'misc_utils.dart';

typedef MessageParser = GeneratedMessage Function(List<int> buffer);

class Scene {
  final Map<String, GeneratedMessage> _shapes = {};

  // used by the library
  final LruMap<String, Object> _widgetStatesByName = LruMap(maximumSize: 100);

  X shapes<X extends GeneratedMessage>(String name) {
    GeneratedMessage m = _shapes[name];
    if (m == null) {
      throw "Shape '$name' not found.";
    }
    return m as X;
  }

  // used by the library
  void _applyDelta(SceneDelta delta) {
    for (var shape in delta.changed) {
      MessageParser p = _shapeParsers[shape.type];
      if (p == null) {
        throw "No message parser for '${shape.type}'.";
      }
      _shapes[shape.name] = p(shape.data);
    }

    delta.deleted.forEach(_shapes.remove);
  }
}

abstract class EditorConnection {
  final Map<String, void Function(Reply)> _replyHandlersByChannel = {};

  void send(Request request);

  void addReplyHandler(String channel, void Function(Reply) handler) {
    _replyHandlersByChannel[channel] = handler;
  }

  void removeReplyHandler(String channel) {
    _replyHandlersByChannel.remove(channel);
  }

  @protected void handleReply(Reply reply) {
    var handler = _replyHandlersByChannel[reply.channel];
    if (handler != null) {
      handler(reply);
    }
  }
}

class PlainEditorConnection extends EditorConnection {
  final WebSocket _socket;

  PlainEditorConnection(this._socket) {
    _socket.onMessage.listen(_onSocketMessage);
  }

  void _onSocketMessage(MessageEvent e) {
    ByteBuffer b = e.data as ByteBuffer;
    handleReply(Reply.fromBuffer(b.asUint8List()));
  }

  @override void send(Request request) {
    _socket.sendTypedData(request.writeToBuffer());
  }

  bool get connected => _socket.readyState == WebSocket.OPEN;
}

class EncapsulatedEditorConnection extends EditorConnection {
  final Connection _hostConnection;
  final ClientMessage Function(Request) _wrapper;
  final Reply Function(ServerMessage) _unwrapper;

  EncapsulatedEditorConnection(this._hostConnection, this._wrapper, this._unwrapper);

  @override void send(Request request) {
    _hostConnection.perform(_wrapper(request))
        .then(_unwrapper)
        .then(handleReply);
  }
}

class Dispatcher {
  final EditorConnection connection;
  final Scene scene;
  final String channel;
  Int64 _lastRequestId = Int64.ZERO;
  Int64 _lastReplyId = Int64.ZERO;
  bool _uiLocked = false;

  final StreamController<void> _onChangeScene = StreamController.broadcast(sync: true);
  Stream<void> get onChangeScene => _onChangeScene.stream;

  final Map<Int64, Completer<ActionResult>> _actionsInProgress = {};

  Dispatcher(this.connection, this.scene, this.channel) {
    connection.addReplyHandler(channel, _handleReply);
  }

  void _handleReply(Reply reply) {
    _lastReplyId = reply.id;

    Completer<ActionResult> completer = _actionsInProgress.remove(reply.id);
    check(completer != null);
    if (reply.ok) {
      completer.complete(ActionResult(reply.hasOutput() ? reply.output : null));
    } else {
      completer.completeError(ActionException());
    }

    if (!waitingForReply && _uiLocked) {
      unlockUi();
      _uiLocked = false;
    }

    scene._applyDelta(reply.sceneDelta);
    _onChangeScene.add(null);
  }

  Future<ActionResult> performAction(String targetName, GeneratedMessage input, {bool disableUi: true}) {
    if (disableUi && !_uiLocked) {
      lockUi();
      _uiLocked = true;
    }

    ++_lastRequestId;

    var completer = Completer<ActionResult>.sync();
    _actionsInProgress[_lastRequestId] = completer;

    var request = Request()
        ..channel = channel
        ..targetName = targetName
        ..actionName = input.info_.messageName.substring(input.info_.messageName.lastIndexOf("_") + 1)
        ..input = input.writeToBuffer();

    connection.send(request);
    return completer.future;
  }

  bool get waitingForReply => _lastRequestId != _lastReplyId;

  void dimUiIfWaitingForReply() {
    if (waitingForReply) {
      if (!_uiLocked) {
        lockUi();
        _uiLocked = true;
      }
      dimLockedUi();
    }
  }

  void stop() {
    connection.removeReplyHandler(channel);
    if (_uiLocked) {
      unlockUi();
      _uiLocked = false;
    }
  }
}

class ActionResult {
  final /* nullable */List<int> output;
  ActionResult(this.output);
}

class ActionException implements Exception {}

class Widget<X extends GeneratedMessage> extends WebComponent {
  final String name;
  final Dispatcher dispatcher;
  final Scene scene;

  X _shape;
  @protected X get shape => _shape;

  bool _shapeChanged;
  @protected bool get shapeChanged => _shapeChanged;

  Widget(String tag, this.name, this.dispatcher)
      : scene = dispatcher.scene,
        super(tag) {
    _shape = scene.shapes(name) as X;
  }

  void render() {
    X newShape = scene.shapes(name) as X;
    _shapeChanged = (_shapeChanged == null) || !identical(_shape, newShape);
    _shape = newShape;
  }

  @protected Future<ActionResult> performAction(GeneratedMessage input, {bool disableUi: true}) =>
      dispatcher.performAction(name, input, disableUi: disableUi);

  @protected void saveState(Object state) {
    scene._widgetStatesByName[name] = state;
  }

  @protected /* nullable */Object recallState() => scene._widgetStatesByName[name];
}

typedef WidgetFactory = Widget Function(String name, Dispatcher dispatcher);

abstract class DocumentEditor<X extends GeneratedMessage> extends Widget<X> {
  static const String WIDGET_NAME = "";

  static Future<void> loadScene(Dispatcher dispatcher, {@required bool disableUi}) async {
    if (disableUi) {
      lockUi();
    }
    try {
      await dispatcher.performAction(WIDGET_NAME, XDocumentEditor_Nop());
      await dispatcher.onChangeScene.first;
    } finally {
      if (disableUi) {
        unlockUi();
      }
    }
  }

  Widget _screen;

  DocumentEditor(String tag, String name, Dispatcher dispatcher) : super(tag, name, dispatcher);

  void enableRendering({void Function() afterRender}) {
    render();
    dispatcher.onChangeScene.listen((_) {
      if (!dispatcher.waitingForReply) {
        render();
        if (afterRender != null) {
          afterRender();
        }
      }
    });
  }

  bool get modified;

  void markAsUnmodified();

  bool get exportableToHtml;

  void exportToHtml() {}

  @protected void renderScreen(String screenName, Map<Type, WidgetFactory> screenFactoriesByShapeClass) {
    bool screenChanged = (screenName != _screen?.name);
    bool firstRender = (_screen == null);

    if (screenChanged) {
      Type shapeClass = scene.shapes(screenName).runtimeType;
      WidgetFactory factory = screenFactoriesByShapeClass[shapeClass];
      if (factory == null) {
        throw "No screen for '$shapeClass'.";
      }

      Widget newScreen = factory(screenName, dispatcher);
      if (_screen == null) {
        element.shadowRoot.append(newScreen.element);
      } else {
        _screen.element.replaceWith(newScreen.element);
      }
      _screen = newScreen;
    }

    _screen.render();

    if (screenChanged && !firstRender) {
      _screen.focusComponent();
    }
  }
}

final Map<String, MessageParser> _shapeParsers = {};
final List<Future<void> Function()> _windowCloseListeners = [];

Widget getWidget(Element e) => getWebComponent(e) as Widget;

void registerShapeParsers(Iterable<MessageParser> parsers) {
  parsers.forEach(registerShapeParser);
}

void registerShapeParser(MessageParser parser) {
  String messageName = parser([]).info_.messageName;
  if (_shapeParsers.containsKey(messageName)) {
    throw "Duplicate parser for '$messageName'.";
  }
  _shapeParsers[messageName] = parser;
}

void addWindowCloseListener(Future<void> Function() listener) {
  _windowCloseListeners.add(listener);
}

void removeWindowCloseListener(Future<void> Function() listener) {
  _windowCloseListeners.remove(listener);
}

Future<void> runWindowCloseListeners() async {
  for (var listener in _windowCloseListeners.toList()) {
    await listener();
  }
}

void initialize() {
  registerShapeParser((b) => XBlob.fromBuffer(b));
}
