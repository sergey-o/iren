/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';

import 'package:build/build.dart';
import 'package:glob/glob.dart';

class TemplateCombinerStage1 implements Builder {
  @override final Map<String, List<String>> buildExtensions = const {".dart": [".index.html"]};

  @override Future<void> build(BuildStep buildStep) async {
    List<AssetId> libraries = await buildStep.resolver.libraries
        .where((library) => !library.isInSdk)
        .asyncMap((library) => buildStep.resolver.assetIdForElement(library))
        .toList();
    libraries.sort();

    var templates = StringBuffer();
    for (var library in libraries) {
      AssetId templateFile = library.changeExtension(".html");
      if (await buildStep.canRead(templateFile)) {
        String s = await buildStep.readAsString(templateFile);
        int p = s.indexOf('<template id="template-ru-irenproject-');
        if (p != -1) {
          templates
              ..writeln()
              ..write(s.substring(p));
        }
      }
    }

    String source = await buildStep.readAsString(AssetId.resolve("index.template.html", from: buildStep.inputId));
    String expanded = source.replaceFirst(r"$(TEMPLATES)", templates.toString());
    buildStep.writeAsString(buildStep.inputId.changeExtension(".index.html"), expanded);
  }
}

class TemplateCombinerStage2 implements Builder {
  @override final Map<String, List<String>> buildExtensions = const {r"$web$": ["index.html"]};

  @override Future<void> build(BuildStep buildStep) async {
    AssetId source = await buildStep.findAssets(Glob("**.index.html")).single;
    AssetId target = AssetId.resolve("index.html", from: buildStep.inputId);
    buildStep.writeAsBytes(target, buildStep.readAsBytes(source));
  }
}

Builder createTemplateCombinerStage1(BuilderOptions options) => TemplateCombinerStage1();

Builder createTemplateCombinerStage2(BuilderOptions options) => TemplateCombinerStage2();
