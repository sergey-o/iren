/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:collection';

import 'package:build/build.dart';
import 'package:quiver/core.dart';
import 'package:source_gen/source_gen.dart';

import 'embedded_resources.dart';

class ResourceEmbedder implements Builder {
  @override final Map<String, List<String>> buildExtensions = const {".dart": [".embeddedResources.dart"]};

  @override Future<void> build(BuildStep buildStep) async {
    var resourcesByLocation = SplayTreeMap<String, String>();

    var locationPattern = RegExp(r'^package:[^\u0000-\u001f"]+$');
    var finder = const TypeChecker.fromRuntime(EmbeddedResource);

    await for (String location in buildStep.resolver.libraries
        .where((library) => !library.isInSdk)
        .expand((library) => library.units)
        .expand((unit) => unit.topLevelVariables)
        .expand((variable) => Optional.fromNullable(finder.firstAnnotationOfExact(variable)))
        .map((annotation) => annotation.getField("location").toStringValue())) {
      if (!resourcesByLocation.containsKey(location)) {
        if (!location.contains(locationPattern)) {
          throw "Incorrect resource location: '$location'.";
        }
        resourcesByLocation[location] = await buildStep.readAsString(AssetId.resolve(location));
      }
    }

    var out = StringBuffer()
        ..write(
            "import 'package:iren_shell/src/embedded_resources_internal.dart';\n"
            "\n"
            "void initializeEmbeddedResources() {\n"
            "  embeddedResources = const <String, String>{\n");

    resourcesByLocation.forEach((location, content) {
      if (content.contains('"""')) {
        throw "Cannot embed resource '$location' due to '\"\"\"' sequence in its content.";
      }
      out.writeln('      r"$location": r"""\n$content""",');
    });

    out.write(
        "  };\n"
        "}\n");

    buildStep.writeAsString(buildStep.inputId.changeExtension(".embeddedResources.dart"), out.toString());
  }
}

Builder createResourceEmbedder(BuilderOptions options) => ResourceEmbedder();
