/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.BadInputException;
import ru.irenproject.Check;
import ru.irenproject.Test;
import ru.irenproject.common.protocol.Proto.BootstrapNotice;
import ru.irenproject.common.protocol.Proto.ClientMessage;
import ru.irenproject.common.protocol.Proto.ProtocolConstants;
import ru.irenproject.common.protocol.Proto.ServerHello;
import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.common.student.Proto.StudentRequest;
import ru.irenproject.common.studentWorkSelector.Proto.ReopenWork;
import ru.irenproject.common.studentWorkSelector.Proto.StudentWorkSelectorReply;
import ru.irenproject.common.studentWorkSelector.Proto.StudentWorkSelectorRequest;
import ru.irenproject.infra.Event;
import ru.irenproject.infra.Realm;
import ru.irenproject.infra.Resident;
import ru.irenproject.itx.ExternallyStoredObjectReader;
import ru.irenproject.itx.ItxReader;
import ru.irenproject.itx.ItxReaderFactory;
import ru.irenproject.live.Proto._LiveQuestion;
import ru.irenproject.live.QuestionIssuer;
import ru.irenproject.profile.Profile;
import ru.irenproject.work.BaseWorkUtils;
import ru.irenproject.work.ClosableWorkSource;
import ru.irenproject.work.FrozenWorkSource;
import ru.irenproject.work.FullSession;
import ru.irenproject.work.Proto.Session;
import ru.irenproject.work.Proto.SessionConnection;
import ru.irenproject.work.Proto.SessionQuestion;
import ru.irenproject.work.Proto.SessionStatus;
import ru.irenproject.work.RawScreen;
import ru.irenproject.work.SessionHandler;
import ru.irenproject.work.SessionHandlerFactory;
import ru.irenproject.work.UpdateResponse;

import com.google.protobuf.Timestamp;
import com.google.protobuf.contrib.j2cl.runtime.JspbParseException;
import com.google.protobuf.contrib.j2cl.runtime.JspbUtils;
import dagger.Component;
import elemental2.dom.DomGlobal;
import elemental2.dom.MessageChannel;
import elemental2.dom.MessageEvent;
import elemental2.dom.MessagePort;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;
import org.w3c.dom.Document;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@JsType @Singleton public final class PocketServer {
  @Component(modules = PocketServerModule.class)
  @Singleton public interface Program {
    PocketServer pocketServer();
  }

  private static final long STUDENT_CHANNEL_ID = 1;
  private static final long STUDENT_WORK_SELECTOR_CHANNEL_ID = 2;

  @SuppressWarnings("unused")
  public static void run() {
    try {
      DaggerPocketServer_Program.create().pocketServer();
    } catch (RuntimeException e) {
      DomGlobal.alert(e.getMessage());
    }
  }

  private final SessionHandlerFactory fSessionHandlerFactory;

  private final ArrayList<RawScreen> fRawScreens;
  private final Session fInitialSession;
  private final SimpleWorkSource fWorkSource;
  private final MessagePortConnection fConnection;

  private long fLastRequestId = ProtocolConstants.LAST_RESERVED_CLIENT_CHANNEL_ID_VALUE;
  private SessionStatus fSessionStatus;
  private SessionHandler fHandler;

  @Inject PocketServer(
      ItxReaderFactory itxReaderFactory,
      QuestionIssuer questionIssuer,
      SessionHandlerFactory sessionHandlerFactory) {
    fSessionHandlerFactory = sessionHandlerFactory;

    MessageChannel messageChannel = new MessageChannel();
    MessagePort serverPort = messageChannel.port1;
    MessagePort clientPort = messageChannel.port2;

    Js.global().set("irenClientPort", clientPort);

    Document document = new DOMParser().parseFromString(
        (String) Pako.inflate(
            DomGlobal.atob(Check.inputNotNull(DomGlobal.document.getElementById("irenTest")).textContent),
            JsPropertyMap.of("to", "string")),
        "text/xml");

    Realm realm = new Realm(PocketServer::logUnexpectedEvent, null, new DomBlobStore());

    ItxReader itxReader = itxReaderFactory.create(
        new ExternallyStoredObjectReader(),
        document.getDocumentElement(),
        realm);

    Test test = itxReader.readTest(document.getDocumentElement());
    Profile profile = test.profileList().profiles().get(0);

    List<_LiveQuestion> liveQuestions = questionIssuer.issueFromTest(
        test,
        profile,
        Locale.ROOT);

    Check.that(!liveQuestions.isEmpty(), () -> new BadInputException("No questions are available."));

    FullSession sessionPrecursor = BaseWorkUtils.buildSessionPrecursor(
        "",
        null,
        null,
        SessionConnection.newBuilder()
            .setOpenedAt(Timestamp.newBuilder()
                .setSeconds(0)
                .setNanos(0))
            .setAddress("")
            .build(),
        profile.options().hasDurationMinutes() ?
            System.currentTimeMillis() + profile.options().getDurationMinutes()*60_000L :
            null,
        liveQuestions);

    Session.Builder sessionBuilder = sessionPrecursor.data.toBuilder();
    for (int size = liveQuestions.size(), i = 0; i < size; ++i) {
      sessionBuilder.addQuestion(SessionQuestion.newBuilder()
          .setScreenId(i)
          .setWeight(liveQuestions.get(i).getWeight()));
    }
    fInitialSession = sessionBuilder.build();
    fSessionStatus = sessionPrecursor.status;

    fRawScreens = liveQuestions.stream()
        .map(liveQuestion -> new RawScreen(liveQuestion, null))
        .collect(Collectors.toCollection(ArrayList::new));

    fWorkSource = new SimpleWorkSource(new FrozenWorkSource(test, profile));

    serverPort.onmessage = this::onPortMessage;
    fConnection = new MessagePortConnection(serverPort);

    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(ProtocolConstants.BOOTSTRAP_CHANNEL_ID_VALUE)
        .setBootstrapNotice(BootstrapNotice.newBuilder()
            .setServerHello(ServerHello.newBuilder()
                .setStudentWorkSelectorChannelId(STUDENT_WORK_SELECTOR_CHANNEL_ID)))
        .build());
  }

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  private static final class DOMParser {
    native Document parseFromString(String string, String type);
  }

  @JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "pako")
  private static final class Pako {
    static native Object inflate(String input, JsPropertyMap<Object> options);
  }

  private static final class SimpleWorkSource extends ClosableWorkSource {
    SimpleWorkSource(FrozenWorkSource frozenWorkSource) {
      super(frozenWorkSource);
    }

    @Override public void close() {}
  }

  private static void logUnexpectedEvent(Resident source, Resident target, Event event) {
    DomGlobal.console.warn("Unexpected event %o (%o -> %o).", event, source, target);
  }

  private void onPortMessage(MessageEvent<Object> e) {
    ++fLastRequestId;

    Check.input(e.data instanceof String);
    String jspb = (String) e.data;

    ClientMessage m;
    try {
      m = JspbUtils.fromJspbString(jspb, ClientMessage.getDefaultInstance());
    } catch (JspbParseException ex) {
      throw new BadInputException(ex);
    }

    if (m.getChannelId() == STUDENT_WORK_SELECTOR_CHANNEL_ID) {
      Check.input(m.hasStudentWorkSelectorRequest());
      handleStudentWorkSelectorRequest(m.getStudentWorkSelectorRequest());
    } else if (m.getChannelId() == STUDENT_CHANNEL_ID) {
      Check.input(m.hasStudentRequest());
      handleStudentRequest(m.getStudentRequest());
    } else {
      throw new BadInputException();
    }
  }

  private void handleStudentWorkSelectorRequest(StudentWorkSelectorRequest m) {
    Check.input(m.getRequestCase() == StudentWorkSelectorRequest.RequestCase.REOPEN_WORK);
    Check.input(fHandler == null);

    fHandler = fSessionHandlerFactory.create(
        fInitialSession,
        fConnection,
        m.getReopenWork().getStudentNoticeChannelId(),
        false,
        () -> fWorkSource,
        screenId -> fRawScreens.get((int) screenId),
        ignored -> fRawScreens.stream()
            .map(rawScreen -> rawScreen.liveQuestionData.getSource())
            .collect(Collectors.toList()));

    fConnection.send(ServerMessage.newBuilder()
        .setChannelId(fLastRequestId)
        .setStudentWorkSelectorReply(StudentWorkSelectorReply.newBuilder()
            .setReopenWorkReply(ReopenWork.Reply.newBuilder()
                .setOk(ReopenWork.Reply.Ok.newBuilder()
                    .setStudentChannelId(STUDENT_CHANNEL_ID))))
        .build());

    fHandler.handleSessionStatusDelta(fSessionStatus);
  }

  private void handleStudentRequest(StudentRequest m) {
    Check.inputNotNull(fHandler);

    switch (m.getRequestCase()) {
      case SET_RESPONSE: {
        fHandler.handleSetResponse(m.getSetResponse());
        fHandler.handleContinueSetResponse();
        break;
      }
      case SUBMIT: {
        SessionHandler.SubmitResult r = fHandler.handleSubmit();
        updateResponse(r.updateResponse);
        fHandler.handleContinueSubmit(r.continueSubmit);
        break;
      }
      case CHOOSE_QUESTION: {
        fHandler.handleChooseQuestion(m.getChooseQuestion());
        fHandler.handleContinueChooseQuestion();
        break;
      }
      case FINISH_WORK: {
        SessionHandler.FinishWorkResult r = fHandler.handleFinishWork();
        if (r.updateResponse != null) {
          updateResponse(r.updateResponse);
        }
        fSessionStatus = fSessionStatus.toBuilder()
            .setFinished(true)
            .buildPartial();
        fHandler.handleSessionStatusDelta(fSessionStatus);
        fHandler.handleContinueFinishWork();
        break;
      }
      case QUERY_QUESTION_DETAILS: {
        fHandler.handleQueryQuestionDetails(m.getQueryQuestionDetails());
        break;
      }
      case CLOSE:
      case REQUEST_NOT_SET:
      default:
        throw new BadInputException();
    }
  }

  private void updateResponse(UpdateResponse u) {
    RawScreen old = fRawScreens.get((int) u.screenId);
    fRawScreens.set((int) u.screenId, new RawScreen(old.liveQuestionData, u.response));
  }
}
