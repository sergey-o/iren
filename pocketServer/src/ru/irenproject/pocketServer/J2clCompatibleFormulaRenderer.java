/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.formula.FormulaImage;
import ru.irenproject.formula.FormulaRendererLike;

import com.google.common.collect.ImmutableMap;
import elemental2.dom.Element;
import elemental2.dom.Node;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;
import jsinterop.base.Js;
import jsinterop.base.JsPropertyMap;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton public final class J2clCompatibleFormulaRenderer implements FormulaRendererLike {
  @Inject J2clCompatibleFormulaRenderer() {}

  @Override public FormulaImage render(String source) {
    String svg;
    ImmutableMap<String, String> style;

    try {
      Object[] resultArray = irenRenderFormula(source);

      Element svgElement = (Element) resultArray[0];
      svgElement.removeAttribute("xmlns:xlink");
      svgElement.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:xlink", "http://www.w3.org/1999/xlink");

      svg = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + new XMLSerializer().serializeToString(svgElement);

      JsPropertyMap<Object> jsStyle = Js.asPropertyMap(resultArray[1]);
      ImmutableMap.Builder<String, String> styleBuilder = ImmutableMap.builder();
      jsStyle.forEach(key -> styleBuilder.put(key, (String) jsStyle.get(key)));

      style = styleBuilder.build();
    } catch (RuntimeException ignored) {
      svg = FormulaRendererLike.buildPlaceholderSvg(false);
      style = PLACEHOLDER_STYLE;
    }

    return new FormulaImage(
        WebHelpers.createByteStringFromBase64(WebHelpers.btoaUTF8(svg)),
        style);
  }

  @JsMethod(namespace = JsPackage.GLOBAL)
  private static native Object[] irenRenderFormula(String source);

  @JsType(isNative = true, namespace = JsPackage.GLOBAL)
  private static final class XMLSerializer {
    native String serializeToString(Node rootNode);
  }
}
