/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import com.google.protobuf.ByteString;
import jsinterop.annotations.JsMethod;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

public final class WebHelpers {
  public static ByteString createByteStringFromBase64(String base64) {
    return NativeByteString.fromBase64String(base64);
  }

  @JsType(isNative = true, namespace = "proto.im", name = "ByteString")
  private static final class NativeByteString {
    static native ByteString fromBase64String(String value);
  }

  @JsMethod(namespace = JsPackage.GLOBAL)
  public static native String btoaUTF8(String s);

  private WebHelpers() {}
}
