/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.Proto._ContentTransformer;
import ru.irenproject.live.Proto._TextReplacement;

import com.google.auto.factory.AutoFactory;
import com.google.common.base.Ascii;

import javax.annotation.Nullable;

@AutoFactory(implementing = ContentTransformer.Factory.class)
public final class J2clCompatibleContentTransformer extends ContentTransformer {
  private final _ContentTransformer fMessage;

  J2clCompatibleContentTransformer(_ContentTransformer message) {
    fMessage = message;
  }

  @Override protected int replacementCount() {
    return fMessage.getTextReplacementCount();
  }

  @Override protected @Nullable String replaceAllIfFound(String source, int replacementIndex) {
    _TextReplacement r = fMessage.getTextReplacement(replacementIndex);

    String normalizedSource = Ascii.toUpperCase(source);
    String normalizedPattern = Ascii.toUpperCase(r.getPattern());

    String res;

    int p = normalizedSource.indexOf(normalizedPattern);
    if (p == -1) {
      res = null;
    } else {
      StringBuilder result = new StringBuilder();
      int i = 0;
      while (p != -1) {
        result
            .append(source, i, p)
            .append(r.getReplacement());
        i = p + normalizedPattern.length();
        p = normalizedSource.indexOf(normalizedPattern, i);
      }
      res = result
          .append(source, i, source.length())
          .toString();
    }

    return res;
  }
}
