/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.Check;
import ru.irenproject.Modifier;
import ru.irenproject.Question;
import ru.irenproject.live.IssueOrderList;
import ru.irenproject.live.Live;
import ru.irenproject.live.ModifierApplier;
import ru.irenproject.live.QuestionIssueException;
import ru.irenproject.live.ReplaceTextIssueOrder;
import ru.irenproject.scriptModifier.ScriptModifier;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Singleton public final class PrecomputedScriptModifierApplier implements ModifierApplier {
  private static final Splitter VARIABLE_SPLITTER = Splitter.on('=').limit(2);
  private static final CharMatcher HEX_DIGIT_MATCHER = CharMatcher.inRange('0', '9').or(CharMatcher.inRange('a', 'f'));

  private final ThreadLocal<Random> fRandom;

  @Inject PrecomputedScriptModifierApplier(@Live ThreadLocal<Random> random) {
    fRandom = random;
  }

  @Override public void apply(Modifier modifier, Question question, IssueOrderList orderList) {
    ScriptModifier m = (ScriptModifier) modifier;

    List<Integer> startingLines = findResultBlockStartingLines(m.script());
    Check.that(!startingLines.isEmpty(), this::badDataException);

    int chosenResultBlockIndex = fRandom.get().nextInt(startingLines.size());
    LinkedHashMap<String, String> variables = parseResultBlock(m.script(), startingLines.get(chosenResultBlockIndex));

    ReplaceTextIssueOrder order = new ReplaceTextIssueOrder();
    for (Map.Entry<String, String> e : variables.entrySet()) {
      order.add("$(" + e.getKey() + ")", e.getValue());
    }

    orderList.add(order);
  }

  private List<Integer> findResultBlockStartingLines(List<String> results) {
    Check.that(!results.isEmpty(), this::badDataException);
    Check.that(results.get(0).equals("//@@EXPORTED_RESULTS@@"), this::badDataException);

    ArrayList<Integer> res = new ArrayList<>();
    boolean endMarkerFound = false;
    boolean insideResultBlock = false;

    for (int size = results.size(), i = 1; i < size; ++i) {
      String s = results.get(i);
      if (s.equals("//@@END_EXPORTED_RESULTS@@")) {
        endMarkerFound = true;
        break;
      } else {
        if (!insideResultBlock) {
          res.add(i);
          insideResultBlock = true;
        }
        if (s.equals("//")) {
          insideResultBlock = false;
        }
      }
    }

    Check.that(endMarkerFound, this::badDataException);
    Check.that(!insideResultBlock, this::badDataException);

    return res;
  }

  private QuestionIssueException badDataException() {
    return new QuestionIssueException("Incorrect script variable data.");
  }

  private LinkedHashMap<String, String> parseResultBlock(List<String> results, int startingLine) {
    LinkedHashMap<String, String> res = new LinkedHashMap<>();

    int i = startingLine;
    String s;
    while (!(s = results.get(i)).equals("//")) {
      Check.that(s.startsWith("//"), this::badDataException);

      List<String> nameValue = VARIABLE_SPLITTER.splitToList(s.substring(2));
      Check.that(nameValue.size() == 2, this::badDataException);
      Check.that(!nameValue.get(0).isEmpty(), this::badDataException);

      res.put(nameValue.get(0), unescapeVariableValue(nameValue.get(1)));

      ++i;
    }

    return res;
  }

  private String unescapeVariableValue(String s) {
    String res;

    int p = s.indexOf('\\');
    if (p == -1) {
      res = s;
    } else {
      StringBuilder result = new StringBuilder();
      int i = 0;
      while (p != -1) {
        Check.that(p <= s.length() - 6, this::badEscapeException);
        Check.that(s.charAt(p + 1) == 'u', this::badEscapeException);

        String hexCode = s.substring(p + 2, p + 6);
        Check.that(HEX_DIGIT_MATCHER.matchesAllOf(hexCode), this::badEscapeException);

        result
            .append(s, i, p)
            .append((char) Integer.parseInt(hexCode, 16));

        i = p + 6;
        p = s.indexOf('\\', i);
      }
      res = result
          .append(s, i, s.length())
          .toString();
    }

    return res;
  }

  private QuestionIssueException badEscapeException() {
    return new QuestionIssueException("Incorrect character escape.");
  }
}
