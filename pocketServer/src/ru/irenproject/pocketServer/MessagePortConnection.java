/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.common.protocol.Proto.ServerMessage;
import ru.irenproject.work.ConnectionLike;

import com.google.protobuf.contrib.j2cl.runtime.JspbUtils;
import elemental2.dom.MessagePort;

public final class MessagePortConnection implements ConnectionLike {
  private final MessagePort fMessagePort;

  public MessagePortConnection(MessagePort messagePort) {
    fMessagePort = messagePort;
  }

  @Override public void enqueue(ServerMessage message) {
    send(message);
  }

  @Override public void send(ServerMessage message) {
    fMessagePort.postMessage(JspbUtils.toJspbString(message));
  }

  @Override public void flush() {}
}
