/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.Check;
import ru.irenproject.infra.BlobId;
import ru.irenproject.infra.BlobStore;

import com.google.common.io.ByteSource;
import com.google.protobuf.ByteString;
import elemental2.dom.DomGlobal;
import elemental2.dom.Element;

public final class DomBlobStore implements BlobStore {
  @Override public BlobId put(ByteSource blob) {
    throw new RuntimeException();
  }

  @Override public ByteSource get(BlobId blobId) {
    Element e = Check.inputNotNull(DomGlobal.document.getElementById("irenBlob-" + blobId.toHexString()));
    ByteString blob = WebHelpers.createByteStringFromBase64(e.textContent);
    return new ByteSource(blob);
  }
}
