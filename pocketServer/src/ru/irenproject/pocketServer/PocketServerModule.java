/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.addNegativeChoiceModifier.AddNegativeChoiceModifierBaseModule;
import ru.irenproject.classifyQuestion.ClassifyQuestionBaseModule;
import ru.irenproject.formula.FormulaRendererLike;
import ru.irenproject.inputQuestion.InputQuestionBaseModule;
import ru.irenproject.live.ContentTransformer;
import ru.irenproject.live.Live;
import ru.irenproject.live.classifyQuestion.LiveClassifyQuestionBaseModule;
import ru.irenproject.live.inputQuestion.LiveInputQuestionBaseModule;
import ru.irenproject.live.matchQuestion.LiveMatchQuestionBaseModule;
import ru.irenproject.live.orderQuestion.LiveOrderQuestionBaseModule;
import ru.irenproject.live.selectQuestion.LiveSelectQuestionBaseModule;
import ru.irenproject.matchQuestion.MatchQuestionBaseModule;
import ru.irenproject.orderQuestion.OrderQuestionBaseModule;
import ru.irenproject.scriptModifier.ScriptModifierBaseModule;
import ru.irenproject.selectQuestion.SelectQuestionBaseModule;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifierBaseModule;
import ru.irenproject.setNegativeChoiceContentModifier.SetNegativeChoiceContentModifierBaseModule;
import ru.irenproject.setWeightModifier.SetWeightModifierBaseModule;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifierBaseModule;
import ru.irenproject.suppressSingleChoiceHintModifier.SuppressSingleChoiceHintModifierBaseModule;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;
import java.util.Random;

@Module(includes = {
    SelectQuestionBaseModule.class,
    LiveSelectQuestionBaseModule.class,

    InputQuestionBaseModule.class,
    LiveInputQuestionBaseModule.class,

    MatchQuestionBaseModule.class,
    LiveMatchQuestionBaseModule.class,

    OrderQuestionBaseModule.class,
    LiveOrderQuestionBaseModule.class,

    ClassifyQuestionBaseModule.class,
    LiveClassifyQuestionBaseModule.class,

    AddNegativeChoiceModifierBaseModule.class,
    ScriptModifierBaseModule.class,
    SetEvaluationModelModifierBaseModule.class,
    SetNegativeChoiceContentModifierBaseModule.class,
    SetWeightModifierBaseModule.class,
    ShuffleChoicesModifierBaseModule.class,
    SuppressSingleChoiceHintModifierBaseModule.class,

    ModifierApplierModule.class,
})
public final class PocketServerModule {
  @Provides @Singleton @Live static ThreadLocal<Random> provideRandom() {
    return ThreadLocal.withInitial(Random::new);
  }

  @Provides static ContentTransformer.Factory provideContentTransformerFactory(
      J2clCompatibleContentTransformerFactory factory) {
    return factory;
  }

  @Provides static FormulaRendererLike provideFormulaRenderer(
      J2clCompatibleFormulaRenderer renderer) {
    return renderer;
  }
}
