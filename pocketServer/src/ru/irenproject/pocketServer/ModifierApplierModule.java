/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject.pocketServer;

import ru.irenproject.ModifierClassKey;
import ru.irenproject.addNegativeChoiceModifier.AddNegativeChoiceModifier;
import ru.irenproject.live.AddNegativeChoiceModifierApplier;
import ru.irenproject.live.ModifierApplier;
import ru.irenproject.live.SetEvaluationModelModifierApplier;
import ru.irenproject.live.SetNegativeChoiceContentModifierApplier;
import ru.irenproject.live.SetWeightModifierApplier;
import ru.irenproject.live.ShuffleChoicesModifierApplier;
import ru.irenproject.live.SuppressSingleChoiceHintModifierApplier;
import ru.irenproject.scriptModifier.ScriptModifier;
import ru.irenproject.setEvaluationModelModifier.SetEvaluationModelModifier;
import ru.irenproject.setNegativeChoiceContentModifier.SetNegativeChoiceContentModifier;
import ru.irenproject.setWeightModifier.SetWeightModifier;
import ru.irenproject.shuffleChoicesModifier.ShuffleChoicesModifier;
import ru.irenproject.suppressSingleChoiceHintModifier.SuppressSingleChoiceHintModifier;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module public final class ModifierApplierModule {
  @ModifierClassKey(AddNegativeChoiceModifier.class)
  @Provides @IntoMap static ModifierApplier provideAddNegativeChoiceModifierApplier(
      AddNegativeChoiceModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(ScriptModifier.class)
  @Provides @IntoMap static ModifierApplier provideScriptModifierApplier(
      PrecomputedScriptModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(SetEvaluationModelModifier.class)
  @Provides @IntoMap static ModifierApplier provideSetEvaluationModelModifierApplier(
      SetEvaluationModelModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(SetNegativeChoiceContentModifier.class)
  @Provides @IntoMap static ModifierApplier provideSetNegativeChoiceContentModifierApplier(
      SetNegativeChoiceContentModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(SetWeightModifier.class)
  @Provides @IntoMap static ModifierApplier provideSetWeightModifierApplier(
      SetWeightModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(ShuffleChoicesModifier.class)
  @Provides @IntoMap static ModifierApplier provideShuffleChoicesModifierApplier(
      ShuffleChoicesModifierApplier applier) {
    return applier;
  }

  @ModifierClassKey(SuppressSingleChoiceHintModifier.class)
  @Provides @IntoMap static ModifierApplier provideSuppressSingleChoiceHintModifierApplier(
      SuppressSingleChoiceHintModifierApplier applier) {
    return applier;
  }
}
