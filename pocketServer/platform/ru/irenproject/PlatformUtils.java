/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package ru.irenproject;

import ru.irenproject.common.content.Proto.Block;
import ru.irenproject.common.content.Proto.Flow;
import ru.irenproject.common.content.Proto.FormulaBlock;
import ru.irenproject.common.dialog.Proto.DialogResponse;
import ru.irenproject.pocketServer.WebHelpers;

import com.google.common.io.ByteSource;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.Message;
import com.google.protobuf.contrib.j2cl.runtime.JspbParseException;
import com.google.protobuf.contrib.j2cl.runtime.JspbUtils;
import jsinterop.annotations.JsPackage;
import jsinterop.annotations.JsType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public final class PlatformUtils {
  public static Any toAny(Message m) {
    return Any.newBuilder()
        .setTypeUrl(getTypeUrl(m))
        .setValue(WebHelpers.createByteStringFromBase64(WebHelpers.btoaUTF8(JspbUtils.toJspbString(m))))
        .build();
  }

  private static String getTypeUrl(Message m) {
    return "/" + ((CustomGeneratedMessage) m).fullName();
  }

  @JsType(isNative = true, namespace = JsPackage.GLOBAL, name = "?")
  private interface CustomGeneratedMessage {
    String fullName();
  }

  public static <T extends Message> T unpackAny(Any any, T prototype) throws InvalidProtocolBufferException {
    if (!any.getTypeUrl().equals(getTypeUrl(prototype))) {
      throw new InvalidProtocolBufferException("Unexpected typeUrl.");
    }

    try {
      return JspbUtils.fromJspbString(any.getValue().toStringUtf8(), prototype);
    } catch (JspbParseException e) {
      throw new InvalidProtocolBufferException(e);
    }
  }

  public static Map<String, Any> getDialogResponseAreaMap(DialogResponse response) {
    LinkedHashMap<String, Any> out = new LinkedHashMap<>();
    for (DialogResponse.AreaEntry e : response.getAreaList()) {
      out.put(e.getKey(), e.getValue());
    }
    return Collections.unmodifiableMap(out);
  }

  public static DialogResponse.Builder putDialogResponseArea(DialogResponse.Builder builder, String areaId,
      Any areaResponse) {
    return builder.addArea(DialogResponse.AreaEntry.newBuilder()
        .setKey(areaId)
        .setValue(areaResponse));
  }

  public static FormulaBlock.Builder putAllFormulaBlockStyle(FormulaBlock.Builder builder, Map<String, String> style) {
    for (Map.Entry<String, String> e : style.entrySet()) {
      builder.addStyle(FormulaBlock.StyleEntry.newBuilder()
          .setKey(e.getKey())
          .setValue(e.getValue()));
    }
    return builder;
  }

  public static void removeFlowBlock(Flow.Builder flow, int blockIndex) {
    ArrayList<Block> blocks = new ArrayList<>(flow.getBlockList());
    blocks.remove(blockIndex);
    flow.clearBlock().addAllBlock(blocks);
  }

  public static int getByteStringSize(ByteString byteString) {
    return byteString.toByteArray().length;
  }

  public static boolean isInitializedOrTrueIfUnsupported(Message m) {
    return true;
  }

  public static ByteString readByteSourceToByteString(ByteSource byteSource) {
    return byteSource.data();
  }

  private PlatformUtils() {}
}
