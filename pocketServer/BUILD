# Copyright 2012-2020 Sergey Ostanin
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

load("@com_google_j2cl//build_defs:rules.bzl", "j2cl_application", "j2cl_library")

java_plugin(
    name = "auto_factory_processor",
    processor_class = "com.google.auto.factory.processor.AutoFactoryProcessor",
    generates_api = True,
    deps = [
        "@com_google_auto_factory_auto_factory",

        "@com_google_auto_auto_common",
        "@com_squareup_javapoet",
        "@javax_inject_javax_inject",

        "@com_google_guava_guava",
        "@com_google_guava_failureaccess",

        "@com_google_googlejavaformat_google_java_format",
        "@com_google_errorprone_javac_shaded",
    ])

java_plugin(
    name = "dagger_compiler_processor",
    processor_class = "dagger.internal.codegen.ComponentProcessor",
    generates_api = True,
    deps = [
        "@com_google_dagger_dagger_compiler",

        "@com_google_dagger_dagger",
        "@com_google_dagger_dagger_producers",
        "@com_google_dagger_dagger_spi",

        "@com_squareup_javapoet",
        "@javax_inject_javax_inject",
        "@org_jetbrains_kotlin_kotlin_stdlib",

        "@com_google_guava_guava",
        "@com_google_guava_failureaccess",
    ])

j2cl_library(
    name = "pocketServerLibrary",
    srcs =
        glob([
            "src/**",
            "platform/**",
            "externalLibraries/**/*.js",
        ]) + [
            "@iren",
            "@iren//:j2clProto",
            "@iren//:imjsProto",
            "@commons_io_sources",
            "@slf4j_api_sources",
        ],
    deps = [
        "@com_google_code_findbugs_jsr305-j2cl",
        "@com_google_elemental2_elemental2_dom-j2cl",
        "@com_google_guava_guava_gwt-j2cl",
        "@com_google_re2j_re2j-j2cl",
        "@org_jetbrains_annotations_annotations-j2cl",

        "@com_google_j2cl//:jsinterop-annotations-j2cl",
        "@com_google_jsinterop_base//:jsinterop-base-j2cl",

        "@com_google_j2cl_protobuf//java/com/google/protobuf/contrib/j2cl:runtime",
        "@com_google_j2cl_protobuf//java/com/google/protobuf/contrib/immutablejs/internal_do_not_use:runtime",

        "@javax_inject_javax_inject-j2cl",
        "@com_google_dagger_dagger-j2cl",
        "@com_google_auto_factory_auto_factory-j2cl",
    ],
    plugins = [
        ":dagger_compiler_processor",
        ":auto_factory_processor",
    ])

j2cl_application(
    name = "pocketServer",
    entry_points = ["ru.irenproject.pocketServer.entryPoint"],
    deps = [":pocketServerLibrary"],
    rewrite_polyfills = True,
    suppress_on_all_sources_in_transitive_closure = [
        "deprecated",
        "extraRequire",
        "jsdocMissingConst",
        "reportUnknownTypes",
    ],
    output_wrapper = "(function(){%output%}).call(this);")
