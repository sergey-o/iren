unit zlibStream;

{**********************************************************************
    Copyright (c) 2007 by Daniel Mantione
      member of the Free Pascal development team

    Implements a Tstream descendents that allow you to read and write
    compressed data according to the Deflate algorithm described in
    RFC1951.

    See the file COPYING.FPC, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

    Modified by Sergey Ostanin to use the original zlib.
    Contains code from zlib.pp.

 **********************************************************************}

{$mode objfpc}
{$packrecords c}

{***************************************************************************}
                                    interface
{***************************************************************************}

uses    classes, ctypes;

const
  ZLIB_VERSION = '1.2.11';

type
  uInt = cuint;

  puIntf = ^uIntf;
  uIntf = uInt;

  uLong = culong;

  puLongf = ^uLongf;
  uLongf = uLong;

  pBytef = ^Bytef;
  Bytef = cchar;

  alloc_func = function(opaque: pointer; items: uInt; size: uInt): pointer; cdecl;
  TAllocfunc = alloc_func;

  free_func = procedure(opaque: pointer; items: uInt; size: uInt); cdecl;
  TFreeFunc = free_func;

  internal_statep = ^internal_state;
  internal_state = record end;
  TInternalState = internal_state;
  PInternalState = internal_statep;

  z_off_t = coff_t;

  z_streamp = ^z_stream;
  z_stream = record
    next_in   : pBytef;            // next input byte
    avail_in  : uInt;              // number of bytes available at next_in
    total_in  : uLong;             // total nb of input bytes read so far

    next_out  : pBytef;            // next output byte should be put there
    avail_out : uInt;              // remaining free space at next_out
    total_out : uLong;             // total nb of bytes output so far

    msg       : pBytef;            // last error message, NULL if no error
    state     : internal_statep;   // not visible by applications

    zalloc    : alloc_func;        // used to allocate the internal state
    zfree     : free_func;         // used to free the internal state
    opaque    : pointer;           // private data object passed to zalloc and zfree

    data_type : cint;              // best guess about the data type: binary or text
    adler     : uLong;             // adler32 value of the uncompressed data
    reserved  : uLong;             // reserved for future use
  end;
  TZStreamRec = z_stream;
  TZStream = z_stream;
  PZStream = z_streamp;

const
  Z_NO_FLUSH = 0;
  Z_PARTIAL_FLUSH = 1; // will be removed, use Z_SYNC_FLUSH instead
  Z_SYNC_FLUSH = 2;
  Z_FULL_FLUSH = 3;
  Z_FINISH = 4;
  Z_BLOCK = 5;
(* Allowed flush values; see deflate() and inflate() below for details *)

  Z_OK = 0;
  Z_STREAM_END = 1;
  Z_NEED_DICT = 2;
  Z_ERRNO = (-1);
  Z_STREAM_ERROR = (-2);
  Z_DATA_ERROR = (-3);
  Z_MEM_ERROR = (-4);
  Z_BUF_ERROR = (-5);
  Z_VERSION_ERROR = (-6);
(* Return codes for the compression/decompression functions. Negative
 * values are errors, positive values are used for special but normal events.
 *)

  Z_NO_COMPRESSION = 0;
  Z_BEST_SPEED = 1;
  Z_BEST_COMPRESSION = 9;
  Z_DEFAULT_COMPRESSION = -(1);
(* compression levels *)

  Z_FILTERED = 1;
  Z_HUFFMAN_ONLY = 2;
  Z_RLE = 3;
  Z_FIXED = 4;
  Z_DEFAULT_STRATEGY = 0;
(* compression strategy; see deflateInit2() below for details *)

  Z_BINARY = 0;
  Z_TEXT = 1;
  Z_ASCII = Z_TEXT; // for compatibility with 1.2.2 and earlier
  Z_UNKNOWN = 2;
(* Possible values of the data_type field (though see inflate()) *)

  Z_DEFLATED = 8;
(* The deflate compression method (the only one supported in this version) *)

  Z_NULL = 0;
(* for initializing zalloc, zfree, opaque *)

function zlibVersion:pchar; cdecl; external;
function deflateInit_(var strm: z_stream; level: cint; version: pchar; stream_size: cint): cint; cdecl; external;
function deflateInit(var strm: z_stream; level: cint): cint;
function deflate(var strm: z_stream; flush: cint): cint; cdecl; external;
function deflateEnd(var strm: z_stream): cint; cdecl; external;
function inflateInit_(var strm: z_stream; version: pchar; stream_size: cint): cint; cdecl; external;
function inflateInit(var strm: z_stream): cint;
function inflate(var strm: z_stream; flush: cint): cint; cdecl; external;
function inflateEnd(var strm: z_stream): cint; cdecl; external;
function deflateInit2_(var strm: z_stream; level, method, windowBits, memLevel, strategy: cint; version: pchar; stream_size: cint): cint; cdecl; external;
function deflateInit2(var strm: z_stream; level, method, windowBits, memLevel, strategy: cint): longint;
function inflateInit2_(var strm: z_stream; windowBits: cint; version:pchar; stream_size: cint): cint; cdecl; external;
function inflateInit2(var strm: z_stream; windowBits: cint): cint;
function crc32(crc: uLong; buf: pbytef; len: uInt): uLong; cdecl; external;
function zError(err: cint): pchar; cdecl; external;

type
        Tcompressionlevel=(
          clnone,                     {Do not use compression, just copy data.}
          clfastest,                  {Use fast (but less) compression.}
          cldefault,                  {Use default compression}
          clmax                       {Use maximum compression}
        );

        Tcustomzlibstream=class(Townerstream)
        protected
          Fstream:z_stream;
          Fbuffer:pointer;
          Fonprogress:Tnotifyevent;
          procedure progress(sender:Tobject);
          property onprogress:Tnotifyevent read Fonprogress write Fonprogress;
        public
          constructor create(stream:Tstream);
          destructor destroy;override;
        end;

        Tcompressionstream=class(Tcustomzlibstream)
        private
          FZlibInitialized: Boolean;
          FFlushOnDestroy: Boolean;
        protected
          raw_written,compressed_written:longint;
        public
          constructor create(level:Tcompressionlevel;
                             dest:Tstream;
                             Askipheader:boolean=false;
                             FlushOnDestroy: Boolean = TRUE);
          destructor destroy;override;
          function write(const buffer;count:longint):longint;override;
          procedure flush;
          function get_compressionrate:single;
        end;

        Tdecompressionstream=class(Tcustomzlibstream)
        private
          FZlibInitialized: Boolean;
        protected
          raw_read,compressed_read:longint;
          skipheader:boolean;
          procedure reset;
          function GetPosition() : Int64; override;
        public
          constructor create(Asource:Tstream;Askipheader:boolean=false);
          destructor destroy;override;
          function read(var buffer;count:longint):longint;override;
          function seek(offset:longint;origin:word):longint;override;
          function get_compressionrate:single;
        end;

        Ezliberror=class(Estreamerror)
        end;

        Ecompressionerror=class(Ezliberror)
        end;

        Edecompressionerror=class(Ezliberror)
        end;

{***************************************************************************}
                                 implementation
{***************************************************************************}

function deflateInit(var strm: z_stream; level: cint): cint;
begin
  Result := deflateInit_(strm, level, ZLIB_VERSION, sizeof(z_stream));
end;

function inflateInit(var strm: z_stream): cint;
begin
  Result := inflateInit_(strm, ZLIB_VERSION, sizeof(z_stream));
end;

function deflateInit2(var strm: z_stream; level, method, windowBits, memLevel, strategy: cint): cint;
begin
  Result := deflateInit2_(strm, level, method, windowBits, memLevel, strategy, ZLIB_VERSION, sizeof(z_stream));
end;

function inflateInit2(var strm: z_stream; windowBits: cint): cint;
begin
  Result := inflateInit2_(strm, windowBits, ZLIB_VERSION, sizeof(z_stream));
end;

const   bufsize=16384;     {Size of the buffer used for temporarily storing
                            data from the child stream.}

const
  Sseek_failed = 'Seek in deflate compressed stream failed.';

const
  MAX_WBITS = 15;
  DEF_MEM_LEVEL = 8;

{$L obj/adler32.o}
{$L obj/compress.o}
{$L obj/crc32.o}
{$L obj/deflate.o}
{$L obj/infback.o}
{$L obj/inffast.o}
{$L obj/inflate.o}
{$L obj/inftrees.o}
{$L obj/trees.o}
{$L obj/uncompr.o}
{$L obj/zutil.o}

function malloc(Size: Integer): Pointer; cdecl; public;
begin
  try
    Result := AllocMem(Size);
  except
    Result := nil;
  end;
end;

procedure free(Block: Pointer); cdecl; public;
begin
  FreeMem(Block);
end;

constructor Tcustomzlibstream.create(stream:Tstream);

begin
  assert(stream<>nil);
  inherited create(stream);
  getmem(Fbuffer,bufsize);
end;

procedure Tcustomzlibstream.progress(sender:Tobject);

begin
  if Fonprogress<>nil then
    Fonprogress(sender);
end;

destructor Tcustomzlibstream.destroy;

begin
  freemem(Fbuffer);
  inherited destroy;
end;

{***************************************************************************}

constructor Tcompressionstream.create(level:Tcompressionlevel;
                                      dest:Tstream;
                                      Askipheader:boolean=false;
                                      FlushOnDestroy: Boolean = TRUE);

var err,l:smallint;

begin
  FFlushOnDestroy := FlushOnDestroy;
  inherited create(dest);
  Fstream.next_out:=Fbuffer;
  Fstream.avail_out:=bufsize;

  case level of
    clnone:
      l:=Z_NO_COMPRESSION;
    clfastest:
      l:=Z_BEST_SPEED;
    cldefault:
      l:=Z_DEFAULT_COMPRESSION;
    clmax:
      l:=Z_BEST_COMPRESSION;
  end;

  if Askipheader then
    err:=deflateInit2(Fstream,l,Z_DEFLATED,-MAX_WBITS,DEF_MEM_LEVEL,0)
  else
    err:=deflateInit(Fstream,l);
  if err<>Z_OK then
    raise Ecompressionerror.create(zerror(err));
  FZlibInitialized := TRUE;
end;

function Tcompressionstream.write(const buffer;count:longint):longint;

var err:smallint;
    lastavail,
    written:longint;

begin
  Fstream.next_in:=@buffer;
  Fstream.avail_in:=count;
  lastavail:=count;
  while Fstream.avail_in<>0 do
    begin
      if Fstream.avail_out=0 then
        begin
          { Flush the buffer to the stream and update progress }
          written:=source.write(Fbuffer^,bufsize);
          inc(compressed_written,written);
          inc(raw_written,lastavail-Fstream.avail_in);
          lastavail:=Fstream.avail_in;
          progress(self);
          { reset output buffer }
          Fstream.next_out:=Fbuffer;
          Fstream.avail_out:=bufsize;
        end;
      err:=deflate(Fstream,Z_NO_FLUSH);
      if err<>Z_OK then
        raise Ecompressionerror.create(zerror(err));
    end;
  inc(raw_written,lastavail-Fstream.avail_in);
  write:=count;
end;

function Tcompressionstream.get_compressionrate:single;

begin
  get_compressionrate:=100*compressed_written/raw_written;
end;


procedure Tcompressionstream.flush;

var err:smallint;
    written:longint;

begin
  {Compress remaining data still in internal zlib data buffers.}
  repeat
    if Fstream.avail_out=0 then
      begin
        { Flush the buffer to the stream and update progress }
        written:=source.write(Fbuffer^,bufsize);
        inc(compressed_written,written);
        progress(self);
        { reset output buffer }
        Fstream.next_out:=Fbuffer;
        Fstream.avail_out:=bufsize;
      end;
    err:=deflate(Fstream,Z_FINISH);
    if err=Z_STREAM_END then
      break;
    if (err<>Z_OK) then
      raise Ecompressionerror.create(zerror(err));
  until false;
  if Fstream.avail_out<bufsize then
    begin
      source.writebuffer(FBuffer^,bufsize-Fstream.avail_out);
      inc(compressed_written,bufsize-Fstream.avail_out);
      progress(self);
    end;
end;


destructor Tcompressionstream.destroy;

begin
  if FZlibInitialized then
  begin
    try
      if FFlushOnDestroy then
        Flush;
    finally
      deflateEnd(Fstream);
    end;
  end;
  inherited destroy;
end;

{***************************************************************************}

constructor Tdecompressionstream.create(Asource:Tstream;Askipheader:boolean=false);

var err:smallint;

begin
  inherited create(Asource);

  skipheader:=Askipheader;
  if Askipheader then
    err:=inflateInit2(Fstream,-MAX_WBITS)
  else
    err:=inflateInit(Fstream);
  if err<>Z_OK then
    raise Edecompressionerror.create(zerror(err));
  FZlibInitialized := TRUE;
end;

function Tdecompressionstream.read(var buffer;count:longint):longint;

var err:smallint;
    lastavail:longint;

begin
  Fstream.next_out:=@buffer;
  Fstream.avail_out:=count;
  lastavail:=count;
  while Fstream.avail_out<>0 do
    begin
      if Fstream.avail_in=0 then
        begin
          {Refill the buffer.}
          Fstream.next_in:=Fbuffer;
          Fstream.avail_in:=source.read(Fbuffer^,bufsize);
          inc(compressed_read,Fstream.avail_in);
          inc(raw_read,lastavail-Fstream.avail_out);
          lastavail:=Fstream.avail_out;
          progress(self);
        end;
      err:=inflate(Fstream,Z_NO_FLUSH);
      if err=Z_STREAM_END then
        break;
      if err<>Z_OK then
        raise Edecompressionerror.create(zerror(err));
    end;
  if err=Z_STREAM_END then
    dec(compressed_read,Fstream.avail_in);
  inc(raw_read,lastavail-Fstream.avail_out);
  read:=count-Fstream.avail_out;
end;

procedure Tdecompressionstream.reset;

var err:smallint;

begin
  source.seek(-compressed_read,sofromcurrent);
  raw_read:=0;
  compressed_read:=0;
  inflateEnd(Fstream);
  if skipheader then
    err:=inflateInit2(Fstream,-MAX_WBITS)
  else
    err:=inflateInit(Fstream);
  if err<>Z_OK then
    raise Edecompressionerror.create(zerror(err));
end;

function Tdecompressionstream.GetPosition() : Int64;
begin
  GetPosition := raw_read;
end;

function Tdecompressionstream.seek(offset:longint;origin:word):longint;

var c:longint;

begin
  if (origin=sofrombeginning) or
     ((origin=sofromcurrent) and (offset+raw_read>=0)) then
    begin
      if origin = sofromcurrent then
        seek := raw_read + offset
      else
        seek := offset;

      if origin=sofrombeginning then
        dec(offset,raw_read);
      if offset<0 then
        begin
          inc(offset,raw_read);
          reset;
        end;
      while offset>0 do
        begin
          c:=offset;
          if c>bufsize then
            c:=bufsize;
          c:=read(Fbuffer^,c);
          dec(offset,c);
        end;
    end
  else
    raise Edecompressionerror.create(Sseek_failed);
end;

function Tdecompressionstream.get_compressionrate:single;

begin
  get_compressionrate:=100*compressed_read/raw_read;
end;


destructor Tdecompressionstream.destroy;

begin
  if FZlibInitialized then
    inflateEnd(Fstream);
  inherited destroy;
end;

end.
