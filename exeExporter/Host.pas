{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit Host;

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, VisualUtils;

type
  THostForm = class(TForm)
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    function HostedControl: TControl;
    { private declarations }
  public
    constructor Create(Owner: TComponent); override;
    { public declarations }
  end;

implementation

{$R *.lfm}

{ THostForm }

procedure THostForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if HostedControl is ICloseQueryListener then
    CanClose := (HostedControl as ICloseQueryListener).CanClose;
end;

function THostForm.HostedControl: TControl;
begin
  if ControlCount > 0 then
    Result := Controls[0]
  else
    Result := nil;
end;

constructor THostForm.Create(Owner: TComponent);
begin
  inherited;
  Width := ScalePixels(900);
  Height := ScalePixels(600);
end;

end.
