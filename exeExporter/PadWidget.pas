{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit PadWidget;

interface

uses
  Classes, SysUtils, Pad, Controls, Windows, VisualUtils, Graphics, Math,
  MiscUtils, Types, LCLType, Usp, GdiPlus;

type
  TPadWidget = class;
  TPadLine = class;
  TPadLineList = TGenericObjectList<TPadLine>;
  TPadBlock = class;
  TPadBlockList = TGenericObjectList<TPadBlock>;

  TPadWidget = class(TCustomControl)
  private const
    BORDER_WIDTH = 1;
  private type
    TPadPosition = (ppBefore, ppLine, ppAfter);
  private
    FModel: TPad;

    FMargin: Integer;
    FSpacing: Integer; { interline spacing in pixels }
    FBorder: Boolean;

    FLines: TPadLineList;

    FLayoutValid: Boolean;
    FLaidOutSize: TSize;
    FRawHeight: Integer; { valid when FLayoutValid = TRUE }

    FMouseListeners: TMouseListenerList;

    procedure LayOut;
    procedure SetMargin(Value: Integer);
    procedure SetSpacing(Value: Integer);
    function YToLine(y: Integer): TPadLine;
    procedure LayoutNeeded;
    procedure InvalidateLayout;
    function YToLineEx(y: Integer; out Position: TPadPosition): TPadLine;
    function InternalGetContentHeight: Integer;
    function GetContentHeight: Integer;
    procedure SetBorder(Value: Boolean);
    procedure ModelChange(Sender: TObject);
    procedure ModelEndModify(Sender: TObject);
  protected
    procedure Paint; override;
    procedure Resize; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure MouseMove(Shift: TShiftState; X, Y: Integer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure SetParent(Parent: TWinControl); override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure WMEraseBkgnd(var Message: TWMEraseBkgnd); message WM_ERASEBKGND;
    procedure CalculatePreferredSize(var PreferredWidth, PreferredHeight: Integer; WithThemeSpace: Boolean); override;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure AddMouseListener(const Listener: IMouseListener);
    procedure RemoveMouseListener(const Listener: IMouseListener);

    property Border: Boolean read FBorder write SetBorder;
    property ContentHeight: Integer read GetContentHeight;
    property Margin: Integer read FMargin write SetMargin;
    property Model: TPad read FModel;
    property Spacing: Integer read FSpacing write SetSpacing;
  end;
  TPadWidgetList = TGenericObjectList<TPadWidget>;

  TPadLine = class
  private
    FIndex: Integer;
    FTop: Integer;
    FHeight: Integer; { line height not including interline spacing }
    FBlocks: TPadBlockList;
    function GetBlockCount: Integer;
  public
    constructor Create;
    destructor Destroy; override;

    property BlockCount: Integer read GetBlockCount;
  end;

  TPadBlock = class
  private
    FSource: TPadObject;
    FLine: TPadLine;
    FPosition: TPoint;
    FWidget: TPadWidget;
  protected
    procedure Draw(Canvas: TCanvas; const p: TPoint); virtual;
    function GetHeight: Integer; virtual; abstract;
    function GetRequiredWidth: Integer; virtual; abstract;
    function GetDesiredWidth: Integer; virtual;
    function GetCharCount: Integer; virtual; abstract;
  public
    constructor Create(Source: TPadObject; Widget: TPadWidget);

    property Source: TPadObject read FSource;
  end;

  TTextPadBlock = class(TPadBlock)
  private
    FStartPos: Integer;
    FLen: Integer;
    function GetSource: TTextPadObject;
    function GetText: UnicodeString;
  protected
    procedure Draw(Canvas: TCanvas; const p: TPoint); override;
    function GetHeight: Integer; override;
    function GetRequiredWidth: Integer; override;
    function GetDesiredWidth: Integer; override;
    function GetCharCount: Integer; override;
  public
    constructor Create(Source: TPadObject; Widget: TPadWidget; StartPos, Len: Integer);

    property Source: TTextPadObject read GetSource;
    property Text: UnicodeString read GetText;
  end;

  TGraphicPadBlock = class(TPadBlock)
  private
    FWidth: Integer;
    FHeight: Integer;
    function GetHorizontalSpacing: Integer;
    function GetSource: TGraphicPadObject;
  protected
    function GetHeight: Integer; override;
    function GetRequiredWidth: Integer; override;
    procedure Draw(Canvas: TCanvas; const p: TPoint); override;
    function GetCharCount: Integer; override;
  public
    constructor Create(Source: TPadObject; Widget: TPadWidget);

    property Source: TGraphicPadObject read GetSource;
  end;

  TLineFeedPadBlock = class(TPadBlock)
  protected
    function GetHeight: Integer; override;
    function GetRequiredWidth: Integer; override;
    function GetCharCount: Integer; override;
  end;

implementation

const
  BAD_IMAGE_PLACEHOLDER_SIZE = 64;
  BAD_IMAGE_PLACEHOLDER_COLOR = clMedGray;

function ObtainCachedImage(g: TGraphicPadObject): TGdiPlusImage;
begin
  if not (g.CachedImage is TGdiPlusImage) then
    g.CachedImage := TGdiPlusImage.Create(Pointer(g.ImageData), Length(g.ImageData));
  Result := g.CachedImage as TGdiPlusImage;
end;

{ TPadWidget }

constructor TPadWidget.Create(Owner: TComponent);
begin
  inherited;
  FModel := TPad.Create;
  FModel.OnChange := ModelChange;
  FModel.OnEndModify := ModelEndModify;

  FLines := TPadLineList.Create;
  FMargin := 5;
  FSpacing := 2;
  Width := 300;
  Height := 200;
  Color := clWhite;
  ControlStyle := ControlStyle + [csOpaque];
  Font.Size := 12;
  Font.Name := 'Arial';
  Font.Color := clBlack;
  FMouseListeners := TMouseListenerList.Create;
end;

destructor TPadWidget.Destroy;
begin
  if FModel <> nil then
  begin
    FModel.OnChange := nil;
    FModel.OnEndModify := nil;
  end;

  FreeAndNil(FLines);
  FreeAndNil(FMouseListeners);
  FreeAndNil(FModel);
  inherited;
end;

procedure GetWordBoundaries(CharAttrs: PSCRIPT_LOGATTR; CharCount: Integer; Boundaries: TIntegerList);
var
  i: Integer;
begin
  if CharCount > 0 then
  begin
    Boundaries.Add(1);
    for i := 2 to CharCount do
    begin
      Inc(CharAttrs);
      if CharAttrs^ and fSoftBreak <> 0 then
        Boundaries.Add(i);
    end;
  end;
end;

procedure TPadWidget.LayOut;
var
  y: Integer;
  x, CurObj, RightMargin: Integer;
  CurLine: TPadLine;
  Blocks: TPadBlockList;
  Words: TIntegerList;
  Block: TPadBlock;
  Done: Boolean;
  Ctx: SCRIPT_STRING_ANALYSIS;
  CharAttrs: PSCRIPT_LOGATTR;

  procedure FinishLine;
  var
    MidLine: Integer;
    b: TPadBlock;
  begin
    if CurLine <> nil then
    begin
      MidLine := y + CurLine.FHeight div 2;
      for b in CurLine.FBlocks do
        b.FPosition.y := MidLine - b.GetHeight div 2;
      Inc(y, CurLine.FHeight + FSpacing);
      CurLine := nil;
    end;
  end;

  procedure FetchBlocks;
  var
    Obj: TPadObject;
    i, OldBlockCount: Integer;
    s: UnicodeString;
  begin
    OldBlockCount := Blocks.Count;
    while (CurObj < FModel.ObjectCount) and (Blocks.Count = OldBlockCount) do
    begin
      Obj := FModel.Objects[CurObj];
      if Obj is TTextPadObject then
      begin
        s := TTextPadObject(Obj).Text;
        if s <> '' then
        begin
          Assert( ScriptStringAnalyse(0, Pointer(s), Length(s), 0, -1,
            SSA_BREAK, 0, nil, nil, nil, nil, nil, Ctx) = S_OK );
          try
            CharAttrs := ScriptString_pLogAttr(Ctx);
            Assert( CharAttrs <> nil );
            Words.Clear;
            GetWordBoundaries(CharAttrs, Length(s), Words);
            if Words.Count > 0 then
            begin
              Words.Add(Length(s)+1);
              for i := 0 to Words.Count-2 do
                Blocks.AddSafely(TTextPadBlock.Create(Obj, Self, Words[i],
                  Words[i+1] - Words[i]));
            end;
          finally
            Assert( ScriptStringFree(Ctx) = S_OK );
          end;
        end;
      end
      else if Obj is TGraphicPadObject then
        Blocks.AddSafely(TGraphicPadBlock.Create(Obj, Self))
      else if Obj is TLineFeedPadObject then
        Blocks.AddSafely(TLineFeedPadBlock.Create(Obj, Self));
      Inc(CurObj);
    end;
  end;

begin
  RightMargin := Width;
  if FBorder then
    Dec(RightMargin, 2 * BORDER_WIDTH);
  Dec(RightMargin, FMargin);

  if FLines <> nil then
  begin
    FLines.Clear;
    if Parent <> nil then
    begin
      Canvas.Font.Assign(Font);
      CurObj := 0;
      CurLine := nil;
      y := FMargin;
      Done := FALSE;
      Words := TIntegerList.Create;
      try
        Blocks := TPadBlockList.Create;
        try
          repeat
            if Blocks.Count = 0 then
              FetchBlocks;
            if Blocks.Count > 0 then
            begin
              if CurLine = nil then
              begin
                CurLine := TPadLine.Create;
                try
                  CurLine.FIndex := FLines.Count;
                  CurLine.FTop := y;
                  FLines.Add(CurLine);
                except
                  CurLine.Free;
                  raise;
                end;
                x := FMargin;
              end;
              Block := Blocks.First;
              if not (Block is TLineFeedPadBlock) and
                (x + Block.GetRequiredWidth > RightMargin) and
                (CurLine.BlockCount > 0) then
                FinishLine
              else
              begin
                Block.FPosition.x := x;
                Inc(x, Block.GetDesiredWidth);
                CurLine.FHeight := Max(CurLine.FHeight, Block.GetHeight);
                CurLine.FBlocks.Add(Block);
                Block.FLine := CurLine;
                Blocks.Extract(Block);
                if Block is TLineFeedPadBlock then
                  FinishLine;
              end;
            end
            else
              Done := TRUE;
          until Done;
          FinishLine;
        finally
          Blocks.Free;
        end;
      finally
        Words.Free;
      end;

      if FLines.Count > 0 then
        FRawHeight := FLines.Last.FTop + FLines.Last.FHeight + FMargin
      else
        FRawHeight := 2*FMargin;
    end;
  end;
end;

procedure TPadWidget.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Listener: IMouseListener;
begin
  inherited;
  if not Focused and CanFocus then
    TrySetFocus(Self);
  for Listener in FMouseListeners do
    Listener.NotifyMouseDown(Self, Button, Shift, X, Y);
end;

procedure TPadWidget.MouseMove(Shift: TShiftState; X, Y: Integer);
var
  Listener: IMouseListener;
begin
  inherited;
  for Listener in FMouseListeners do
    Listener.NotifyMouseMove(Self, Shift, X, Y);
end;

procedure TPadWidget.Paint;
var
  i: Integer;
  Line: TPadLine;
  Block: TPadBlock;
  y, FirstLine, LastLine: Integer;
  ClipRect: TRect;
  ClipWidth, ClipHeight: Integer;
  Bmp: TBitmap;
begin
  inherited;
  LayoutNeeded;

  ClipRect := Canvas.ClipRect;
  ClipWidth := ClipRect.Right - ClipRect.Left;
  ClipHeight := ClipRect.Bottom - ClipRect.Top;
  if (ClipWidth > 0) and (ClipHeight > 0) then
  begin
    Bmp := TBitmap.Create;
    try
      Bmp.Width := ClipWidth;
      Bmp.Height := ClipHeight;
      Bmp.Canvas.Brush.Color := Color;
      Bmp.Canvas.FillRect(Rect(0, 0, ClipWidth, ClipHeight));
      if FLines.Count > 0 then
      begin
        Bmp.Canvas.Font.Assign(Font);

        Line := YToLine(ClipRect.Top);
        if Line <> nil then
          FirstLine := Line.FIndex
        else
          FirstLine := 0;
        Line := YToLine(ClipRect.Bottom);
        if Line <> nil then
          LastLine := Line.FIndex
        else
          LastLine := FLines.Count-1;

        for i := FirstLine to LastLine do
        begin
          Line := FLines[i];
          for Block in Line.FBlocks do
          begin
            y := Block.FPosition.y;
            Block.Draw(Bmp.Canvas, Point(Block.FPosition.x - ClipRect.Left, y - ClipRect.Top));
          end;
        end;
      end;

      Canvas.Draw(ClipRect.Left, ClipRect.Top, Bmp);
    finally
      Bmp.Free;
    end;
  end;
end;

procedure TPadWidget.Resize;
begin
  inherited;
  if not FLayoutValid or (Width <> FLaidOutSize.cx) or (Height <> FLaidOutSize.cy) then
    InvalidateLayout;
  Invalidate;
end;

procedure TPadWidget.SetMargin(Value: Integer);
begin
  if FMargin <> Value then
  begin
    Assert( Value >= 0 );
    FMargin := Value;

    InvalidateLayout;
    Invalidate;
  end;
end;

procedure TPadWidget.SetParent(Parent: TWinControl);
begin
  if Parent <> Self.Parent then
  begin
    InvalidateLayout;
    inherited;
    if (Parent = nil) and (FLines <> nil) then
      FLines.Clear;
    Invalidate;
  end;
end;

procedure TPadWidget.SetSpacing(Value: Integer);
begin
  if FSpacing <> Value then
  begin
    Assert( Value >= 0 );
    FSpacing := Value;

    InvalidateLayout;
    Invalidate;
  end;
end;

procedure TPadWidget.WMEraseBkgnd(var Message: TWMEraseBkgnd);
begin
  Message.Result := 1;
end;

procedure TPadWidget.CalculatePreferredSize(var PreferredWidth, PreferredHeight: Integer; WithThemeSpace: Boolean);
begin
  PreferredHeight := ContentHeight;
  if FLines.Count = 0 then
    Inc(PreferredHeight, Canvas.TextHeight('|'));
  if FBorder then
    Inc(PreferredHeight, 2 * BORDER_WIDTH);
end;

function TPadWidget.YToLine(y: Integer): TPadLine;
var
  DummyPosition: TPadPosition;
begin
  Result := YToLineEx(y, DummyPosition);
end;

function TPadWidget.YToLineEx(y: Integer; out Position: TPadPosition): TPadLine;
var
  Line: TPadLine;
begin
  LayoutNeeded;
  Result := nil;
  Position := ppBefore;
  if y >= FMargin then
  begin
    if y >= FRawHeight - FMargin then
      Position := ppAfter
    else
    begin
      for Line in FLines do
      begin
        if y < Line.FTop + Line.FHeight then
        begin
          Result := Line;
          Position := ppLine;
          Break;
        end;
      end;
    end;
  end;
end;

procedure TPadWidget.LayoutNeeded;
begin
  if not FLayoutValid then
  begin
    LayOut;
    FLaidOutSize := Size(Width, Height);
    FLayoutValid := TRUE;
  end;
end;

procedure TPadWidget.InvalidateLayout;
begin
  FLayoutValid := FALSE;
  InvalidatePreferredSize;
end;

function TPadWidget.GetContentHeight: Integer;
begin
  LayoutNeeded;
  Result := InternalGetContentHeight;
end;

function TPadWidget.InternalGetContentHeight: Integer;
begin
  Result := FRawHeight;
  if (FLines.Count > 0) and (FLines.Last.FBlocks.Last is TLineFeedPadBlock) then
    Inc(Result, Canvas.TextHeight('|') + FSpacing);
end;

procedure TPadWidget.CreateParams(var Params: TCreateParams);
begin
  inherited;
  if FBorder then
    Params.Style := Params.Style or WS_BORDER;
end;

procedure TPadWidget.SetBorder(Value: Boolean);
begin
  if FBorder <> Value then
  begin
    FBorder := Value;
    RecreateWnd(Self);
  end;
end;

procedure TPadWidget.ModelChange(Sender: TObject);
begin
  InvalidateLayout;
  Invalidate;
end;

procedure TPadWidget.ModelEndModify(Sender: TObject);
begin
  AdjustSize;
end;

procedure TPadWidget.MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Listener: IMouseListener;
begin
  inherited;
  for Listener in FMouseListeners do
    Listener.NotifyMouseUp(Self, Button, Shift, X, Y);
end;

procedure TPadWidget.AddMouseListener(const Listener: IMouseListener);
begin
  FMouseListeners.Add(Listener);
end;

procedure TPadWidget.RemoveMouseListener(const Listener: IMouseListener);
begin
  FMouseListeners.Remove(Listener);
end;

{ TPadLine }

constructor TPadLine.Create;
begin
  inherited;
  FBlocks := TPadBlockList.Create;
end;

destructor TPadLine.Destroy;
begin
  FreeAndNil(FBlocks);
  inherited;
end;

function TPadLine.GetBlockCount: Integer;
begin
  if FBlocks <> nil then
    Result := FBlocks.Count
  else
    Result := 0;
end;

{ TLineFeedPadBlock }

function TLineFeedPadBlock.GetCharCount: Integer;
begin
  Result := 1;
end;

function TLineFeedPadBlock.GetHeight: Integer;
begin
  Result := FWidget.Canvas.TextHeight('|');
end;

function TLineFeedPadBlock.GetRequiredWidth: Integer;
begin
  Result := 0;
end;

{ TPadBlock }

constructor TPadBlock.Create(Source: TPadObject; Widget: TPadWidget);
begin
  inherited Create;
  FSource := Source;
  FWidget := Widget;
end;

procedure TPadBlock.Draw(Canvas: TCanvas; const p: TPoint);
begin
  { do nothing }
end;

function TPadBlock.GetDesiredWidth: Integer;
begin
  Result := GetRequiredWidth;
end;

{ TTextPadBlock }

constructor TTextPadBlock.Create(Source: TPadObject; Widget: TPadWidget; StartPos, Len: Integer);
begin
  Assert( Source is TTextPadObject );
  inherited Create(Source, Widget);
  FStartPos := StartPos;
  FLen := Len;
end;

procedure TTextPadBlock.Draw(Canvas: TCanvas; const p: TPoint);
begin
  inherited;
  Canvas.Brush.Style := bsClear;
  Canvas.Font.Color := FWidget.Font.Color;
  Canvas.TextOut(p.x, p.y, UTF8Encode(Text));
  Canvas.Brush.Style := bsSolid;
end;

function TTextPadBlock.GetCharCount: Integer;
begin
  Result := FLen;
end;

function TTextPadBlock.GetDesiredWidth: Integer;
begin
  try
    Result := FWidget.Canvas.TextWidth(UTF8Encode(Text));
  except
    Result := 0;
  end;
end;

function TTextPadBlock.GetHeight: Integer;
begin
  try
    Result := FWidget.Canvas.TextHeight(UTF8Encode(Text));
  except
    Result := 0;
  end;
end;

function TTextPadBlock.GetRequiredWidth: Integer;
begin
  try
    Result := FWidget.Canvas.TextWidth(UTF8Encode(TrimRight(Text)));
  except
    Result := 0;
  end;
end;

function TTextPadBlock.GetSource: TTextPadObject;
begin
  Result := TTextPadObject(inherited Source);
end;

function TTextPadBlock.GetText: UnicodeString;
begin
  Result := Copy(Source.Text, FStartPos, FLen);
end;

{ TGraphicPadBlock }

constructor TGraphicPadBlock.Create(Source: TPadObject; Widget: TPadWidget);
var
  Image: TGdiPlusImage;
begin
  inherited Create(Source, Widget);
  Image := ObtainCachedImage(Source as TGraphicPadObject);
  if Image.Valid then
  begin
    FWidth := Image.Width;
    FHeight := Image.Height;
  end
  else
  begin
    FWidth := BAD_IMAGE_PLACEHOLDER_SIZE;
    FHeight := BAD_IMAGE_PLACEHOLDER_SIZE;
  end;
end;

procedure TGraphicPadBlock.Draw(Canvas: TCanvas; const p: TPoint);
var
  Image: TGdiPlusImage;
  hs: Integer;
begin
  inherited;
  Image := ObtainCachedImage(Source);
  hs := GetHorizontalSpacing;

  if Image.Valid then
    Image.Draw(Canvas.Handle, p.x + hs, p.y)
  else
  begin
    Canvas.Brush.Color := BAD_IMAGE_PLACEHOLDER_COLOR;
    Canvas.FillRect(Rect(p.x + hs, p.y, p.x + hs + FWidth, p.y + FHeight));
  end;
end;

function TGraphicPadBlock.GetCharCount: Integer;
begin
  Result := 1;
end;

function TGraphicPadBlock.GetHeight: Integer;
begin
  Result := FHeight;
end;

function TGraphicPadBlock.GetHorizontalSpacing: Integer;
begin
  Result := 2;
end;

function TGraphicPadBlock.GetRequiredWidth: Integer;
begin
  Result := FWidth + 2*GetHorizontalSpacing;
end;

function TGraphicPadBlock.GetSource: TGraphicPadObject;
begin
  Result := TGraphicPadObject(inherited Source);
end;

end.
