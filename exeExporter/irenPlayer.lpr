{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

program irenPlayer;

{$IFDEF UPX_NOT_FOUND}
  {$ERROR RU_IRENPROJECT_UPX environment variable is not defined}
{$ENDIF}

uses
  GdiPlus{ must come before Interfaces for correct finalization order },
  Interfaces, Classes, SysUtils, Translator, Forms,

  SelectQuestionScreen, InputQuestionScreen, MatchQuestionScreen,
  OrderQuestionScreen, ClassifyQuestionScreen,

  ItImport, LCLType, LocalTestSession, Profile, VisualUtils, Host,
  Controls, TestCore, TestUtils, Windows, MiscUtils, ExeTestUtils;

{$R *.rc}

type
  TFinishWorkHandler = class
  private
    class procedure OnFinishWork(const PartRight: Double);
  end;

{ TFinishWorkHandler }

class procedure TFinishWorkHandler.OnFinishWork(const PartRight: Double);
begin
  ExitCode := 10 + Round(PartRight * 100);
end;

function LoadTest: TTestDocument;
var
  ItStream: TMemoryStream;
  TestTitle, TestMimeTypeIgnored: String;
begin
  Result := nil;
  try
    ItStream := TMemoryStream.Create;
    try
      ExtractItStreamFromExe(GetExeFileName, ItStream, TestTitle, TestMimeTypeIgnored);
      Result := ImportFromItStream(ItStream);
    finally
      ItStream.Free;
    end;
    Application.Title := TestTitle;
  except
    Result.Free;
    raise;
  end;
end;

procedure OpenTest(TestDocument: TTestDocument; Host: TWinControl);
var
  Test: TTest;
  Profile: TProfile;
begin
  Test := TestDocument.ExtractTest;
  try
    if TestDocument.Profiles.Count = 0 then
      raise Exception.Create('No profiles.');

    Profile := TestDocument.Profiles.Extract(0);
    try
      OpenTestSession(Test, Profile, TFinishWorkHandler.OnFinishWork, Host);
    finally
      Profile.Free;
    end;
  finally
    Test.Free;
  end;
end;

type
  TApplicationExceptionHandler = class
  private
    class procedure ApplicationException(Sender: TObject; E: Exception);
  end;

class procedure TApplicationExceptionHandler.ApplicationException(
  Sender: TObject; E: Exception);
begin
  Application.MessageBox(PChar(E.Message), PChar(Application.Title), MB_OK or MB_ICONERROR);
end;

procedure Run;
var
  HostForm: THostForm;
  Doc: TTestDocument;
begin
  try
    Application.Initialize;
    InitializeProgram;
    Application.OnException := TApplicationExceptionHandler.ApplicationException;

    Doc := LoadTest;
    try
      Application.CreateForm(THostForm, HostForm);
      HostForm.Caption := Application.Title;
      OpenTest(Doc, HostForm);
    finally
      Doc.Free;
    end;
    Application.Run;
  except on E: Exception do
    Application.MessageBox(PChar(E.Message), PChar(Application.Title), MB_OK or MB_ICONERROR);
  end;
end;

begin
  Run;
end.
