{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit TestUiUtils;

interface

uses
  Classes, SysUtils, TestCore, Graphics;

type
  {$INTERFACES CORBA}
  IQuestionScreen = interface
    ['ea78e8d72ddd366f']
    procedure SetUp(Question: TQuestion);
    procedure GoReadOnly;
    function GetQuestion: TQuestion;
  end;
  {$INTERFACES DEFAULT}

  TDiagramColorScheme = class
  public
    class function UnansweredColor: TColor;
    class function AnsweredColor: TColor;
    class function GetColorForPartRight(PartRight: Single): TColor;
  end;

implementation

{ TDiagramColorScheme }

class function TDiagramColorScheme.AnsweredColor: TColor;
begin
  Result := $ff6600;
end;

class function TDiagramColorScheme.GetColorForPartRight(
  PartRight: Single): TColor;
begin
  if PartRight >= 0.99 then
    Result := $00e000
  else if PartRight <= 0.01 then
    Result := $0000e0
  else
    Result := $00ccff;
end;

class function TDiagramColorScheme.UnansweredColor: TColor;
begin
  Result := $808080;
end;

end.
