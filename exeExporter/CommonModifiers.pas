{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit CommonModifiers;

interface

uses
  Classes, SysUtils, Contnrs, StrUtils, MiscUtils, TestCore, SecureRandom;

type
  TSetEvaluationModelModifier = class(TModifier)
  private
    FLaxEvaluation: Boolean;
    FQuestionClasses: TQuestionClassList;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;

    property LaxEvaluation: Boolean read FLaxEvaluation write FLaxEvaluation;
    property QuestionClasses: TQuestionClassList read FQuestionClasses;
  end;

  TSetEvaluationModelAlterant = class(TAlterant)
  private
    FLaxEvaluation: Boolean;
  public
    procedure Apply(var Question: TQuestion); override;

    property LaxEvaluation: Boolean read FLaxEvaluation write FLaxEvaluation;
  end;

  TSetWeightModifier = class(TModifier)
  private
    FNewWeight: Integer;
    procedure SetNewWeight(Value: Integer);
  public
    constructor Create; override;
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;

    property NewWeight: Integer read FNewWeight write SetNewWeight;
  end;

  TSetWeightAlterant = class(TAlterant)
  private
    FNewWeight: Integer;
    procedure SetNewWeight(Value: Integer);
  public
    constructor Create; override;
    procedure Apply(var Question: TQuestion); override;

    property NewWeight: Integer read FNewWeight write SetNewWeight;
  end;

  TVariable = class
  private
    FName: String;
    FValue: String;
  public
    function Clone: TVariable;

    property Name: String read FName write FName;
    property Value: String read FValue write FValue;
  end;
  TVariableList = TGenericObjectList<TVariable>;

  TScriptModifier = class(TModifier)
  private
    FScript: TStringList;
    FVariableLists: TObjectList;
    procedure EnsureVariableListsParsed;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure GetScript(Script: TStrings);
    procedure SetScript(Script: TStrings);
    procedure Apply(var Question: TQuestion; Alterants: TAlterantList); override;
    procedure Assign(Source: TModifier); override;
  end;

  TExpandVariablesAlterant = class(TAlterant)
  private
    FVariableList: TVariableList;
  public
    constructor Create; override;
    destructor Destroy; override;
    procedure Apply(var Question: TQuestion); override;

    property VariableList: TVariableList read FVariableList;
  end;

implementation

function UnescapeVariableValue(const s: String): String;
var
  i, p, CharCode: Integer;
begin
  Result := '';
  i := 1;
  repeat
    p := PosEx('\', s, i);
    if p <> 0 then
    begin
      if (p > Length(s) - 5) or (s[p + 1] <> 'u') or not TryStrToInt('$' + Copy(s, p + 2, 4), CharCode) then
        raise Exception.Create('Incorrect character escape.');
      Result := Result + Copy(s, i, p - i) + UTF8Encode(UnicodeString(UnicodeChar(CharCode)));
      i := p + 6;
    end;
  until p = 0;
  Result := Result + Copy(s, i, Length(s) - i + 1);
end;

{ TVariable }

function TVariable.Clone: TVariable;
begin
  Result := TVariable.Create;
  Result.FName := FName;
  Result.FValue := FValue;
end;

{ TSetEvaluationModelModifier }

procedure TSetEvaluationModelModifier.Apply(var Question: TQuestion;
  Alterants: TAlterantList);
var
  Alterant: TSetEvaluationModelAlterant;
begin
  if (Question.LaxEvaluation <> FLaxEvaluation)
    and ((FQuestionClasses.Count = 0)
    or (FQuestionClasses.IndexOf(TQuestionClass(Question.ClassType)) <> -1)) then
  begin
    Alterant := TSetEvaluationModelAlterant.Create;
    Alterants.Add(Alterant);

    Alterant.LaxEvaluation := FLaxEvaluation;
    Alterant.Apply(Question);
  end;
end;

procedure TSetEvaluationModelModifier.Assign(Source: TModifier);
var
  m: TSetEvaluationModelModifier;
begin
  inherited;
  m := Source as TSetEvaluationModelModifier;
  FLaxEvaluation := m.FLaxEvaluation;
  FQuestionClasses.Assign(m.FQuestionClasses);
end;

constructor TSetEvaluationModelModifier.Create;
begin
  inherited;
  FLaxEvaluation := TRUE;
  FQuestionClasses := TQuestionClassList.Create;
end;

destructor TSetEvaluationModelModifier.Destroy;
begin
  FreeAndNil(FQuestionClasses);
  inherited;
end;

{ TSetEvaluationModelAlterant }

procedure TSetEvaluationModelAlterant.Apply(var Question: TQuestion);
begin
  Question.LaxEvaluation := FLaxEvaluation;
end;

{ TSetWeightModifier }

procedure TSetWeightModifier.Apply(var Question: TQuestion; Alterants: TAlterantList);
var
  Alterant: TSetWeightAlterant;
begin
  if Question.Weight <> FNewWeight then
  begin
    Alterant := TSetWeightAlterant.Create;
    Alterants.Add(Alterant);

    Alterant.NewWeight := FNewWeight;
    Alterant.Apply(Question);
  end;
end;

procedure TSetWeightModifier.Assign(Source: TModifier);
begin
  inherited;
  FNewWeight := (Source as TSetWeightModifier).FNewWeight;
end;

constructor TSetWeightModifier.Create;
begin
  inherited;
  FNewWeight := 1;
end;

procedure TSetWeightModifier.SetNewWeight(Value: Integer);
begin
  Assert( Value >= 1 );
  FNewWeight := Value;
end;

{ TSetWeightAlterant }

procedure TSetWeightAlterant.Apply(var Question: TQuestion);
begin
  Question.Weight := FNewWeight;
end;

procedure TSetWeightAlterant.SetNewWeight(Value: Integer);
begin
  Assert( Value >= 1 );
  FNewWeight := Value;
end;

constructor TSetWeightAlterant.Create;
begin
  inherited;
  FNewWeight := 1;
end;

{ TScriptModifier }

procedure TScriptModifier.Apply(var Question: TQuestion; Alterants: TAlterantList);
var
  Alterant: TExpandVariablesAlterant;
  ReplaceCount: Integer;
  v: TVariable;
  VariableList: TVariableList;
begin
  EnsureVariableListsParsed;
  VariableList := TVariableList(FVariableLists[SecureRandomInteger(FVariableLists.Count)]);

  Alterant := TExpandVariablesAlterant.Create;
  try
    for v in VariableList do
    begin
      ReplaceCount := Question.ReplaceString(Format('$(%s)', [v.Name]), v.Value);
      if ReplaceCount > 0 then
        Alterant.VariableList.AddSafely(v.Clone);
    end;
  except
    Alterant.Free;
    raise;
  end;

  if Alterant.VariableList.Count > 0 then
    Alterants.Add(Alterant)
  else
    Alterant.Free;
end;

procedure TScriptModifier.Assign(Source: TModifier);
begin
  inherited;
  FreeAndNil(FVariableLists);
  FScript.Assign((Source as TScriptModifier).FScript);
end;

procedure TScriptModifier.EnsureVariableListsParsed;
var
  VariableList: TVariableList;
  v: TVariable;
  Ok: Boolean;
  s: String;
  p: Integer;
begin
  if FVariableLists = nil then
  begin
    try
      VariableList := nil;
      try
        Ok := FALSE;

        for s in FScript do
        begin
          if FVariableLists = nil then
          begin
            if s <> '//@@EXPORTED_RESULTS@@' then
              Break;
            FVariableLists := TObjectList.Create;
          end
          else if s = '//@@END_EXPORTED_RESULTS@@' then
          begin
            Ok := VariableList = nil;
            Break;
          end
          else if s = '//' then
          begin
            if VariableList = nil then
              VariableList := TVariableList.Create;
            FVariableLists.Add(VariableList);
            VariableList := nil;
          end
          else
          begin
            if LeftStr(s, 2) <> '//' then
              Break;

            p := Pos('=', s);
            if p <= 3 then
              Break;

            if VariableList = nil then
              VariableList := TVariableList.Create;
            v := TVariable.Create;
            VariableList.AddSafely(v);

            v.Name := Copy(s, 3, p - 3);
            v.Value := UnescapeVariableValue(Copy(s, p + 1, Length(s) - p));
          end;
        end;

        if not Ok or (FVariableLists.Count = 0) then
          raise Exception.Create('Incorrect script variable data.');
      finally
        VariableList.Free;
      end;
    except
      FreeAndNil(FVariableLists);
      raise;
    end;
  end;
end;

constructor TScriptModifier.Create;
begin
  inherited;
  FScript := TStringList.Create;
end;

destructor TScriptModifier.Destroy;
begin
  FreeAndNil(FScript);
  FreeAndNil(FVariableLists);
  inherited;
end;

procedure TScriptModifier.GetScript(Script: TStrings);
begin
  Script.AddStrings(FScript);
end;

procedure TScriptModifier.SetScript(Script: TStrings);
begin
  FreeAndNil(FVariableLists);
  FScript.Assign(Script);
end;

{ TExpandVariablesAlterant }

procedure TExpandVariablesAlterant.Apply(var Question: TQuestion);
var
  v: TVariable;
begin
  for v in FVariableList do
    Question.ReplaceString(Format('$(%s)', [v.Name]), v.Value);
end;

constructor TExpandVariablesAlterant.Create;
begin
  inherited;
  FVariableList := TVariableList.Create;
end;

destructor TExpandVariablesAlterant.Destroy;
begin
  FreeAndNil(FVariableList);
  inherited;
end;

end.
