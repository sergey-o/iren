{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit StreamUtils;

interface

uses
  Classes, SysUtils, Math;

procedure WriteStreamInt64(Stream: TStream; Value: Int64);
procedure WriteStreamInteger(Stream: TStream; Value: Integer);
procedure WriteStreamCardinal(Stream: TStream; Value: Cardinal);
procedure WriteStreamSizedString(Stream: TStream; const s: String);
procedure WriteStreamString(Stream: TStream; const s: String);
procedure WriteStreamDouble(Stream: TStream; const Value: Double);
procedure WriteStreamSingle(Stream: TStream; Value: Single);
procedure WriteStreamBoolean(Stream: TStream; Value: Boolean);

function ReadStreamInt64(Stream: TStream): Int64;
function ReadStreamInteger(Stream: TStream): Integer;
function ReadStreamCardinal(Stream: TStream): Cardinal;
function ReadStreamSizedString(Stream: TStream): String;
function ReadStreamFixedString(Stream: TStream; StringLength: Integer): String;
function ReadStreamDouble(Stream: TStream): Double;
function ReadStreamSingle(Stream: TStream): Single;
function ReadStreamBoolean(Stream: TStream): Boolean;

function CopyStreamTail(Source, Target: TStream): Integer;
function ReadStreamTail(Stream: TStream): String;
function ReadUnseekableStreamTail(Stream: TStream): String;

implementation

procedure WriteStreamInt64(Stream: TStream; Value: Int64);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure WriteStreamInteger(Stream: TStream; Value: Integer);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure WriteStreamCardinal(Stream: TStream; Value: Cardinal);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure WriteStreamSizedString(Stream: TStream; const s: String);
begin
  WriteStreamInteger(Stream, Length(s));
  WriteStreamString(Stream, s);
end;

procedure WriteStreamString(Stream: TStream; const s: String);
begin
  if s <> '' then
    Stream.WriteBuffer(Pointer(s)^, Length(s));
end;

procedure WriteStreamDouble(Stream: TStream; const Value: Double);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure WriteStreamSingle(Stream: TStream; Value: Single);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

procedure WriteStreamBoolean(Stream: TStream; Value: Boolean);
begin
  Stream.WriteBuffer(Value, SizeOf(Value));
end;

function ReadStreamInt64(Stream: TStream): Int64;
begin
  {$HINTS OFF}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$HINTS ON}
end;

function ReadStreamInteger(Stream: TStream): Integer;
begin
  {$HINTS OFF}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$HINTS ON}
end;

function ReadStreamCardinal(Stream: TStream): Cardinal;
begin
  {$HINTS OFF}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$HINTS ON}
end;

function ReadStreamSizedString(Stream: TStream): String;
var
  Size: Integer;
begin
  Size := ReadStreamInteger(Stream);
  if (Size < 0) or (Size > Stream.Size - Stream.Position) then
    raise Exception.Create('Invalid string size when reading from stream.');

  SetLength(Result, Size);
  if Size > 0 then
    Stream.ReadBuffer(Result[1], Size);
end;

function ReadStreamFixedString(Stream: TStream; StringLength: Integer): String;
begin
  SetLength(Result, StringLength);
  if StringLength > 0 then
    Stream.ReadBuffer(Result[1], StringLength);
end;

function ReadStreamDouble(Stream: TStream): Double;
begin
  {$HINTS OFF}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$HINTS ON}
end;

function ReadStreamSingle(Stream: TStream): Single;
begin
  {$HINTS OFF}
  Stream.ReadBuffer(Result, SizeOf(Result));
  {$HINTS ON}
end;

function ReadStreamBoolean(Stream: TStream): Boolean;
begin
  Result := Stream.ReadByte <> 0;
end;

function CopyStreamTail(Source, Target: TStream): Integer;
{ Returns the number of bytes copied. }
var
  Buffer: array [0..4095] of Byte;
  Count: Integer;
begin
  Result := 0;
  repeat
    {$HINTS OFF}
    Count := Source.Read(Buffer, SizeOf(Buffer));
    {$HINTS ON}
    if Count > 0 then
    begin
      Target.WriteBuffer(Buffer, Count);
      Inc(Result, Count);
    end;
  until Count = 0;
end;

function ReadStreamTail(Stream: TStream): String;
var
  BytesLeft: Integer;
begin
  BytesLeft := Max(Stream.Size - Stream.Position, 0);
  SetLength(Result, BytesLeft);
  if BytesLeft > 0 then
    Stream.ReadBuffer(Result[1], BytesLeft);
end;

function ReadUnseekableStreamTail(Stream: TStream): String;
var
  Buffer: TMemoryStream;
begin
  Buffer := TMemoryStream.Create;
  try
    CopyStreamTail(Stream, Buffer);
    Buffer.Seek(0, soFromBeginning);
    Result := ReadStreamTail(Buffer);
  finally
    Buffer.Free;
  end;
end;

end.
