{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit Windows7Taskbar;

interface

uses
  Classes, SysUtils, Windows, ActiveX, shlobj, InterfaceBase;

type
  ITaskbarList3 = interface(ITaskbarList2)
    ['{ea1afb91-9e28-4b86-90e9-9e9f8a5eefaf}']
    function SetProgressValue(hwnd: HWND; ullCompleted, ullTotal: ULONGLONG): HRESULT; stdcall;
    function SetProgressState(hwnd: HWND; tbpFlags: Integer): HRESULT; stdcall;
  end;

  TWindows7Taskbar = class
  private
    FComInitialized: Boolean;
    FTaskbarList3: ITaskbarList3;
  public
    constructor Create;
    destructor Destroy; override;
    procedure SetProgressValue(const Completed, Total: QWord);
  end;

implementation

const
  TBPF_NOPROGRESS = 0;

{ TWindows7Taskbar }

constructor TWindows7Taskbar.Create;
begin
  inherited;
  if CheckWin32Version(6, 1) then
  begin
    FComInitialized := Succeeded(CoInitialize(nil));
    if FComInitialized then
      CoCreateInstance(CLSID_TaskbarList, nil, CLSCTX_INPROC_SERVER, ITaskbarList3, FTaskbarList3);
  end;
end;

destructor TWindows7Taskbar.Destroy;
begin
  if FTaskbarList3 <> nil then
  begin
    if WidgetSet <> nil then
      FTaskbarList3.SetProgressState(WidgetSet.AppHandle, TBPF_NOPROGRESS);
    FTaskbarList3 := nil;
  end;

  if FComInitialized then
    CoUninitialize;
  inherited;
end;

procedure TWindows7Taskbar.SetProgressValue(const Completed, Total: QWord);
begin
  if FTaskbarList3 <> nil then
    FTaskbarList3.SetProgressValue(WidgetSet.AppHandle, Completed, Total);
end;

end.
