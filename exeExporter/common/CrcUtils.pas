{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit CrcUtils;

interface

uses
  Classes, SysUtils, Math, zlibStream;

function ComputeStreamChunkCrc32(Stream: TStream; const Size: Int64): Cardinal;

implementation

function ComputeStreamChunkCrc32(Stream: TStream; const Size: Int64): Cardinal;
var
  Buffer: array [0..4095] of Byte;
  Remaining: Int64;
  n: Integer;
begin
  Assert( Size >= 0 );
  Remaining := Size;
  Result := crc32(0, nil, 0);

  while Remaining > 0 do
  begin
    n := Min(Remaining, SizeOf(Buffer));
    {$HINTS OFF}
    Stream.ReadBuffer(Buffer, n);
    {$HINTS ON}
    Result := crc32(Result, @Buffer, n);
    Dec(Remaining, n);
  end;
end;

end.
