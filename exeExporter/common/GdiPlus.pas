{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit GdiPlus;

interface

uses
  SysUtils, Windows, ActiveX;

const
  GDIPLUS_DLL = 'gdiplus.dll';
  GPSTATUS_OK = 0;

type
  GpStatus = Integer;
  PGpImage = Pointer;
  PGpGraphics = Pointer;

  GdiplusStartupInput = packed record
    GdiplusVersion: UINT32;
    DebugEventCallback: Pointer;
    SuppressBackgroundThread: BOOL;
    SuppressExternalCodecs: BOOL;
  end;

  TGdiPlusImage = class
  private
    FHandle: PGpImage;
    FWidth: Integer;
    FHeight: Integer;
    function GetValid: Boolean;
  public
    constructor Create(Buffer: Pointer; Size: Integer);
    destructor Destroy; override;
    procedure Draw(dc: HDC; x, y: Integer);

    property Height: Integer read FHeight;
    property Valid: Boolean read GetValid;
    property Width: Integer read FWidth;
  end;

function GdiplusStartup(out token: ULONG_PTR; const input: GdiplusStartupInput; output: Pointer): GpStatus;
  stdcall; external GDIPLUS_DLL;
procedure GdiplusShutdown(token: ULONG_PTR); stdcall; external GDIPLUS_DLL;

function GdipLoadImageFromStream(stream: IStream; out image: PGpImage): GpStatus; stdcall; external GDIPLUS_DLL;
function GdipDisposeImage(image: PGpImage): GpStatus; stdcall; external GDIPLUS_DLL;
function GdipGetImageWidth(image: PGpImage; out width: Cardinal): GpStatus; stdcall; external GDIPLUS_DLL;
function GdipGetImageHeight(image: PGpImage; out height: Cardinal): GpStatus; stdcall; external GDIPLUS_DLL;

function GdipCreateFromHDC(hdc: HDC; out graphics: PGpGraphics): GpStatus; stdcall; external GDIPLUS_DLL;
function GdipDeleteGraphics(graphics: PGpGraphics): GpStatus; stdcall; external GDIPLUS_DLL;
function GdipDrawImageRectI(graphics: PGpGraphics; image: PGpImage; x, y, width, height: Integer): GpStatus;
  stdcall; external GDIPLUS_DLL;

implementation

var
  Token: ULONG_PTR;

function Initialize: ULONG_PTR;
var
  Input: GdiplusStartupInput;
begin
  {$HINTS OFF}
  FillChar(Input, SizeOf(Input), 0);
  {$HINTS ON}
  Input.GdiplusVersion := 1;
  if GdiplusStartup(Result, Input, nil) <> GPSTATUS_OK then
    raise Exception.Create('GdiplusStartup failed.');
end;

{ TGdiPlusImage }

constructor TGdiPlusImage.Create(Buffer: Pointer; Size: Integer);
var
  h: HGLOBAL;
  p: Pointer;
  Stream: IStream;
  WidthAsCardinal, HeightAsCardinal: Cardinal;
begin
  inherited Create;
  if Size > 0 then
  begin
    h := GlobalAlloc(GMEM_MOVEABLE, Size);
    if h <> 0 then
    begin
      p := GlobalLock(h);
      if p <> nil then
      begin
        Move(Buffer^, p^, Size);
        GlobalUnlock(h);
        if Succeeded(CreateStreamOnHGlobal(h, TRUE, Stream)) then
        begin
          h := 0;
          Stream.SetSize(Size);
          if GdipLoadImageFromStream(Stream, FHandle) = GPSTATUS_OK then
          begin
            if (GdipGetImageWidth(FHandle, WidthAsCardinal) = GPSTATUS_OK) and (WidthAsCardinal <= MaxInt) then
              FWidth := WidthAsCardinal;
            if (GdipGetImageHeight(FHandle, HeightAsCardinal) = GPSTATUS_OK) and (HeightAsCardinal <= MaxInt) then
              FHeight := HeightAsCardinal;
          end;
        end;
      end;
      if h <> 0 then
        GlobalFree(h);
    end;
  end;
end;

destructor TGdiPlusImage.Destroy;
begin
  if FHandle <> nil then
    GdipDisposeImage(FHandle);
  inherited;
end;

procedure TGdiPlusImage.Draw(dc: HDC; x, y: Integer);
var
  g: PGpGraphics;
begin
  if Valid and (GdipCreateFromHDC(dc, g) = GPSTATUS_OK) then
  begin
    GdipDrawImageRectI(g, FHandle, x, y, FWidth, FHeight);
    GdipDeleteGraphics(g);
  end;
end;

function TGdiPlusImage.GetValid: Boolean;
begin
  Result := (FHandle <> nil) and (FWidth > 0) and (FHeight > 0);
end;

initialization

  try
    Token := Initialize;
  except on E: Exception do
    begin
      MessageBox(0, PChar(E.Message), nil, MB_OK or MB_ICONERROR);
      raise;
    end;
  end;

finalization

  GdiplusShutdown(Token);

end.
