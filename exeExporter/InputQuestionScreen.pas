{
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
}

unit InputQuestionScreen;

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, ExtCtrls, StdCtrls, TestUiUtils,
  InputQuestion, TestCore, VisualUtils, PadWidget, Windows, LCLType, QuestionScreens;

type
  TInputQuestionScreenFrame = class(TFrame, IQuestionScreen)
    edtAnswer: TEdit;
    lblAnswer: TLabel;
    pnlAnswer: TPanel;
    pnlFormulation: TPanel;
    pnlQuestion: TPanel;
    sbxQuestion: TScrollBox;
    procedure edtAnswerChange(Sender: TObject);
    procedure edtAnswerEnter(Sender: TObject);
  private
    FQuestion: TInputQuestion;
    FFormulation: TPadWidget;
    { private declarations }
  public
    destructor Destroy; override;
    procedure SetUp(Question: TQuestion);
    procedure GoReadOnly;
    function GetQuestion: TQuestion;
    { public declarations }
  end;

implementation

{$R *.lfm}

{ TInputQuestionScreenFrame }

procedure TInputQuestionScreenFrame.edtAnswerChange(Sender: TObject);
begin
  FQuestion.Response.Answer := UTF8Decode(edtAnswer.Text);
end;

procedure TInputQuestionScreenFrame.edtAnswerEnter(Sender: TObject);
begin
  edtAnswer.SelStart := TInputQuestion.GetMaxAnswerLength;
end;

destructor TInputQuestionScreenFrame.Destroy;
begin
  FreeAndNil(FFormulation);
  FreeAndNil(FQuestion);
  inherited;
end;

procedure TInputQuestionScreenFrame.SetUp(Question: TQuestion);
begin
  FQuestion := (Question as TInputQuestion).Clone as TInputQuestion;
  MakeVerticallyAutoScrollable(sbxQuestion);

  DisableAutoSizing;
  try
    FFormulation := TPadWidget.Create(nil);
    FFormulation.Border := TRUE;
    FFormulation.Align := alClient;
    FFormulation.Parent := pnlFormulation;
    FFormulation.Model.Assign(FQuestion.Formulation);

    edtAnswer.Font.Name := 'Courier New';
    edtAnswer.Font.Size := 12;
    SetEditTextWithoutOnChange(edtAnswer, UTF8Encode(FQuestion.Response.Answer));
    edtAnswer.MaxLength := TInputQuestion.GetMaxAnswerLength;
  finally
    EnableAutoSizing;
  end;
end;

procedure TInputQuestionScreenFrame.GoReadOnly;
begin
  edtAnswer.ReadOnly := TRUE;
  if edtAnswer.HandleAllocated then
    SendMessage(edtAnswer.Handle, EM_EMPTYUNDOBUFFER, 0, 0);
  edtAnswer.TabStop := FALSE;
end;

function TInputQuestionScreenFrame.GetQuestion: TQuestion;
begin
  Result := FQuestion;
end;

initialization

  QuestionScreenRegistry.Add(TInputQuestionScreenFrame, TInputQuestion);

end.
