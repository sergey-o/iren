/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';
import 'dart:js' as js;
import 'dart:typed_data';

import 'package:iren_proto/google/protobuf/any.pb.dart';
import 'package:protobuf/protobuf.dart';

final String uiDecimalMark = ((js.context.callMethod("eval", ["1.6.toLocaleString()"]) as String)
    .substring(1, 2) == ",") ? "," : ".";

final String transparentOnePixelPngUrl = Url.createObjectUrlFromBlob(Blob([Uint8List.fromList([
    0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
    0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x08, 0x06, 0x00, 0x00, 0x00, 0x1f, 0x15, 0xc4,
    0x89, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x44, 0x41, 0x54, 0x08, 0xd7, 0x63, 0x60, 0x60, 0x60, 0x60,
    0x00, 0x00, 0x00, 0x05, 0x00, 0x01, 0x5e, 0xf3, 0x2a, 0x3a, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45,
    0x4e, 0x44, 0xae, 0x42, 0x60, 0x82])], "image/png"));

void check(bool condition, [Object Function() exceptionSupplier]) {
  if (!condition) {
    throw (exceptionSupplier == null) ? "Check failed." : exceptionSupplier();
  }
}

void applyFormulaStyle(Map<String, String> style, CssStyleDeclaration target) {
  target
      ..width = "${_parseEx(style["width"])}ex"
      ..height = "${_parseEx(style["height"])}ex"
      ..verticalAlign = "${_parseEx(style["vertical-align"]) + _parseEx(style["margin-bottom"])}ex";
}

double _parseEx(/* nullable */String s) =>
    ((s != null) && s.endsWith("ex")) ? double.parse(s.substring(0, s.length - 2)) : 0.0;

Any toAny(GeneratedMessage m) => Any.pack(m, typeUrlPrefix: "");

String getTypeUrl(GeneratedMessage m) => "/${m.info_.qualifiedMessageName}";
