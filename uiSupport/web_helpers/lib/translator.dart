/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:collection';

final LinkedHashMap<String, Map<String/* namespace */, Map<String, dynamic>>> _dictionariesByLanguage = LinkedHashMap();
/* nullable */Map<String, Map<String, dynamic>> _mainDictionary;
/* nullable */Map<String, Map<String, dynamic>> _backupDictionary;

dynamic translate(String namespace, String key) {
  /* nullable */dynamic translateWith(/* nullable */Map<String, Map<String, dynamic>> dictionary) {
    Map<String, dynamic> m = (dictionary == null) ? null : dictionary[namespace];
    return (m == null) ? null : m[key];
  }

  return translateWith(_mainDictionary) ?? translateWith(_backupDictionary) ?? key;
}

void registerDictionaries(Map<String, Map<String, Map<String, dynamic>>> dictionariesByLanguage) {
  dictionariesByLanguage.forEach((language, namespaces) {
    Map<String, Map<String, dynamic>> registeredNamespaces = _dictionariesByLanguage.putIfAbsent(language, () => {});
    namespaces.forEach((namespace, translations) {
      if (registeredNamespaces.containsKey(namespace)) {
        throw "Namespace '$namespace' for language '$language' already exists.";
      }
      registeredNamespaces[namespace] = translations;
    });
  });
}

void selectDictionary(String language) {
  _mainDictionary = _dictionariesByLanguage[language];
  if (_mainDictionary == null) {
    int p = language.indexOf("-");
    if (p != -1) {
      _mainDictionary = _dictionariesByLanguage[language.substring(0, p)];
    }
  }

  _backupDictionary = _dictionariesByLanguage.values.first;
  if (identical(_backupDictionary, _mainDictionary)) {
    _backupDictionary = null;
  }
}
