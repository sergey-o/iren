/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:async/async.dart';
import 'package:fixnum/fixnum.dart';
import 'package:iren_client/iren_client.dart';
import 'package:iren_client/src/common.dart';
import 'package:iren_client/src/login_screen.dart';
import 'package:iren_client/src/main_screen.dart';
import 'package:iren_client/src/pocket_server_support.dart';
import 'package:iren_client/src/score_screen.dart';
import 'package:iren_client/src/select_work_screen.dart';
import 'package:iren_proto/login.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/student.pb.dart';
import 'package:iren_proto/student_work_selector.pb.dart';
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

class _LogScreen extends ProgramScreen {
  @override final DivElement element = DivElement();

  void log(String message) {
    element.append(ParagraphElement()
        ..text = message);
  }
}

class _OpenedWork {
  final Int64 studentChannelId;
  final Stream<StudentNotice> studentNoticeStream;
  final Future<void> studentChannelClosedNotice;
  final SavedSession savedSession;

  _OpenedWork(this.studentChannelId, this.studentNoticeStream, this.studentChannelClosedNotice, this.savedSession);
}

class _OpenWorkResult {
  final OpenWork_Reply reply;
  final /* nullable */_OpenedWork openedWork;

  _OpenWorkResult(this.reply, this.openedWork);
}

Map<String, /* nullable */String> _programArguments;

Future<void> main() async {
  if (document.readyState != "complete") {
    await window.onLoad.first;
  }

  registerClientDictionaries();
  selectDictionary(usePocketServer ? preselectedLanguage() : window.navigator.language);

  _parseProgramArguments();
  registerAreaWidgets();

  if (usePocketServer) {
    _communicateWithPocketServer();
  } else {
    document.title = tr("Iren");
    _communicateWithRegularServer();
  }
}

void _parseProgramArguments() {
  _programArguments = {};
  if (window.location.hash.startsWith("#/")) {
    for (String s in window.location.hash.substring(2).split(",")) {
      int n = s.indexOf("=");
      if (n == -1) {
        _programArguments[s] = null;
      } else {
        _programArguments[s.substring(0, n)] = s.substring(n + 1);
      }
    }
  }
}

Future<void> _communicateWithRegularServer({bool connectionLost: false}) async {
  var screen = _LogScreen();
  showScreen(screen);

  WebSocket socket = await _openSocket(screen.log, connectionLost: connectionLost);

  socket.onClose.listen((_) => _communicateWithRegularServer(connectionLost: true));
  var connection = Connection(WebSocketTransport(socket));

  Stream<ServerMessage> bootstrapMessageStream = connection.tune(Int64(
      ProtocolConstants.BOOTSTRAP_CHANNEL_ID.value));

  check((await bootstrapMessageStream.first).hasServerVersion());

  BootstrapNotice m = (await bootstrapMessageStream.first).bootstrapNotice;
  check(m.hasServerHello());
  check(m.serverHello.hasLoginChannelId());
  Int64 loginChannelId = m.serverHello.loginChannelId;
  check(m.serverHello.hasStudentWorkSelectorChannelId());
  Int64 studentWorkSelectorChannelId = m.serverHello.studentWorkSelectorChannelId;

  await _tryRestoreSavedSession(SavedSession.getIfPresent(), connection, studentWorkSelectorChannelId);

  while (true) {
    var selectWorkScreen = SelectWorkScreen(connection, studentWorkSelectorChannelId);
    showScreen(selectWorkScreen);
    String selectedWorkId = await selectWorkScreen.selectedWorkId;
    showScreen(null);

    SavedSession savedSession = SavedSession.getIfPresent();
    _OpenedWork openedWork = ((savedSession != null) && (savedSession.workId == selectedWorkId)) ?
        await _tryReopenWork(savedSession, connection, studentWorkSelectorChannelId) :
        null;
    openedWork ??= await _tryOpenWork(selectedWorkId, connection, loginChannelId, studentWorkSelectorChannelId);

    if (openedWork != null) {
      await _displayStudentWorkUi(connection, openedWork, studentWorkSelectorChannelId);
    }
  }
}

Future<void> _communicateWithPocketServer() async {
  Connection connection = createPocketServerConnection();

  BootstrapNotice m = (await connection.tune(Int64(ProtocolConstants.BOOTSTRAP_CHANNEL_ID.value)).first)
      .bootstrapNotice;
  check(m.hasServerHello());
  check(m.serverHello.hasStudentWorkSelectorChannelId());

  await _tryRestoreSavedSession(DummySavedSession(), connection, m.serverHello.studentWorkSelectorChannelId);
}

Future<void> _tryRestoreSavedSession(
    /* nullable */SavedSession savedSession,
    Connection connection,
    Int64 studentWorkSelectorChannelId) async {
  if (savedSession != null) {
    _OpenedWork openedWork = await _tryReopenWork(savedSession, connection, studentWorkSelectorChannelId);
    if (openedWork != null) {
      await _displayStudentWorkUi(connection, openedWork, studentWorkSelectorChannelId,
          showMessageIfInitiallyBanned: false);
    }
  }
}

Future</* nullable */_OpenedWork> _tryReopenWork(
    SavedSession savedSession,
    Connection connection,
    Int64 studentWorkSelectorChannelId) async {
  Int64 studentNoticeChannelId = connection.openClientChannel();

  Int64 requestId = connection.send(ClientMessage()
      ..channelId = studentWorkSelectorChannelId
      ..studentWorkSelectorRequest = (StudentWorkSelectorRequest()
          ..reopenWork = (ReopenWork()
              ..workId = savedSession.workId
              ..studentNoticeChannelId = studentNoticeChannelId
              ..sessionKey = savedSession.sessionKey)));

  ReopenWork_Reply reply = (await connection.tune(requestId).first).studentWorkSelectorReply.reopenWorkReply;

  return reply.hasOk() ?
      _OpenedWork(
          reply.ok.studentChannelId,
          connection.tune(studentNoticeChannelId).map((m) => m.studentNotice),
          connection.tune(requestId).first,
          savedSession) :
      null;
}

Future</* nullable */_OpenedWork> _tryOpenWork(
    String workId,
    Connection connection,
    Int64 loginChannelId,
    Int64 studentWorkSelectorChannelId) async {
  _OpenedWork res;

  _OpenWorkResult initialResult = await _tryOpenWorkForUser(workId, connection, studentWorkSelectorChannelId);

  if (initialResult.reply.hasUserDataRequired()) {
    var loginScreen = LoginScreen(
        initialResult.reply.userDataRequired.maxUserNameLength,
        maxGroupNameLength: initialResult.reply.userDataRequired.hasMaxGroupNameLength() ?
            initialResult.reply.userDataRequired.maxGroupNameLength :
            null);
    showScreen(loginScreen);

    await for (var _ in loginScreen.onLogIn.cast<Object>()) { // .cast works around compiler problem
      SavedSession.remove();

      var userData = OpenWork_UserData()
          ..userName = loginScreen.userName;
      if (loginScreen.groupName != null) {
        userData.groupName = loginScreen.groupName;
      }

      _OpenWorkResult result = await _tryOpenWorkForUser(workId, connection, studentWorkSelectorChannelId, userData);

      String error;
      if (result.reply.hasIncorrectUserName()) {
        error = tr("Incorrect user name.");
      } else if (result.reply.hasIncorrectUserData()) {
        error = tr("Incorrect login information.");
      } else if (result.reply.hasUserSessionAlreadyExists()) {
        error = tr("This user is already taking the selected test.");
      } else {
        res = result.openedWork;
        break;
      }

      loginScreen
          ..errorMessage = error
          ..enabled = true;
    }
  } else if (initialResult.reply.hasLoginRequired()) {
    res = (await _tryLogIn(initialResult.reply.loginRequired, connection, loginChannelId)) ?
        (await _tryOpenWorkForUser(workId, connection, studentWorkSelectorChannelId)).openedWork :
        null;
  } else {
    res = initialResult.openedWork;
  }

  return res;
}

Future<bool> _tryLogIn(
    OpenWork_Reply_LoginRequired loginRequired,
    Connection connection,
    Int64 loginChannelId) async {
  bool res = false;

  var loginScreen = LoginScreen(
      loginRequired.maxUserNameLength,
      maxPasswordLength: loginRequired.maxPasswordLength);
  showScreen(loginScreen);

  await for (var _ in loginScreen.onLogIn.cast<Object>()) { // .cast works around compiler problem
    SavedSession.remove();

    var logIn = LogIn()
        ..userName = loginScreen.userName
        ..password = loginScreen.password;
    loginScreen.clearPassword();

    LogIn_Reply reply = (await _performLoginRequest(connection, loginChannelId, LoginRequest()
        ..logIn = logIn)).logInReply;

    if (reply.hasOk()) {
      res = true;
      break;
    } else {
      check(reply.hasFailure());
      String error;
      if (reply.failure == AuthenticationFailure.INCORRECT_USER_NAME) {
        error = tr("Incorrect user name.");
      } else if (reply.failure == AuthenticationFailure.UNKNOWN_USER_OR_MISMATCHING_PASSWORD) {
        error = tr("Incorrect login information.");
      } else if (reply.failure == AuthenticationFailure.RETRY_LATER) {
        error = tr("An error has occurred, please try logging in later.");
      } else if (reply.failure == AuthenticationFailure.INTERNAL_ERROR) {
        error = tr("An error has occurred on the server.");
      } else {
        error = reply.failure.name;
      }

      loginScreen
          ..errorMessage = error
          ..enabled = true;
    }
  }

  return res;
}

Future<LoginReply> _performLoginRequest(Connection connection, Int64 channelId, LoginRequest request) async =>
    (await connection.perform(ClientMessage()
        ..channelId = channelId
        ..loginRequest = request)).loginReply;

Future<_OpenWorkResult> _tryOpenWorkForUser(
    String workId,
    Connection connection,
    Int64 studentWorkSelectorChannelId,
    [OpenWork_UserData userData]) async {
  Int64 studentNoticeChannelId = connection.openClientChannel();

  var openWork = OpenWork()
      ..workId = workId
      ..studentNoticeChannelId = studentNoticeChannelId;
  if (userData != null) {
    openWork.userData = userData;
  }

  Int64 requestId = connection.send(ClientMessage()
      ..channelId = studentWorkSelectorChannelId
      ..studentWorkSelectorRequest = (StudentWorkSelectorRequest()
          ..openWork = openWork));

  OpenWork_Reply reply = (await connection.tune(requestId).first).studentWorkSelectorReply.openWorkReply;

  _OpenedWork openedWork;
  if (reply.hasOk()) {
    var savedSession = SavedSession(
        workId: workId,
        sessionKey: reply.ok.sessionKey,
        alwaysTransient: userData == null);
    savedSession.save();

    openedWork = _OpenedWork(
        reply.ok.studentChannelId,
        connection.tune(studentNoticeChannelId).map((m) => m.studentNotice),
        connection.tune(requestId).first,
        savedSession);
  } else {
    openedWork = null;
  }

  return _OpenWorkResult(reply, openedWork);
}

Future<void> _displayStudentWorkUi(
    Connection connection,
    _OpenedWork openedWork,
    Int64 studentWorkSelectorChannelId,
    {bool showMessageIfInitiallyBanned: true}) async {
  _OpenedWork currentOpenedWork = openedWork;
  bool redisplaying = false;
  do {
    Closing_Reason closingReason = await _displayStudentWorkUiOnce(
        connection,
        currentOpenedWork,
        showMessageIfInitiallyBanned || redisplaying);
    currentOpenedWork = (closingReason == Closing_Reason.RESUMED) ?
        await _tryReopenWork(openedWork.savedSession, connection, studentWorkSelectorChannelId) :
        null;
    redisplaying = true;
  } while (currentOpenedWork != null);
}

Future</* nullable */Closing_Reason> _displayStudentWorkUiOnce(
    Connection connection,
    _OpenedWork openedWork,
    bool showMessageIfInitiallyBanned) async {
  bool showMessageIfBanned = showMessageIfInitiallyBanned;
  Closing closingNotice = null;

  void switchScreenIfNeeded(StudentNotice m) {
    if (m.hasMainScreenTurnOn()) {
      openedWork.savedSession.save();
      showMessageIfBanned = true;
      showScreen(MainScreen(m.mainScreenTurnOn, connection, openedWork.studentChannelId,
          openedWork.studentNoticeStream));
    } else if (m.hasShowScoreScreen()) {
      openedWork.savedSession.save(transient: true);
      showMessageIfBanned = true;
      showScreen(ScoreScreen(m.showScoreScreen, connection, openedWork.studentChannelId,
          openedWork.studentNoticeStream));
    } else if (m.hasClosing()) {
      closingNotice = m.closing;
    }
  }

  StreamSubscription<StudentNotice> subscription = openedWork.studentNoticeStream.listen(switchScreenIfNeeded);
  try {
    await openedWork.studentChannelClosedNotice;
  } finally {
    subscription.cancel();
  }

  showScreen(null);
  Closing_Reason res = closingNotice?.reason;

  if (res == Closing_Reason.BANNED) {
    openedWork.savedSession.save(transient: true);
    if (showMessageIfBanned) {
      window.alert(tr("Your access to this test has been restricted."));
    }
  }

  return res;
}

Future<WebSocket> _openSocket(void Function(String message) log, {bool connectionLost: false}) async {
  const Duration RECONNECT_DELAY = Duration(seconds: 3);
  const Duration CONNECTING_MESSAGE_DELAY = Duration(seconds: 3);

  if (connectionLost) {
    log(tr("The connection to the server has been lost."));
    await Future<void>.delayed(RECONNECT_DELAY);
  }

  var showConnectingMessage = Timer(connectionLost ? Duration.zero : CONNECTING_MESSAGE_DELAY,
      () => log(tr("Connecting...")));

  WebSocket res;
  do {
    res = WebSocket(_webSocketUri())
        ..binaryType = "arraybuffer";
    await StreamGroup.merge([res.onOpen, res.onClose]).first;
    if (res.readyState != WebSocket.OPEN) {
      res = null;
      await Future<void>.delayed(RECONNECT_DELAY);
    }
  } while (res == null);

  showConnectingMessage.cancel();
  return res;
}

String _webSocketUri() {
  String protocol = (window.location.protocol == "https:") ? "wss" : "ws";
  String customPort = _programArguments["port"];
  String host = (customPort == null) ? window.location.host : "${window.location.hostname}:$customPort";
  return "$protocol://$host${window.location.pathname}websocket/";
}
