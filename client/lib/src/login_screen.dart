/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'common.dart';

class LoginScreen extends ProgramScreen {
  @override final DivElement element = DivElement();

  final StreamController<void> _onLogIn = StreamController.broadcast();
  Stream<void> get onLogIn => _onLogIn.stream;

  final TextInputElement _userName = TextInputElement();
  String get userName => _userName.value;

  /* nullable */TextInputElement _groupName;
  /* nullable */String get groupName => _groupName?.value;

  /* nullable */PasswordInputElement _password;
  /* nullable */String get password => _password?.value;

  final ButtonElement _logInButton = ButtonElement();
  final ButtonElement _cancelButton = ButtonElement();
  final SpanElement _logInFailedLabel = SpanElement();

  LoginScreen(int maxUserNameLength, {int maxGroupNameLength, int maxPasswordLength}) {
    _userName.id = "ru-irenproject-loginScreen-userName";

    var form = FormElement()
        ..classes.add("ru-irenproject-loginScreen-form")
        ..append(SpanElement()
            ..classes.add("ru-irenproject-loginScreen-label")
            ..append(LabelElement()
                ..text = "${tr("User name:")}\u00a0"
                ..htmlFor = _userName.id))
        ..append(_userName
            ..classes.add("ru-irenproject-loginScreen-inputField")
            ..maxLength = maxUserNameLength
            ..onInput.listen((_) => _clearErrorMessage()))
        ..append(BRElement());

    if (maxGroupNameLength != null) {
      _groupName = TextInputElement()
          ..id = "ru-irenproject-loginScreen-groupName"
          ..classes.add("ru-irenproject-loginScreen-inputField")
          ..maxLength = maxGroupNameLength
          ..onInput.listen((_) => _clearErrorMessage());
      form
          ..append(SpanElement()
              ..classes.add("ru-irenproject-loginScreen-label")
              ..append(LabelElement()
                  ..text = "${tr("Group:")}\u00a0"
                  ..htmlFor = _groupName.id))
          ..append(_groupName)
          ..append(BRElement());
    }

    if (maxPasswordLength != null) {
      _password = PasswordInputElement()
          ..id = "ru-irenproject-loginScreen-password"
          ..classes.add("ru-irenproject-loginScreen-inputField")
          ..maxLength = maxPasswordLength
          ..onInput.listen((_) => _clearErrorMessage());
      form
          ..append(SpanElement()
              ..classes.add("ru-irenproject-loginScreen-label")
              ..append(LabelElement()
                  ..text = "${tr("Password:")}\u00a0"
                  ..htmlFor = _password.id))
          ..append(_password)
          ..append(BRElement());
    }

    form
        ..append(SpanElement()
            ..classes.add("ru-irenproject-loginScreen-label"))
        ..append(_logInButton
            ..text = tr("Log In")
            ..onClick.listen(_onLogInClick))
        ..append(_cancelButton
            ..classes.add("ru-irenproject-loginScreen-cancelButton")
            ..type = "button"
            ..text = tr("Cancel")
            ..onClick.listen((_) => _onCancelClick()))
        ..append(BRElement())
        ..append(SpanElement()
            ..classes.add("ru-irenproject-loginScreen-label"))
        ..append(_logInFailedLabel
            ..classes.add("ru-irenproject-loginScreen-logInFailedLabel"));

    element
        ..classes.add("ru-irenproject-loginScreen")
        ..append(form);
  }

  @override void onShow() {
    _userName.focus();
  }

  void _onLogInClick(MouseEvent e) {
    e.preventDefault();

    this.enabled = false;
    _clearErrorMessage();
    _onLogIn.add(null);
  }

  void _onCancelClick() {
    this.enabled = false;
    _onLogIn.close();
  }

  set enabled(bool value) {
    _userName.disabled = !value;
    _groupName?.disabled = !value;
    _password?.disabled = !value;
    _logInButton.disabled = !value;
    _cancelButton.disabled = !value;
  }

  set errorMessage(String value) {
    _logInFailedLabel.text = value;
  }

  void _clearErrorMessage() {
    _logInFailedLabel.text = "";
  }

  void clearPassword() {
    _password?.value = "";
  }
}
