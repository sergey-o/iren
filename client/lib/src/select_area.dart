/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';

import 'package:iren_proto/area/select_area.pb.dart';

import 'common.dart';

class SelectAreaWidget extends AreaWidget {
  final SelectArea _area;
  @override final SelectResponse response;

  @override final FormElement element = FormElement();
  final List<InputElement> _checkboxes = [];
  final List<DivElement> _choices = [];

  SelectAreaWidget(this._area, this.response) {
    element
        ..classes.add("ru-irenproject-selectArea")
        ..onSubmit.listen((e) => e.preventDefault());

    _area.choice.asMap().forEach((i, choice) {
      var choiceDiv = DivElement()
          ..classes.add("ru-irenproject-selectArea-choice");

      InputElement checkbox = (_area.multipleSelection ?
          CheckboxInputElement() : RadioButtonInputElement()) as InputElement;
      _checkboxes.add(checkbox);
      if (!_area.multipleSelection) {
        checkbox.name = "ru-irenproject-selectArea-switch";
      }
      checkbox
          ..classes.add("ru-irenproject-selectArea-checkbox")
          ..checked = response.selected[i]
          ..onClick.listen((_) => _updateResponse());
      choiceDiv.append(checkbox);

      DivElement flow = renderFlow(choice)
          ..classes.add("ru-irenproject-selectArea-choice-flow")
          ..onClick.listen((_) => _onChoiceClick(i));
      _choices.add(flow);
      choiceDiv.append(flow);

      element.append(choiceDiv);
    });
  }

  void _onChoiceClick(int choiceIndex) {
    if (!isReadOnly) {
      InputElement cb = _checkboxes[choiceIndex];
      cb.checked = _area.multipleSelection ? !cb.checked : true;
      _updateResponse();
    }
  }

  void _updateResponse() {
    bool changed = false;

    _checkboxes.asMap().forEach((i, cb) {
      bool c = cb.checked;
      if (response.selected[i] != c) {
        changed = true;
        response.selected[i] = c;
      }
    });

    if (changed) {
      triggerResponse();
    }
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    for (var cb in _checkboxes) {
      cb.disabled = readOnly;
    }
  }
}
