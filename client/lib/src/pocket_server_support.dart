/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

@JS()
library iren_client.pocket_server_support;

import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'dart:math' as math;

import 'package:iren_proto/google/protobuf/any.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:js/js.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_helpers/web_helpers.dart';

import 'common.dart';

class _MessagePortTransport extends Transport {
  final MessagePort _port;

  @override final Stream<CloseEvent> onClose = StreamController<CloseEvent>.broadcast().stream;

  _MessagePortTransport(this._port) {
    _port.onMessage.listen(_onPortMessage);
  }

  void _onPortMessage(MessageEvent e) {
    messageStreamController.add(_mergeFromJspbString(e.data as String, ServerMessage()));
  }

  @override void send(ClientMessage message) {
    _port.postMessage(_writeToJspbString(message));
  }

  @override bool get connected => true;
}

@JS("irenClientPort")
external /* nullable */MessagePort get _getMessagePort;

final /* nullable */MessagePort _messagePort = _getMessagePort;

bool get usePocketServer => _messagePort != null;

@JS("irenLanguage")
external /* nullable */String get _getPreselectedLanguage;

final /* nullable */String _preselectedLanguage = _getPreselectedLanguage;

String preselectedLanguage() {
  check(_preselectedLanguage != null);
  return _preselectedLanguage;
}

Connection createPocketServerConnection() {
  check(usePocketServer);
  return Connection(_MessagePortTransport(_messagePort));
}

T _mergeFromJspbString<T extends GeneratedMessage>(String s, T out) => out
    ..mergeFromJsonMap(_jspbToJsonMap(jsonDecode(s) as List<dynamic>, out.info_));

Map<String, dynamic> _jspbToJsonMap(List<dynamic> jspb, BuilderInfo messageType) {
  Map<String, dynamic> res = {};

  jspb.asMap().forEach((i, value) {
    if (value != null) {
      if (value is Map) {
        throw "Extension object is not supported (found in ${messageType.qualifiedMessageName}).";
      }

      int tag = i + 1;
      FieldInfo fieldInfo = messageType.fieldInfo[tag];
      if (fieldInfo == null) {
        throw "Unknown tag $tag in ${messageType.qualifiedMessageName}.";
      }

      dynamic out;
      if (fieldInfo.isGroupOrMessage) {
        BuilderInfo fieldType = fieldInfo.isMapField ?
            (fieldInfo as MapFieldInfo).mapEntryBuilderInfo :
            fieldInfo.subBuilder().info_;
        out = (fieldInfo.isRepeated || fieldInfo.isMapField) ?
            (value as List<dynamic>)
                .map((item) => _jspbToJsonMap(item as List<dynamic>, fieldType))
                .toList() :
            _jspbToJsonMap(value as List<dynamic>, fieldType);
      } else {
        out = value;
      }

      res["$tag"] = out;
    }
  });

  return res;
}

String _writeToJspbString(GeneratedMessage m) => jsonEncode(_jsonMapToJspb(m.writeToJsonMap(), m.info_));

List<dynamic> _jsonMapToJspb(Map<String, dynamic> jsonMap, BuilderInfo messageType) {
  List<dynamic> res = [];

  jsonMap.forEach((tagString, value) {
    int tag = int.parse(tagString);
    FieldInfo fieldInfo = messageType.fieldInfo[tag];

    dynamic out;
    if (fieldInfo.isGroupOrMessage) {
      BuilderInfo fieldType = fieldInfo.isMapField ?
          (fieldInfo as MapFieldInfo).mapEntryBuilderInfo :
          fieldInfo.subBuilder().info_;
      out = (fieldInfo.isRepeated || fieldInfo.isMapField) ?
          (value as List<dynamic>)
              .map((item) => _jsonMapToJspb(item as Map<String, dynamic>, fieldType))
              .toList() :
          _jsonMapToJspb(value as Map<String, dynamic>, fieldType);
    } else {
      out = value;
    }

    res.length = math.max(res.length, tag);
    res[tag - 1] = out;
  });

  return res;
}

T unpackCustomAny<T extends GeneratedMessage>(Any any, T out) => usePocketServer ?
    _mergeFromJspbString(utf8.decode(any.value), out) :
    any.unpackInto(out);

Any toCustomAny(GeneratedMessage m) => usePocketServer ?
    (Any()
        ..typeUrl = getTypeUrl(m)
        ..value = utf8.encode(_writeToJspbString(m))) :
    toAny(m);
