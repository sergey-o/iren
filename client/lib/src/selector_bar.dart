/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:iren_proto/misc_types.pb.dart';
import 'package:web_helpers/web_helpers.dart';

import 'common.dart';

class SelectorBar {
  final Element element = DivElement();
  final DivElement _label = DivElement();
  final SpanElement _currentItemLabel = SpanElement();
  final DivElement _diagram = DivElement();

  final int _itemCount;
  /* nullable */int _currentItem;
  bool _selectionEnabled = true;

  final StreamController<SelectItemEvent> _onSelectItem = StreamController.broadcast(sync: true);
  Stream<SelectItemEvent> get onSelectItem => _onSelectItem.stream;

  SelectorBar(this._itemCount) {
    check(_itemCount >= 1);

    for (int i = 0; i < _itemCount; ++i) {
      _diagram.append(DivElement()
          ..classes.add("ru-irenproject-selectorBar-diagram-item")
          ..onClick.listen((_) => _onItemClick(i)));
    }

    element
        ..classes.add("ru-irenproject-selectorBar")
        ..append(_label
            ..classes.add("ru-irenproject-selectorBar-label")
            ..style.visibility = "hidden"
            ..appendText("${tr("Question")} ")
            ..append(_currentItemLabel
                ..classes.add("ru-irenproject-selectorBar-label-currentItem"))
            ..appendText(" ${trD("ITEM_COUNT")(_itemCount)}"))
        ..append(_diagram
            ..classes.add("ru-irenproject-selectorBar-diagram"));
  }

  void layOut() {
    String s = _currentItemLabel.text;
    _currentItemLabel.text = "$_itemCount";
    _label.style
        ..width = ""
        ..width = "${_label.offsetWidth}px";
    _currentItemLabel.text = s;
  }

  void setCurrentItem(int value) {
    _checkItemIndex(value);

    const String CLASS_NAME = "ru-irenproject-selectorBar-diagram-currentItem";
    if (_currentItem != null) {
      _diagram.children[_currentItem].classes.remove(CLASS_NAME);
    }

    _currentItem = value;
    _diagram.children[_currentItem].classes.add(CLASS_NAME);

    _currentItemLabel.text = "${_currentItem+1}";
    _label.style.visibility = "";
  }

  void _checkItemIndex(int itemIndex) {
    check(itemIndex >= 0);
    check(itemIndex < _itemCount);
  }

  void setItemColor(int itemIndex, String color) {
    _checkItemIndex(itemIndex);
    _diagram.children[itemIndex].style.backgroundColor = color;
  }

  void setItemWeight(int itemIndex, int weight) {
    _checkItemIndex(itemIndex);
    _diagram.children[itemIndex].style.flexGrow = "$weight";
  }

  void _onItemClick(int itemIndex) {
    if (_selectionEnabled && (itemIndex != _currentItem)) {
      _onSelectItem.add(SelectItemEvent(itemIndex));
    }
  }

  bool get selectionEnabled => _selectionEnabled;

  set selectionEnabled(bool value) {
    _selectionEnabled = value;
    _diagram.classes.toggle("ru-irenproject-selectorBar-diagram-disabled", !_selectionEnabled);
  }
}

class SelectItemEvent {
  final int itemIndex;
  SelectItemEvent(this.itemIndex);
}

const Map<QuestionStatus, String> QUESTION_COLORS = {
    QuestionStatus.UNANSWERED: "#808080",
    QuestionStatus.ANSWERED: "#0066ff",
    QuestionStatus.CORRECT: "#00e000",
    QuestionStatus.INCORRECT: "#e00000",
    QuestionStatus.PARTIALLY_CORRECT: "#ffcc00"};

void setUpSelectorBar(List<QuestionDescriptor> questionDescriptors, SelectorBar selectorBar) {
  questionDescriptors.asMap().forEach((i, q) {
    selectorBar
        ..setItemWeight(i, q.weight)
        ..setItemColor(i, QUESTION_COLORS[q.status]);
  });
}
