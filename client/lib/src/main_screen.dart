/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:fixnum/fixnum.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/student.pb.dart';

import 'common.dart';
import 'pocket_server_support.dart';
import 'selector_bar.dart';

class MainScreen extends ProgramScreen {
  static const int _MILLISECONDS_RESERVED = 5000;

  final int _questionCount;
  /* nullable */int _currentQuestion;

  final Connection _connection;
  final Int64 _studentChannelId;
  StreamSubscription<StudentNotice> _noticeSubscription;
  /* nullable */StreamSubscription<Event> _windowBeforeUnloadSubscription;

  @override final DivElement element = DivElement();
  final DivElement _dialogPanel = DivElement();

  final ButtonElement _submitButton = ButtonElement();
  final ButtonElement _previousButton = ButtonElement();
  final ButtonElement _nextButton = ButtonElement();
  final ButtonElement _finishWorkButton = ButtonElement();

  final SpanElement _currentResultCaption = SpanElement();
  final SpanElement _currentResult = SpanElement();

  final DivElement _timePanel = DivElement();
  final DivElement _timeLabel = DivElement();
  final DivElement _timeProgress = DivElement();

  final SelectorBar _selectorBar;
  /* nullable */DialogWidget _dialog;

  /* nullable */Timer _setResponseTimer;
  /* nullable */bool _responseChanged;

  /* nullable */bool _waitingForReply;
  /* nullable */bool _setResponseEnqueued;
  /* nullable */bool _submitEnqueued;
  /* nullable */int _questionToChoose;

  bool _finishWorkEnqueued = false;
  bool _finishingWork = false;

  final /* nullable */num _durationMilliseconds;
  /* nullable */ShowRemainingTime _timing;
  /* nullable */num _timingArrivalMilliseconds;
  /* nullable */Timer _clockTimer;

  MainScreen(MainScreenTurnOn message, this._connection, this._studentChannelId, Stream<StudentNotice> noticeStream)
      : _questionCount = message.questionDescriptor.length,
        _selectorBar = SelectorBar(message.questionDescriptor.length),
        _durationMilliseconds = message.hasDurationMilliseconds() ? message.durationMilliseconds : null {
    setUpSelectorBar(message.questionDescriptor, _selectorBar);
    _selectorBar.onSelectItem.listen((e) => _chooseQuestion(e.itemIndex));

    element
        ..classes.add("ru-irenproject-mainScreen")
        ..append(DivElement()
            ..classes.add("ru-irenproject-mainScreen-topPanel")
            ..append(_selectorBar.element))
        ..append(_dialogPanel
            ..classes.addAll(["ru-irenproject-mainScreen-dialogPanel", "dnd-scrollable"])
            ..onKeyDown.listen(_onDialogPanelKeyDown))
        ..append(DivElement()
            ..classes.add("ru-irenproject-mainScreen-bottomPanel")
            ..append(_submitButton
                ..classes.add("ru-irenproject-mainScreen-submitButton")
                ..text = tr("Submit")
                ..disabled = true
                ..onClick.listen((_) => _submit()))
            ..append(_previousButton
                ..classes.add("ru-irenproject-mainScreen-previousButton")
                ..text = tr("Previous")
                ..onClick.listen((_) => _chooseQuestion(_currentQuestion - 1)))
            ..append(_nextButton
                ..classes.add("ru-irenproject-mainScreen-nextButton")
                ..text = tr("Next")
                ..onClick.listen((_) => _chooseQuestion(_currentQuestion + 1)))
            ..append(_currentResultCaption
                ..classes.add("ru-irenproject-mainScreen-currentResultCaption")
                ..text = "${tr("CURRENT_RESULT")}\u00a0"
                ..style.display = "none")
            ..append(_currentResult
                ..classes.add("ru-irenproject-mainScreen-currentResult")
                ..style.display = "none")
            ..append(_timePanel
                ..classes.add("ru-irenproject-mainScreen-timePanel")
                ..title = tr("Time remaining")
                ..style.display = "none"
                ..append(DivElement()
                    ..classes.add("ru-irenproject-mainScreen-timeIcon"))
                ..append(DivElement()
                    ..classes.add("ru-irenproject-mainScreen-timeBox")
                    ..append(_timeProgress
                        ..classes.add("ru-irenproject-mainScreen-timeProgress"))
                    ..append(_timeLabel
                        ..classes.add("ru-irenproject-mainScreen-timeLabel"))))
            ..append(_finishWorkButton
                ..classes.add("ru-irenproject-mainScreen-finishWorkButton")
                ..text = tr("Finish Work")
                ..disabled = true
                ..onClick.listen((_) => _onFinishWorkClick())));

    _setNavigationEnabled(false);

    _noticeSubscription = noticeStream.listen(_onNotice);

    if (usePocketServer) {
      _windowBeforeUnloadSubscription = window.onBeforeUnload.listen(
          (e) => _onWindowBeforeUnload(e as BeforeUnloadEvent));
    }
  }

  @override void onDismiss() {
    _clearSetResponseTimer();
    _clearClockTimer();
    _noticeSubscription?.cancel();
    _noticeSubscription = null;
    _windowBeforeUnloadSubscription?.cancel();
    _windowBeforeUnloadSubscription = null;
  }

  @override void onShow() {
    _selectorBar.layOut();
  }

  @override void onResize() {
    _dialog?.layOut();
  }

  void _onWindowBeforeUnload(BeforeUnloadEvent e) {
    e
        ..preventDefault()
        ..returnValue = "";
  }

  void _onNotice(StudentNotice m) {
    if (m.hasShowDialog()) {
      _showDialog(m.showDialog);
    } else if (m.hasShowRemainingTime()) {
      _showRemainingTime(m.showRemainingTime);
    } else if (m.hasShowCurrentResult()) {
      _showCurrentResult(m.showCurrentResult);
    } else if (m.hasSetResponseOk()) {
      _handleSetResponseOk();
    } else if (m.hasSubmitOk()) {
      _handleSubmitOk(m.submitOk);
    } else if (m.hasShowQuestionStatus()) {
      _showQuestionStatus(m.showQuestionStatus);
    }
  }

  void _showDialog(ShowDialog m) {
    _clearSetResponseTimer();
    _responseChanged = false;

    _waitingForReply = false;
    _setResponseEnqueued = false;
    _submitEnqueued = false;
    _questionToChoose = null;

    if (_finishWorkEnqueued) {
      _processQueue();
    } else {
      _dialog = DialogWidget(m.dialog, m.response);
      _dialog
          ..setReadOnly(m.readOnly)
          ..onResponse.listen(_onDialogResponse);

      _dialogPanel.children = [_dialog.element];
      _dialog.layOut();
      _dialogPanel.scrollTop = 0;

      _currentQuestion = m.questionIndex;
      _selectorBar.setCurrentItem(_currentQuestion);

      if (!m.readOnly && ![_previousButton, _nextButton].contains(document.activeElement)) {
        _dialog.setFocus();
      }

      _submitButton.disabled = !m.canSubmit;
      _setNavigationEnabled(!m.canSubmit);

      _finishWorkButton.disabled = false;
    }
  }

  void _handleSetResponseOk() {
    _waitingForReply = false;
    _processQueue();
  }

  void _handleSubmitOk(SubmitOk m) {
    _waitingForReply = false;

    if (!_finishingWork) {
      _dialog.setReadOnly(m.makeReadOnly);
      _setNavigationEnabled(true);
    }

    _processQueue();
  }

  void _showQuestionStatus(ShowQuestionStatus m) {
    _selectorBar.setItemColor(m.questionIndex, QUESTION_COLORS[m.status]);
  }

  void _clearSetResponseTimer() {
    _setResponseTimer?.cancel();
    _setResponseTimer = null;
  }

  void _processQueue() {
    _processSetResponse();
    _processFinishWork();
    _processSubmit();
    _processChooseQuestion();
  }

  void _processSetResponse() {
    if (!_waitingForReply && _setResponseEnqueued) {
      _send(StudentRequest()
          ..setResponse = (SetResponse()
              ..response = _dialog.response));
      _waitingForReply = true;

      _responseChanged = false;
      _setResponseEnqueued = false;
    }
  }

  void _processFinishWork() {
    if (!_waitingForReply && _finishWorkEnqueued) {
      _send(StudentRequest()
          ..finishWork = FinishWork());
      _waitingForReply = true;

      _finishWorkEnqueued = false;
    }
  }

  void _processSubmit() {
    if (!_waitingForReply && _submitEnqueued) {
      _send(StudentRequest()
          ..submit = Submit());
      _waitingForReply = true;

      _submitEnqueued = false;
    }
  }

  void _processChooseQuestion() {
    if (!_waitingForReply && (_questionToChoose != null)) {
      _send(StudentRequest()
          ..chooseQuestion = (ChooseQuestion()
              ..questionIndex = _questionToChoose));
      _waitingForReply = true;

      _questionToChoose = null;
    }
  }

  Int64 _send(StudentRequest request) => _connection.send(ClientMessage()
      ..channelId = _studentChannelId
      ..studentRequest = request);

  void _onDialogResponse(DialogEvent e) {
    if (identical(e.source, _dialog)) {
      _responseChanged = true;
      _submitButton.disabled = false;
      if ((document.activeElement == null) || !_dialogPanel.contains(document.activeElement)) {
        _submitButton.focus();
      }
      _setNavigationEnabled(false);

      _clearSetResponseTimer();
      _setResponseTimer = Timer(const Duration(seconds: 1), () {
        _clearSetResponseTimer();
        _setResponseEnqueued = true;
        _processQueue();
      });
    }
  }

  void _submit() {
    _submitButton.disabled = true;
    _dialog.setReadOnly(true);

    _enqueueSetResponseIfChanged();

    _submitEnqueued = true;
    _processQueue();
  }

  void _enqueueSetResponseIfChanged() {
    _clearSetResponseTimer();
    if (_responseChanged) {
      _setResponseEnqueued = true;
    }
  }

  void _onFinishWorkClick() {
    YesNoDialog(tr("Do you want to finish the test?"), tr("Finish"), tr("Not Now"),
        "ru-irenproject-finishWorkDialog", _finishWork);
  }

  void _finishWork() {
    if (!_finishingWork) {
      _finishingWork = true;

      _finishWorkButton.disabled = true;
      _submitButton.disabled = true;
      _dialog.setReadOnly(true);
      _setNavigationEnabled(false);

      _enqueueSetResponseIfChanged();

      _finishWorkEnqueued = true;
      _processQueue();
    }
  }

  void _chooseQuestion(int questionIndex) {
    _dialog.setReadOnly(true);

    _questionToChoose = questionIndex;
    _processQueue();
  }

  void _setNavigationEnabled(bool enabled) {
    _selectorBar.selectionEnabled = enabled;

    _previousButton.disabled = !(enabled && (_currentQuestion > 0));
    _nextButton.disabled = !(enabled && (_currentQuestion < _questionCount - 1));
  }

  void _showRemainingTime(ShowRemainingTime m) {
    bool limited = m.hasMillisecondsRemaining();

    _timing = limited ? m : null;
    _timingArrivalMilliseconds = limited ? window.performance.now() : null;

    _timePanel.style.display = limited ? "" : "none";
    if (limited) {
      _updateTimePanel(_millisecondsRemaining());
    }

    _clearClockTimer();
    if (limited) {
      _clockTimer = Timer.periodic(const Duration(seconds: 1), (_) {
        num remaining = _millisecondsRemaining();
        _updateTimePanel(remaining);
        if (remaining == 0) {
          _finishWork();
        }
      });
    }
  }

  num _millisecondsRemaining() => math.max(
      _timing.millisecondsRemaining
          - (window.performance.now() - _timingArrivalMilliseconds)
          - _MILLISECONDS_RESERVED, 0);

  void _updateTimePanel(num millisecondsRemaining) {
    _timeLabel.text = formatRemainingTime(millisecondsRemaining, showLastSeconds: true);

    num partRemaining;
    if (_durationMilliseconds == null) {
      partRemaining = 0;
    } else {
      num millisecondsTotal = math.max(_durationMilliseconds - _MILLISECONDS_RESERVED, 0);
      partRemaining = (millisecondsRemaining < millisecondsTotal) ? millisecondsRemaining / millisecondsTotal : 1;
    }
    _timeProgress.style.right = "${(1 - partRemaining) * 100}%";
  }

  void _clearClockTimer() {
    _clockTimer?.cancel();
    _clockTimer = null;
  }

  void _onDialogPanelKeyDown(KeyboardEvent e) {
    if (!keyboardInputBlocked() && (e.keyCode == KeyCode.ENTER) && !_submitButton.disabled) {
      e
          ..preventDefault()
          ..stopPropagation();
      _submit();
    }
  }

  void _showCurrentResult(ShowCurrentResult m) {
    _currentResultCaption.style.display = _currentResult.style.display = "";
    _currentResult.text = "${scaledDecimalToIntegerPercent(m.scaledSessionResult)}%";
  }
}
