/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';

import 'package:fixnum/fixnum.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/student_work_selector.pb.dart';

import 'common.dart';

class SelectWorkScreen extends ProgramScreen {
  final Connection _connection;
  final Int64 _channelId;

  final Completer<String> _selectedWorkId = Completer();
  Future<String> get selectedWorkId => _selectedWorkId.future;

  @override final DivElement element = DivElement();
  final DivElement _workList = DivElement();

  StreamSubscription<ServerMessage> _workListSubscription;

  SelectWorkScreen(this._connection, this._channelId) {
    element
        ..classes.add("ru-irenproject-selectWorkScreen")
        ..append(_workList
            ..classes.add("ru-irenproject-selectWorkScreen-workList"));

    Int64 noticeChannelId = _send(StudentWorkSelectorRequest()
        ..watchWorkList = WatchWorkList());
    _workListSubscription = _connection.tune(noticeChannelId)
        .listen((m) => _display(m.studentWorkSelectorNotice.workList));
  }

  @override void onDismiss() {
    if (_workListSubscription != null) {
      _workListSubscription.cancel();
      _workListSubscription = null;
      _send(StudentWorkSelectorRequest()
          ..unwatchWorkList = UnwatchWorkList());
    }
  }

  Int64 _send(StudentWorkSelectorRequest request) => _connection.send(ClientMessage()
      ..channelId = _channelId
      ..studentWorkSelectorRequest = request);

  void _display(WorkList list) {
    _workList
        ..nodes.clear()
        ..dataset["placeholder"] = tr("No tests are available.");

    for (var item in list.item) {
      _workList.append(makeKeyboardAccessible(DivElement(), trackSpace: false)
          ..text = item.title
          ..onClick.listen((e) {
            e.preventDefault();
            if (!_selectedWorkId.isCompleted) {
              _selectedWorkId.complete(item.id);
            }
          }));
    }
  }
}
