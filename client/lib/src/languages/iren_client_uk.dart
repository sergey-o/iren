/*
  © Translation contributors, see the TRANSLATORS file

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

final Map<String, Map<String, dynamic>> uk = {
  "iren_client": {
    "CURRENT_RESULT": "Вірно:",
    "QUESTION_RESULT": "Оцінка відповіді:",
    "QUESTION_SCORE": "Набрані бали:",
    "POINTS": (String earned, int total) => "$earned з $total",
    "ITEM_COUNT": (int itemCount) => "з $itemCount",
    "Iren": "Айрен",
    "User name:": "Користувач:",
    //TR "Group:": "Группа:",
    //TR "Password:": "Пароль:",
    "Log In": "Увійти",
    //TR "Cancel": "Отмена",
    "Incorrect user name.": "Неправильне ім’я користувача.",
    //TR "Incorrect login information.": "Неверные данные для входа.",
    //TR "An error has occurred, please try logging in later.": "Произошла ошибка, попробуйте зайти позже.",
    //TR "An error has occurred on the server.": "Произошла ошибка на сервере.",
    //TR "This user is already taking the selected test.": "Этот пользователь уже выполняет выбранную работу.",
    "Submit": "Відповісти",
    "Answer:": "Відповідь:",
    "The connection to the server has been lost.": "Зв’язок з сервером перервано.",
    "Connecting...": "З’єднання...",
    "Finish Work": "Завершити роботу",
    "Do you want to finish the test?": "Завершити роботу?",
    "Finish": "Завершити",
    "Not Now": "Не зараз",
    "The work is finished.": "Роботу завершено.",
    "Overall Results": "Загальний підсумок",
    "Result:": "Підсумок:",
    "Grade:": "Оцінка:",
    "Questions offered:": "Запропоновано питань:",
    "Points earned:": "Набрано балів:",
    "Questions": "Питання",
    "Previous": "Назад",
    "Next": "Вперед",
    "Your Answer": "Ваша відповідь",
    "Correct Answer": "Правильна відповідь",
    "Question": "Питання",
    "Question weight:": "Вага відповіді:",
    "Section:": "Розділ:",
    "Show section:": "Показати розділ:",
    "(All)": "(Все)",
    "Sections": "Розділи",
    "Section": "Розділ",
    "Result, %": "Підсумок, %",
    "Points Earned": "Набрано балів",
    "Points Total": "Всього балів",
    "Questions Total": "Всього питань",
    //TR "Close": "Закрыть",
    //TR "Start again": "Начать заново",
    "Time remaining": "Залишилось часу",
    "No tests are available.": "Немає доступних робіт.",
    //TR "Your access to this test has been restricted.": "Доступ к работе запрещен.",
    //TR "Created with Iren": "Тест создан в программе Айрен",
    //TR "Third-party libraries": "Используемые библиотеки",
  }
};
