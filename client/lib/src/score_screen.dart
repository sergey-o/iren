/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:html';
import 'dart:math' as math;

import 'package:fixnum/fixnum.dart';
import 'package:iren_proto/dialog.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:iren_proto/student.pb.dart';
import 'package:meta/meta.dart';

import 'common.dart';
import 'pocket_server_support.dart';
import 'selector_bar.dart';

class ScoreScreen extends ProgramScreen {
  static const String _PAGE_HEADER = "ru-irenproject-scoreScreen-pageHeader";
  static const String _SELECTED = "ru-irenproject-scoreScreen-pageHeader-selected";

  @override final DivElement element = DivElement();
  /* nullable */DivElement _questionPageHeader;
  final DivElement _pageContainer = DivElement();
  final DivElement _creditsPanel = _createCreditsPanel();

  /* nullable */ShowScore _showScore;
  final Connection _connection;
  final Int64 _studentChannelId;
  final Stream<StudentNotice> _noticeStream;

  /* nullable */OverallScorePage _overallPage;
  /* nullable */QuestionScorePage _questionPage;
  /* nullable */SectionScorePage _sectionPage;

  ScoreScreen(ShowScoreScreen message, this._connection, this._studentChannelId, this._noticeStream) {
    element.classes.add("ru-irenproject-scoreScreen");

    if (message.hasShowScore()) {
      _showScore = message.showScore;

      var pageSelector = DivElement()
          ..classes.add("ru-irenproject-scoreScreen-pageSelector");
      element.append(pageSelector);

      pageSelector.append(makeKeyboardAccessible(DivElement())
          ..classes.addAll([_PAGE_HEADER, _SELECTED])
          ..text = tr("Overall Results")
          ..onClick.listen(_onOverallScoreClick));

      if (_showScore.hasQuestionScore()) {
        _questionPageHeader = DivElement();
        pageSelector.append(makeKeyboardAccessible(_questionPageHeader)
            ..classes.add(_PAGE_HEADER)
            ..text = tr("Questions")
            ..onClick.listen((_) => _onQuestionScoreClick()));
      }

      if (_showScore.hasSectionScore() && (_showScore.sectionScore.sectionOutcome.length > 1)) {
        pageSelector.append(makeKeyboardAccessible(DivElement())
            ..classes.add(_PAGE_HEADER)
            ..text = tr("Sections")
            ..onClick.listen(_onSectionScoreClick));
      }

      pageSelector.append(_createCloseButton());

      element.append(_pageContainer
          ..classes.add("ru-irenproject-scoreScreen-pageContainer"));

      _overallPage = OverallScorePage(_showScore);
      _pageContainer.children = [_overallPage.element];
    } else {
      element
          ..append(DivElement()
              ..classes.add("ru-irenproject-scoreScreen-scoreUnavailablePanel")
              ..appendText(tr("The work is finished."))
              ..append(_createCloseButton()))
          ..style.justifyContent = "space-between";
    }

    if (usePocketServer) {
      element.append(_creditsPanel);
    }
  }

  @override void onDismiss() {
    _questionPage?.dismiss();
  }

  @override void onResize() {
    if ((_questionPage != null) && isAttached(_questionPage.element)) {
      _questionPage.layOut();
    }
  }

  DivElement _createCloseButton() {
    DivElement res = makeKeyboardAccessible(DivElement())
        ..classes.add("ru-irenproject-scoreScreen-closeButton");

    if (usePocketServer) {
      res
          ..classes.add("ru-irenproject-scoreScreen-closeButton-restart")
          ..text = tr("Start again")
          ..onClick.listen((_) => window.location.reload());
    } else {
      res
          ..classes.add("ru-irenproject-scoreScreen-closeButton-normal")
          ..title = tr("Close")
          ..onClick.listen((_) => _close());
    }

    return res;
  }

  void _close() {
    SavedSession.remove();
    _send(StudentRequest()
        ..close = Close());
  }

  Int64 _send(StudentRequest request) => _connection.send(ClientMessage()
      ..channelId = _studentChannelId
      ..studentRequest = request);

  void _onOverallScoreClick(MouseEvent e) {
    if (_selectHeader(e.currentTarget as Element)) {
      _creditsPanel.style.display = "";
      _pageContainer.children = [_overallPage.element];
    }
  }

  void _onQuestionScoreClick() {
    _questionPage ??= _createQuestionPage();
    _showQuestionPage();
  }

  void _showQuestionPage() {
    if (_selectHeader(_questionPageHeader)) {
      _creditsPanel.style.display = "none";
      _pageContainer.children = [_questionPage.element];
      _questionPage.layOut();
    }
  }

  QuestionScorePage _createQuestionPage({int section}) => QuestionScorePage(
      _showScore.questionScore,
      _showScore.hasSectionScore() && (_showScore.sectionScore.sectionOutcome.length > 1)
          && _showScore.sectionScore.questionSectionIndex.isNotEmpty ? _showScore.sectionScore : null,
      _connection,
      _studentChannelId,
      _noticeStream,
      section: section);

  void _onSectionScoreClick(MouseEvent e) {
    if (_selectHeader(e.currentTarget as Element)) {
      _creditsPanel.style.display = "none";
      _sectionPage ??= SectionScorePage(_showScore.sectionScore,
          _showScore.sectionScore.questionSectionIndex.isEmpty ? null : _onSelectSection);
      _pageContainer.children = [_sectionPage.element];
    }
  }

  bool _selectHeader(Element header) {
    bool res = !header.classes.contains(_SELECTED);
    if (res) {
      for (var e in header.parent.children) {
        e.classes.toggle(_SELECTED, identical(e, header));
      }
    }
    return res;
  }

  void _onSelectSection(int section) {
    if (_questionPage == null) {
      _questionPage = _createQuestionPage(section: section);
    } else {
      _questionPage.selectSection(section, switchQuestion: true);
    }
    _showQuestionPage();
  }

  static DivElement _createCreditsPanel() => DivElement()
      ..classes.add("ru-irenproject-scoreScreen-creditsPanel")
      ..append(AnchorElement(href: "https://irenproject.ru/")
          ..text = tr("Created with Iren")
          ..target = "_blank"
          ..rel = "noopener")
      //TODO automatically insert version
      ..append(AnchorElement(href: "https://irenproject.bitbucket.io/libraries-0.2020.08/")
          ..text = tr("Third-party libraries")
          ..target = "_blank"
          ..rel = "noopener");
}

class OverallScorePage {
  final Element element = DivElement();

  OverallScorePage(ShowScore showScore) {
    element.classes.add("ru-irenproject-testScore-overall");

    var table = TableElement()
        ..classes.add("ru-irenproject-testScore-table");

    if (showScore.hasScaledResult()) {
      int percent = scaledDecimalToIntegerPercent(showScore.scaledResult);

      element.append(_renderChart(percent));

      table.addRow()
          ..classes.add("ru-irenproject-testScore-table-mainRow")
          ..addCell().text = tr("Result:")
          ..addCell().text = "$percent%";
    }

    if (showScore.hasMark()) {
      table.addRow()
          ..classes.add("ru-irenproject-testScore-table-mainRow")
          ..addCell().text = tr("Grade:")
          ..addCell().text = showScore.mark;
    }

    table.addRow()
        ..classes.add("ru-irenproject-testScore-table-divider")
        ..addCell().text = tr("Questions offered:")
        ..addCell().text = "${showScore.questionCount}";

    if (showScore.hasScaledScore()) {
      table.addRow()
          ..addCell().text = tr("Points earned:")
          ..addCell().text = trD("POINTS")(
              formatScaledDecimal(showScore.scaledScore), showScore.perfectScore) as String;
    }

    element.append(table);
  }

  Element _renderChart(int percent) {
    const int SIZE = 200;
    const String BACKGROUND_COLOR = "#c0c0c0";
    const String CORRECT_COLOR = "#00e000";

    var res = CanvasElement(width: SIZE, height: SIZE)
        ..classes.add("ru-irenproject-testScore-chart");
    CanvasRenderingContext2D ctx = res.context2D;

    num xc = SIZE/2 + 0.5;
    num yc = SIZE/2 + 0.5;
    num r = SIZE/2 - 1;

    if ((percent == 0) || (percent == 100)) {
      ctx
          ..beginPath()
          ..arc(xc, yc, r, 0, 2*math.pi)
          ..fillStyle = (percent == 0) ? BACKGROUND_COLOR : CORRECT_COLOR
          ..fill()
          ..stroke();
    } else {
      num angle = -math.pi/2 + 2*math.pi/100*percent;

      ctx
          ..beginPath()
          ..arc(xc, yc, r, 0, 2*math.pi)
          ..fillStyle = BACKGROUND_COLOR
          ..fill()

          ..beginPath()
          ..moveTo(xc, yc)
          ..arc(xc, yc, r, -math.pi/2, angle)
          ..closePath()
          ..fillStyle = CORRECT_COLOR
          ..fill()
          ..stroke()

          ..beginPath()
          ..arc(xc, yc, r, angle, -math.pi/2)
          ..stroke();
    }

    return res;
  }
}

class QuestionScorePage {
  static const String _RESPONSE_HEADER = "ru-irenproject-questionScore-responseHeader";
  static const String _SELECTED = "ru-irenproject-questionScore-responseHeader-selected";
  static const String _INACTIVE_SECTION_QUESTION_COLOR = "#d8d8d8";

  final Element element = DivElement();
  final DivElement _questionContainer = DivElement();
  final DivElement _detailsPanel = DivElement();
  final DivElement _bottomPanel = DivElement();
  final ButtonElement _previousButton = ButtonElement();
  final ButtonElement _nextButton = ButtonElement();

  final Connection _connection;
  final Int64 _studentChannelId;
  final QuestionScore _questionScore;
  final /* nullable */SectionScore _sectionScore;
  StreamSubscription<StudentNotice> _noticeSubscription;

  int _questionIndex;
  final SelectorBar _selectorBar;
  /* nullable */DialogWidget _dialogWidget;
  bool _showingCorrectResponse = false;
  /* nullable */SelectElement _sectionList;
  /* nullable */int _selectedSection;

  /* nullable */Dialog _dialog;
  /* nullable */DialogResponse _response;
  /* nullable */DialogResponse _correctResponse;

  QuestionScorePage(this._questionScore, this._sectionScore, this._connection, this._studentChannelId,
      Stream<StudentNotice> noticeStream, {int section})
      : _selectorBar = SelectorBar(_questionScore.questionDescriptor.length) {
    _questionIndex = (section == null) ? 0 : _sectionScore.questionSectionIndex.indexOf(section);

    _selectorBar.onSelectItem.listen(_onSelectorBarSelectItem);

    element
        ..classes.add("ru-irenproject-questionScore")
        ..append(DivElement()
            ..classes.add("ru-irenproject-questionScore-topPanel")
            ..append(_selectorBar.element));

    if (_questionScore.correctResponseAvailable) {
      element.append(DivElement()
          ..classes.add("ru-irenproject-questionScore-responseSelector"))
          ..append(makeKeyboardAccessible(DivElement())
              ..classes.addAll([_RESPONSE_HEADER, _SELECTED])
              ..text = tr("Your Answer")
              ..onClick.listen((e) => _onHeaderClick(e, false)))
          ..append(makeKeyboardAccessible(DivElement())
              ..classes.add(_RESPONSE_HEADER)
              ..text = tr("Correct Answer")
              ..onClick.listen((e) => _onHeaderClick(e, true)));
    }

    element
        ..append(_questionContainer
            ..classes.add("ru-irenproject-questionScore-questionContainer"))
        ..append(_detailsPanel
            ..classes.add("ru-irenproject-questionScore-detailsPanel"))
        ..append(_bottomPanel
            ..classes.add("ru-irenproject-questionScore-bottomPanel")
            ..append(_previousButton
                ..classes.add("ru-irenproject-questionScore-previousButton")
                ..text = tr("Previous")
                ..onClick.listen((_) => _selectAdjacentQuestion(forward: false)))
            ..append(_nextButton
                ..classes.add("ru-irenproject-questionScore-nextButton")
                ..text = tr("Next")
                ..onClick.listen((_) => _selectAdjacentQuestion(forward: true))));

    if (_sectionScore != null) {
      _sectionList = SelectElement()
          ..classes.add("ru-irenproject-questionScore-sectionList")
          ..append(OptionElement(data: tr("(All)")))
          ..children.addAll(_sectionScore.sectionOutcome.map((s) => OptionElement(data: s.name)))
          ..onChange.listen((_) => selectSection(
              (_sectionList.selectedIndex > 0) ? _sectionList.selectedIndex - 1 : null, switchQuestion: true));
      _bottomPanel
          ..append(SpanElement()
              ..classes.add("ru-irenproject-questionScore-sectionListCaption")
              ..text = "${tr("Show section:")}\u00a0")
          ..append(_sectionList);
    }

    selectSection(section, switchQuestion: false);

    _noticeSubscription = noticeStream.listen(_onNotice);
    _sendQueryQuestionDetails(_questionIndex);
  }

  void dismiss() {
    _noticeSubscription?.cancel();
    _noticeSubscription = null;
  }

  void _onNotice(StudentNotice m) {
    if (m.hasShowQuestionDetails()) {
      _showQuestionDetails(m.showQuestionDetails);
    }
  }

  void _showQuestionDetails(ShowQuestionDetails m) {
    if (_inSelectedSection(m.questionIndex)) {
      _dialog = m.dialog;
      _response = m.response;
      _correctResponse = m.hasCorrectResponse() ? m.correctResponse : null;

      _questionIndex = m.questionIndex;
      _selectorBar.setCurrentItem(_questionIndex);

      _updateControlAvailability();
      _updateDetailsPanel(m);
      _showCurrentQuestion();
    }
  }

  void _updateControlAvailability() {
    if (_inSelectedSection(_questionIndex)) {
      _previousButton.disabled = (_getAdjacentQuestion(forward: false) == null);
      _nextButton.disabled = (_getAdjacentQuestion(forward: true) == null);
    } else {
      _previousButton.disabled = _nextButton.disabled = true;
    }
  }

  void _updateDetailsPanel(ShowQuestionDetails m) {
    _detailsPanel.nodes.clear();

    if (m.hasScaledResult()) {
      _detailsPanel
          ..appendText("${tr("QUESTION_RESULT")} ")
          ..append(SpanElement()
              ..classes.add("ru-irenproject-questionScore-result")
              ..text = "${scaledDecimalToIntegerPercent(m.scaledResult)}%");
    }

    if (m.hasWeight()) {
      _detailsPanel
          ..appendText("${tr("Question weight:")} ")
          ..append(SpanElement()
              ..classes.add("ru-irenproject-questionScore-weight")
              ..text = "${m.weight}");
    }

    if (m.hasScaledScore()) {
      _detailsPanel
          ..appendText("${tr("QUESTION_SCORE")} ")
          ..append(SpanElement()
              ..classes.add("ru-irenproject-questionScore-score")
              ..text = formatScaledDecimal(m.scaledScore));
    }

    if (_sectionScore != null) {
      String sectionName = _sectionScore.sectionOutcome[_sectionScore.questionSectionIndex[_questionIndex]].name;
      _detailsPanel
          ..appendText("${tr("Section:")} ")
          ..append(SpanElement()
              ..text = sectionName
              ..title = sectionName);
    }

    _detailsPanel.style.display = _detailsPanel.hasChildNodes() ? "" : "none";
  }

  void _showCurrentQuestion() {
    _dialogWidget = DialogWidget(_dialog, _showingCorrectResponse ? _correctResponse : _response)
        ..setReadOnly(true);
    _questionContainer.children = [_dialogWidget.element];

    if (isAttached(element)) {
      _dialogWidget.layOut();
    }

    _questionContainer.scrollTop = 0;
  }

  void layOut() {
    _selectorBar.layOut();
    _dialogWidget?.layOut();
  }

  void _onSelectorBarSelectItem(SelectItemEvent e) {
    if (!_inSelectedSection(e.itemIndex)) {
      selectSection(null, switchQuestion: false);
    }
    _sendQueryQuestionDetails(e.itemIndex);
  }

  void _sendQueryQuestionDetails(int questionIndex) {
    _send(StudentRequest()
        ..queryQuestionDetails = (QueryQuestionDetails()
            ..questionIndex = questionIndex));
  }

  Int64 _send(StudentRequest request) => _connection.send(ClientMessage()
      ..channelId = _studentChannelId
      ..studentRequest = request);

  void _selectAdjacentQuestion({@required bool forward}) {
    int questionIndex = _getAdjacentQuestion(forward: forward);
    if (questionIndex != null) {
      _sendQueryQuestionDetails(questionIndex);
    }
  }

  void _onHeaderClick(MouseEvent e, bool showCorrectResponse) {
    if (_selectHeader(e.currentTarget as Element)) {
      _showingCorrectResponse = showCorrectResponse;
      if (_dialog != null) {
        int saved = _questionContainer.scrollTop;
        _showCurrentQuestion();
        _questionContainer.scrollTop = saved;
      }
    }
  }

  bool _selectHeader(Element header) {
    bool res = !header.classes.contains(_SELECTED);
    if (res) {
      for (var e in header.parent.children) {
        e.classes.toggle(_SELECTED, identical(e, header));
      }
    }
    return res;
  }

  void selectSection(/* nullable */int section, {@required bool switchQuestion}) {
    _selectedSection = section;
    if (_sectionList != null) {
      _sectionList.selectedIndex = (_selectedSection == null) ? 0 : _selectedSection + 1;
    }

    setUpSelectorBar(_questionScore.questionDescriptor, _selectorBar);
    if (_selectedSection != null) {
      _sectionScore.questionSectionIndex.asMap().forEach((q, s) {
        if (s != _selectedSection) {
          _selectorBar.setItemColor(q, _INACTIVE_SECTION_QUESTION_COLOR);
        }
      });
    }

    _updateControlAvailability();

    if (switchQuestion && !_inSelectedSection(_questionIndex)) {
      int newQuestionIndex = _getAdjacentQuestion(forward: true) ?? _getAdjacentQuestion(forward: false);
      if (newQuestionIndex != null) {
        _sendQueryQuestionDetails(newQuestionIndex);
      }
    }
  }

  /* nullable */int _getAdjacentQuestion({@required bool forward}) {
    int index = _questionIndex;
    bool inRange;
    do {
      index += forward ? 1 : -1;
      inRange = (index >= 0) && (index < _questionScore.questionDescriptor.length);
    } while (inRange && !_inSelectedSection(index));
    return inRange ? index : null;
  }

  bool _inSelectedSection(int questionIndex) => (_selectedSection == null)
      || (_sectionScore.questionSectionIndex[questionIndex] == _selectedSection);
}

class SectionScorePage {
  final Element element = DivElement();
  final TableElement _table = TableElement();

  final SectionScore _sectionScore;
  final void /* nullable */Function(int section) _selectSectionHandler;

  SectionScorePage(this._sectionScore, [this._selectSectionHandler]) {
    element
        ..classes.add("ru-irenproject-sectionScore")
        ..append(_table
            ..classes.add("ru-irenproject-sectionScore-table"));

    _renderHeader(_table.createTHead().addRow());

    TableSectionElement tbody = _table.createTBody();
    for (var outcome in _sectionScore.sectionOutcome) {
      TableRowElement row = tbody.addRow();
      _renderSectionOutcome(outcome, row);
      if (_selectSectionHandler != null) {
        row
            ..classes.add("ru-irenproject-sectionScore-selectable")
            ..onClick.listen(_onRowClick);
      }
    }
  }

  void _renderHeader(TableRowElement row) {
    row.addCell().text = tr("Section");

    if (_sectionScore.resultAvailable) {
      row.addCell().text = tr("Result, %");
    }

    if (_sectionScore.scoreAvailable) {
      row
          ..addCell().text = tr("Points Earned")
          ..addCell().text = tr("Points Total");
    }

    if (_sectionScore.questionCountAvailable) {
      row.addCell().text = tr("Questions Total");
    }
  }

  void _renderSectionOutcome(SectionOutcome outcome, TableRowElement row) {
    row.addCell().text = outcome.name;

    if (_sectionScore.resultAvailable) {
      row.addCell().text = "${scaledDecimalToIntegerPercent(outcome.scaledResult)}";
    }

    if (_sectionScore.scoreAvailable) {
      row
          ..addCell().text = formatScaledDecimal(outcome.scaledScore)
          ..addCell().text = "${outcome.perfectScore}";
    }

    if (_sectionScore.questionCountAvailable) {
      row.addCell().text = "${outcome.questionCount}";
    }
  }

  void _onRowClick(MouseEvent e) {
    _selectSectionHandler((e.currentTarget as TableRowElement).sectionRowIndex);
  }
}
