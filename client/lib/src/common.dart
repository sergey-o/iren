/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:async';
import 'dart:collection';
import 'dart:html';
import 'dart:math' as math;
import 'dart:typed_data';

import 'package:async/async.dart';
import 'package:fixnum/fixnum.dart';
import 'package:image/image.dart' show DecodeInfo, Decoder, JpegDecoder, PngDecoder;
import 'package:iren_proto/content.pb.dart';
import 'package:iren_proto/dialog.pb.dart';
import 'package:iren_proto/google/protobuf/any.pb.dart';
import 'package:iren_proto/protocol.pb.dart';
import 'package:meta/meta.dart';
import 'package:protobuf/protobuf.dart';
import 'package:web_helpers/translator.dart';
import 'package:web_helpers/web_helpers.dart';

import 'pocket_server_support.dart';

typedef AreaWidgetFactory = AreaWidget Function(Any area, /* nullable */Any response);

class DialogWidget {
  final DivElement element = DivElement();
  final List<AreaWidget> _areas = [];
  final LinkedHashMap<String, AreaWidget> _actionAreas = LinkedHashMap();

  final StreamController<DialogEvent> _onResponse = StreamController.broadcast(sync: true);
  Stream<DialogEvent> get onResponse => _onResponse.stream;

  DialogWidget(Dialog dialog, DialogResponse response) {
    for (var dialogArea in dialog.dialogArea) {
      AreaWidgetFactory f = _areaWidgetRegistry[dialogArea.area.typeUrl];
      if (f == null) {
        throw "No widget for area '${dialogArea.area.typeUrl}'.";
      }

      AreaWidget areaWidget = f(dialogArea.area, dialogArea.hasId() ? getAreaResponse(dialogArea.id, response) : null);
      _areas.add(areaWidget);

      if (dialogArea.hasId()) {
        check(!_actionAreas.containsKey(dialogArea.id));
        _actionAreas[dialogArea.id] = areaWidget;
        areaWidget.onResponse.listen((_) => _onResponse.add(DialogEvent(this)));
      }

      element.append(areaWidget.element);
    }
  }

  DialogResponse get response => DialogResponse()
      ..area.addAll(_actionAreas.map((id, w) => MapEntry(id, toCustomAny(w.response))));

  void layOut() {
    for (var w in _areas) {
      w.layOut();
    }
  }

  void setFocus() {
    for (var w in _areas) {
      if (w.setFocus()) {
        break;
      }
    }
  }

  void setReadOnly(bool readOnly) {
    for (var w in _areas) {
      w.setReadOnly(readOnly);
    }
  }
}

class DialogEvent {
  final DialogWidget source;
  DialogEvent(this.source);
}

abstract class AreaWidget {
  /* nullable */GeneratedMessage get response;
  Element get element;

  bool _readOnly = false;

  final StreamController<void> _onResponse = StreamController.broadcast(sync: true);
  Stream<void> get onResponse => _onResponse.stream;

  @protected void triggerResponse() {
    _onResponse.add(null);
  }

  void layOut() {}

  bool setFocus() => false;

  bool get isReadOnly => _readOnly;

  void setReadOnly(bool readOnly) {
    _readOnly = readOnly;
    element.classes.toggle("ru-irenproject-area-readOnly", readOnly);
  }
}

class FlowAreaWidget extends AreaWidget {
  @override /* nullable */GeneratedMessage get response => null;

  @override final DivElement element;

  FlowAreaWidget(FlowArea area)
      : element = renderFlow(area.flow)
            ..classes.add("ru-irenproject-flowArea");
}

class YesNoDialog {
  final DivElement _element = DivElement();
  final Element _curtain;
  final ButtonElement _yesButton = ButtonElement();
  final ButtonElement _noButton = ButtonElement();

  YesNoDialog(String message, String yes, String no, String className, void Function() onYesClick)
      : _curtain = attachCurtain() {
    document.body.append(_element
        ..classes.addAll(["ru-irenproject-yesNoDialog", className])
        ..appendText(message)
        ..append(BRElement())
        ..append(_yesButton
            ..text = yes
            ..onClick.listen((e) {
              _close();
              onYesClick();
            }))
        ..append(_noButton
            ..text = no
            ..onClick.listen((_) => _close()))
        ..onKeyDown.listen(_onKeyDown)
        ..onKeyPress.listen((e) => e.stopPropagation())
        ..onKeyUp.listen((e) => e.stopPropagation()));

    _yesButton.focus();
  }

  void _close() {
    _element.remove();
    _curtain.remove();
  }

  void _onKeyDown(KeyboardEvent e) {
    e.stopPropagation();

    switch (e.keyCode) {
      case KeyCode.ESC:
        _close();
        break;
      case KeyCode.TAB:
        e.preventDefault();
        ((document.activeElement == _yesButton) ? _noButton : _yesButton).focus();
        break;
    }
  }
}

class _ImageSize {
  final int width;
  final int height;
  _ImageSize(this.width, this.height);
}

abstract class ProgramScreen {
  Element get element;
  void onShow() {}
  void onResize() {}
  void onDismiss() {}
}

class Connection {
  final Transport _transport;

  Int64 _lastRequestId = Int64(ProtocolConstants.LAST_RESERVED_CLIENT_CHANNEL_ID.value);
  Int64 _lastClientChannelId = Int64.ZERO;

  Connection(this._transport);

  Stream<ServerMessage> get onMessage => _transport.onMessage;

  /// Returns request id.
  Int64 send(ClientMessage message) {
    ++_lastRequestId;
    _transport.send(message);
    return _lastRequestId;
  }

  Future<ServerMessage> perform(ClientMessage request) => tune(send(request)).first;

  Stream<ServerMessage> tune(Int64 channelId) => onMessage.where((m) => m.channelId == channelId);

  Int64 openClientChannel() {
    --_lastClientChannelId;
    return _lastClientChannelId;
  }

  Stream<T> emitErrorOnClose<T>(Stream<T> source) => StreamGroup.merge([source, _transport.onClose.transform(
      StreamTransformer.fromHandlers(
          handleData: (_, sink) => sink.addError(ClosedConnectionException())))]);

  bool get connected => _transport.connected;
}

class ClosedConnectionException implements Exception {}

abstract class Transport {
  @protected final StreamController<ServerMessage> messageStreamController = StreamController.broadcast(sync: true);
  Stream<ServerMessage> get onMessage => messageStreamController.stream;

  void send(ClientMessage message);

  Stream<CloseEvent> get onClose;
  bool get connected;
}

class WebSocketTransport extends Transport {
  final WebSocket _socket;

  WebSocketTransport(this._socket) {
    _socket.onMessage.listen(_onSocketMessage);
  }

  void _onSocketMessage(MessageEvent e) {
    ByteBuffer b = e.data as ByteBuffer;
    var m = ServerMessage.fromBuffer(b.asUint8List());
    messageStreamController.add(m);
  }

  @override void send(ClientMessage message) {
    if (![WebSocket.CLOSING, WebSocket.CLOSED].contains(_socket.readyState)) {
      _socket.sendTypedData(message.writeToBuffer());
    }
  }

  @override Stream<CloseEvent> get onClose => _socket.onClose;

  @override bool get connected => _socket.readyState == WebSocket.OPEN;
}

class SavedSession {
  static const String _SAVED_SESSION = "savedSession";
  static const String _ALWAYS_TRANSIENT = "alwaysTransient";

  static /* nullable */SavedSession getIfPresent() {
    SavedSession res;
    String s = window.localStorage[_SAVED_SESSION] ?? window.sessionStorage[_SAVED_SESSION];
    if (s == null) {
      res = null;
    } else {
      List<String> parts = s.split("|");
      res = (parts.length == 3) ? SavedSession(
          workId: parts[0],
          sessionKey: parts[1],
          alwaysTransient: parts[2] == _ALWAYS_TRANSIENT) : null;
    }

    return res;
  }

  static void remove() {
    window.localStorage.remove(_SAVED_SESSION);
    window.sessionStorage.remove(_SAVED_SESSION);
  }

  final String workId;
  final String sessionKey;
  final bool alwaysTransient;

  SavedSession({@required this.workId, @required this.sessionKey, @required this.alwaysTransient});

  void save({bool transient: false}) {
    String value = "$workId|$sessionKey|${alwaysTransient ? _ALWAYS_TRANSIENT : ""}";

    if (transient || alwaysTransient) {
      window
          ..sessionStorage[_SAVED_SESSION] = value
          ..localStorage.remove(_SAVED_SESSION);
    } else {
      window
          ..localStorage[_SAVED_SESSION] = value
          ..sessionStorage.remove(_SAVED_SESSION);
    }
  }
}

class DummySavedSession extends SavedSession {
  DummySavedSession() : super(workId: "", sessionKey: "", alwaysTransient: true);

  @override void save({bool transient: false}) {}
}

const int _SCALED_DECIMAL_FRACTION_DIGITS = 4; // must correspond to the value on the server
const int _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS = 2;

const String _KEY_BLOCKER_CLASS = "ru-irenproject-keyBlocker";

bool _globalKeyHandlerInstalled = false;

final Map<String, Decoder Function()> _imageDecoders = {
    "image/png": () => PngDecoder(),
    "image/jpeg": () => JpegDecoder()};

final Map<String/* typeUrl */, AreaWidgetFactory> _areaWidgetRegistry = {};

/* nullable */ProgramScreen _currentScreen;
/* nullable */StreamSubscription<Event> _windowResizeSubscription;

DivElement renderFlow(Flow flow) {
  var res = DivElement()
      ..classes.add("ru-irenproject-flow")
      ..style.userSelect = "none";
  ParagraphElement p;

  void createParagraph() {
    p = ParagraphElement();
    res.append(p);
  }

  createParagraph();

  for (var block in flow.block) {
    switch (block.type) {
      case BlockType.TEXT:
        p.appendText(block.textBlock.text);
        break;
      case BlockType.IMAGE:
        _ImageSize size = _getImageSize(block.imageBlock.data, block.imageBlock.mimeType);
        var image = ImageElement(src: transparentOnePixelPngUrl)
            ..classes.add("ru-irenproject-flow-image")
            ..style.width = "${size.width}px"
            ..style.height = "${size.height}px";
        p.append(image);

        var reader = FileReader();
        reader
            ..onLoad.listen((_) => image.src = reader.result as String)
            ..readAsDataUrl(Blob([block.imageBlock.data], block.imageBlock.mimeType));
        break;
      case BlockType.LINE_FEED:
        createParagraph();
        break;
      case BlockType.FORMULA:
        var formula = ImageElement(src: transparentOnePixelPngUrl);
        applyFormulaStyle(block.formulaBlock.style, formula.style);
        p.append(formula);

        var reader = FileReader();
        reader
            ..onLoad.listen((_) => formula.src = reader.result as String)
            ..readAsDataUrl(Blob([block.formulaBlock.svg], "image/svg+xml"));
        break;
    }
  }

  return res;
}

_ImageSize _getImageSize(List<int> image, String mimeType) {
  var f = _imageDecoders[mimeType];
  if (f == null) {
    throw "No image decoder for $mimeType.";
  }
  DecodeInfo info = f().startDecode(image);
  return _ImageSize(info.width, info.height);
}

Any getAreaResponse(String areaId, DialogResponse dialogResponse) {
  Any res = dialogResponse.area[areaId];
  check(res != null);
  return res;
}

num scaledDecimalToPercent(int scaledDecimal) =>
    scaledDecimal / math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - 2);

int scaledDecimalToIntegerPercent(int scaledDecimal) =>
    scaledDecimal ~/ math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - 2);

String formatScaledDecimal(Int64 scaledDecimal) {
  Int64 n = scaledDecimal ~/ math.pow(10, _SCALED_DECIMAL_FRACTION_DIGITS - _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS);

  int d = math.pow(10, _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS).toInt();
  Int64 whole = n ~/ d;
  int fraction = (n % d).toInt();

  String fs = formatWithLeadingZeros(fraction, _FORMATTED_SCALED_DECIMAL_FRACTION_DIGITS);
  return "$whole$uiDecimalMark$fs";
}

String formatWithLeadingZeros(int n, int digits) {
  String s = n.toString();
  String padding = List.filled(math.max(digits - s.length, 0), "0").join();
  return padding + s;
}

bool isAttached(Element e) => document.documentElement.contains(e);

String formatRemainingTime(num millisecondsRemaining, {bool showLastSeconds: false}) {
  String format(int n) => formatWithLeadingZeros(n, 2);

  const int LIMIT = (23*60+1)*60*1000;
  int seconds = math.min(millisecondsRemaining, LIMIT) ~/ 1000;

  String s;
  if (showLastSeconds && (seconds <= 30)) {
    s = "00:00:${format(seconds)}";
  } else {
    int minutes = (seconds + 59) ~/ 60;
    s = "${format(minutes ~/ 60)}:${format(minutes % 60)}";
  }

  return ((millisecondsRemaining > LIMIT) ? "> " : "") + s;
}

void _installGlobalKeyHandler() {
  if (!_globalKeyHandlerInstalled) {
    window.onKeyDown.listen(_globalKeyHandler);
    window.onKeyPress.listen(_globalKeyHandler);
    window.onKeyUp.listen(_globalKeyHandler);

    _globalKeyHandlerInstalled = true;
  }
}

void _globalKeyHandler(KeyboardEvent e) {
  if (keyboardInputBlocked()) {
    e.preventDefault();
  }
}

bool keyboardInputBlocked() => querySelector(".$_KEY_BLOCKER_CLASS") != null;

Element attachCurtain() {
  _installGlobalKeyHandler();

  var res = DivElement()
      ..classes.addAll(["ru-irenproject-curtain", _KEY_BLOCKER_CLASS]);
  document.body.append(res);
  return res;
}

E makeKeyboardAccessible<E extends Element>(E e, {bool trackSpace: true}) => e
    ..tabIndex = 0
    ..onKeyDown.listen((event) => _convertToClick(event, trackSpace));

void _convertToClick(KeyboardEvent e, bool trackSpace) {
  if (!keyboardInputBlocked() && [KeyCode.ENTER, if (trackSpace) KeyCode.SPACE].contains(e.keyCode)) {
    e
        ..preventDefault()
        ..stopImmediatePropagation()
        ..target.dispatchEvent(MouseEvent("click"));
  }
}

void registerAreaWidget(GeneratedMessage sampleArea, AreaWidgetFactory factory) {
  String typeUrl = getTypeUrl(sampleArea);
  check(!_areaWidgetRegistry.containsKey(typeUrl));
  _areaWidgetRegistry[typeUrl] = factory;
}

void showScreen(/* nullable */ProgramScreen screen) {
  Element screenHost = document.getElementById("screenHost");
  if (screenHost == null) {
    throw "screenHost not found.";
  }
  _windowResizeSubscription ??= window.onResize.listen((_) => _currentScreen?.onResize());

  screenHost.nodes.clear();
  _currentScreen?.onDismiss();

  _currentScreen = screen;

  if (_currentScreen != null) {
    screenHost.append(_currentScreen.element);
    _currentScreen.onShow();
  }
}

String tr(String key) => trD(key) as String;

dynamic trD(String key) => translate("iren_client", key);
