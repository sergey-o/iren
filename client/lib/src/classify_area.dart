/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'dart:html';
import 'dart:math' as math;

import 'package:html5_dnd/html5_dnd.dart' as dnd;
import 'package:iren_proto/area/classify_area.pb.dart';
import 'package:iren_proto/content.pb.dart';

import 'common.dart';

class ClassifyAreaWidget extends AreaWidget {
  static const int _CONTAINER_BORDER_SIZE = 1;
  static const int _CONTAINER_PADDING = 10;
  static const int _CONTAINER_FRAME_SIZE = _CONTAINER_BORDER_SIZE + _CONTAINER_PADDING;
  static const int _HORIZONTAL_SPACING = 50;
  static const int _ITEM_VERTICAL_SPACING = 13;
  static const int _DIVIDER_BORDER_SIZE = 1;

  final ClassifyArea _area;
  @override final ClassifyResponse response;

  @override final DivElement element = DivElement();
  final List<Element> _offered = [];
  final List<Element> _containers = [];
  final List<Element> _innerBoxes = [];
  final Element _leftPanel = DivElement();
  Element _divider;
  final dnd.DraggableGroup _dragGroup = dnd.DraggableGroup();
  final dnd.DropzoneGroup _dropGroup = dnd.DropzoneGroup();

  ClassifyAreaWidget(this._area, this.response) {
    element
        ..classes.add("ru-irenproject-classifyArea")
        ..append(_leftPanel);

    for (var title in _area.categoryTitle) {
      _createInnerBox(_createContainer()
          ..append(_renderCategoryTitle(title)));
    }

    _area.offered.forEach(_createItem);

    _createDivider();

    _dragGroup.installAll(_offered);
    _dropGroup.onDrop.listen(_onDrop);
    _dragGroup.onDragEnd.listen(_onDragEnd);
  }

  Element _renderCategoryTitle(Flow title) => renderFlow(title)
      ..classes.add("ru-irenproject-classifyArea-categoryTitle");

  Element _createContainer() {
    var res = DivElement()
        ..classes.addAll(["ru-irenproject-classifyArea-container", "dnd-rectangle-target"])
        ..style.borderWidth = "${_CONTAINER_BORDER_SIZE}px";
    _leftPanel.append(res);
    _dropGroup.install(res);
    _containers.add(res);
    return res;
  }

  void _createInnerBox(Element container) {
    var e = DivElement();
    container.append(e);
    _innerBoxes.add(e);
  }

  void _createItem(Flow flow) {
    Element e = renderFlow(flow)
        ..classes.add("ru-irenproject-classifyArea-item");
    element.append(e);
    _offered.add(e);
  }

  void _createDivider() {
    _divider = DivElement()
        ..classes.add("ru-irenproject-classifyArea-divider")
        ..style.borderWidth = "${_DIVIDER_BORDER_SIZE}px";
    element.append(_divider);
  }

  @override void layOut() {
    int areaWidth = element.clientWidth;
    int itemWidth = math.max((areaWidth - 2*_CONTAINER_FRAME_SIZE - _HORIZONTAL_SPACING) ~/ 2, 0);
    _leftPanel.style.width = "${itemWidth + 2*_CONTAINER_FRAME_SIZE}px";

    int maxItemHeight = 0;
    for (var e in _offered) {
      e.style.width = "${itemWidth}px";
      maxItemHeight = math.max(maxItemHeight, e.offsetHeight);
    }

    int classifiedItemLeft = _CONTAINER_FRAME_SIZE;
    int unclassifiedItemLeft = areaWidth - itemWidth;
    int unclassifiedItemTop = 0;
    var innerBoxHeights = List.filled(_innerBoxes.length, 0);

    _offered.asMap().forEach((i, e) {
      int categoryIndex = response.mapping[i];
      if (categoryIndex == -1) {
        e.style
            ..top = "${unclassifiedItemTop}px"
            ..left = "${unclassifiedItemLeft}px";
      } else {
        e.style
            ..top = "${innerBoxHeights[categoryIndex]}px"
            ..left = "${classifiedItemLeft}px";
        innerBoxHeights[categoryIndex] += e.offsetHeight + _ITEM_VERTICAL_SPACING;
      }
      unclassifiedItemTop += e.offsetHeight + _ITEM_VERTICAL_SPACING;
    });

    element.style.minHeight = "${math.max(unclassifiedItemTop - _ITEM_VERTICAL_SPACING, 0)}px";

    _innerBoxes.asMap().forEach((i, innerBox) {
      innerBox.style.height = "${innerBoxHeights[i] + maxItemHeight + 2*_CONTAINER_PADDING}px";
    });

    _offered.asMap().forEach((i, e) {
      int categoryIndex = response.mapping[i];
      if (categoryIndex != -1) {
        e.style.top = "${e.offsetTop + _innerBoxes[categoryIndex].offsetTop + _CONTAINER_PADDING}px";
      }
    });

    _divider.style.right = "${itemWidth + _HORIZONTAL_SPACING~/2 - _DIVIDER_BORDER_SIZE}px";
  }

  void _onDrop(dnd.DropzoneEvent e) {
    _putToContainer(_offered.indexOf(e.draggable), _containers.indexOf(e.dropzone));
  }

  void _onDragEnd(dnd.DraggableEvent e) {
    if (!dnd.dragCanceled && !dnd.droppedOverTarget) {
      _putToContainer(_offered.indexOf(e.draggable), null);
    }
  }

  void _putToContainer(int offeredIndex, /* nullable */int containerIndex) {
    int newMapping = containerIndex ?? -1;
    if (response.mapping[offeredIndex] != newMapping) {
      response.mapping[offeredIndex] = newMapping;
      triggerResponse();
      layOut();
    }
  }

  @override void setReadOnly(bool readOnly) {
    super.setReadOnly(readOnly);
    _dragGroup.cancelDrag();

    _dragGroup.uninstallAll(_offered);
    if (!readOnly) {
      _dragGroup.installAll(_offered);
    }
  }
}
