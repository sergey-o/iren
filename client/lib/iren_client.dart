/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

import 'package:iren_proto/area/classify_area.pb.dart';
import 'package:iren_proto/area/input_area.pb.dart';
import 'package:iren_proto/area/match_area.pb.dart';
import 'package:iren_proto/area/order_area.pb.dart';
import 'package:iren_proto/area/select_area.pb.dart';
import 'package:iren_proto/content.pb.dart';

import 'src/classify_area.dart';
import 'src/common.dart';
import 'src/input_area.dart';
import 'src/match_area.dart';
import 'src/order_area.dart';
import 'src/pocket_server_support.dart';
import 'src/select_area.dart';

export 'src/common.dart' show Connection, ClosedConnectionException, DialogEvent, DialogWidget, WebSocketTransport,
    formatRemainingTime, formatScaledDecimal, scaledDecimalToIntegerPercent, scaledDecimalToPercent;
export 'src/languages.dart' show registerClientDictionaries;
export 'src/selector_bar.dart';

void registerAreaWidgets() {
  registerAreaWidget(FlowArea.getDefault(), (a, r) => FlowAreaWidget(
      unpackCustomAny(a, FlowArea())));

  registerAreaWidget(SelectArea.getDefault(), (a, r) => SelectAreaWidget(
      unpackCustomAny(a, SelectArea()),
      unpackCustomAny(r, SelectResponse())));

  registerAreaWidget(InputArea.getDefault(), (a, r) => InputAreaWidget(
      unpackCustomAny(a, InputArea()),
      unpackCustomAny(r, InputResponse())));

  registerAreaWidget(MatchArea.getDefault(), (a, r) => MatchAreaWidget(
      unpackCustomAny(a, MatchArea()),
      unpackCustomAny(r, MatchResponse())));

  registerAreaWidget(OrderArea.getDefault(), (a, r) => OrderAreaWidget(
      unpackCustomAny(a, OrderArea()),
      unpackCustomAny(r, OrderResponse())));

  registerAreaWidget(ClassifyArea.getDefault(), (a, r) => ClassifyAreaWidget(
      unpackCustomAny(a, ClassifyArea()),
      unpackCustomAny(r, ClassifyResponse())));
}
