/*
  Copyright 2012-2020 Sergey Ostanin

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

"use strict";

const asarAwareFs = require("fs");
const child_process = require("child_process");
const crypto = require("crypto");
const electron = require("electron");
const fs = require("original-fs");
const path = require("path");

const {app, BrowserWindow, ipcMain, dialog} = electron;

process.on("uncaughtException", e => {
  dialog.showErrorBox("Error", e.stack || "(no description)");
  app.exit(1);
});

let mainWindow = null;
const devel = process.argv.includes("--devel");
const devtoolsSwitch = "--devtools";

const dataDir = path.join(
    (process.platform == "linux") ? path.join(app.getPath("home"), ".local", "share") : app.getPath("appData"),
    "irenproject.ru");

ipcMain.on("getDataDirectory", e => {
  e.returnValue = dataDir;
});

let programName = null;

ipcMain.on("getProgramName", e => {
  e.returnValue = programName;
});

let server = null;
let connectionConfig = null;
let connectionConfigRequested = false;

app.on("window-all-closed", () => {
  if (!server) {
    app.quit();
  }
});

let savedLoginInfo = JSON.stringify({});

ipcMain.on("saveLoginInfo", (e, loginInfo) => {
  savedLoginInfo = loginInfo;
  e.returnValue = null;
});

ipcMain.on("getSavedLoginInfo", e => {
  e.returnValue = savedLoginInfo;
});

let certificateDialog = null;

ipcMain.on("setCertificateDialog", (e, dialog) => {
  certificateDialog = JSON.parse(dialog);
  e.returnValue = null;
});

app.userAgentFallback = `iren/${asarAwareFs.readFileSync(path.join(__dirname, "VERSION"), "utf8")}`;

app.setAppPath(devel ?
    path.join(__dirname, "..", "..", "..", "..", "shell", "web") :
    path.join(__dirname, "..", "iren.asar"));

app.applicationMenu = null;
app.disableHardwareAcceleration();

if (process.argv.includes("--master")) {
  runMaster();
} else if (process.argv.includes("--workplace")) {
  runWorkplace();
} else {
  runEditor();
}

function runMaster() {
  programName = "master";

  app.name = "irenMaster";
  app.setPath("userData", path.join(dataDir, "master", "shellData"));
  app.setPath("userCache", path.join(app.getPath("cache"), "irenproject.ru", "master", "shellCache"));

  if (ensureSingleInstance()) {
    if (devel) {
      connectionConfig = JSON.stringify({
          port: 9981,
          supervisorKey: fs.readFileSync(path.join(dataDir, "develKey")).toString("hex")});
    } else {
      startServer("irenServer", "ru.irenproject.Server", path.join(dataDir, "server.log"));
    }

    app.on("ready", () => {
      createWindow("irenMaster");
      loadPage();
    });
  }
}

function loadPage() {
  if (devel) {
    mainWindow.loadURL("http://127.0.0.1:8082/");
  } else {
    mainWindow.loadFile("index.html");
  }
}

function runWorkplace() {
  programName = "master";

  app.name = "irenWorkplace";
  app.setPath("userData", path.join(dataDir, "workplace", "shellData"));
  app.setPath("userCache", path.join(app.getPath("cache"), "irenproject.ru", "workplace", "shellCache"));

  connectionConfig = JSON.stringify(null);

  app.on("ready", () => {
    createWindow("irenWorkplace");
    mainWindow.webContents.on("certificate-error", onCertificateError);
    loadPage();
  });
}

function runEditor() {
  programName = "editor";

  app.name = "irenEditor";
  app.setPath("userData", path.join(dataDir, "editor", "shellData"));
  app.setPath("userCache", path.join(app.getPath("cache"), "irenproject.ru", "editor", "shellCache"));

  if (ensureSingleInstance()) {
    if (devel) {
      connectionConfig = JSON.stringify({
          port: 9982,
          editorKey: fs.readFileSync(path.join(dataDir, "editor", "editorKey")).toString("hex")});
    } else {
      startServer("irenEditorServer", "ru.irenproject.editor.EditorServer",
          path.join(dataDir, "editor", "editorServer.log"));
    }

    app.on("ready", () => {
      createWindow("irenEditor");
      loadPage();
    });
  }
}

function ensureSingleInstance() {
  const lockAcquired = app.requestSingleInstanceLock();

  if (lockAcquired) {
    app.on("second-instance", (_, argv, workingDirectory) => {
      if (mainWindow) {
        if (mainWindow.isMinimized()) {
          mainWindow.restore();
        }
        mainWindow.focus();

        if (argv.includes(devtoolsSwitch)) {
          mainWindow.webContents.openDevTools();
        }

        if ((argv.length >= 2) && !argv[argv.length - 1].startsWith("-")) {
          mainWindow.webContents.send("openFile", path.resolve(workingDirectory, argv[argv.length - 1]));
        }
      }
    });
  } else {
    app.quit();
  }

  return lockAcquired;
}

function startServer(exeName, className, logFileName) {
  const topDir = path.join(__dirname, "..", "..", "..");

  const logFile = fs.openSync(logFileName, "w");
  try {
    server = child_process.spawn(
        path.join(topDir, "jre", "bin", exeName + ((process.platform == "win32") ? ".exe" : "")),
        [
            "-XX:CICompilerCount=2",
            "--add-opens=java.base/java.lang=ALL-UNNAMED",
            "--add-opens=java.base/java.lang.invoke=ALL-UNNAMED",
            "--add-opens=java.base/java.nio=ALL-UNNAMED",
            "-classpath",
            path.join(topDir, "lib", "*"),
            className,
            "--companion"
        ],
        {stdio: ["pipe", "pipe", logFile], detached: true});
  } finally {
    fs.closeSync(logFile);
  }

  server.on("exit", () => {
    app.exit(0);
  });

  let connectionConfigBuffer = "";
  server.stdout.on("data", data => {
    connectionConfigBuffer += data;
  });

  server.stdout.on("end", () => {
    connectionConfig = connectionConfigBuffer;
    sendConnectionConfigIfPossible();
  });
}

function createWindow(partition) {
  const webPreferences = {
      nodeIntegration: true,
      partition: partition,
      backgroundThrottling: false};

  mainWindow = new BrowserWindow({
      webPreferences: webPreferences,
      width: 1020,
      height: 700,
      useContentSize: true,
      title: "...",
      backgroundColor: "#f0f0f0"});

  let relayCloseRequests = false;

  ipcMain.on("relayCloseRequests", e => {
    relayCloseRequests = true;
    e.returnValue = null;
  });

  mainWindow.on("close", e => {
    if (relayCloseRequests) {
      e.preventDefault();
      mainWindow.webContents.send("requestClose");
    }
  });

  mainWindow.webContents.on("did-start-loading", e => {
    if (e.sender.isLoadingMainFrame()) {
      connectionConfigRequested = false;
      relayCloseRequests = false;
    }
  });

  mainWindow.webContents.on("will-navigate", (e, url) => {
    if (url != mainWindow.webContents.getURL()) {
      e.preventDefault();
    }
  });

  mainWindow.on("closed", () => {
    mainWindow = null;
    if (server) {
      server.stdin.end();
    }
  });

  ipcMain.on("getConnectionConfig", () => {
    connectionConfigRequested = true;
    sendConnectionConfigIfPossible();
  });

  if (devel || process.argv.includes(devtoolsSwitch)) {
    mainWindow.webContents.openDevTools();
  }
}

function sendConnectionConfigIfPossible() {
  if (connectionConfigRequested && (connectionConfig != null) && mainWindow) {
    mainWindow.webContents.send("connectionConfig", connectionConfig);
    connectionConfigRequested = false;
  }
}

function onCertificateError(event, url, error, certificate, callback) {
  let ok = false;
  const cd = certificateDialog;
  if (cd && mainWindow) {
    if (cd.url == url) {
      if (certificate.fingerprint.startsWith("sha256/")) {
        const fingerprint = Buffer.from(certificate.fingerprint.substring(7), "base64").toString("hex");

        const knownServersFile = path.join(dataDir, "knownServers");
        let knownServers;
        try {
          knownServers = JSON.parse(fs.readFileSync(knownServersFile, "utf8"));
          if (!(knownServers instanceof Object) || Array.isArray(knownServers)) {
            knownServers = {};
          }
        } catch (_) {
          knownServers = {};
        }

        const exists = Object.prototype.hasOwnProperty.call(knownServers, cd.serverAddress);
        if (exists && (knownServers[cd.serverAddress] === fingerprint)) {
          ok = true;
        } else if ((dialog.showMessageBoxSync({
            type: exists ? "warning" : "question",
            buttons: [cd.connectMessage, cd.cancelMessage],
            title: mainWindow.getTitle(),
            message: `${exists ? cd.idChangedMessage : cd.newServerMessage}\n\n`
                + `${cd.addressMessage}\n${cd.serverAddress}\n\n`
                + `${cd.idMessage}\n${fingerprint}\n\n${cd.verifyIdMessage}`,
            cancelId: 1,
            defaultId: exists ? 1 : 0,
            noLink: true}) == 0) && mainWindow) {
          knownServers[cd.serverAddress] = fingerprint;
          try {
            writeViaTempFile(JSON.stringify(knownServers), knownServersFile);
          } catch (_) {}
          ok = true;
        }
      } else {
        dialog.showErrorBox("Unknown certificate fingerprint format", `Value: ${certificate.fingerprint}`);
      }
    } else {
      dialog.showErrorBox("Server URL mismatch", `Expected: ${cd.url}\nRequested: ${url}`);
    }
  }

  if (ok) {
    event.preventDefault();
  }
  callback(ok);
}

function writeViaTempFile(data, fileName) {
  const tempFileName = `${fileName}~${crypto.randomBytes(8).toString("hex")}`;
  try {
    fs.writeFileSync(tempFileName, data, {flag: "wx"});
    fs.renameSync(tempFileName, fileName);
  } catch (e) {
    try {
      fs.unlinkSync(tempFileName);
    } catch (_) {}
    throw e;
  }
}
